// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/*****************************************************************************
fichier : cOptimisateur.h
contenu : definition de la classe cOptimisateur
*****************************************************************************/
#ifndef _COPTIMISATEUR_H
#define _COPTIMISATEUR_H

#include <string>
#include <vector>

using namespace std;

#include "general.h"
#include "cAlgorithme.h"
#include "cClimat.h"

// ===========================================================================
// cOptimisateur
// Remarque : ParaType et NbIterations double car sinon probleme en lecture
// ===========================================================================
class cOptimisateur
{
 public:  //------------------------------------------------------------------

  double Para[NB_PARAMETRES];
  double ParaMax[NB_PARAMETRES];
  double ParaMin[NB_PARAMETRES];
  double ParaInit[NB_PARAMETRES];
  double ParaType[NB_PARAMETRES]; 
  double ParaPas[NB_PARAMETRES];

  string Optimiser;

  OPTION_CLIMATS OptionClimats;
  vector <cClimat*> LClimatsReels;
  string LocalisationClimat;
  int NbClimatsPoint;
  int NbSimulRealisees;

  cAlgorithme* AlgorithmeUtilise;

  void FaireOptimisation(void);
  void MAJTetas(void);
  double Simuler(int nb_annees_generees); 
  double Simuler(vector <cClimat*> lclimats); 
  // renvoie la valeur moyenne du crit�re sur les simulations demand�es
  void EvaluerOptimum(void);


    
 private:  //-----------------------------------------------------------------
  string NomAlgorithme;

  void InitOptimisateur(bool b_ouvrir_fichiers);
  double LireValeurCritere(void);
  void SortirProprementOptimisateur(bool b_fermer_fichiers);
};


#endif
