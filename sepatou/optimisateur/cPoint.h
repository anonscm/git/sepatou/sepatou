// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cPoint.h
contenu : d�finition de la classe cPoint
******************************************************************************/
#ifndef _CPOINT_H
#define _CPOINT_H

#include <vector>

using namespace std;

#include "general.h"
#include "cClimat.h"

// ============================================================================
// cPoint
// ============================================================================
class cPoint
{
 public:  //-------------------------------------------------------------------
  double Para[NB_PARAMETRES];
  double Critere;
  vector <double> LCritere;
  
  cPoint( double para[NB_PARAMETRES] );
  cPoint( double para[NB_PARAMETRES], vector <cClimat*> lclimats);
};



#endif
