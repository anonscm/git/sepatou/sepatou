// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/*****************************************************************************
fichier : cOptimisateur.cc
contenu : definition des methodes de la classe cOptimisateur
*****************************************************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

#include "cOptimisateur.h"

#include "general.h"
#include "cResultatOptimisateur.h"
#include "cAnomalie.h"
#include "cAlgorithmeSystematique.h"
#include "cAlgorithmeKiefer.h"
#include "cAlgorithmePartition2p.h"
#include "pat.h"
#include "cSimulateur.h"
#include "cSIObservation.h"
#include "cFoInterpretation.h"
#include "cSDPlanificateur.h"
#include "cEnvironnementExterieur.h"
#include "cRandom.h"

extern cResultatOptimisateur ResultatOptimisateur;
extern cSimulateur Simulateur;
extern cSIObservation SIObservation;
extern cSDPlanificateur SDPlanificateur;
extern cEnvironnementExterieur EnvironnementExterieur;
extern cRandom Random;

// ===========================================================================
// FaireOptimisation
// ===========================================================================
void cOptimisateur::FaireOptimisation(void)
{
  try
    {
      // initialisation 
      Simulateur.InitElementsInvariants();  // du simulateur
      InitOptimisateur(true);                   // de l'optimisateur
  
      // optimisation
      AlgorithmeUtilise->Optimiser();

      // lib�ration m�moire 
      Simulateur.LibererMemoire();          // du simulateur
      SortirProprementOptimisateur(true);       // de l'optimisateur
    }
  catch (cAnomalie* anomalie) {
    anomalie->Afficher();}
}


// ===========================================================================
// InitOptimisateur
// ===========================================================================
void cOptimisateur::InitOptimisateur(bool b_ouvrir_fichiers)
{
  ifstream fAlgorithme;
  string commentaire, option_climats, NomFichier;
  double a, b, c[NB_PARAMETRES];
  double ecart_max;
  int nb_pt_cellule, nb_simulations_max, nb_meilleures, nb_iterations_max;
 
  // Lecture de la description de l'algorithme et des param�tres � optimiser
  NomFichier = CHEMIN_ALGORITHME_CFG;
  fAlgorithme.open(NomFichier.c_str(), ios::in);
  if (!fAlgorithme)
    throw (new cAnomalie ("Dans la methode cOptimisateur.InitOptimisateur, probleme pour ouvrir le fichier : " + NomFichier) );

  fAlgorithme >> commentaire >> commentaire >> NomAlgorithme;
  fAlgorithme >> commentaire >> commentaire >> commentaire >> commentaire 
	      >> commentaire >> commentaire >> commentaire;
  for (int i=0; i<NB_PARAMETRES; i++)
    {
            fAlgorithme >> commentaire;
            fAlgorithme >> ParaMin[i];
            fAlgorithme >> ParaMax[i];
	    fAlgorithme >> ParaType[i];
	    fAlgorithme >> ParaPas[i];
	    fAlgorithme >> ParaInit[i];
	    fAlgorithme >> c[i];
    };
  fAlgorithme >> commentaire >> commentaire >> a;
  fAlgorithme >> commentaire >> commentaire >> b;
  fAlgorithme >> commentaire >> commentaire >> nb_iterations_max;
  fAlgorithme >> commentaire >> commentaire >> ecart_max;
  fAlgorithme >> commentaire >> commentaire >> Optimiser;
  fAlgorithme >> commentaire >> commentaire >> nb_pt_cellule;
  fAlgorithme >> commentaire >> commentaire >> option_climats;
  fAlgorithme >> commentaire >> commentaire >> nb_simulations_max;
  fAlgorithme >> commentaire >> commentaire >> nb_meilleures;
  fAlgorithme.close();

  // MAJ attribut OptionClimats
  LocalisationClimat = EnvironnementExterieur.LocalisationClimat;
  if (option_climats == "aleatoires")
    OptionClimats = ALEATOIRES;
  else
    if (option_climats == "semi_aleatoires")
      OptionClimats = SEMI_ALEATOIRES;
    else
      if (option_climats == "reels")
	{
	  OptionClimats = REELS;
	  // mise � jour de LClimatsReels
	  if (LocalisationClimat == "Segala")
	    for (int a=1971; a<1996; a++)
	      {
		LClimatsReels.push_back(new cClimat);
		LClimatsReels[a-1971]->Init(LocalisationClimat,a);
	      }
	  else
	    if (LocalisationClimat == "Rennes")
	      for (int a=1983; a<2000; a++)
		{
		  LClimatsReels.push_back(new cClimat);
		  LClimatsReels[a-1983]->Init(LocalisationClimat,a);
		}
	    else
	      throw (new cAnomalie ("Dans la methode cOptimisateur.InitOptimisateur, la valeur de LocalisationClimat n'est pas Segala ou Rennes") );
	}
      else
	   throw (new cAnomalie ("Dans la methode cOptimisateur.InitOptimisateur, la valeur OptionClimats doit etre aleatoire, semi-aleatoire ou reel"));
  
  // MAJ du nombre de simulation par point
  NbClimatsPoint = Simulateur.NbClimatsGeneres;
  
  // cr�ation de l'instance d'algorithme utilis�
  if (NomAlgorithme == "Systematique")
    AlgorithmeUtilise = new cAlgorithmeSystematique;
  else
    if (NomAlgorithme == "Kiefer")
      {
	if (Optimiser == "maximiser")
	  { if (a < 0) a = - a;}
	else
	  if (Optimiser == "minimiser")
	    { if (a > 0) a = - a;}
	  else 
	    throw (new cAnomalie ("Dans la methode cOptimisateur.InitOptimisateur, la valeur d'Optimiser doit etre maximiser ou minimiser"));
	AlgorithmeUtilise = new cAlgorithmeKiefer(a, b, c, 
						  nb_iterations_max, ecart_max);
      }
  else
    if (NomAlgorithme == "Partition2p")
      AlgorithmeUtilise = new cAlgorithmePartition2p(nb_pt_cellule, nb_simulations_max, nb_meilleures);
    else
      throw (new cAnomalie ("Dans la methode cOptimisateur.InitOptimisateur, le nom de l'algorithme n'est pas reconnu"));
 
 // Ouverture des fichiers d'enregistrement
 ResultatOptimisateur.OuvrirFichiersSortiesOptimisateur(NomAlgorithme, b_ouvrir_fichiers);
}


// ===========================================================================
// LibererMemoireOptimisateur
// ===========================================================================
void cOptimisateur::SortirProprementOptimisateur(bool b_fermer_fichiers)
{
 // Fermeture des fichiers d'enregistrement
 ResultatOptimisateur.FermerFichiersSortiesOptimisateur(NomAlgorithme, b_fermer_fichiers);  

 delete AlgorithmeUtilise;

 for (unsigned int i=0; i<LClimatsReels.size(); i++)
   delete LClimatsReels[i];
 LClimatsReels.clear();
}


// ===========================================================================
// MAJTetas
// ===========================================================================
void cOptimisateur::MAJTetas(void)
{
  SDPlanificateur.Teta1 = Para[0];
  SDPlanificateur.Teta2 = Para[1];
  SDPlanificateur.Teta3 = Para[2];
  SDPlanificateur.Teta4 = Para[3];
  SDPlanificateur.Teta5 = Para[4];
  SDPlanificateur.Teta6 = Para[5];
  SDPlanificateur.Teta7 = Para[6];
  SDPlanificateur.Teta8 = Para[7];
  SDPlanificateur.Teta9 = Para[8];
  SDPlanificateur.Teta10 = Para[9];
}


// ===========================================================================
// LireValeurCritere
// ATTENTION : il faut rechercher quelle est la derniere fonction 
// d'interpretation utilisateur definie de type float. Ceci permet de 
// pr�compiler la classe cSIObservation. (Sinon on pourrait utiliser 
// NB_fFO_INTERPRETATION d�fini dans Strategie.h)
// ===========================================================================
double cOptimisateur::LireValeurCritere(void)
{
  int i, i_critere = 0;
  
  if (SIObservation.fFo[0] == 0)
    throw (new cAnomalie ("Dans la methode cOptimisateur.LireValeurCritere, il n'y a aucun crit�re num�rique d�fini."));
  else
    for (i=0; i<NB_MAX_FO_TYPE; i++)
      if (SIObservation.fFo[i] == NULL)
	i = NB_MAX_FO_TYPE;
      else
	i_critere = i;

  return SIObservation.fFo[i_critere]->CODE();
}


// ===========================================================================
// Simuler : renvoie la valeur moyenne du crit�re
// ===========================================================================
double cOptimisateur::Simuler(int nb_annees_generees)
{
  double critere = 0;

  if ( nb_annees_generees  == 0 ) 
    throw (new cAnomalie ("Dans la methode cOptimisateur.Simuler, pas de nombre d'ann�es � sumuler donn� en argument"));

  for (int i=0; i<nb_annees_generees; i++)
    {
      Simulateur.Annee = ANNEE_GENEREE + i;     
      Simulateur.Simuler();
      critere += LireValeurCritere();
    };
    critere = critere / (double) nb_annees_generees;
  
  return critere;
}

double cOptimisateur::Simuler(vector <cClimat*> lclimats)
{
  double critere = 0;

  if ( lclimats.size() == 0 ) 
    throw (new cAnomalie ("Dans la methode cOptimisateur.Simuler, aucun climat � simuler n'est donn� en argument"));

  for (unsigned int j=0; j<lclimats.size(); j++)
    {
      Simulateur.Annee = j;
      Simulateur.Simuler( lclimats[j] );
      critere += LireValeurCritere();
    }
    critere = critere / (double) lclimats.size();
  
  return critere;
}


// ===========================================================================
// EvaluerOptimum
// ===========================================================================
void cOptimisateur::EvaluerOptimum(void)
{
  ifstream fOptimum;
  string NomFichier;
  vector <double> l_critere;
  double ParaIntervalle[NB_PARAMETRES];
  double critere_min, critere_max, moyenne_critere, ecart_type, c;
  unsigned int n;
  int i,k;
  double NB_SIMULATIONS = 1000;

  try
    {
      Random.Init(1);

      // initialisation //////////////////////////////////////////////////////////
      Simulateur.InitElementsInvariants();  // du simulateur
      InitOptimisateur(false);              // de l'optimisateur
      // lecture de l'optimum
      NomFichier = CHEMIN_OPTIMUM;
      fOptimum.open(NomFichier.c_str(), ios::in);
      if (!fOptimum)
	throw (new cAnomalie ("Dans la methode cOptimisateur.EvaluerOptimum, probleme pour ouvrir le fichier : " + NomFichier) );

      if (NomAlgorithme != "Partition2p")
	  for (i=0; i<NB_PARAMETRES; i++)
	    fOptimum >> Para[i];
      else
	for (i=0; i<NB_PARAMETRES; i++)
	  {	 
 	    fOptimum >> ParaMin[i];
 	    fOptimum >> ParaMax[i];
	    ParaIntervalle[i] = ParaMax[i] - ParaMin[i];
	  }
      fOptimum.close();

      // �valuation de l'optimum ////////////////////////////////////////////
      if (NomAlgorithme != "Partition2p")
	{
	  MAJTetas();
	  for (i = 0; i<NB_SIMULATIONS; i++)
	    l_critere.push_back(Simuler(1));
	}
      else
	for (i = 0; i<NB_SIMULATIONS; i++)
	  {
	    // mise � jour de Para
	      for (k=0; k<NB_PARAMETRES; k++)
		if (ParaType[k] != VIDE)
		  Para[k] = ParaMin[k] + Random.Generate01() * ParaIntervalle[k];
		else
		  Para[k] = VIDE;

	    MAJTetas();
	    l_critere.push_back(Simuler(1));
	  }

      // calcul moyenne, �cart type et intervalle de variation  //////////////
      critere_min = l_critere[0];
      critere_max = l_critere[0];
      moyenne_critere = l_critere[0];
      for (n=1; n<l_critere.size(); n++)
	{
	  c = l_critere[n];
	  if (c < critere_min) 
	    critere_min = c;
	  if (c > critere_max) 
	    critere_max = c;
	  moyenne_critere += c;
	}
      moyenne_critere = moyenne_critere / (double) l_critere.size();
      ecart_type = 0;
      for (n=0; n<l_critere.size(); n++)
	ecart_type += pow( moyenne_critere - l_critere[n] , 2);
      ecart_type = ecart_type /(double) l_critere.size() ;
      ecart_type = sqrt (ecart_type);

      // affichage //////////////////////////////////////////////////////////
      cout << "EVALUATION DE L'OPTIMUM" << endl;
      cout << "Valeur moyenne : " << moyenne_critere << endl;
      cout << "Ecart type : " << ecart_type << endl;
      cout <<"Intervalle de variation : ["<<critere_min<<"  "<<critere_max<<"]"<<endl;


      // lib�ration m�moire 
      Simulateur.LibererMemoire();          // du simulateur
      SortirProprementOptimisateur(false);       // de l'optimisateur
    }
  catch (cAnomalie* anomalie) {
    anomalie->Afficher();}
}

