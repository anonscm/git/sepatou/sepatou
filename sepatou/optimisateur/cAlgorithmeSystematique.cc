// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cAlgorithmeSystematique.cc
contenu : definition des m�thodes de la classe cAlgorithmeSystematique
******************************************************************************/

#include <iostream>
#include <fstream>

#include "cAlgorithmeSystematique.h"

#include "general.h"
#include "cOptimisateur.h"
#include "cResultatOptimisateur.h"

extern cOptimisateur Optimisateur;
extern cResultatOptimisateur ResultatOptimisateur;

//---------------------------------------------------------------------------
// Optimiser
//---------------------------------------------------------------------------
void  cAlgorithmeSystematique::Optimiser(void)
{ 
  bool boucler, optimum_non_initialise;
  int i;
  double valeur_optimum, valeur_critere, P[NB_PARAMETRES];

  Optimisateur.NbSimulRealisees = 0;

  //initialisation des param�tres
  for (i=0; i<NB_PARAMETRES; i++)
    Optimisateur.Para[i] = Optimisateur.ParaMin[i];
  

  optimum_non_initialise = true;
  boucler = true;
  while (boucler)
    {
      // R�alisation des simulations
      Optimisateur.MAJTetas();

      if (Optimisateur.OptionClimats == REELS)
	{
	  valeur_critere = Optimisateur.Simuler(Optimisateur.LClimatsReels);
	  Optimisateur.NbSimulRealisees += Optimisateur.LClimatsReels.size();
	}
      else
	{
	  valeur_critere = Optimisateur.Simuler(Optimisateur.NbClimatsPoint);
	  Optimisateur.NbSimulRealisees += Optimisateur.NbClimatsPoint;
 	}

      // Ecriture des parametres et du crit�re
      if (ResultatOptimisateur.Sauver_Parametres)
	{
	  for (i=0; i<NB_PARAMETRES; i++)
	    ResultatOptimisateur.fParametres << Optimisateur.Para[i] << "\t";
	  ResultatOptimisateur.fParametres << endl;
	};
      if (ResultatOptimisateur.Sauver_Critere)
	ResultatOptimisateur.fCritere << valeur_critere <<endl;
      
      // M�morisation de l'optimum
      if ( optimum_non_initialise || 
	   (Optimisateur.Optimiser == "maximiser" 
	    && valeur_critere > valeur_optimum) ||
	   (Optimisateur.Optimiser == "minimiser"
 	    && valeur_critere < valeur_optimum) )
	{
	  valeur_optimum =  valeur_critere;
	  for (i=0; i<NB_PARAMETRES; i++)
	    P[i] = Optimisateur.Para[i];
	  
	  optimum_non_initialise = false;
	};
      
      boucler = EvoluerParametres();
    };
  
 
  // enregistrement de l'optimum
  for (i=0; i<NB_PARAMETRES; i++)
    ResultatOptimisateur.fOptimum << P[i] << "\t";
  ResultatOptimisateur.fOptimum << valeur_optimum << endl;

  cout << "Nombre de simulations r�alis�es : " << Optimisateur.NbSimulRealisees << endl;
}


//---------------------------------------------------------------------------
// EvoluerParametres : retourne true si passage � jeu suivant de parametres
// possible.
// Du fait que les parametres utilis�s ne sont pas n�cessairement continus
// (� partir de 1 jusqu'� n), l'algorithme ne peut pas etre une suite de for.
// Il explore donc un arbre de recherche.
//---------------------------------------------------------------------------
bool cAlgorithmeSystematique::EvoluerParametres(void)
{
  bool boucler = true;
  bool rechercher = true;

  int i = NB_PARAMETRES-1;
  
  while (rechercher)
    {
      if (Optimisateur.ParaType[i] == VIDE)
	if (i==0)
	  { 
	    rechercher= false; 
	    boucler = false;
	  }
	else
	  {
	    Optimisateur.Para[i] = Optimisateur.ParaMin[i];
	    i = i-1;
	  }
      else
	if (Optimisateur.Para[i] + Optimisateur.ParaPas[i] 
	    <= Optimisateur.ParaMax[i]) 
	  {
	    Optimisateur.Para[i] += Optimisateur.ParaPas[i];
	    rechercher = false;
	  }
	else
	  if (i==0)
	    { 
	      rechercher= false; 
	      boucler = false;
	    }
	  else
	    {
	      Optimisateur.Para[i] = Optimisateur.ParaMin[i];
	      i = i-1;
	    };
    }; 

  return boucler;
}
