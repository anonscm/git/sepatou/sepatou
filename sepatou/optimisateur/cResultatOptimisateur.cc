// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cResultatOptimisateur.cc
contenu : definition des methodes de la classe cResultatOptimisateur
******************************************************************************/

#include <string>

#include "cResultatOptimisateur.h"

#include "general.h"
#include "cAnomalie.h"



// ============================================================================
// OuvrirFichiersSortiesOptimisateur
// ============================================================================
void cResultatOptimisateur::OuvrirFichiersSortiesOptimisateur(string nom_algorithme, bool b_ouvrir)
{
  ifstream fSortiesOptimisateur;
  string Fichier, sortie;

  // lecture des sorties � sauvegarder pour l'optimisateur 
  Fichier = CHEMIN_SORTIES_OPTIMISATEUR_CFG;
  fSortiesOptimisateur.open(Fichier.c_str(), ios::in);
  if (!fSortiesOptimisateur)
    throw (new cAnomalie ("Dans la methode cResultat.OuvrirFichiersSortiesOptimisateur, probleme pour ouvrir le fichier : " + Fichier) );
 
  Sauver_Critere = false;
  Sauver_Parametres = false;
  Sauver_GradientStochastique = false;
  Sauver_Niveau = false;

  if (b_ouvrir)
    {
      while (fSortiesOptimisateur >> sortie)
	{
	  if (sortie == "Critere") Sauver_Critere = true;
	  else 
	    if (sortie == "Parametres") Sauver_Parametres = true;
	    else 
	      if (sortie == "GradientStochastique") Sauver_GradientStochastique = true;
	      else 
		if (sortie == "Niveau") Sauver_Niveau = true;
		else
		  throw (new cAnomalie ("Dans la methode cResultat::OuvrirFichiersSortiesOptimisateur, une variable inconnue est demandee � sauvegarder : " + sortie) );
	};
      
      fSortiesOptimisateur.close();
      
      // ouverture des fichiers pour les sorties � sauvegarder 
      
      // on enregistre toujours l'optimum
      Fichier = CHEMIN_OPTIMUM;
      fOptimum.open(Fichier.c_str(),ios::out);
      if (!fOptimum)
	throw (new cAnomalie ("Dans la methode cResultat.OuvrirFichiersSortiesOptimisateur, probleme pour ouvrir le fichier : " + Fichier) );
      
      if (Sauver_Critere)
	{
	  Fichier = CHEMIN_CRITERE;
	  fCritere.open(Fichier.c_str(),ios::app);
	  if (!fCritere)
	    throw (new cAnomalie ("Dans la methode cResultat.OuvrirFichiersSortiesOptimisateur, probleme pour ouvrir le fichier : " + Fichier) );
	};
      
      if (Sauver_Parametres && nom_algorithme != "Partition2p")
	{
	  Fichier = CHEMIN_PARAMETRES;
	  fParametres.open(Fichier.c_str(),ios::app);
	  if (!fParametres)
	    throw (new cAnomalie ("Dans la methode cResultat.OuvrirFichiersSortiesOptimisateur, probleme pour ouvrir le fichier : " + Fichier) );
	}
      else
	Sauver_Parametres = false;
      
      if (Sauver_GradientStochastique && nom_algorithme == "Kiefer")
	{
	  Fichier = CHEMIN_GRADIENT;
	  fGradientStochastique.open(Fichier.c_str(),ios::app);
	  if (!fGradientStochastique)
	    throw (new cAnomalie ("Dans la methode cResultat.OuvrirFichiersSortiesOptimisateur, probleme pour ouvrir le fichier : " + Fichier) );
	}
      else 
	Sauver_GradientStochastique = false;
      
      if (Sauver_Niveau && nom_algorithme == "Partition2p")
	{
	  Fichier = CHEMIN_NIVEAU;
	  fNiveau.open(Fichier.c_str(),ios::app);
	  if (!fNiveau)
	    throw (new cAnomalie ("Dans la methode cResultat.OuvrirFichiersSortiesOptimisateur, probleme pour ouvrir le fichier : " + Fichier) );
	}
      else
	Sauver_Niveau = false;
    }
}


// ============================================================================
// FermerFichiersSortiesOptimisateur
// ============================================================================
void cResultatOptimisateur::FermerFichiersSortiesOptimisateur(string nom_algorithme, bool b_fermer)
{
if (b_fermer)
  {
    // fermeture des fichiers ouverts
    fOptimum.close();
    if (Sauver_Critere)
      fCritere.close();
    if (Sauver_Parametres)
      fParametres.close();
    if (Sauver_GradientStochastique)
      fGradientStochastique.close();
    if (Sauver_Niveau)
      fNiveau.close();
  }
}


