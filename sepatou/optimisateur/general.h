// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : general.h
contenu : definition des define et enum de l'optimisateur OPTI-SEPATOU
******************************************************************************/

#ifndef _GENERAL_H
#define _GENERAL_H



// DEFINITION DE CHEMINS
#define CHEMIN_ALGORITHME_CFG "tmp/Algorithme.cfg"
#define CHEMIN_SORTIES_OPTIMISATEUR_CFG "tmp/SortiesOptimisateur.cfg"
#define CHEMIN_PARAMETRES "tmp/Parametres"
#define CHEMIN_CRITERE "tmp/Critere"
#define CHEMIN_OPTIMUM "tmp/Optimum"
#define CHEMIN_GRADIENT "tmp/GradientStochastique"
#define CHEMIN_NIVEAU "tmp/Niveau"
#define CHEMIN_REPRISE "Reprise"

// PARAMETRES A OPTIMISER
// nombre maximum de param�tres � optimiser
#define NB_PARAMETRES 10

// nombre de valeur d'�cart � moyenner pour comparer
// avec la valeur donn�e pour arreter l'algorithme de Kiefer
#define NB_ECART 10


// VALEURS PARTICULIERES
#define VIDE -9999
#define INIT_ALEATOIRE -9998

// option pour les climats
enum OPTION_CLIMATS {ALEATOIRES, SEMI_ALEATOIRES, REELS};

#endif
