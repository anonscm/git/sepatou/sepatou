// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cCellule.cc
contenu : definition des m�thodes de la classe cCellule
******************************************************************************/


#include <math.h>

#include "cCellule.h"

#include "pat.h"
#include "general.h"
#include "cPoint.h"
#include "cOptimisateur.h"
#include "cRandom.h"
#include "cClimat.h"
#include "cAlgorithmePartition2p.h"

extern cOptimisateur Optimisateur;
extern cRandom Random;


// ============================================================================
// constructeur
// ============================================================================
cCellule::cCellule(double borne_min[NB_PARAMETRES], 
		   double borne_max[NB_PARAMETRES],
		   cCellule* pere )
{
  int i, nb_pt_cellule;
  unsigned int k, j;
  double intervalle[NB_PARAMETRES], para [NB_PARAMETRES];
  vector <cClimat*> lclimats;

  nb_pt_cellule = ((cAlgorithmePartition2p*) Optimisateur.AlgorithmeUtilise)->NbPtCellule;

  // mise � jour des bornes
  for (i = 0; i<NB_PARAMETRES; i++)
    {
      BorneMin[i] = borne_min[i];
      BorneMax[i] = borne_max[i];
      intervalle[i] = BorneMax[i] - BorneMin[i];
    }

  /*
  /////////////////// modif densit� constante/////////////////////////////
  // d�commenter ce paragraphe et modifier le test d'arret 
  // de cAlgorithmePartition2p::Optimiser
  // Mettre + NbSimulPartition en commentaire
  ////////////////////////////////////////////////////////////////////
  double surface = 1;
  double surface_initiale = 1;
  for (i = 0; i<NB_PARAMETRES; i++)
    if (Optimisateur.ParaType[i] != VIDE)
      {
	surface = surface * intervalle[i];
	surface_initiale = surface_initiale * 
	  (Optimisateur.ParaMax[i] - Optimisateur.ParaMin[i]);
      }
      nb_pt_cellule = (int) ceil(nb_pt_cellule*surface/surface_initiale); 
  ////////////////////////////////////////////////////////////////////
  */

  // mise � jour du pere
  Pere = pere;

  // mise � jour du niveau 
  if (pere == NULL)
    Niveau = 0;
  else
    Niveau = Pere->Niveau + 1;

  // examen des climats � utiliser dans la cellule
  if (Optimisateur.OptionClimats == SEMI_ALEATOIRES)
    {
      // semi al�atoire : meme ensemble de climats sur la cellule
      for (i = 0; i < Optimisateur.NbClimatsPoint; i++)
	{
	  lclimats.push_back(new cClimat);
	  lclimats[i]->Init(Optimisateur.LocalisationClimat,
			    ANNEE_GENEREE + i);
	}
    }
  else
    if (Optimisateur.OptionClimats == REELS)
      // climats reels
      lclimats = Optimisateur.LClimatsReels;

  // creation des points
  for (i = 0; i < nb_pt_cellule; i++)
    {
      for (j = 0; j<NB_PARAMETRES; j++)
  	para [j] = intervalle[j] * Random.Generate01() + BorneMin[j];
      if (Optimisateur.OptionClimats == ALEATOIRES)
	LPoints.push_back ( new cPoint (para) );
      else
	LPoints.push_back ( new cPoint (para, lclimats) ); 
    }
  
  // mise a jour du critere : moyenne des criteres des points de la cellule
  Critere = 0;
  for (k = 0; k < LPoints.size(); k++)
    Critere += LPoints[k]->Critere;
  Critere = Critere / LPoints.size();
  /*
  //////////////////// optimum des criteres des points de la cellule///////
  // d�commenter ce paragraphe et commenter les 3 lignes pr�c�dentes
  Critere = LPoints[0]->Critere;
  for (k = 1; k < LPoints.size(); k++)
    if ((Optimisateur.Optimiser == "maximiser") 
	&& (LPoints[k]->Critere > Critere))
      Critere = LPoints[k]->Critere;
    if ((Optimisateur.Optimiser == "minimiser") 
	&& (LPoints[k]->Critere < Critere))
	Critere = LPoints[k]->Critere;
  ///////////////////////////////////////////////////////////////////////
  */
  
  // mise � jour de la variance : par rapport � chaque �valuation de crit�re
  // sur la cellule (plusieurs climats possibles par point)
  Variance = 0;
  for (k = 0; k < LPoints.size(); k++)
    for (j = 0; j < LPoints[k]->LCritere.size(); j++)
      Variance += pow( Critere - LPoints[k]->LCritere[j] , 2);
  Variance = Variance / (nb_pt_cellule * Optimisateur.NbClimatsPoint);
  /*
  //////////////////// par rapport au crit�re de chaque point /////////
  // d�commenter ce paragraphe et commenter les 4 lignes pr�cedentes
  for (k = 0; k < LPoints.size(); k++)
    Variance += pow( Critere - LPoints[k]->Critere , 2);
  Variance = Variance / nb_pt_cellule;
  ///////////////////////////////////////////////////////////////////////
  */

  AExplorer = true;
  Meilleure = false;

  // destruction des climats cr��s
  if (Optimisateur.OptionClimats == SEMI_ALEATOIRES)
    for (k = 0; k < lclimats.size(); k++)
      delete lclimats[k];
}


// ============================================================================
// destructeur
// ============================================================================
cCellule::~cCellule()
{
  for (unsigned int i=0; i<LPoints.size(); i++)
    delete LPoints[i];
}


