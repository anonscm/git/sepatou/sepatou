// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cAlgorithmePartition2p.cc
contenu : definition des m�thodes de la classe cAlgorithmePartition2p
******************************************************************************/




#include <math.h>

#include "cAlgorithmePartition2p.h"
#include "cResultatOptimisateur.h"

#include "general.h"
#include "cOptimisateur.h"
#include "cResultatOptimisateur.h"
#include "cRandom.h"

extern cOptimisateur Optimisateur;
extern cResultatOptimisateur ResultatOptimisateur;
extern cRandom Random;

// ============================================================================
// desctructeur
// ============================================================================
cAlgorithmePartition2p::cAlgorithmePartition2p(int nb_pt_cellule, int nb_simulations_max, 
					       int nb_meilleures)
{
NbPtCellule = nb_pt_cellule;
NbSimulationsMax = nb_simulations_max;
NbMeilleures = nb_meilleures;
}


// ============================================================================
// desctructeur
// ============================================================================
cAlgorithmePartition2p::~cAlgorithmePartition2p()
{
  unsigned int i;
  for (i=0; i<LCellulesAExplorer.size(); i++)
    if (! LCellulesAExplorer[i]->Meilleure)
      delete LCellulesAExplorer[i];
  for (i=0; i<LCellulesMeilleures.size(); i++)
    delete LCellulesMeilleures[i];
}


// ============================================================================
// Optimiser
// ============================================================================
void cAlgorithmePartition2p::Optimiser(void)
{
  cCellule* c;
  cCellule*  cellule_courante;
  vector <cCellule*> l_cellules_creees;

  Optimisateur.NbSimulRealisees = 0;
  // mise � jour de l'estimation du nombre maximum 
  // de simulations r�alis�es dans une partition
  MAJNbSimulPartition();
  cout << "Nombre maximum de simulations n�cessaires pour r�aliser une partition : " << NbSimulPartition <<endl;

  // Initialisation de la liste des cellules � explorer et des meilleures
  // avec une cellule correspondant au domaine
  c = new cCellule ( Optimisateur.ParaMin, Optimisateur.ParaMax, NULL );
  LCellulesAExplorer.push_back ( c );
  LCellulesMeilleures.push_back ( c );
  c->Meilleure = true;
  CritereMax = c->Critere;
  VarianceMax = c->Variance;

  while (!( LCellulesAExplorer.empty() ) && 
	 ( Optimisateur.NbSimulRealisees + NbSimulPartition <= NbSimulationsMax))
      {
	// la cellule la plus prometteuse va �tre partitionn�e
	cellule_courante = ChoisirCelluleExploree();     
	// enregistrement du niveau de la cellule partionn�e
	if (ResultatOptimisateur.Sauver_Niveau)
	  LNiveauxExplores.push_back ( cellule_courante->Niveau ); 

	l_cellules_creees = Partitioner2p  (cellule_courante ); 
	if (l_cellules_creees.size() != 0)
	  {
	    if (cellule_courante->Meilleure)
	      EnleverCelluleMeilleure(cellule_courante);
	    delete cellule_courante;
	    // rangement �ventuel des meilleures cellules cr��es
	    InsererCellulesMeilleures (l_cellules_creees);
	    // Attention les cellules cr��es dans la liste des meilleures 
	    // avant de les ins�rer dans la liste AExplorer. En effet, seules les cellules 
	    // partitionnables sont plac�es dans AExplorer, les cellules non partitionables 
	    // non dans la liste des meilleures sont d�truites.
	    InsererCellulesAExplorer (l_cellules_creees); 
	  }
	else
	  // c'est une feuille de l'arbre
	  if (cellule_courante->Meilleure)
	    cellule_courante->AExplorer = false;
	  else
	    delete cellule_courante;
      }

  // sauvegarde des sorties
  Enregistrer();

  cout << "Nombre de simulations r�alis�es : " << Optimisateur.NbSimulRealisees << endl;
}


// ============================================================================
// InsererCellulesAExplorer : ajoute les cellules partitionables de la liste 
// donn�e en argument � la liste des cellules � explorer. D�truit les cellules
// � ins�rer qui ne sont pas parmi les meilleures
// ============================================================================
void cAlgorithmePartition2p::InsererCellulesAExplorer 
                                               (vector <cCellule*> l_cellules)
{
  for (unsigned int i=0; i<l_cellules.size (); i++)
      LCellulesAExplorer.push_back(l_cellules[i]);
}


// ============================================================================
// ChoisirCelluleExploree : retourne 1 cellule de la liste LCellulesAExplorer 
// jug�e la plus prometteuse (grand crit�re et/ou grande variance). 
// Il s'agit ici de r�soudre un probl�me d'optimisation o� l'on souhaite 
// maximiser le crit�re et la variance.
// LCellulesAExplorer ne doit pas etre vide.
// ============================================================================
cCellule* cAlgorithmePartition2p::ChoisirCelluleExploree (void)
{
  cCellule* cellule;

  int i_choix;
  double eval, eval_choix, alpha;
  double critere_max, variance_max;
  bool init_eval_choix;

  // on prend les crit�res et variances max de l'it�ration pr�c�dente
  critere_max = CritereMax;
  variance_max = VarianceMax;
      
  alpha = tan( Random.Generate01() * M_PI/2 );
  if (Optimisateur.Optimiser == "minimiser")
    alpha = -alpha;

  init_eval_choix = false;

  for (unsigned int i=0; i < LCellulesAExplorer.size(); i++)
    {
      if (critere_max!=0 && variance_max!=0)
	eval = (LCellulesAExplorer[i]->Critere / critere_max) + 
	  alpha * (LCellulesAExplorer[i]->Variance / variance_max);
      else
	eval = LCellulesAExplorer[i]->Critere  + 
	  alpha * LCellulesAExplorer[i]->Variance;
      
      if (! init_eval_choix)
	{init_eval_choix = true; eval_choix = eval; i_choix = i; }

      if (eval > eval_choix)
	{eval_choix = eval ; i_choix = i;}
      if (LCellulesAExplorer[i]->Critere > CritereMax)
	CritereMax = LCellulesAExplorer[i]->Critere;
      if (LCellulesAExplorer[i]->Variance > VarianceMax)
	VarianceMax = LCellulesAExplorer[i]->Variance;
    }

  cellule = LCellulesAExplorer[i_choix];
  LCellulesAExplorer.erase(LCellulesAExplorer.begin()+i_choix);

  return cellule;		
}


// ============================================================================
// InsererCellulesMeilleures : ins�re les cellules donn�es en argument  
// dans la liste des meilleures cellules trouv�es en tenant compte 
// de la longueur maximale de cette liste, que l'on ordonne du moins bon (1er)
// au meilleur (dernier).
// La place de l'insertion est cherch�e par dichotomie.
// En cas d'�galit� de crit�re, on consid�re comme meilleure 
// la cellule consid�r�e (� ins�rer)
// ============================================================================
void cAlgorithmePartition2p::InsererCellulesMeilleures 
                                                (vector <cCellule*> l_cellules)
{
  bool ranger, enlever;
  int i_min, i_max, i_moy;
  cCellule* c;
  cCellule* cel;
 
  for (int i=0; i < (int) l_cellules.size(); i++)
    {
      c = l_cellules[i];
      // faut-il ranger la cellule, faut-il en enlever une des meilleures
      ranger = false; enlever = false;
      if ((int) LCellulesMeilleures.size() < NbMeilleures)
	{ranger = true; enlever = false;}
      else
	if  ( EstMeilleure (c, LCellulesMeilleures[0])  )
	  {ranger = true; enlever = true;}
	else
	  {ranger = false; enlever = false;}

      if (ranger)
	{
	  if (enlever)
	    {
	      cel = LCellulesMeilleures[0];
	      if (cel->AExplorer)
		cel->Meilleure = false;
	      else
		delete cel;
	      LCellulesMeilleures.erase(LCellulesMeilleures.begin());
	    }

	  // recherche de l'intervalle ou sera ins�r� la cellule
	  c->Meilleure = true;
	  i_min = 0; 
	  i_max = LCellulesMeilleures.size() -1; 
	  while ( i_max - i_min > 1 )
	    {
	      i_moy = i_min + (i_max - i_min)/2 ;
	      if ( EstMeilleure(c, LCellulesMeilleures[i_moy]) )
		i_min = i_moy + 1;
	      else
		i_max = i_moy;
            }

	  // insertion de la cellule
	  // en enlevant les cellules partitionn�e des meilleures
	  // au d�but de la 1ere partition, la liste des meilleures
	  // est vide, le 1er test a donc �t� rajout�
	  if (i_max == -1)
	     LCellulesMeilleures.push_back( c );
	  else
	  if ( EstMeilleure(c, LCellulesMeilleures[i_max]) )
	    LCellulesMeilleures.insert(LCellulesMeilleures.begin()+i_max+1, c);
	  else
	    if (i_max == i_min) // le test du meilleur est plus couteux
	      LCellulesMeilleures.insert(LCellulesMeilleures.begin()+i_min, c);
	    else
	      if ( EstMeilleure(c, LCellulesMeilleures[i_min]) )
		LCellulesMeilleures.insert(LCellulesMeilleures.begin()+i_min+1, c);
	      else
		LCellulesMeilleures.insert(LCellulesMeilleures.begin()+i_min, c);
	}
    }
}


// ============================================================================
// EnleverCelluleMeilleure : enl�ve une cellule
//                           de la liste des meilleures
// ============================================================================
void cAlgorithmePartition2p::EnleverCelluleMeilleure (cCellule* c)
{
  bool egales;
  int k;
  if (c->Meilleure)
    {
      for (unsigned int i=0; i<LCellulesMeilleures.size(); i++)
	{
	  egales = true;
	  for (k=0; k<NB_PARAMETRES; k++)
	    if (LCellulesMeilleures[i]->BorneMin[k] != c->BorneMin[k] ||
		LCellulesMeilleures[i]->BorneMax[k] != c->BorneMax[k] )
	      {
		egales = false;
		k = NB_PARAMETRES;
	  }
	  if (egales)
	    {
	      LCellulesMeilleures.erase(LCellulesMeilleures.begin()+i);
	      i = LCellulesMeilleures.size();
	    }
	}
    }
}


// ============================================================================
// EstMeilleure : renvoie true si la cellule c donn�e en 1er argument est
//                meilleure que celle C donn�e en 2�me argument
//                Si les 2 cellule ont la meme valeur de crit�re,
//                on consid�re que c est meilleure (permet de conserver la
//                derni�re trouv�e
// ============================================================================
bool cAlgorithmePartition2p::EstMeilleure (cCellule* c, cCellule* C)
{
  bool meilleure = false;
  if (Optimisateur.Optimiser == "maximiser") 
    {
      if (c->Critere >= C->Critere)
	meilleure = true;
    }
  else
    if (c->Critere <= C->Critere)
      meilleure = true;
      
  return meilleure;
}

// ============================================================================
// Partitioner2p : pour une cellule p�re donn�e en argument, renvoie la liste 
// des cellules fils (si la cellule n'est pas partitionnable, va renvoyer 
// une liste vide).
// ============================================================================
vector <cCellule*> cAlgorithmePartition2p::Partitioner2p (cCellule* cellule)
{ 
  bool boucler;
  vector <cCellule*> l_cellules;
  
if ( Partitionable (cellule) )
  {
    for (int i=0; i < NB_PARAMETRES; i++)
      if (Zpartition_faisable[i])
	{
	  Zpartition_borne_min[i] = cellule->BorneMin[i];
	  Zpartition_borne_max[i] = cellule->BorneMin[i] + 
	    (cellule->BorneMax[i] - cellule->BorneMin[i]) / 2;
	}
    else
	{
	  Zpartition_borne_min[i] = cellule->BorneMin[i];
	  Zpartition_borne_max[i] = cellule->BorneMax[i]; 
	}
       
  
    boucler = true;
    while (boucler)  
      {
	l_cellules.push_back( new cCellule(Zpartition_borne_min, 
					   Zpartition_borne_max, 
					   cellule) );
	boucler = TrouverCelluleSuivante(cellule);
      }
  }
return l_cellules;
}


// ============================================================================
// TrouverCelluleSuivante : renvoie false s'il n'y a pas de nouvelle cellule 
// possible apr�s la cellule donn�e en argument ; renvoie true si une nouvelle 
// cellule a �t� trouv�e. Les valeurs des bornes de la nouvelle cellule 
// trouv�e sont plac�es dans les attributs Zpartition_borne_min et 
// Zpartition_borne_max.
// ============================================================================
bool cAlgorithmePartition2p::TrouverCelluleSuivante (cCellule* cellule)
{ 
  bool suivante_existe = true;
  bool rechercher = true;
  int i = NB_PARAMETRES -1;

  while (rechercher)
    {if (! Zpartition_faisable[i])
      if (i == 0)
	{
	  rechercher = false;
	  suivante_existe = false; 
	}
      else
	i --;
    else
      if (Zpartition_borne_min[i] == cellule->BorneMin[i])
	{
	  Zpartition_borne_min[i] += 
	                    (cellule->BorneMax[i] - cellule->BorneMin[i]) / 2;
	  Zpartition_borne_max[i] = cellule->BorneMax[i];
	  rechercher = false;
	}
      else
	if (i == 0)
	  {
	    rechercher = false;
	    suivante_existe = false; 
	  }
	else
	  {
	    Zpartition_borne_min[i] = cellule->BorneMin[i];
	    Zpartition_borne_max[i] = cellule->BorneMin[i] +
		    (cellule->BorneMax[i] - cellule->BorneMin[i]) / 2;
	    i --; // parametre pr�c�dent
	  }
    }

  return suivante_existe;
}


// ============================================================================
// Partitionable : renvoie false si la cellule donn�e en argument 
// n'est pas partitionnable ; renvoie true, sinon
// Met � jour Zpartition_faisable[NB_PARAMETRES]
// ============================================================================
bool cAlgorithmePartition2p::Partitionable (cCellule* cellule)
{
  bool partitionable = false;
  for (int i=0; i < NB_PARAMETRES; i++)
    if ((Optimisateur.ParaType[i] != VIDE) &&
	(cellule->BorneMax[i] - cellule->BorneMin[i] >= 
	 2 * Optimisateur.ParaPas[i]))
      {
	partitionable = true;
	Zpartition_faisable[i] = true;
      }
    else
      Zpartition_faisable[i] = false;
  return partitionable;
}


// ============================================================================
// MAJNbSimulPartition :  calcule le nombre de simulation r�alis�es pour 
// r�aliser une partition dans le cas o� les fils h�ritent des points de 
// leur p�re (ce n'est pas la 1�re partition) et o� l'intervalle de chaque 
// param�tre peut �tre partitionn�. 
// ============================================================================
void cAlgorithmePartition2p::MAJNbSimulPartition (void)
{
  int nb_climats;

  // recherche du nombre de parametres utilises
  int nb_para = 0;
  for (int i=0; i < NB_PARAMETRES; i++)
    if (Optimisateur.ParaType[i] != VIDE)
      nb_para ++;

  // recherche du nombre de climats en chaque point
  if (Optimisateur.OptionClimats == REELS)
    nb_climats = Optimisateur.LClimatsReels.size();
  else
    nb_climats = Optimisateur.NbClimatsPoint;

  // calcul du nombre de simulations realisees 
  NbSimulPartition = ( (int) pow(2., nb_para) ) * 
    nb_climats * NbPtCellule;
}


// ============================================================================
// Enregistrer : sauvegarde des sorties sur fichier
// ============================================================================
void cAlgorithmePartition2p::Enregistrer(void)
{
  cCellule* c;
  int i,j;

  if (LCellulesMeilleures.size() != 0)
    {
      // sauvegarde de l'optimum (dernier de la liste des meilleurs)
      c = LCellulesMeilleures[LCellulesMeilleures.size()-1];
      for (i=0; i<NB_PARAMETRES; i++)
	ResultatOptimisateur.fOptimum << c->BorneMin[i] << "\t" << c->BorneMax[i] << "\t";
      ResultatOptimisateur.fOptimum << c->Critere << "\t" << c->Variance << endl;
      
      // sauvegarde des meilleures cellules trouv�es
      if (ResultatOptimisateur.Sauver_Critere)
	for (j=LCellulesMeilleures.size()-1; j >= 0; j--)
	  {
	    c = LCellulesMeilleures[j];
	    for (i=0; i<NB_PARAMETRES; i++)
	      ResultatOptimisateur.fCritere << c->BorneMin[i] << "\t" << c->BorneMax[i] << "\t"; 
	    ResultatOptimisateur.fCritere << c->Critere << "\t" << c->Variance << "\t" << c->Niveau << endl;
	  }
      
      // sauvegarde des niveaux parcourus
      if (ResultatOptimisateur.Sauver_Niveau)
	for(j=0; j < (int) LNiveauxExplores.size(); j++)
	  ResultatOptimisateur.fNiveau << LNiveauxExplores[j] << endl;
    }
}


