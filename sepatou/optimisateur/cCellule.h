// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cCellule.h
contenu : d�finition de la classe cCellule
******************************************************************************/
#ifndef _CCELLULE_H
#define _CCELLULE_H

#include <vector>

using namespace std;

#include "general.h"
#include "cPoint.h"

// ============================================================================
// cCellule
// ============================================================================
class cCellule
{
 public:  //-------------------------------------------------------------------
  double BorneMin[NB_PARAMETRES];
  double BorneMax[NB_PARAMETRES];
  double Critere;
  double Variance;
  int Niveau;
  cCellule* Pere;
  bool AExplorer;
  bool Meilleure;
    
  cCellule(double borne_min[NB_PARAMETRES], double borne_max[NB_PARAMETRES],
	 cCellule* pere);
  ~cCellule();
  
 private:  //------------------------------------------------------------------
  vector <cPoint*> LPoints;
};



#endif
