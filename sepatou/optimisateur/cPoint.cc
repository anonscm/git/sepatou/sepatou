// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cPoint.cc
contenu : definition des m�thodes de la classe cPoint
******************************************************************************/


#include "cPoint.h"

#include "general.h"
#include "cOptimisateur.h"

extern cOptimisateur Optimisateur;

// ============================================================================
// constructeur
// ============================================================================
cPoint::cPoint( double para[NB_PARAMETRES] )
{
  int i;
  unsigned int j;

  // mise a jour des parametres du point
  for (i=0; i < NB_PARAMETRES; i++)
    Para[i] = para[i];

  // mise � jour des param�tres de simulation avant de simuler
  for (i=0; i < NB_PARAMETRES; i++)
    Optimisateur.Para[i]=Para[i];
  Optimisateur.MAJTetas();


    for (i=0; i < Optimisateur.NbClimatsPoint; i++)
      {
	LCritere.push_back( Optimisateur.Simuler(1) );
	Optimisateur.NbSimulRealisees ++;
      }
    
  // calcul du critere moyen
  Critere = 0;
  for (j=0; j < LCritere.size(); j++)
    Critere += LCritere[j];
  Critere = Critere / LCritere.size();
}



cPoint::cPoint( double para[NB_PARAMETRES], vector <cClimat*> lclimats)
{
  int i;
  unsigned int j;
  vector <cClimat*> lclim;

  // mise a jour des parametres du point
  for (i=0; i < NB_PARAMETRES; i++)
    Para[i] = para[i];

  // mise � jour des param�tres de simulation avant de simuler
  for (i=0; i < NB_PARAMETRES; i++)
    Optimisateur.Para[i]=Para[i];
  Optimisateur.MAJTetas();


  // simulation et enregistrement du critere
  for (j=0; j < lclimats.size(); j++)
      {
	lclim.clear();
	lclim.push_back(lclimats[j]);
	LCritere.push_back( Optimisateur.Simuler(lclim) );
	Optimisateur.NbSimulRealisees ++;
      }
    
  // calcul du critere moyen
  Critere = 0;
  for (j=0; j < LCritere.size(); j++)
    Critere += LCritere[j];
  Critere = Critere / LCritere.size();
}
