// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cResultatOptimisateur.h
contenu : definition de la classe cResultatOptimisateur
******************************************************************************/
#ifndef _CRESULTAT_OPTIMISATEUR_H
#define _CRESULTAT_OPTIMISATEUR_H

#include <string>
#include <fstream>

using namespace std;


// ============================================================================
// cResultatOptimisateur : classe g�rant les resultats de l'optimisateur
// ============================================================================
class cResultatOptimisateur
{
 public:  //------------------------------------------------------------------
  ofstream fOptimum;
  ofstream fCritere;
  ofstream fParametres;
  ofstream fGradientStochastique;
  ofstream fNiveau;
  bool Sauver_Critere;
  bool Sauver_Parametres;
  bool Sauver_GradientStochastique;
  bool Sauver_Niveau;

  void OuvrirFichiersSortiesOptimisateur(string nom_algorithme, bool b_ouvrir);
  void FermerFichiersSortiesOptimisateur(string nom_algorithme, bool b_fermer);
};



#endif
