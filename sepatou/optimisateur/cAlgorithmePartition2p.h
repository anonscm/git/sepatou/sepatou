// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cAlgorithmePartition2p.h
contenu : d�finition de la classe cAlgorithmePartition2p
******************************************************************************/
#ifndef _CALGORITHME_PARTITION2P_H
#define _CALGORITHME_PARTITION2P_H

#include <vector>
#include <iostream>

using namespace std;

#include "general.h"
#include "cAlgorithme.h"
#include "cCellule.h"

// ============================================================================
// cAlgorithmePartition2p
// ============================================================================
class cAlgorithmePartition2p : public cAlgorithme
{
 public:  //------------------------------------------------------------------ 
  int NbPtCellule;

  cAlgorithmePartition2p(int nb_pt_cellule, int nb_simulations_max, int nb_meilleures);
  ~cAlgorithmePartition2p();
  void Optimiser(void);

 private:  //------------------------------------------------------------------
  int NbSimulPartition;
  vector <cCellule*> LCellulesAExplorer;  // pas d'ordre
  vector <cCellule*> LCellulesMeilleures;  // la 1ere est la plus mauvaise
  vector <int> LNiveauxExplores; // le 1er elt est le 1er niveau partitionne
  int NbSimulationsMax;
  int NbMeilleures;
  double CritereMax;
  double VarianceMax;
  double Zpartition_borne_min[NB_PARAMETRES];
  double Zpartition_borne_max[NB_PARAMETRES];
  bool Zpartition_faisable[NB_PARAMETRES];
  

  void InsererCellulesAExplorer (vector <cCellule*> l_cellules);
  cCellule* ChoisirCelluleExploree (void);
  void InsererCellulesMeilleures (vector <cCellule*> l_cellules);
  void EnleverCelluleMeilleure (cCellule* c);
  bool EstMeilleure (cCellule* c, cCellule* C);
  vector <cCellule*> Partitioner2p (cCellule* cellule);
  bool TrouverCelluleSuivante (cCellule* cellule);
  bool Partitionable (cCellule* cellule);
  void MAJNbSimulPartition (void);
  void Enregistrer(void);
}; 



#endif
