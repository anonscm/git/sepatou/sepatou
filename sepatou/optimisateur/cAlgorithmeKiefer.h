// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cAlgorithmeKiefer.h
contenu : definition de la classe cAlgorithmeKiefer
******************************************************************************/
#ifndef _CALGORITHME_KIEFER_H
#define _CALGORITHME_KIEFER_H


#include "general.h"
#include "cAlgorithme.h"

// ============================================================================
// cAlgorithmeKiefer
// ============================================================================
class cAlgorithmeKiefer : public cAlgorithme
{
 public:  //-------------------------------------------------------------------
  cAlgorithmeKiefer(double a, double b, double* c, int nb_iterations_max, double ecart_max);

  void Optimiser(void);
 
  
 private:  //------------------------------------------------------------------
  double A;
  double B;
  double C[NB_PARAMETRES];
  int NbIterationsMax;
  double EcartMax;
  int n; // nb d'it�rations r�alisees

  double ParaGradient[NB_PARAMETRES];
  double ParaNorme[NB_PARAMETRES];
  double ParaOptimum[NB_PARAMETRES];
  double Ecart[NB_ECART];
  double Critere;
  double CriterePlus[NB_PARAMETRES];
  double CritereMoins[NB_PARAMETRES];

  void Initialiser(void);
  void Enregistrer(void);
  void EvoluerParametres(void); 
  double TransformerReelNorme(double X, double m, double M);
  double TransformerNormeReel(double x, double m, double M);
  double CalculerEcartMoyen(void);
};



#endif
