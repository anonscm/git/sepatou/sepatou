// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cAlgorithmeKiefer.cc
contenu : definition des m�thodes de la classe cAlgorithmeKiefer
******************************************************************************/

#include <iostream>
#include <fstream>
#include <math.h>

#include "cAlgorithmeKiefer.h"

#include "pat.h"
#include "general.h"
#include "cOptimisateur.h"
#include "cResultatOptimisateur.h"
#include "cAnomalie.h"
#include "cRandom.h"
#include "cClimat.h"

extern cOptimisateur Optimisateur;
extern cResultatOptimisateur ResultatOptimisateur;
extern cRandom Random;


// ============================================================================
// constructeur
// ============================================================================
cAlgorithmeKiefer::cAlgorithmeKiefer(double a, double b, double* c,
				     int nb_iterations_max, double ecart_max)
{
  A = a;
  B = b;
  for (int i=0; i<NB_PARAMETRES; i++)
    C[i] = c[i];
  NbIterationsMax = nb_iterations_max;
  EcartMax = ecart_max;
}

// ============================================================================
// Optimiser
// ============================================================================
void cAlgorithmeKiefer::Optimiser (void)
{
  int i; // num�ro dans le tableau des parametres
  double cn[NB_PARAMETRES], para;
  double cn_plus, cn_moins, optimum_i;
  vector <cClimat*> lclimats;

  Optimisateur.NbSimulRealisees = 0;

  Initialiser(); 

  while (!( (NbIterationsMax != VIDE && n > NbIterationsMax) ||
	    (EcartMax != VIDE && CalculerEcartMoyen() < EcartMax) ))
    {
      // calcul du gradient pour chaque param�tre
      for (i=0; i< NB_PARAMETRES; i++)
	if (Optimisateur.ParaType[i] != VIDE)
	  {
	    cn[i] = C[i] * pow(n, -B);

	    // calcul cn_plus et cn_moins
	    if (ParaNorme[i] + cn[i] > 1)
	      cn_plus = 1 - ParaNorme[i];
	    else   
	      cn_plus =  cn[i];
	    if (ParaNorme[i] - cn[i] < 0)
	      cn_moins = ParaNorme[i];
	    else   
	      cn_moins = cn[i];

	    // mise � jour �ventuelle de lclimats
	    if (Optimisateur.OptionClimats == SEMI_ALEATOIRES)
	      {
		lclimats.clear();
		for (int k=0; k < Optimisateur.NbClimatsPoint ; k++)
		  {
		    lclimats.push_back(new cClimat);
		    lclimats[k]->Init(Optimisateur.LocalisationClimat, 
				      ANNEE_GENEREE + k);
		  }
	      }

	    para = Optimisateur.Para[i];
	    Optimisateur.Para[i] =  TransformerNormeReel(ParaNorme[i] + cn_plus,
			        Optimisateur.ParaMin[i], Optimisateur.ParaMax[i]);
	    Optimisateur.MAJTetas();
	    if (Optimisateur.OptionClimats == ALEATOIRES)
	      {
		CriterePlus[i] = Optimisateur.Simuler(Optimisateur.NbClimatsPoint);
		Optimisateur.NbSimulRealisees += Optimisateur.NbClimatsPoint;
	      }
	    else
	      if (Optimisateur.OptionClimats == SEMI_ALEATOIRES)
		{
		  CriterePlus[i] = Optimisateur.Simuler(lclimats);
		  Optimisateur.NbSimulRealisees += lclimats.size();
		}
	      else
		{
		  CriterePlus[i] = Optimisateur.Simuler(Optimisateur.LClimatsReels);
		  Optimisateur.NbSimulRealisees += Optimisateur.LClimatsReels.size();
		}
	    
	    Optimisateur.Para[i] =  TransformerNormeReel(ParaNorme[i] - cn_moins,
				Optimisateur.ParaMin[i], Optimisateur.ParaMax[i]);
	    Optimisateur.MAJTetas();
	    if (Optimisateur.OptionClimats == ALEATOIRES)
	      {
		CritereMoins[i] = Optimisateur.Simuler(Optimisateur.NbClimatsPoint);
		Optimisateur.NbSimulRealisees += Optimisateur.NbClimatsPoint;
	      }
	    else
	      if (Optimisateur.OptionClimats == SEMI_ALEATOIRES)
		{
		  CritereMoins[i] = Optimisateur.Simuler(lclimats);
		  Optimisateur.NbSimulRealisees += lclimats.size();
		}
	      else
		{
		  CritereMoins[i] = Optimisateur.Simuler(Optimisateur.LClimatsReels);
		  Optimisateur.NbSimulRealisees += Optimisateur.LClimatsReels.size();
		}
	    
	    ParaGradient[i] =  (CriterePlus[i] - CritereMoins[i])
	      / (cn_plus + cn_moins);
	    Optimisateur.Para[i] = para;

	    // destruction des climats cr��s
	    for (unsigned int k = 0; k < lclimats.size(); k++)
	      delete lclimats[k];
	  };

      Enregistrer();
      
      EvoluerParametres();	  
	  
      n++; // incr�ment du nb d'it�rations
    };

  // Enregistrement de l'optimum : derni�res valeurs avant Evoluer
  for (i=0; i<NB_PARAMETRES; i++)
    {
      optimum_i = TransformerNormeReel(ParaOptimum[i],
		   Optimisateur.ParaMin[i], Optimisateur.ParaMax[i]);
      ResultatOptimisateur.fOptimum << optimum_i << "\t";
    };
  ResultatOptimisateur.fOptimum << Critere << endl;
  
  // Ecriture du fichier Reprise 
  ofstream fReprise;
  string nom_fichier;
  
  nom_fichier = CHEMIN_REPRISE;
  fReprise.open(nom_fichier.c_str(), ios::out);
  fReprise << n << endl;
  for (i=0; i<NB_PARAMETRES; i++)
    fReprise << Optimisateur.Para[i] << "\t" << ParaGradient[i] << endl;
  fReprise.close();

  cout << "Nombre de simulations r�alis�es : " << Optimisateur.NbSimulRealisees << endl;
}
 

// ============================================================================
// Initialisation de 
//   n : num�ro d'it�ration qui va etre faite
//   Optimisateur.Para et ParaNorme : valeur des parametres
// ============================================================================
void cAlgorithmeKiefer::Initialiser(void)
{
  ifstream fReprise;
  string nom_fichier;
  int i;

  nom_fichier = CHEMIN_REPRISE;
  fReprise.open(nom_fichier.c_str(), ios::in);

  if (!fReprise)
    { // pas de reprise

      // initialisation du nombre d'it�ration
      n = 1;

      // initialisation des parametres
      for (i=0; i<NB_PARAMETRES; i++)
	if (Optimisateur.ParaType[i] != VIDE)
	  {
	    if (Optimisateur.ParaInit[i] == INIT_ALEATOIRE)
	      Optimisateur.Para[i] = Random.Generate01() * 
		(Optimisateur.ParaMax[i] - Optimisateur.ParaMin[i]) 
		+ Optimisateur.ParaMin[i];
	    else
	      Optimisateur.Para[i] = Optimisateur.ParaInit[i];
	    ParaNorme[i] = TransformerReelNorme(Optimisateur.Para[i],
						Optimisateur.ParaMin[i], 
						Optimisateur.ParaMax[i]);
      };
    }
  else
    { // reprise
      fReprise >> n;

      for (int i=0; i<NB_PARAMETRES; i++)
	{
            fReprise >> Optimisateur.Para[i];
	    ParaNorme[i] = TransformerReelNorme(Optimisateur.Para[i],
						Optimisateur.ParaMin[i], 
						Optimisateur.ParaMax[i]);
            fReprise >> ParaGradient[i];
	};
      fReprise.close();
    };
}

// ============================================================================
// Ecriture sur disque de la valeur des param�tres, du gradient, du crit�re
// et m�morisation de l'optimum
// ============================================================================
void cAlgorithmeKiefer::Enregistrer(void)
{
  int i;
  int nb_parametres_utilises;

  if (ResultatOptimisateur.Sauver_Parametres)
    {
      for (i=0; i<NB_PARAMETRES; i++)
	ResultatOptimisateur.fParametres << Optimisateur.Para[i] << "\t";
      ResultatOptimisateur.fParametres << endl;
    };
  if (ResultatOptimisateur.Sauver_GradientStochastique)
    {
      for (i=0; i<NB_PARAMETRES; i++)
	ResultatOptimisateur.fGradientStochastique << ParaGradient[i] << "\t";
      ResultatOptimisateur.fGradientStochastique << endl;
    };
  
  if (ResultatOptimisateur.Sauver_Critere)
    {
      Critere = 0;
      nb_parametres_utilises = 0;
      for (i=0; i<NB_PARAMETRES; i++)
	if (Optimisateur.ParaType[i] != VIDE)
	  {
	    nb_parametres_utilises++;
		    if (ParaGradient[i] != 0)
		      Critere += (CriterePlus[i] + CritereMoins[i]) / 2.;
		    else
		      {
			Optimisateur.MAJTetas();	      
			if (Optimisateur.OptionClimats == REELS)
			  {
			    Critere += Optimisateur.Simuler(Optimisateur.LClimatsReels);
			    Optimisateur.NbSimulRealisees += 
			      Optimisateur.LClimatsReels.size();
			  }
			else
			  Critere += Optimisateur.Simuler(Optimisateur.NbClimatsPoint);
			{
			  Optimisateur.NbSimulRealisees += Optimisateur.NbClimatsPoint;
			}
		      };
	  };
      if (nb_parametres_utilises == 0)
		throw (new cAnomalie ("Dans la methode cAlgorithmeKiefer.Optimiser, aucun parametre utilis�."));
      Critere = Critere / nb_parametres_utilises;
      ResultatOptimisateur.fCritere << Critere << endl;
    }; 
  
  // M�morisation de l'optimum : dernier jeu de parametres
  for (i=0; i<NB_PARAMETRES; i++)
    ParaOptimum[i] = ParaNorme[i];
}

// ============================================================================
// EvoluerParametres
// ============================================================================
void cAlgorithmeKiefer::EvoluerParametres(void)
{
  double norme;
  double an = A / n;

  for (int i=0; i<NB_PARAMETRES; i++)
    if (Optimisateur.ParaType[i] != VIDE)
      {
	norme = ParaNorme[i] + an * ParaGradient[i];
	if (norme > 1)
	  {
	    ParaNorme[i] = 1;
	  Optimisateur.Para[i] = Optimisateur.ParaMax[i];
	  }
	else
	  if (norme < 0)
	    {
	      	    ParaNorme[i] = 0;
	    Optimisateur.Para[i] = Optimisateur.ParaMin[i];
	    }
	  else
	    {
	      	    ParaNorme[i] = norme;
	    Optimisateur.Para[i] = TransformerNormeReel(norme,
							Optimisateur.ParaMin[i], 
							Optimisateur.ParaMax[i]);;
	    }
      };
}


// ============================================================================
// renvoie un nombre compris entre [0, 1] corespondant � X compris entre [m, M]
// ============================================================================
double cAlgorithmeKiefer::TransformerReelNorme(double X, double m, double M)
{
if (X == VIDE)
  throw (new cAnomalie ("Dans la methode cAlgorithmeKiefer.TransformerReelNorme, le 1er argument corespond � vide."));
else
  if (M - m != 0)
    return (X - m) / (M -m);
  else
    return 0; 
}


// ============================================================================
// renvoie un nombre compris entre [m, M] corespondant � x compris entre [0, 1]
// ============================================================================
double cAlgorithmeKiefer::TransformerNormeReel(double x, double m, double M)
{
if ((x > 1 || x <0) || x== VIDE)
  throw (new cAnomalie ("Dans la methode cAlgorithmeKiefer.TransformerNormeReel, le 1er argument n'est pas compris entre [0, 1]."));
else
  if (M - m != 0)
    return x * (M - m) + m;
  else   
    return m; 
}


// ============================================================================
// L'�cart du vecteur de param�tre (� une it�ration) appel� �cart global ici
// est la valeur absolue maximum du gradient de chaque param�tre. 
// Il est calcul� pour le cas courant.
// Un ecart moyen est calcul� � partir de NB_ECART �carts globaux, 
// c'est lui qui est renvoy�.
// ============================================================================
double cAlgorithmeKiefer::CalculerEcartMoyen(void)
{
  double ecart, ecart_global;
  double ecart_moyen;
  int i;

  if (n > 1)
    { // pour n = 1, on n'a pas encore calcul� les gradients
      // calcul de l'ecart (valeur absolue maximum des gradients 
      // de chaque param�tre utilis�)
      ecart_global = 0;
      for (i=0; i< NB_PARAMETRES; i++)
	if (Optimisateur.ParaType[i] != VIDE)
	  {
	    if (ParaGradient[i] < 0)
	      ecart = - ParaGradient[i];
	    else
	      ecart = ParaGradient[i];
	    if (ecart > ecart_global)
	      ecart_global = ecart;
	  };
      Ecart[n % NB_ECART] = ecart_global;
    };


  if (n < NB_ECART + 1)
      {
	return 99999999; // on s'embete pas
      }
  else
    {
      // calcul de la moyenne sur NB_ECART consid�r�s
      ecart_moyen = 0;
      for (i=0; i< NB_ECART; i++)
	ecart_moyen += Ecart[i];
      ecart_moyen = ecart_moyen / (float) NB_ECART;
      return ecart_moyen;
    };
}
