/*
** Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
** 
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software 
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

 /*-----------------------------------------------------------------
 * fichier : lex_verif.l
 * contenu : definition des regles lexicales du langage utilisateur
 *-----------------------------------------------------------------*/

%{
#include "lnu.h"
#include "yacc_verif.h" 
/* fichier g�n�r� par bison -d -v -oyacc_verif.c yacc_verif.y */


/* d�claration des variables globales */
int num_ligne = 1;   

/* declaration extern de la liste de symbole */
extern CListe<symbole> lsymbole;

/* signature des fonctions internes (declarees a la fin de ce fichier)*/
void yyerror(string s);
%}

/* definition des expressions regulieres */

lettre   [a-zA-Z]
chiffre  [0-9]
symboledeb  [!?%]
symbole  [_]
delim    [ \t]+
nombre   ([1-9]+{chiffre}*)|"0"
entier   {nombre}
reel     {chiffre}*\.{chiffre}+
id       ({symboledeb}|{lettre})({symbole}|{lettre}|{chiffre})*


%START COMMENTAIRE

/* definition des regles lexicales du langage utilisateur */
%%
"("                   { return('('); }
")"                   { return(')'); }

"{"                   { return('{'); }
"}"                   { return('}'); }

";"                   { return(';'); }
":"                   { return(':'); }
","                   { return(','); }

"="                   { return(EQ); }

"!=" |
"<>"                  { return(NEQ); }

"<"                   { return(LT); }
"<="                  { return(LEQ); }
">"                   { return(GT); }
">="                  { return(GEQ); }

"+"                   { return('+'); }
"-"                   { return('-'); }
"*"                   { return('*'); }
"/"                   { return('/'); }

"ET"                  { return ET; }
"OU"                  { return OU; }
"PARMI"               { return PARMI; }
"VIDE"                { return VIDE; }
"MODULO"              { return MODULO; }
"EXISTE"              { return EXISTE; }
"PAS"                 { return PAS; }

"SI"                  { return SI; }
"ALORS"               { return ALORS; }
"SINON"               { return SINON; }

"DEBUT"               { return DEBUT; }
"DU"                  { return DU; }
"AU"                  { return AU; }
"FAIRE"               { return FAIRE; }
"FIN"                 { return FIN; }

"INDICATEUR"          { return INDICATEUR; }
"CARACTERISTIQUE"     { return CARACTERISTIQUE; }
"FONCTION"            { return FONCTION; }
"CODE"                { return CODE; }
"ACTIVITE"            { return ACTIVITE; }
"FIN ACTIVITE"        { return FIN_ACTIVITE; }
"REGLE PLANIFICATION" { return RgPLANIF; }
"REUTILISABLE"        { return REUTILISABLE; }
"DECLENCHEUR"         { return DECLENCHEUR; }
"REGLE OPERATOIRE"    { return RgOPERA; }


"FAUX"                { return FAUX; }
"VRAI"                { return VRAI; }

{entier}              { return ENTIER; }
{reel}                { return REEL; }

"J"{entier}"_"{entier} { yylval = yytext; return NoJour; }  

\"[^\"]*              { 
                        if(yytext[yyleng-1] == '\\')
                          yymore();
                        else
                        {
                          yylval = (yytext+1);
                          yyinput();
                          return CHAINE;
                        }
                      }  

{id}                  { yylval = yytext;
                        string t=AddSym(yytext,lsymbole);
                        if (t.length() == 0)
	                  return ID;
	                else if (t[2] == I_INCONNU)
	                       return ID;
	                     else
	             	switch(t[0]) {
	             	  case T_BOOLEEN      : return ID_booleen;
	               	  case T_NOMBRE_INT   : return ID_nombre_int;
	             	  case T_ENUM         : return ID_enum;
	             	  case T_NBRE_ENUM    : return ID_nbre_enum;
	             	  case T_NOMBRE       : return ID_nombre;
	             	  case T_CHAINE       : return ID_chaine;
	             	  case T_PARCELLE     : return ID_parcelle;
	             	  case T_ENS_PARCELLE : return ID_ens_parcelle;
	             	  case T_INDICATEUR   : return ID_indicateur;
			  case T_ACTIVITE     : return ID_activite;
			  case T_ACTION       : return ID_action;
			  case T_ENS_ACTION   : return ID_ens_action;
			  case T_ENS_ACTIVITE : return ID_ens_activite;
	             	  default             : return ID; 
	             	}
	              }

{delim}               ;

"#"                          { BEGIN COMMENTAIRE; }
<COMMENTAIRE>[^#\n]*\n       { num_ligne++; BEGIN COMMENTAIRE; }
<COMMENTAIRE>[^#\n]*"#"      { BEGIN 0;}

\n                    { num_ligne++; }
 
.                     { yyerror("caractere inconnu"); }

%%
 
/* procedure C annexe */

//--------------------------------------------------------------------
void yyerror(string  s)
{
  if (yytext[0] == '\"') yytext++;
  cout << "erreur ligne " << num_ligne << " => `" 
       << yytext << "' : " << s << endl;
  exit(0);
}