// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/*-------------------------------------------------------------------------
 * fichier : CListe.cc
 * contenu : corps de la classe CListe
 *-------------------------------------------------------------------------*/

#include "CListe.h"

template <class T>
CListe<T>::CListe()
{
  pFirst = NULL;
  pParcours = NULL;
  pLast = NULL;
  NbCellule = 0;
}

template <class T>
CListe<T>::~CListe()
{
  while(pFirst)
    {
      pParcours = pFirst;
      pFirst = pFirst->Next;
      delete pParcours;
    }
}

template <class T>
void CListe<T>::Clean(int DELETE)
{
  while(pFirst)
    {
      pParcours = pFirst;
      pFirst = pFirst->Next;
      if ((DELETE) && (pParcours->Object))
	delete pParcours->Object;
      delete pParcours;
      NbCellule--;
    }
  pLast = NULL;
}

template <class T>
int CListe<T>::Add(T* object)
{
  Cellule * new_pcel = new Cellule;
  if (new_pcel != NULL)
    {
      new_pcel->Object = object;
      new_pcel->Next = pFirst;
      pFirst = new_pcel;
      if (pLast == NULL) 
	pLast = pFirst;
      
      NbCellule++;
      return 1;
    }
  // else
  return 0;
}

template <class T>
int CListe<T>::AddLast(T* object)
{
  if (pFirst == NULL)
    return Add(object);
  // else
  Cellule* new_pcel = new Cellule;
  if (new_pcel != NULL)
    {
      new_pcel->Object = object;
      new_pcel->Next = NULL;
      pLast->Next = new_pcel;
      pLast = new_pcel;
      NbCellule++;
      return 1;
    }
  // else
  return 0;
}

template <class T>
T* CListe<T>::Del(T &object)
{
  if (pFirst != NULL) {
    if (*pFirst->Object == object) {
      T* pobject = pFirst->Object;
      pParcours = pFirst->Next;
      delete pFirst;
      pFirst = pParcours;
      if (pFirst == NULL)
	pLast = pFirst;

      NbCellule--;
      return pobject;
    }
    // else
    return NULL;
  }
  // else  
  pParcours = pFirst;
  while((pParcours->Next != NULL) && (! (*pParcours->Next->Object == object)))
    pParcours = pParcours->Next;

  if (pParcours->Next == NULL)
    return NULL;
  // else
  T* pobject = pParcours->Next->Object;
  pParcours->Next = pParcours->Next->Next;
  if (pParcours->Next == NULL)
    pLast = pParcours;

  pParcours = pParcours->Next;

  NbCellule--;
  return pobject;
}

template <class T>
T* CListe<T>::Del(int i)
{
  if (i == 1) {
    if (pFirst != NULL) {
      T* pobject = pFirst->Object;
      pParcours = pFirst->Next;
      delete pFirst;
      pFirst = pParcours;
      if (pFirst == NULL)
	pLast = pFirst;

      NbCellule--;
      return pobject;
    }
    else
      return NULL;
  }
  // else  
  pParcours = pFirst;
  i--;
  while((i-- > 1)&&(pParcours->Next != NULL))
    pParcours = pParcours->Next;

  if (pParcours->Next == NULL)
    return NULL;
  // else
  T* pobject = pParcours->Next->Object;
  pParcours->Next = pParcours->Next->Next;
  if (pParcours->Next == NULL)
    pLast = pParcours;

  pParcours = pParcours->Next;

  NbCellule--;
  return pobject;
}

template <class T>
T &CListe<T>::operator [](int i)
{
  pParcours = pFirst;
  while((i-- > 1)&&(pParcours != NULL))
    pParcours = pParcours->Next;

  if (pParcours != NULL)
    return *pParcours->Object;
  // else
  throw Erreur();
}

template <class T>
T* CListe<T>::Current()
{
  if (pParcours != NULL)
    return pParcours->Object;
  // else
  return NULL;
}

template <class T>
void CListe<T>::Restart()
{
  pParcours = pFirst;
}

template <class T>
T &CListe<T>::operator ++(int)
{
  if (pParcours != NULL) {
    T* pobject = pParcours->Object;
    pParcours = pParcours->Next;
    return *pobject;
  }
  // else
  throw Erreur();
}

template <class T>
T &CListe<T>::operator ++()
{
  if ((pParcours != NULL) && (pParcours->Next != NULL)) {
    pParcours = pParcours->Next;
    return *pParcours->Object;
  }
  // else
  throw Erreur();
}

template <class T>
CListe<T>::operator int()
{
  if (pParcours == NULL)
    return 0;
  // else
  return 1;
}  

template <class T>
int CListe<T>::LgListe()
{
  return NbCellule;
}


