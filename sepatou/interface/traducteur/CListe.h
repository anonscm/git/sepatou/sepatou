// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/*-------------------------------------------------------------------------
 * fichier : CListe.h
 * contenu : definition de la classe CListe
 *           La classe Cliste represente une liste de pointeur sur des objets
 *           de type T.
 *           L'operateur == doit etre d�fini pour le type T.
 *-------------------------------------------------------------------------*/

#ifndef CLISTE_H
#define CLISTE_H 1


#include <stdio.h>


template <class T> 
class CListe
{
  public :
    class Erreur {};
  
  CListe();  // creation d'une liste vide : pFirst = pParcours = pLast = NULL 
  ~CListe(); // destruction de la liste : ne detruit pas les objets de la liste 
  
  void Clean(int DELETE = 0); // vide la liste : si DELETE = 1 
                              // alors destruction des objets 
  int Add(T* object); /* ajout d'un objet en debut de liste : pParcours = pFirst
			 retourne 0 si echec, 1 si reussite */
  int AddLast(T* object); /* ajout d'un objet en fin de liste : pParcours = pLast
			     retourne 0 si echec, 1 si reussite */
  T *Del(T &object); /* suppression de l'objet == `object' de la liste :
			pParcours = Next de l'objet supprimer 
			(NUll si dernier objet supprimer),
			retourne un pointeur sur l'objet supprimer
			ou NULL si pas de suppression */
  T *Del(int i); /* suppression de l'objet d'indice `i' de la liste : 
                    pParcours = Next 
		    de l'objet supprimer (NUll si dernier objet supprimer),
		    retourne un pointeur sur l'objet supprimer
		    ou NULL si pas de suppression */
  T &operator [](int i); /* retourne un pointeur sur l'objet d'indice = i
			    ou NULL si indice > pLast
			    indice de pFirst = 1,
			    indice de pLast = NbCellule */
  T *Current(); /* retourne l'objet point� par pParcours */
  void Restart();  /* place le pointeur de parcours au debut de la liste,
		      pParcours = pFirst */
  T &operator ++(int);  /* retourne un pointeur sur l'objet point� par pParcours et
			   place pParcours sur l'objet suivant 
                           (post-incr�mentation) */
  T &operator ++(); /* place pParcours sur l'objet suivant et retourne un pointeur
		       sur cet objet, (pr�-incr�mentation) */
  operator int(); /* retourne 1 si pParcours pointe un objet,
		     0 sinon (pParcours en fin de liste ou liste vide) */
  int LgListe();  /* retourne la longeur de la liste (le nombre de cellule) */
  
  private :
    struct Cellule
    { public :
      T* Object;
      Cellule* Next;
    };
  Cellule* pFirst;    /* pointeur sur la premiere cellule de la liste */
  Cellule* pParcours; /* pointeur sur la cellule courante de la liste */
  Cellule* pLast;     /* pointeur sur la derniere cellule de la liste */
  int      NbCellule;
  
};

#endif

