// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/*-------------------------------------------------------------------------
 * fichier : Templates.cc
 * contenu : instanciation des classes d�riv�es des classes g�n�riques
 *           !!! ce fichier est n�cessaire pour une compilation avec g++
 *           !!! ce fichier ne passe pas en compilation avec CC
 *-------------------------------------------------------------------------*/


 
#include "CListe.cc"
#include "lnu.h"

int operator ==(symbole s1,symbole s2)
{
  return (s1.nom == s2.nom && s1.type == s2.type);
}

int operator ==(gp_enum gp1,gp_enum gp2)
{
  return (gp1.nom_gp_enum == gp2.nom_gp_enum);
}

// D�clarations des classes servant de param�tres aux classes d�riv�es

// Instanciation des classes d�riv�es
template class CListe<symbole>;
template class CListe<gp_enum>;

