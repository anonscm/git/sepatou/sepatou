// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/*-----------------------------------------------------------------
 * fichier : lnu.h
 * contenu : definition des structures de donnees du verificateur
 *           de strategie ecrite dans le langage utilisateur lnu
 *           pour en verifier la syntaxe et la traduire en C++
 *-----------------------------------------------------------------*/

#ifndef LNU_H
#define LNU_H 1 

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include "CListe.h"

using namespace std;
 
/* denomination des differents types de valeurs */
#define T_BOOLEEN                'a'
#define T_ENUM                   'b'
#define T_NOMBRE_INT             'c'
#define T_NOMBRE                 'd'
#define T_CHAINE                 'e'
#define T_PARCELLE               'f'
#define T_ENS_PARCELLE           'g'
#define T_INDICATEUR             'h'
#define T_ACTIVITE               'i'
#define T_ENS_ACTIVITE           'm'
#define T_ACTION                 'j'
#define T_ENS_ACTION             'k'
#define T_REGLE                  'n'
#define T_NBRE_ENUM              'x'
#define T_INCONNU                '?'

/* denomination des differents types d'identificateur */
#define I_VARIABLE               'a'
#define I_FONCTION               'b'
#define I_CONSTANTE              'c'
#define I_EXPRESSION             'd'
#define I_INDICATEUR             'e'
#define I_INCONNU                '?'

/* denomibation des differentes classes de symboles */
#define C_PLAN_CONCENTREMAIS     'a'
#define C_PLAN_PATURAGE          'b'
#define C_PLAN_FAUCHE            'c'
#define C_PLAN_COORDINATION      'd'
#define C_PLAN_AZOTE             'e'
#define C_SDP                    'f'
#define C_SDO                    'g'
#define C_SIO                    'h'
#define C_SIS                    'i'
#define C_GLOBAL                 'j'
#define C_ENS_PARCELLE           'k'
#define C_TEMPS                  'l'
#define C_FOI                    'm'
#define C_VARU_SDP               'n'
#define C_VARU_SDO               'o'
#define C_INCONNU                '?'

/* denomination des differents types de parametres */
#define P_PAR_DEFAUT             'a'
#define P_OBLIGATOIRE            'b'
#define P_INCONNU                '?'

/* Traduction des types de valeurs */
#define TRADU_T_BOOLEEN          "bool"
#define TRADU_T_NOMBRE_INT       "int"
#define TRADU_T_NOMBRE           "float"
#define TRADU_T_CHAINE           "string"
#define TRADU_T_PARCELLE         "cParcelle*"
#define TRADU_T_ENS_PARCELLE     "cEnsembleParcelles"

/* definition des noms des fichiers */
#define FICH_INDIC               "Indicateurs.cc"
#define FICH_FO_INTER            "FoInterpretation.cc"
#define FICH_RG_PLANI            "ReglesPlanification.cc"
#define FICH_CREER_RG_PLANI      "CreerRgPlani.txt"
#define FICH_RG_OPERA            "ReglesOperatoires.cc"
#define FICH_CREER_RG_OPERA      "CreerRgOpera.txt"
#define FICH_STRAT_H             "Strategie.h"
#define FICH_TMP                 "FichTmp.tmp"
#define FICH_ENTETE              "entete_cc.txt"
#define FICH_ENTETE_INDIC        "suite_entete_indicateurs.txt"
#define LIB_IN "symbole.dic" 
/* nom du fichier texte de la librairie */
#define DICO_ENUM_IN "enum.dic" 
/* nom du fichier du dictionnaire de groupe enumere */
#define LIB_OUT "symbole_out.dic" 
/* nom du fichier texte de la librairie */


#define YYSTYPE string 
/* redefinition du type des unites lexicales et des symboles non-terminaux */

#define NBVALPOSSIBLE 10   
/* nombre max de valeurs possibles pour les types T_ENUM et T_NBR_ENUM */


// DEFINITION DE STRUCTURES
typedef struct {
  string nom_val; /* nom de la valeur possible */
  string tradu_val; /* traduction de la valeur possible */
} val_possible; /* definition du type valeur possible */

typedef struct {
  string nom_gp_enum; /* nom du groupe enumere */
  val_possible gp_val_possible[NBVALPOSSIBLE]; /* tableau d'enumeration des valeurs possibles */
} gp_enum; /* definition du type groupe enumere */

typedef struct {
  string nom; /* nom du symbole */
  string type; /* type du symbole (5 caracteres) : 12345
		   1 -> type de la valeur retournee ou de la variable (b,e,r,c,p,P)
                   2 -> lieu de declaration (n,c,m,a,p,f,e,G,?,
		   3 -> genre du symbole : 'V'ariable, 'F'onction, 'I'ndicateur
		   4 -> type du 1er argument (b,e,r,c,p,P)
		   5 -> type du 2ieme argument (b,e,r,c,p,P) */
  string tradu; /* traduction du symbole */
  gp_enum* groupe_enum; /* groupe enumere si type T_ENUM ou T_NBRE_ENUM  */
} symbole; /* definition du type symbole : nom + type */


// SIGNATURES DE FONCTIONS
string itoa(int i); 
/* convertie un entier en string */

string Type(string s, CListe<symbole> &ls); 
/* retourne le type du symbole s */

gp_enum* GroupeEnum(string nom, CListe<symbole> &ls); 
/* retourne le groupe enum du symbole nom */

symbole* Symbole(string nom, CListe<symbole> &ls); 
/* retourne le symbole de nom nom */

int VerifGpEnum(string nom, string chaine, CListe<symbole> &ls); 
/* verifie que la chaine est bien une valeur possible du symbole nom */

string DelSym(string nom, CListe<symbole> &ls); 
/* efface le symbole nom de la liste de symbole ls */

string AddSym(string nom, CListe<symbole> &ls); 
/* ajoute le symbole nom de la liste de symbole ls */

string MaJType(CListe<symbole> &ls, string nom, string tradu, char type, 
		char classe, char t_ind, string arg = ""); 
/* met a jour le symbole de nom ds la liste ls */

string TraduType(string type); 
/* retourne la traduction du type d'une valeur */

string TraduType1Lettre(string type); 
/* retourne la 1er lettre de la traduction du type d'une valeur */

string TraduNom(string nom, CListe<symbole> &ls); 
/* retourne le champ tradu du symbole de nom nom de la list ls */

string TraduClasse(string type); 
/* retourne la traduction de la classe du type d'un symbole */

string FaireNomFO(string nom); 
/* retourne un nom de fonction d'interpretation */

string FaireEnumFO(string nom, string type); 
/* retourne un enum de fonction d'interpretation */

string FaireNomI(string nom); 
/* retourne un nom d'indicateur */

string FaireEnumI(string nom); 
/* retourne un enum d'indicateur */

string FaireNomRP(string nom); 
/* retourne un nom de regle de planification */

string FaireNomRO(string nom); 
/* retourne un nom de regle d'operatoire */

string FaireEnumVSDP(string nom); 
/* retourne un enum de variable utilisateur de planification */

string FaireEnumVSDO(string nom); 
/* retourne un enum de variable utilisateur operatoire */

string FaireNomVFOI(string nom); 
/* retourne un nom de variable utilisateur de fonction d'interpretation */


string avant (string s, string motif);
// retourne la string avant motif

string apres (string s, string motif);
// retourne la string apres motif

     
#endif

