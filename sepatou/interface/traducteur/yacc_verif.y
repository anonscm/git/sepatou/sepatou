/*
** Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
** 
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software 
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*-----------------------------------------------------------------
 * fichier : yacc_verif.y
 * contenu : definition des regles syntaxique du langage utilisateur
 *           pour en verifier la syntaxe 
 *-----------------------------------------------------------------*/

%{
#include <fstream>
#include <string>
#include <stdlib.h>
#include <math.h>
using namespace std;
#include "lnu.h"
#include "librairie.cc"

/* declaration des fonctions et des variables externes */
extern void yyerror(string s); /* fo de message d'erreur de lex_verif.l */
extern int yylex(); /* fonction d'appel de l'analyseur lexical */
extern FILE* yyin; /* fichier d'entrer de l'analyseur lexical */
extern FILE* yyout; /* fichier de sortie de l'analyseur lexical */

string VerifFonction(string fonction, string arg); 
/* verifie que fonction(arg) est du type I_FONCTION et 
   que les arguments arg sont du bon type */

string VerifVarConst(string var_const); 
/* verifie que var_const est du type I_VARIABLE ou I_CONSTANTE */

string VerifVariable(string variable); 
/* verifie que variable est du type I_VARIABLE */

string msg; /* message d'erreur */

string type; /* type retourne */

string ListeVar; /* liste de variable locale a une fonction */

CListe<symbole> lsymbole; /* declaration de la liste de symbole */

CListe<gp_enum> lgp_enum; /* declaration de la liste de groupe enumere */
%}

/* declaration des unites lexicales */ 
%start strategie
%token ET OU 
%token EQ NEQ LT LEQ GT GEQ VIDE EXISTE PARMI PAS
%token SI ALORS SINON
%token DEBUT DU AU FAIRE FIN
%token INDICATEUR CARACTERISTIQUE FONCTION CODE 
%token ACTIVITE FIN_ACTIVITE RgPLANIF REUTILISABLE DECLENCHEUR RgOPERA
%token VRAI FAUX
%token ENTIER
%token REEL
%token CHAINE
%token  NoJour
%token ID              /* identificateur */
%token ID_booleen      /* identificateur de type booleen */
%token ID_enum         /* identificateur de type enum */
%token ID_nbre_enum    /* identificateur de type nombre enum */
%token ID_nombre_int   /* identificateur de type entier */
%token ID_nombre       /* identificateur de type nombre */
%token ID_chaine       /* identificateur de type chaine */
%token ID_parcelle     /* identificateur de type parcelle */
%token ID_ens_parcelle /* identificateur de type ensemble de parcelles */
%token ID_indicateur   /* identificateur de type indicateur */
%token ID_activite     /* identificateur de type activite */
%token ID_action       /* identificateur de type action */
%token ID_ens_action   /* identificateur de type ensemble d'actions */
%token ID_ens_activite /* identificateur de type ensemble de plans */

/* declaration des priorites (par ordre croissant */
%right AFFECT /* operateur d'affectation, association a droite */
%left OU ET   /* association a gauche */
%left EQ NEQ LT LEQ GT GEQ  /* association a gauche */
%left MODULO  /* association a gauche */
%left '+' '-'  /* association a gauche */
%left '*' '/'  /* association a gauche */
%right PAS /* operateur unaire booleen, association a droite */
%right UMINUS  /* operateur unaire -, association a droite */

/* definitions des regles syntaxiques */
%% 
strategie :
     | strategie declaration_1indicateur 
     | strategie declaration_1foi
     | strategie declaration_1rg_planif
     | strategie declaration_rg_opera
     | strategie error
       { yyclearin;
         yyerrok;
       } 
;

/* declaration d'un indicateur */
declaration_1indicateur :  INDICATEUR ':' ID CARACTERISTIQUE ':' expr_booleen
       { string vide; vide = "";
	 if ($3[0] != '!') {
	   msg = vide + "Le nom d'un indicateur `" + $3 
	         + "' doit commencer par un !";
	   yyerror(msg);
         }
	 MaJType(lsymbole, $3, FaireEnumI($3),T_INDICATEUR, C_SIO, I_INDICATEUR);
         cout << "Indicateur " << $3 << " : verification OK" << endl;
       }
;

un_nbre_enum : ID_nbre_enum   { VerifVarConst($1); }
     | ID_nbre_enum arguments { VerifFonction($1,$2); }
;

un_enum : ID_enum        { VerifVarConst($1); }
     | ID_enum arguments { VerifFonction($1,$2); }
;

un_nombre : REEL { $$ = T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_CONSTANTE; }
     |  ENTIER { $$ = T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; } 
     | NoJour { $$ = T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_CONSTANTE; }
     | ID_nombre_int arguments { $$ = VerifFonction($1,$2); $$[0] = T_NOMBRE;}
     | ID_nombre_int           { $$ = VerifVarConst($1); $$[0] = T_NOMBRE;}
     | ID_nombre arguments { $$ = VerifFonction($1,$2); } 
     | ID_nombre           { $$ = VerifVarConst($1); }
     | ID { MaJType(lsymbole, $1, $1,T_NOMBRE, C_GLOBAL, I_INCONNU);}
     | ID_indicateur { $$ = T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_INDICATEUR; }
     | un_nbre_enum { $$ = Type($1,lsymbole); } 
;

un_booleen : VRAI { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_CONSTANTE; }
     | FAUX       { $$ = T_BOOLEEN; $$ = $$ +C_GLOBAL; $$ = $$ + I_CONSTANTE; }
     | ID_booleen arguments { $$ = VerifFonction($1,$2); }
     | ID_booleen           { $$ = VerifVarConst($1); }
;

booleen_booleen : un_booleen
     | un_booleen EQ un_booleen
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | un_booleen NEQ un_booleen
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
;

booleen_nombre : expr_nombre EQ expr_nombre
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre NEQ expr_nombre
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre LT expr_nombre
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre LEQ expr_nombre
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre GT expr_nombre
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre GEQ expr_nombre
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
;

booleen_chaine : expr_chaine EQ expr_chaine
       { $$ =  T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_chaine NEQ expr_chaine
       { $$ =  T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
;

booleen_parcelle : expr_parcelle EQ expr_parcelle
       { $$ =  T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_parcelle NEQ expr_parcelle
       { $$ =  T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_parcelle PARMI expr_ens_parcelle
       { $$ =  T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
;

booleen_ens_parcelle : expr_ens_parcelle EQ expr_ens_parcelle
       { $$ =  T_BOOLEEN; $$ =  $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_ens_parcelle NEQ expr_ens_parcelle
       { $$ =  T_BOOLEEN; $$ =  $$ + C_GLOBAL; $$ =  $$ + I_EXPRESSION; }
     | EXISTE un_ens_parcelle
       { $$ =  T_BOOLEEN; $$ =  $$ + C_GLOBAL; $$ =  $$ + I_EXPRESSION; }
;

booleen_enum : un_enum EQ un_enum 
       { if (GroupeEnum($1,lsymbole) != GroupeEnum($3,lsymbole)) {
	 string vide; vide = "";
	 msg = vide + "Les enums compares sont de groupe different : "
               + GroupeEnum($1,lsymbole)->nom_gp_enum+" <> "
               + GroupeEnum($3,lsymbole)->nom_gp_enum;
	 yyerror(msg);
         }
       }
     | un_enum NEQ un_enum 
       { if (GroupeEnum($1,lsymbole) != GroupeEnum($3,lsymbole)) {
	 string vide; vide = "";
	 msg = vide + "Les enums compares sont de groupe different : "
               + GroupeEnum($1,lsymbole)->nom_gp_enum
               + " <> " + GroupeEnum($3,lsymbole)->nom_gp_enum;
	 yyerror(msg);
         }
       }
     | un_enum EQ CHAINE
       { if (!VerifGpEnum($1,$3,lsymbole)) {
	 string vide; vide = "";
	 msg = vide + $3 + " n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($1,lsymbole)->nom_gp_enum;
	 yyerror(msg);
         }
       }
     | un_enum NEQ CHAINE
       { if (!VerifGpEnum($1,$3,lsymbole)) {
	 string vide; vide = "";
	 msg = vide + $3 + " n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($1,lsymbole)->nom_gp_enum;
	 yyerror(msg);
         }
       }
     | CHAINE EQ un_enum
       { if (!VerifGpEnum($3,$1,lsymbole)) {
	 string vide; vide = "";
	 msg = vide + $1 + " n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($3,lsymbole)->nom_gp_enum;
	 yyerror(msg);
         }
       }
     | CHAINE NEQ un_enum
       { if (!VerifGpEnum($3,$1,lsymbole)) {
	 string vide; vide = "";
	 msg =  vide + $1 + " n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($3,lsymbole)->nom_gp_enum;
	 yyerror(msg);
         }
       }
;

booleen_nbre_enum : un_nbre_enum EQ expr_nombre 
     | un_nbre_enum NEQ expr_nombre 
     | un_nbre_enum EQ CHAINE
       { if (!VerifGpEnum($1,$3,lsymbole)) {
	 string vide; vide = "";
	 msg = vide + $3 + " n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($1,lsymbole)->nom_gp_enum;
	 yyerror(msg);
         }
       }
     | un_nbre_enum NEQ CHAINE
       { if (!VerifGpEnum($1,$3,lsymbole)) {
	 string vide; vide = "";
	   msg = vide + $3 + " n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($1,lsymbole)->nom_gp_enum;
	   yyerror(msg);
         }
       }
     | CHAINE EQ un_nbre_enum
       { if (!VerifGpEnum($3,$1,lsymbole)) {
	 string vide; vide = "";
	 msg = vide + $1 + " n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($3,lsymbole)->nom_gp_enum;
	 yyerror(msg);
         }
       }
     | CHAINE NEQ un_nbre_enum
       { if (!VerifGpEnum($3,$1,lsymbole)) {
	 string vide; vide = "";
	 msg = vide + $1 + "n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($3,lsymbole)->nom_gp_enum;
	 yyerror(msg);
         }
       }
;

expr_booleen : booleen_booleen
     | booleen_nombre
     | booleen_chaine
     | booleen_parcelle
     | booleen_ens_parcelle
     | booleen_enum
     | booleen_nbre_enum
     | '(' expr_booleen ')'
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_booleen ET expr_booleen
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_booleen OU expr_booleen
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ =  $$ + I_EXPRESSION; }
     | PAS expr_booleen
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ =  $$ + I_EXPRESSION; }
;

expr_nombre : un_nombre
     | '(' expr_nombre ')'
       { $$ =  T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre '+' expr_nombre
       { $$ =  T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre '-' expr_nombre
       { $$ =  T_NOMBRE; $$ =  $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre '*' expr_nombre
       { $$ =  T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre '/' expr_nombre
       { $$ =  T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_nombre MODULO expr_nombre
       { $$ =  T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; } 
     | '-' expr_nombre %prec UMINUS
       { $$ =  T_NOMBRE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
;

expr_chaine : CHAINE
       { $$ =  T_CHAINE; $$ = $$ + C_GLOBAL; $$ = $$ + I_CONSTANTE; }
     | ID_chaine arguments { $$ = VerifFonction($1,$2); }
     | ID_chaine           { $$ = VerifVarConst($1); }
;

une_parcelle : ID_parcelle arguments { $$ = VerifFonction($1,$2); }
     | ID_parcelle                   { $$ = VerifVarConst($1); }
;

expr_parcelle : une_parcelle 
       { $$ = T_PARCELLE; $$ =  $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
;

un_ens_parcelle : ID_ens_parcelle arguments { $$ = VerifFonction($1,$2); }
     | ID_ens_parcelle                      { $$ = VerifVarConst($1); }
     | VIDE { $$ =  T_ENS_PARCELLE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
;

expr_ens_parcelle : un_ens_parcelle
       { $$ =  T_ENS_PARCELLE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | '(' expr_ens_parcelle ')'
       { $$ =  T_ENS_PARCELLE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_ens_parcelle '+' expr_ens_parcelle
       { $$ =  T_ENS_PARCELLE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_ens_parcelle '-' expr_ens_parcelle
       { $$ =  T_ENS_PARCELLE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_ens_parcelle '+' expr_parcelle
       { $$ =  T_ENS_PARCELLE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_ens_parcelle '-' expr_parcelle
       { $$ =  T_ENS_PARCELLE; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
; 
 
une_activite : ID_activite
;

une_action : ID_action
;

arguments : '(' ')'            { $$ = ""; }
     | '(' suite_expr_type ')' { $$ = $2; }
;

suite_expr_type : expr_type { $$ = $1[0]; }
     | suite_expr_type ',' expr_type 
       { string vide; vide = ""; $$ = vide + $1 + $3[0]; }
;

expr_type : un_booleen
     | expr_nombre
     | expr_chaine
     | expr_parcelle
     | expr_ens_parcelle
;

/* declaration d'une fonction d'interpretation */ 
declaration_1foi : FONCTION ':' ID CODE ':' du_code_return
       { string temp;
         string tradu = TraduType1Lettre($6) + "ValeurFo(" 
                        + FaireEnumFO($3,$6) + ")";
         $$ =  MaJType(lsymbole, $3, tradu,$6[0], C_FOI, I_CONSTANTE);
	 temp = ListeVar;
	 while (temp != "") {
	   DelSym(avant(temp,","),lsymbole);
	   temp = apres(temp,",");
	 }
	 ListeVar = "";
	 cout << "Fonction " << $3 << " de type " << ecrire_type($$[0])
              << " : verification OK" << endl;
       }
;

du_code_return : un_code_return
     | '{' un_code_return '}' { $$ = $2; }
     | '{' suite_code un_code_return '}' { $$ = $3; }
;

un_code_return : expr_type
     | SI expr_booleen ALORS du_code_return SINON du_code_return
       { if ($4[0] != $6[0]) { 
	     string vide; vide = "";
	     msg = vide + "Les valeurs retournees sont de type different : " 
                   + $4 + "<>" + $6;
	     yyerror(msg);
             }
         $$ = $4;  
       } 
;

du_code : un_code
     | '{' suite_code '}'
;

suite_code : un_code
     | suite_code un_code
;

un_code : SI expr_booleen ALORS du_code
     | SI expr_booleen ALORS du_code SINON du_code
     | declaration_var_local 
     | affectation_var_local
;

/* affectation d'une variable locale */
affectation_var_local : affectation
       { if ($1[1] != C_FOI)  {
	   msg = "La variable affect�e est invisible dans une FoI";
	   yyerror(msg);
           }
       }
;

/* declaration d'une variable locale */
declaration_var_local : ID EQ un_booleen
       { $$ = MaJType(lsymbole, $1, FaireNomVFOI($1), $3[0], C_FOI, I_VARIABLE);
         ListeVar = ListeVar + $1 + ","; }
     |  ID EQ expr_nombre
       { $$ = MaJType(lsymbole, $1, FaireNomVFOI($1), $3[0], C_FOI, I_VARIABLE);
         ListeVar = ListeVar + $1 + ","; }
     |  ID EQ expr_chaine
       { $$ = MaJType(lsymbole, $1, FaireNomVFOI($1), $3[0], C_FOI, I_VARIABLE);
         ListeVar = ListeVar + $1 + ","; }
     |  ID EQ expr_parcelle
       { $$ = MaJType(lsymbole, $1, FaireNomVFOI($1), $3[0], C_FOI, I_VARIABLE);
         ListeVar = ListeVar + $1 + ","; }
     |  ID EQ expr_ens_parcelle
       { $$ = MaJType(lsymbole, $1, FaireNomVFOI($1), $3[0], C_FOI, I_VARIABLE);
         ListeVar = ListeVar + $1 + ","; }
;

/* affectation d'une variable */
affectation : ID_booleen EQ un_booleen %prec AFFECT { $$ = VerifVariable($1); }
     | ID_nombre EQ expr_nombre %prec AFFECT     { $$ = VerifVariable($1); }
     | ID_chaine EQ expr_chaine      %prec AFFECT     { $$ = VerifVariable($1); }
     | ID_parcelle EQ expr_parcelle  %prec AFFECT     { $$ = VerifVariable($1); }
     | ID_ens_parcelle EQ expr_ens_parcelle %prec AFFECT 
       { $$ = VerifVariable($1); }
     | ID_ens_activite EQ '{' une_activite ',' une_activite ',' une_activite ',' une_activite ',' une_activite '}' %prec AFFECT
       { $$ = VerifVariable($1); }
     | ID_ens_action EQ '{' une_action ',' une_action ',' une_action ',' une_action ',' une_action ',' une_action ',' une_action '}' %prec AFFECT
       { $$ = VerifVariable($1); }
     | ID_enum EQ CHAINE %prec AFFECT
       { $$ = VerifVariable($1);
         if (!VerifGpEnum($1,$3,lsymbole)) {
            string vide; vide = "";
	    msg = vide + $3 + " n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($1,lsymbole)->nom_gp_enum;
	    yyerror(msg);
            }
       }
     | ID_nbre_enum EQ CHAINE %prec AFFECT
       { $$ = VerifVariable($1);
         if (!VerifGpEnum($1,$3,lsymbole)) {
            string vide; vide = "";
	    msg =  vide + $3 + " n'appartient pas aux valeurs possibles du groupe enumere " + GroupeEnum($1,lsymbole)->nom_gp_enum;
	    yyerror(msg);
            }
       }
     | ID_nbre_enum EQ expr_nombre %prec AFFECT
       { $$ = VerifVariable($1); }
;

/* declaraction d'une regle de planification */
declaration_1rg_planif : RgPLANIF reutilisation ':' ID declencheurs corps_rg_planif
       { MaJType(lsymbole, $4, FaireNomRP($4),T_REGLE, C_SDP, I_CONSTANTE);
         cout << "Regle de planification " << $4 << " : verification OK" << endl; }
;

reutilisation : /* empty */
     | REUTILISABLE
;

declencheurs : DECLENCHEUR ':' expr_booleen_indicateur
     | des_declencheurs DECLENCHEUR ':' expr_booleen_indicateur
;

des_declencheurs : DECLENCHEUR ':' expr_booleen_indicateur
     | des_declencheurs DECLENCHEUR ':' expr_booleen_indicateur
;

expr_booleen_indicateur : ID_indicateur
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | '(' expr_booleen_indicateur ')'
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_booleen_indicateur ET expr_booleen_indicateur
       { $$ = T_BOOLEEN; $$ =  $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
     | expr_booleen_indicateur OU expr_booleen_indicateur
       { $$ = T_BOOLEEN; $$ = $$ + C_GLOBAL; $$ = $$ + I_EXPRESSION; }
;

corps_rg_planif : un_code_rg_planif
     | '{' suite_code_rg_planif '}'
;

suite_code_rg_planif : un_code_rg_planif
     | suite_code_rg_planif un_code_rg_planif
;

un_code_rg_planif : directive
     | SI expr_booleen ALORS corps_rg_planif
     | SI expr_booleen ALORS corps_rg_planif SINON corps_rg_planif
     | declaration_var_deci 
     | affectation_var_deci
;

directive : un_debut un_du un_au un_faire
     | un_debut un_du un_au un_faire un_fin
     | un_du un_au un_faire un_fin
     | un_du un_au un_faire
;

un_debut : DEBUT du_code_directive
;

un_du : DU expr_nombre
;

un_au : AU expr_nombre
;

un_faire : FAIRE du_code_faire
;

un_fin : FIN du_code_directive
;

du_code_directive : un_code_directive
     | '{' suite_code_directive '}'
;

suite_code_directive : un_code_directive
     | un_code_directive suite_code_directive
;

un_code_directive : SI expr_booleen ALORS du_code_directive
     | SI expr_booleen ALORS du_code_directive SINON du_code_directive
     | declaration_var_deci 
     | affectation_var_deci 
;

affectation_var_deci : affectation
       { if (($1[1] != C_SDP) &&
	     ($1[1] != C_VARU_SDP) &&
	     ($1[1] != C_VARU_SDO) &&
	     ($1[1] != C_PLAN_CONCENTREMAIS) &&
	     ($1[1] != C_PLAN_PATURAGE) &&
	     ($1[1] != C_PLAN_FAUCHE) &&
	     ($1[1] != C_PLAN_AZOTE) &&
	     ($1[1] != C_PLAN_COORDINATION)) {
	   msg = "La variable affect�e est invisible dans une RgPlanif";
	   yyerror(msg);
         }
       }
;

declaration_var_deci : ID EQ expr_type
       { string vide; vide = "";
	 $$ = MaJType(lsymbole, $1, vide + TraduType1Lettre($3) + "Variable[" 
              + FaireEnumVSDP($1) +"]",$3[0], C_VARU_SDP, I_VARIABLE);
         if ($1[0] != '%') {
	   msg = vide + "La variable de d�cision utilisateur " + $1 
                 + " doit commencer par un %";
	   yyerror(msg);
         }
       }
;

du_code_faire : affectation_var_deci_planif
     | '{' suite_code_faire '}' { $$ = $2; }
;

suite_code_faire : affectation_var_deci_planif
     | suite_code_faire affectation_var_deci_planif
       { if ($1[1] != $2[1]) {
           string vide; vide = "";
	   msg = vide + "directive, variables de plans differentes : "
	     + $1[1] + " <> " + $2[1];
	   yyerror(msg);
           }
         $$ = $1;
       }
;

affectation_var_deci_planif : affectation
       { if (($1[1] != C_PLAN_CONCENTREMAIS) &&
	     ($1[1] != C_PLAN_PATURAGE) &&
	     ($1[1] != C_PLAN_FAUCHE) &&
	     ($1[1] != C_PLAN_AZOTE) &&
	     ($1[1] != C_PLAN_COORDINATION)) {
	   msg = "La variable affect�e est invisible dans une directive";
	   yyerror(msg);
         } ;
       }
;

/* declaration des regles operatoires d'une activite */
declaration_rg_opera : ACTIVITE ':' ID_activite suite_rg_opera FIN_ACTIVITE
       { cout << "Activite " << $3 << " : verification OK" << endl; }
;

suite_rg_opera : une_rg_opera
     | suite_rg_opera une_rg_opera
;

/* declaration d'une regle operatoire */
une_rg_opera : RgOPERA ':' ID corps_rg_opera
       { MaJType(lsymbole, $3, FaireNomRO($3),T_REGLE, C_SDO, I_CONSTANTE);
         cout << "Regle operatoire " << $3 << " : verification OK" << endl; }
;

corps_rg_opera : SI expr_booleen ALORS du_code_opera
     | SI expr_booleen ALORS du_code_opera SINON du_code_opera
     | affectation_opera
     | declaration_opera 
     | '{' corps_rg_opera '}'
     | '{' suite_code_opera un_code_opera '}'
;

du_code_opera : un_code_opera
     | '{' suite_code_opera '}'
;

suite_code_opera : un_code_opera
     | suite_code_opera un_code_opera
;

un_code_opera : SI expr_booleen ALORS du_code_opera
     |  SI expr_booleen ALORS du_code_opera SINON du_code_opera
     | declaration_opera 
     | affectation_opera
 ;

affectation_opera : affectation 
       { if (($1[1] != C_SDO) &&
	     ($1[1] != C_SDP) &&
	     ($1[1] != C_VARU_SDO) &&
	     ($1[1] != C_VARU_SDP)) {
	   msg = "La variable affect�e est invisible dans une RgOpera";
	   yyerror(msg);
         }
       }
;

declaration_opera : ID EQ expr_type
       { string vide; vide = "";
	 $$ = MaJType(lsymbole, $1, vide + TraduType1Lettre($3) + "Variable[" 
              + FaireEnumVSDO($1) +"]", $3[0], C_VARU_SDO, I_VARIABLE);
         if ($1[0] != '%') {
	   msg = vide + "La variable de d�cision utilisateur " + $1
                 + " doit commencer par un %";
	   yyerror(msg);
         }
       }
;

%%
/* procedures C annexes */


//---------------------------------------------------------------------------
string VerifFonction(string fonction, string arg)
  /* verifie que fonction est le nom d'un symbole de type I_FONCTION et 
    que le type de arg corresponde avec celui de la fonction */
{
  string vide; vide = "";
  string type_f = Type(fonction,lsymbole);
  if (type_f[2] != I_FONCTION) {
    msg = vide + fonction + " n'est pas une fonction";
    yyerror(msg);
  }
  int i=0;
  while((3+(i*2) < type_f.length()) && (i < arg.length())) {
    if ((type_f[3+(i*2)] != arg[i]) &&
	(type_f[3+(i*2)] == T_NOMBRE_INT) &&
	(arg[i] != T_NOMBRE)) {
      msg =  vide + fonction + "(" + arg + ") est du type " + type_f;
      yyerror(msg);
    }
    i++;
  }
  while(3+(i*2) < type_f.length()) {
    if (type_f[4+(i*2)] != P_PAR_DEFAUT) {
    msg =  vide + "Pas assez d'arg, " + fonction + "(" + arg + ") est du type " + type_f;
    yyerror(msg);
    }
    i++;
  }
  if (arg.length() > ((type_f.length()-3)/2)) {
    msg = vide + "Trop d'arg, " + fonction + "(" + arg + ") est du type " + type_f;
    yyerror(msg);
    }
  return type_f;
}

//---------------------------------------------------------------------------
string VerifVarConst(string var_const)
  /* verifie que var_const est le nom d'un symbole de type I_VARIABLE ou de type I_CONSTANTE */
{
  string vide; vide = "";
  string type_v_c = Type(var_const,lsymbole);
  if ((type_v_c[2] != I_VARIABLE)&&(type_v_c[2] != I_CONSTANTE)) {
    msg = vide + var_const + "(" + type_v_c 
          + ") n'est pas une variable ou une constante";
    yyerror(msg);
  }
  // else
  return type_v_c;
}

//---------------------------------------------------------------------------
string VerifVariable(string variable)
  /* verifie que variable est le nom d'un symbole de type I_VARIABLE */
{
  string vide; vide = "";
  string type_v = Type(variable,lsymbole);
  if (type_v[2] != I_VARIABLE) {
    msg = vide + variable + "(" + type_v + ") n'est pas une variable";
    yyerror(msg);
  }
  // else
  return type_v;
}

//---------------------------------------------------------------------------
int TestVarDeclaree()
  /* test si toutes les variables utilisateurs de la liste de symbole 
     ont ete affectees au moins une fois */
{
  string vide; vide = "";
  lsymbole.Restart();
  while(lsymbole) {
    if ((lsymbole.Current()->type == "") ||
	(lsymbole.Current()->tradu == "") ||
	(lsymbole.Current()->type[2] == I_INCONNU)) {
      msg = vide + "symbole " + lsymbole.Current()->nom + " de type : " +
	lsymbole.Current()->type + "et de tradu : " +
	lsymbole.Current()->tradu + " , non declare\n";
      yyerror(msg);
    }
    // else
    lsymbole++;
  }
  return 1;
} 

//---------------------------------------------------------------------------
/* si tout se passe bien ecriture d'un fichier vide : OKverification */
int main(int argc, char* *argv)
{
  printf("VERIFICATION DE LA STRATEGIE\n");
  /* reaffecter le descripteur du fichier d'entree `yyin' de lex */
  FILE* pfich;
  pfich = fopen(*(argv+1),"r");
  if (pfich != NULL)
    {
    yyin = pfich;

    if (! lire_dico_enum(lgp_enum,DICO_ENUM_IN)) 
      cout<<"erreur de lecture du dictionnaire de groupe enumere"<<endl;
    else
      if (! lire_librairie(lsymbole,lgp_enum,LIB_IN)) 
	cout<<"erreur de lecture de librairie"<<endl;
      else
	if (! yyparse())
	  {
	    //afficher_librairie(lsymbole,lgp_enum);
	    TestVarDeclaree();
	    cout<<"\n\n"<<endl;

	    if (ecrire_librairie(lsymbole,LIB_OUT))
	      {
		/* OK */
		ofstream fresultat;
		fresultat.open("OKverification",ios::out);
		fresultat.close();
	      };
	  };
    };
  return 0;
}
