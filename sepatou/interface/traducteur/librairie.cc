// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/*-----------------------------------------------------------------
 * fichier : librairie.cc
 * contenu : fonctions de chargement et d'�criture des librairies
 *-----------------------------------------------------------------*/

#ifndef LIBRAIRIE_CC
#define LIBRAIRIE_CC 1

#include <iostream>
#include <fstream>
#include <string>
#include "CListe.h"

/* une donnee est lue et ecrite ds un fichier sous son nom et enregistree
   ou lue dans une liste sous son code */
typedef struct SDonnee{
  char* nomD; /* nom de la donnee lu ou ecrit ds le fichier */
  char codeD; /* code de la donnee */
} ; 

/* tableau de donnees des types de valeurs */
SDonnee tab_type[] = {"T_BOOLEEN",T_BOOLEEN,\
		      "T_ENUM",T_ENUM,\
		      "T_NBRE_ENUM",T_NBRE_ENUM,\
		      "T_NOMBRE_INT",T_NOMBRE_INT,\
		      "T_NOMBRE",T_NOMBRE,\
		      "T_CHAINE",T_CHAINE,\
		      "T_PARCELLE",T_PARCELLE,\
		      "T_ENS_PARCELLE",T_ENS_PARCELLE,\
		      "T_INDICATEUR",T_INDICATEUR,\
		      "T_ACTIVITE",T_ACTIVITE,\
		      "T_ENS_ACTIVITE",T_ENS_ACTIVITE,\
		      "T_ACTION",T_ACTION,\
		      "T_ENS_ACTION",T_ENS_ACTION,\
		      "T_REGLE",T_REGLE,\
		      "T_INCONNU",T_INCONNU};

/* tableau de donnees des types d'identificateurs (symboles) */
SDonnee tab_type_iden[] = {"I_VARIABLE",I_VARIABLE,\
			   "I_FONCTION",I_FONCTION,\
			   "I_CONSTANTE",I_CONSTANTE,\
			   "I_EXPRESSION",I_EXPRESSION,\
			   "I_INDICATEUR",I_INDICATEUR,\
			   "I_INCONNU",I_INCONNU};

/* tableau de donnees des classes */
SDonnee tab_classe[] = {"C_PLAN_ALIM_CONSERVEE",C_PLAN_CONCENTREMAIS,\
		        "C_PLAN_PATURAGE",C_PLAN_PATURAGE,\
		        "C_PLAN_FAUCHE",C_PLAN_FAUCHE,\
		        "C_PLAN_AZOTE",C_PLAN_AZOTE,\
		        "C_PLAN_COORDINATION",C_PLAN_COORDINATION,\
		        "C_SDP",C_SDP,\
		        "C_SDO",C_SDO,\
		        "C_SIO",C_SIO,\
		        "C_SIS",C_SIS,\
		        "C_GLOBAL",C_GLOBAL,\
		        "C_ENS_PARCELLE",C_ENS_PARCELLE,\
		        "C_TEMPS",C_TEMPS,\
		        "C_FOI",C_FOI,\
		        "C_VARU_SDP",C_VARU_SDP,\
		        "C_VARU_SDO",C_VARU_SDO,\
		        "C_INCONNU",C_INCONNU};

/* tableau de donnees des types de parametres */
SDonnee tab_type_param[] = {"P_PAR_DEFAUT",P_PAR_DEFAUT,\
			    "P_OBLIGATOIRE",P_OBLIGATOIRE,\
			    "P_INCONNU",P_INCONNU};


//---------------------------------------------------------------------
gp_enum* chercher_nom_rg_enum(CListe<gp_enum> &lgp,string nom_gp_enum)
/* retourne le groupe enumere nom_gp_enum contenu ds la liste lgp */
{
  lgp.Restart();
  while((lgp) && (lgp.Current()->nom_gp_enum != nom_gp_enum)) lgp++;

  if (lgp) 
    return lgp.Current();
  // else
  return NULL;
}


/*****************************************************************************/
/* lecture, ecriture du dictionnaire des symboles predefinies du langage LnU */
/*****************************************************************************/

//---------------------------------------------------------------------
int erreur_lecture_dico_sym(string n,string s, string saux="")
  /* affiche un message d'erreur de lecture du diconnaire de symboles */
{
  printf("Erreur de lecture du dictionnaire des symboles -> %s ... `%s' %s\n",n.c_str(),s.c_str(),saux.c_str());
  return 0;
}

//---------------------------------------------------------------------
char trouver_type(string ty)
  /* retourne le code du type de valeur de nom ty */
{
  int i=0;
  while ((tab_type[i].codeD != T_INCONNU) && (ty != tab_type[i].nomD)) i++;
  return tab_type[i].codeD;
}

//---------------------------------------------------------------------
string ecrire_type(char ty)
  /* retourne le nom du type de valeur de code ty */
{
  int i=0;
  while ((tab_type[i].codeD != T_INCONNU) && (ty != tab_type[i].codeD)) i++;
  return tab_type[i].nomD;
}

//---------------------------------------------------------------------
char trouver_classe(string cl)
  /* retourne le code de la classe de nom cl */
{
  int i=0;
  while ((tab_classe[i].codeD != C_INCONNU) && (cl != tab_classe[i].nomD)) i++;
  return tab_classe[i].codeD;
}

//---------------------------------------------------------------------
string ecrire_classe(char cl)
  /* retourne le nom de la classe de code cl */
{
  int i=0;
  while ((tab_classe[i].codeD != C_INCONNU) && (cl != tab_classe[i].codeD)) i++;
  return tab_classe[i].nomD;
}

//---------------------------------------------------------------------
char trouver_type_identificateur(string ti)
  /* retourne le code du type d'identificateur de nom ti */
{
  int i=0;
  while ((tab_type_iden[i].codeD != I_INCONNU) && (ti != tab_type_iden[i].nomD)) i++;
  return tab_type_iden[i].codeD;
}

//---------------------------------------------------------------------
string ecrire_type_identificateur(char ti)
  /* retourne le nom du type d'identificateur de code ti */
{
  int i=0;
  while ((tab_type_iden[i].codeD != I_INCONNU) && (ti != tab_type_iden[i].codeD)) i++;
  return tab_type_iden[i].nomD;
}

//---------------------------------------------------------------------
char trouver_type_arg(string ta)
  /* retourne le code du type d'argument de nom ta */
{
  int i=0;
  while ((tab_type_param[i].codeD != P_INCONNU) && (ta != tab_type_param[i].nomD)) i++;
  return tab_type_param[i].codeD;
}

//---------------------------------------------------------------------
string ecrire_type_arg(char ta)
  /* retourne le nom du type d'argument de code ta */
{
  int i=0;
  while ((tab_type_param[i].codeD != P_INCONNU) && (ta != tab_type_param[i].codeD)) i++;
  return tab_type_param[i].nomD;
}

 
//---------------------------------------------------------------------
int lire_librairie(CListe<symbole> &ls,CListe<gp_enum> &lgp,char* lib_in)
  /* Charge le fichier dictionnaire de symbole de nom lib_in dans la liste ls et rattachement des types T_ENUM et T_NBRE_ENUM a leur groupe enumere */
{ 
  ifstream f(lib_in,ios::in);
  string nom,type,tradu,nom_gp_enum;
  gp_enum* groupe_enum;
  int indice;
  char c;
  string ty,cl,ti,ar,ta;
  while(f>>nom) {// lecture du nom du symbole
    type = ""; // efface le type pr�c�dent
    f>>c;
    if (c != ':') return erreur_lecture_dico_sym(nom,": attendu");
    // else 
    f>>tradu;
    f>>c;
    if (c != ',') return erreur_lecture_dico_sym(nom,", attendue");
    // else 
    f>>ty; // lecture du type du symbole
    c = trouver_type(ty);
    if (c == T_INCONNU) return erreur_lecture_dico_sym(nom,ty,"type inconnu");
    //else 
    type = c;
    if ((c == T_ENUM) || (c == T_NBRE_ENUM)) { // lecture du nom du groupe enumere
      f>>c;
      if (c != ',') return erreur_lecture_dico_sym(nom,", attendue");
      // else
      f>>nom_gp_enum;
      groupe_enum = chercher_nom_rg_enum(lgp,nom_gp_enum);
      if (groupe_enum == NULL) return erreur_lecture_dico_sym(nom,nom_gp_enum,"nom de groupe enum inconnu");
    }
    else groupe_enum = NULL;
      
    f>>c;
    if (c != ',') return erreur_lecture_dico_sym(nom,", attendue");
    //else
    f>>cl; // lecture de la classe du symbole
   c = trouver_classe(cl);
    if (c == C_INCONNU) return erreur_lecture_dico_sym(nom,cl," classe inconnue");
    // else
    type = type + c;
    f>>c;
    if (c != ',') return erreur_lecture_dico_sym(nom,", attendue");
    //else
    f>>ti; // lecture du type d'identificateur du symbole
    c = trouver_type_identificateur(ti);
    if (c == I_INCONNU) return erreur_lecture_dico_sym(nom,ti," type identificateur inconnue");
    // else
    type = type + c;
    
    f>>c;
    if (c == ',') {
      if (type[2] != I_FONCTION) return erreur_lecture_dico_sym(nom," doit etre une fonction ou alors ; attendu");
      // else
      indice = 3;
      do {
	f>>ar; // lecture du type de l'argument
	c = trouver_type(ar);
	if (c == T_INCONNU) return erreur_lecture_dico_sym(nom,ar," type inconnu");
	// else
	type = type + c;
	indice++;
	f>>c;
	if (c != ',') return erreur_lecture_dico_sym(nom,", attendue"); 
	// else
	f>>ta; // lecture d'argument par defaut
	c = trouver_type_arg(ta);
	if (c == P_INCONNU) return erreur_lecture_dico_sym(nom,ta," passage de parametre inconnu");
	// else
	type = type + c;
	indice++;
	
	f>>c; // lecture de ',' ou de ';'
      }
      while(c == ',');
    }
    if (c != ';') return erreur_lecture_dico_sym(nom,"; attendue");
    // else
    symbole* s=new symbole;
    s->nom = nom;
    s->type = type;
    s->tradu = tradu;
    s->groupe_enum = groupe_enum;
    ls.AddLast(s);
  }
  return 1;
}

//---------------------------------------------------------------------
int ecrire_librairie(CListe<symbole> &ls,char* lib_out)
  /* ecriture de la librairie de symbole ls dans le fichier de nom lib_out */
{ 
  string type;
  int i;
  ofstream f(lib_out,ios::out);
  ls.Restart();

  while(ls) {
    f << ls.Current()->nom << " : " << ls.Current()->tradu << " ," << endl;

    type = ls.Current()->type;
    f << "\t"<<ecrire_type(type[0]) << " ,";
    if ( (type[0] == T_ENUM) || (type[0] == T_NBRE_ENUM) )
      f << " " << ls.Current()->groupe_enum->nom_gp_enum << " ," << endl;
    else
      f << endl;

    f << "\t" << ecrire_classe(type[1]) << " ," << endl;

    f << "\t" << ecrire_type_identificateur(type[2]);
    
    for(i=0;(i<((type.length()-3)/2));i++) {
      f << " ," << endl << "\t" << ecrire_type(type[3+(i*2)]) << " ," << endl;
      f << "\t" << ecrire_type_arg(type[4+(i*2)]);
    }
    f << " ;" << endl << endl;
    
    ls++;
  }
  return 1;
}
    

//---------------------------------------------------------------------
void afficher_librairie(CListe<symbole> &ls,CListe<gp_enum> &lgp)
  /* affiche la liste des groupes enumeres lgp et la liste des symboles ls*/
{
  cout << endl << "Table des groupes enumeres :" << endl;
  lgp.Restart();
  while (lgp) {
    cout << lgp.Current()->nom_gp_enum << endl;
    lgp++;
  }

  cout << endl << "Table des symboles :" << endl;
  ls.Restart();
  while (ls) {
    cout << ls.Current()->nom << " : " << ls.Current()->type << endl;
    ls++;
  }
}

/*****************************************************************************/
/* lecture, ecriture du dictionnaire des groupes enumeres predefinis         */
/*  du langage LnU                                                           */
/*****************************************************************************/

//---------------------------------------------------------------------
int erreur_lecture_dico_enum(string n,string s)
  /* affiche un message d'erreur de lecture du diconnaire des groupes enumeres */
{
  printf("Erreur de lecture du dictionnaire des groupes enum --> %s ... `%s'\n",n.c_str(),s.c_str());
  return 0;
}

//---------------------------------------------------------------------
int lire_dico_enum(CListe<gp_enum> &lgp,char* dico_in)
  /* chargement du fichier dictionnaire des groupes enumeres ds la liste lgp */
{ 
  ifstream f(dico_in,ios::in);
  string nom_gp_enum, nom_val, tradu_val;
  int nb_val;
  char c;
  gp_enum* groupe_enum;

  while(f >> nom_gp_enum) {// lecture du nom du groupe
    groupe_enum = new gp_enum; // declaration d'un nouveau groupe enumere
    groupe_enum->nom_gp_enum = nom_gp_enum;

    f >> c;
    if (c != ':') return erreur_lecture_dico_enum(nom_gp_enum,": attendu");
    // else 
    f >> nom_val; // lecture du nom de la 1er valeur enumere
    f >> c;
    if (c != ',') return erreur_lecture_dico_enum(nom_gp_enum,", attendue");
    // else 
    f >> tradu_val; // lecture de la traduction de la 1er valeur enumere
    nb_val = 1; // nombre de valeur lue = 1
    groupe_enum->gp_val_possible[nb_val-1].nom_val = nom_val;
    groupe_enum->gp_val_possible[nb_val-1].tradu_val = tradu_val;
    
    f >> c;
    while((c != ';')&&(c == ',')) {
      f >> nom_val; // lecture du nom de la 1er valeur enumere
      f >> c;
      if (c != ',') return erreur_lecture_dico_enum(nom_gp_enum,", attendue");
      // else 
      f >> tradu_val; // lecture de la traduction de la 1er valeur enumere
      if (nb_val == NBVALPOSSIBLE) return erreur_lecture_dico_enum(nom_gp_enum,"trop de valeur enumeree");
      nb_val++; // nombre de valeur lue = 1
      groupe_enum->gp_val_possible[nb_val-1].nom_val = nom_val;
      groupe_enum->gp_val_possible[nb_val-1].tradu_val = tradu_val;
      
      f >> c;
      if ((c != ',') && (c != ';')) 
	return erreur_lecture_dico_enum(nom_gp_enum,", ou ; attendu");
    } 
    if (c != ';') 
      return erreur_lecture_dico_enum(nom_gp_enum,"; attendu");
    // else
    lgp.AddLast(groupe_enum);
  }
  return 1;
}



#endif
