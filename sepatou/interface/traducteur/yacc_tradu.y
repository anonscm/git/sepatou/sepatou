/*
** Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
** 
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software 
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*-----------------------------------------------------------------
 * fichier : yacc_tradu.y
 * contenu : definition des regles syntaxique du langage utilisateur
 *           pour en verifier la syntaxe d
 *-----------------------------------------------------------------*/
 
%{  
#include <stdlib.h>
#include <fstream>
#include <string>
#include <math.h>
using namespace std;
#include "lnu.h"
#include "librairie.cc"
#include "yacc_tradu.h"


/* declaration des fonctions et des variables externes */
extern void yyerror(string s); /* fo de message d'erreur de lex_tradu.l */
extern int yylex(); /* fonction d'appel de l'analyseur lexical */
extern FILE* yyin; /* fichier d'entrer de l'analyseur lexical */
extern FILE* yyout; /* fichier de sortie de l'analyseur lexical */

/* signature des fonctions en annexes*/
string TraduVarConst(string nom); 
/* traduit un symbole de type I_VARIABLE ou I_CONSTANTE */

string TraduIndicateur(string indicateur, string fonction); 
/* traduit un symbole de type I_INDICATEUR */

string TraduFonction(string nom, string arg); 
/* traduit un symbole de type I_FONCTION */

string TraduValEnum(string nom, string val_enum); 
/* traduit une valeur possible val_enum d'un symbole de nom nom */

/* declaration des fichiers a ecrire */
ofstream FichIndic; /* fichier des indicateurs */
ofstream FichFoInter; /* fichier des fonctions d'interpretations */
ofstream FichRgPlani; /* fichier des regles de planifications */
ofstream FichCreerRgPlani; /* fichier de creation des regles de planifications */
ofstream FichRgOpera; /* fichier des regles operatoires */
ofstream FichCreerRgOpera; /* fichier de creation des regles operatoires */
ofstream FichStrat_H; /* fichier de declaration des enums */

CListe<symbole> lsymbole; /* liste des symboles */
CListe<gp_enum> lgp_enum; /* declaration de la liste de groupe enumere */


string NomRg; /* nom de la regle courante */
string Tradu; /* tampon de traduction temporaire */
string ListeIndicateur; /* liste des indicateurs d'un declencheur */
string ListeVar; /* liste des variables locales d'une FoI */
string NomPlan; /* nom du plan courant */
string NomActivite; /* nom de l'activite courante */
string NomEnum; /* nom du symbole courant de type enum */
string TraduReutilisation; /* REUTILISABLE ou NON_REUTILISABLE */
int CptDeclencheur = 1; /* compteur de declencheur d'une regle */
int CptDirective = 1; /* compteur de directive */

%}



/* declaration des unites lexicales */ 
%start strategie
%token ET OU 
%token EQ NEQ LT LEQ GT GEQ VIDE EXISTE PARMI PAS
%token SI ALORS SINON
%token DEBUT DU AU FAIRE FIN
%token INDICATEUR CARACTERISTIQUE FONCTION CODE
%token ACTIVITE FIN_ACTIVITE RgPLANIF REUTILISABLE DECLENCHEUR RgOPERA
%token VRAI FAUX
%token ENTIER 
%token REEL
%token CHAINE
%token NoJour
%token ID              /* identificateur */
%token ID_booleen      /* identificateur de type booleen */
%token ID_enum         /* identificateur de type enum */
%token ID_nbre_enum    /* identificateur de type nombre enum */
%token ID_nombre_int   /* identificateur de type entier */
%token ID_nombre       /* identificateur de type nombre */
%token ID_chaine       /* identificateur de type chaine */
%token ID_parcelle     /* identificateur de type parcelle */
%token ID_ens_parcelle /* identificateur de type ensemble de parcelles */
%token ID_indicateur   /* identificateur de type indicateur */
%token ID_activite     /* identificateur de type activite */
%token ID_action       /* identificateur de type action */
%token ID_ens_action   /* identificateur de type ensemble d'actions */
%token ID_ens_activite /* identificateur de type ensemble d'activite */
%token ID_regle        /* identificateur de type regle */

/* declaration des priorites (par ordre croissant */
%right AFFECT /* operateur d'affectation, association a droite */
%left OU ET  /* association a gauche */
%left EQ NEQ LT LEQ GT GEQ /* association a gauche */
%left MODULO /* association a gauche */
%left '+' '-' /* association a gauche */
%left '*' '/' /* association a gauche */
%right PAS /* operateur unaire booleen, association a droite */
%right UMINUS /* operateur unaire -, association a droite */
 
/* definitions des regles syntaxiques */
%%
strategie :
     | strategie declaration_1indicateur 
     | strategie declaration_1foi
     | strategie declaration_1rg_planif
     | strategie declaration_rg_opera
     | strategie error
       { yyclearin;
         yyerrok;
       }
;

/* declaration d'un indicateur */
declaration_1indicateur : INDICATEUR ':' ID_indicateur CARACTERISTIQUE ':' expr_booleen
       { FichIndic << TRADU_T_BOOLEEN << " " << FaireNomI($3)
	           << "_CARACTERISTIQUE()" << endl;
         FichIndic << "{" << endl << "return " << $6 << ";\n}\n" << endl;
	 ListeIndicateur = "";
	 cout << "Indicateur " << $3 << " : traduction OK" << endl;
       }
;

un_nbre_enum : ID_nbre_enum   { $$ = TraduVarConst($1); NomEnum = $1; }
     | ID_nbre_enum arguments { $$ = TraduFonction($1,$2); NomEnum = $1; } 
;

un_enum : ID_enum        { $$ = TraduVarConst($1); NomEnum = $1; }
     | ID_enum arguments { $$ = TraduFonction($1,$2); NomEnum = $1; } 
;
un_nombre : REEL
     | ENTIER /* { string vide; vide = ""; $$ = vide + $1 + ".0"; } */
     | NoJour
       { Tradu = apres($1, "J");
         string vide; vide = ""; 
         $$ = vide + "Temps.NoJour(" + avant(Tradu, "_") + "," 
              + apres(Tradu, "_") + ")"; }
     | ID_nombre_int arguments { $$ = TraduFonction($1,$2); } 
     | ID_nombre_int           { $$ = TraduVarConst($1); }
     | ID_nombre arguments { $$ = TraduFonction($1,$2); } 
     | ID_nombre           { $$ = TraduVarConst($1); }
     | ID_indicateur 
       { $$ = TraduIndicateur($1,"ValeurIndicateur"); 
         ListeIndicateur = ListeIndicateur + $1 + ",";
       }
     | un_nbre_enum
;

un_booleen : VRAI { $$ = "true"; }
     | FAUX       { $$ = "false"; }
     | ID_booleen arguments { $$ = TraduFonction($1,$2); }
     | ID_booleen           { $$ = TraduVarConst($1); }
;

booleen_booleen : un_booleen
     | un_booleen EQ un_booleen  
        { string vide; vide = ""; $$ = vide + "(" + $1 + " == " + $3 + ")" ; }
     | un_booleen NEQ un_booleen 
        { string vide; vide = ""; $$ = vide + "(" + $1 + " != " + $3 + ")" ; }
;

booleen_nombre : expr_nombre EQ expr_nombre
       { string vide; vide = ""; $$ = vide + "(" + $1 + " == " + $3 + ")"; }
     | expr_nombre NEQ expr_nombre
       { string vide; vide = ""; $$ = vide + "(" + $1 + " != " + $3 + ")"; }
     | expr_nombre LT expr_nombre
       { string vide; vide = ""; $$ = vide + "(" + $1 + " < " + $3 + ")"; }
     | expr_nombre LEQ expr_nombre
       { string vide; vide = ""; $$ = vide + "(" + $1 + " <= " + $3 + ")"; }
     | expr_nombre GT expr_nombre
       { string vide; vide = ""; $$ = vide + "(" + $1 + " > " + $3 + ")"; }
     | expr_nombre GEQ expr_nombre
       { string vide; vide = ""; $$ = vide + "(" + $1 + " >= " + $3 + ")"; }
;

booleen_chaine : expr_chaine EQ expr_chaine 
        { string vide; vide = ""; $$ = vide + "(" + $1 + " == " + $3 + ")"; }
     | expr_chaine NEQ expr_chaine          
        { string vide; vide = ""; $$ = vide + "(" + $1 + " != " + $3 + ")"; }
;

booleen_parcelle : expr_parcelle EQ expr_parcelle 
       { string vide; vide = ""; $$ =  vide + "(" + $1 + " == " + $3 + ")"; }
     | expr_parcelle NEQ expr_parcelle       
       { string vide; vide = ""; $$ = vide + "(" + $1 + " != " + $3 + ")"; }
     | expr_parcelle PARMI expr_ens_parcelle 
       { string vide; vide = ""; $$ = vide + $3+".Parmi(" + $1 + ")"; }
;

booleen_ens_parcelle : expr_ens_parcelle EQ expr_ens_parcelle
       { string vide; vide = ""; $$ = vide + "(" + $1 + " == " + $3 + ")";
       }
     | expr_ens_parcelle NEQ expr_ens_parcelle
       { string vide; vide = ""; $$ =  vide + "(" + $1 + " != " + $3 + ")";
       }
     | EXISTE un_ens_parcelle
       { string vide; vide = ""; $$ = vide + "(" +$2 + ".NonVide())"; 
       }
;

booleen_enum : un_enum EQ un_enum 
       { string vide; vide = ""; $$ = vide + "(" + $1 + " == " + $3 + ")"; }
     | un_enum NEQ un_enum        
       { string vide; vide = ""; $$ = vide + "(" + $1 + " != " + $3 + ")"; }
     | un_enum EQ CHAINE          
        { string vide; vide = ""; 
          $$ = vide + "(" + $1 + " == " + TraduValEnum(NomEnum,$3) + ")"; }
     | un_enum NEQ CHAINE         
        { string vide; vide = ""; 
          $$ = vide + "(" + $1 + " != " + TraduValEnum(NomEnum,$3) + ")"; }
     | CHAINE EQ un_enum          
        { string vide; vide = ""; 
          $$ = vide + "(" + TraduValEnum(NomEnum,$1) + " == " + $3 + ")"; }
     | CHAINE NEQ un_enum         
        { string vide; vide = ""; 
          $$ = vide + "(" + TraduValEnum(NomEnum,$1) + " != " + $3 + ")"; }
;

booleen_nbre_enum : un_nbre_enum EQ expr_nombre 
       { string vide; vide = "";  $$ =  vide + "(" + $1 + " == " + $3 + ")"; }
     | un_nbre_enum NEQ expr_nombre 
       { string vide; vide = "";  $$ = vide + "(" + $1 + " != " + $3 + ")"; }
     | un_nbre_enum EQ CHAINE            
         { string vide; vide = "";  
           $$ = vide + "(" + $1 + " == " + TraduValEnum(NomEnum,$3) + ")"; }
     | un_nbre_enum NEQ CHAINE           
         { string vide; vide = "";  
           $$ = vide + "(" + $1 + " != " + TraduValEnum(NomEnum,$3) + ")"; }
     | CHAINE EQ un_nbre_enum            
         { string vide; vide = "";  
           $$ = vide + "(" + TraduValEnum(NomEnum,$1) + " == " + $3 + ")"; }
     | CHAINE NEQ un_nbre_enum           
         { string vide; vide = "";  
           $$ = vide + "(" + TraduValEnum(NomEnum,$1) + " == " + $3 + ")"; }
;

expr_booleen : booleen_booleen
     | booleen_nombre
     | booleen_chaine
     | booleen_parcelle
     | booleen_ens_parcelle
     | booleen_enum
     | booleen_nbre_enum
     | '(' expr_booleen ')'         { $$ = $2; }
     | expr_booleen ET expr_booleen 
        { string vide; vide = "";  $$ = vide + "(" + $1 + " &&\n" + $3 + ")" ; }
     | expr_booleen OU expr_booleen 
        { string vide; vide = "";  $$ = vide + "(" + $1 + " ||\n" + $3 + ")" ; }
     | PAS expr_booleen             
        { string vide; vide = "";  $$ =  vide +"(! " + $2 + ")"; }
;

expr_nombre : un_nombre
     | '(' expr_nombre ')' { $$ = $2; }
     | expr_nombre '+' expr_nombre
       { string vide; vide = "";  $$ = vide + "(" + $1 + " + " + $3 + ")"; }
     | expr_nombre '-' expr_nombre
       { string vide; vide = "";  $$ =  vide +"(" + $1 + " - " + $3 + ")"; }
     | expr_nombre '*' expr_nombre
       { string vide; vide = "";  $$ = vide + "(" + $1 + " * " + $3 + ")"; }
     | expr_nombre '/' expr_nombre
       { string vide; vide = "";  $$ = vide + "(" + $1 + " / " + $3 + ")"; }
     | expr_nombre MODULO expr_nombre
       { string vide; vide = "";  $$ = vide + "(" + $1 + " % " + $3 + ")"; }
     | '-' expr_nombre %prec UMINUS
       { string vide; vide = "";  $$ = vide + "(-" + $2 + ")"; }
;

expr_chaine : CHAINE       
       {string vide; vide = "";  $$ =  vide + "\"" + $1 + "\"" ; }
     | ID_chaine arguments {  $$ = TraduFonction($1,$2); }
     | ID_chaine           {  $$ = TraduVarConst($1); }
;

une_parcelle : ID_parcelle arguments { $$ = TraduFonction($1,$2); }
     | ID_parcelle                   { $$ = TraduVarConst($1); }
;

expr_parcelle : une_parcelle
;
  
un_ens_parcelle :  ID_ens_parcelle arguments { $$ = TraduFonction($1,$2); }
     | ID_ens_parcelle                       { $$ = TraduVarConst($1); }
     | VIDE                                  { $$ =  "EnsembleParcellesVide"; }
;

expr_ens_parcelle : un_ens_parcelle
     | '(' expr_ens_parcelle ')' 
        {string vide; vide = ""; $$ = vide + "(" + $2 + ")"; }
     | expr_ens_parcelle '+' expr_ens_parcelle
       {string vide; vide = ""; $$ = vide + "(" + $1 + " + " + $3 + ")"; }
     | expr_ens_parcelle '-' expr_ens_parcelle
       {string vide; vide = ""; $$ = vide + "(" + $1 + " - " + $3 + ")"; }
     | expr_ens_parcelle '+' expr_parcelle
       {string vide; vide = ""; $$ = vide + "(" + $1 + " + " + $3 + ")"; }
     | expr_ens_parcelle '-' expr_parcelle
       {string vide; vide = ""; $$ = vide + "(" + $1 + " - " + $3 + ")"; }
;

une_activite : ID_activite { $$ = TraduNom($1,lsymbole); }
;

une_action : ID_action { $$ = TraduNom($1,lsymbole); }
;

arguments : '(' ')'            { $$ = ""; }
     | '(' suite_expr_type ')' { $$ = $2; }
;

suite_expr_type : expr_type
     | suite_expr_type ',' expr_type 
       {string vide; vide = ""; $$ = vide + $1 + ", " + $3; }
;

expr_type : un_booleen
     | expr_nombre
     | expr_chaine
     | expr_parcelle
     | expr_ens_parcelle
;

/* declaration d'une fonction d'interpretation */
declaration_1foi :FONCTION ':' ID_booleen CODE ':' du_code_return
       { string temp;
         FichFoInter << TRADU_T_BOOLEEN << " " << FaireNomFO($3)
                     << "_CODE(void)" << endl;
         FichFoInter << "{" << endl << $6 << endl << "}\n" << endl;

	 temp = ListeVar;
	 while (temp != "") {
	   DelSym(avant(temp, ","),lsymbole);
	   temp = apres(temp, ",");
	 }
	 ListeVar = "";
	 cout << "Fonction " << $3 << " de type " << ecrire_type(T_BOOLEEN)
              << " : traduction OK" << endl;
	 ListeIndicateur = "";
       } 
     | FONCTION ':' ID_nombre CODE ':' du_code_return
       { string temp;
         FichFoInter << TRADU_T_NOMBRE << " " << FaireNomFO($3)
                     << "_CODE(void)" << endl;
         FichFoInter << "{" << endl << $6 << endl << "}\n" << endl;

	 temp = ListeVar;
	 while (temp != "") {
	   DelSym(avant(temp, ","),lsymbole);
	   temp = apres(temp, ",");
	 }
	 ListeVar = "";
	 cout << "Fonction " << $3 << " de type " << ecrire_type(T_NOMBRE)
              << " : traduction OK" << endl;
	 ListeIndicateur = "";
       }
     | FONCTION ':' ID_chaine CODE ':' du_code_return
       { string temp;
         FichFoInter << TRADU_T_CHAINE << " " << FaireNomFO($3)
                     << "_CODE(void)" << endl;
         FichFoInter << "{" << endl << $6 << endl << "}\n" << endl;

	 temp = ListeVar;
	 while (temp != "") {
	   DelSym(avant(temp, ","),lsymbole);
	   temp = apres(temp, ",");
	 }
	 ListeVar = "";
	 cout << "Fonction " << $3 << " de type " << ecrire_type(T_CHAINE)
              << " : traduction OK" << endl;
	 ListeIndicateur = "";
       }
     | FONCTION ':' ID_parcelle CODE ':' du_code_return
       { string temp;
         FichFoInter << TRADU_T_PARCELLE << " " << FaireNomFO($3) 
                     << "_CODE()" << endl;
         FichFoInter << "{" << endl << $6 << endl << "}\n" << endl;

	 temp = ListeVar;
	 while (temp != "") { 
	   DelSym(avant(temp,","),lsymbole);
	   temp = apres(temp, ",");
	 }
	 ListeVar = "";
	 cout << "Fonction " << $3 << " de type " << ecrire_type(T_CHAINE)
              << " : traduction OK" << endl;
       }
     | FONCTION ':' ID_ens_parcelle CODE ':' du_code_return
       { string temp;
         FichFoInter << TRADU_T_ENS_PARCELLE << " " << FaireNomFO($3)
                     << "_CODE(void)" << endl;
         FichFoInter << "{" << endl << $6 << endl << "}\n" << endl;

	 temp = ListeVar;
	 while (temp != "") {
	   DelSym(avant(temp, ","),lsymbole);
	   temp = apres(temp, ",");
	 }
	 ListeVar = "";
	 cout << "Fonction " << $3 << " de type " << ecrire_type(T_ENS_PARCELLE)
              << " : traduction OK" << endl;
	 ListeIndicateur = "";
       }
;


du_code_return : un_code_return
     | '{' un_code_return '}' { $$ = $2; }
     | '{' suite_code un_code_return '}' 
       {string vide; vide = ""; $$ = vide + $2 + "\n" + $3; }
;

un_code_return : expr_type  
       {string vide; vide = "";  $$ = vide + "return " + $1 + ";"; }
     | SI expr_booleen ALORS du_code_return SINON du_code_return
       {string vide; vide = "";  
        $$ =  vide + "if (" + $2 + ") {\n" + $4 + "\n}\nelse {\n" + $6 + "\n}"; }
;

du_code : un_code 
     | '{' suite_code '}' { $$ = $2; }
;
suite_code : un_code
     | suite_code un_code {string vide; vide = "";  $$ =  vide + $1 + "\n" + $2; }
;

un_code : SI expr_booleen ALORS du_code
       {string vide; vide = ""; 
        $$ = vide + "if (" + $2 + ") {\n" + $4 + "\n}"; }
     | SI expr_booleen ALORS du_code SINON du_code
       {string vide; vide = ""; 
        $$ =  vide + "if (" + $2 + ") {\n" + $4 + "\n}\nelse {\n" + $6 + "\n}"; }
     | declaration_var_local    {string vide; vide = ""; $$ = vide + $1 + ";"; }
     | affectation    {string vide; vide = ""; $$ = vide + $1 + ";"; }
;
/* declaration d'une variable utilisateur dans une fonction d'interpretation*/
declaration_var_local : ID EQ un_booleen
       { MaJType(lsymbole,$1, FaireNomVFOI($1),T_BOOLEEN, C_FOI, I_VARIABLE);
         $$ = TRADU_T_BOOLEEN; $$ = $$ + " " + TraduNom($1,lsymbole) + " = " + $3;
         ListeVar = ListeVar + $1 + ",";
       }
     | ID EQ expr_nombre
       { MaJType(lsymbole,$1, FaireNomVFOI($1),T_NOMBRE, C_FOI, I_VARIABLE);
	 $$ = TRADU_T_NOMBRE; $$ = $$ + " " + TraduNom($1,lsymbole) + " = " + $3;
         ListeVar = ListeVar + $1 + ",";
       }
     | ID EQ expr_chaine
       { MaJType(lsymbole,$1, FaireNomVFOI($1),T_CHAINE, C_FOI, I_VARIABLE);
	 $$ = TRADU_T_CHAINE; $$ = $$ + " " + TraduNom($1,lsymbole) + " = " + $3;
         ListeVar = ListeVar + $1 + ",";
       }
     | ID EQ expr_parcelle
       { MaJType(lsymbole,$1, FaireNomVFOI($1),T_PARCELLE, C_FOI, I_VARIABLE);
         $$ = TRADU_T_PARCELLE; $$ = $$ + " " + TraduNom($1,lsymbole) + " = " + $3;
         ListeVar = ListeVar + $1 + ",";
       }
     | ID EQ expr_ens_parcelle
       { MaJType(lsymbole,$1, FaireNomVFOI($1),T_ENS_PARCELLE, C_FOI, I_VARIABLE);
	 $$ = TRADU_T_ENS_PARCELLE; 
         $$ = $$ + " " + TraduNom($1,lsymbole) + " = " + $3;
         ListeVar = ListeVar + $1 + ",";	 
       }
;

/* affectation d'une variable */
affectation : ID_booleen EQ un_booleen 
       { $$ = TraduVarConst($1) + " = " + $3;
         Tradu = Type($1,lsymbole);
       }
     | ID_nombre EQ expr_nombre
       { $$ = TraduVarConst($1) + " = " + $3;
         Tradu = Type($1,lsymbole);
       }
     | ID_chaine EQ expr_chaine
       { $$ = TraduVarConst($1) + " = " + $3;
         Tradu = Type($1,lsymbole);
       }
     | ID_parcelle EQ expr_parcelle
       { $$ = TraduVarConst($1) + " = " + $3;
         Tradu = Type($1,lsymbole);
       }
     | ID_ens_parcelle EQ expr_ens_parcelle
       { $$ = TraduVarConst($1) + " = " + $3;
         Tradu = Type($1,lsymbole);
       }
     | ID_ens_activite EQ '{' une_activite ',' une_activite ',' une_activite ',' une_activite ',' une_activite '}'
       { Tradu = TraduVarConst($1);
         $$ = Tradu + "[0] = " + $4 + ";\n" + Tradu + "[1] = " + $6 + ";\n" +
	   Tradu + "[2] = " + $8 + ";\n" + Tradu + "[3] = " + $10 + ";\n" +
	   Tradu + "[4] = " + $12;
         Tradu = Type($1,lsymbole);  }
     | ID_ens_action EQ '{' une_action ',' une_action ',' une_action ',' une_action ',' une_action ',' une_action',' une_action  '}'
       { Tradu = TraduVarConst($1);
         $$ = Tradu + "[0] = " + $4 + ";\n" + Tradu + "[1] = " + $6 + ";\n" +
	   Tradu + "[2] = " + $8 + ";\n" + Tradu + "[3] = " + $10 + ";\n" +
	   Tradu + "[4] = " + $12 + ";\n" + Tradu + "[5] = " + $14 + ";\n" + 
           Tradu + "[6] = " + $16;
         Tradu = Type($1,lsymbole);  }
     | ID_enum EQ CHAINE %prec AFFECT
       { $$ = TraduVarConst($1) + " = " + TraduValEnum($1,$3);
         Tradu = Type($1,lsymbole); }
     | ID_nbre_enum EQ CHAINE %prec AFFECT
       { $$ = TraduVarConst($1) + " = " + TraduValEnum($1,$3);
         Tradu = Type($1,lsymbole); }
     | ID_nbre_enum EQ expr_nombre %prec AFFECT
       { $$ = TraduVarConst($1) + " = " + $3;
         Tradu = Type($1,lsymbole); }
;

/* definition d'une regle de planification */
declaration_1rg_planif : RgPLANIF reutilisation ':' ID_regle declencheurs corps_rg_planif
       { FichRgPlani << $5;
	 FichRgPlani << "void " << NomRg << "_ALORS()\n{\n" << $6 << "\n}\n\n";

	 CptDeclencheur = 1;
	 ListeIndicateur = "";
	 cout << "Regle de planification " << $4 << " : traduction OK" << endl;
       }
;

reutilisation : /* empty */ { TraduReutilisation = "NON_REUTILISABLE"; }
     | REUTILISABLE  { TraduReutilisation = "REUTILISABLE"; }
;

declencheurs : DECLENCHEUR ':' expr_booleen_indicateur
       { string vide; vide = "";
         $$ = vide + TRADU_T_BOOLEEN;
         $$ = $$ + " " + NomRg + "_DECLENCHEUR()\n{\nreturn " + $3 + " ;\n}\n\n";

	 FichCreerRgPlani << "cReglePlanification* " << NomRg << ";" << endl;
	 FichCreerRgPlani << NomRg << " = new cReglePlanification("; 
	 FichCreerRgPlani << NomRg << "_DECLENCHEUR, " << NomRg 
                          <<"_ALORS, " << TraduReutilisation << ", \"" 
                          << NomRg << "\");" << endl;
	 FichCreerRgPlani << "SDPlanificateur.AjouterRegle(" << NomRg
                          << ");" << endl << endl;
	 ListeIndicateur = "";
       }
     | des_declencheurs DECLENCHEUR ':' expr_booleen_indicateur
       { string vide; vide = "";
	 $$ = vide + $1 + TRADU_T_BOOLEEN;
         CptDeclencheur++;
	 Tradu = vide + "RP" + itoa(CptDeclencheur) + apres(NomRg, "RP");
	 $$ = $$ + " " + Tradu + "_DECLENCHEUR()\n{\nreturn " + $4 + " ;\n}\n\n";

	 FichCreerRgPlani << "cReglePlanification* " << Tradu << ";" << endl;
	 FichCreerRgPlani << Tradu << " = new cReglePlanification(";
	 FichCreerRgPlani << Tradu << "_DECLENCHEUR, " << NomRg << "_ALORS, "
                          << TraduReutilisation << ", \"" << NomRg << "("
                          << itoa(CptDeclencheur) << ")\");" << endl;
	 FichCreerRgPlani << "SDPlanificateur.AjouterRegle("<<Tradu<<");"
                          << endl << endl;
	 ListeIndicateur = "";
       }
;

des_declencheurs : DECLENCHEUR ':' expr_booleen_indicateur
       { string vide; vide = "";
	 $$ =  vide + TRADU_T_BOOLEEN;
	 Tradu = vide + "RP" + itoa(CptDeclencheur) + apres(NomRg, "RP");
	 $$ = $$ + " " + Tradu + "_DECLENCHEUR()\n{\nreturn " +
	      $3 + ";\n}\n\n";

	 FichCreerRgPlani << "cReglePlanification* " << Tradu << ";" << endl;
	 FichCreerRgPlani << Tradu << " = new cReglePlanification(";
	 FichCreerRgPlani << Tradu << "_DECLENCHEUR, " << NomRg << "_ALORS, "
                          << TraduReutilisation << ", \"" << NomRg << "("
                          << itoa(CptDeclencheur) << ")\");" << endl;
	 FichCreerRgPlani << "SDPlanificateur.AjouterRegle(" << Tradu << ");"
                          << endl << endl;

	 ListeIndicateur = "";
       }
     | des_declencheurs DECLENCHEUR ':' expr_booleen_indicateur
       { string vide; vide = "";
	 CptDeclencheur++;
         $$ = vide + $1 + TRADU_T_BOOLEEN;
	 Tradu = vide + "RP" + itoa(CptDeclencheur) + apres(NomRg, "RP");
	 $$ = $$ + " " + Tradu + "_DECLENCHEUR()\n{\nreturn " + $4 + " ;\n}\n\n";

	 FichCreerRgPlani << "cReglePlanification* " << Tradu << ";" << endl;
	 FichCreerRgPlani << Tradu << " = new cReglePlanification(";
	 FichCreerRgPlani << Tradu << "_DECLENCHEUR, " << NomRg << "_ALORS, "
                          << TraduReutilisation << ", \"" << NomRg << "("
                          << itoa(CptDeclencheur) << ")\");" << endl;
	 FichCreerRgPlani << "SDPlanificateur.AjouterRegle(" << Tradu
                          << ");" << endl << endl;
	 ListeIndicateur = "";
       }
;

expr_booleen_indicateur : ID_indicateur
       { string vide; vide = "";
	 $$ = vide + "(SIObservation.A(" + TraduIndicateur($1,"") + "))";
         ListeIndicateur = ListeIndicateur + $1 + ","; }
     | '(' expr_booleen_indicateur ')' { $$ = $2; }
     | expr_booleen_indicateur ET expr_booleen_indicateur
       { string vide; vide = "";
	 $$ =  vide + "(" + $1 + " &&\n" + $3 + ")"; }
     | expr_booleen_indicateur OU expr_booleen_indicateur
       { string vide; vide = "";
	 $$ =  vide + "(" + $1 + " ||\n" + $3 + ")"; }
;

corps_rg_planif : un_code_rg_planif
     | '{' suite_code_rg_planif '}' { $$ =  $2; }
;

suite_code_rg_planif : un_code_rg_planif
     | suite_code_rg_planif un_code_rg_planif {string vide; vide = "";
	  $$ = vide + $1 + "\n" + $2; }
;

un_code_rg_planif : directive
     | SI expr_booleen ALORS corps_rg_planif
       { string vide; vide = "";
	 $$ = vide + "if (" + $2 + ") {\n" + $4 + " \n}"; }
     | SI expr_booleen ALORS corps_rg_planif SINON corps_rg_planif
       { string vide; vide = "";
	 $$ = vide + "if (" + $2 + ") {\n" + $4 + "\n}\nelse {\n" + $6 + " \n}"; }
     | affectation    
       {string vide; vide = ""; $$ = vide + $1 + ";"; }
;

directive : un_debut un_du un_au un_faire
       { FichRgPlani << $1 << $2 << $3 << $4;
         string vide; vide = "";
         Tradu = vide + "D";
	 Tradu = Tradu + itoa(CptDirective);
	 $$ = vide + "cDirective* " + Tradu +";\n";
	 $$ = $$ + Tradu + "= new cDirective(";
	 Tradu = vide + NomRg + "_" + Tradu;
	 $$ = $$ + Tradu + "_DEBUT, " + Tradu + "_DU, " + Tradu + "_AU, " +
	       Tradu + "_FAIRE, NULL);\n";
	 $$ = $$ + TraduClasse(NomPlan) + ".AjouterDirective(D" 
              + itoa(CptDirective) + ");";
	 CptDirective++;
       }
     | un_debut un_du un_au un_faire un_fin
       { FichRgPlani << $1 << $2 << $3 << $4 << $5;
         string vide; vide = "";
         Tradu = vide + "D";
	 Tradu = Tradu + itoa(CptDirective);
	 $$ = vide + "cDirective* " + Tradu +";\n";
	 $$ = $$ + Tradu + "= new cDirective(";
	 Tradu = vide + NomRg + "_" + Tradu;
	 $$ = $$ + Tradu + "_DEBUT, " + Tradu + "_DU, " + Tradu + "_AU, " +
	       Tradu + "_FAIRE, " + Tradu + "_FIN);";
	 $$ = $$ + TraduClasse(NomPlan) + ".AjouterDirective(D" 
              + itoa(CptDirective) + ");";
	 CptDirective++;
       }
     | un_du un_au un_faire un_fin
       { FichRgPlani << $1 << $2 << $3 << $4;
         string vide; vide = "";
         Tradu = vide + "D";
	 Tradu = Tradu + itoa(CptDirective);
	 $$ = vide + "cDirective* " + Tradu +";\n";
	 $$ = $$ + Tradu + "= new cDirective(";
	 Tradu = vide + NomRg + "_" + Tradu;
	 $$ = $$ + "NULL, " + Tradu + "_DU, " + Tradu + "_AU, " +
	       Tradu + "_FAIRE, " + Tradu + "_FIN);";
	 $$ = $$ + TraduClasse(NomPlan) + ".AjouterDirective(D" 
              + itoa(CptDirective) + ");";
	 CptDirective++;
       }
     | un_du un_au un_faire
       { FichRgPlani << $1 << $2 << $3;
         string vide; vide = "";
         Tradu = vide + "D";
	 Tradu = Tradu + itoa(CptDirective);
	 $$ = vide + "cDirective* " + Tradu +";\n";
	 $$ = $$ + Tradu + "= new cDirective(";
	 Tradu = vide + NomRg + "_" + Tradu;
	 $$ = $$ + "NULL, " + Tradu + "_DU, " + Tradu + "_AU, " +
	       Tradu + "_FAIRE, NULL);\n";
	 $$ = $$ + TraduClasse(NomPlan) + ".AjouterDirective(D" 
              + itoa(CptDirective) + ");";
	 CptDirective++;
       }
;

un_debut : DEBUT du_code_directive
       { string vide; vide = "";
         $$ = vide + "void " + NomRg + "_D";
         $$ = $$ + itoa(CptDirective) + "_DEBUT()\n{\n" + $2 + "\n}\n\n";
       }
;

un_du : DU expr_nombre
       { string vide; vide = "";
         $$ = vide + "int " + NomRg + "_D";
         $$ = $$ + itoa(CptDirective) + "_DU()\n{\n";
	 if (ListeIndicateur != "") {
	   string temp; temp  = ListeIndicateur;	   
	   $$ = $$ + "if (";
	   while(temp != "") {
	     $$ = $$ + "(SIObservation.Avant(" 
                  + TraduIndicateur(avant(temp, ","), "") + "))";
	     temp = apres(temp, ",");
	     if (temp != "") $$ = $$ + " ||\n";
	   }
	   $$ = $$ + ") return DATE_INCONNUE;\nelse "; 
	 }
	 $$ = $$ + "return (int)" + $2 + ";\n}\n\n";
	 ListeIndicateur = "";
       }
;

un_au : AU expr_nombre
       { string vide; vide = "";
         $$ = vide + "int " + NomRg + "_D";
         $$ = $$ + itoa(CptDirective) + "_AU()\n{\n";
	 if (ListeIndicateur != "") {
	   string temp; temp = ListeIndicateur;	   
	   $$ = $$ + "if (";
	   while(temp != "") {
	     $$ = $$ + "(SIObservation.Avant(" 
                  + TraduIndicateur(avant(temp, ","), "") + "))";
	     temp = apres(temp, ",");
	     if (temp != "") $$ = $$ + " ||\n";
	   }
	   $$ = $$ + ") return DATE_INCONNUE;\nelse "; 
	 }
	 $$ = $$ + "return (int)" + $2 + ";\n}\n\n";
	 ListeIndicateur = "";
       }
;

un_faire : FAIRE du_code_faire
       { string vide; vide = "";
         $$ = vide + "void " + NomRg + "_D";
         $$ = $$ + itoa(CptDirective) + "_FAIRE()\n{\n" + $2 + "\n}\n\n";
       }
;

un_fin : FIN du_code_directive
       { string vide; vide = "";
         $$ = vide + "void " + NomRg + "_D";
         $$ = $$ + itoa(CptDirective) + "_FIN()\n{\n" + $2 + "\n}\n\n";
       }
;

du_code_directive : un_code_directive
     | '{' suite_code_directive '}' { $$ = $2; }
;

suite_code_directive : un_code_directive
     | suite_code_directive un_code_directive 
       { string vide; vide = ""; $$ = vide + $1 + "\n" + $2; }
;

un_code_directive : SI expr_booleen ALORS du_code_directive
       { string vide; vide = "";
         $$ = vide + "if (" + $2 + ") {\n" + $4 + "\n}"; }
     | SI expr_booleen ALORS du_code_directive SINON du_code_directive
       { string vide; vide = "";
         $$ = vide + "if (" + $2 + ") {\n" + $4 + "\n}\nelse {\n" + $6 + "\n}"; }
     | affectation    
       {  string vide; vide = ""; $$ = vide + $1 + ";"; }
;

du_code_faire : affectation_var_deci_planif 
     | '{' suite_code_faire '}' { $$ = $2; }
;

suite_code_faire : affectation_var_deci_planif
     | suite_code_faire affectation_var_deci_planif 
       {  string vide; vide = ""; $$ = vide + $1 + "\n" + $2; }
;

affectation_var_deci_planif : affectation 
       { string vide; vide = "";
         $$ = vide + $1 + ";";
         if (Tradu[1] != C_GLOBAL)
	   NomPlan = Tradu;
       }
;



/* definition des regles operatoires d'une activite */
declaration_rg_opera : ACTIVITE ':' ID_activite suite_rg_opera FIN_ACTIVITE
       { cout << "Activite " << $3 << " : traduction OK" << endl;}
;

suite_rg_opera : une_rg_opera
     | suite_rg_opera une_rg_opera
;

une_rg_opera : RgOPERA ':' ID_regle corps_rg_opera
       { FichCreerRgOpera << NomRg << " = new cRegleOperatoire(" << NomActivite
	                  << ", " << $4 << ", \"" << $3 << "\");\n";
	 ListeIndicateur = "";
	 cout << "Regle operatoire " << $3 << " : traduction OK" << endl;
       }
;

corps_rg_opera : SI expr_booleen ALORS du_code_opera
       { string vide; vide = "";
         $$ = vide + TRADU_T_BOOLEEN;
         $$ = $$ + " " + NomRg + "_SI()\n{\nreturn " + $2 + ";\n}\n\n";
	 $$ = $$ + "void " + NomRg + "_ALORS()\n{\n" + $4 + "\n}\n";
	 FichRgOpera << $$ << endl;
	 $$ = vide + NomRg+"_SI, "+NomRg+"_ALORS, NULL";
       }
     | SI expr_booleen ALORS du_code_opera SINON du_code_opera
       { string vide; vide = "";
         $$ = vide + TRADU_T_BOOLEEN;
         $$ = $$ + " " + NomRg + "_SI()\n{\nreturn " + $2 + ";\n}\n\n";
	 $$ = $$ + "void " + NomRg + "_ALORS()\n{\n" + $4 + "\n}\n\n";
	 $$ = $$ + "void " + NomRg + "_SINON()\n{\n" + $6 + "\n}\n";
	 FichRgOpera << $$ << endl;
	 $$ = vide + NomRg + "_SI, " + NomRg + "_ALORS, " + NomRg + "_SINON";
       }
     | affectation 
       { string vide; vide = "";
         $$ = vide + "void " + NomRg + "_ALORS()\n{\n" + $1 + ";\n}\n";
         FichRgOpera<<$$<<endl;
         $$ = vide + "NULL, "+ NomRg + "_ALORS, NULL";
       }
     | '{' corps_rg_opera '}' { $$ = $2; }
     | '{' suite_code_opera un_code_opera '}'
       { string vide; vide = "";
         $$ = vide + "void " + NomRg + "_ALORS()\n{\n" + $2 + "\n" + $3 + "\n}\n";
         FichRgOpera << $$ << endl;
	 $$ = vide + "NULL, "+ NomRg + "_ALORS, NULL";
       }
;


du_code_opera : un_code_opera
     | '{' suite_code_opera '}' { $$ = $2; }
;

suite_code_opera : un_code_opera
     | suite_code_opera un_code_opera 
       {  string vide; vide = ""; $$ = vide + $1 + "\n" + $2; }
;
 
un_code_opera : SI expr_booleen ALORS du_code_opera
       {  string vide; vide = "";
          $$ = vide + "if (" + $2 + ") {\n" + $4 + "\n}"; }
     | SI expr_booleen ALORS du_code_opera SINON du_code_opera
       {  string vide; vide = "";
          $$ = vide + "if (" + $2 + ") {\n" + $4 + "\n}\nelse {\n" + $6 + "\n}"; }
     | affectation   
       {  string vide; vide = "";
          $$ = vide + $1 + ";"; }
;
%%
/* procedures C annexes */

//----------------------------------------------------------------------------
string TraduValEnum(string nom,string val_enum)
  /* traduction d'une valeur possible val_enum d'un symbole 
     de type T_ENUM ou T_NBRE_ENUM et de nom nom */
{  
  string vide; vide = "";
  string stmp;
  int i = 0;
  val_possible* gp_val_possible = GroupeEnum(nom,lsymbole)->gp_val_possible;

  while((i<NBVALPOSSIBLE)&&(gp_val_possible[i].nom_val != val_enum)) i++;
  if (i<NBVALPOSSIBLE)
    return gp_val_possible[i].tradu_val;
  //else
  stmp = vide + "erreur de traduction d'une valeur enumere `" + val_enum 
         + "' de la variable " + nom;
  yyerror(stmp);
}

//----------------------------------------------------------------------------
string TraduVarConst(string nom)
  /* traduction d'un symbole de type I_VARIABLE ou I_CONSTANTE */
{
  string vide; vide = "";
  string stmp;
  symbole* s;
  s=Symbole(nom,lsymbole);
  string traduction; traduction = vide + TraduClasse(s->type) + "." + s->tradu;
  switch(s->type[1]) {
  case C_SIO : 
    traduction = traduction + "()";
    break;
  case C_FOI : 
    if (s->type[2] == I_VARIABLE)
      traduction = s->tradu;
    break;
  default : ;
  }
  stmp = traduction;
  return stmp;
}

//----------------------------------------------------------------------------
string TraduIndicateur(string nom, string fonction)
  /* traduction d'un symbole de type I_INDICATEUR avec fonction comme appel C++ */
{
  string vide; vide = "";
  string traduction;
  symbole* s;

  s=Symbole(nom,lsymbole);

  if (fonction == "")
     traduction = s->tradu;
  else
     traduction = vide + TraduClasse(s->type) + "." + fonction 
                  + "(" + s->tradu + ")";

  return traduction;
}

//----------------------------------------------------------------------------
string TraduArgObjet(symbole* s,string arg,char t)
  /* traduction d'une fonction C en un appel de methode objet C++ */
{  
  string vide; vide = "";
  string stmp = arg + ",";
  string prec,cour,suiv;
  int i = 0;
  while(((i+3) < s->type.length()) && (s->type[i+3] != t))
    i += 2;
  
  if ((i+3) < s->type.length()) {
    prec = "";
    cour = avant(stmp, ",");
    suiv = apres(stmp, ",");
    while(i>0) {
      prec = prec + cour + ",";
      cour = avant(suiv, ",");
      suiv = apres(suiv, ",");
      i -=2;
    }
    suiv = prec + suiv;

    if ((suiv != "") && (suiv[(suiv.length()-1)] == ','))
      suiv[(suiv.length()-1)] = ')';
    else 
      suiv = suiv + ")";

    if (cour != "") {
      stmp = vide + "(" + cour + ")." + s->tradu + "(" + suiv;
      return stmp;
    }
  }
  // else
    stmp = vide + "erreur de tradu de methode d'objet i=" + itoa(i) 
           + prec + "!" + suiv;
    yyerror(stmp);
}


//----------------------------------------------------------------------------
string TraduFonction(string nom, string arg)
  /* traduction d'un symbole de type I_FONCTION avec arg comme arguments */
{
  string vide; vide = "";
  string stmp;
  symbole* s;
  s=Symbole(nom,lsymbole);
  string traduction;

  switch(s->type[1]) {
  case C_ENS_PARCELLE :  
    traduction = vide + TraduArgObjet(s,arg,T_ENS_PARCELLE);
    break;
  case C_FOI :  
    traduction =  vide + TraduClasse(s->type) + "." + s->tradu;
    break;
  default : 
    traduction =  vide + TraduClasse(s->type) + "." + s->tradu + "(" + arg + ")";
  }
  stmp = traduction;
  return stmp;
}


//----------------------------------------------------------------------------
int CreerIndicateurs()
  /* generation de la fonction void CreerIndicateurs() */
{
  FichIndic << "void CreerIndicateurs()\n{\n// Definition des pointeurs d'indicateur\n";
  FichIndic << "cIndicateur ";
  string tab = "            ";

  lsymbole.Restart();
  while ((lsymbole)&&(lsymbole.Current()->type[2] != I_INDICATEUR))
    lsymbole++;
  
  if (lsymbole) {
    FichIndic << "*" << FaireNomI(lsymbole.Current()->nom);
  
    lsymbole++;

    while (lsymbole) { 
      if (lsymbole.Current()->type[2] == I_INDICATEUR)  {
	FichIndic << " ,\n" << tab << "*" << FaireNomI(lsymbole.Current()->nom);
      }
      lsymbole++;
    }
    
    FichIndic<<" ;\n\n// Creation des indicateurs\n";
    lsymbole.Restart();
    while (lsymbole) { 
      if (lsymbole.Current()->type[2] == I_INDICATEUR)  {
	FichIndic << FaireNomI(lsymbole.Current()->nom) << " = new cIndicateur("
		  << FaireNomI(lsymbole.Current()->nom) << "_CARACTERISTIQUE, \""
		  << lsymbole.Current()->nom<<"\");\n";
      }
      lsymbole++;
    }
    
    FichIndic<<"\n// Memorisation des indicateurs\n";
    lsymbole.Restart();
    while (lsymbole) { 
      if (lsymbole.Current()->type[2] == I_INDICATEUR)  {
	FichIndic << "SIObservation.AjouterIndicateur("
		  << FaireNomI(lsymbole.Current()->nom) << ");\n";
      }
      lsymbole++;
    }
  }
}
  
//----------------------------------------------------------------------------
int CreerFoInterpretation()
  /* generation du debut de la fonction void CreerFoInterpretation() */
{
  string s; 

  FichFoInter << "void CreerFoInterpretation()\n{\n// Definition des pointeurs des fonctions\n";
  lsymbole.Restart();
  while ((lsymbole)&&(lsymbole.Current()->type[1] != C_FOI))
    lsymbole++;
  
  if (lsymbole) {
    s = lsymbole.Current()->type[0];
    FichFoInter << "c" << TraduType1Lettre(s) << "FoInterpretation* "
	        << FaireNomFO(lsymbole.Current()->nom) << ";\n";
  
    lsymbole++;

    while (lsymbole) { 
      if (lsymbole.Current()->type[1] == C_FOI)  {
	s = lsymbole.Current()->type[0];
	FichFoInter << "c" << TraduType1Lettre(s) << "FoInterpretation* "
		    <<FaireNomFO(lsymbole.Current()->nom) << ";\n";
      }
      lsymbole++;
    }
    
    FichFoInter<<"\n// Creation des fonctions\n";
    lsymbole.Restart();
    while (lsymbole) { 
      if (lsymbole.Current()->type[1] == C_FOI)  {
	s = lsymbole.Current()->type[0];
	FichFoInter << FaireNomFO(lsymbole.Current()->nom) << " = new c"
		    << TraduType1Lettre(s) << "FoInterpretation("
		    << FaireNomFO(lsymbole.Current()->nom) << "_CODE, \""
		    << lsymbole.Current()->nom << "\");\n";
      }
      lsymbole++;
    }
    
    FichFoInter<<"\n// Memorisation des fonctions\n";
    lsymbole.Restart();
    while (lsymbole) { 
      if (lsymbole.Current()->type[1] == C_FOI)  {
	s = lsymbole.Current()->type[0];
	FichFoInter << "SIObservation." << TraduType1Lettre(s)
		    << "AjouterFo(" << FaireNomFO(lsymbole.Current()->nom) 
                    << ");\n";
      }
      lsymbole++;
    }
  }
}
  
//----------------------------------------------------------------------------
int DeclareRgOpera()
  /* Definition des pointeurs des regles operatoires */
{
  string vide; vide = "";

  string DecRgOp;
  DecRgOp = vide + "cRegleOperatoire ";
  string tab     = "                 ";
  lsymbole.Restart();
  while ((lsymbole)&&(! ((lsymbole.Current()->type[0] == T_REGLE) &&
		      (lsymbole.Current()->type[1] == C_SDO)))) 
    lsymbole++;
  
  if (lsymbole) {
    DecRgOp = DecRgOp + "*" + lsymbole.Current()->tradu;
  
    lsymbole++;

    while (lsymbole) { 
      if ((lsymbole.Current()->type[0] == T_REGLE) &&
	 (lsymbole.Current()->type[1] == C_SDO))  {
	DecRgOp = DecRgOp + " ,\n" + tab + + "*" + lsymbole.Current()->tradu;
      }
      lsymbole++;
    }
    
    DecRgOp =  DecRgOp + " ;\n";
    FichCreerRgOpera << "// Definition des pointeurs des regles operatoires" 
                     << endl;
    FichCreerRgOpera << DecRgOp << endl;
    FichCreerRgOpera << "// Creation des regles operatoires" << endl;
  }
}
  
//----------------------------------------------------------------------------
int MemoRgOpera()
  /* memorisation des regles operatoires */
{
  lsymbole.Restart();
  while ((lsymbole)&&(! ((lsymbole.Current()->type[0] == T_REGLE) &&
			 (lsymbole.Current()->type[1] == C_SDO)))) 
    lsymbole++;
  
  if (lsymbole) {
    FichCreerRgOpera << endl << "// Memorisation des regles operatoires" << endl;
    FichCreerRgOpera << "SDOperatoire.AjouterRegle("
	             << lsymbole.Current()->tradu << ");\n";
  
    lsymbole++;

    while (lsymbole) { 
      if ((lsymbole.Current()->type[0] == T_REGLE) &&
	 (lsymbole.Current()->type[1] == C_SDO))  {
	FichCreerRgOpera << "SDOperatoire.AjouterRegle("
		         << lsymbole.Current()->tradu << ");\n";
      }
      lsymbole++;
    }
  }
}
  
//----------------------------------------------------------------------------
int Entete_strategie_h()
  /* entete du fichier FICH_STRAT_H */
{
    FichStrat_H << "/*******************************************************";
    FichStrat_H << "\n\tfichier : Strategie.h";
    FichStrat_H << "\n\tcontenu : definition d'enumeration :";
    FichStrat_H << "\n\t\tdes variables utilisateur du systeme de planification";
    FichStrat_H << "\n\t\tdes variables utilisateur du systeme operatoire";
    FichStrat_H << "\n\t\tdes fonctions d'interpretations";
    FichStrat_H << "\n\t\tet des indicateurs";
    FichStrat_H << "\n\tauteur : genere automatiquement par le traducteur";
    FichStrat_H << "\n******************************************************/\n\n";
    FichStrat_H << "#ifndef _STRATEGIE_H\n#define _STRATEGIE_H\n\n";
}
  
//----------------------------------------------------------------------------
int TraduEnumVarSDP()
  /* declaration du type enumere VARIABLE_SDPLANIFICATEUR */
{
  string vide; vide = "";

  string TVarSDP;
  TVarSDP = vide + "enum VARIABLE_SDPLANIFICATEUR { ";
  string tab     = "                                ";

  lsymbole.Restart();
  while ((lsymbole)&&(! ((lsymbole.Current()->nom[0] == '%') &&
			 (lsymbole.Current()->type[1] == C_VARU_SDP)))) 
    lsymbole++;
  
  if (lsymbole) {
    TVarSDP = TVarSDP + FaireEnumVSDP(lsymbole.Current()->nom);
    FichStrat_H << TVarSDP;
  
    lsymbole++;

    while (lsymbole) { 
      if ((lsymbole.Current()->nom[0] == '%') &&
	  (lsymbole.Current()->type[1] == C_VARU_SDP))  {
	TVarSDP = vide + " ,\n" + tab + FaireEnumVSDP(lsymbole.Current()->nom);
	FichStrat_H << TVarSDP;
      }
      lsymbole++;
    }
    
    TVarSDP = vide + " ,\n" + tab + "NB_VARIABLE_SDPLANIFICATEUR };\n";
    FichStrat_H << TVarSDP << endl;
  }
}
  
//----------------------------------------------------------------------------
int TraduEnumVarSDO()
  /* declaration du type enumere VARIABLE_SDOPERATOIRE */
{
  string vide; vide = "";

  string TVarSDO;
  TVarSDO = vide + "enum VARIABLE_SDOPERATOIRE { ";
  string tab     = "                             ";

  lsymbole.Restart();
  while ((lsymbole)&&(! ((lsymbole.Current()->nom[0] == '%') &&
			 (lsymbole.Current()->type[1] == C_VARU_SDO)))) 
    lsymbole++;
  
  if (lsymbole) {
    TVarSDO = TVarSDO + FaireEnumVSDO(lsymbole.Current()->nom);
    FichStrat_H << TVarSDO;
  
    lsymbole++;

    while (lsymbole) { 
      if ((lsymbole.Current()->nom[0] == '%') &&
	  (lsymbole.Current()->type[1] == C_VARU_SDO))  {
	TVarSDO = vide + " ,\n" + tab + FaireEnumVSDO(lsymbole.Current()->nom);
	FichStrat_H << TVarSDO;
      }
      lsymbole++;
    }
    
    TVarSDO = vide + " ,\n" + tab + "NB_VARIABLE_SDOPERATOIRE };\n";
    FichStrat_H << TVarSDO << endl;
  }
}
  
//----------------------------------------------------------------------------
int TraduEnumFonctionInter()
  /* declaration du type enumere FO_INTERPRETATION */
{
  string vide; vide = "";
  string s;
  string tab  = "                          ";
  string tab_enum[5];

  lsymbole.Restart();
  while (lsymbole) { 
    if (lsymbole.Current()->type[1] == C_FOI) {
      switch(lsymbole.Current()->type[0]) {
      case T_BOOLEEN : 
	if (tab_enum[0] == "") 
	  tab_enum[0] = FaireEnumFO(lsymbole.Current()->nom, lsymbole.Current()->type);
	else
	  tab_enum[0] = tab_enum[0] + " ,\n" + tab + FaireEnumFO(lsymbole.Current()->nom,lsymbole.Current()->type);break;
      case T_NOMBRE : 
	if (tab_enum[1] == "") 
	  tab_enum[1] = FaireEnumFO(lsymbole.Current()->nom,lsymbole.Current()->type);
	else
	  tab_enum[1] = tab_enum[1] + " ,\n" + tab + FaireEnumFO(lsymbole.Current()->nom,lsymbole.Current()->type);break;
      case T_CHAINE :
	if (tab_enum[2] == "")
	  tab_enum[2] = FaireEnumFO(lsymbole.Current()->nom,lsymbole.Current()->type);
	else
	  tab_enum[2] = tab_enum[2] + " ,\n" + tab + FaireEnumFO(lsymbole.Current()->nom,lsymbole.Current()->type);break;
      case T_PARCELLE : 
	if (tab_enum[3] == "")
	  tab_enum[3] = FaireEnumFO(lsymbole.Current()->nom,lsymbole.Current()->type);
	else
	  tab_enum[3] = tab_enum[3] + " ,\n" + tab + FaireEnumFO(lsymbole.Current()->nom,lsymbole.Current()->type);break;
      case T_ENS_PARCELLE : 
	if (tab_enum[4] == "")
	  tab_enum[4] = FaireEnumFO(lsymbole.Current()->nom,lsymbole.Current()->type);
	else
	  tab_enum[4] = tab_enum[4] + " ,\n" + tab + FaireEnumFO(lsymbole.Current()->nom,lsymbole.Current()->type);break;
      }  
    }
    lsymbole++;
  }
  
//  if (tab_enum[0] != "")
 {
   s = vide + T_BOOLEEN;
    FichStrat_H << "\nenum " << TraduType1Lettre(s) << "FO_INTERPRETATION { ";
    FichStrat_H << tab_enum[0];
    if (tab_enum[0] == "") FichStrat_H << " \n";
    else FichStrat_H << " ,\n";
    FichStrat_H << tab << "NB_"<< TraduType1Lettre(s) << "FO_INTERPRETATION };\n";
  }
  
//  if (tab_enum[1] != "")
 {
   s = vide + T_NOMBRE;
    FichStrat_H << "\nenum " << TraduType1Lettre(s) << "FO_INTERPRETATION { ";
    FichStrat_H << tab_enum[1];
    if (tab_enum[1] == "") FichStrat_H << " \n" ;
    else  FichStrat_H << " ,\n" ;
    FichStrat_H << tab << "NB_"<<TraduType1Lettre(s) << "FO_INTERPRETATION };\n";
  }

//  if (tab_enum[2] != "")
 {
   s = vide + T_CHAINE;
    FichStrat_H << "\nenum " << TraduType1Lettre(s) << "FO_INTERPRETATION { ";
    FichStrat_H << tab_enum[2];
    if (tab_enum[2] == "") FichStrat_H << " \n" ;
    else  FichStrat_H << " ,\n" ;
    FichStrat_H << tab << "NB_" << TraduType1Lettre(s) << "FO_INTERPRETATION };\n";
  }

//  if (tab_enum[3] != "")
 {
   s = vide + T_PARCELLE;
    FichStrat_H << "\nenum " << TraduType1Lettre(s) << "FO_INTERPRETATION { ";
    FichStrat_H<<tab_enum[3];
    if (tab_enum[3] == "") FichStrat_H << " \n" ;
    else  FichStrat_H << " ,\n" ;
    FichStrat_H << tab << "NB_" << TraduType1Lettre(s) << "FO_INTERPRETATION };\n";
  }
  
//  if (tab_enum[4] != "")
 {
   s = vide + T_ENS_PARCELLE;
    FichStrat_H << "\nenum " << TraduType1Lettre(s) << "FO_INTERPRETATION { ";
    FichStrat_H<<tab_enum[4];
    if (tab_enum[4] == "") FichStrat_H << " \n" ;
    else  FichStrat_H << " ,\n" ;
    FichStrat_H << tab << "NB_"<< TraduType1Lettre(s) << "FO_INTERPRETATION };\n";
  }
  FichStrat_H << endl;
}

  
//----------------------------------------------------------------------------
int TraduEnumIndicateur()
  /* enumeration C++ des inidicateurs */
{
string vide; vide = "";
  /* declaration du type enumere INDICATEUR */
  string TIndi;  TIndi = vide + "enum INDICATEUR { ";
  string tab;    tab   = vide + "                  ";

  lsymbole.Restart();
  while ((lsymbole)&&(lsymbole.Current()->type[2] != I_INDICATEUR)) 
    lsymbole++;
  
  if (lsymbole) {
    TIndi =  TIndi + FaireEnumI(lsymbole.Current()->nom);
    FichStrat_H << TIndi;
  
    lsymbole++;

    while (lsymbole) { 
      if (lsymbole.Current()->type[2] == I_INDICATEUR) {
	TIndi = vide + " ,\n" + tab + FaireEnumI(lsymbole.Current()->nom);
	FichStrat_H << TIndi;
      }
      lsymbole++;
    }
    
    TIndi = vide + " ,\n" + tab + "NB_INDICATEUR };\n";
    FichStrat_H << TIndi << endl;
  }
}
  
//----------------------------------------------------------------------------
void ConCatFich(ofstream &fich_dest,char* nom_source)
  /* ajoute dans le fichier ouvert fich_dest ( a la fin), 
    le contenu du fichier de nom nom_source */
{
  ifstream fich_source(nom_source,ios::in);
  char c;
  while (fich_source) {
    fich_source.get(c);
    fich_dest<<c;
  }
  fich_source.close();
}

//----------------------------------------------------------------------------
void TabuFich(char* nom)
  /* tabulation (base sur les { et }) du fichier de nom nom */
{
  char c;
  ifstream fich_source(nom,ios::in);
  ofstream fich_tmp(FICH_TMP,ios::out);
  string tabu; tabu = "";
  bool TABU = false;
  while (fich_source) {
    fich_source.get(c);
    switch(c) {
    case '\n' : fich_tmp<<c;
      TABU=true;
      break;
    case '{'  : 
      if (TABU) fich_tmp<<tabu;
      fich_tmp<<c;
      tabu = tabu + " ";
      TABU = false;
      break;
    case '}'  : 
      tabu = tabu.substr(1, tabu.size());
      if (TABU) fich_tmp<<tabu;
      fich_tmp<<c;
      TABU = false;
      break;
    default : 
      if (TABU) fich_tmp<<tabu;
      fich_tmp<<c;
      TABU = false;
    }
  }
  fich_source.close();
  fich_tmp.close();
  fich_tmp.open(nom,ios::out);
  ConCatFich(fich_tmp,FICH_TMP);
  fich_tmp.close();
}

//----------------------------------------------------------------------------
/* si tout se passe bien ecriture d'un fichier vide : OK traduction */
int main(int argc, char* *argv)
{
  printf("TRADUCTION DE LA STRATEGIE\n");
  /* reaffecter le descripteur du fichier d'entree `yyin' de lex */
  FILE* pfich;
  ifstream FichTmpIn;
  ofstream FichTmpOut;
  pfich = fopen(*(argv+1),"r");
  if (pfich != NULL)
    {
      yyin = pfich;

      /* ouverture des fichiers en ecritures */
      FichIndic.open(FICH_INDIC, ios::out);
      FichFoInter.open(FICH_FO_INTER, ios::out);
      FichRgPlani.open(FICH_RG_PLANI, ios::out);
      FichCreerRgPlani.open(FICH_CREER_RG_PLANI, ios::out);
      FichRgOpera.open(FICH_RG_OPERA, ios::out);
      FichCreerRgOpera.open(FICH_CREER_RG_OPERA, ios::out);
      FichStrat_H.open(FICH_STRAT_H, ios::out);
      
      /* copie du fichier FICH_ENTETE au debut des fichiers .cc */
      ConCatFich(FichIndic,FICH_ENTETE);
      ConCatFich(FichIndic,FICH_ENTETE_INDIC);
      ConCatFich(FichFoInter,FICH_ENTETE);
      ConCatFich(FichRgPlani,FICH_ENTETE);
      ConCatFich(FichRgOpera,FICH_ENTETE);
      
      if (! lire_dico_enum(lgp_enum,DICO_ENUM_IN)) 
	cout << "erreur de lecture du dictionnaire de groupe enumere" << endl;
      else
	if (! lire_librairie(lsymbole,lgp_enum,LIB_OUT)) 
	  cout << "erreur de lecture de librairie" << endl;
	else
	  {
	    FichCreerRgPlani << "void CreerReglesPlanification()\n{\n";
	    
	    FichCreerRgOpera << "void CreerReglesOperatoires()\n{\n";
	    /* ecriture du fichier d'enum : FICH_STRAT_H */
	    Entete_strategie_h();
	    TraduEnumVarSDP();
	    TraduEnumVarSDO();
	    TraduEnumFonctionInter();
	    TraduEnumIndicateur();
	    FichStrat_H << "\n#endif\n";
	    FichStrat_H.close();
	    
	    DeclareRgOpera();
	    if (! yyparse())
	      {
		CreerIndicateurs();
		FichIndic << "}\n";
		FichIndic.close();
		TabuFich(FICH_INDIC);
		
		CreerFoInterpretation();
		FichFoInter << "}\n";
		FichFoInter.close();
		TabuFich(FICH_FO_INTER);
		
		FichCreerRgPlani << "}\n";
		FichCreerRgPlani.close();
		ConCatFich(FichRgPlani,FICH_CREER_RG_PLANI);
		FichRgPlani.close();
		TabuFich(FICH_RG_PLANI);
		
		MemoRgOpera();
		FichCreerRgOpera << "}\n";
		FichCreerRgOpera.close(); 
		ConCatFich(FichRgOpera,FICH_CREER_RG_OPERA);
		FichRgOpera.close();
		TabuFich(FICH_RG_OPERA);
		
		printf("\n\n");
		ofstream fresultat;
		fresultat.open("OKtraduction", ios::out);
		fresultat.close();
	      };
	  };
    };
  return 0;
}

