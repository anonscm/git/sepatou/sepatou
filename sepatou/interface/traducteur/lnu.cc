// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/*-----------------------------------------------------------------
 * fichier : lnu.cc
 * contenu : corps des fonctions de manipulation de la liste de symbole
 *           et d'un symbole
 *-----------------------------------------------------------------*/

#include "lnu.h"

extern void yyerror(string s);/*fo definie dans lex_verif.l ou dans lex_tradu.l*/

//---------------------------------------------------------------------
string itoa(int i) 
  /* convertie l'entier i en string */
{
  char c[10];
  sprintf(c,"%d",i);
  return c;
}

//---------------------------------------------------------------------
string Type(string nom, CListe<symbole> &ls)
  /* retourne le type du symbole */
{
  string vide; vide = "";
  string stmp;

  ls.Restart();
  while( (ls) && (ls.Current()->nom != nom) ) ls++;
  if (ls)
    return ls.Current()->type;
  // else
  stmp = vide + "recherche de type : echec, symbole `" + nom + "' inconnu";
  yyerror(stmp);
}

//---------------------------------------------------------------------
gp_enum* GroupeEnum(string nom, CListe<symbole> &ls)
  /* retourne le groupe enum du symbole (nom) */
{
  string vide; vide = "";
  string stmp;

  ls.Restart();
  while( (ls) && (ls.Current()->nom != nom) ) ls++;
  if (ls) return ls.Current()->groupe_enum;
  // else
  stmp = vide + "recherche de groupe enum : echec, symbole `" + nom + "' inconnu";
  yyerror(stmp);
}

//---------------------------------------------------------------------
symbole* Symbole(string nom, CListe<symbole> &ls)
  /* retourne le symbole */
{
  string vide; vide = "";
  string stmp;

  ls.Restart();
  while( (ls) && (ls.Current()->nom !=nom) ) ls++;
  if (ls)
    return ls.Current();
  // else
  stmp = vide + "recherche de type : echec, symbole `" + nom + "' inconnu";
  yyerror(stmp);
}

//---------------------------------------------------------------------
int VerifGpEnum(string nom, string chaine, CListe<symbole> &ls)
{
  int i = 0;
  if (chaine != "") {
    val_possible* gp_val_possible = GroupeEnum(nom,ls)->gp_val_possible;
    while( (i < NBVALPOSSIBLE) && (gp_val_possible[i].nom_val != chaine) ) i++;
    return (i < NBVALPOSSIBLE);
  }
  // else
  return 0;
}

//---------------------------------------------------------------------
string DelSym(string nom, CListe<symbole> &ls)
{
  int i = 1;

  ls.Restart();
  while( (ls) && (ls.Current()->nom != nom) ) {
    ls++;
    i++;
  }
  if (ls)  {
    ls.Del(i);
    return nom;
  }
  //else
  return "";
}

//---------------------------------------------------------------------
string AddSym(string nom,CListe<symbole> &ls)
  /* ajoute un symbole (de type inconnu) */
{
  ls.Restart();
  while( (ls) && (ls.Current()->nom != nom) ) ls++;
  if (ls)
    return ls.Current()->type;
  //else 
  symbole* ps = new symbole;
  if (ps != NULL) {
    ps->nom = nom;
    ps->type = "";
    ps->tradu = "";
  
    if (ls.AddLast(ps))
      return "";
  }
  // else
  yyerror("ajout de symbole impossible");
}

//---------------------------------------------------------------------
string MaJType(CListe<symbole> &ls, string nom, string tradu, char type, char classe, char t_ind, string arg)
  /* mise a jour du type du symbole
     si symbole.nom existe, si il n'a pas de type alors 
     symbole.type = type et on retourne son type
     sinon si symbole.type[1] = ? alors si symbole.type[0] = type[0] 
     et on retourne symbole.type sinon erreur :
     (le symbole.nom n'existe pas ou erreur de type)
     */
{
  string vide; vide = "";
  string t,msg;
  int i;

  t = vide + type + classe + t_ind;
  i = 0;
  while (i<arg.length()) {
    t = t + arg[i];
    t = t + P_OBLIGATOIRE;
    i++;
  }

  ls.Restart();
  while( (ls) && (ls.Current()->nom != nom) )
    ls++;
  if (ls) {
    if (ls.Current()->type.length() == 0) {
      ls.Current()->tradu = tradu;
      ls.Current()->type = t;
      return ls.Current()->type;
    }
    else
      if ((ls.Current()->type[2] == I_INCONNU)&&
	  ((ls.Current()->type[0] == T_INCONNU) || 
	   (ls.Current()->type[0] == t[0]))) {
	ls.Current()->tradu = tradu;
	ls.Current()->type = t;
	return ls.Current()->type;
      }
      else {
	msg = vide + nom + " est du type " + ls.Current()->type + " et non du type " + t;
	yyerror(msg);
      }
  }
  else {
    msg = vide + nom + " ne peut etre declaree";
    yyerror(msg);
  }
}

//---------------------------------------------------------------------
string TraduType(string type)
{
  string vide; vide = "";
  string stmp;
  switch(type[0]) {
  case T_BOOLEEN : return TRADU_T_BOOLEEN;
  case T_NOMBRE_INT : return TRADU_T_NOMBRE_INT;
  case T_NOMBRE : return TRADU_T_NOMBRE;
  case T_CHAINE : return TRADU_T_CHAINE;
  case T_PARCELLE : return TRADU_T_PARCELLE;
  case T_ENS_PARCELLE : return TRADU_T_ENS_PARCELLE;
  default : stmp = vide + "traduction du type " + type + " impossible";
    yyerror(stmp);
  }
}

//---------------------------------------------------------------------
string TraduType1Lettre(string type)
{
  string vide; vide = "";
  string stmp;
  switch(type[0]) {
  case T_BOOLEEN : return "b";
  case T_NOMBRE_INT : return "i";
  case T_NOMBRE : return "f";
  case T_CHAINE : return "s";
  case T_PARCELLE : return "p";
  case T_ENS_PARCELLE : return "e";
  default : stmp = vide + "traduction du type " + type + " impossible";
    yyerror(stmp);
  }
}

//---------------------------------------------------------------------
string TraduNom(string nom, CListe<symbole> &ls)
  /* retourne la traduction (tradu) d'un symbole (nom)*/
{
  string vide; vide = "";
  string stmp;

  ls.Restart();
  while( (ls) && (ls.Current()->nom !=nom) ) ls++;
  if (ls)
    return ls.Current()->tradu;
  // else
  stmp = vide + "Tradu de " + nom + " : impossible";
  yyerror(stmp);
}

//---------------------------------------------------------------------
string TraduClasse(string type)
  /* retourne la traduction de la classe (type[1]) du type */
{
  string stmp;
  switch(type[1]) {
  case C_PLAN_CONCENTREMAIS : 
    stmp = "PlanAlimentationConservee";
    break;
  case C_PLAN_PATURAGE : 
    stmp = "PlanPaturage";
    break;
  case C_PLAN_FAUCHE : 
    stmp = "PlanFauche"; 
    break;
  case C_PLAN_COORDINATION : 
    stmp = "PlanCoordination";
    break;
  case C_PLAN_AZOTE : 
    stmp = "PlanFertilisation";
    break;
  case C_SDP : 
    stmp = "SDPlanificateur";
    break;
  case C_SDO : 
    stmp = "SDOperatoire";
    break;
  case C_SIO : 
    stmp = "SIObservation";
    break;
  case C_SIS : 
    stmp = "SISurveillance";
    break;
  case C_TEMPS : 
    stmp = "Temps";
    break;
  case C_FOI : 
    stmp = "SIObservation";
    break;
  case C_VARU_SDP : 
    stmp = "SDPlanificateur";
    break;
  case C_VARU_SDO : 
    stmp = "SDOperatoire";
    break;
  default :
    stmp = "traduction de la classe ";
     stmp += type[1];
    stmp += " impossible";
    yyerror(stmp);
  }
  return stmp;
}

//---------------------------------------------------------------------
string FaireEnum(string nom)
{
  string stmp = nom;
  if ((nom[0] == '!') ||
      (nom[0] == '?') ||
      (nom[0] == '%'))
    stmp = stmp.substr(1,stmp.size());
  return stmp;
}

//---------------------------------------------------------------------
string FaireTradu(string nom)
{
  string stmp = nom;
  if ((nom[0] == '!') ||
      (nom[0] == '?') ||
      (nom[0] == '%'))
    stmp = stmp.substr(1,stmp.size());
  return stmp;
}

//---------------------------------------------------------------------
string FaireEnumFO(string nom_utilisateur,string type)
{
  string vide; vide = "";
  string stmp = vide +"ENUM_"+ TraduType1Lettre(type) + "FO_" + FaireEnum(nom_utilisateur);
  return stmp;
}

//---------------------------------------------------------------------
string FaireNomFO(string nom_utilisateur)
{
  string vide; vide = "";
  string stmp = vide + "FO_" + FaireTradu(nom_utilisateur);
  return stmp;
}

//---------------------------------------------------------------------
string FaireEnumI(string nom_utilisateur)
{
  string vide; vide = "";
  string stmp = vide + "ENUM_I_" + FaireEnum(nom_utilisateur);
  return stmp;
}

string FaireNomI(string nom_utilisateur)
{
  string vide; vide = "";
  string stmp = vide + "I_" + FaireTradu(nom_utilisateur);
  return stmp;
}

//---------------------------------------------------------------------
string FaireNomRP(string nom_utilisateur)
{
  string vide; vide = "";
  string stmp = vide + "RP_" + FaireTradu(nom_utilisateur);
  return stmp;
}

//---------------------------------------------------------------------
string FaireNomRO(string nom_utilisateur)
{
  string vide; vide = "";
  string stmp = vide + "RO_" + FaireTradu(nom_utilisateur);
  return stmp;
}

//---------------------------------------------------------------------
string FaireEnumVSDP(string nom_utilisateur)
{
  string vide; vide = "";
  string stmp = vide + "ENUM_VSDP_" + FaireEnum(nom_utilisateur);
  return stmp;
}

//---------------------------------------------------------------------
string FaireEnumVSDO(string nom_utilisateur)
{
  string vide; vide = "";
  string stmp = vide + "ENUM_VSDO_" + FaireEnum(nom_utilisateur);
  return stmp;
}

//---------------------------------------------------------------------
string FaireNomVFOI(string nom)
{
  string stmp = FaireTradu(nom);
  return stmp;
}

//---------------------------------------------------------------------
string avant (string s, string motif)
{
int r;
r = s.find(motif);
if ( r >= 0 && r <= s.size() )
  return s.substr(0, r);
else
  return s;
}


//---------------------------------------------------------------------
string apres (string s, string motif)
{
int r;
r = s.find(motif);
if ( r >= 0 && r <= s.size() )
  return s.substr(r+1, s.size());
else
  return "";
}


