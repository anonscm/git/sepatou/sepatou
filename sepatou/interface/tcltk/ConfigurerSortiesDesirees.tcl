# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerSortiesDesirees.tcl
# Contenu : Description les parametres de sortie du simulateur
######################################################################

######################################################################
#Procedure : Desc_Sortie                                             #
#Description : Cree la fenetre chargee de decrire les parametres de  #
#              sortie du simulateur                                  #
######################################################################
proc Desc_Sortie {} {
    
global LOGICIEL 
global VAR_SORTIES_POSSIBLES

global b_trace
global b_chroniq
global b_statistiques
global list_var
global list_var_retenues

set list_var_retenues ""

if {[winfo exists .wm_Sortie] == 0} {
    toplevel .wm_Sortie

    #Titre de la fenetre
    wm title .wm_Sortie "$LOGICIEL : Description des sorties desirees"

    ### Zone de commandes des boutons Trace et Chronique ####################
    frame .wm_Sortie.f_top 

    #Initialisation de l'etat des boutons (0 : non selectionne)
    set b_trace 0
    set b_chroniq 0
    set b_statistiques 0

    #Creation des boutons
    button .wm_Sortie.f_top.b_trace -text "Trace d�cisions" \
	-command "Reflexe_trace .wm_Sortie.f_top.b_trace "
    button .wm_Sortie.f_top.b_chroniq -text "Chronique d'actions" \
	-command "Reflexe_chroniq .wm_Sortie.f_top.b_chroniq "
    button .wm_Sortie.f_top.b_statistiques -text "STATISTIQUES" \
	-command "Reflexe_statistiques .wm_Sortie.f_top.b_statistiques "

    pack .wm_Sortie.f_top.b_trace .wm_Sortie.f_top.b_chroniq \
	.wm_Sortie.f_top.b_statistiques \
	-pady 10 -side top -expand 1 -anchor w -fill x
    pack .wm_Sortie.f_top -side top -expand 1


    ### Zone de commandes des boutons ok et annuler ########################
    frame .wm_Sortie.f_bottom -borderwidth 3

    #Creation des boutons
    button .wm_Sortie.f_bottom.b_ok -text Ok \
	-command "Enregistrer_Sortie"
    button .wm_Sortie.f_bottom.b_cancel -text Annuler -command "destroy .wm_Sortie" 

    pack .wm_Sortie.f_bottom.b_ok .wm_Sortie.f_bottom.b_cancel \
	-padx 20 -pady 10 -side left -expand 1
    pack .wm_Sortie.f_bottom -side bottom -anchor s -fill x -pady 5

    ### Separateur
    frame .wm_Sortie.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack .wm_Sortie.f_sep -side bottom -fill x -pady 5


    ### Zone de selection des variables ####################################
    frame .wm_Sortie.f_zone
    pack .wm_Sortie.f_zone -side top -padx 20 -expand 1 -anchor n

    # variables possibles
    frame .wm_Sortie.f_zone.f_listVar
    label .wm_Sortie.f_zone.f_listVar.l_varPoss -text "Variables possibles"
    set list_var [ScrolledListbox .wm_Sortie.f_zone.f_listVar.scrlst_listVar \
		      -width 20 -height 15 -setgrid true]

    pack .wm_Sortie.f_zone.f_listVar.l_varPoss \
	.wm_Sortie.f_zone.f_listVar.scrlst_listVar \
	-side top -anchor w -pady 5

    # variables retenues
    frame .wm_Sortie.f_zone.f_listRet
    label .wm_Sortie.f_zone.f_listRet.l_varRet -text "Variables retenues"

    #Creation de la scrolled List des variables retenues
    set list_var_retenues \
	[ScrolledListbox .wm_Sortie.f_zone.f_listRet.scrlst_listSelectVar \
	     -width 20 -height 15 -setgrid true]

    pack  .wm_Sortie.f_zone.f_listRet.l_varRet \
	.wm_Sortie.f_zone.f_listRet.scrlst_listSelectVar \
	-side top -anchor w -pady 5

    #Creation des boutons servant a transferrer les donnees d'une liste a l'autre
    frame .wm_Sortie.f_zone.f_contener

    button .wm_Sortie.f_zone.f_contener.b_-> -text "->" \
	-command {InsertVar $list_var  $list_var_retenues \
		      .wm_Sortie.f_zone.f_contener.b_-> $VAR_SORTIES_POSSIBLES} \
	-state disabled
    button .wm_Sortie.f_zone.f_contener.b_<- -text "<-" \
	-command {InsertVar $list_var_retenues  $list_var \
		      .wm_Sortie.f_zone.f_contener.b_<- $VAR_SORTIES_POSSIBLES} \
	-state disabled

    pack .wm_Sortie.f_zone.f_contener.b_-> \
	.wm_Sortie.f_zone.f_contener.b_<- \
	-side top -pady 5

    bind $list_var <ButtonPress-1> {ListSelectStart $list_var %y}

    bind $list_var <B1-Motion> {ListSelectExtend $list_var %y}

    bind $list_var <ButtonRelease-1> \
	{.wm_Sortie.f_zone.f_contener.b_-> configure -state normal}

    bind $list_var_retenues <ButtonPress-1> \
	{ListSelectStart $list_var_retenues %y}

    bind $list_var_retenues <B1-Motion> \
	{ListSelectExtend  $list_var_retenues %y}

    bind $list_var_retenues <ButtonRelease-1> \
	{.wm_Sortie.f_zone.f_contener.b_<- configure -state normal}

    pack .wm_Sortie.f_zone.f_listVar\
	.wm_Sortie.f_zone.f_contener \
	.wm_Sortie.f_zone.f_listRet \
	-side left -padx 10 -pady 20 -expand 1


    ### Chargement des donnees ##########################################
    Charger_Dft_Sortie
}

}
######################################################################
#Procedure : Charger_Dft_Sortie                       
#Description : Charge les variables de sorties 
######################################################################
proc Charger_Dft_Sortie {} {

    global DIR_TEMPORAIRE
    global FIC_SORTIES_TXT
    global VAR_SORTIES_POSSIBLES
    global FIC_TRACE
    global FIC_CHRONIQUE

    global b_trace
    global b_chroniq
    global b_statistiques
    global list_var
    global list_var_retenues

    #Ouverture du fichier de chargement
    if {[catch {set file [open "${DIR_TEMPORAIRE}$FIC_SORTIES_TXT" r]}] == 0} {
	seek $file 0 start

	gets $file line
	while {[eof $file] != 1} {
	    if {[string compare $FIC_TRACE $line] == 0 } {
		# Trace selectionnee 
		.wm_Sortie.f_top.b_trace configure -relief sunken
		set b_trace 1
	    } elseif {[string compare $FIC_CHRONIQUE $line] == 0} {
		# chronique selectionnee
		.wm_Sortie.f_top.b_chroniq configure -relief sunken
		set b_chroniq 1
	    } elseif {[string compare STATISTIQUES $line] == 0} {
		# statistiques selectionnees
		.wm_Sortie.f_top.b_statistiques configure -relief sunken
		set b_statistiques 1
	    } elseif {[string first $line $VAR_SORTIES_POSSIBLES] != -1 } {
		$list_var_retenues insert end $line
	    } else {
		puts "ERREUR : une variable du fichier $FIC_SORTIES_TXT n'est pas permise"
	    }
	    gets $file line
	}
    }

    # mise a jour des variables possibles non deja retenues
    set cpt [eval [list $list_var_retenues size]]
    for  {set i 0} {$i < [llength $VAR_SORTIES_POSSIBLES]} {incr i} {
	set trouve 0
	set val [lindex $VAR_SORTIES_POSSIBLES $i]

	for  {set j 0} {$j < $cpt} {incr j} {
	    if {[string compare [$list_var_retenues get $j] $val] == 0 } {
		set trouve 1
	    }
	}
	if {$trouve == 0} {
	    $list_var insert end $val
	}
    }
}

######################################################################
#Procedure : Reflexe_trace                                           #
#Description : Procedure qui permet de changer l'etat du bouton      #
#              trace                                                 #
#parametre : bouton : nom du bouton                                  #
######################################################################
proc Reflexe_trace {bouton} {

    global b_trace

    if {$b_trace == 0} { 
	$bouton configure -relief sunken
	set b_trace 1
    } elseif {$b_trace == 1} {
	$bouton configure -relief raised
	set b_trace 0
    } 
}

######################################################################
#Procedure : Reflexe_chroniq                                         #
#Description : Procedure qui permet de changer l'etat du bouton      #
#              chronique d'actions                                   #
#parametre : bouton : nom du bouton                                  #
######################################################################
proc Reflexe_chroniq {bouton} {

    global b_chroniq

    if {$b_chroniq == 0} { 
	$bouton configure -relief sunken
	set b_chroniq 1
    } elseif {$b_chroniq == 1} {
	$bouton configure -relief raised
	set b_chroniq 0
    } 
}

######################################################################
#Procedure : Reflexe_statistiques                                    #
#Description : Procedure qui permet de changer l'etat du bouton      #
#              STATISTIQUES                                          #
#parametre : bouton : nom du bouton                                  #
######################################################################
proc Reflexe_statistiques {bouton} {

    global b_statistiques

    if {$b_statistiques == 0} { 
	$bouton configure -relief sunken
	set b_statistiques 1
    } elseif {$b_statistiques == 1} {
	$bouton configure -relief raised
	set b_statistiques 0
    } 
}

######################################################################
#Procedure : Enregistrer_Sortie                                      #
#Description : Procedure servant a enregistrer les variables         #
#              choisies pour le simulateur                           #
######################################################################
proc Enregistrer_Sortie {} {

    global DIR_TEMPORAIRE
    global FIC_SORTIES_TXT
    global FIC_TRACE
    global FIC_CHRONIQUE

    global b_trace
    global b_chroniq
    global b_statistiques
    global list_var_retenues

    set file [open "${DIR_TEMPORAIRE}$FIC_SORTIES_TXT" w]
    if {$b_trace == 1} {
	puts $file $FIC_TRACE
    }
    if {$b_chroniq == 1} {
	puts $file $FIC_CHRONIQUE
    }
    if {$b_statistiques == 1} {
	puts $file STATISTIQUES
    }
    set nb [eval [list $list_var_retenues size]]
    for {set i 0} {$i < $nb} {incr i} {
	puts $file [$list_var_retenues get $i]
    }
    close $file
    destroy .wm_Sortie
}
