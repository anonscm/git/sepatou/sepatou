# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerStrategieReglePlanification.tcl 
# Contenu : Description d'une regle de planification
######################################################################

# Une regle de planification est une variable de nom : Regle_Plan<n>
# avec les attributs suivants :
#   (nom_reg_plan) : nom
#   (decl_reg_plan) : declencheurs
#   (code_reg_plan) : code (alors)
#   (commentaires_reg_plan) : commentaires
# La liste des regles de planification est dans la variable : list_Regle_Plan

######################################################################
#Procedure : Creer_Description_Regle_Planification                   #
#Description : Creation de la fenetre chargee de decrire une regle   #
#              de planification                                      #
#Parametre : Struct_Regle_Plan : Structure contenant les             #
#                                informations d'une regle de         #
#                                planification                       #
#  (nom_reg_plan,decl_reg_plan,code_reg_plan, commentaires_reg_plan)
#              mode_visu : 'ecriture' 'lecture'
#              type_insertion : type d'insertion dans la liste des fo 
#                               'ajouter' 'editer'
#              i : position du curseur si edition
######################################################################
proc Creer_Description_Regle_Planification {dir Struct_Regle_Plan mode_visu type_insertion i} {

global LOGICIEL

#Variables communes avec celles du fichier Entree_Exploitation
#Declaration du nom de la structure pour pouvoir l'editer
global $Struct_Regle_Plan

### Creation de la fenetre
set ws .wm_RP_$Struct_Regle_Plan

if {[winfo exists $ws] == 0} {
    toplevel $ws

    #Titre de la fenetre
    wm title $ws "$LOGICIEL : Description d'une regle de planification"

###############################################################################
    ### Zone de saisie 

    frame $ws.f_top -borderwidth 3

    #Creation des libelles
    label $ws.f_top.l_nom -text Nom 
    label $ws.f_top.l_decl -text D�clencheur
    label $ws.f_top.l_code -text "\n ALORS"
    label $ws.f_top.l_com -text "\n Commentaires"

    # nom
    entry $ws.f_top.e_nom -width 60 -relief sunken 
    $ws.f_top.e_nom delete 0 end

    # declencheurs
    entry $ws.f_top.e_decl -width 60 -relief sunken 
    $ws.f_top.e_decl delete 0 end

    # code
    frame $ws.f_top.f_code
    text $ws.f_top.f_code.txt_code \
	-width 60 -height 10 -background white \
	-yscrollcommand "$ws.f_top.f_code.scrb_code set"
    $ws.f_top.f_code.txt_code delete 1.0 end
    scrollbar $ws.f_top.f_code.scrb_code -orient vertical \
	-command "$ws.f_top.f_code.txt_code yview"

    # commentaires
    frame $ws.f_top.f_com
    text $ws.f_top.f_com.txt_com \
	-width 60 -height 5 -background white \
	-yscrollcommand "$ws.f_top.f_com.scrb_com set"
    $ws.f_top.f_com.txt_com delete 1.0 end
    scrollbar $ws.f_top.f_com.scrb_com -orient vertical \
	-command "$ws.f_top.f_com.txt_com yview"

    # Lecture des textes, si on edite une regle
    if {$type_insertion == "editer"} {
	$ws.f_top.e_nom insert end \
	    [set ${Struct_Regle_Plan}(nom_reg_plan)]
	$ws.f_top.e_decl insert end \
	    [set ${Struct_Regle_Plan}(decl_reg_plan)]
	$ws.f_top.f_code.txt_code insert end \
	    [set ${Struct_Regle_Plan}(code_reg_plan)]
	$ws.f_top.f_com.txt_com insert end \
	    [set ${Struct_Regle_Plan}(commentaires_reg_plan)]
    }

    # affichage
    pack $ws.f_top -side top  -fill both -expand 1

    pack $ws.f_top.l_nom -side top
    pack $ws.f_top.e_nom -pady 10 -side top -fill x

    pack $ws.f_top.l_decl -side top
    pack $ws.f_top.e_decl -pady 10 -side top -fill x

    pack $ws.f_top.l_code  -side top
    pack $ws.f_top.f_code.scrb_code -side right -fill y
    pack $ws.f_top.f_code.txt_code -side top -fill both -expand 1
    pack $ws.f_top.f_code  -side top  -fill both -expand 1

    pack $ws.f_top.l_com -side top
    pack $ws.f_top.f_com.scrb_com -side right -fill y
    pack $ws.f_top.f_com.txt_com -side top -fill both 
    pack $ws.f_top.f_com  -side top  -fill both -expand 1


###############################################################################
    ### Zone de commandes des boutons ok, annuler et imprimer

    frame $ws.f_bottom -borderwidth 3
    pack $ws.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button $ws.f_bottom.b_ok -text Ok \
	-command "Ok_Regle_Planification $Struct_Regle_Plan $i $ws $type_insertion" 
    if {[string compare "lecture" $mode_visu] == 0} {
	$ws.f_bottom.b_ok configure -state disabled
    }
    button $ws.f_bottom.b_cancel \
	-text Annuler -command "destroy $ws"
    button $ws.f_bottom.b_imp -text Imprimer \
	-command "Imprimer_Regle_Planification $dir $ws"

    pack $ws.f_bottom.b_ok $ws.f_bottom.b_cancel $ws.f_bottom.b_imp \
	-side left -expand 1 

    ### Separateur
    frame $ws.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack $ws.f_sep -side bottom -fill x -pady 5

}

}

######################################################################
#Procedure : Ok_Regle_Planification                                  #
#Description : Selon le type choisi, met a jour les informations sur #
#              une regle de planification ou ajoute une nouvelle     #
#              regle de planification                                #
#Parametre : struct : Structure de la regle de planification         #
#            ind : Indice indiquant la position de l'element a       #
#                  mettre a jour (cas edition uniquement)            #
#            win : nom de la fenetre a detruire apres traitement     #
#            type : type d'insertion 'ajouter' 'editer'
######################################################################
proc Ok_Regle_Planification {struct ind win type} {

    global LOGICIEL
    #Variables communes avec celles du fichier Entree_Exploitation
    global list_Regle_Plan
    global nb_plan
    #Declaration du contenu de la variable
    global $struct

    set nom  [$win.f_top.e_nom get]
    if {[PbControleChaine $nom nom ]} {
	CreerFenetreDialogue $LOGICIEL error \
	    "Nom de r�gle de planification invalide"
    } elseif {[string length  [$win.f_top.e_decl get]] == 0} {
	CreerFenetreDialogue $LOGICIEL error \
	    "Partie declencheurs de la r�gle de planification vide"
    } elseif {[string length [$win.f_top.f_code.txt_code get 1.0 end]] == 1} {
	CreerFenetreDialogue $LOGICIEL error \
	    "Partie code de la r�gle de planification vide"
    } else {

	#Memorisation de la liste cree 
	#dans le fichier Entree_Strategie.tcl
	set parent .wm_Strategie.f_midleft1.scrlst_bot.lst_list

	#Enregistrement
	set ${struct}(nom_reg_plan) $nom
	set ${struct}(decl_reg_plan) \
	    [$win.f_top.e_decl get]
	set ${struct}(code_reg_plan) \
	    [EpurerText [$win.f_top.f_code.txt_code get 1.0 end]]
	set ${struct}(commentaires_reg_plan) \
	    [EpurerText [$win.f_top.f_com.txt_com get 1.0 end]]

	if {$type == "editer"} {
	    $parent delete $ind
	    $parent insert $ind $nom
	} else {
	    ### Cas d'Ajout
	    $parent insert end $nom

	    #Insertion dans la liste list_Regle_Plan
	    if {[info exist list_Regle_Plan]} {
		#Cas ou la liste existe deja
		set list_Regle_Plan [linsert $list_Regle_Plan end $struct]
	    } else {
		#Cas ou le premier element est insere
		set list_Regle_Plan [list $struct]
	    }

	    #incrementation de l'indice pour la prochaine 
	    #regle de planification a creer
	    set nb_plan [expr $nb_plan+1]
	}
	destroy $win
    }
}

######################################################################
#Procedure : Imprimer_Regle_Planification                            #
#Description : Imprime les donnees saisies par l'utilisateur         #
#Parametre : dir : dir tmp
#            win : nom de la fenetre appelante                       #
######################################################################
proc Imprimer_Regle_Planification {dir win} {
    global LOGICIEL
    global SEPARATEUR_LNU
    global CMD_REMOVE
    global CMD_A2PS

    set file [open ${dir}ReglePlanification.imp w]

    puts $file $SEPARATEUR_LNU
    puts $file "# [EpurerText [$win.f_top.f_com.txt_com get 1.0 end]] #"
    puts $file $SEPARATEUR_LNU
    puts $file "REGLE PLANIFICATION : [$win.f_top.e_nom get]"
    puts $file $SEPARATEUR_LNU
    #Enregistrement du (ou des) declencheur
    set chaine [$win.f_top.e_decl get]
    set index [string first " & " $chaine]
    while {$index != -1} {
	puts $file "DECLENCHEUR : [string range $chaine 0 $index]"
	set chaine [string range $chaine [expr $index + 3] end]
	set index [string first " & " $chaine]
    }
    puts $file "DECLENCHEUR : $chaine"
    puts $file $SEPARATEUR_LNU
    puts $file [EpurerText [$win.f_top.f_code.txt_code get 1.0 end]]
    puts $file $SEPARATEUR_LNU

    close $file


    catch {exec $CMD_A2PS ${dir}ReglePlanification.imp}
    exec $CMD_REMOVE ${dir}ReglePlanification.imp

    CreerFenetreDialogue $LOGICIEL info "Impression en cours"

}


