# Copyright (C) 2005 Marie-Josée Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : ConfigurerChargerConfiguration.tcl
# contenu : 2 procedures permettant de charger une configuration (fichiers
#           decrivant l'exploitation, la strategie, les simulations voulues
#           et les sorties desirees) sous le repertoire temporaire.
#           2 procedures permettant de charger une exploitation sauvegardée.
#           2 procedures permettant de charger une strategie sauvegardée.
##################################################################

#----------------------------------------------------------------------------
# Creation de la fentre de demande de chargement d'une configuration
#----------------------------------------------------------------------------
proc ChargerConfiguration {} {

    global DIR_CONFIGURATIONS
    global LOGICIEL

    set w .wm_ChargerConfiguration

    if {[winfo exists $w] == 0} {
	toplevel $w
	wm title $w "$LOGICIEL : Description de la configuration"

	label $w.l_configuration -text "Configurations"
	pack $w.l_configuration -pady 20

	### Zone de selection ##############################################
	frame $w.f_topw -borderwidth 10
	pack $w.f_topw -padx 20 -pady 20 -side top -expand 1

	#Creation de la Scrolled List
	ScrolledListbox $w.f_topw.scrlst_list -width 50 -height 15 
	set list [recherche_rep $DIR_CONFIGURATIONS]
	set nb [llength $list]
	for  {set i 0} {$i < $nb} {incr i} {
	    set val [lindex $list $i]
	    $w.f_topw.scrlst_list.lst_list insert end $val
	}
	pack $w.f_topw.scrlst_list -padx 4 -side left -anchor n -expand 1


    ###Zone de commandes des boutons ok, annuler #############################
    frame $w.f_bottom -borderwidth 3
    pack $w.f_bottom -side bottom -fill x -pady 5

    button $w.f_bottom.b_ok -text Ok -command "Ok_ChargerConfiguration"
    button $w.f_bottom.b_cancel -text Annuler -command "destroy $w"
    pack $w.f_bottom.b_ok $w.f_bottom.b_cancel -side left -expand 1 
    ### Separateur
    frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack $w.f_sep -side bottom -fill x -pady 5
    }
}

#----------------------------------------------------------------------------
# si clic Ok, chargement de la configuration
#----------------------------------------------------------------------------
proc Ok_ChargerConfiguration {} {

    global DIR_CONFIGURATIONS
    global DIR_TEMPORAIRE
    global FIC_EXPLOITATION_TXT
    global FIC_STRATEGIE_TXT
    global FIC_FOINTERPRETATION_LNU
    global FIC_INDICATEURS_LNU
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU
    global FIC_SIMULATEUR_TXT
    global FIC_SORTIES_TXT
    global CMD_COPY
    global NOM_STRATEGIE
    global NOM_EXPLOITATION
    global MODIF_STRATEGIE
    global LOGICIEL

    set list .wm_ChargerConfiguration.f_topw.scrlst_list.lst_list    
    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue $LOGICIEL error "Sélectionnez une configuration"
    } else {
	set rep [$list get $i]

	# copie des fichiers
	exec $CMD_COPY  $DIR_CONFIGURATIONS${rep}/$FIC_EXPLOITATION_TXT $DIR_TEMPORAIRE
	exec $CMD_COPY  $DIR_CONFIGURATIONS${rep}/$FIC_STRATEGIE_TXT $DIR_TEMPORAIRE
	exec $CMD_COPY  $DIR_CONFIGURATIONS${rep}/$FIC_FOINTERPRETATION_LNU $DIR_TEMPORAIRE
	exec $CMD_COPY  $DIR_CONFIGURATIONS${rep}/$FIC_INDICATEURS_LNU $DIR_TEMPORAIRE
	exec $CMD_COPY  $DIR_CONFIGURATIONS${rep}/$FIC_REGLESPLANIFICATION_LNU $DIR_TEMPORAIRE
	exec $CMD_COPY  $DIR_CONFIGURATIONS${rep}/$FIC_REGLESOPERATOIRES_LNU $DIR_TEMPORAIRE
	exec $CMD_COPY  $DIR_CONFIGURATIONS${rep}/$FIC_SIMULATEUR_TXT $DIR_TEMPORAIRE
	exec $CMD_COPY  $DIR_CONFIGURATIONS${rep}/$FIC_SORTIES_TXT $DIR_TEMPORAIRE

	# mise a jour des variables globales de l'interface
	if {[catch {set file [open "$DIR_TEMPORAIRE$FIC_EXPLOITATION_TXT" r]}] == 0} {
	    seek $file 0 start
	    gets $file line
	    set NOM_EXPLOITATION [SortirValeur $line]
	    close $file
	} 
	if {[catch {set file [open "$DIR_TEMPORAIRE$FIC_STRATEGIE_TXT" r]}] == 0} {
	    seek $file 0 start
	    gets $file line
	    set NOM_STRATEGIE [SortirValeur $line]
	    close $file
	} 

	set MODIF_STRATEGIE 1

	destroy .wm_ChargerConfiguration
    }
}






#----------------------------------------------------------------------------
# Creation de la fenetre de demande de chargement d'une exploitation
#----------------------------------------------------------------------------
proc ChargerExploitation {} {

    global DIR_SIMULATIONS
    global LOGICIEL

    set w .wm_ChargerExploitation

    if {[winfo exists $w] == 0} {
	toplevel $w
	wm title $w "$LOGICIEL : Description de l'exploitation"

	label $w.l_exploitation -text "Exploitations"
	pack $w.l_exploitation -pady 20

	### Zone de selection ##############################################
	frame $w.f_topw -borderwidth 10
	pack $w.f_topw -padx 20 -pady 20 -side top -expand 1

	#Creation de la Scrolled List
	ScrolledListbox $w.f_topw.scrlst_list -width 50 -height 15 
	set list [recherche_rep $DIR_SIMULATIONS]
	set nb [llength $list]
	for  {set i 0} {$i < $nb} {incr i} {
	    set val [lindex $list $i]
	    $w.f_topw.scrlst_list.lst_list insert end $val
	}
	pack $w.f_topw.scrlst_list -padx 4 -side left -anchor n -expand 1


	###Zone de commandes des boutons ok, annuler ########################
	frame $w.f_bottom -borderwidth 3
	pack $w.f_bottom -side bottom -fill x -pady 5
	
	button $w.f_bottom.b_ok -text Ok -command "Ok_ChargerExploitation"
	button $w.f_bottom.b_cancel -text Annuler -command "destroy $w"
	pack $w.f_bottom.b_ok $w.f_bottom.b_cancel -side left -expand 1 
	### Separateur
	frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $w.f_sep -side bottom -fill x -pady 5
    }
}

#----------------------------------------------------------------------------
# si clic Ok, chargement de l'exploitation
#----------------------------------------------------------------------------
proc Ok_ChargerExploitation {} {

    global DIR_SIMULATIONS
    global DIR_TEMPORAIRE
    global FIC_EXPLOITATION_TXT
    global CMD_COPY
    global NOM_EXPLOITATION
    global LOGICIEL

    set list .wm_ChargerExploitation.f_topw.scrlst_list.lst_list    
    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue $LOGICIEL error "Sélectionnez une exploitation"
    } else {
	set rep [$list get $i]

	# copie du fichier
	exec $CMD_COPY  $DIR_SIMULATIONS${rep}/$FIC_EXPLOITATION_TXT $DIR_TEMPORAIRE

	# mise a jour des variables globales de l'interface
	if {[catch {set file [open "$DIR_TEMPORAIRE$FIC_EXPLOITATION_TXT" r]}] == 0} {
	    seek $file 0 start
	    gets $file line
	    set NOM_EXPLOITATION [SortirValeur $line]
	    close $file
	} 

	destroy .wm_ChargerExploitation
    }
}



#----------------------------------------------------------------------------
# Creation de la fenetre de demande de chargement d'une strategie
#----------------------------------------------------------------------------
proc ChargerStrategie {} {

    global DIR_SIMULATIONS
    global LOGICIEL
    global exploitation_choisie

    set w .wm_ChargerStrategie
    if {[winfo exists $w] == 0} {
	toplevel $w
	wm title $w "$LOGICIEL : Description de la strategie"

	label $w.l_strategie -text "Choisir d'abord une exploitation. Après la liste des stratégies définies pour cette exploitation sera affichée."
	pack $w.l_strategie -pady 20

	### Zone de selection ##############################################
	frame $w.f_topw -borderwidth 10
	pack $w.f_topw -padx 20 -pady 20 -side top -expand 1

	set exploitation_choisie ""
	label $w.f_topw.l_exploitation -text Exploitation
	pack $w.f_topw.l_exploitation
	#ComboBox pour le choix de l'exploitation
	tixComboBox $w.f_topw.lst_exploitation\
	-variable exploitation_choisie \
	-command Rechercher_Strategies 
	pack $w.f_topw.lst_exploitation

	foreach nom_exploitation [recherche_rep $DIR_SIMULATIONS] {
	    $w.f_topw.lst_exploitation insert end $nom_exploitation
	}
	label $w.f_topw.l_espace -text ""
	pack $w.f_topw.l_espace
	label $w.f_topw.l_strategies -text Strategies
	pack $w.f_topw.l_strategies 
	#Creation de la Scrolled List
	ScrolledListbox $w.f_topw.scrlst_list_strategies -width 50 -height 15 
	pack $w.f_topw.scrlst_list_strategies -padx 4 -side left -anchor n -expand 1

	###Zone de commandes des boutons ok, annuler #############################
	frame $w.f_bottom -borderwidth 3
	pack $w.f_bottom -side bottom -fill x -pady 5
	
	button $w.f_bottom.b_ok -text Ok -command "Ok_ChargerStrategie"
	button $w.f_bottom.b_cancel -text Annuler -command "destroy $w"
	pack $w.f_bottom.b_ok $w.f_bottom.b_cancel -side left -expand 1 
	### Separateur
	frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $w.f_sep -side bottom -fill x -pady 5
    }
}

#----------------------------------------------------------------------------
# si clic Ok, chargement de la strategie
#----------------------------------------------------------------------------
proc Ok_ChargerStrategie {} {

    global DIR_SIMULATIONS
    global DIR_TEMPORAIRE
    global FIC_STRATEGIE_TXT
    global FIC_FOINTERPRETATION_LNU
    global FIC_INDICATEURS_LNU
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU
    global CMD_COPY
    global NOM_STRATEGIE
    global MODIF_STRATEGIE
    global LOGICIEL
    global exploitation_choisie


    if {[PbControleChaine $exploitation_choisie nom]} {
	CreerFenetreDialogue $LOGICIEL error "Sélectionnez une exploitation sauvegardée."
    } else {
	set list_strategies .wm_ChargerStrategie.f_topw.scrlst_list_strategies.lst_list
	set i_strategies [$list_strategies curselection]
	if {$i_strategies == ""} {
	    CreerFenetreDialogue $LOGICIEL error "Sélectionnez une strategie sauvegardée."
	} else {
	    set rep_strategie [$list_strategies get $i_strategies]
	    
	    # copie des fichiers
	    exec $CMD_COPY  $DIR_SIMULATIONS${exploitation_choisie}/${rep_strategie}/$FIC_STRATEGIE_TXT $DIR_TEMPORAIRE
	    exec $CMD_COPY  $DIR_SIMULATIONS${exploitation_choisie}/${rep_strategie}/$FIC_FOINTERPRETATION_LNU $DIR_TEMPORAIRE
	    exec $CMD_COPY  $DIR_SIMULATIONS${exploitation_choisie}/${rep_strategie}/$FIC_INDICATEURS_LNU $DIR_TEMPORAIRE
	    exec $CMD_COPY  $DIR_SIMULATIONS${exploitation_choisie}/${rep_strategie}/$FIC_REGLESPLANIFICATION_LNU $DIR_TEMPORAIRE
	    exec $CMD_COPY  $DIR_SIMULATIONS${exploitation_choisie}/${rep_strategie}/$FIC_REGLESOPERATOIRES_LNU $DIR_TEMPORAIRE
	    
	    # mise a jour des variables globales de l'interface
	    if {[catch {set file [open "$DIR_TEMPORAIRE$FIC_STRATEGIE_TXT" r]}] == 0} {
		seek $file 0 start
		gets $file line
		set NOM_STRATEGIE [SortirValeur $line]
		close $file
	    }

	    set MODIF_STRATEGIE 1
	    destroy .wm_ChargerStrategie
	} 
    }
}



#----------------------------------------------------------------------------
# recherche des strategies sauvegardées sous une exploitation
#----------------------------------------------------------------------------
proc Rechercher_Strategies {exploitation} {

    global DIR_SIMULATIONS

    set l_strategies .wm_ChargerStrategie.f_topw.scrlst_list_strategies.lst_list

    $l_strategies delete 0 end
    set liste [recherche_rep $DIR_SIMULATIONS${exploitation}]
    for {set j 0} {$j < [llength $liste]} {incr j} {
	$l_strategies insert end [lindex $liste $j]
    }
}

