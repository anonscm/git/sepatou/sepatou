# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : GererSauvegardesEtudesdeCas.tcl
# contenu : procedures permettant de gerer les etudes de cas
##################################################################

#----------------------------------------------------------------------------
# Creation de la fenetre de gestion des etudes de cas
#----------------------------------------------------------------------------
proc GererSauvegardesEtudesDeCas {} {

    global LOGICIEL
    global DIR_SIMULATIONS

    global hlist_Gerer

    if {[winfo exists .wm_GererCas] == 0} {
	toplevel .wm_GererCas
	wm title .wm_GererCas "$LOGICIEL : Gestion des etudes de cas sauvegardees"
	wm geometry .wm_GererCas 400x600	
	
	label .wm_GererCas.l_etudes -text "ETUDES DE CAS SAUVEGARDEES"
	pack .wm_GererCas.l_etudes -padx 10 -pady 10

	### Zone des boutons ################################################
	frame .wm_GererCas.f_boutons
	button .wm_GererCas.f_boutons.b_enlever -text Enlever \
	    -command "Enlever_Gerer"
	button .wm_GererCas.f_boutons.b_afficher -text Afficher \
	    -command "Afficher_Gerer"
	pack .wm_GererCas.f_boutons.b_enlever \
	    .wm_GererCas.f_boutons.b_afficher -side left
	pack .wm_GererCas.f_boutons -padx 10 -pady 5 


	### Zone de selection ##############################################
	frame .wm_GererCas.f_top -borderwidth 10
	pack .wm_GererCas.f_top -padx 20   -expand 1 -fill both

	#Creation de tixTree
	tixTree .wm_GererCas.f_top.tree -browsecmd MemoriserChoixGerer -options {
	    hlist.separator /
	    hlist.itemType imagetext
	}
	set hlist_Gerer [.wm_GererCas.f_top.tree subwidget hlist]

	### mise a jour de l'affichage
	set folder [tix getimage folder]
	set filee [tix getimage file]
	
	foreach e [recherche_rep $DIR_SIMULATIONS] {
	    $hlist_Gerer add $e -itemtype imagetext \
		-text $e -image $folder
	    foreach s [recherche_rep $DIR_SIMULATIONS$e] {
		$hlist_Gerer add ${e}/$s -itemtype imagetext \
		    -text $s -image $folder
		foreach m [recherche_rep $DIR_SIMULATIONS${e}/${s}] {
		    $hlist_Gerer add ${e}/${s}/$m -itemtype imagetext \
			-text $m -image $folder
		    foreach a [recherche_rep $DIR_SIMULATIONS${e}/${s}/${m}] {
			$hlist_Gerer add ${e}/${s}/${m}/$a -itemtype imagetext \
			    -text $a -image $folder
			foreach f [recherche_fic $DIR_SIMULATIONS${e}/${s}/${m}/${a}] {
			    $hlist_Gerer add ${e}/${s}/${m}/${a}/$f -itemtype imagetext \
				-text $f -image $filee
			}
		    }
		}
	    }
	}

	.wm_GererCas.f_top.tree autosetmode
	pack .wm_GererCas.f_top.tree  -expand 1 -fill both
	
	###Zone de commandes du bouton fermer #############################
	frame .wm_GererCas.f_bottom -borderwidth 3
	pack .wm_GererCas.f_bottom -side bottom -fill x -pady 5
	button .wm_GererCas.f_bottom.b_fermer \
	    -text Fermer -command "Fermer_SousFenetres_Gerer .wm_GererCas"
	pack .wm_GererCas.f_bottom.b_fermer -side left -expand 1 
	### Separateur
	frame .wm_GererCas.f_sep \
	    -width 100 -height 2 -borderwidth 1 -relief sunken
	pack .wm_GererCas.f_sep -side bottom -fill x -pady 5
    }
}


#----------------------------------------------------------------------------
# Memorisation du choix graphique fait
#----------------------------------------------------------------------------
proc MemoriserChoixGerer {entry} {
    global Choix_Gerer
    set Choix_Gerer $entry
}


#----------------------------------------------------------------------------
# Affiche une exploitation, une strategie ou une variable
#----------------------------------------------------------------------------
proc Afficher_Gerer {} {

    global LOGICIEL
    global DIR_SIMULATIONS
    global FIC_TRACE
    global FIC_CHRONIQUE
    global FIC_PROBLEME

    global Choix_Gerer

    set w .wm_GererCas

    if {$Choix_Gerer == ""} {
	CreerFenetreDialogue $LOGICIEL error "S�lectionnez un item"
    } else {
	set separateur [string first / $Choix_Gerer]
	if { $separateur == -1 } {
	    # c'est une exploitation
	    source ConfigurerExploitation.tcl
	    Desc_Exploitation lecture $DIR_SIMULATIONS/${Choix_Gerer}/
	} else {
	    set nom_exploitation [string range $Choix_Gerer 0 [expr $separateur - 1]]
	    set choix [string range $Choix_Gerer [expr $separateur + 1] end]
	    set separateur [string first / $choix]
	    if { $separateur == -1 } {
		# c'est une strategie
		source ConfigurerStrategie.tcl
		Desc_Strategie lecture \
		    ${DIR_SIMULATIONS}${nom_exploitation}/${choix}/

	    } else {
		set nom_strategie [string range $choix 0 [expr $separateur - 1]]
		set choix [string range $choix [expr $separateur + 1] end]
		set separateur [string first / $choix]
		if { $separateur == -1 } {
		    # c'est une station meteo
		    CreerFenetreDialogue $LOGICIEL error "Une station m�t�o n'est pas visualisable"
		} else {
		    set nom_station [string range $choix 0 [expr $separateur - 1]] 
		    set choix [string range $choix [expr $separateur + 1] end]
		    set separateur [string first / $choix]
		    if { $separateur == -1 } {
			# c'est une ann�e simul�e
			CreerFenetreDialogue $LOGICIEL error "Une ann�e simul�e n'est pas visualisable"
		    } else {
			set nom_annee [string range $choix 0 [expr $separateur - 1]]
			set choix [string range $choix [expr $separateur + 1] end]
			set separateur [string first / $choix]
			if { $separateur == -1 } {
			    # c'est une variable
			    if {[string compare $choix $FIC_TRACE] == 0} {
				source VisualiserSortiesTrace.tcl
				Afficher_trace ${w}trace $nom_annee\
				    ${DIR_SIMULATIONS}${nom_exploitation}/${nom_strategie}/${nom_station}/${nom_annee}/
				
			    } elseif {[string compare $choix $FIC_CHRONIQUE] == 0} {
				source VisualiserSortiesChronique.tcl
				Afficher_chronique ${w}chronique $nom_annee\
				    ${DIR_SIMULATIONS}${nom_exploitation}/${nom_strategie}/${nom_station}/${nom_annee}/
				
			    } elseif {[string compare $choix $FIC_PROBLEME] == 0} {
				source VisualiserSortiesProbleme.tcl
				Afficher_probleme ${w}Probleme $nom_annee\
					${DIR_SIMULATIONS}${nom_exploitation}/${nom_strategie}/${nom_station}/${nom_annee}/
			    } else {
				source  VisualiserSortiesVariables.tcl 
				Afficher_variables ${w}$choix \
				    $choix [list $nom_annee]\
				    ${DIR_SIMULATIONS}${nom_exploitation}/${nom_strategie}/${nom_station}/
			    }
			    
			} else {
			    CreerFenetreDialogue $LOGICIEL error "ERREUR dans  Afficher_Gerer"
			}
		    }
		}
	    }
	}
    }
}

#----------------------------------------------------------------------------
# Enleve un sous arbre
#----------------------------------------------------------------------------
proc Enlever_Gerer {} {

    global LOGICIEL
    global DIR_SIMULATIONS
    global CMD_REMOVE
    global OPT_RM_DIR

    global Choix_Gerer
    global hlist_Gerer

    if {$Choix_Gerer == ""} {
	CreerFenetreDialogue $LOGICIEL error "S�lectionnez un item"
    } else {
	set cas [lindex [split $Choix_Gerer /] end]
	set msg "Voulez-vous vraiment \n enlever : $cas ?"
	if { [Confirmer $LOGICIEL $msg]  == 1} {
	    # destruction du sous-arbre
	    exec $CMD_REMOVE $OPT_RM_DIR $DIR_SIMULATIONS$Choix_Gerer
	    # mise a jour de l'affichage
	    $hlist_Gerer hide - $Choix_Gerer
	}
    }
}


#----------------------------------------------------------------------------
# Fermeture de la fenetre et des sous-fenetres
#----------------------------------------------------------------------------
proc Fermer_SousFenetres_Gerer {w} {

    set list_fenetres [winfo children .]
    DetruireFenetresPrefixees $w $list_fenetres
}


