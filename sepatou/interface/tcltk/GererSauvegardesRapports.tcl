# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : GererSauvegardesRapports.tcl
# contenu : procedures permettant de gerer les rapports 
##################################################################

##################################################################
# Procedure : GererSauvegardesRapports
# description : lance la fenetre de choix d'un rapport a gerer 
# Parametres    : aucun
##################################################################
proc GererSauvegardesRapports {} {
 
    global LOGICIEL
    global DIR_RAPPORTS
    
    if {[winfo exists .wm_GererRapports] == 0} {
	# cr�ation de la fenetre
	toplevel .wm_GererRapports
	wm title .wm_GererRapports "$LOGICIEL : Gestion des rapports sauvegardes"
	wm geometry .wm_GererRapports 400x500	
	
	#Cr�ation d'un titre pour la fen�tre sous forme de label
	label .wm_GererRapports.l_configuration -text RAPPORTS
	pack .wm_GererRapports.l_configuration -padx 10 -pady 10
	
	### Zone des boutons ################################################
	#Cr�ation de la frame contenant les boutons
	frame .wm_GererRapports.f_boutons
	pack .wm_GererRapports.f_boutons -padx 10 -pady 10 -expand 1
	#Cr�ation des boutons Enlever et Afficher
	button .wm_GererRapports.f_boutons.b_enlever -text Enlever \
	    -command "Enlever_GererRapports"
	button .wm_GererRapports.f_boutons.b_afficher -text Afficher \
	    -command "Afficher_GererRapports"
	pack .wm_GererRapports.f_boutons.b_enlever \
	    .wm_GererRapports.f_boutons.b_afficher -side left -expand 1 
	
	

	### Zone de selection ###############################################
	#Cr�ation de la zone de S�lection des rapports
	frame .wm_GererRapports.f_topw -borderwidth 10
	pack .wm_GererRapports.f_topw -padx 20 -pady 20 -side top -expand 1

	#Creation de la Scrolled List qui contient la liste 
	#des rapports sauvegard�s
	ScrolledListbox .wm_GererRapports.f_topw.scrlst_list \
	    -width 20 -height 15 
	### Chargement dans la Scrolled List de la liste des rapports
	set list [recherche_rep $DIR_RAPPORTS]
	set nb [llength $list]
	for  {set i 0} {$i < $nb} {incr i} {
	    set val [lindex $list $i]
	    .wm_GererRapports.f_topw.scrlst_list.lst_list insert end $val
	}
	pack .wm_GererRapports.f_topw.scrlst_list \
	    -padx 4 -side left -anchor n -expand 1
	
	
	###Zone de commandes du bouton fermer #############################
	#Cr�ation de la frame contenant le bouton Fermer
	frame .wm_GererRapports.f_bottom -borderwidth 3
	pack .wm_GererRapports.f_bottom -side bottom -fill x -pady 5
	#Cr�ation du bouton Fermer
	button .wm_GererRapports.f_bottom.b_fermer -text Fermer \
	    -command "Fermer_GererRapports"
	pack .wm_GererRapports.f_bottom.b_fermer -side left -expand 1 
	
	### Cr�ation de la ligne s�paratrice du bas
	frame .wm_GererRapports.f_sep \
	    -width 100 -height 2 -borderwidth 1 -relief sunken
	pack .wm_GererRapports.f_sep -side bottom -fill x -pady 5
    }
}

##################################################################
# Procedure : Fermer_GererRapports
# description : ferme toutes les sous-fenetres
# Parametres    : aucun
##################################################################
proc Fermer_GererRapports {} {
 
    set list_fenetres [winfo children .]    
    DetruireFenetresPrefixees .wm_GererRapports $list_fenetres
}

##################################################################
# Procedure : Enlever_GererRapports
# description : enleve un rapport sauvegarde
# Parametres    : aucun
##################################################################
proc Enlever_GererRapports {} {

    global LOGICIEL
    global DIR_RAPPORTS
    global CMD_REMOVE
    global OPT_RM_DIR

    # liste des rapports sauvegard�s
    set list .wm_GererRapports.f_topw.scrlst_list.lst_list    
    # num�ro correspondant au rapport s�lectionn� 
    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue $LOGICIEL error "S�lectionnez un rapport"
    } else {
	# rep du nom du rapport s�lectionn�
	set rep [$list get $i]
	set msg "Voulez-vous vraiment enlever \n le rapport : $rep ?"
	if { [Confirmer $LOGICIEL $msg] == 1} {
	    exec $CMD_REMOVE $OPT_RM_DIR ${DIR_RAPPORTS}$rep
	    # Mise a jour de la liste des rapports
	    $list delete $i
	}
    }
}


##################################################################
# Procedure : Afficher_GererRapports
# description : Affiche un rapport sauvegarde
# Parametres    : aucun
##################################################################
proc Afficher_GererRapports {} {

    global LOGICIEL
    global DIR_RAPPORTS

    # liste des rapports sauvegard�s
    set list .wm_GererRapports.f_topw.scrlst_list.lst_list    
    # i : num�ro correspondant au rapport s�lectionn�
    set i [$list curselection]
  
    if {$i == ""} {
	# aucun rapport n'a ete selectionne
	CreerFenetreDialogue $LOGICIEL error "S�lectionnez un rapport" 
    } else {
	# rep : nom du rapport s�lectionn�
	set rep [$list get $i]
	# affichage du rapport s�lectionn�
	source ExecuterFaireRapport.tcl
	VisualiserRapport $DIR_RAPPORTS$rep $rep
    }
}        