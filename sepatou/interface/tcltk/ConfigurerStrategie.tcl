# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerStrategie.tcl 
# Contenu : description de la strategie du simulateur   
######################################################################

source ConfigurerStrategieFoInterpretation.tcl
source ConfigurerStrategieIndicateur.tcl
source ConfigurerStrategieReglePlanification.tcl
source ConfigurerStrategieRegleOperatoire.tcl
source ConfigurerStrategieCharger.tcl


######################################################################
#Procedure   : Desc_Strategie                                    
#Description : Cree la fenetre chargee de decrire la strategie du 
#              simulateur  
#Entrees     : mode_visu = 'ecriture' 'lecture'
#              dir : repertoire ou est potentiellement lu 
#                    (et enregistre en mode ecriture)
#                    les fichiers de description d une strategie
######################################################################
proc Desc_Strategie {mode_visu dir} {
 
global LOGICIEL
global FIC_STRATEGIE_TXT
global FIC_FOINTERPRETATION_LNU
global FIC_INDICATEURS_LNU
global FIC_REGLESPLANIFICATION_LNU
global FIC_REGLESOPERATOIRES_LNU
global APP_ACTIVITES
global nb_fct
global list_Fonction
global nb_indic
global list_Indicateur
global nb_plan
global list_Regle_Plan
global ind_Act


# initialisation de toutes les variables globales liees a la strategie
set nb_fct 0
set list_Fonction ""
set nb_indic 0
set list_Indicateur ""
set nb_plan 0
set list_Regle_Plan ""
for  {set i 0} {$i < [llength $APP_ACTIVITES]} {incr i} {
    global list_Regle_Op$i
    set list_Regle_Op$i ""
}
set ind_Act 0



###############################################################################
### Creation de la fenetre
if {[winfo exists .wm_Strategie] == 0} {
    toplevel .wm_Strategie
    wm title .wm_Strategie "$LOGICIEL : Description de la strategie"

###############################################################################
    ### Zone de saisie du nom de la strategie et des dates

    frame .wm_Strategie.f_top -borderwidth 10

    #Creation du frame pour le libelle et la saisie du nom
    frame .wm_Strategie.f_top.f_label

    label .wm_Strategie.f_top.f_label.l_nom -text {Nom}
    entry .wm_Strategie.f_top.f_label.e_nom

    pack .wm_Strategie.f_top.f_label.l_nom .wm_Strategie.f_top.f_label.e_nom \
	-padx 10 -side left

    #Creation des frames des dates
    frame .wm_Strategie.f_top.f_left
    frame .wm_Strategie.f_top.f_right

    ### Remplissage du cadre de gauche

    #Creation du titre
    label .wm_Strategie.f_top.f_left.l_date -text {Date 2�me apport azote}
    frame .wm_Strategie.f_top.f_left.f_date

    #Creation des libelles
    label .wm_Strategie.f_top.f_left.f_date.l_jj -text {Jour}
    label .wm_Strategie.f_top.f_left.f_date.l_mm -text {Mois}

    #Creation des OptionMenu
    tixOptionMenu .wm_Strategie.f_top.f_left.f_date.opm_jj \
	-options {menubutton.width 3}
    tixOptionMenu .wm_Strategie.f_top.f_left.f_date.opm_mm \
	-options {menubutton.width 3}
    #Jour de 2eme apport
    Num .wm_Strategie.f_top.f_left.f_date.opm_jj 1 31
    #Mois de 2eme apport
    Num .wm_Strategie.f_top.f_left.f_date.opm_mm 2 12


    #Gestion des libelles et des OptionMenu
    pack .wm_Strategie.f_top.f_left.f_date.l_jj \
	.wm_Strategie.f_top.f_left.f_date.opm_jj \
	.wm_Strategie.f_top.f_left.f_date.l_mm .wm_Strategie.f_top.f_left.f_date.opm_mm \
	-padx 10 -side left


    pack .wm_Strategie.f_top.f_left.l_date .wm_Strategie.f_top.f_left.f_date -pady 3 \
	-side top -expand 1 -anchor w -fill x

    ### Remplissage du cadre de droite

    #Creation du titre
    label .wm_Strategie.f_top.f_right.l_date -text {Date 3�me apport azote}
    frame .wm_Strategie.f_top.f_right.f_date

    #Creation des libelles
    label .wm_Strategie.f_top.f_right.f_date.l_jj -text {Jour}
    label .wm_Strategie.f_top.f_right.f_date.l_mm -text {Mois}

    #Creation des OptionMenu
    tixOptionMenu .wm_Strategie.f_top.f_right.f_date.opm_jj \
	-options {menubutton.width 3} 
    tixOptionMenu .wm_Strategie.f_top.f_right.f_date.opm_mm \
	-options {menubutton.width 3} 
    #Jour de 3eme apport
    Num .wm_Strategie.f_top.f_right.f_date.opm_jj 1 31
    #Mois de 3eme apport
    Num .wm_Strategie.f_top.f_right.f_date.opm_mm 2 12

    #Gestion des libelles et des  OptionMenu
    pack .wm_Strategie.f_top.f_right.f_date.l_jj \
	.wm_Strategie.f_top.f_right.f_date.opm_jj \
	.wm_Strategie.f_top.f_right.f_date.l_mm \
	.wm_Strategie.f_top.f_right.f_date.opm_mm \
	-padx 10 -side left

    pack .wm_Strategie.f_top.f_right.l_date .wm_Strategie.f_top.f_right.f_date \
	-pady 3 -side top -expand 1 -anchor w -fill x

    pack .wm_Strategie.f_top.f_label .wm_Strategie.f_top.f_left \
	-side top -pady 10 -anchor w

    #Gestion du cadre de gauche et du cadre de droite
    pack .wm_Strategie.f_top.f_left .wm_Strategie.f_top.f_right \
	-side left -expand 1 -padx 20

    pack .wm_Strategie.f_top -side top -anchor w -fill x -expand 1

###############################################################################
    ### Zone de commandes des boutons ok, annuler et imprimer

    frame .wm_Strategie.f_bottom -borderwidth 3

    #Creation des boutons
    button .wm_Strategie.f_bottom.b_ok -text Ok -command "Ok_Strategie $dir"
    if {[string compare "lecture" $mode_visu] == 0} {
	.wm_Strategie.f_bottom.b_ok configure -state disabled
    }

    button .wm_Strategie.f_bottom.b_cancel -text Annuler -command Annuler_Strategie 
    button .wm_Strategie.f_bottom.b_imp -text Imprimer -command "Imprimer_Strategie $dir"

    pack .wm_Strategie.f_bottom.b_ok .wm_Strategie.f_bottom.b_cancel \
	.wm_Strategie.f_bottom.b_imp \
	-padx 20 -pady 10  -side left -expand 1

    pack .wm_Strategie.f_bottom -side bottom -fill x -pady 5

    ### Separateur
    frame .wm_Strategie.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack .wm_Strategie.f_sep -side bottom -fill x -pady 5

###############################################################################
    ### Creation de 4 Scrolled Listbox avec titre et boutons associes

    #Appel de CreerList dans Fonctions_utilitaires.tcl
    CreerList .wm_Strategie.f_midright1 FONCTIONS D'INTERPRETATION \
	Ajouter_Fct Editer_Fct Enlever_Fct $dir $mode_visu
    CreerList .wm_Strategie.f_midright2 INDICATEURS "" \
	Ajouter_Indicateur Editer_Indicateur Enlever_Indicateur $dir $mode_visu
    CreerList .wm_Strategie.f_midleft1 {REGLES DE} PLANIFICATION \
	Ajouter_Regle_Plan Editer_Regle_Plan Enlever_Regle_Plan $dir $mode_visu
    CreerList .wm_Strategie.f_midleft2  REGLES OPERATOIRES \
	Ajouter_Regle_Op Editer_Regle_Op Enlever_Regle_Op $dir $mode_visu
    for  {set i 0} {$i < [llength $APP_ACTIVITES]} {incr i} {
	set val [lindex $APP_ACTIVITES $i]
	.wm_Strategie.f_midleft2.f_mid_option.opm_op add command $i -label $val 
    }
 
    .wm_Strategie.f_midleft2.f_mid_option.opm_op\
	configure -command Charger_Regle  -variable ind_Act

    #effacement des options menu non utilises
    pack forget .wm_Strategie.f_midleft1.f_mid_option.opm_op
    pack forget .wm_Strategie.f_midright1.f_mid_option.opm_op
    pack forget .wm_Strategie.f_midright2.f_mid_option.opm_op


    pack .wm_Strategie.f_midleft1 .wm_Strategie.f_midleft2 \
	.wm_Strategie.f_midright1 .wm_Strategie.f_midright2 \
	-side left -padx 0 -pady 20 -anchor s -expand 1

###############################################################################
    ### Chargement a partir d'un fichier
    Charger_Dft_Strat $dir

}
}

######################################################################
#Procedure : Ajouter_Fct                                             #
#Description : Ouvre la fenetre Description d'une fonction           #
#              d'interpretation                                      #
######################################################################
proc Ajouter_Fct {dir mode_visu} {

    global nb_fct
    global Fonction$nb_fct

    Creer_Description_Fct_Interpretation $dir Fonction$nb_fct $mode_visu ajouter -1
}

######################################################################
#Procedure : Enlever_Fct                                             #
#Description : Detruit les informations relatives a la fonction      #
#              selectionnee et l'efface de la liste affichee et de   #
#              la memoire                                            #
#Parametre : del : contient le nom de la liste sur laquelle on       #
#                  efface l'element selectionne                      #
######################################################################
proc Enlever_Fct {del} {

    global list_Fonction

    set i [$del curselection]

    if {$i != ""} {

	#recuperation de la structure de l'element a effacer
	set Fonct [lindex $list_Fonction $i]

	#Suppression dans la liste affichant les fonctions
	$del delete $i
	#Suppression dans la liste interne 
	set list_Fonction [lreplace $list_Fonction $i $i]

	#Si la fonction a ete editee,
	#on detruit la fenetre d'edition de la fonction a enlever
	set wedit .wm_Fo_$Fonct
	if {[winfo exists $wedit]} {
	    destroy $wedit
	}
    }
}

######################################################################
#Procedure : Editer_Fct                                              #
#Description : Ouvre la fenetre Description d'une fonction           #
#              d'interpretation et charge les informations relatives #
#              a l'element dont on veut visualiser les informations  #
#Parametre : edit : contient le nom de la liste sur laquelle on fait #
#                   l'edition de l'element selectionne               #
######################################################################
proc Editer_Fct {dir edit mode_visu} {
    
    global list_Fonction

    set i [$edit curselection]

    if {$i != ""} {
	set Fct [lindex $list_Fonction $i]
	Creer_Description_Fct_Interpretation $dir $Fct $mode_visu editer $i
    }
}

######################################################################
#Procedure : Ajouter_Indicateur                                      #
#Description : Ouvre la fenetre Description d'un indicateur          #
######################################################################
proc Ajouter_Indicateur {dir mode_visu} {

    global nb_indic
    global Indicateur$nb_indic

    Creer_Description_Indicateur $dir Indicateur$nb_indic $mode_visu ajouter -1
}

######################################################################
#Procedure : Enlever_Indicateur                                      #
#Description : Detruit les informations relatives a l'indicateur     #
#              selectionne et l'efface de la liste affichee et de    #
#              la memoire                                            #
#Parametre : del : contient le nom de la liste sur laquelle on       #
#                  efface l'element selectionne                      #
######################################################################
proc Enlever_Indicateur {del} {

    global list_Indicateur

    set i [$del curselection]

    if {$i != ""} {

	#recuperation de la structure de l'element a effacer
	set Indic [lindex $list_Indicateur $i]

	#Suppression dans la liste affichant les indicateurs
	$del delete $i
	#Suppression dans la liste interne 
	set list_Indicateur [lreplace $list_Indicateur $i $i]

	#Si l'indicateur a ete edite,
	#on detruit la fenetre d'edition de l'indicateur a enlever
	set wedit .wm_I_$Indic
	if {[winfo exists $wedit]} {
	    destroy $wedit
	}
    }
}

######################################################################
#Procedure : Editer_Indicateur                                       #
#Description : Ouvre la fenetre Description d'un indicateur et       #
#              charge les informations relatives a l'element dont    #
#              on veut visualiser les informations                   #
#Parametre : edit : contient le nom de la liste sur laquelle on fait #
#                   l'edition de l'element selectionne               #
######################################################################
proc Editer_Indicateur {dir edit mode_visu} {

    global list_Indicateur

    set i [$edit curselection]

    if {$i != ""} {
	set Indic [lindex $list_Indicateur $i]
	Creer_Description_Indicateur $dir $Indic $mode_visu editer $i
    }
}

######################################################################
#Procedure : Ajouter_Regle_Plan                                      #
#Description : Ouvre la fenetre Description d'une regle de           #
#              planification                                         #
######################################################################
proc Ajouter_Regle_Plan {dir  mode_visu} {

    global nb_plan
    global Regle_Plan$nb_plan

    Creer_Description_Regle_Planification $dir Regle_Plan$nb_plan $mode_visu ajouter -1
}

######################################################################
#Procedure : Enlever_Regle_Plan                                      #
#Description : Detruit les informations relatives a la regle de      #
#              planification selectionnee et l'efface de la liste    #
#              affichee et de la memoire                             #
#Parametre : del : contient le nom de la liste sur laquelle on       #
#                  efface l'element selectionne                      #
######################################################################
proc Enlever_Regle_Plan {del} {

    global list_Regle_Plan

    set i [$del curselection]

    if {$i != ""} {

	#recuperation de la structure de l'element a effacer
	set Regle [lindex $list_Regle_Plan $i]

	#Suppression dans la liste affichant les regles de planification
	$del delete $i
	#Suppression dans la liste interne 
	set list_Regle_Plan [lreplace $list_Regle_Plan $i $i]

	#Si la fonction a ete editer,
	#on detruit la fenetre d'edition de la regle de planification
	#a enlever
	set wedit .wm_RP_$Regle
	if {[winfo exists $wedit]} {
	    destroy $wedit
	}
    }
}

######################################################################
#Procedure : Editer_Regle_Plan                                       #
#Description : Ouvre la fenetre Description d'une regle de           #
#              planification et charge les informations relatives a  #
#              l'element dont on veut visualiser les informations    #
#Parametre : edit : contient le nom de la liste sur laquelle on fait #
#                   l'edition de l'element selectionne               #
######################################################################
proc Editer_Regle_Plan {dir edit mode_visu} {

    global list_Regle_Plan

    set i [$edit curselection]

    if {$i != ""} {
	set Plan [lindex $list_Regle_Plan $i]
	Creer_Description_Regle_Planification $dir $Plan $mode_visu editer $i
    }
}

######################################################################
#Procedure : Ajouter_Regle_Op                                        #
#Description : Ouvre la fenetre Description d'une regle operatoire   #
######################################################################
proc Ajouter_Regle_Op {dir mode_visu} {

    global ind_Act
    global list_Regle_Op$ind_Act
    set nb [llength [set list_Regle_Op$ind_Act]]
    global Regle_Op${ind_Act}N$nb

    Creer_Description_Regle_Operatoire $dir Regle_Op${ind_Act}N$nb \
	list_Regle_Op$ind_Act $mode_visu ajouter -1
}

######################################################################
#Procedure : Enlever_Regle_Op                                        #
#Description : Detruit les informations relatives a la regle         #
#              operatoire selectionnee et l'efface de la liste       #
#              affichee et de la memoire                             #
#Parametre : del : contient le nom de la liste sur laquelle on       #
#                  efface l'element selectionne                      #
######################################################################
proc Enlever_Regle_Op {del} {

    global ind_Act
    global list_Regle_Op$ind_Act

    set i [$del curselection]

    if {$i != ""} {

	#recuperation de la structure de l'element a effacer
	set Regle [lindex [set list_Regle_Op$ind_Act] $i]

	#Suppression dans la liste affichant les regles operatoires
	$del delete $i
	#Suppression dans la liste interne 
	set list_Regle_Op$ind_Act [lreplace [set list_Regle_Op$ind_Act] $i $i]

	#Si la fonction a ete editer,
	#on detruit la fenetre d'edition de la regle operatoire a enlever
	set wedit .wm_RO_$Regle
	if {[winfo exists $wedit]} {
	    destroy $wedit
	}
    }
}

######################################################################
#Procedure : Editer_Regle_Op                                         #
#Description : Ouvre la fenetre Description d'une regle operatoire   #
#              et charge les informations relatives a l'element dont #
#              on veut visualiser les informations                   #
#Parametre : edit : contient le nom de la liste sur laquelle on fait #
#                   l'edition de l'element selectionne               #
######################################################################
proc Editer_Regle_Op {dir edit mode_visu} {
    
    global ind_Act
    global list_Regle_Op$ind_Act

    set i [$edit curselection]

    if {$i != ""} {
	set Op [lindex [set list_Regle_Op$ind_Act] $i]
	Creer_Description_Regle_Operatoire $dir $Op list_Regle_Op$ind_Act \
	    $mode_visu editer $i
    }
}

######################################################################
#Procedure   : Enregistrer_Strategie        
#Description : Enregistre une strategie dans 5 fichiers
#Parametre : dir : repertoire ou enregistrer
#            fic_txt : fichier texte 
#            fic_fo : fichier des fo d interpretation
#            fic_I : fichier des indicateurs
#            fic_RP : fichier des regles de planification
#            fic_RO : fichier des regles operatoires
######################################################################
proc Enregistrer_Strategie {dir fic_txt fic_fo fic_I fic_RP fic_RO} {

    global SEPARATEUR_LNU
    global APP_ACTIVITES

    global list_Regle_Plan
    global list_Fonction
    global list_Indicateur

######################################################################
    ### Enregistrement dans le fichier texte
    set nom [.wm_Strategie.f_top.f_label.e_nom get]
    set j2 [.wm_Strategie.f_top.f_left.f_date.opm_jj cget -value]
    set m2 [.wm_Strategie.f_top.f_left.f_date.opm_mm cget -value]
    set j3 [.wm_Strategie.f_top.f_right.f_date.opm_jj cget -value]
    set m3 [.wm_Strategie.f_top.f_right.f_date.opm_mm cget -value]

    set file [open "$dir$fic_txt" w]
    seek $file 0 start

    set line ""; append line "Nom = " $nom
    puts $file $line

    set line ""; append line "Jour2emeApportN = " $j2
    puts $file $line

    set line ""; append line "Mois2emeApportN = " $m2
    puts $file $line

    set line ""; append line "Jour3emeApportN = " $j3
    puts $file $line

    set line ""; append line "Mois3emeApportN = " $m3
    puts $file $line

    close $file

######################################################################
    ### Enregistrement des regles de planification
    set file [open "$dir$fic_RP" w]

    set nb [llength $list_Regle_Plan]
    for {set i 0} {$i < $nb} {incr i} {
	set elem [lindex $list_Regle_Plan $i]
	global $elem

	puts $file $SEPARATEUR_LNU
	puts $file "# [set ${elem}(commentaires_reg_plan)] #"
	puts $file $SEPARATEUR_LNU
	puts $file "REGLE PLANIFICATION : [set ${elem}(nom_reg_plan)]"
	puts $file $SEPARATEUR_LNU
	#Enregistrement du (ou des) declencheur
	set chaine [set ${elem}(decl_reg_plan)]
	set index [string first " & " $chaine]
	while {$index != -1} {
	    puts $file "DECLENCHEUR : [string range $chaine 0 $index]"
	    set chaine [string range $chaine [expr $index + 3] end]
	    set index [string first " & " $chaine]
	}
	puts $file "DECLENCHEUR : $chaine"
	puts $file $SEPARATEUR_LNU
	puts $file [set ${elem}(code_reg_plan)]
	puts $file $SEPARATEUR_LNU
	puts $file \n
    }

    close $file

######################################################################
    ### Enregistrement des Regles Operatoires
    set file [open "$dir$fic_RO" w]

    #Enregistrement des regles par activite
    set nb [llength $APP_ACTIVITES]
    for {set i 0} {$i < $nb} {incr i} {
	global list_Regle_Op$i

	puts $file "ACTIVITE : [lindex $APP_ACTIVITES $i]"
	puts $file \n
	set nb_op [llength [set list_Regle_Op$i]]
	for {set j 0} {$j < $nb_op} {incr j} {
	    set elem [lindex [set list_Regle_Op$i] $j]
	    global $elem

	    puts $file $SEPARATEUR_LNU
	    puts $file "# [set ${elem}(commentaires_reg_op)] #"
	    puts $file $SEPARATEUR_LNU
	    puts $file "REGLE OPERATOIRE : [set ${elem}(nom_reg_op)]"
	    puts $file $SEPARATEUR_LNU
	    puts $file "SI "
	    puts $file "[set ${elem}(si_reg_op)]"
	    puts $file $SEPARATEUR_LNU
	    puts $file "ALORS "
	    puts $file "[set ${elem}(alors_reg_op)]"
	    puts $file $SEPARATEUR_LNU
	    if {[set ${elem}(sinon_reg_op)] != ""} {
		puts  $file "SINON "
		puts $file "[set ${elem}(sinon_reg_op)]"
	    }
	    puts $file $SEPARATEUR_LNU
            puts $file \n
	}
	puts $file "FIN ACTIVITE"
	puts $file \n
    }

    close $file

######################################################################
    ### Enregistrement des fonctions d'interpretation
    set file [open "$dir$fic_fo" w]

    for {set i 0} {$i < [llength $list_Fonction]} {incr i} {
	set elem [lindex $list_Fonction $i]
	global $elem

	puts $file $SEPARATEUR_LNU

	puts $file "# [set ${elem}(commentaires_fonct)] #"
	puts $file $SEPARATEUR_LNU
	puts $file "FONCTION : [set ${elem}(nom_fonct)]"
	puts $file "CODE : "
	puts $file "[set ${elem}(code_fonct)]"
    }

    close $file

######################################################################
    ### Enregistrement des Indicateurs
    set file [open "$dir$fic_I" w]

    set nb [llength $list_Indicateur]
    for {set i 0} {$i < $nb} {incr i} {
	set elem [lindex $list_Indicateur $i]
	global $elem

	puts $file $SEPARATEUR_LNU
	puts $file "# [set ${elem}(commentaires_indic)] #"
	puts $file $SEPARATEUR_LNU
	puts $file "INDICATEUR : [set ${elem}(nom_indic)]"
	puts $file "CARACTERISTIQUE : "
	puts $file "[set ${elem}(car_indic)]"
    }

    close $file
}

######################################################################
# Procedure   : Ok_Strategie
# Description : verification des donnees,
#               enregistrement dans fichiers temporaires si OK
#               fermeture des fenetres liees
######################################################################
proc Ok_Strategie {dir} {

    global LOGICIEL
    global NOM_STRATEGIE
    global MODIF_STRATEGIE
    global FIC_STRATEGIE_TXT
    global FIC_FOINTERPRETATION_LNU
    global FIC_INDICATEURS_LNU
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU

    # Verification des saisies
    set nom [.wm_Strategie.f_top.f_label.e_nom get]
    set j2 [.wm_Strategie.f_top.f_left.f_date.opm_jj cget -value]
    set m2 [.wm_Strategie.f_top.f_left.f_date.opm_mm cget -value]
    set j3 [.wm_Strategie.f_top.f_right.f_date.opm_jj cget -value]
    set m3 [.wm_Strategie.f_top.f_right.f_date.opm_mm cget -value]


    set annul 0
    if {[PbControleChaine $nom nom]} {
	set annul 1
	set msg_err "Nom de strat�gie invalide"
    } elseif  {$m2 > $m3} {
	set annul 1
	set msg_err "2�me apport d'azote avant 3�me apport"
    } elseif  { $m2 == $m3 && $j2 >= $j3} {
	set annul 1
	set msg_err "2�me apport d'azote avant 3�me apport"
    }

    if {$annul == 0} {
	# MAJ des variables globales
	set NOM_STRATEGIE $nom
	set MODIF_STRATEGIE 1

	Enregistrer_Strategie $dir $FIC_STRATEGIE_TXT\
	    $FIC_FOINTERPRETATION_LNU $FIC_INDICATEURS_LNU\
	    $FIC_REGLESPLANIFICATION_LNU $FIC_REGLESOPERATOIRES_LNU
	Annuler_Strategie ;# fermeture des fentres
    } else {
	CreerFenetreDialogue $LOGICIEL error $msg_err
    }

}

######################################################################
#Procedure : Annuler_Strategie      
#Description : 
######################################################################
proc Annuler_Strategie {} {

    set list_fenetres [winfo children .]
	
    DetruireFenetresPrefixees .wm_Fo_ $list_fenetres
    DetruireFenetresPrefixees .wm_I_ $list_fenetres
    DetruireFenetresPrefixees .wm_RP_ $list_fenetres
    DetruireFenetresPrefixees .wm_RO_ $list_fenetres

    destroy .wm_Strategie
}

######################################################################
#Procedure : Imprimer_Strategie                                      #
#Description : Imprime les donnees saisies par l'utilisateur         #
######################################################################
proc Imprimer_Strategie {dir} {
    global LOGICIEL
    global CMD_REMOVE
    global CMD_A2PS

    Enregistrer_Strategie $dir Strategie.imp \
	    FoInterpretation.imp Indicateurs.imp ReglesPlanification.imp ReglesOperatoires.imp

    catch {exec $CMD_A2PS ${dir}ReglesPlanification.imp}
    exec $CMD_REMOVE ${dir}ReglesPlanification.imp

    catch {exec $CMD_A2PS ${dir}ReglesOperatoires.imp}
    exec $CMD_REMOVE ${dir}ReglesOperatoires.imp

    catch {exec $CMD_A2PS ${dir}FoInterpretation.imp}
    exec $CMD_REMOVE ${dir}FoInterpretation.imp

    catch {exec $CMD_A2PS ${dir}Indicateurs.imp}
    exec $CMD_REMOVE ${dir}Indicateurs.imp

    catch {exec $CMD_A2PS ${dir}Strategie.imp}
    exec $CMD_REMOVE ${dir}Strategie.imp

    CreerFenetreDialogue $LOGICIEL info "Impression en cours"
}
