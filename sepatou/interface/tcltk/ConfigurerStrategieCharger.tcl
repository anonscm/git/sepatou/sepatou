# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerStrategieCharger.tcl   
# Contenu : Permet de charger une strategie a partir d'un fichier
######################################################################



######################################################################
#Procedure : Charger_Dft_Strat                                       #
#Description : Charge une description de strategie a partir du 
#              repertoire indique par chemin          
#Parametre : chemin : chemin designant l'emplacement ou s'effectue   #
#                     le chargement de la strategie                  #
######################################################################
proc Charger_Dft_Strat {chemin} {

    global FIC_STRATEGIE_TXT

    #Ouverture du fichier texte
    if {[catch {set file [open "${chemin}$FIC_STRATEGIE_TXT" r]}] == 0} {
	seek $file 0 start

	gets $file line
	.wm_Strategie.f_top.f_label.e_nom delete 0 end
        .wm_Strategie.f_top.f_label.e_nom insert 0 [SortirValeur $line]

	gets $file line
	set j2 [SortirValeur $line]
        .wm_Strategie.f_top.f_left.f_date.opm_jj configure -value $j2

	gets $file line
	set m2 [SortirValeur $line]
        .wm_Strategie.f_top.f_left.f_date.opm_mm configure -value $m2

	gets $file line
	set j3 [SortirValeur $line]
        .wm_Strategie.f_top.f_right.f_date.opm_jj configure -value $j3

	gets $file line
	set m3 [SortirValeur $line]
        .wm_Strategie.f_top.f_right.f_date.opm_mm configure -value $m3

	close $file
    }

    LireReglesPlanification $chemin
    LireReglesOperatoires $chemin
    LireFoInterpretation $chemin
    LireIndicateurs $chemin

}

######################################################################
#Chargement des regles de planification
######################################################################
proc LireReglesPlanification {chemin} {

    global FIC_REGLESPLANIFICATION_LNU
    global SEPARATEUR_LNU

    global nb_plan
    global list_Regle_Plan

    set liste_Plan .wm_Strategie.f_midleft1.scrlst_bot.lst_list

    if {[catch {set file [open "${chemin}$FIC_REGLESPLANIFICATION_LNU" r]}] \
	    == 0} {
	seek $file 0 start

	#initialisation des listes
	$liste_Plan delete 0 end
	set list_Regle_Plan ""
	set nb_plan 0

	set chercher_RP 1
	while {$chercher_RP == 1} {
	    if {[eof $file] == 1} {
		set chercher_RP 0
	    } else {
		gets $file line; set line [string trimleft $line]
                if {[string first $SEPARATEUR_LNU $line] == 0} {
		    # debut d une regle

		    # lecture du commentaire #############################
		    gets $file line; set line [string trimleft $line]
		    set commentaires ""
		    if {[string first $SEPARATEUR_LNU $line] != 0} {
			while {[string first $SEPARATEUR_LNU $line] != 0} {
			    if {$commentaires == ""} {
				set commentaires $line
			    } else {
				append commentaires \n  $line
			    }
			    gets $file line
			}
		    }

		    # on enleve le 1er '# ' et le dernier ' #'
		    set commentaires [Enlever2caracteresDebutFin $commentaires]

		    # lecture du nom #####################################
		    while {[string first "REGLE PLANIFICATION" $line] != 0} {
			gets $file line; set line [string trimleft $line]
		    }
		    global Regle_Plan$nb_plan

		    set regle [string range $line 21 end]
		    set regle [string trimleft $regle]
		    set Regle_Plan${nb_plan}(nom_reg_plan) $regle

		    #Chargement de la regle de planification dans la listbox
		    $liste_Plan insert end $regle

		    #Chargement en memoire de la regle de planification
		    # dans la liste des regles de planification
		    if {[set list_Regle_Plan] != ""} {
			#Cas ou la liste existe deja
			set list_Regle_Plan \
			    [linsert $list_Regle_Plan end Regle_Plan$nb_plan]
		    } else {
			#Cas ou le premier element est insere
			set list_Regle_Plan [list Regle_Plan$nb_plan]
		    }

		    # lecture des declencheurs #############################
		    while {[string first "DECLENCHEUR" $line] != 0} {
			gets $file line; set line [string trimleft $line]
		    }
		    set Regle_Plan${nb_plan}(decl_reg_plan) ""

		    while {([string first "DECLENCHEUR" $line] == 0) \
			   && ([eof $file] != 1)} {
			set line [string range $line 14 end]
			set line [string trimleft $line]
			if {[set Regle_Plan${nb_plan}(decl_reg_plan)] != ""} {
			    append Regle_Plan${nb_plan}(decl_reg_plan) " & " $line
			} else {
			    set Regle_Plan${nb_plan}(decl_reg_plan) $line
			}
			gets $file line
		    }

		    # lecture du code ######################################
		    gets $file line; set line [string trimleft $line]
		    set Regle_Plan${nb_plan}(code_reg_plan) $line
		    gets $file line
		    while {([string first $SEPARATEUR_LNU $line] != 0) && \
			   ([eof $file] != 1)} {
			append Regle_Plan${nb_plan}(code_reg_plan) \n $line 
			gets $file line
		    }

		    # mise a jour du commentaire
		    set Regle_Plan${nb_plan}(commentaires_reg_plan) $commentaires

		    incr nb_plan
		}
	    }
	}

	close $file
    }
}

###############################################################################
#Chargement des regles operatoires
###############################################################################
proc LireReglesOperatoires {chemin} {

    global FIC_REGLESOPERATOIRES_LNU
    global APP_ACTIVITES
    global SEPARATEUR_LNU
    for  {set i 0} {$i < [llength $APP_ACTIVITES]} {incr i 1} {
	global list_Regle_Op$i
	global nb_op$i
    }

    set liste_Op .wm_Strategie.f_midleft2.scrlst_bot.lst_list

    #initialisation des listes
    for  {set i 0} {$i < [llength $APP_ACTIVITES]} {incr i 1} {
	$liste_Op delete 0 end
	set list_Regle_Op$i ""
	set nb_op$i 0
    }

    if {[catch {set file [open "${chemin}$FIC_REGLESOPERATOIRES_LNU" r]}] \
	    == 0} {
	seek $file 0 start

	set chercher_activite 1
	while {$chercher_activite} {
	    if {[eof $file]} {
		set chercher_activite 0
	    } else {
		gets $file line; set line [string trimleft $line]
                if {[string first "ACTIVITE" $line] == 0} {
		    # debut d une activite
		    set existe 0
		    set ind 0
		    set trouve 0
		    set activite [string range $line 11 end]
		    set activite [string trimleft $activite]
		    while  {($ind < [llength $APP_ACTIVITES]) && ($trouve == 0)} {
			if {[string first [lindex $APP_ACTIVITES $ind] $activite]== 0} {
			    set trouve 1
			} else {
			    incr ind
			}
		    }
		    if {$trouve == 0} {
			puts "Une activite donnee est invalide"
			puts $activite
			set chercher_activite 0
		    } else {

			while {[string first "FIN ACTIVITE" $line] != 0} {
			    set nb_op 0
			    set chercher_RO 1
			    while {$chercher_RO} {
				if {[eof $file] || [string first "FIN ACTIVITE" $line] == 0} {
				    set chercher_RO 0
				} else {
				    gets $file line; set line [string trimleft $line]
				    if  {[string first $SEPARATEUR_LNU $line] == 0} {
					# debut d une regle

					# lecture du commentaire #######################
					gets $file line; set line [string trimleft $line]
					set commentaires ""
					if {[string first $SEPARATEUR_LNU $line] != 0} {
					    while {[string first $SEPARATEUR_LNU $line] != 0} {
						if {$commentaires == ""} {
						    set commentaires $line
						} else {
						    append commentaires \n  $line
						}
						gets $file line
					    }
					}
					# on enleve le 1er '# ' et le dernier ' #'
					set commentaires [Enlever2caracteresDebutFin $commentaires]

					# lecture du nom ###############################
					while {[string first "REGLE OPERATOIRE" $line] != 0} {
					    gets $file line; set line [string trimleft $line]
					}
					global Regle_Op${ind}N${nb_op}

					set regle [string range $line 19 end]
					set regle [string trimleft $regle]
					set Regle_Op${ind}N${nb_op}(nom_reg_op) $regle

					#Chargement de la regle operatoire dans la listbox
					#si la regle fait partie de la 1ere activite de la liste 
					#d'activites
					if {$ind == 0} {
					    $liste_Op insert end $regle
					}

					#Chargement en memoire de la regle operatoire
					# dans la liste des regles operatoire
					if {[info exist list_Regle_Op$ind]} {
					    #Cas ou la liste existe deja
					    set list_Regle_Op$ind [linsert \
						   [set list_Regle_Op$ind] \
							end Regle_Op${ind}N${nb_op}]
					} else {
					    #Cas ou le premier element est insere
					    set list_Regle_Op$ind \
						[list Regle_Op${ind}N${nb_op}]
					}		

					# lecture du SI #########################
					while {[string first "SI" $line] != 0} {
					    gets $file line; set line [string trimleft $line]
					}
					gets $file line; set line [string trimleft $line]
					if {[string first $SEPARATEUR_LNU $line] != 0} {
					    set Regle_Op${ind}N${nb_op}(si_reg_op)  $line 
					}
					gets $file line; set line [string trimleft $line]
					while {[string first $SEPARATEUR_LNU $line] != 0} {
					    append Regle_Op${ind}N${nb_op}(si_reg_op) \n $line 
					    gets $file line
					}

					# lecture du ALORS #######################
					while {[string first "ALORS" $line] != 0} {
					    gets $file line; set line [string trimleft $line]
					}
					gets $file line; set line [string trimleft $line]
					if {[string first $SEPARATEUR_LNU $line] != 0} {
					    set Regle_Op${ind}N${nb_op}(alors_reg_op)  $line 
					}
					gets $file line; set line [string trimleft $line]
					while {[string first $SEPARATEUR_LNU $line] != 0} {
					    append Regle_Op${ind}N${nb_op}(alors_reg_op) \n $line 
					    gets $file line
					}

					# lecture du SINON ######################
					gets $file line; set line [string trimleft $line]
					if {[string first $SEPARATEUR_LNU $line] != 0} {
					    while {[string first "SINON" $line] != 0} {
						gets $file line; set line [string trimleft $line]
					    }
					    gets $file line; set line [string trimleft $line]
					    if {[string first $SEPARATEUR_LNU $line] != 0} {
						set Regle_Op${ind}N${nb_op}(sinon_reg_op)  $line 
					    }
					    gets $file line; set line [string trimleft $line]
					    while {[string first $SEPARATEUR_LNU $line] != 0} {
						append Regle_Op${ind}N${nb_op}(sinon_reg_op) \n $line 
						gets $file line
					    }
					} else {
					    set Regle_Op${ind}N${nb_op}(sinon_reg_op) ""
					}

					# mise a jour du commentaire
					set Regle_Op${ind}N${nb_op}(commentaires_reg_op) $commentaires

					set nb_op$ind $nb_op

					incr nb_op
				    }
				}
			    }
			}
		    }
		}
	    }
	}
	close $file
    }
}

###############################################################################
#Chargement des fonctions d'interpretation
###############################################################################
proc LireFoInterpretation {chemin} {

    global FIC_FOINTERPRETATION_LNU
    global SEPARATEUR_LNU

    global nb_fct
    global list_Fonction

    set liste_Fct .wm_Strategie.f_midright1.scrlst_bot.lst_list

    if {[catch {set file [open "${chemin}$FIC_FOINTERPRETATION_LNU" r]}] == 0} {
	seek $file 0 start

	#initialisation des listes
	$liste_Fct delete 0 end
	set list_Fonction ""
	set nb_fct 0

	set chercher_FO 1
	if {[eof $file]!=1} {
	    gets $file line; set line [string trimleft $line]
	}
	while {$chercher_FO} {
	    if {[eof $file]} {
		set chercher_FO 0
	    } else {
                if {[string first $SEPARATEUR_LNU $line] == 0} {
		    # debut d une fonction

		    # lecture du commentaire #############################
		    gets $file line; set line [string trimleft $line]
		    set commentaires ""
		    if {[string first $SEPARATEUR_LNU $line] != 0} {
			while {[string first $SEPARATEUR_LNU  $line] != 0} {
			    if {$commentaires == ""} {
				set commentaires $line
			    } else {
				append commentaires \n  $line
			    }
			    gets $file line
			}
		    }

		    # on enleve le 1er '# ' et le dernier ' #'
		    set commentaires [Enlever2caracteresDebutFin $commentaires]

		    # lecture du nom #####################################
		    while {[string first "FONCTION" $line] != 0} {
			gets $file line; set line [string trimleft $line]
		    }
		    global Fonction$nb_fct

		    set fonction [string range $line 11 end]
		    set fonction [string trimleft $fonction]
		    set Fonction${nb_fct}(nom_fonct) $fonction

		    #Chargement de la fonction d'interpretation dans la listbox
		    $liste_Fct insert end $fonction

		    #Chargement en memoire de la fonction d'interpretation
		    # dans la liste des fonctions d'interpretation
		    if {[info exists list_Fonction]} {
			#Cas ou la liste existe deja
			set list_Fonction [linsert $list_Fonction end Fonction$nb_fct]
		    } else {
			#Cas ou le premier element est insere
			set list_Fonction [list Fonction$nb_fct]
		    }

		    # lecture du code #####################################
		    while {[string first "CODE" $line] != 0} {
			gets $file line; set line [string trimleft $line]
		    }

		    gets $file line
		    if {[string first $SEPARATEUR_LNU $line] != 0} {
			set  Fonction${nb_fct}(code_fonct) $line 
		    }
		    gets $file line
		    while {[string first $SEPARATEUR_LNU $line] != 0  && \
			   [eof $file] != 1} {
			append Fonction${nb_fct}(code_fonct) \n $line 
			gets $file line
		    }

		    # mise a jour du commentaire
		    set Fonction${nb_fct}(commentaires_fonct) $commentaires

		    incr nb_fct
		} 
	    }
	}

	close $file
    }
}

###############################################################################
#Chargement des indicateurs
###############################################################################
proc LireIndicateurs {chemin} {

    global FIC_INDICATEURS_LNU
    global SEPARATEUR_LNU

    global nb_indic
    global list_Indicateur

    set liste_Indic .wm_Strategie.f_midright2.scrlst_bot.lst_list

    if {[catch {set file [open "${chemin}$FIC_INDICATEURS_LNU" r]}] == 0} {
	seek $file 0 start

	#initialisation des listes
	$liste_Indic delete 0 end
	set list_Indicateur ""
	set nb_findic 0

	set chercher_I 1
	if {[eof $file]!=1} {
	    gets $file line; set line [string trimleft $line]
	}
	while {$chercher_I} {
	    if {[eof $file]} {
		set chercher_I 0
	    } else {
                if {[string first $SEPARATEUR_LNU $line] == 0} {
		    # debut d un indicateur

		    # lecture du commentaire #############################
		    gets $file line; set line [string trimleft $line]
		    set commentaires ""
		    if {[string first $SEPARATEUR_LNU $line] != 0} {
			while {[string first $SEPARATEUR_LNU $line] != 0} {
			    if {$commentaires == ""} {
				set commentaires $line
			    } else {
				append commentaires \n  $line
			    }
			    gets $file line
			}
		    }
		    # on enleve le 1er '# ' et le dernier ' #'
		    set commentaires [Enlever2caracteresDebutFin $commentaires]

		    # lecture du nom #####################################
		    while {[string first "INDICATEUR" $line] != 0} {
			gets $file line; set line [string trimleft $line]
		    }
		    global Indicateur$nb_indic

		    set indicateur [string range $line 13 end]
		    set indicateur [string trimleft $indicateur]
		    set Indicateur${nb_indic}(nom_indic) $indicateur

		    #Chargement de l'indicateur dans la listbox
		    $liste_Indic insert end $indicateur

		    #Chargement en memoire de l'indicateur dans la liste
		    #des indicateurs
		    if {[set list_Indicateur] != ""} {
			#Cas ou la liste existe deja
			set list_Indicateur [linsert $list_Indicateur \
					     end Indicateur$nb_indic]
		    } else {
			#Cas ou le premier element est insere
			set list_Indicateur [list Indicateur$nb_indic]
		    }

		    # lecture caracteristique ############################
		    while {[string first "CARACTERISTIQUE" $line] != 0} {
			gets $file line; set line [string trimleft $line]
		    }

		    gets $file line
		    if {[string first $SEPARATEUR_LNU $line] != 0} {
			set Indicateur${nb_indic}(car_indic)  $line 
		    }
		    gets $file line
		    while {[string first $SEPARATEUR_LNU $line] != 0  && \
			   [eof $file] != 1} {
			append Indicateur${nb_indic}(car_indic) \n $line
			gets $file line
		    }

		    # mise a jour du commentaire
		    set Indicateur${nb_indic}(commentaires_indic) $commentaires

		    incr nb_indic
		}
	    }
	}

	close $file
    }
}
    

######################################################################
#Procedure : Charger_Regle                                           #
#Description : Charge les regles operatoires d'une liste choisie     #
#Parametre : ind : designe l'indice de la liste des regles           #
#                  operatoire a afficher                             #
######################################################################
proc Charger_Regle {ind} {

    global list_Regle_Op$ind

    set liste_Op .wm_Strategie.f_midleft2.scrlst_bot.lst_list
    $liste_Op delete 0 end

    set nb [llength [set list_Regle_Op$ind]]

    for {set i 0} {$i < $nb} {incr i} {
	global Regle_Op${ind}N$i
	set regle [set Regle_Op${ind}N${i}(nom_reg_op)]
	#Chargement de la regle operatoire dans la listbox
	$liste_Op insert end $regle

    }
}

