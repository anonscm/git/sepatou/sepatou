# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : VisualiserConfiguration.tcl
# contenu : procedures permettant de visualiser les principaux
#           elements d'une configuration
##################################################################

#----------------------------------------------------------------------------
# Creation de la fentre de visualisation d'une configuration
# w : nom de la fenetre cr��e
# chemin : r�pertoire ou se trouve le fichier de description (finit par /)
# nom : nom de la configuration
#----------------------------------------------------------------------------
proc VisualiserConfiguration {w chemin nom} {

    global LOGICIEL

    catch {destroy $w}
    toplevel $w
    wm title $w "$LOGICIEL : Visualisation de configuration"
    
    ### Zone d'affichage #####################################################
    frame $w.f_config
    label $w.f_config.l_nom -text "Nom configuration"
    entry $w.f_config.e_nom
    $w.f_config.e_nom insert end $nom

    frame $w.f_exploitation
    label $w.f_exploitation.l_nom -text "Nom exploitation"
    entry $w.f_exploitation.e_nom
    
    frame $w.f_strategie
    label $w.f_strategie.l_nom -text "Nom strategie"
    entry $w.f_strategie.e_nom
    
    frame $w.f_station
    label $w.f_station.l_station -text "Station m�t�o"
    entry $w.f_station.e_station

    frame $w.f_annees -borderwidth 1 -relief sunken
    Creer_titre_souligne $w.f_annees.t_titre \
	"Ann�es � simuler"
    frame $w.f_annees.f_r
    label $w.f_annees.f_r.l_reelles -text "R�elles"
    ScrolledListbox $w.f_annees.f_r.scrlst_reelles
    frame $w.f_annees.f_l
    label $w.f_annees.f_l.l_generees -text "G�n�r�es\n"
    label $w.f_annees.f_l.l_graine -text "Graine"
    frame $w.f_annees.f_e
    entry $w.f_annees.f_e.e_generees 
    entry $w.f_annees.f_e.e_graine 
 
    frame $w.f_sorties
    label $w.f_sorties.l_sorties -text Sorties
    ScrolledListbox $w.f_sorties.scrlst_sorties
    
    pack $w.f_config.l_nom -side left
    pack $w.f_config.e_nom -side right
    pack $w.f_config -expand 1 -pady 10
    
    pack $w.f_exploitation.l_nom -side left
    pack $w.f_exploitation.e_nom -side right
    pack $w.f_exploitation -expand 1 -pady 10
    
    pack $w.f_strategie.l_nom -side left
    pack $w.f_strategie.e_nom -side right
    pack $w.f_strategie -expand 1 -pady 10

    pack $w.f_station.l_station -side left
    pack $w.f_station.e_station -side right
    pack $w.f_station -expand 1 -pady 10
    
    pack $w.f_annees.t_titre
    pack $w.f_annees.f_r.l_reelles -side left
    pack $w.f_annees.f_r.scrlst_reelles -side right
    pack $w.f_annees.f_r -side left
    pack $w.f_annees.f_l.l_generees
    pack $w.f_annees.f_l.l_graine
    pack $w.f_annees.f_l -side left
    pack $w.f_annees.f_e.e_generees 
    pack $w.f_annees.f_e.e_graine  
    pack $w.f_annees.f_e -side right
    pack $w.f_annees -expand 1 -pady 10 -padx 10
    
    pack $w.f_sorties.l_sorties -side left
    pack $w.f_sorties.scrlst_sorties -side right
    pack $w.f_sorties -expand 1 -pady 10
    
    LireVisualisationConfiguration $chemin $w
    
    
    ###Zone de commandes des boutons fermer, imprimer #########################
    
    frame $w.f_bottom -borderwidth 3
    pack $w.f_bottom -side bottom -fill x -pady 5
    
    #Creation des boutons
    button $w.f_bottom.b_fermer -text Fermer \
	-command "destroy $w"
    button $w.f_bottom.b_imprimer \
	-text Imprimer -command "ImprimerConfiguration $w"
    pack $w.f_bottom.b_imprimer \
	$w.f_bottom.b_fermer -side left -expand 1 
    
    ### Separateur
    frame $w.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack $w.f_sep -side bottom -fill x -pady 5
}


#----------------------------------------------------------------------------
# lecture des informations decrivant une configuration
#----------------------------------------------------------------------------
proc LireVisualisationConfiguration {chemin w} {

    global FIC_EXPLOITATION_TXT
    global FIC_STRATEGIE_TXT
    global FIC_SIMULATEUR_TXT
    global FIC_SORTIES_TXT

    if {[catch {set file [open "$chemin$FIC_EXPLOITATION_TXT" r]}] == 0} {
	gets $file line
	$w.f_exploitation.e_nom insert 0 [SortirValeur $line]
	close $file
    } 
    if {[catch {set file [open "$chemin$FIC_STRATEGIE_TXT" r]}] == 0} {
	gets $file line
	$w.f_strategie.e_nom insert 0 [SortirValeur $line]
	close $file
    } 

    if {[catch {set file [open "$chemin$FIC_SIMULATEUR_TXT" r]}] == 0} {
	gets $file line ;# jourd
	gets $file line ;# moid
	gets $file line ;# jourf
	gets $file line ;# moisf
	gets $file line ;# station_meteo
	$w.f_station.e_station insert 0 [SortirValeur $line]
	gets $file line ;# nb annnes generees
	$w.f_annees.f_e.e_generees insert 0 [SortirValeur $line]
	gets $file line ;# graine
	$w.f_annees.f_e.e_graine insert 0 [SortirValeur $line]
	while { [gets $file line] > 0} {
	    $w.f_annees.f_r.scrlst_reelles.lst_list insert end $line
	}
	close $file
    } 

    if {[catch {set file [open "$chemin$FIC_SORTIES_TXT" r]}] == 0} {
	while { [gets $file line] > 0} {
	    $w.f_sorties.scrlst_sorties.lst_list insert end $line
	}
	close $file
    } 
}



#----------------------------------------------------------------------------
# impression de la configuration
#----------------------------------------------------------------------------
proc ImprimerConfiguration {w} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global CMD_REMOVE
    global CMD_A2PS

    set file [open ${DIR_TEMPORAIRE}Configuration.imp w]

    puts $file "Nom configuration = [$w.f_config.e_nom get]"
    puts $file "\n"
    puts $file "Nom exploitation = [$w.f_exploitation.e_nom get]"
    puts $file "Nom strat�gie = [$w.f_strategie.e_nom get]"
    puts $file "\n"
    puts $file "ANNEES A SIMULER"
    puts $file "Ann�es R�elles : [$w.f_annees.f_r.scrlst_reelles.lst_list  get 0 end]"
    puts $file "Nombre d'ann�es � g�n�rer = [$w.f_annees.f_e.e_generees get]"
    puts $file "Graine = [$w.f_annees.f_e.e_graine get]"
    puts $file "\n"
    puts $file "ALEA RESERVE MAIS"
    puts $file "Ecart = [$w.f_mais.e_ecart get]"
    puts $file "Nombre = [$w.f_mais.e_nb get]"
    puts $file "\n"
    puts $file "SORTIES DESIREES"
    puts $file [$w.f_sorties.scrlst_sorties.lst_list get 0 end]

    close $file

    catch {exec $CMD_A2PS ${DIR_TEMPORAIRE}Configuration.imp}
    exec $CMD_REMOVE ${DIR_TEMPORAIRE}Configuration.imp

    CreerFenetreDialogue $LOGICIEL info "Impression en cours"
}

