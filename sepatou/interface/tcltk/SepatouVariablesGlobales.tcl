# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

####################################################################
# fichier : SepatouVariablesGlobales.tcl
# contenu : Definition des variables globales a toute l'interface
#####################################################################

# nom du logiciel
global LOGICIEL
set LOGICIEL SEPATOU

# chemin de la racine
global DIR_RACINE
set DIR_RACINE [glob ~/SEPATOU]

# chemin du repertoire des climats 
global DIR_CLIMATS
set DIR_CLIMATS ${DIR_RACINE}/climats/

# chemin du repertoire des etudes de cas sauvegardees
global DIR_SIMULATIONS
set DIR_SIMULATIONS ${DIR_RACINE}/simulations/

# chemin du repertoire des configurations sauvegardees
global DIR_CONFIGURATIONS
set DIR_CONFIGURATIONS ${DIR_RACINE}/configurations/

# chemin du repertoire des rapports sauvegardes
global DIR_RAPPORTS
set DIR_RAPPORTS ${DIR_RACINE}/rapports/

# chemin du repertoire des statistiques sauvegardees
global DIR_SAUVEGARDES_STATISTIQUES
set DIR_SAUVEGARDES_STATISTIQUES ${DIR_RACINE}/statistiques/

# chemin du repertoire pour l'execution 
global DIR_EXECUTION
set DIR_EXECUTION ${DIR_RACINE}/execution/

# chemin du repertoire temporaire de travail 
global DIR_TEMPORAIRE
set DIR_TEMPORAIRE ${DIR_RACINE}/execution/tmp/

# chemin du repertoire contenant des donnees pour les statistiques
# et les visualisations memorisees sous forme de fichiers postscript
global DIR_STATISTIQUES
set  DIR_STATISTIQUES ${DIR_RACINE}/execution/tmp/statistiques/

# chemin du repertoire contenant le rapport courant
global DIR_RAPPORTCOURANT
set DIR_RAPPORTCOURANT ${DIR_RACINE}/execution/tmp/rapport/


#####################################################################
#                Definition des noms de fichiers      
#####################################################################

# nom de l'executable du simulateur
global NOM_SIMULATEUR
set NOM_SIMULATEUR PAT

# fichier de configuration de l'exploitation 
global FIC_EXPLOITATION_TXT
set FIC_EXPLOITATION_TXT "Exploitation.cfg"

# fichiers de configuration de la strategie 
# fichier temporaire de configuration generale
global FIC_STRATEGIE_TXT
set FIC_STRATEGIE_TXT "Strategie.cfg"

# fichier des fonctions d'interpretation en lnu
global FIC_FOINTERPRETATION_LNU
set FIC_FOINTERPRETATION_LNU "FoInterpretation.lnu"

# fichier des indicateurs en lnu
global FIC_INDICATEURS_LNU
set FIC_INDICATEURS_LNU "Indicateurs.lnu"

# fichier des regles de planification en lnu
global FIC_REGLESPLANIFICATION_LNU
set FIC_REGLESPLANIFICATION_LNU "ReglesPlanification.lnu"

# fichier des regles operatoires en lnu
global FIC_REGLESOPERATOIRES_LNU
set FIC_REGLESOPERATOIRES_LNU "ReglesOperatoires.lnu"

# fichier de la strategie en lnu
global FIC_STRATEGIE_LNU
set FIC_STRATEGIE_LNU "Strategie.lnu"

# fichier des fonctions d'interpretation en c++
global FIC_FOINTERPRETATION_CC
set FIC_FOINTERPRETATION_CC "FoInterpretation.cc"

# fichier des indicateurs en c++
global FIC_INDICATEURS_CC
set FIC_INDICATEURS_CC "Indicateurs.cc"

# fichier des regles de planification en c++
global FIC_REGLESPLANIFICATION_CC
set FIC_REGLESPLANIFICATION_CC "ReglesPlanification.cc"

# fichier des regles operatoires en c++
global FIC_REGLESOPERATOIRES_CC
set FIC_REGLESOPERATOIRES_CC "ReglesOperatoires.cc"

# fichier de la strategie en c++
global FIC_STRATEGIE_H
set FIC_STRATEGIE_H "Strategie.h"

# fichier de configuration des simulations demandees 
global FIC_SIMULATEUR_TXT
set FIC_SIMULATEUR_TXT "Simulateur.cfg"

# fichier de configuration des sorties desirees 
global FIC_SORTIES_TXT
set FIC_SORTIES_TXT "Sorties.cfg"


#####################################################################
#           Definition des noms de sorties possibles     
#####################################################################

# Liste des noms de variables de sorties possibles
# Les fichiers de sorties ont le nom de la variable
global VAR_SORTIES_POSSIBLES
set VAR_SORTIES_POSSIBLES {MS MSc MSs IF Dmoy IN EAUd IH SU V_M q_l_r q_l_p Estockee FONCTIONS INDICATEURS}

# nom du fichier de sortie de la trace
global FIC_TRACE
set FIC_TRACE trace

# nom du fichier de sortie de la chronique
global FIC_CHRONIQUE
set FIC_CHRONIQUE chronique

# nom du fichier de sortie des dates clefs
global FIC_DATES_CLEFS
set FIC_DATES_CLEFS clefs

# nom du fichier decrivant les problemes rencontres pendant simulation
global FIC_PROBLEME
set FIC_PROBLEME Probleme


#####################################################################
# Variables contenant les commandes liees au syst�me d'exploitation 
#####################################################################

# destruction d'un fichier
global CMD_REMOVE
set CMD_REMOVE rm

# destruction d'un repertoire (meme non vide)
global OPT_RM_DIR
set OPT_RM_DIR "-r"

# deplacement d'un fichier
global CMD_MOVE
set CMD_MOVE mv

# copie de fichiers 
global CMD_COPY
set CMD_COPY cp

# creation d'un repertoire
global CMD_MKDIR
set CMD_MKDIR mkdir

# concatenation de fichiers
global CMD_CAT
set CMD_CAT cat

# liste des repertoires et fichiers sous un repertoire
global CMD_LS
set CMD_LS ls

# conversion de ASCII vers postscript
global CMD_A2PS
set CMD_A2PS a2ps

# Commande d'impression
global CMD_IMPRIMER
set CMD_IMPRIMER lpr
global OPT_IMPRIMER
set OPT_IMPRIMER ""


#####################################################################
#             Variables generales de l'interface             
#####################################################################
global NOM_STRATEGIE

global NOM_EXPLOITATION

# Liste des noms des actions possibles que l'on retrouve
#  dans l'agenda (Chronique des actions realisees)
global ACTIONS_ALIMENTATION 
set ACTIONS_ALIMENTATION {Paturage Foin Concentre Mais}

global APP_ACTIVITES
set APP_ACTIVITES {ActiviteAlimentationConservee \
                   ActivitePaturage \
                   ActiviteFauche \
                   ActiviteFertilisation \
		   ActiviteCoordination}

# Liste des correspondances entre variables et unites
global UNITES_VARIABLES
set UNITES_VARIABLES { {T �C} {Pluie mm} {Rg MJ/m�} {MS g/m�} {MSc g/m�} {MSs g/m�} {IF -} {Dmoy g/100g} {IN -} {EAUd mm} {IH -} {SU ha} {V_M UE} {q_l_r Kg} {q_l_p Kg} {Estockee UFL} }

# Separateur lnu
global SEPARATEUR_LNU
set SEPARATEUR_LNU #=$=#

# numero 1ere annee generee
global ANNEE_GENEREE
set ANNEE_GENEREE 3000

