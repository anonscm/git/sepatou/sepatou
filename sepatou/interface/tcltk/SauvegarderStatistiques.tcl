# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : SauvegarderStatistiques.tcl
# contenu : procedures permettant de gerer lsauvegarder des statistiques 
##################################################################

#----------------------------------------------------------------------------
# Creation de la fenetre de sauvegarde des statistiques
#----------------------------------------------------------------------------
proc SauvegarderStatistiques {} {

    global LOGICIEL
    global DIR_STATISTIQUES
    
    if {[winfo exists .wm_SauvegarderStatistiques] == 0} {
	toplevel .wm_SauvegarderStatistiques
	wm title .wm_SauvegarderStatistiques "$LOGICIEL : Sauvegarde des statistiques"
	wm geometry .wm_SauvegarderStatistiques 400x500	
	
	label .wm_SauvegarderStatistiques.l_statistique -text {STATISTIQUES REALISEES}
	pack .wm_SauvegarderStatistiques.l_statistique -padx 10 -pady 10


	### Zone de selection ###############################################
	frame .wm_SauvegarderStatistiques.f_topw -borderwidth 10
	pack .wm_SauvegarderStatistiques.f_topw -padx 20 -pady 20 -side top -expand 1

	#Creation de la Scrolled List
	ScrolledListbox .wm_SauvegarderStatistiques.f_topw.scrlst_list \
	    -width 40 -height 15 
	### Chargement des statistiques
	set list [recherche_fic $DIR_STATISTIQUES]
	set nb [llength $list]
	for  {set i 0} {$i < $nb} {incr i} {
	    set val [lindex $list $i]
	    set longeur_val [string length $val]
	    set fin [string range $val [expr $longeur_val - 3] $longeur_val]
	    if {[string compare $fin ".ps"] == 0} {
		set val [string range $val 0 [expr $longeur_val - 4]]
		.wm_SauvegarderStatistiques.f_topw.scrlst_list.lst_list insert end $val
	    }
	}
	pack .wm_SauvegarderStatistiques.f_topw.scrlst_list \
	    -padx 4 -side left -anchor n -expand 1
	
	
	###Zone de commandes du bouton fermer #############################
	
	frame .wm_SauvegarderStatistiques.f_bottom -borderwidth 3
	pack .wm_SauvegarderStatistiques.f_bottom -side bottom -fill x -pady 5
	button .wm_SauvegarderStatistiques.f_bottom.b_sauver -text Sauvegarder \
	    -command "Sauver_SauvegarderStatistiques"	
	button .wm_SauvegarderStatistiques.f_bottom.b_fermer -text Fermer \
	    -command "Fermer_SauvegarderStatistiques"
	pack .wm_SauvegarderStatistiques.f_bottom.b_sauver \
	    .wm_SauvegarderStatistiques.f_bottom.b_fermer -side left -expand 1 
	### Separateur
	frame .wm_SauvegarderStatistiques.f_sep \
	    -width 100 -height 2 -borderwidth 1 -relief sunken
	pack .wm_SauvegarderStatistiques.f_sep -side bottom -fill x -pady 5
    }
}

#----------------------------------------------------------------------------
# fermeture
#----------------------------------------------------------------------------
proc Fermer_SauvegarderStatistiques {} {

    set list_fenetres [winfo children .]
    DetruireFenetresPrefixees .wm_SauvegarderStatistiques $list_fenetres
}

#----------------------------------------------------------------------------
# sauvegarder une statistique
#----------------------------------------------------------------------------
proc Sauver_SauvegarderStatistiques {} {

    global LOGICIEL
    global DIR_STATISTIQUES
    global DIR_SAUVEGARDES_STATISTIQUES
    global CMD_COPY

    set list .wm_SauvegarderStatistiques.f_topw.scrlst_list.lst_list    
    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue $LOGICIEL error "S�lectionnez une image statistique"
    } else {
	set rep [$list get $i]
	if  [file exist ${DIR_SAUVEGARDES_STATISTIQUES}$rep.ps] {
	    set msg "Une image statistique de nom : ${rep} est d�j� sauvegard�e. Confirmez-vous la sauvegarde ?"
	    if {[Confirmer $LOGICIEL $msg] == 1} {
	    exec $CMD_COPY ${DIR_STATISTIQUES}${rep}.ps $DIR_SAUVEGARDES_STATISTIQUES
	    }
	} else {
	    exec $CMD_COPY ${DIR_STATISTIQUES}${rep}.ps $DIR_SAUVEGARDES_STATISTIQUES
	    CreerFenetreDialogue $LOGICIEL info "Sauvegarde realisee"
	} 
    }
}


