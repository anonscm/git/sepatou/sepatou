# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : VisualiserClimats.tcl
# Contenu : Affichage de la fenetre de choix des climats
#           (des climats dont on souhaite visualiser les valeurs)
##############################################################


#-------------------------------------#
# Procedure qui se charge de creer et #
#  d'afficher la fenetre de choix des #
#  climats a visualiser.              #
#-------------------------------------#
proc Fenetre_visualisation_climats {} { 

global LOGICIEL
global DIR_TEMPORAIRE
global NOM_STRATEGIE

global no_fen_climats
set no_fen_climats 0
global no_para
set no_para 0

global l_climats_simules

# Creation d'une fenetre
catch {destroy .wm_VisuClimats}
toplevel .wm_VisuClimats
wm title .wm_VisuClimats "$LOGICIEL : Visualisation des climats"

# Affichage de la station m�teo
global localc
set localc [LireNomStationMeteo]
label .wm_VisuClimats.l_station -text "Station m�t�o : $localc"
pack .wm_VisuClimats.l_station -pady 20


# La fenetre est composee du frame dow et sep
#  cales en bas, du frame top cale en haut et
#  des frames tor et tol cale respectivement
#  a droite et a gauche de la zone restante.
frame .wm_VisuClimats.f_dow -borderwidth 2
pack .wm_VisuClimats.f_dow -side bottom -pady 1
frame .wm_VisuClimats.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
pack .wm_VisuClimats.f_sep -side bottom -fill x -pady 5
frame .wm_VisuClimats.f_top -borderwidth 2
pack .wm_VisuClimats.f_top -side top -pady 10
frame .wm_VisuClimats.f_tor -borderwidth 2 -relief raised
pack .wm_VisuClimats.f_tor -side right -padx 2 -pady 4 -fill both -expand true
frame .wm_VisuClimats.f_tol -borderwidth 2 -relief raised
pack .wm_VisuClimats.f_tol -side left -padx 2 -pady 4 -fill both -expand true

# Le frame du bas correspond a Afficher et Fermer
button .wm_VisuClimats.f_dow.b_ok -text {Afficher} \
    -command Ok_VisuClimats
button .wm_VisuClimats.f_dow.b_annuler -text {Fermer}\
    -command Fermer_VisuClimats
pack .wm_VisuClimats.f_dow.b_ok .wm_VisuClimats.f_dow.b_annuler\
    -side left -expand 1 -padx 60

## Premiere scrolled List
ScrolledListbox .wm_VisuClimats.f_tol.scrlst_listVar\
    -width 10 -height 10 -setgrid true
set l_climats_simules [recherche_rep ${DIR_TEMPORAIRE}${NOM_STRATEGIE}]
foreach annee $l_climats_simules {
    .wm_VisuClimats.f_tol.scrlst_listVar.lst_list insert end $annee
}

## Deuxieme scrolled List
ScrolledListbox .wm_VisuClimats.f_tol.scrlst_listSelectVar\
    -width 10 -height 10 -setgrid true

## Troisieme scrolled List
ScrolledListbox .wm_VisuClimats.f_tor.scrlst_listVar\
    -width 10 -height 10 -setgrid true
.wm_VisuClimats.f_tor.scrlst_listVar.lst_list insert end "Pluie"
.wm_VisuClimats.f_tor.scrlst_listVar.lst_list insert end "T"
.wm_VisuClimats.f_tor.scrlst_listVar.lst_list insert end "Rg"
.wm_VisuClimats.f_tor.scrlst_listVar.lst_list insert end "Etp"

## Quatrieme scrolled List
ScrolledListbox .wm_VisuClimats.f_tor.scrlst_listSelectVar\
    -width 10 -height 10 -setgrid true


######## Remplissage du frame tol ########
Creer_titre_souligne .wm_VisuClimats.f_tol.ts_annees "Annees"

## Boutons situes entre les deux scrolled list precedentes
frame .wm_VisuClimats.f_tol.f_contener
button .wm_VisuClimats.f_tol.f_contener.b_-> -text "->" \
    -command {InsertVar .wm_VisuClimats.f_tol.scrlst_listVar.lst_list\
		  .wm_VisuClimats.f_tol.scrlst_listSelectVar.lst_list \
		  .wm_VisuClimats.f_tol.f_contener.b_-> \
		  $l_climats_simules } \
    -state disabled
button .wm_VisuClimats.f_tol.f_contener.b_<- -text "<-" \
    -command {InsertVar .wm_VisuClimats.f_tol.scrlst_listSelectVar.lst_list\
		  .wm_VisuClimats.f_tol.scrlst_listVar.lst_list \
		  .wm_VisuClimats.f_tol.f_contener.b_<- \
		  $l_climats_simules } \
    -state disabled
pack .wm_VisuClimats.f_tol.f_contener.b_-> \
    .wm_VisuClimats.f_tol.f_contener.b_<- -side top -fill both -expand true

## Actions associees aux 2 scrolled list et aux 2 boutons precedents
bind .wm_VisuClimats.f_tol.scrlst_listVar.lst_list <ButtonPress-1> \
     {ListSelectStart .wm_VisuClimats.f_tol.scrlst_listVar.lst_list %y}
bind .wm_VisuClimats.f_tol.scrlst_listVar.lst_list <B1-Motion> \
    {ListSelectExtend .wm_VisuClimats.f_tol.scrlst_listVar.lst_list %y}
bind .wm_VisuClimats.f_tol.scrlst_listVar.lst_list <ButtonRelease-1> \
    {.wm_VisuClimats.f_tol.f_contener.b_-> configure -state normal}

bind .wm_VisuClimats.f_tol.scrlst_listSelectVar.lst_list <ButtonPress-1> \
    {ListSelectStart .wm_VisuClimats.f_tol.scrlst_listSelectVar.lst_list %y}
bind .wm_VisuClimats.f_tol.scrlst_listSelectVar.lst_list <B1-Motion> \
    {ListSelectExtend .wm_VisuClimats.f_tol.scrlst_listSelectVar.lst_list %y}
bind .wm_VisuClimats.f_tol.scrlst_listSelectVar.lst_list <ButtonRelease-1> \
    {.wm_VisuClimats.f_tol.f_contener.b_<- configure -state normal}

pack .wm_VisuClimats.f_tol.scrlst_listVar .wm_VisuClimats.f_tol.f_contener \
   .wm_VisuClimats.f_tol.scrlst_listSelectVar -side left -expand true

######### Remplissage du frame tor #########

Creer_titre_souligne .wm_VisuClimats.f_tor.ts_clim "Parametres"

## Boutons situes entre les deux scrolled list precedentes
frame .wm_VisuClimats.f_tor.f_contener

button .wm_VisuClimats.f_tor.f_contener.b_-> -text "->" \
    -command {InsertVar .wm_VisuClimats.f_tor.scrlst_listVar.lst_list \
		  .wm_VisuClimats.f_tor.scrlst_listSelectVar.lst_list \
		  .wm_VisuClimats.f_tor.f_contener.b_->\
		  [list Pluie T Rg Etp] } \
    -state disabled
button .wm_VisuClimats.f_tor.f_contener.b_<- -text "<-"\
    -command {InsertVar .wm_VisuClimats.f_tor.scrlst_listSelectVar.lst_list \
		  .wm_VisuClimats.f_tor.scrlst_listVar.lst_list \
		  .wm_VisuClimats.f_tor.f_contener.b_<- \
		  [list Pluie T Rg Etp] } \
    -state disabled
pack .wm_VisuClimats.f_tor.f_contener.b_-> \
    .wm_VisuClimats.f_tor.f_contener.b_<- -side top \
    -fill both -expand true

# Actions associees aux 2 scrolled list et aux 2 boutons precedents
bind .wm_VisuClimats.f_tor.scrlst_listVar.lst_list <ButtonPress-1> \
     {ListSelectStart .wm_VisuClimats.f_tor.scrlst_listVar.lst_list %y}
bind .wm_VisuClimats.f_tor.scrlst_listVar.lst_list <B1-Motion> \
    {ListSelectExtend .wm_VisuClimats.f_tor.scrlst_listVar.lst_list %y}
bind .wm_VisuClimats.f_tor.scrlst_listVar.lst_list <ButtonRelease-1> \
    {.wm_VisuClimats.f_tor.f_contener.b_-> configure -state normal}

bind .wm_VisuClimats.f_tor.scrlst_listSelectVar.lst_list <ButtonPress-1> \
    {ListSelectStart .wm_VisuClimats.f_tor.scrlst_listSelectVar.lst_list %y}
bind .wm_VisuClimats.f_tor.scrlst_listSelectVar.lst_list <B1-Motion> \
    {ListSelectExtend .wm_VisuClimats.f_tor.scrlst_listSelectVar.lst_list %y}
bind .wm_VisuClimats.f_tor.scrlst_listSelectVar.lst_list <ButtonRelease-1> \
    {.wm_VisuClimats.f_tor.f_contener.b_<- configure -state normal}

pack .wm_VisuClimats.f_tor.scrlst_listVar -side left -expand true -fill both -anchor n
pack .wm_VisuClimats.f_tor.f_contener -side left -expand true
pack .wm_VisuClimats.f_tor.scrlst_listSelectVar -side left -expand true -fill both


}



#-------------------------------------#
# fermeture des sous fenetres
#-------------------------------------#
proc Fermer_VisuClimats {} {

    set list_fenetres [winfo children .]
    DetruireFenetresPrefixees .wm_VisuClimats $list_fenetres
}


#-------------------------------------#
# Procedure qui va appeler la fenetre #
#  d'affichage des climats souhaites. #
#-------------------------------------#
proc Ok_VisuClimats {} {

global LOGICIEL
global no_fen_climats

set nbannees [ eval [list .wm_VisuClimats.f_tol.scrlst_listSelectVar.lst_list size]]
set nbparametres [ eval [list .wm_VisuClimats.f_tor.scrlst_listSelectVar.lst_list size]]
 # On sauvegarde le contenu de la liste des annees selectionnees
set liste_annees [ eval [list .wm_VisuClimats.f_tol.scrlst_listSelectVar.lst_list get 0 $nbannees]]
 # On sauvegarde le contenu de la liste des parametres selectionnes
set liste_parametres [ eval [list .wm_VisuClimats.f_tor.scrlst_listSelectVar.lst_list get 0 $nbparametres]]

    if { $nbannees == 0} {
       # Affichage d'un premier message d'erreur
       CreerFenetreDialogue $LOGICIEL error "Aucune annee selectionnee"
    } elseif {$nbparametres == 0} {
       # Affichage d'un deuxieme message d'erreur
       CreerFenetreDialogue $LOGICIEL error "Aucun parametre selectionne"
    } else {
	for {set i 0} {$i < $nbparametres } {incr i} {
	    set parametre [lindex $liste_parametres $i]
	    incr no_fen_climats
	    Afficher_climats .wm_VisuClimats${no_fen_climats} $parametre \
		$liste_annees
	}
    }
}

#--------------------------------------------------------
# affichage d'un parametre
#--------------------------------------------------------
proc Afficher_climats {w para liste_annees} {

    global LOGICIEL
    global CMD_REMOVE
    global OPT_RM_DIR
    global DIR_CLIMATS

    global localc ;# station m�t�o
    global no_para
    incr no_para
    global var${no_para}
    global type_contenu ;# type des variables a afficher

    # lecture des donn�es
    set lecture 0

    set nb_jours 0

    set nb_clim [llength $liste_annees]
    for {set no_clim 0} {$no_clim < $nb_clim } {incr no_clim} {
	set annee [lindex $liste_annees $no_clim]

	if {$annee < 3000} {
	    # annee reelle
	    set NomFichier ${DIR_CLIMATS}$localc/${annee}.${para}
	} else {
	    set NomFichier ${para}.tmp
	    exec CREER_FICHIERS_DUN_CLIMAT_GENERE $localc [LireGraine] $annee
	}
	
	if {[catch {set fileid [open $NomFichier r]}] != 0 } {
	    set lecture 1
	    CreerFenetreDialogue $LOGICIEL error \
		"Erreur d'ouverture du fichier $NomFichier"
	} else {
	    set j 0
	    while {[eof $fileid] == 0} {
		gets $fileid line
		set var${no_para}($no_clim)(0)($j) $line
		incr j
	    }
	    # destruction des fichiers temporaire climat genere 
	    if {$annee >= 3000} {
		exec $CMD_REMOVE $OPT_RM_DIR T.tmp Rg.tmp Pluie.tmp Etp.tmp
	    }
	}
	
	if {$nb_jours == 0} {
	    set nb_jours [expr $j - 1]
	} elseif { [expr $j - 1] < $nb_jours } {
	    set nb_jours [expr $j - 1]
	}
    }

    if {$lecture == 0} {
	# affichage des donnees
	set type_contenu %numerique%
	source VisualiserSortiesVariables.tcl
	AfficherDonnees $w $para $liste_annees var${no_para} 1 vide $nb_jours 1
    }
}