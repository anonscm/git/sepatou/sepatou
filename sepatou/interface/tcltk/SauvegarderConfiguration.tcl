# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : SauvegarderConfiguration.tcl    
# Contenu : Sauvegarde de la description de l'exploitation
##############################################################

#-----------------------------------------------------------
# Ouverture d'une fenetre demandant le nom de la configuration
#-----------------------------------------------------------
proc SauvegarderConfiguration {} {

    global LOGICIEL

    catch {destroy .wm_SauvegarderConfiguration}
    toplevel .wm_SauvegarderConfiguration
    wm title .wm_SauvegarderConfiguration "$LOGICIEL : Sauvegarde de la configuration courante"

    frame .wm_SauvegarderConfiguration.f_aff
    label .wm_SauvegarderConfiguration.f_aff.l_nom -text "Nom "
    entry .wm_SauvegarderConfiguration.f_aff.e_nom 

    pack .wm_SauvegarderConfiguration.f_aff.l_nom -side left
    pack .wm_SauvegarderConfiguration.f_aff.e_nom -side right
    pack .wm_SauvegarderConfiguration.f_aff -padx 10 -pady 10

    
    ###Zone de commandes des boutons ok, annuler #############################

    frame .wm_SauvegarderConfiguration.f_bottom -borderwidth 3
    pack .wm_SauvegarderConfiguration.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button .wm_SauvegarderConfiguration.f_bottom.b_ok -text Ok \
	    -command "Ok_SauvegarderConfiguration"
    button .wm_SauvegarderConfiguration.f_bottom.b_cancel \
	-text Annuler -command "destroy .wm_SauvegarderConfiguration"
    pack .wm_SauvegarderConfiguration.f_bottom.b_ok \
         .wm_SauvegarderConfiguration.f_bottom.b_cancel -side left -expand 1 

    ### Separateur
    frame .wm_SauvegarderConfiguration.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack .wm_SauvegarderConfiguration.f_sep -side bottom -fill x -pady 5
}

#-----------------------------------------------------------
# sauvegarde de la configuration
#-----------------------------------------------------------
proc Ok_SauvegarderConfiguration {} {

    global LOGICIEL
    global CMD_COPY
    global CMD_MKDIR
    global CMD_LS
    global DIR_TEMPORAIRE
    global DIR_CONFIGURATIONS
    global FIC_EXPLOITATION_TXT
    global FIC_STRATEGIE_TXT
    global FIC_FOINTERPRETATION_LNU
    global FIC_INDICATEURS_LNU
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU
    global FIC_SIMULATEUR_TXT
    global FIC_SORTIES_TXT

    set nom [.wm_SauvegarderConfiguration.f_aff.e_nom get]

    set existe_rep [file exist ${DIR_CONFIGURATIONS}$nom]

    set confirmation 0
    if {$existe_rep == 1} {
	set msg "Voulez-vous vraiment sauvegarder en �crasant la sauvegarde existante ?"
	set confirmation [Confirmer $LOGICIEL $msg]
    } else {
	exec $CMD_MKDIR ${DIR_CONFIGURATIONS}$nom
    }

    if {$existe_rep == 0 || $confirmation == 1} {
	exec $CMD_COPY $DIR_TEMPORAIRE$FIC_EXPLOITATION_TXT ${DIR_CONFIGURATIONS}$nom
	exec $CMD_COPY $DIR_TEMPORAIRE$FIC_STRATEGIE_TXT ${DIR_CONFIGURATIONS}$nom
	exec $CMD_COPY $DIR_TEMPORAIRE$FIC_FOINTERPRETATION_LNU ${DIR_CONFIGURATIONS}$nom
	exec $CMD_COPY $DIR_TEMPORAIRE$FIC_INDICATEURS_LNU ${DIR_CONFIGURATIONS}$nom
	exec $CMD_COPY $DIR_TEMPORAIRE$FIC_REGLESPLANIFICATION_LNU ${DIR_CONFIGURATIONS}$nom
	exec $CMD_COPY $DIR_TEMPORAIRE$FIC_REGLESOPERATOIRES_LNU ${DIR_CONFIGURATIONS}$nom
	exec $CMD_COPY $DIR_TEMPORAIRE$FIC_SIMULATEUR_TXT ${DIR_CONFIGURATIONS}$nom
	exec $CMD_COPY $DIR_TEMPORAIRE$FIC_SORTIES_TXT ${DIR_CONFIGURATIONS}$nom
    }

    destroy .wm_SauvegarderConfiguration
}

