# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerStrategieFoInterpretation.tcl  
# Contenu : Description une fonction d'interpretation
######################################################################

# Une fonction est une variable de nom Fonction<n>
# avec les attributs suivants :
#    (nom_fonct) : nom
#    (code_fonct) : code
#    (commentaires_fonct) : commentaires
# La liste des fonctions est dans la variable : list_fonction

######################################################################
#Procedure   : Creer_Description_Fct_Interpretation             
#Description : Creation de la fenetre chargee de decrire une     
#              fonction d'interpretation                           
# Entrees    : Struct_Fonction : Structure contenant les informations
#                              d'une fonction d'interpretation    
#                (nom_fonct,code_fonct, commentaires_fonct)   
#              mode_visu : 'ecriture' 'lecture'
#              type_insertion : type d'insertion dans la liste des fo 
#                               'ajouter' 'editer'
#              i : position du curseur si edition
######################################################################
proc Creer_Description_Fct_Interpretation {dir Struct_Fonction mode_visu type_insertion i} {

global LOGICIEL

#Variables communes avec celles du fichier Entree_Exploitation
#Declaration du nom de la structure pour pouvoir l'editer
global $Struct_Fonction

### Creation de la fenetre
set ws .wm_Fo_$Struct_Fonction

if {[winfo exists $ws] == 0} {
    toplevel $ws

    #Titre de la fenetre
    wm title $ws "$LOGICIEL : Description d'une fonction d'interpr�tation"

###############################################################################
    ### Zone de saisie 

    frame $ws.f_top -borderwidth 3

    #Creation des libelles
    label $ws.f_top.l_nom -text Nom 
    label $ws.f_top.l_code -text Code
    label $ws.f_top.l_com -text "\n Commentaires"

    # nom
    entry $ws.f_top.e_nom -width 60 -relief sunken
    $ws.f_top.e_nom delete 0 end
 
    # code 
    frame $ws.f_top.f_code
    text $ws.f_top.f_code.txt_code \
	-width 60 -height 10 -background white \
 	-yscrollcommand "$ws.f_top.f_code.scrb_code set"
    $ws.f_top.f_code.txt_code delete 1.0 end
    scrollbar $ws.f_top.f_code.scrb_code -orient vertical \
	-command "$ws.f_top.f_code.txt_code yview"

    # commentaires 
    frame $ws.f_top.f_com
    text $ws.f_top.f_com.txt_com \
	-width 60 -height 5 -background white\
	-yscrollcommand "$ws.f_top.f_com.scrb_com set"
    $ws.f_top.f_com.txt_com delete 1.0 end
    scrollbar $ws.f_top.f_com.scrb_com -orient vertical \
	-command "$ws.f_top.f_com.txt_com yview"

    # lecture, si on edite 
    if {$type_insertion == "editer"} {
	$ws.f_top.e_nom insert end \
	    [set ${Struct_Fonction}(nom_fonct)]
	$ws.f_top.f_code.txt_code insert end \
	    [set ${Struct_Fonction}(code_fonct)]
	$ws.f_top.f_com.txt_com insert end \
	    [set ${Struct_Fonction}(commentaires_fonct)]
    }

    #affichage
    pack $ws.f_top -side top -fill both -expand 1

    pack $ws.f_top.l_nom -side top
    pack $ws.f_top.e_nom -pady 10 -side top -fill x

    pack $ws.f_top.l_code  -side top
    pack $ws.f_top.f_code.scrb_code -side right -fill y
    pack $ws.f_top.f_code.txt_code -side top -fill both -expand 1
    pack $ws.f_top.f_code  -side top  -fill both -expand 1

    pack $ws.f_top.l_com -side top
    pack $ws.f_top.f_com.scrb_com -side right -fill y
    pack $ws.f_top.f_com.txt_com -side top -fill both -expand 1
    pack $ws.f_top.f_com  -side top  -fill both -expand 1



###############################################################################
    ### Zone de commandes des boutons ok, annuler et imprimer

    frame $ws.f_bottom -borderwidth 3
    pack $ws.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button $ws.f_bottom.b_ok \
	-text Ok -command "Ok_Fct $Struct_Fonction $i $ws $type_insertion" 
    if {[string compare "lecture" $mode_visu] == 0} {
	$ws.f_bottom.b_ok configure -state disabled
    }

    button $ws.f_bottom.b_cancel \
	-text Annuler -command "destroy $ws"

    button $ws.f_bottom.b_imp \
	-text Imprimer -command "Imprimer_Fonction $dir $ws"

    pack $ws.f_bottom.b_ok $ws.f_bottom.b_cancel $ws.f_bottom.b_imp \
	-side left -expand 1 

    ### Separateur
    frame $ws.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack $ws.f_sep -side bottom -fill x -pady 5

}
}

######################################################################
#Procedure : Ok_Fct                                                  #
#Description : Selon le type choisi, met a jour les informations sur #
#              une fonction ou ajoute une nouvelle fonction          #
#Parametre : struct : Structure de la fonction d'interpretation      #
#            ind : Indice indiquant la position de l'element a       #
#                  mettre a jour (cas edition uniquement)            #
#            win : nom de la fenetre a detruire apres traitement     #
#            type : type d'insertion 'ajouter' 'editer'
######################################################################
proc Ok_Fct {struct ind win type} {

    global LOGICIEL

    #Variables communes avec celles du fichier Entree_Exploitation
    global list_Fonction
    global nb_fct
    #Declaration du contenu de la variable
    global $struct

    set nom_fonct [$win.f_top.e_nom get]
    # Verification des donnees
    if {[PbControleChaine  $nom_fonct  nom]} {
	CreerFenetreDialogue $LOGICIEL error "Nom de fonction invalide"
    } elseif  {[string length [$win.f_top.f_code.txt_code get 1.0 end]] == 1} {
	CreerFenetreDialogue $LOGICIEL error "Le code de la fonction est vide"
    } else {

	#Memorisation dans la liste creee dans le fichier Entree_Strategie.tcl
	set parent .wm_Strategie.f_midright1.scrlst_bot.lst_list

	# enregistrement
	set ${struct}(nom_fonct) $nom_fonct
	set ${struct}(code_fonct)\
	    [EpurerText [$win.f_top.f_code.txt_code get 1.0 end]]
	set ${struct}(commentaires_fonct)\
	    [EpurerText [$win.f_top.f_com.txt_com get 1.0 end]]

	if {$type == "editer"} {
	    $parent delete $ind
	    $parent insert $ind $nom_fonct
	} else {
	    ### Cas d'Ajout
	    $parent insert end $nom_fonct

	    #insertion de la fonction dans la liste des fonctions
	    if [info exist list_Fonction] {
		set list_Fonction [linsert $list_Fonction end $struct]
	    } else {
		set list_Fonction [list $struct]
	    }

	    #incrementation de l'indice pour la prochaine fonction a creer
	    set nb_fct [expr $nb_fct+1]
	}
	destroy $win
    }
}

######################################################################
#Procedure : Imprimer_Fonction                                       #
#Description : Imprime les donnees saisies par l'utilisateur         #
#Parametre : struct : Structure de la fonction                       #
#            win : nom de la fenetre appelante                       #
######################################################################
proc Imprimer_Fonction {dir win} {

    global LOGICIEL
    global SEPARATEUR_LNU
    global CMD_REMOVE
    global CMD_A2PS

    set file [open ${dir}FoInterpretation.imp w]

    puts $file $SEPARATEUR_LNU
    puts $file "# [EpurerText [$win.f_top.f_com.txt_com get 1.0 end]] #"
    puts $file $SEPARATEUR_LNU
    puts $file "FONCTION : [$win.f_top.e_nom get]"
    puts $file "CODE : "
    puts $file [EpurerText [$win.f_top.f_code.txt_code get 1.0 end]]

    close $file

    catch {exec $CMD_A2PS ${dir}FoInterpretation.imp}
    exec $CMD_REMOVE ${dir}FoInterpretation.imp

    CreerFenetreDialogue $LOGICIEL info "Impression en cours"
}


