# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##########################################################################
# Fichier : VisualiserStatistiques1Variable1.tcl
# Contenu : Affichage de la fenetre graphique de la variable V1
##########################################################################

#------------------------------------------------------------------#
# Procedure qui se charge d'afficher la fenetre 
# graphique de la variable v donne par la liste 
#       parametre: 
#               liste_variable : la liste de variable a visualiser
#-----------------------------------------------------------------#
proc VisualiserStatistiques1Variable1 {V} {

    global LOGICIEL
    global DIR_STATISTIQUES

    global l_var_statistiques
    global tab_valeurs_max_min
    global num
    global max 
    global min
    global pas
    global nb_valeurs

    # initialisation du numero de la fenetre 
    incr num

    # initialisation du min et du max
    # recherche de la ligne exacte ou se trouve le 
    # min et le max de la variable
    set i [lsearch -exact $l_var_statistiques $V]
    incr i
    set max($num) $tab_valeurs_max_min($i,2)
    set min($num) $tab_valeurs_max_min($i,1)
    set val_entr_intervalle($num) [list]
   
    # initialisation du pas
    set pas($num) [PasCourant $V $max($num) $min($num)]
   
    set var [LireFichierStatistique $V]
    for {set i 0} {$i<=[llength $var]} {incr i} {
	set v [lindex $var $i]
	if {$v>=$min($num)&&$v<=$max($num)} {
	    lappend val_entr_intervalle($num) $v
	}
    }
    set nb_valeurs [llength $val_entr_intervalle($num)]


   
    ########################### Creation de la fenetre ########################
    toplevel .wm_VisuStatistiques1Variable1$num
    wm title .wm_VisuStatistiques1Variable1$num \
	"$LOGICIEL : Visualisation statistique de $V"

    # La fenetre est compose de 6 frames,5 en haut et une en bas   
    #          _____________________
    #          |  | |         | |  |  
    #          |  | |         | |  |
    #          |  | |         | |  | 
    #          |1 |2|    3    |4| 5|
    #          |  | |         | |  | 
    #          |  | |         | |  |
    #          |  | |         | |  |
    #          ---------------------
    #          |         6         |
    #          ---------------------

    # creation des frames
    frame .wm_VisuStatistiques1Variable1$num.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    frame .wm_VisuStatistiques1Variable1$num.f_down -borderwidth 2  
    frame .wm_VisuStatistiques1Variable1$num.f_topl -borderwidth 2 \
	-width 300  -bg gray85       
    frame .wm_VisuStatistiques1Variable1$num.f_sepr -width 2 \
	-height 100  -borderwidth 1 -relief sunken
    frame .wm_VisuStatistiques1Variable1$num.f_sepl -width 2 \
	-height 100  -borderwidth 1 -relief sunken   
    frame .wm_VisuStatistiques1Variable1$num.f_topmid -borderwidth 2    
    frame .wm_VisuStatistiques1Variable1$num.f_droite   
    frame .wm_VisuStatistiques1Variable1$num.f_gauche 
    frame .wm_VisuStatistiques1Variable1$num.f_topr \
	-borderwidth 2 -width 300
    

    pack .wm_VisuStatistiques1Variable1$num.f_down \
	.wm_VisuStatistiques1Variable1$num.f_sep \
	-side bottom -fill x
    pack  .wm_VisuStatistiques1Variable1$num.f_topl \
	 -side left 
    pack .wm_VisuStatistiques1Variable1$num.f_sepl \
	-side left -fill y -padx 5
    pack .wm_VisuStatistiques1Variable1$num.f_gauche \
	-side left -padx 8 -pady 8
      
    pack .wm_VisuStatistiques1Variable1$num.f_topmid \
	-side left -padx 8 -pady 8  
   
    pack .wm_VisuStatistiques1Variable1$num.f_droite \
	-side left -padx 8 -pady 8      
    pack .wm_VisuStatistiques1Variable1$num.f_sepr\
	-side left -fill y -padx 5   
    pack .wm_VisuStatistiques1Variable1$num.f_topr \
	-side left -padx 8 -pady 8

    ###Remplissage du frame du  bas (6)
    #boutons g�n�raux        
    button .wm_VisuStatistiques1Variable1$num.f_down.b_imp \
	-text {Imprimer} -command \
	"ImprimerVisuVariable1 .wm_VisuStatistiques1Variable1$num"
    button .wm_VisuStatistiques1Variable1$num.f_down.b_mem \
	-text {M�moriser} -command \
	"MemoriserVisuVariable1 .wm_VisuStatistiques1Variable1$num"
    button .wm_VisuStatistiques1Variable1$num.f_down.b_annuler \
	-text {Fermer} -command \
	"FermerVisuVariable1 .wm_VisuStatistiques1Variable1$num"
  
    pack .wm_VisuStatistiques1Variable1$num.f_down.b_mem \
	.wm_VisuStatistiques1Variable1$num.f_down.b_imp \
	.wm_VisuStatistiques1Variable1$num.f_down.b_annuler \
	-side left  -expand 1 -padx 60 -pady 2

    ###Remplissage du frame de droite (5)
    # boutons generaux    
    button .wm_VisuStatistiques1Variable1$num.f_topr.b_moyenne \
	-text {Moyenne} -width 8 -command \
	"AfficherMoyenne1 .wm_VisuStatistiques1Variable1$num  $pas($num) $min($num) $max($num) [list $val_entr_intervalle($num)]"
    
    button .wm_VisuStatistiques1Variable1$num.f_topr.b_mediane \
	-text {M�diane} -width 8 -command \
	"AfficherMediane1 .wm_VisuStatistiques1Variable1$num  $pas($num) $min($num) $max($num) [list $val_entr_intervalle($num)]"
   
    button .wm_VisuStatistiques1Variable1$num.f_topr.b_quantiles \
	-text {Quantiles} -width 8 -command \
	"AfficherQuantiles1 .wm_VisuStatistiques1Variable1$num $V "
    pack .wm_VisuStatistiques1Variable1$num.f_topr.b_moyenne \
	.wm_VisuStatistiques1Variable1$num.f_topr.b_mediane \
	.wm_VisuStatistiques1Variable1$num.f_topr.b_quantiles -pady 15
 
    # creation des boutons de pas de discretisation
    message .wm_VisuStatistiques1Variable1$num.f_topr.m_pas \
	-width 135 -text "Pas de discretisation"
    button .wm_VisuStatistiques1Variable1$num.f_topr.b_pas_plus \
	-text {+} -command "AfficherPasAgranditVariable1 $num $V [list $val_entr_intervalle($num)]" 
    button .wm_VisuStatistiques1Variable1$num.f_topr.b_pas_moins \
	-text {-} -command "AfficherPasReduitVariable1 $num $V [list $val_entr_intervalle($num)]"
    pack .wm_VisuStatistiques1Variable1$num.f_topr.b_pas_plus \
	.wm_VisuStatistiques1Variable1$num.f_topr.m_pas \
	.wm_VisuStatistiques1Variable1$num.f_topr.b_pas_moins \
	-padx 15 -pady 5

    ###Remplissage du frame du milieu (3)
    #Mise en place de l'histogramme
    barchart .wm_VisuStatistiques1Variable1$num.f_topmid.g1 \
	-title $V -width 400 -height 400
    .wm_VisuStatistiques1Variable1$num.f_topmid.g1 config -relief groove
    pack .wm_VisuStatistiques1Variable1$num.f_topmid.g1
 
    AfficherHistogramme .wm_VisuStatistiques1Variable1$num \
	$V $val_entr_intervalle($num) $pas($num) $min($num) \
	$max($num)
    
    ###Remplissage du frame de gauche (1)
    message .wm_VisuStatistiques1Variable1$num.f_topl.b_mess \
	-width 200 -borderwidth 2 -text "Nombre de valeurs : $nb_valeurs" \
	-bg gray85
    pack .wm_VisuStatistiques1Variable1$num.f_topl.b_mess -fill x

    #creation du graph pour les boites a moustache et moyenne
    graph .wm_VisuStatistiques1Variable1$num.f_topmid.f_btes -width 400 \
	-height 100 -title "" -plotrelief flat
    if {$min($num) != $max($num)} {
	set max_x [expr $max($num) + $pas($num)/2.]
	set min_x [expr $min($num) - $pas($num)/2.]
    } else {
	set max_x [expr $max($num) + 10]
	set min_x [expr $min($num) - 10]
    }
    .wm_VisuStatistiques1Variable1$num.f_topmid.f_btes xaxis configure \
	-hide 1 -min $min_x -max $max_x
    .wm_VisuStatistiques1Variable1$num.f_topmid.f_btes yaxis configure \
	-min 0 -max 40 -color lightgrey

    pack .wm_VisuStatistiques1Variable1$num.f_topmid.f_btes -side bottom 

    ###Remplissage des frames du haut (2 et 4)
    # creation des zones de texte pour les quantiles
    text .wm_VisuStatistiques1Variable1$num.f_droite.t1\
    -width 5 -height 1 -relief flat -background lightgrey -state disabled
    text .wm_VisuStatistiques1Variable1$num.f_gauche.t1 \
    -width 5 -height 1 -relief flat -background lightgrey -state disabled
    
    #package des zones de texte  
    pack .wm_VisuStatistiques1Variable1$num.f_droite.t1 \
    -side top -padx 8
    pack .wm_VisuStatistiques1Variable1$num.f_gauche.t1 \
    -side top -padx 8
}  



#----------------------------------------------#
# fermeture des sous fenetres
#                        w: la fenetre courante
#----------------------------------------------#
proc FermerVisuVariable1 {w} {

    set list_fenetres [winfo children .]
    DetruireFenetresPrefixees $w $list_fenetres
}

#----------------------------------------------#
#  affiche la fenetre de memorisation du graphique 
#                        w: la fenetre courante
#----------------------------------------------#
proc MemoriserVisuVariable1 {w} {

    global LOGICIEL
    global DIR_STATISTIQUES

    catch {destroy ${w}Memoriser}
    toplevel ${w}Memoriser
    wm title ${w}Memoriser "$LOGICIEL : Memorisation de la sortie graphique courante"

    # recherche des fichiers .ps
    set fichiers_ps {}
    foreach f [recherche_fic $DIR_STATISTIQUES] {
	if {[regexp .ps $f]==1} {
	    lappend fichiers_ps [string range $f 0 [expr [string length $f]-4]]
	}
    }
    # affichage de la liste de fichiers
    frame ${w}Memoriser.f_aff
    label ${w}Memoriser.f_aff.l_titre  -text "Liste des fichiers existants :"
    ScrolledListbox ${w}Memoriser.f_aff.scrlst_liste -setgrid true
    foreach f $fichiers_ps {
	${w}Memoriser.f_aff.scrlst_liste.lst_list insert end $f
    }
    # zone d'entree du nom donne par l'utilisateur
    label ${w}Memoriser.f_aff.l_nom  -text "Nom "
    entry ${w}Memoriser.f_aff.e_nom 
    pack ${w}Memoriser.f_aff.l_titre -anchor e -side top -pady 8 
    pack ${w}Memoriser.f_aff.scrlst_liste \
	-side top -pady 15 -anchor e
    pack ${w}Memoriser.f_aff.l_nom -side left -anchor w
    pack ${w}Memoriser.f_aff.e_nom -side left
    pack ${w}Memoriser.f_aff -padx 10 -pady 10

  
    ###Zone de commandes des boutons ok, annuler
    frame ${w}Memoriser.f_bottom -borderwidth 3
    pack ${w}Memoriser.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button ${w}Memoriser.f_bottom.b_ok -text Ok \
	-command "OkMemoriser $w"
    button ${w}Memoriser.f_bottom.b_cancel \
	-text Annuler -command "destroy ${w}Memoriser"
    pack ${w}Memoriser.f_bottom.b_ok \
         ${w}Memoriser.f_bottom.b_cancel -side left -expand 1 

    ### Separateur
    frame ${w}Memoriser.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack ${w}Memoriser.f_sep -side bottom -fill x -pady 5
      
}

#---------------------------------------------------------------------------#
# memorisation du graphique 
#                     w: la fenetre avec la figure a memoriser
#----------------------------------------------------------------------------#
proc OkMemoriser {w} {

    global LOGICIEL
    global DIR_STATISTIQUES

    # recuperation des infos de la fenetre w
    set nom [${w}Memoriser.f_aff.e_nom get]
    set lfichierPS [${w}Memoriser.f_aff.scrlst_liste.lst_list get 0 end]

    # verification du nom
    if {[PbControleChaine $nom fichier] != 0} {
	CreerFenetreDialogue $LOGICIEL error \
	    "Le nom donne n'est pas correct"
    } elseif  {[lsearch -exact $lfichierPS $nom]!=-1} {
	# detruire la petite fenetre si elle existe deja
	catch {destroy ${w}MemoriserConfirm}
	# creation d'une fenetre 
	toplevel ${w}MemoriserConfirm
	wm title ${w}MemoriserConfirm "$LOGICIEL : Confirmation de la saisie"
	frame ${w}MemoriserConfirm.f_question
	frame ${w}MemoriserConfirm.f_sep -relief sunken \
	    -borderwidth 1 -height 2
	label ${w}MemoriserConfirm.f_question.f_icon \
	    -bitmap question -foreground red
	# demande de confirmation
	message ${w}MemoriserConfirm.f_question.f_mess \
	    -text "Vous avez choisi un nom de fichier d�j� existant ! Voulez-vous confirmer ce nom ? "
	pack ${w}MemoriserConfirm.f_question.f_icon \
	    ${w}MemoriserConfirm.f_question.f_mess \
	    -side left -pady 12 -pady 16 -expand true
	pack ${w}MemoriserConfirm.f_question \
	    ${w}MemoriserConfirm.f_sep -side top -fill x
	# bouton OK et Annuler
	button ${w}MemoriserConfirm.b_ok \
	    -text "Ok" \
	    -command "$w.f_topmid.g1 postscript output ${DIR_STATISTIQUES}$nom.ps;destroy ${w}Memoriser;destroy ${w}MemoriserConfirm" 
	pack ${w}MemoriserConfirm.b_ok \
	    -side left -pady 4 -padx 20 -fill x
	button ${w}MemoriserConfirm.b_annuler \
	    -text "Annuler" \
	    -command "destroy ${w}MemoriserConfirm"
	pack ${w}MemoriserConfirm.b_annuler -side left \
	    -padx 20 -pady 4 -fill x 
    } else {
	# enregistrement du fichier
	$w.f_topmid.g1 postscript output ${DIR_STATISTIQUES}$nom.ps
	destroy ${w}Memoriser
    }
}

#----------------------------------------------#
# impression du graphique 
#            parametre :
#                        w: la fenetre courante
#----------------------------------------------#
proc ImprimerVisuVariable1 {w} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global CMD_IMPRIMER
    global OPT_IMPRIMER
    global CMD_REMOVE

    # creation du fichier postcript correspondant a l'affichage du\
	graphique de la fenetre courante
    $w.f_topmid.g1 postscript output ${DIR_TEMPORAIRE}histogramme$w.ps
    # impression et supression du fichier
    catch {exec $CMD_IMPRIMER $OPT_IMPRIMER \
	       ${DIR_TEMPORAIRE}histogramme$w.ps}
    exec $CMD_REMOVE ${DIR_TEMPORAIRE}histogramme$w.ps
    CreerFenetreDialogue $LOGICIEL info "Impression en cours"    
}


#----------------------------------------------------------------#
# creation et package de l'histogramme
# parametres:
#            w: le graphique
#            V:  variable
#            val_entr_intervalle: les valeurs de la variable
#            pas: le pas de discretisation
#            min: le minimum de l'intervalle
#            max: le maximum de l'intervalle
#-----------------------------------------------------------------#
proc AfficherHistogramme {w V val_entr_intervalle pas min max} {
   
    # destruction de l'histogramme s'il existe deja
    catch {$w.f_topmid.g1 element delete "$V"}

    if ($max!=$min) {
	set x [Intervalles $min $max $pas  $val_entr_intervalle]
	set y [ComptageNbValeurs $min $max $pas  $val_entr_intervalle ]
	set ymax [Max $y]
	foreach t $x {
	    lappend x_arrondis [ArrondirA1Decimale $t]
	}

	$w.f_topmid.g1 configure -barwidth $pas
	$w.f_topmid.g1 element create "$V" -label "" -xdata $x -ydata $y \
	    -foreground blue     
	$w.f_topmid.g1  xaxis configure -stepsize $pas -majorticks $x_arrondis \
	    -min $min -max $max 
	$w.f_topmid.g1 yaxis configure -max $ymax
    } else {
	$w.f_topmid.g1 xaxis configure -min [expr $min - 10] -max [expr $max + 10]
	$w.f_topmid.g1 element create "$V" -label "" \
	    -xdata $min -ydata [llength $val_entr_intervalle]  -foreground blue     
    }

    if {[$w.f_topr.b_moyenne cget -relief]=="sunken"} {
	set stat_elem [CalculerStatistiquesElementaires $val_entr_intervalle]
	set et [lindex $stat_elem 4]
	set moy [lindex $stat_elem 3]
	catch { DetruireMoyenne $w}
	CreerMoyenne $w $pas $moy $et
    }

    # gestion des boutons
    if {$max == $min} {
	$w.f_topr.b_pas_moins configure -state disabled
	$w.f_topr.b_pas_plus configure -state disabled
	$w.f_topr.b_moyenne configure -state disabled
	$w.f_topr.b_mediane configure -state disabled
	$w.f_topr.b_quantiles configure -state disabled
    } else {
	if { [llength $val_entr_intervalle] == [$w.f_topmid.g1 yaxis cget -max] } {
	    $w.f_topr.b_pas_moins configure -state normal
	    $w.f_topr.b_pas_plus configure -state disabled
	} else {
	    if {[llength $x] >= 80} {
		$w.f_topr.b_pas_moins configure -state disabled
		$w.f_topr.b_pas_plus configure -state normal
	    } else {
		$w.f_topr.b_pas_moins configure -state normal
		$w.f_topr.b_pas_plus configure -state normal
	    }
	}
    }
}

#-----------------------------------------------------#
# procedure qui affiche les quantiles sur un graphique
# parametre:
#           w: la fenetre
#           variables: la liste de variables 
#-----------------------------------------------------#
proc AfficherQuantiles1 {w V} { 
     
    set etat_bquantile [$w.f_topr.b_quantiles cget -relief]
    if {$etat_bquantile == "raised"} {
	$w.f_topr.b_quantiles configure -relief sunken
	ActiverBind1 $w $V
		
    } else {
	$w.f_topr.b_quantiles configure -relief raised 
	DesactiverBind1 $w $w.f_topmid.g1

    }
	  
}

#-----------------------------------------------------#
# procedure qui affiche la boite amoustache de la moyenne
#           w: la fenetre 
#          pas: le pas de discretisation
#          min max : le minimum et le maximum de la variable
#          val_entr_intervalle: les valeurs de la variable
#-----------------------------------------------------#
proc AfficherMediane1 {w pas min max val_entr_intervalle} {

    set etat_bmediane [$w.f_topr.b_mediane cget -relief]
    if {$etat_bmediane == "raised"} {

	# calcul des statistiques elementaires
	set stat_elem [CalculerStatistiquesElementaires $val_entr_intervalle]
	set val_except [CalculerValeursExceptionnelles [lindex $stat_elem 0] \
			    [lindex $stat_elem 2] $val_entr_intervalle]
 	# mise a jour du relief du bouton
	$w.f_topr.b_mediane configure -relief sunken 
	# affichage des outils boite a moustache	
	AfficherBoiteAMoustache $w.f_topmid.f_btes "h" \
	    $min [lindex $stat_elem 0] [lindex $stat_elem 1] \
	    [lindex $stat_elem 2] $max $val_except
	message $w.f_topl.b_mess_vide1 -text "" -bg gray85
	message $w.f_topl.b_mess_mediane \
	    -text "M�diane : [ArrondirA1Decimale [lindex $stat_elem 1] ]" \
	    -foreground darkgreen -width 200 -bg gray85 
	message $w.f_topl.b_mess_IQR -text \
	    "1er quartile : [ArrondirA1Decimale [lindex $stat_elem 0] ]
3�me quartile :  [ArrondirA1Decimale [lindex $stat_elem 2] ]" \
	    -foreground forestgreen -width 200 -bg gray85
	pack $w.f_topl.b_mess_vide1 $w.f_topl.b_mess_mediane \
	    $w.f_topl.b_mess_IQR -fill x

    } else {
	# mise a jour du bouton
	$w.f_topr.b_mediane configure -relief raised
	DetruireBoiteAMoustache  $w.f_topmid.f_btes 
	destroy $w.f_topl.b_mess_mediane $w.f_topl.b_mess_IQR \
                $w.f_topl.b_mess_vide1 
    }
}
	 
#-----------------------------------------------------#
# procedure qui affiche la boite amoustache de la moyenne
#          w:la fenetre 
#          pas: le pas de discretisation
#          min max : le minimum et le maximum de la variable
#          val_entr_intervalle: les valeurs de la variable
#-----------------------------------------------------#
proc AfficherMoyenne1 {w pas min max val_entr_intervalle} {    
   
    set etat_bmoyenne [$w.f_topr.b_moyenne cget -relief]
    if {$etat_bmoyenne=="raised"} {
	set stat_elem [CalculerStatistiquesElementaires $val_entr_intervalle]
	set et [lindex $stat_elem 4]
	set moy [lindex $stat_elem 3]
	#configuration du bouton
	$w.f_topr.b_moyenne configure -relief sunken
	CreerMoyenne $w $pas $moy $et

    } else {
	$w.f_topr.b_moyenne configure -relief raised
	DetruireMoyenne $w
    }
}
#-----------------------------------------------------#
# procedure qui  cree la representation de la moyenne
#           w:la fenetre
#          pas: le pas de discretisation
#          moy: la moyenne de la variable
#           et: l'ecart-type 
#-----------------------------------------------------#
proc CreerMoyenne {w pas moy et} {

    #calcul des largeur et longueur des traits de la representation
    set demi_largeur_moyenne \
	[expr ([$w.f_topmid.g1 xaxis cget -max] - [$w.f_topmid.g1 xaxis cget -min])/100.]
    set m1 [expr $moy - $demi_largeur_moyenne]
    set m2 [expr $moy + $demi_largeur_moyenne]
    set hauteur_max [$w.f_topmid.g1 yaxis cget -max]
    set et1 [expr $moy - $et/2.]
    set et2 [expr $moy + $et/2.]
    set hauteur_et [expr $hauteur_max * 1.05]

    #creation de la repr�sentation 
    $w.f_topmid.g1 marker create polygon -name ligne_moyenne1 \
	-coords {$m1 0 $m1 $hauteur_max  $m2 $hauteur_max  $m2 0}  \
	-fill orange4 
    $w.f_topmid.g1 marker create polygon -name ligne_ecart_type1 \
	-coords {$et1 $hauteur_max $et1 $hauteur_et $et2 $hauteur_et $et2 $hauteur_max}  \
	-fill orange 
    message $w.f_topl.b_mess_vide2 -text "" -bg gray85
    message $w.f_topl.b_mess_moyenne \
	-text "Moyenne : [ArrondirA1Decimale $moy ]" \
	-foreground orange4  -width 200 -bg gray85
    message $w.f_topl.b_mess_ecart_type \
	-text "Ecart-type : [ArrondirA1Decimale $et ]" \
	-foreground orange -width 200 -bg gray85
    message $w.f_topl.b_mess_reserve \
	-text "Repr�sentation graphique sous r�serve de normalit�" \
	-width 200 -bg gray85 -foreground orange
    pack $w.f_topl.b_mess_vide2 $w.f_topl.b_mess_moyenne \
	$w.f_topl.b_mess_ecart_type  $w.f_topl.b_mess_reserve -fill x 
}

#-----------------------------------------------------#
# procedure qui detruit la representation de la moyenne
# parametre:
#           w:la fenetre
#-----------------------------------------------------#
proc DetruireMoyenne {w} {
    $w.f_topmid.g1 marker delete ligne_moyenne1
    $w.f_topmid.g1 marker delete ligne_ecart_type1
    destroy message $w.f_topl.b_mess_vide2 $w.f_topl.b_mess_moyenne \
	$w.f_topl.b_mess_reserve $w.f_topl.b_mess_ecart_type 
}

#------------------------------------------------------------#
# Procedure qui active le bind                               #
##          V: variables visualis�e #
#          w: la fenetre dans laquelle on affiche            #
#------------------------------------------------------- ----#   
proc ActiverBind1 {w V} {
   
    # initialisation des elements necessaires au calcul du pixel    
    set surface [$w.f_topmid.g1 extents plotarea]
    set ecart [lindex [$w.f_topmid.g1 cget -plotpadx] 0]
    set xmin [expr [lindex $surface 0]+$ecart]
    set xmax [expr [lindex $surface 2]+$xmin-2*$ecart] 
    set surf [lindex $surface 0]
    # enregistrement de la valeur d'un pixel
    set m [expr ([$w.f_topmid.g1 xaxis cget -max] - \
		     [$w.f_topmid.g1 xaxis cget -min]) / ($xmax - $xmin)]
  
    #mise en place du bind
    $w.f_topmid.g1 configure -borderwidth 1 -relief solid -bg lightpink
    $w.f_droite.t1 configure \
	-relief sunken -background white 
    $w.f_gauche.t1 configure \
	-relief sunken -background white 
    # mise en place du crosshairs et des quantiles
    bind $w.f_topmid.g1 <Motion> "$w.f_topmid.g1 crosshairs configure -position @%x,%y -color red; InsertQuantiles $w $m $ecart $surf $V"
}

##############################################################################
# proc�dure qui permet de calculer les quantiles et de les inserer dans la 
# zone de texte
#               w:la fenetre
#               V:la variable
#               m:la valeur d'un pixel
#               ecart:le padx  du graphique
#               surf:les marges a droite et au dessus du graphique
#              
##############################################################################
proc InsertQuantiles {w m ecart surf V} {
    $w.f_topmid.g1 crosshairs on
    set v1 [LireFichierStatistique $V]
    
    # enregistrement de la position du curseur
    set pos [$w.f_topmid.g1 crosshairs cget -position]
    set pos2 [string trim $pos @]
    set pos3 [split $pos2 ,]
    # calcul des coordonnees reelles
    set a [expr [$w.f_topmid.g1 xaxis cget -min]+ \
	       $m*[expr [lindex $pos3 0]-$surf-$ecart]]
    # appel du calcul des quantiles
    set q [CalculerQuantiles1 $v1 $a]
    set q1 [ArrondirA1Decimale [lindex $q 0]]
    set q2 [ArrondirA1Decimale [lindex $q 1]]	
    # insertion des quantiles
    $w.f_droite.t1 configure -state normal  \
	-foreground red
    $w.f_droite.t1 insert 1.0 "$q1%    "
    $w.f_droite.t1 delete 1.7 end
    $w.f_droite.t1 configure -state disabled	
    $w.f_gauche.t1 configure -state normal \
	-foreground red
    $w.f_gauche.t1 insert 1.0 "$q2%    "  
    $w.f_gauche.t1 delete 1.7 end
    $w.f_gauche.t1 configure -state disabled	
   
}

#-----------------------------------------------------------#
# Procedure qui desactive le bind                           #
# Parametre :                                               #
#        w: la fenetre                                      #
#     graph :le graphique sur lequel on applique le bind    #
#-----------------------------------------------------------#   
proc DesactiverBind1 {w graph} {
    # desactivation des affichages associes aux quantiles
      $graph crosshairs off
      $w.f_topmid.g1 configure  -relief flat -bg lightgray
      $w.f_droite.t1 configure -state normal -relief flat -background lightgrey
      $w.f_droite.t1 delete 0.0 end
      $w.f_droite.t1 configure -state disabled
      $w.f_gauche.t1 configure -state normal -relief flat -background lightgrey
      $w.f_gauche.t1 delete 0.0 end
      $w.f_gauche.t1 configure -state disabled
      bind $graph <Motion> {}
  
}


#------------------------------------------------------#
# Aggrandir pas de discretisation
#           num: le numero de la fenetre courante
#           args: la liste de variables
#-------------------------------------------------------#
proc AfficherPasAgranditVariable1 {num V val_entre_intervalle} {
    global pas  
    global min
    global max

    set pas($num) [PasAgrandit $pas($num)]
    AfficherHistogramme .wm_VisuStatistiques1Variable1$num \
	$V $val_entre_intervalle $pas($num) $min($num) $max($num)
}


#-------------------------------------------------------#
# Reduire pas de discretisation
#-------------------------------------------------------#
proc AfficherPasReduitVariable1 {num V val_entre_intervalle} {
    global pas   
    global min
    global max

    set pas($num) [PasReduit $pas($num)]
    AfficherHistogramme .wm_VisuStatistiques1Variable1$num \
	$V $val_entre_intervalle $pas($num) $min($num) $max($num)
}