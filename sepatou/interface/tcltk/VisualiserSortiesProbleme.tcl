# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : VisualiserSortiesProbleme.tcl
# Contenu : Affichage d'un fichier Probleme
##############################################################

#-----------------------------------------------------------
# affiche le contenu des fichiers Probleme
#-----------------------------------------------------------
proc Afficher_probleme {} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global FIC_PROBLEME
    global NOM_STRATEGIE

    set list_annee_probleme ""
    foreach annee [recherche_rep $DIR_TEMPORAIRE$NOM_STRATEGIE] {
	if [file exist $DIR_TEMPORAIRE$NOM_STRATEGIE/${annee}/$FIC_PROBLEME] {
	    lappend list_annee_probleme $annee
	}
    }

    if {[llength $list_annee_probleme] != 0 ||\
	    [file exist $DIR_TEMPORAIRE$FIC_PROBLEME]} {

	toplevel .wm_VisuSortiesProblemes
	wm title .wm_VisuSortiesProblemes "$LOGICIEL : Problemes rencontres lors des simulations"
    
	frame .wm_VisuSortiesProblemes.f_affichage
	text .wm_VisuSortiesProblemes.f_affichage.t -relief sunken -bd 2 \
	    -yscrollcommand ".wm_VisuSortiesProblemes.f_affichage.scrb set"
	scrollbar .wm_VisuSortiesProblemes.f_affichage.scrb \
	    -command ".wm_VisuSortiesProblemes.f_affichage.t yview"
	pack .wm_VisuSortiesProblemes.f_affichage.scrb -side right -fill y
	pack .wm_VisuSortiesProblemes.f_affichage.t -expand yes -side left -fill both
	pack .wm_VisuSortiesProblemes.f_affichage -pady 5
	
	frame .wm_VisuSortiesProblemes.f_controle
	button .wm_VisuSortiesProblemes.f_controle.b_fermer -text Fermer \
	    -command "destroy .wm_VisuSortiesProblemes"
	pack .wm_VisuSortiesProblemes.f_controle.b_fermer -padx 10
	pack .wm_VisuSortiesProblemes.f_controle -expand 1 -side bottom
	frame .wm_VisuSortiesProblemes.sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack .wm_VisuSortiesProblemes.sep  -fill x -pady 4  -expand 1 -side bottom

	# lecture des donn�es
	foreach annee $list_annee_probleme {
	    set file [open  $DIR_TEMPORAIRE$NOM_STRATEGIE/${annee}/$FIC_PROBLEME r]
	    .wm_VisuSortiesProblemes.f_affichage.t insert end [read $file]
	}
	if [file exist $DIR_TEMPORAIRE$FIC_PROBLEME] {
	    set file [open  $DIR_TEMPORAIRE$FIC_PROBLEME r]
	    .wm_VisuSortiesProblemes.f_affichage.t insert end [read $file]
	}
    }
}
