# Copyright (C) 2005 Marie-Josée Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerStrategieIndicateur.tcl 
# Contenu : Description d'un indicateur
######################################################################

# Un indicateur est une variable de nom Indicateur<n>
# avec les attributs :
#   (nom_indic) : nom
#   (car_indic) : caracteristique
#   (commentaires_indic) : commentaires
# La liste des indicateurs est dans la variable : list_indicateur

######################################################################
#Procedure : Creer_Description_Indicateur                            #
#Description : Creation de la fenetre chargee de decrire un          #
#              indicateur                                            #
#Parametre : Struct_Indicateur : Structure contenant les             #
#                                informations d'un indicateur        #
#                  (nom_indic,car_indic, commentaires_indic) 
#            mode_visu : 'lecture' 'ecriture'
#            type_insertion : 'ajouter' 'editer'
#            i : position du curseur si edition
######################################################################
proc Creer_Description_Indicateur {dir Struct_Indicateur mode_visu type_insertion i} {

global LOGICIEL

#Variables communes avec celles du fichier Entree_Exploitation
#Declaration du nom de la structure pour pouvoir l'editer
global $Struct_Indicateur

### Creation de la fenetre
set ws .wm_I_$Struct_Indicateur

if {[winfo exists $ws] == 0} {
    toplevel $ws

    #Titre de la fenetre
    wm title $ws "$LOGICIEL : Description d'un indicateur"

###############################################################################
    ### Zone de saisie du nom de l'indicateur et de ses  caracteristiques

    frame $ws.f_top -borderwidth 10

    #Creation des libelles
    label $ws.f_top.l_nom -text Nom 
    label $ws.f_top.l_car -text Caractéristique
    label $ws.f_top.l_com -text "\n Commentaires"

    # nom
    entry $ws.f_top.e_nom -width 60 -relief sunken 
    $ws.f_top.e_nom delete 0 end

    # caracteristique
    frame $ws.f_top.f_car
    text $ws.f_top.f_car.txt_car \
	-width 60 -height 10 -background white \
	-yscrollcommand "$ws.f_top.f_car.scrb_car set"
    $ws.f_top.f_car.txt_car delete 1.0 end
    scrollbar $ws.f_top.f_car.scrb_car -orient vertical \
	-command "$ws.f_top.f_car.txt_car yview"

    # commentaires
    frame $ws.f_top.f_com
    text $ws.f_top.f_com.txt_com \
	-width 60 -height 5 -background white \
	-yscrollcommand "$ws.f_top.f_com.scrb_com set"
    $ws.f_top.f_com.txt_com delete 1.0 end
    scrollbar $ws.f_top.f_com.scrb_com -orient vertical \
	-command "$ws.f_top.f_com.txt_com yview"

    # lecture texte si on edite 
    if {$type_insertion == "editer"} {
	set n [set ${Struct_Indicateur}(nom_indic)]
	$ws.f_top.e_nom insert end $n
	$ws.f_top.f_car.txt_car insert end \
	    [set ${Struct_Indicateur}(car_indic)]
	$ws.f_top.f_com.txt_com insert end \
	    [set ${Struct_Indicateur}(commentaires_indic)]
    }

    # affichage
    pack $ws.f_top -side top  -fill both -expand 1

    pack $ws.f_top.l_nom  -side top 
    pack $ws.f_top.e_nom -pady 10 -side top -fill x

    pack $ws.f_top.l_car -side top 
    pack $ws.f_top.f_car.scrb_car -side right -fill y
    pack $ws.f_top.f_car.txt_car -side top -fill both -expand 1
    pack $ws.f_top.f_car -side top -fill both -expand 1

    pack $ws.f_top.l_com -side top 
    pack $ws.f_top.f_com.scrb_com -side right -fill y
    pack $ws.f_top.f_com.txt_com -side top -fill both -expand 1
    pack $ws.f_top.f_com -side top -fill both -expand 1


###############################################################################
    ### Zone de commandes des boutons ok, annuler et imprimer

    frame $ws.f_bottom -borderwidth 3
    pack $ws.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button $ws.f_bottom.b_ok \
	-text Ok -command "Ok_Indicateur $Struct_Indicateur $i $ws $type_insertion"
    if {[string compare "lecture" $mode_visu] == 0} {
	$ws.f_bottom.b_ok configure -state disabled
    }
    button $ws.f_bottom.b_cancel \
	-text Annuler -command "destroy $ws"
    button $ws.f_bottom.b_imp \
	-text Imprimer -command "Imprimer_Indicateur $dir $ws"

    pack $ws.f_bottom.b_ok $ws.f_bottom.b_cancel $ws.f_bottom.b_imp \
	-side left -expand 1 

    ### Separateur
    frame $ws.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack $ws.f_sep -side bottom -fill x -pady 5

}
}

######################################################################
#Procedure : Ok_Indicateur                                           #
#Description : Selon le type choisi, met a jour les informations sur #
#              un indicateur ou ajoute un nouvel indicateur          #
#Parametre : struct : Structure de l'indicateur                      #
#            ind : Indice indiquant la position de l'element a       #
#                  mettre a jour (cas edition uniquement)            #
#            win : nom de la fenetre a detruire apres traitement     #
#            type : type d'insertion 'ajouter' 'editer'
######################################################################
proc Ok_Indicateur {struct ind win type} {

    global LOGICIEL

    #Variables communes avec celles du fichier Entree_Exploitation
    global list_Indicateur
    global nb_indic
    #Declaration du contenu de la variable
    global $struct

    set nom_indic [$win.f_top.e_nom get]
    # Verification des donnees
    if {[PbControleChaine $nom_indic nom]} {
	CreerFenetreDialogue $LOGICIEL error "Nom d'indicateur invalide"
    } elseif {[string length [$win.f_top.f_car.txt_car get 1.0 end]] == 1} {
	CreerFenetreDialogue $LOGICIEL error \
	    "Partie caracteristique de l'indicateur vide"
    } else {

	#Memorisation de la liste cree dans le fichier Entree_Strategie.tcl
	set parent .wm_Strategie.f_midright2.scrlst_bot.lst_list

	#Enregistrement
	set ${struct}(nom_indic) $nom_indic
	set ${struct}(car_indic) [EpurerText [$win.f_top.f_car.txt_car get 1.0 end]]
	set ${struct}(commentaires_indic)\
	    [EpurerText [$win.f_top.f_com.txt_com get 1.0 end]]

	if {$type == "editer"} {
	    $parent delete $ind
	    $parent insert $ind $nom_indic
	} else {
	    ### Cas d'Ajout
	    $parent insert end $nom_indic

	    #Insertion dans la liste list_Indicateur
	    if [info exist list_Indicateur] {
		set list_Indicateur [linsert $list_Indicateur end $struct]
	    } else {
		set list_Indicateur [list $struct]
	    }

	    #incrementation de l'indice pour le prochain indicateur a creer
	    set nb_indic [expr $nb_indic+1]
	}
	destroy $win
    }
}

######################################################################
#Procedure : Imprimer_Indicateur                                     #
#Description : Imprime les donnees saisies par l'utilisateur         #
#Parametre : struct : Structure de la indicateur                     #
#            win : nom de la fenetre appelante                       #
######################################################################
proc Imprimer_Indicateur {dir win} {

    global LOGICIEL
    global SEPARATEUR_LNU
    global CMD_REMOVE
    global CMD_A2PS

    set file [open ${dir}Indicateur.imp w]

    puts $file $SEPARATEUR_LNU
    puts $file "# [EpurerText [$win.f_top.f_com.txt_com get 1.0 end]] #"
    puts $file $SEPARATEUR_LNU
    puts $file "INDICATEUR : [$win.f_top.e_nom get]"
    puts $file "CARACTERISTIQUE : "
    puts $file [EpurerText [$win.f_top.f_car.txt_car get 1.0 end]]

    close $file

    catch {exec $CMD_A2PS ${dir}Indicateur.imp}
    exec $CMD_REMOVE ${dir}Indicateur.imp

    CreerFenetreDialogue $LOGICIEL info "Impression en cours"

}
