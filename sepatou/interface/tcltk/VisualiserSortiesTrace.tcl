# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : VisualiserSortiesTrace.tcl   
# Contenu : Affichage de la trace avec une possibilite 
#           d'appliquer un filtre : Si....alors.... 
##############################################################

#----------------------------------------------#
# Procedure qui se charge de creer la fenetre  #
#  de visualisation de la trace et qui affiche #
#  par defaut la trace sans filtre.            #
#----------------------------------------------#
# Parametres :                                 #
#    fen_tc : fenetre correspondant a la trace #
#              courante demandee,              #
#    annee : annee climatique correspondant a  #
#             la trace visualisee.             # 
#    chemin : annee comprise, finit par /
#----------------------------------------------#
proc Afficher_trace {fen_tc annee chemin} {

global LOGICIEL
global FIC_TRACE

 if [file exist $chemin$FIC_TRACE] {
   # Creation d'une fenetre
   catch {destroy $fen_tc}
   toplevel $fen_tc
   wm title $fen_tc "$LOGICIEL : Trace $annee"

   # La fenetre est composee de quatre frames 
   #  (barre, top, sep et fcommands) alignes verticalement 
   frame $fen_tc.f_barre -borderwidth 2
   pack $fen_tc.f_barre -side top

   frame $fen_tc.f_top -borderwidth 10
   pack $fen_tc.f_top -side top -fill both -expand true
   #  sep est un separateur
   frame $fen_tc.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
   pack $fen_tc.f_sep -side top -fill x -pady 5
   #  fcommands contient les commandes imprimer et fermer
   frame $fen_tc.f_fcommands
   pack $fen_tc.f_fcommands -side bottom
   button $fen_tc.f_fcommands.b_imp -text Imprimer \
          -command "Imprimer_trace $fen_tc $annee"
   button $fen_tc.f_fcommands.b_quit -text Fermer -command "destroy $fen_tc"
   pack $fen_tc.f_fcommands.b_quit $fen_tc.f_fcommands.b_imp \
          -side right -padx 60 -pady 4

   # Le frame barre est en haut de l'ecran, il est
   #  compose des boutons commandant l'affichage de la trace
   #  sans (bouton tout) ou avec filtre (bouton sialors)
   button $fen_tc.f_barre.b_tout -text {Sans filtre} \
            -command "affich 0 $fen_tc $chemin"
   button $fen_tc.f_barre.b_sialors -text {SI...ALORS} \
            -command "affich 1 $fen_tc $chemin"
   pack $fen_tc.f_barre.b_tout $fen_tc.f_barre.b_sialors -side left

   # Le frame top est lui-meme divise en deux frames :
   #  text correspond a la zone d'affichage de la trace
   #  scroll permet de faire defiler la zone d'affichage
   text $fen_tc.f_top.t_zoneaff -relief sunken -bd 2 \
       -yscrollcommand "$fen_tc.f_top.scrb_scroll set" -setgrid 1 -height 30
   scrollbar $fen_tc.f_top.scrb_scroll \
       -command "$fen_tc.f_top.t_zoneaff yview"
   pack $fen_tc.f_top.scrb_scroll -side right -fill y
   pack $fen_tc.f_top.t_zoneaff -expand yes -side left -fill both

   # Initialisation de l'affichage avec la trace entiere
   affich 0 $fen_tc $chemin

} else {

   CreerFenetreDialogue $LOGICIEL error \
       "Erreur d'ouverture du fichier $chemin$FIC_TRACE"
}

}

#---------------------------------------------------------#
# Procedure qui lit le fichier dans lequel se trouve      #
#  la trace et l'affiche avec ou sans le filtre Si..alors.#
#---------------------------------------------------------#
# Parametres :                                            #
#    filtre = 0 (soit on veut toute la trace),            #
#    filtre = 1 (on souhaite que les Si..alors),          #
#    fsup : fenetre dans laquelle s'effectue l'affichage, #
#    chemin : annee comprise, finit par /
#---------------------------------------------------------#
proc affich {filtre fsup chemin} {

global FIC_TRACE

# Ouverture du fichier trace
set fileid [open $chemin$FIC_TRACE r]
seek $fileid 0 start

# Positionnement en haut a gauche dans la zone d'affichage
$fsup.f_top.t_zoneaff mark set insert 0.0
$fsup.f_top.t_zoneaff delete 1.0 end

# Changement des etats des boutons type d'affichage
if {$filtre == 0} {
    #pas de filtre
    $fsup.f_barre.b_sialors configure -state normal
    $fsup.f_barre.b_tout configure -state disabled
} elseif {$filtre == 1} {
    # filtre SI ... ALORS ...et SI ... SINON ...
    $fsup.f_barre.b_sialors configure -state disabled
    $fsup.f_barre.b_tout configure -state normal
}

# Remplissage de la zone texte
if {$filtre == 0} {
    while {[gets $fileid line] >= 0} {
        $fsup.f_top.t_zoneaff insert "insert wordend"  $line\n
    }
} elseif {$filtre == 1} {
    set line_precedente ""
    while {[gets $fileid line] >= 0} {
	if {[string compare [lindex $line  0] "si"] == 0} {
	    set line_precedente $line
	} elseif {[string compare [lindex $line 0] "alors"] == 0||
		    [string compare [lindex $line 0] "sinon"] == 0} {
	    $fsup.f_top.t_zoneaff insert "insert wordend"  $line_precedente\n
	    $fsup.f_top.t_zoneaff insert "insert wordend" $line\n
	    set line_precedente ""
	} else {
	    $fsup.f_top.t_zoneaff insert "insert wordend"  $line\n
	}
    }
}


# Fermeture du fichier trace
close $fileid
}

#-------------------------------------------------#
#  impression de la trace
#-------------------------------------------------#
proc Imprimer_trace {w annee} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global CMD_REMOVE
    global CMD_A2PS
    
    set filetxt [open "${DIR_TEMPORAIRE}trace$annee.imp" w]
    puts $filetxt "TRACE DE L'ANNEE $annee :"
    puts $filetxt " "
    $w.f_top.t_zoneaff mark set insert 0.0
    puts $filetxt [$w.f_top.t_zoneaff get "insert linestart" end]
    close $filetxt
    # Impression du fichier
    catch {exec $CMD_A2PS ${DIR_TEMPORAIRE}trace$annee.imp}
    # Suppression du fichier
    exec $CMD_REMOVE ${DIR_TEMPORAIRE}trace$annee.imp

    CreerFenetreDialogue $LOGICIEL info "Impression en cours"
}

