# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : SauvegarderRapport.tcl    
# Contenu : Sauvegarde un rapport qui a ete cree
##############################################################

############################################################################
# Ouverture d'une fenetre demandant de valider la sauvegarde
############################################################################
proc SauvegarderRapport {} {
    
    global LOGICIEL
    global NOM_RAPPORT
    global DIR_RAPPORTS

    catch {destroy .wm_SauvegarderRapport}
    toplevel .wm_SauvegarderRapport
    wm title .wm_SauvegarderRapport "$LOGICIEL : Sauvegarde du rapport courant"

    frame .wm_SauvegarderRapport.f_aff
    label .wm_SauvegarderRapport.f_aff.l_icon \
	-bitmap question -foreground red

    # le rapport a-t-il deja ete sauvegarde,
    if {[file exist $DIR_RAPPORTS$NOM_RAPPORT] == 0} {
	message .wm_SauvegarderRapport.f_aff.msg_question \
	    -aspect 1000 -justify center \
	    -text "Voulez-vous vraiment sauvegarder \nle rapport courant : $NOM_RAPPORT ?"
    } else {
	message .wm_SauvegarderRapport.f_aff.msg_question \
	    -aspect 1000 -justify center \
	    -text "Voulez-vous vraiment sauvegarder le rapport courant : $NOM_RAPPORT en �crasant la sauvegarde existante?"
    }

    pack .wm_SauvegarderRapport.f_aff.l_icon -side left -padx 16 -pady 12
    pack .wm_SauvegarderRapport.f_aff.msg_question -padx 8 -pady 8
    pack .wm_SauvegarderRapport.f_aff

    
    ###Zone de commandes des boutons ok, annuler #############################

    frame .wm_SauvegarderRapport.f_bottom -borderwidth 3
    pack .wm_SauvegarderRapport.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button .wm_SauvegarderRapport.f_bottom.b_ok -text Ok \
	    -command "Ok_SauvegarderRapport"
    button .wm_SauvegarderRapport.f_bottom.b_cancel \
	-text Annuler -command "destroy .wm_SauvegarderRapport"
    pack .wm_SauvegarderRapport.f_bottom.b_ok \
         .wm_SauvegarderRapport.f_bottom.b_cancel -side left -expand 1 

}

############################################################################
# Description : sauvegarde le rapport
############################################################################
proc Ok_SauvegarderRapport {} {

    global DIR_RAPPORTS
    global DIR_RAPPORTCOURANT
    global CMD_REMOVE
    global OPT_RM_DIR
    global CMD_COPY
    global CMD_MKDIR
    global NOM_RAPPORT

    #Destruction du r�pertoire d�j� sauvegard� qui aurait le m�me nom que
    #celui � sauvegarder
    catch {exec $CMD_REMOVE $OPT_RM_DIR  $DIR_RAPPORTS$NOM_RAPPORT}
    
    #Cr�ation du r�pertoire ~/SEPATOU/rapports/NOM_RAPPORT
    exec $CMD_MKDIR $DIR_RAPPORTS$NOM_RAPPORT
    
    #Cr�ation d'une liste contenant tous les fichiers du 
    #r�pertoire contenant le rapport courant 
    set liste_fichier [recherche_fic $DIR_RAPPORTCOURANT]
    
    #Boucle qui copie chaque fichier de la liste 
    foreach f $liste_fichier {
	exec $CMD_COPY ${DIR_RAPPORTCOURANT}$f \
	    $DIR_RAPPORTS$NOM_RAPPORT
    }
    
    # Destruction de la fen�tre de demande de confirmation
    destroy .wm_SauvegarderRapport
}