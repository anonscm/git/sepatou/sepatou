# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : Sepatou.tcl
# contenu : interface graphique du logiciel SEPATOU
##################################################################

##### chargement des fichiers generaux ###########################
source SepatouFoUtilitaires.tcl
source SepatouVariablesGlobales.tcl


##### chargement des extensions ##################################
load /usr/lib/libTix8.4.so.1
lappend auto_path $tix_library
namespace import -force tix::*
load /usr/lib/libBLT.so
lappend auto_path $blt_library
namespace import blt::*
namespace import -force blt::tile::*


##### variables globales #########################################
global CONFIG_EXPLOITATION
global CONFIG_STRATEGIE
global CONFIG_SIMULATIONS
global CONFIG_SORTIES
global MODIF_STRATEGIE
set CONFIG_EXPLOITATION 0
set CONFIG_STRATEGIE 0
set CONFIG_SIMULATIONS 0
set CONFIG_SORTIES 0
set MODIF_STRATEGIE 0


#----------------------------------------------------------------#
# procedure qui affiche la fenetre principale
#----------------------------------------------------------------#
proc Afficher_Fenetre_Principale {} {

global LOGICIEL

###### mise a jour du titre de la fenetre principale ##############
wm title . "$LOGICIEL : aide a la determination de strategies \
            pour le paturage tournant"

##### affichage d une zone affichage texte sous le menu principal #
frame .f_affichage
text .f_affichage.t_texte -width 80 -height 30 -state disabled\
    -yscrollcommand {.f_affichage.scrb_scroll set} \
    -background #dfd ;# fond vert clair
scrollbar .f_affichage.scrb_scroll -command {.f_affichage.t_texte yview}
pack .f_affichage.scrb_scroll -side right -fill y
pack .f_affichage.t_texte -side left -fill both -expand true
pack .f_affichage -side bottom -fill both -expand true


######## declaration du menu General ###############################
menubutton .mb_General -text "G�n�ral"  -menu .mb_General.m_menu
pack .mb_General -side left
menu .mb_General.m_menu -tearoff 0
.mb_General.m_menu add separator
.mb_General.m_menu add command -label "Pr�sentation" -command General_Presentation
.mb_General.m_menu add command -label Impression -command General_Impression
.mb_General.m_menu add command -label Quitter -command General_Quitter

######## declaration du menu Configurer ###########################  
menubutton .mb_Configurer  -text Configurer  -menu .mb_Configurer.m_menu
pack .mb_Configurer -side left
menu .mb_Configurer.m_menu -tearoff 0
.mb_Configurer.m_menu add separator
.mb_Configurer.m_menu add command -label Exploitation -command Configurer_Exploitation
.mb_Configurer.m_menu add command -label "Strat�gie" -command Configurer_Strategie
.mb_Configurer.m_menu add command -label Simulations -command Configurer_Simulations
.mb_Configurer.m_menu add command -label "SortiesDesir�es"  -command Configurer_SortiesDesirees
.mb_Configurer.m_menu add separator
.mb_Configurer.m_menu add command -label ChargerExploitation -command Configurer_ChargerExploitation
.mb_Configurer.m_menu add command -label ChargerStrategie -command Configurer_ChargerStrategie
.mb_Configurer.m_menu add command -label ChargerConfiguration -command Configurer_ChargerConfiguration

######## declaration du menu Executer ############################
menubutton .mb_Executer -text "Ex�cuter"  -menu .mb_Executer.m_menu
pack .mb_Executer -side left
menu .mb_Executer.m_menu -tearoff 0
.mb_Executer.m_menu add separator 
.mb_Executer.m_menu add command -label TraduireStrategie -command Executer_TraduireStrategie
.mb_Executer.m_menu add command -label Simuler -command Executer_Simuler
.mb_Executer.m_menu add command -label "Faire Rapport" -command Executer_FaireRapport

######## declaration du menu Visualiser  ########################
menubutton .mb_Visualiser -text Visualiser  -menu .mb_Visualiser.m_menu
pack .mb_Visualiser -side left
menu .mb_Visualiser.m_menu -tearoff 0
.mb_Visualiser.m_menu add separator
.mb_Visualiser.m_menu add command -label Historiques -command Visualiser_Sorties
.mb_Visualiser.m_menu add command -label Statistiques1 -command Visualiser_Statistiques1
.mb_Visualiser.m_menu add command -label Climats -command Visualiser_Climats
.mb_Visualiser.m_menu add command -label Rapport -command Visualiser_Rapport
.mb_Visualiser.m_menu add separator
.mb_Visualiser.m_menu add command -label Configuration -command Visualiser_Configuration

######## declaration du menu Sauvegarder  ########################
menubutton .mb_Sauvegarder -text Sauvegarder  -menu .mb_Sauvegarder.m_menu
pack .mb_Sauvegarder -side left
menu .mb_Sauvegarder.m_menu -tearoff 0
.mb_Sauvegarder.m_menu add separator
.mb_Sauvegarder.m_menu add command -label Exploitation -command Sauvegarder_Exploitation
.mb_Sauvegarder.m_menu add command -label "Strat�gie" -command Sauvegarder_Strategie
.mb_Sauvegarder.m_menu add command -label Historiques -command Sauvegarder_Sorties
.mb_Sauvegarder.m_menu add command -label Statistiques -command Sauvegarder_Statistiques
.mb_Sauvegarder.m_menu add command -label Rapport -command Sauvegarder_Rapport
.mb_Sauvegarder.m_menu add separator
.mb_Sauvegarder.m_menu add command -label Configuration -command Sauvegarder_Configuration

######## declaration du menu GererSauvegardes ################################
menubutton .mb_GererSauvegardes -text "G�rerSauvegardes"  -menu .mb_GererSauvegardes.m_menu
pack .mb_GererSauvegardes -side right
menu .mb_GererSauvegardes.m_menu -tearoff 0
.mb_GererSauvegardes.m_menu add separator
.mb_GererSauvegardes.m_menu add command -label EtudesDeCas -command GererSauvegardes_EtudesDeCas
.mb_GererSauvegardes.m_menu add command -label Statistiques -command GererSauvegardes_Statistiques
.mb_GererSauvegardes.m_menu add command -label Rapports -command GererSauvegardes_Rapports
.mb_GererSauvegardes.m_menu add separator
.mb_GererSauvegardes.m_menu add command -label Configurations -command GererSauvegardes_Configurations

###### initialisation des appels possibles #########################
.mb_Executer.m_menu entryconfigure 1 -state disabled
.mb_Executer.m_menu entryconfigure 2 -state disabled
.mb_Executer.m_menu entryconfigure 3 -state disabled
.mb_Visualiser.m_menu entryconfigure 1 -state disabled
.mb_Visualiser.m_menu entryconfigure 2 -state disabled
.mb_Visualiser.m_menu entryconfigure 3 -state disabled
.mb_Visualiser.m_menu entryconfigure 4 -state disabled
.mb_Sauvegarder.m_menu entryconfigure 1 -state disabled
.mb_Sauvegarder.m_menu entryconfigure 2 -state disabled
.mb_Sauvegarder.m_menu entryconfigure 3 -state disabled
.mb_Sauvegarder.m_menu entryconfigure 4 -state disabled
.mb_Sauvegarder.m_menu entryconfigure 5 -state disabled
.mb_Sauvegarder.m_menu entryconfigure 7 -state disabled
}

#----------------------------------------------------------------#
# procedure qui decrit le logiciel
#----------------------------------------------------------------#
proc General_Presentation {} {
    global LOGICIEL

    CreerFenetreDialogue $LOGICIEL info \
       "SEPATOU est un logiciel d'aide � la d�termination de strat�gies pour conduire l'alimentation d'un troupeau de vache pratiquant le paturage tournant.

Version : v1.21 (mars 2006)"
}

#----------------------------------------------------------------#
# procedure qui enregistre la commande d'impression
#----------------------------------------------------------------#
proc General_Impression {} {
    # demande des commandes d'impression
    source GeneralImpression.tcl
    GeneralImpression
}

#----------------------------------------------------------------#
# procedure qui arrete le logiciel
#----------------------------------------------------------------#
proc General_Quitter {} {
    global LOGICIEL

    toplevel .wm_Quitter
    wm title .wm_Quitter "$LOGICIEL : Quitter"

    frame .wm_Quitter.f_aff
    label .wm_Quitter.f_aff.l_icon -bitmap question -foreground red
    message .wm_Quitter.f_aff.msg_Quitter -aspect 1000 -justify center \
    -text "Voulez-vous vraiment quitter ?"
    pack .wm_Quitter.f_aff.l_icon -side left -padx 16 -pady 12
    pack .wm_Quitter.f_aff.msg_Quitter -padx 8 -pady 8
    pack .wm_Quitter.f_aff

    frame .wm_Quitter.f_controle 
    button .wm_Quitter.f_controle.b_ok -text "Ok" -command exit
    button .wm_Quitter.f_controle.b_annuler -text "Annuler" \
	-command {destroy .wm_Quitter}
    pack .wm_Quitter.f_controle.b_ok -side left -padx 10
    pack .wm_Quitter.f_controle.b_annuler  -padx 10
    pack .wm_Quitter.f_controle -expand 1 -side bottom

    frame .wm_Quitter.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack .wm_Quitter.f_sep -fill x -pady 4  -expand 1 -side bottom

    grab .wm_Quitter
    tkwait window .wm_Quitter
    grab release .wm_Quitter
}

#----------------------------------------------------------------#
# procedure qui configure l'exploitation
#----------------------------------------------------------------#
proc Configurer_Exploitation {} {
    global CONFIG_EXPLOITATION
    global DIR_TEMPORAIRE

    # description de l'exploitation et
    # ecriture du fichier temporaire tmp/Exploitation.txt
    source ConfigurerExploitation.tcl
    Desc_Exploitation ecriture $DIR_TEMPORAIRE

    # mise a jour des appels possibles
    set CONFIG_EXPLOITATION 1
    .mb_Sauvegarder.m_menu entryconfigure 1 -state normal    
    TesterFinConfiguration

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration exploitation\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui configure la strategie
#----------------------------------------------------------------#
proc Configurer_Strategie {} {
    global CONFIG_STRATEGIE
    global DIR_TEMPORAIRE

    source ConfigurerStrategie.tcl
    Desc_Strategie ecriture $DIR_TEMPORAIRE

    # mise a jour des appels possibles
    set CONFIG_STRATEGIE 1
    .mb_Sauvegarder.m_menu entryconfigure 2 -state normal    
    TesterFinConfiguration  
 
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration strategie\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui caracterise les simulations voulues
#----------------------------------------------------------------#
proc Configurer_Simulations {} {
    global CONFIG_SIMULATIONS
    global DIR_TEMPORAIRE

    # description des simulations demandees et
    # ecriture du fichier temporaire tmp/Simulateur.txt
    source ConfigurerSimulations.tcl
    Desc_Simulation $DIR_TEMPORAIRE 1

    # mise a jour des appels possibles
    set CONFIG_SIMULATIONS 1
    TesterFinConfiguration

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration des simulations demandees\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui configure les sorties desirees
#----------------------------------------------------------------#
proc Configurer_SortiesDesirees {} {
    global CONFIG_SORTIES

    # description des sorties desirees et
    # ecriture du fichier temporaire tmp/Sorties.txt
    source ConfigurerSortiesDesirees.tcl
    Desc_Sortie

    # mise a jour des appels possibles
    set CONFIG_SORTIES 1
    TesterFinConfiguration

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration des sorties desirees\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui charge une exploitation
#----------------------------------------------------------------#
proc Configurer_ChargerExploitation {} {
    global CONFIG_EXPLOITATION

    # chargement exploitation
    source ConfigurerChargerConfiguration.tcl
    ChargerExploitation

    # mise a jour des appels possibles
    set CONFIG_EXPLOITATION 1
    TesterFinConfiguration
    .mb_Sauvegarder.m_menu entryconfigure 1 -state normal    

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Chargement d'une exploitation\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui charge une strategie
#----------------------------------------------------------------#
proc Configurer_ChargerStrategie {} {
    global CONFIG_STRATEGIE

    # chargement configuration
    source ConfigurerChargerConfiguration.tcl
    ChargerStrategie

    # mise a jour des appels possibles
    set CONFIG_STRATEGIE 1
    TesterFinConfiguration
    .mb_Sauvegarder.m_menu entryconfigure 2 -state normal    

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Chargement d'une strategie\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui charge une configuration (exploitation, strategie,
# sorties desirees, simulations voulues)
#----------------------------------------------------------------#
proc Configurer_ChargerConfiguration {} {
    global CONFIG_EXPLOITATION
    global CONFIG_STRATEGIE
    global CONFIG_SIMULATIONS
    global CONFIG_SORTIES

    # chargement configuration
    source ConfigurerChargerConfiguration.tcl
    ChargerConfiguration

    # mise a jour des appels possibles
    set CONFIG_EXPLOITATION 1
    set CONFIG_STRATEGIE 1
    set CONFIG_SIMULATIONS 1
    set CONFIG_SORTIES 1   
    TesterFinConfiguration
    .mb_Sauvegarder.m_menu entryconfigure 1 -state normal    
    .mb_Sauvegarder.m_menu entryconfigure 2 -state normal    
     .mb_Sauvegarder.m_menu entryconfigure 7 -state normal    

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Chargement d'une configuration\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}



#----------------------------------------------------------------#
# procedure qui traduit la strategie courante
#----------------------------------------------------------------#
proc Executer_TraduireStrategie {} {
    global DIR_TEMPORAIRE
    global DIR_STATISTIQUES
    global DIR_RAPPORTCOURANT
    global FIC_FOINTERPRETATION_LNU
    global FIC_INDICATEURS_LNU
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU
    global FIC_STRATEGIE_LNU
    global FIC_FOINTERPRETATION_CC
    global FIC_INDICATEURS_CC
    global FIC_REGLESPLANIFICATION_CC
    global FIC_REGLESOPERATOIRES_CC
    global FIC_STRATEGIE_H
    global NOM_STRATEGIE
    global NOM_SIMULATEUR
    global CMD_CAT
    global CMD_MKDIR
    global CMD_REMOVE
    global OPT_RM_DIR
    global MODIF_STRATEGIE

    if {$MODIF_STRATEGIE == 1} {
	busy hold .
	update
	# destruction du fichier existant, en cas de probleme en cours
	# de procedure evite une execution avec le fichier pre-existant
	if [file exist $NOM_SIMULATEUR] {
	    exec $CMD_REMOVE $OPT_RM_DIR $NOM_SIMULATEUR
	}

	# rassemblement des fichiers lnu dans un seul fichier
	exec $CMD_CAT $DIR_TEMPORAIRE$FIC_FOINTERPRETATION_LNU > $FIC_STRATEGIE_LNU
	exec $CMD_CAT $DIR_TEMPORAIRE$FIC_INDICATEURS_LNU >> $FIC_STRATEGIE_LNU
	exec $CMD_CAT $DIR_TEMPORAIRE$FIC_REGLESPLANIFICATION_LNU >> $FIC_STRATEGIE_LNU
	exec $CMD_CAT $DIR_TEMPORAIRE$FIC_REGLESOPERATOIRES_LNU >> $FIC_STRATEGIE_LNU

	# traduction strategie LnU --> C++ 
	catch {exec VERIFIER_STRATEGIE $FIC_STRATEGIE_LNU > SortieVerificateur.tmp}
	# affichage resultat
       .f_affichage.t_texte configure -state normal
       if {[catch {set file [open "SortieVerificateur.tmp" r]}] == 0} {
	   .f_affichage.t_texte insert end "\n\n"
	   .f_affichage.t_texte insert end [read $file]
	   .f_affichage.t_texte yview end
	   close $file
	   exec $CMD_REMOVE $OPT_RM_DIR SortieVerificateur.tmp
       }
       .f_affichage.t_texte configure -state disabled
 
	if [file exist OKverification] {
	    exec $CMD_REMOVE $OPT_RM_DIR OKverification 
	    catch {exec TRADUIRE_STRATEGIE $FIC_STRATEGIE_LNU > SortieTraducteur.tmp}
	    # affichage resultat
	    .f_affichage.t_texte configure -state normal
	    if {[catch {set file [open "SortieTraducteur.tmp" r]}] == 0} {
		.f_affichage.t_texte insert end [read $file]
		.f_affichage.t_texte yview end
		close $file
		exec $CMD_REMOVE $OPT_RM_DIR SortieTraducteur.tmp
	    }
	    .f_affichage.t_texte configure -state disabled

	    if [file exist FichTmp.tmp] {
		exec $CMD_REMOVE $OPT_RM_DIR FichTmp.tmp
	    }

	    if [file exist OKtraduction] {
		exec $CMD_REMOVE $OPT_RM_DIR OKtraduction 

		if [file exist FichTmp.tmp] {
		    exec $CMD_REMOVE $OPT_RM_DIR FichTmp.tmp
		}
	
		# destruction et creation de repertoires sous $DIR_TEMPORAIRE
		foreach rep [recherche_rep $DIR_TEMPORAIRE] {
		    if {([string compare ${DIR_TEMPORAIRE}${rep}/ $DIR_STATISTIQUES] != 0) &&\
			([string compare ${DIR_TEMPORAIRE}${rep}/ $DIR_RAPPORTCOURANT] != 0) } {
			exec $CMD_REMOVE $OPT_RM_DIR $DIR_TEMPORAIRE$rep
		    }
		}

		exec $CMD_MKDIR $DIR_TEMPORAIRE$NOM_STRATEGIE

		# compilation du simulateur
		exec COMPILER_SIMULATEUR > SortieCompilation.tmp
		.f_affichage.t_texte configure -state normal
		if {[catch {set file [open "SortieCompilation.tmp" r]}] == 0} {
		    .f_affichage.t_texte configure -state normal
		    .f_affichage.t_texte insert end "\n\n"
		    .f_affichage.t_texte insert end [read $file]
		    close $file
		    exec $CMD_REMOVE $OPT_RM_DIR SortieCompilation.tmp
		    .f_affichage.t_texte yview end
		}

		# destruction des fichiers temporaires crees
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_STRATEGIE_LNU
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_STRATEGIE_H
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_INDICATEURS_CC
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_FOINTERPRETATION_CC
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_REGLESPLANIFICATION_CC
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_REGLESOPERATOIRES_CC

		# affichage dans zone texte
		.f_affichage.t_texte insert end "Fin traduction strat�gie\n"
		.f_affichage.t_texte yview end
		.f_affichage.t_texte configure -state disabled

		# mise a jour des appels possibles
		.mb_Executer.m_menu entryconfigure 2 -state normal 

		set MODIF_STRATEGIE 0
	    }
	}
	busy release .
    }
    beep 100
}

#----------------------------------------------------------------#
# procedure qui realise les simulations voulues
#----------------------------------------------------------------#
proc Executer_Simuler {} {
   global LOGICIEL
   global DIR_TEMPORAIRE
   global DIR_STATISTIQUES
   global FIC_SORTIES_TXT
   global FIC_PROBLEME
   global NOM_SIMULATEUR
   global NOM_STRATEGIE
   global MODIF_STRATEGIE
   global CMD_MKDIR
   global CMD_REMOVE
   global OPT_RM_DIR

   if {$MODIF_STRATEGIE == 1} { 
	# la strategie est modifiee ##############################
       CreerFenetreDialogue $LOGICIEL error \
           "La strat�gie a �t� modifi�e, il faut la traduire avant de simuler."

    } else {
	# la strategie n'a pas ete modifiee ################################
	# destruction et creation de repertoires sous $DIR_TEMPORAIRE
	foreach rep [recherche_rep $DIR_TEMPORAIRE$NOM_STRATEGIE] {
	    exec $CMD_REMOVE $OPT_RM_DIR $DIR_TEMPORAIRE${NOM_STRATEGIE}/$rep
	}

	if [file exist $DIR_TEMPORAIRE$FIC_PROBLEME] {
	    exec $CMD_REMOVE $OPT_RM_DIR $DIR_TEMPORAIRE$FIC_PROBLEME
	}

	# on enleve les fichiers memorisant des graphiques
	# dont le nom ne contient pas .pas
	foreach f [recherche_fic $DIR_STATISTIQUES] {
	    if {[regexp .ps $f] == 0} {
	    exec $CMD_REMOVE $OPT_RM_DIR $DIR_STATISTIQUES$f
	    }
	}

	if {[LireNbAnneesAGenerer] <= 500} {
	    foreach annee [ListerAnneesASimuler] {
		exec $CMD_MKDIR $DIR_TEMPORAIRE${NOM_STRATEGIE}/$annee        
	    }
	} else {
	    set demande_statistiques 0
	    set nb_sorties 0
	    set file [open "$DIR_TEMPORAIRE$FIC_SORTIES_TXT" r]
	    while { [gets $file line] > 0} {
		incr nb_sorties;
		if {[string compare $line STATISTIQUES] == 0} {
		    set demande_statistiques 1
		}
	    }

	    if {$demande_statistiques == 0 || \
		    ($demande_statistiques == 1 && $nb_sorties > 1)} {
		CreerFenetreDialogue $LOGICIEL info \
		    "Les statistiques seules sont toujours enregistr�es lorsque plus de 500 ann�es sont simul�es"
		# on vide le fichier des sorties desirees
		set file [open "${DIR_TEMPORAIRE}$FIC_SORTIES_TXT" w]
		puts $file STATISTIQUES
		close $file
	    }
	}

	# lancements des simulations
	exec $NOM_SIMULATEUR > SortieSimulateur.tmp  
	if {[catch {set file [open "SortieSimulateur.tmp" r]}] == 0} {
	    .f_affichage.t_texte configure -state normal
	    .f_affichage.t_texte insert end "\n\n"
	    .f_affichage.t_texte insert end [read $file]
	    close $file
	    exec $CMD_REMOVE $OPT_RM_DIR SortieSimulateur.tmp
	    .f_affichage.t_texte yview end
	    .f_affichage.t_texte configure -state disabled
	}
	
	# mise a jour des appels possibles
	.mb_Executer.m_menu entryconfigure 3 -state normal
	.mb_Visualiser.m_menu entryconfigure 1 -state normal 
	.mb_Visualiser.m_menu entryconfigure 2 -state normal 
	.mb_Visualiser.m_menu entryconfigure 3 -state normal 
	.mb_Sauvegarder.m_menu entryconfigure 3 -state normal 
	.mb_Sauvegarder.m_menu entryconfigure 4 -state normal 
	
	# affichage dans zone texte
	.f_affichage.t_texte configure -state normal
	.f_affichage.t_texte insert end "Realisation des simulations demandees\n"
	.f_affichage.t_texte yview end
	.f_affichage.t_texte configure -state disabled
    }
    beep 100
}

#----------------------------------------------------------------#
# procedure qui realise un rapport
#----------------------------------------------------------------#
proc Executer_FaireRapport {} {
    
    source ExecuterFaireRapport.tcl
    ExecuterFaireRapport

    # mise a jour des appels possibles
    .mb_Visualiser.m_menu entryconfigure 4 -state normal  
    .mb_Sauvegarder.m_menu entryconfigure 5 -state normal 

    #affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Realisation du rapport\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled    
}

#----------------------------------------------------------------#
# procedure qui visualise les sorties
#----------------------------------------------------------------#
proc Visualiser_Sorties {} {

    source VisualiserSorties.tcl
    Fenetre_visualisation_sorties
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Visualisation des sorties\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui visualise les statistiques 1
#----------------------------------------------------------------#
proc Visualiser_Statistiques1 {} {

    source VisualiserStatistiques1.tcl
    VisualiserStatistiques1
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Visualisation de statistiques1\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui visualise les parametres climatiques
#----------------------------------------------------------------#
proc Visualiser_Climats {} {

    source VisualiserClimats.tcl
    Fenetre_visualisation_climats

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Visualisation des climats\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# peocedure qui visualise un rapport
#----------------------------------------------------------------#
proc Visualiser_Rapport {} {

    global DIR_RAPPORTCOURANT
    global NOM_RAPPORT

    source ExecuterFaireRapport.tcl
    VisualiserRapport $DIR_RAPPORTCOURANT $NOM_RAPPORT

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Visualisation du rapport courant\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}


#----------------------------------------------------------------#
# procedure qui visualise les elements d'une configuration
#----------------------------------------------------------------#
proc Visualiser_Configuration {} {

    global DIR_TEMPORAIRE

    source VisualiserConfiguration.tcl
    VisualiserConfiguration .wm_VisualiserConfiguration $DIR_TEMPORAIRE \
	"configuration courante"
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Visualisation de la configuration\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui sauvegarde une exploitation
#----------------------------------------------------------------#
proc Sauvegarder_Exploitation {} {

    source SauvegarderExploitation.tcl
    SauvegarderExploitation

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Sauvegarde de la description de l'exploitation\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui sauvegarde une strategie
#----------------------------------------------------------------#
proc Sauvegarder_Strategie {} {

    source SauvegarderStrategie.tcl
    SauvegarderStrategie
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Sauvegarde de la description de la strategie\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui sauvegarde des sorties
#----------------------------------------------------------------#
proc Sauvegarder_Sorties {} {

    source SauvegarderSorties.tcl
    SauvegarderSorties
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Sauvegarde des sorties obtenues\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui sauvegarde des statistiques
#----------------------------------------------------------------#
proc Sauvegarder_Statistiques {} {

    source SauvegarderStatistiques.tcl
    SauvegarderStatistiques
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Sauvegarde des statistiques\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui sauvegarde le rapport courant
#----------------------------------------------------------------#
proc Sauvegarder_Rapport {} {
    
    source SauvegarderRapport.tcl
    SauvegarderRapport
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Sauvegarde du rapport courant\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui sauvegarde une configuration
#----------------------------------------------------------------#
proc Sauvegarder_Configuration {} {

    source SauvegarderConfiguration.tcl
    SauvegarderConfiguration
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Sauvegarde de la configuration\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui gere les cas sauvegardes (exploitations,
# strategies, sorties)
#----------------------------------------------------------------#
proc GererSauvegardes_EtudesDeCas {} {

    source GererSauvegardesEtudesDeCas.tcl
    GererSauvegardesEtudesDeCas
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Gestion des �tudes de cas sauvegard�es\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui gere les statistiques sauvegardees
#----------------------------------------------------------------#
proc GererSauvegardes_Statistiques {} {

    source GererSauvegardesStatistiques.tcl
    GererSauvegardesStatistiques
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Gestion des statistiques sauvegard�es\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui gere les rapports sauvegardees
#----------------------------------------------------------------#
proc GererSauvegardes_Rapports {} {

    source GererSauvegardesRapports.tcl
    GererSauvegardesRapports
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Gestion des rapports sauvegard�s\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui gere les configurations sauvegardees
#----------------------------------------------------------------#
proc GererSauvegardes_Configurations {} {

    source GererSauvegardesConfigurations.tcl
    GererSauvegardesConfigurations
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Gestion des configurations\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}


#----------------------------------------------------------------#
# procedure qui teste si une configuration complete a ete decrite
# et qui met a jour le menu principal
#----------------------------------------------------------------#
proc TesterFinConfiguration {} {
    global CONFIG_EXPLOITATION
    global CONFIG_STRATEGIE
    global CONFIG_SIMULATIONS
    global CONFIG_SORTIES
    if {$CONFIG_EXPLOITATION == 1} {
	if {$CONFIG_STRATEGIE == 1} {
	    if {$CONFIG_SIMULATIONS == 1} {
		if {$CONFIG_SORTIES == 1} {
		    .mb_Executer.m_menu entryconfigure 1 -state normal 
		    .mb_Sauvegarder.m_menu entryconfigure 7 -state normal }}}}
}



#----------------------------------------------------------------#
# appel de la fenetre principale
#----------------------------------------------------------------#
Afficher_Fenetre_Principale