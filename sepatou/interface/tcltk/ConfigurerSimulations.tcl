# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerSimulations.tcl  
# Contenu : Description des entrees du simulateur
######################################################################


######################################################################
#Procedure : Desc_Simulation                
#            dir_tmp : r�pertoire o� sera lu le fichier FIC_SIMULATEUR_TXT
#            annees_reelles : 0 = pas d'afficahge des ann�es r�elles
#                             1 = afficahge des ann�es r�elles
#Description : Cree la fenetre chargee de decrire les entrees du     #
#              simulateur                                            #
######################################################################
proc Desc_Simulation {dir_tmp annees_reelles} {

global LOGICIEL
global DIR_CLIMATS

global jourd
global moisd
global jourf
global moisf
global station_meteo
global graine
global nb_annees

# initialisation des variables globales relatives aux simulations
set jourd ""
set moisd ""
set jourf ""
set moisf ""
set station_meteo ""
set graine ""
set nb_annees ""


###############################################################################
### Creation de la fenetre

if {[winfo exists .wm_Simulation] == 0} {
    toplevel .wm_Simulation

    #Titre de la fenetre
    wm title .wm_Simulation "$LOGICIEL : Description des simulations demandees"

###############################################################################
    ### Zones de saisie des dates

    frame .wm_Simulation.f_top 
    frame .wm_Simulation.f_top.f_left 
    frame .wm_Simulation.f_top.f_right 

    ### Remplissage du cadre de gauche

    #Creation du titre
    label .wm_Simulation.f_top.f_left.l_date_deb -text {Date d�but simulation}

    frame .wm_Simulation.f_top.f_left.f_date

    #Creation des libelles
    label .wm_Simulation.f_top.f_left.f_date.l_jj -text {Jour}
    label .wm_Simulation.f_top.f_left.f_date.l_mm -text {Mois}

    #Creation des OptionMenu
    tixOptionMenu .wm_Simulation.f_top.f_left.f_date.opm_jj \
	-options {menubutton.width 3} -variable jourd
    tixOptionMenu .wm_Simulation.f_top.f_left.f_date.opm_mm \
	-options {menubutton.width 3} -variable moisd

    Num .wm_Simulation.f_top.f_left.f_date.opm_jj 1 1
    Num .wm_Simulation.f_top.f_left.f_date.opm_mm 2 2

    #Gestion des libelles et des OptionMenu
    pack .wm_Simulation.f_top.f_left.f_date.l_jj \
	.wm_Simulation.f_top.f_left.f_date.opm_jj \
	.wm_Simulation.f_top.f_left.f_date.l_mm \
	.wm_Simulation.f_top.f_left.f_date.opm_mm \
	-padx 10 -pady 10 -side left


    pack .wm_Simulation.f_top.f_left.l_date_deb\
	.wm_Simulation.f_top.f_left.f_date \
        -side top -expand 1 -anchor w

###############################################################################
    ### Remplissage du cadre de droite

    #Creation du titre
    label .wm_Simulation.f_top.f_right.l_date_fin -text {Date fin simulation}

    frame .wm_Simulation.f_top.f_right.f_date

    #Creation des libelles
    label .wm_Simulation.f_top.f_right.f_date.l_jj -text {Jour}
    label .wm_Simulation.f_top.f_right.f_date.l_mm -text {Mois}

    #Creation des OptionMenu
    tixOptionMenu .wm_Simulation.f_top.f_right.f_date.opm_jj \
	-options {menubutton.width 3} -variable jourf
    tixOptionMenu .wm_Simulation.f_top.f_right.f_date.opm_mm \
	-options {menubutton.width 3} -variable moisf

    Num .wm_Simulation.f_top.f_right.f_date.opm_jj 1 1
    Num .wm_Simulation.f_top.f_right.f_date.opm_mm 8 8

    #Gestion des libelles et des OptionMenu
    pack .wm_Simulation.f_top.f_right.f_date.l_jj \
	.wm_Simulation.f_top.f_right.f_date.opm_jj \
	.wm_Simulation.f_top.f_right.f_date.l_mm \
	.wm_Simulation.f_top.f_right.f_date.opm_mm \
	-padx 10 -pady 10 -side left

    pack .wm_Simulation.f_top.f_right.l_date_fin\
	.wm_Simulation.f_top.f_right.f_date \
        -side top -expand 1 -anchor w

    #Gestion du cadre de gauche et du cadre de droite
    pack .wm_Simulation.f_top.f_left .wm_Simulation.f_top.f_right \
	-side left -expand 1 -padx 5 -pady 10

    pack .wm_Simulation.f_top -side top -padx 5 -expand 1 -anchor n


###############################################################################
    ### Zone de commandes des boutons ok et annuler

    frame .wm_Simulation.f_bottom -borderwidth 3

    #Creation des boutons
    button .wm_Simulation.f_bottom.b_ok \
	-text Ok -command "Enregistrer_Simulation $dir_tmp\
.wm_Simulation.f_midleft.f_clim.f_mid.lst_an_r"

    button .wm_Simulation.f_bottom.b_cancel \
	-text Annuler -command "destroy .wm_Simulation"
    pack .wm_Simulation.f_bottom.b_ok .wm_Simulation.f_bottom.b_cancel \
	-padx 20 -pady 10 -side left -expand 1

    pack .wm_Simulation.f_bottom -side bottom -anchor s -fill x -pady 5

    ### Separateur
    frame .wm_Simulation.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack .wm_Simulation.f_sep -side bottom -fill x -pady 5

###############################################################################
    ### Cadre alea climats

    frame .wm_Simulation.f_midleft

    #Creation du titre
    label .wm_Simulation.f_midleft.l_clim -text {ALEA CLIMATS}

    #Creation d'un cadre visible
    frame .wm_Simulation.f_midleft.f_clim -borderwidth 3 -relief groove

    #Creation de tous les frames contenus dans le cadre visible
    frame .wm_Simulation.f_midleft.f_clim.f_top ;#station meteo
    frame .wm_Simulation.f_midleft.f_clim.f_mid ;#annees reelles
    frame .wm_Simulation.f_midleft.f_clim.f_bot ;#Frame des saisies
    frame .wm_Simulation.f_midleft.f_clim.f_bot.f_l ;#Frame bas-gauche
    frame .wm_Simulation.f_midleft.f_clim.f_bot.f_r ;#Frame bas-droite

    #Creation des libelles
    label .wm_Simulation.f_midleft.f_clim.f_top.l_meteo -text {Station m�t�o}
    if {$annees_reelles == 1} {
	label .wm_Simulation.f_midleft.f_clim.f_mid.l_an_r \
	    -text {Ann�es r�elles}
    }
    label .wm_Simulation.f_midleft.f_clim.f_bot.f_l.l_an_g \
	-text {Nb ann�es g�n�r�es}
    label .wm_Simulation.f_midleft.f_clim.f_bot.f_l.l_grain -text {Graine}

    #ComboBox pour le choix de la station meteo
    tixComboBox .wm_Simulation.f_midleft.f_clim.f_top.lst_meteo \
	-variable station_meteo \
	-command Rechercher_Annees_Reelles 

    foreach nom_station [recherche_rep $DIR_CLIMATS] {
      .wm_Simulation.f_midleft.f_clim.f_top.lst_meteo insert end $nom_station
    }


    #Listbox pour le choix des annees reelles
    eval {listbox  .wm_Simulation.f_midleft.f_clim.f_mid.lst_an_r \
	      -yscrollcommand [list \
		      .wm_Simulation.f_midleft.f_clim.f_mid.scrb_y set]} \
	-width 10 -selectmode multiple

    #Creation de la scrollbar et programmation du reflexe
    scrollbar .wm_Simulation.f_midleft.f_clim.f_mid.scrb_y \
	-orient vertical \
	-command [list .wm_Simulation.f_midleft.f_clim.f_mid.lst_an_r yview]

    #Zone de saisies
    entry .wm_Simulation.f_midleft.f_clim.f_bot.f_r.e_an_g \
	-width 10 -textvariable nb_annees
    entry .wm_Simulation.f_midleft.f_clim.f_bot.f_r.e_grain \
	-width 10 -textvariable graine

    #Gestion des libelles avec leurs champs dans leurs frames respectifs
    pack .wm_Simulation.f_midleft.f_clim.f_top.l_meteo \
	.wm_Simulation.f_midleft.f_clim.f_top.lst_meteo \
	-side left -padx 5

    if {$annees_reelles == 1} {
	#gestion du scrollbar
	pack .wm_Simulation.f_midleft.f_clim.f_mid.scrb_y -side right -fill y
    
	pack  .wm_Simulation.f_midleft.f_clim.f_mid.l_an_r \
	    .wm_Simulation.f_midleft.f_clim.f_mid.lst_an_r \
	    -side left -padx 5
    }

    pack .wm_Simulation.f_midleft.f_clim.f_bot.f_l.l_an_g \
	.wm_Simulation.f_midleft.f_clim.f_bot.f_r.e_an_g \
	-side left -padx 5

    pack .wm_Simulation.f_midleft.f_clim.f_bot.f_l.l_grain \
	.wm_Simulation.f_midleft.f_clim.f_bot.f_r.e_grain \
	-side left -padx 5

    #Gestion entre tous les frames / frame alea climats
    pack .wm_Simulation.f_midleft.f_clim.f_bot.f_l.l_an_g \
	.wm_Simulation.f_midleft.f_clim.f_bot.f_l.l_grain \
	-side top -anchor w -pady 3
    pack .wm_Simulation.f_midleft.f_clim.f_bot.f_r.e_an_g \
	.wm_Simulation.f_midleft.f_clim.f_bot.f_r.e_grain \
	-side top -pady 3

    pack .wm_Simulation.f_midleft.f_clim.f_bot.f_l \
	.wm_Simulation.f_midleft.f_clim.f_bot.f_r\
	-side left -padx 3 -expand 1 -anchor w

    pack .wm_Simulation.f_midleft.f_clim.f_top .wm_Simulation.f_midleft.f_clim.f_mid \
	.wm_Simulation.f_midleft.f_clim.f_bot \
	-side top -anchor w -pady 5

    #Gestion du titre "Alea climats avec son cadre visible
    pack .wm_Simulation.f_midleft.l_clim .wm_Simulation.f_midleft.f_clim \
	-side top -anchor w -pady 5 -expand 1
    pack .wm_Simulation.f_midleft \
	-side left -anchor n -padx 15 -pady 30 -expand 1


    ### Chargement a partir d'un fichier
    Charger_Dft_Simul $dir_tmp
}

}

######################################################################
#Procedure : Charger_Dft_Simul                                       #
#Description : Charge une description de simulation par defaut       #
######################################################################
proc Charger_Dft_Simul {dir_tmp} {

    global FIC_SIMULATEUR_TXT
    global jourd
    global moisd
    global jourf
    global moisf
    global station_meteo
    global graine
    global nb_annees

    set list .wm_Simulation.f_midleft.f_clim.f_mid.lst_an_r

    #Ouverture du fichier de chargement
    if {[catch {set file [open "${dir_tmp}$FIC_SIMULATEUR_TXT" r]}] \
	== 0} {

	    seek $file 0 start

	    gets $file line
	    set jourd [SortirValeur $line]
	    gets $file line
	    set moisd [SortirValeur $line]
	    gets $file line
	    set jourf [SortirValeur $line]
	    gets $file line
	    set moisf [SortirValeur $line]
	    gets $file line
	    set station_meteo [SortirValeur $line]
	    gets $file line
	    set nb_annees [SortirValeur $line]
	    gets $file line
	    set graine [SortirValeur $line]

	    #recherche des dates selectionnees par defaut
	    set cpt [$list size]
	    gets $file annee
	    while {[eof $file] != 1} {
		set i 0
		set trouve 0
		while {($i < $cpt) || ($trouve != 1)} {		    
		    if {[string first $annee [$list get $i]] != -1} {
			#Selection de la date dans la list
			$list selection set $i
			set trouve 1
		    }
		    incr i
		}
		gets $file annee
	    }
	}

}

######################################################################
#Procedure : Rechercher_Annees_Reelles                                   #
#Description : Procedure servant a rechercher les annees reelles     #
#              sous un repertoire donne                              #
######################################################################
proc Rechercher_Annees_Reelles {station} {
   
    global DIR_CLIMATS

    set l_annees .wm_Simulation.f_midleft.f_clim.f_mid.lst_an_r

    set liste ""
    set liste_fichiers [recherche_fic $DIR_CLIMATS$station]
    foreach fic $liste_fichiers {
	set point [string first "." $fic]
	if {$point != -1} {
	    set annee [string range $fic 0 [expr $point -1]]
	    if {[string index $annee 0] == "1" || 
		[string index $annee 0] == "2"} {
		if {[lsearch $liste $annee] == -1 } {
		    lappend liste $annee
		}
	    }
	}
    }

    # On a maintenant une liste d'annees que l'on recopie
    #  dans la liste  affichee a l'ecran
    $l_annees delete 0 end
    for {set j 0} {$j < [llength $liste]} {incr j} {
	$l_annees insert end [lindex $liste $j]
    }
}

######################################################################
#Procedure : Enregistrer_Simulation                                  #
#Description : Procedure servant a enregistrer les parametres de     #
#              simulation                                            #
#parametre : list : nom de la liste dont on extrait les annees       #
#                   reelles selectionnees pour les enregistrer       #
######################################################################
proc Enregistrer_Simulation {dir_tmp list} {

    global FIC_SIMULATEUR_TXT
    global LOGICIEL

    global jourd
    global moisd
    global jourf
    global moisf
    global station_meteo
    global graine
    global nb_annees

    ### Verification des saisies

    set annul 0

    if {[PbControleChaine $nb_annees entier]} {
	set annul 1
	set msg_err "Le nombre d'ann�es g�n�r�es doit etre un entier"
    } elseif {[PbControleChaine $graine entier]} {
	set annul 1
	set msg_err "La graine doit etre un entier"
    } elseif {$moisd > $moisf} {
	set annul 1
	set msg_err "Erreur : mois debut simulation > mois fin simulation"
    } elseif {$moisd == $moisf && $jourd >= $jourf} {
	set annul 1
	set msg_err " Erreur : date debut simulation > date fin simulation"
    } elseif {$nb_annees < 0 || $nb_annees > 9000000} {
	set annul 1
	set msg_err "Erreur : Le nombre d'ann�es g�n�r�es doit etre compris entre 0 et 9 000 000"
    } elseif {$graine < 0 || $graine > 100} {
	set annul 1
	set msg_err "Erreur : La graine doit etre comprise entre 0 et 100"
    }

    ### Enregistrement

    if {$annul == 0} {
	set file [open "${dir_tmp}$FIC_SIMULATEUR_TXT" w]
	seek $file 0 start 

	#Ecriture des donnees
	set line ""; append line "JourDebutSimulation = " $jourd
	puts $file $line
	set line ""; append line "MoisDebutSimulation = " $moisd
	puts $file $line
	set line ""; append line "JourFinSimulation = " $jourf
	puts $file $line
	set line ""; append line "MoisFinSimulation = " $moisf
	puts $file $line
	set line ""; append line "StationMeteo = " $station_meteo
	puts $file $line
	# ATTENTION necessit� d'enlever les zeros a gauche, sinon
	# lorsqu'on teste le nombre d'annees de simulation demandees
	# le test 0501 > 500 donne FAUX
	set line ""; append line "NbAnneesGenerees = " [EnleverZerosAGauche $nb_annees]
	puts $file $line
	set line ""; append line "Graine = " $graine
	puts $file $line


	# annees reelles choisies 
	foreach i [$list curselection] {
	    puts $file [$list get $i]
	}

	close $file

	#Fermeture de la fenetre principale
	destroy .wm_Simulation
    } else {
	CreerFenetreDialogue $LOGICIEL error $msg_err
    }
	    
}
