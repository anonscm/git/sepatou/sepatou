# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

############################################################################
# Fichier : ExecuterFaireRapport.tcl
# Contenu : fichier contenant les proc�dures permettant de r�aliser un rapport
############################################################################

############################################################################
# Procedure : ExecuterFaireRapport
# Description : cree la fenetre qui permet de creer un rapport
# Parametres : aucun
############################################################################
proc ExecuterFaireRapport {} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global DIR_STATISTIQUES
    global DIR_RAPPORTCOURANT
    global FIC_PROBLEME
    global FIC_TRACE
    global FIC_CHRONIQUE
    global FIC_DATES_CLEFS
    global VAR_SORTIES_POSSIBLES
    global NOM_STRATEGIE

    # listes utilisees dans les boutons -> et <-
    global list_an_poss
    global list_an_retenues
    global l_annees_simulees
    global list_var
    global list_var_retenues
    global l_totale_sorties_simulees
    global list_stat
    global list_stat_retenues
    global l_stat_realisees

    # checkbuttons, apparemment pas de possibilite 
    # de connaitre l'etat sans passer par variable globale
    global ExpRap  
    global StratRap
    global ChronRap
    global AlimRap
    
    
    #Creation de la fenetre
    catch {destroy .wm_FaireRapport}
    toplevel .wm_FaireRapport
    wm title .wm_FaireRapport "$LOGICIEL : Creation de rapport"
    
    ### Zone de commandes des boutons ####################
    frame .wm_FaireRapport.f_bottom
    
    #Creation des boutons
    button .wm_FaireRapport.f_bottom.b_Realiser -text R�aliser \
	-command "RealiserRapport .wm_FaireRapport"
    button .wm_FaireRapport.f_bottom.b_Modifier -text Modifier \
	-state disabled -command ModifierRapport
    button .wm_FaireRapport.f_bottom.b_Visualiser -text Visualiser \
	-state disabled -command {global NOM_RAPPORT; VisualiserRapport $DIR_RAPPORTCOURANT $NOM_RAPPORT}
    button .wm_FaireRapport.f_bottom.b_Imprimer -text Imprimer \
	-state disabled -command ImprimerRapport 
    button .wm_FaireRapport.f_bottom.b_Fermer -text Fermer \
	-command FermerRapport 
    
    pack .wm_FaireRapport.f_bottom.b_Realiser \
	.wm_FaireRapport.f_bottom.b_Modifier \
	.wm_FaireRapport.f_bottom.b_Visualiser \
        .wm_FaireRapport.f_bottom.b_Imprimer \
	.wm_FaireRapport.f_bottom.b_Fermer \
	-padx 20 -pady 10 -side left -expand 1
    pack .wm_FaireRapport.f_bottom -side bottom -anchor s -fill x  -pady 5
    
    
    ###Creation de la ligne separatrice du bas
    frame .wm_FaireRapport.f_sep1 -width 100 -height 2 -borderwidth 1 \
	-relief sunken
    pack .wm_FaireRapport.f_sep1 -side bottom -fill x
    
    
    ### Creation de la partie gauche #########################################
    frame  .wm_FaireRapport.f_gauche -borderwidth 2 -relief raised
    pack .wm_FaireRapport.f_gauche -side left -fill both -expand 1
    
    
    ### Creation  de la zone d'entree des caracteristiques du rapport  #######
    frame .wm_FaireRapport.f_gauche.f_Entree -borderwidth 3 -relief groove 
    
    #Creation du cadre contenant les labels
    frame .wm_FaireRapport.f_gauche.f_Entree.f_label
    label .wm_FaireRapport.f_gauche.f_Entree.f_label.l_NomRapport \
	-text "Nom du Rapport
(sans blanc)"
    label .wm_FaireRapport.f_gauche.f_Entree.f_label.l_Date -text Date
    label .wm_FaireRapport.f_gauche.f_Entree.f_label.l_Auteur -text Auteur
    
    #Creation du cadre contenant les zones de saisie
    frame .wm_FaireRapport.f_gauche.f_Entree.f_entry
    entry .wm_FaireRapport.f_gauche.f_Entree.f_entry.e_NomRapport -width 20 
    entry .wm_FaireRapport.f_gauche.f_Entree.f_entry.e_Date -width 9 
    entry .wm_FaireRapport.f_gauche.f_Entree.f_entry.e_Auteur -width 30
    
    pack .wm_FaireRapport.f_gauche.f_Entree.f_label.l_NomRapport \
	.wm_FaireRapport.f_gauche.f_Entree.f_label.l_Date \
	.wm_FaireRapport.f_gauche.f_Entree.f_label.l_Auteur \
	-pady 10 -side top -anchor w
    
    #Placement des zones de saisies dans le cadre des saisies
    pack .wm_FaireRapport.f_gauche.f_Entree.f_entry.e_NomRapport \
	.wm_FaireRapport.f_gauche.f_Entree.f_entry.e_Date \
	.wm_FaireRapport.f_gauche.f_Entree.f_entry.e_Auteur \
	-pady 10 -side top -anchor w 
    
    pack .wm_FaireRapport.f_gauche.f_Entree.f_label \
	.wm_FaireRapport.f_gauche.f_Entree.f_entry  -side left -padx 5
    
    pack .wm_FaireRapport.f_gauche.f_Entree -side top -anchor w -fill x
    
    ###Creation de la zone de choix Exploitation et Strategie    ##############
    frame .wm_FaireRapport.f_gauche.f_Choix1
    
    #Creation des boutons de choix
    checkbutton .wm_FaireRapport.f_gauche.f_Choix1.ckb_Exploitation \
	-text Exploitation -variable ExpRap -onvalue 1 -offvalue 0
    checkbutton .wm_FaireRapport.f_gauche.f_Choix1.ckb_Strategie \
	-text Strat�gie -variable StratRap -onvalue 1 -offvalue 0
    
    pack .wm_FaireRapport.f_gauche.f_Choix1.ckb_Exploitation -side top \
	-anchor w -padx 15
    pack .wm_FaireRapport.f_gauche.f_Choix1.ckb_Strategie -side top \
	-anchor w -padx 15
    pack .wm_FaireRapport.f_gauche.f_Choix1 -side top  -pady 15
    
    ###Creation du titre  de la zone de selection des Annees Simulees  
    frame .wm_FaireRapport.f_gauche.f_texte1 
    pack  .wm_FaireRapport.f_gauche.f_texte1  -side top -anchor w
    label .wm_FaireRapport.f_gauche.f_texte1.l_Historiques -text HISTORIQUES 
    label .wm_FaireRapport.f_gauche.f_texte1.l_AnnSim -text "Ann�es Simul�es"
    
    pack .wm_FaireRapport.f_gauche.f_texte1.l_Historiques  \
	.wm_FaireRapport.f_gauche.f_texte1.l_AnnSim  \
	-pady 5 -side top -padx 130
    
    
    #Creation  de la zone de selection des Annees Simulees 
    frame .wm_FaireRapport.f_gauche.f_zone1 
    pack .wm_FaireRapport.f_gauche.f_zone1 -side top   -pady 1 
    
    #Creation de la scrolled list des annees possibles et de son titre
    frame .wm_FaireRapport.f_gauche.f_zone1.f_listAnPoss
    label .wm_FaireRapport.f_gauche.f_zone1.f_listAnPoss.l_anPoss \
	-text Possibles
    set list_an_poss \
	[ScrolledListbox \
	     .wm_FaireRapport.f_gauche.f_zone1.f_listAnPoss.scrlst_listAnPoss \
	     -width 10 -height 10 -setgrid true]
    
    pack .wm_FaireRapport.f_gauche.f_zone1.f_listAnPoss.l_anPoss \
	.wm_FaireRapport.f_gauche.f_zone1.f_listAnPoss.scrlst_listAnPoss \
	-side top -pady 1
    
    #Creation de la scrolled List des annees voulues et de son titre
    frame .wm_FaireRapport.f_gauche.f_zone1.f_listAnRet
    label .wm_FaireRapport.f_gauche.f_zone1.f_listAnRet.l_anRet -text Voulues
    set list_an_retenues \
	[ScrolledListbox \
	     .wm_FaireRapport.f_gauche.f_zone1.f_listAnRet.scrlst_listAnRet \
	     -width 10 -height 10 -setgrid true]
    
    pack  .wm_FaireRapport.f_gauche.f_zone1.f_listAnRet.l_anRet \
	.wm_FaireRapport.f_gauche.f_zone1.f_listAnRet.scrlst_listAnRet \
	-side top -pady 1
    
    
    #Remplissage de la scrolled list des annees possibles
    set l_annees_simulees [recherche_rep $DIR_TEMPORAIRE$NOM_STRATEGIE]

    foreach s $l_annees_simulees {
	
	.wm_FaireRapport.f_gauche.f_zone1.f_listAnPoss.scrlst_listAnPoss.lst_list \
	    insert end $s
    }
    
    #Creation des boutons servant a transferrer les donnees d'une liste 
    #a l'autre
    frame .wm_FaireRapport.f_gauche.f_zone1.f_contener
    
    button .wm_FaireRapport.f_gauche.f_zone1.f_contener.b_-> -text "->"\
	-command {InsertVar $list_an_poss $list_an_retenues \
		      .wm_FaireRapport.f_gauche.f_zone1.f_contener.b_-> \
		      $l_annees_simulees} -state disabled	
    button .wm_FaireRapport.f_gauche.f_zone1.f_contener.b_<- -text "<-" \
	-command {InsertVar $list_an_retenues  $list_an_poss  \
		      .wm_FaireRapport.f_gauche.f_zone1.f_contener.b_<- \
		      $l_annees_simulees} -state disabled	
    
    pack .wm_FaireRapport.f_gauche.f_zone1.f_contener.b_-> \
	.wm_FaireRapport.f_gauche.f_zone1.f_contener.b_<- \
	-side top -pady 5
    
    #Creation des commandes de transfert
    bind $list_an_poss <ButtonPress-1> {ListSelectStart $list_an_poss %y}
    
    bind $list_an_poss <B1-Motion> {ListSelectExtend $list_an_poss %y}
    
    bind $list_an_poss <ButtonRelease-1> \
	{.wm_FaireRapport.f_gauche.f_zone1.f_contener.b_-> configure \
	     -state normal}
    
    bind $list_an_retenues <ButtonPress-1> \
	{ListSelectStart $list_an_retenues %y}
    
    bind $list_an_retenues <B1-Motion> \
	{ListSelectExtend  $list_an_retenues %y}
    
    bind $list_an_retenues <ButtonRelease-1> \
	{.wm_FaireRapport.f_gauche.f_zone1.f_contener.b_<- configure \
	     -state normal}
    
    pack .wm_FaireRapport.f_gauche.f_zone1.f_listAnPoss\
	.wm_FaireRapport.f_gauche.f_zone1.f_contener \
	.wm_FaireRapport.f_gauche.f_zone1.f_listAnRet \
	-side left -padx 10 -pady 10 
    
    
    
    ### Creation de la zone de choix Chronique et Alimentation  ##############
    frame .wm_FaireRapport.f_gauche.f_Choix2
    
    #Creation des boutons de choix
    checkbutton .wm_FaireRapport.f_gauche.f_Choix2.ckb_Chronique \
	-text "Chronique des actions" -variable ChronRap\
	-onvalue 1 -offvalue 0
    checkbutton .wm_FaireRapport.f_gauche.f_Choix2.ckb_Aliment \
	-text "Alimentation du troupeau" -variable AlimRap\
	-onvalue 1 -offvalue 0
    
    pack .wm_FaireRapport.f_gauche.f_Choix2.ckb_Chronique  -side top \
	-anchor w -padx 15
    pack .wm_FaireRapport.f_gauche.f_Choix2.ckb_Aliment  -side top \
	-anchor w -padx 15
    pack .wm_FaireRapport.f_gauche.f_Choix2 -side bottom  -pady 15 \
	-fill y -expand 1
    
    
    
    ### Creation de la partie droite ########################################
    frame  .wm_FaireRapport.f_droite -borderwidth 2 -relief raised
    pack .wm_FaireRapport.f_droite -side right -fill both -expand 1
      
    #Creation du Titre de la zone de selection des Variables
    frame .wm_FaireRapport.f_droite.f_texte2 
    label .wm_FaireRapport.f_droite.f_texte2.l_Variables -text Variables 
    pack .wm_FaireRapport.f_droite.f_texte2.l_Variables  -pady 5 \
	-side right -padx 180
    pack  .wm_FaireRapport.f_droite.f_texte2  -side top 
    
    
    ###Creation de la zone de selection des Variables  #######################
    frame .wm_FaireRapport.f_droite.f_zone2 
    pack .wm_FaireRapport.f_droite.f_zone2 -side top
    
    #Creation de la scrolled list des  variables possibles
    frame .wm_FaireRapport.f_droite.f_zone2.f_listVar
    label .wm_FaireRapport.f_droite.f_zone2.f_listVar.l_varPoss \
	-text Possibles 
    set list_var \
	[ScrolledListbox \
	     .wm_FaireRapport.f_droite.f_zone2.f_listVar.scrlst_listVar \
	     -width 20 -height 10 -setgrid true ]
    
    pack .wm_FaireRapport.f_droite.f_zone2.f_listVar.l_varPoss \
	.wm_FaireRapport.f_droite.f_zone2.f_listVar.scrlst_listVar \
	-side top  -pady 1
    
    #Creation de la scrolled list des  variables voulues
    frame .wm_FaireRapport.f_droite.f_zone2.f_listRet
    label .wm_FaireRapport.f_droite.f_zone2.f_listRet.l_varRet -text Voulues
    
    set list_var_retenues \
	[ScrolledListbox \
	     .wm_FaireRapport.f_droite.f_zone2.f_listRet.scrlst_listSelectVar \
	     -width 20 -height 10 -setgrid true]
    
    pack  .wm_FaireRapport.f_droite.f_zone2.f_listRet.l_varRet \
	.wm_FaireRapport.f_droite.f_zone2.f_listRet.scrlst_listSelectVar \
	-side top  -pady 1
    
    
    #Remplissage de la scrolled list des variables possibles
    set a [lindex $l_annees_simulees 0]
    set l_totale_sorties_simulees \
	[recherche_fic $DIR_TEMPORAIRE$NOM_STRATEGIE/$a]
    
    #boucle qui recherche la liste des variables possibles
    foreach s $l_totale_sorties_simulees {
	if {[string compare $s $FIC_TRACE] != 0 && \
		[string compare $s $FIC_CHRONIQUE] != 0 && \
		[string compare $s $FIC_DATES_CLEFS] != 0 && \
		[string compare $s $FIC_PROBLEME] != 0 } {
	    .wm_FaireRapport.f_droite.f_zone2.f_listVar.scrlst_listVar.lst_list\
		insert end $s
	}
    }
    
    #Creation des boutons servant a transferrer les donnees d'une liste 
    #a l'autre
    frame .wm_FaireRapport.f_droite.f_zone2.f_contener
    
    button .wm_FaireRapport.f_droite.f_zone2.f_contener.b_-> -text "->" \
	-state disabled -command \
	{InsertVar $list_var  $list_var_retenues \
	     .wm_FaireRapport.f_droite.f_zone2.f_contener.b_-> \
	     $l_totale_sorties_simulees }

    button .wm_FaireRapport.f_droite.f_zone2.f_contener.b_<- -text "<-" \
	-state disabled -command \
	{InsertVar $list_var_retenues  $list_var \
	 .wm_FaireRapport.f_droite.f_zone2.f_contener.b_<- \
	 $l_totale_sorties_simulees }

    pack .wm_FaireRapport.f_droite.f_zone2.f_contener.b_-> \
	.wm_FaireRapport.f_droite.f_zone2.f_contener.b_<- -side top -pady 5


    #Creation des commandes de transfert
    bind $list_var <ButtonPress-1> {ListSelectStart $list_var %y}
    
    bind $list_var <B1-Motion> {ListSelectExtend $list_var %y}
    
    bind $list_var <ButtonRelease-1> \
	{.wm_FaireRapport.f_droite.f_zone2.f_contener.b_-> configure \
	     -state normal}
    
    bind $list_var_retenues <ButtonPress-1> \
	{ListSelectStart $list_var_retenues %y}
    
    bind $list_var_retenues <B1-Motion> {ListSelectExtend $list_var_retenues %y}
    
    bind $list_var_retenues <ButtonRelease-1> \
	{.wm_FaireRapport.f_droite.f_zone2.f_contener.b_<- configure \
	     -state normal}

    pack .wm_FaireRapport.f_droite.f_zone2.f_listVar\
	.wm_FaireRapport.f_droite.f_zone2.f_contener \
	.wm_FaireRapport.f_droite.f_zone2.f_listRet \
	-side left -padx 10 -pady 10 -expand 1
    
    
    #Creation du Titre de la zone de selection des Statistiques
    frame .wm_FaireRapport.f_droite.f_texte3 
    label .wm_FaireRapport.f_droite.f_texte3.l_Stat -text STATISTIQUES 
    pack  .wm_FaireRapport.f_droite.f_texte3  -side top -pady 10
    pack .wm_FaireRapport.f_droite.f_texte3.l_Stat  \
	-pady 1 -side right -padx 180
    
    
    ### Creation  de la zone de selection des Statistiques ##################
    frame .wm_FaireRapport.f_droite.f_zone3
    pack .wm_FaireRapport.f_droite.f_zone3 -side top
    
    #Creation de la scrolled list des statistiques realisees
    frame .wm_FaireRapport.f_droite.f_zone3.f_listStat
    label .wm_FaireRapport.f_droite.f_zone3.f_listStat.l_statPoss \
	-text R�alis�es
    
    set list_stat \
	[ScrolledListbox \
	     .wm_FaireRapport.f_droite.f_zone3.f_listStat.scrlst_listStat \
	     -width 20 -height 10 -setgrid true]
    
    pack .wm_FaireRapport.f_droite.f_zone3.f_listStat.l_statPoss \
	.wm_FaireRapport.f_droite.f_zone3.f_listStat.scrlst_listStat \
	-side top  -pady 1
    
    #Creation de la scrolled List des statistiques voulues
    frame .wm_FaireRapport.f_droite.f_zone3.f_listStatRet
    label .wm_FaireRapport.f_droite.f_zone3.f_listStatRet.l_statRet \
	-text Voulues
    
    set list_stat_retenues \
	[ScrolledListbox \
	     .wm_FaireRapport.f_droite.f_zone3.f_listStatRet.scrlst_listSelectStat\
	     -width 20 -height 10 -setgrid true]
    
    pack  .wm_FaireRapport.f_droite.f_zone3.f_listStatRet.l_statRet \
	.wm_FaireRapport.f_droite.f_zone3.f_listStatRet.scrlst_listSelectStat\
	-side top -pady 1
  
    #Remplissage de la scrolled list des statistiques realisees
    set l_stat_realisees [list]
    #boucle qui recherche les fichiers .ps et qui les mets dans une liste 
    #sans leur extension
    foreach s [recherche_fic $DIR_STATISTIQUES] {
	if {[string match *.ps $s]} {
	    lappend l_stat_realisees [lindex [split $s .] 0]
	}
    }	    
    
    #boucle qui ins�re la liste des stat possibles dans la scrolled list box
    foreach s $l_stat_realisees {
	.wm_FaireRapport.f_droite.f_zone3.f_listStat.scrlst_listStat.lst_list\
	    insert end $s
    }
    
    
    #Creation des boutons servant a transferrer les donnees d'une liste 
    #a l'autre
    frame .wm_FaireRapport.f_droite.f_zone3.f_contener
    
    button .wm_FaireRapport.f_droite.f_zone3.f_contener.b_-> -text "->" \
	-command \
	{InsertVar $list_stat  $list_stat_retenues \
	     .wm_FaireRapport.f_droite.f_zone3.f_contener.b_-> \
	     $l_stat_realisees} -state disabled
    button .wm_FaireRapport.f_droite.f_zone3.f_contener.b_<- -text "<-" \
	-command \
	{InsertVar $list_stat_retenues  $list_stat \
	     .wm_FaireRapport.f_droite.f_zone3.f_contener.b_<- \
	     $l_stat_realisees} -state disabled
    
    pack .wm_FaireRapport.f_droite.f_zone3.f_contener.b_-> \
	.wm_FaireRapport.f_droite.f_zone3.f_contener.b_<- -side top -pady 5
    
    #Creation des commandes de transfert
    bind $list_stat <ButtonPress-1> {ListSelectStart $list_stat %y}
    
    bind $list_stat <B1-Motion> {ListSelectExtend $list_stat %y}
    
    bind $list_stat <ButtonRelease-1> \
	{.wm_FaireRapport.f_droite.f_zone3.f_contener.b_-> configure \
	     -state normal}
    
    bind $list_stat_retenues <ButtonPress-1> \
	{ListSelectStart $list_stat_retenues %y}
    
    bind $list_stat_retenues <B1-Motion> \
	{ListSelectExtend  $list_stat_retenues %y}
    
    bind $list_stat_retenues <ButtonRelease-1> \
	{.wm_FaireRapport.f_droite.f_zone3.f_contener.b_<- configure \
	     -state normal}
    
    pack .wm_FaireRapport.f_droite.f_zone3.f_listStat\
	.wm_FaireRapport.f_droite.f_zone3.f_contener \
	.wm_FaireRapport.f_droite.f_zone3.f_listStatRet \
	-side left -padx 10 -pady 10 -expand 1
}



############################################################################
# Procedure : RealiserRapport
# Description : cree le rapport
# Parametres : aucun
############################################################################
proc RealiserRapport {w} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global DIR_RAPPORTCOURANT
    global DIR_STATISTIQUES
    global CMD_COPY
    global FIC_EXPLOITATION_TXT
    global FIC_STRATEGIE_TXT
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU
    global FIC_INDICATEURS_LNU
    global FIC_FOINTERPRETATION_LNU
    global NOM_STRATEGIE
    global NOM_RAPPORT

    # checkbuttons
    global ExpRap
    global StratRap
    global ChronRap
    global AlimRap

    # variables de VisualiserSortiesVariables.tcl
    global no_var
    global nb_parcelles_fichier
    global nb_jours_fichier
    

    # lecture des donnees sur la fenetre
    set Nom_Rapport [${w}.f_gauche.f_Entree.f_entry.e_NomRapport get]
    set Auteur_Rapport [${w}.f_gauche.f_Entree.f_entry.e_Date get]
    set Date_Rapport [${w}.f_gauche.f_Entree.f_entry.e_Auteur get]

    set liste_var_selct [${w}.f_droite.f_zone2.f_listRet.scrlst_listSelectVar.lst_list get 0 end]
    set nbvar [llength $liste_var_selct]
    set liste_an_selct [${w}.f_gauche.f_zone1.f_listAnRet.scrlst_listAnRet.lst_list get 0 end]
    set nbannees [llength $liste_an_selct]
    set liste_stat_selct [${w}.f_droite.f_zone3.f_listStatRet.scrlst_listSelectStat.lst_list get 0 end]
    set nbstat [llength $liste_stat_selct]


    set annul 0 
    #V�rification du remplissage des champs minimaux 
    if {[PbControleChaine $Nom_Rapport nom]} {
	set annul 1
	set msg_err "Vous ne pouvez pas donner ce nom au rapport"
    } elseif {[string length $Date_Rapport] == 0} {
	set annul 1
	set msg_err "Il faut dater le rapport"
    } elseif {[string length $Auteur_Rapport] == 0} {
	set annul 1
	set msg_err "Il faut donner le nom de l'auteur du rapport"
    }

    #V�rification que le rapport soit non vide
    if {($ExpRap == 0) && ($StratRap == 0) && ($ChronRap == 0) && \
	    ($AlimRap == 0) && ($nbvar == 0) && ($nbstat == 0 )} {
	set annul 1
	set msg_err "Vous ne pouvez pas cr�er un rapport vide"
    }
    
    #V�rification de la coh�rence des choix entre les annees,chronique et alim
    if {($ChronRap == 1 && $nbannees == 0) || \
	    ($AlimRap == 1 && $nbannees == 0) || \
	    ($AlimRap == 1 && $nbannees == 0 && $nbvar != 0) || \
	    ($ChronRap == 0 && $nbannees == 0 && $nbvar != 0) || \
	    ($nbannees == 0 && $nbvar != 0) || \
	    ($AlimRap == 1 && $nbvar != 0 && $nbannees == 0)} { 
	set annul 1
	set msg_err "Il faut s�lectionner au moins une ann�e"
    } 
  
    
    if {$annul == 1} {
	CreerFenetreDialogue $LOGICIEL error $msg_err
    } else {	
	#ouverture en �criture du fichier contenant le rapport
	set rapfile [open $DIR_RAPPORTCOURANT$Nom_Rapport.tex w]
	#Commande qui permet de ne pas afficher les commentaires de compilation
	puts $rapfile "\\batchmode"
	#D�claration du format du document
	puts $rapfile "\\documentclass\[10pt\]{article}"
	#Inclusion de packages n�cessaires pour la cr�ation du rapport
	puts $rapfile "\\usepackage\[french\]{babel}"
	puts $rapfile "\\usepackage{epsfig}"
	puts $rapfile "\\usepackage{supertabular}"
	#Cr�ation du titre du rapport en faisant attention au tiret-bas
	#decoupage de la variable avec comme caract�re s�parateur _
	#et reconstruction de la variable avec comme caract�re recolleur \\_
	puts $rapfile "\\title{\\Huge{[join [split $Nom_Rapport _] \\_]}}"
	puts $rapfile "\\author{\\huge{$Auteur_Rapport}}"
	puts $rapfile "\\date{$Date_Rapport}"
	puts $rapfile "\\begin{document}"
	puts $rapfile "\\maketitle"
	puts $rapfile "\\newpage"
	puts $rapfile "\\newpage"
	puts $rapfile "\\tableofcontents"
	puts $rapfile "\\newpage"
	
	#Si on a choisi Exploitation 
	if {$ExpRap == 1} {
	    # lecture de l'exploitation
	    set f [open $DIR_TEMPORAIRE$FIC_EXPLOITATION_TXT r]
	    set contents [read $f]
	    close $f
	    #Cr�ation du titre du chapitre
	    puts $rapfile "\\section{DESCRIPTION DE L'EXPLOITATION}"
	    puts $rapfile "\\noindent"
	    puts $rapfile "\\begin{verbatim}"
	    puts $rapfile "$contents"
	    puts $rapfile "\\end{verbatim}"
	    puts $rapfile ""
            CommentaireRapport $rapfile
	    puts $rapfile "\\newpage"
	}
	puts $rapfile ""
	
	#Si on a choisi Strat�gie 
	if {$StratRap == 1} {
	    
	    # lecture de la strat�gie
	    set f [open $DIR_TEMPORAIRE$FIC_STRATEGIE_TXT r]
	    set dates_strategie [read $f] 
	    close $f
	    set f [open $DIR_TEMPORAIRE$FIC_REGLESPLANIFICATION_LNU r]
	    set regles_planification [read $f] 
	    close $f
	    set f [open $DIR_TEMPORAIRE$FIC_REGLESOPERATOIRES_LNU r]
	    set regles_operatoires [read $f] 
	    close $f
	    set f [open $DIR_TEMPORAIRE$FIC_INDICATEURS_LNU r]
	    set indicateurs [read $f] 
	    close $f
	    set f [open $DIR_TEMPORAIRE$FIC_FOINTERPRETATION_LNU r]
	    set fo_interpretation [read $f] 
	    close $f

	    #Cr�ation du titre du chapitre
	    puts $rapfile "\\section{DESCRIPTION DE LA STRATEGIE}"	
	    puts $rapfile "\\noindent"
	    puts $rapfile "\\begin{verbatim}"
	    puts $rapfile ""
	    puts $rapfile "DATES"
	    puts $rapfile ""
	    puts $rapfile "$dates_strategie"
	    puts $rapfile ""
	    puts $rapfile "REGLES DE PLANIFICATION"
	    puts $rapfile ""
	    puts $rapfile "$regles_planification"
	    puts $rapfile ""
	    puts $rapfile "REGLES OPERATOIRES"
	    puts $rapfile ""
	    puts $rapfile "$regles_operatoires"
	    puts $rapfile ""
	    puts $rapfile "INDICATEURS"
	    puts $rapfile ""
	    puts $rapfile "$indicateurs"
	    puts $rapfile ""
	    puts $rapfile "FONCTIONS D'INTERPRETATION"
	    puts $rapfile ""
	    puts $rapfile "$fo_interpretation"
	    puts $rapfile "\\end{verbatim}"
	    CommentaireRapport $rapfile
	    puts $rapfile "\\newpage"
	    
	}
	puts $rapfile ""
	
	#Initialisation d'une variable correspondant au titre du chapitre
	set titre 0	
	#Si on a choisi Chronique des actions
	if {$ChronRap == 1} {
	    #Cr�ation du titre de chapitre
	    puts $rapfile "\\section{HISTORIQUES DE SIMULATION}"	
	    set titre 1
	    puts $rapfile "\\subsection{CHRONIQUE DES ACTIONS}"
	    source VisualiserSortiesChronique.tcl
	    for {set j 0} {$j < $nbannees} {incr j} {
		set annee [lindex $liste_an_selct $j]
		# affichage de la chronique en icone
		Afficher_chronique .wm_FaireRapportChronique$annee  $annee \
		    $DIR_TEMPORAIRE$NOM_STRATEGIE/$annee/  0
		puts $rapfile "\\subsubsection{$annee}"
		puts $rapfile "\\noindent"
		puts $rapfile "\\newline"
		puts $rapfile "\\psfig{figure = chronique$annee.ps, width = 15 cm}"
	    }
	    CommentaireRapport $rapfile
	    puts $rapfile "\\newpage"
	    puts $rapfile ""
	}
	
	#Si on a choisi Alimentation du troupeau
	if {$AlimRap == 1} {
	    #Cr�ation du titre de chapitre, s'il n'existe pas d�j�
	    if {$titre == 0} {
		puts $rapfile "\\section{HISTORIQUES DE SIMULATION}"	
	    }
	    #Cr�ation du titre du sous-chapitre
	    set titre 1
	    puts $rapfile "\\subsection{ALIMENTATION DU TROUPEAU}"	
	    source VisualiserSortiesAlimentation.tcl
	    for {set j 0} {$j < $nbannees} {incr j} {
		set annee [lindex $liste_an_selct $j ]
		Afficher_alimentation .wm_FaireRapportAlimentation$annee $annee  0
		puts $rapfile "\\subsubsection{$annee}"
		puts $rapfile "\\noindent"
		puts $rapfile "\\psfig{figure = alimgraphique$annee.ps, width = 15 cm}"
	    }
	    CommentaireRapport $rapfile
	    puts $rapfile ""
	}
	
	#Si on a s�lectionn� des variables
	if {$nbvar != 0} {
	    #Cr�ation du titre de chapitre, s'il n'existe pas d�j�
	    if {$titre == 0} {
		puts $rapfile "\\section{HISTORIQUES DE SIMULATION}"	
	    }
	    #Cr�ation du titre du sous-chapitre
	    puts $rapfile "\\subsection{VARIABLES}"	
	    
	    set annee [lindex $liste_an_selct 0]
	    set visu_no_fen_variable 0
	    for {set j 0} {$j < $nbvar} {incr j} {
		incr visu_no_fen_variable
		set var [lindex $liste_var_selct $j ]
		#Ouverture en lecture du fichier contenant la variable
		set varfile [open $DIR_TEMPORAIRE$NOM_STRATEGIE/$annee/$var r]
		set ligne [gets $varfile]
		if {$ligne  == "%texte%"} {
		    # variable de type symbolique
		    if {[string match *parcelle* [gets $varfile]] == 1} {
			# variable est d�pend des parcelles
			CreerFenetreDialogue $LOGICIEL erreur \
			    "Erreur lors de l'ouverture du fichier contenant les donn�es de la variable $var"
		    } else {
			# affichage en tableau
			for {set i 0} {$i < $nbannees} {incr i} {
			    set annee [lindex $liste_an_selct $i ]
			    puts $rapfile "\\subsubsection{[join [split $var _] \\_] $annee}"
			    puts $rapfile "\\noindent"
			    #Cr�ation d'ent�te et de bas de page
			    #pour un tableau sur plusieurs pages en LaTeX
			    puts $rapfile "\\tablefirsthead{\\hline {\\bf Jour} & $annee \\\\ \\hline}"
			    puts $rapfile "\\tablehead{\\hline" 
			    puts $rapfile "\\multicolumn{2}{|c|}{Suite$\\ldots$} \\\\ \\hline"
			    puts $rapfile "{\\bf Jour} & $annee \\\\}"
			    puts $rapfile "\\tabletail{\\hline \\multicolumn{2}{|c|}{A Suivre$\\ldots$} \\\\ \\hline}" 
			    puts $rapfile "\\tablelasttail{\\hline}"
			    #D�but du tableau proprement dit
			    puts $rapfile "\\begin{supertabular}{|l|l|}"
			    set f [open $DIR_TEMPORAIRE$NOM_STRATEGIE/${annee}/$var r]
			    set contents [gets $f]
			    #Initialisaton de la variable contenant les jours
			    set k 1
			    while {[gets $f line] >=0} {
				#Cr�ation d'une ligne du tableau
				puts $rapfile "$k & $line\\\\"
				puts $rapfile "\\hline"
				incr k
			    }
			    close $f
			    puts $rapfile "\\end{supertabular}"
			    CommentaireRapport $rapfile
			}
		    }
		    
		} else {
		    # variable de type num�rique 
		    if {[string match *parcelle* $ligne] == 1} {
			# dependant des parcelles
			close $varfile   
			source VisualiserSortiesVariables.tcl
			puts $rapfile "\\subsubsection{[join [split $var _] \\_]}"
			puts $rapfile "\\noindent"
			for {set t 0} {$t < [llength [LireParcelles]]} {incr t}  {
			    Afficher_variables \
				.wm_FaireRapportVariables${visu_no_fen_variable}\_p$t \
				$var $liste_an_selct \
				$DIR_TEMPORAIRE$NOM_STRATEGIE/ 0 $t
			    puts $rapfile "\\psfig{figure = wm_FaireRapportVariables${visu_no_fen_variable}_p$t.ps, width = 15 cm}"
			}
			CommentaireRapport $rapfile    
		    } else {
			# independant des parcelles
			source VisualiserSortiesVariables.tcl
			Afficher_variables \
			    .wm_FaireRapportVariables${visu_no_fen_variable} \
			    $var $liste_an_selct $DIR_TEMPORAIRE$NOM_STRATEGIE/ 0
			puts $rapfile "\\subsubsection{[join [split $var _] \\_]}"
			puts $rapfile "\\noindent"
			puts $rapfile "\\psfig{figure = wm_FaireRapportVariables${visu_no_fen_variable}.ps, width = 15 cm}"
			CommentaireRapport $rapfile
			puts $rapfile ""			
		    }
		}			
	    }
	    puts $rapfile "\\newpage"
	} 
	puts $rapfile ""
	
	#Si on a choisi des statistiques 
	if {$nbstat != 0} {
	    puts $rapfile "\\section{STATISTIQUES}"
	    for {set j 0} {$j < $nbstat} {incr j} {
		set stat [lindex $liste_stat_selct $j]
		#Copies des fichiers statistiques .ps dans le r�pertoire 
		exec $CMD_COPY $DIR_STATISTIQUES$stat.ps $DIR_RAPPORTCOURANT 
		puts $rapfile "\\subsubsection{$stat}"
		puts $rapfile "\\noindent"
		puts $rapfile "\\psfig{figure = $stat.ps}"
		CommentaireRapport $rapfile
		puts $rapfile ""
	    }    
	}
	puts $rapfile ""
	puts $rapfile "\\end{document}"
	close $rapfile
	
	#Cr�ation de la fen�tre de demande d'attente
	toplevel .wm_FaireRapportRealisation
	wm title .wm_FaireRapportRealisation "$LOGICIEL : Creation du rapport en cours"
	button .wm_FaireRapportRealisation.b_fermer -text Ok \
	    -command " destroy .wm_FaireRapportRealisation"
	pack .wm_FaireRapportRealisation.b_fermer -side bottom -pady 4
	frame .wm_FaireRapportRealisation.f_sep -width 100 -height 2 \
	    -borderwidth 1 -relief sunken
	pack .wm_FaireRapportRealisation.f_sep -side bottom -fill x -pady 4
	label .wm_FaireRapportRealisation.l_icon -bitmap info -foreground red
	pack .wm_FaireRapportRealisation.l_icon -side left -padx 16 -pady 12
	message .wm_FaireRapportRealisation.msg_erreur -justify center -aspect 300 \
	    -justify center -text "Pouvez-vous attendre qu'il n'y ait plus d'ic�nes qui apparaissent dans la barre des t�ches, ensuite cliquer Ok"
	pack .wm_FaireRapportRealisation.msg_erreur -side right -padx 8 -pady 8 \
	    -fill both -expand true

	# On inhibe l'acces aux autres fenetres tant que 
	#le bouton Ok n'a pas ete appuye
	grab .wm_FaireRapportRealisation
	tkwait window .wm_FaireRapportRealisation
	grab release .wm_FaireRapportRealisation
    
	#Si on a choisi Chronique des actions
	if {$ChronRap == 1} {
	    for {set j 0} {$j < $nbannees} {incr j} {
		set annee [lindex $liste_an_selct $j]
		#Cr�ation des images en postscript
		Imprimer_chronique .wm_FaireRapportChronique$annee $annee 1
		#Destruction de la fen�tre correspondant � la chronique
		destroy .wm_FaireRapportChronique$annee
	    }
	}
	
	#Si on a choisi Alimentation du troupeau
	if {$AlimRap == 1} {
	    for {set j 0} {$j < $nbannees} {incr j} {
		set annee [lindex $liste_an_selct $j ]
		#Cr�ation des images en postscript
		Imprimer_alimentation .wm_FaireRapportAlimentation$annee $annee 1 
		#Destruction de la fen�tre correspondant � l'alimentation
		destroy .wm_FaireRapportAlimentation$annee
	    }
	}
	
	#Si on a choisi des variables
	set visu_no_fen_variable 0
	if {$nbvar != 0} { 
	    #boucle qui parcoure les ann�es
	    for {set j 0} {$j < $nbvar} {incr j} {
		incr visu_no_fen_variable
		set var [lindex $liste_var_selct $j ]
		#Ouverture du fichier contenant la variable en lecture
		set varfile [open $DIR_TEMPORAIRE$NOM_STRATEGIE/$annee/$var r]
		#lecture de la 1�re ligne
		set ligne [gets $varfile]
		#si la variable est de type num�rique (pas symbolique)
		if {$ligne  != "%texte%"} {
		    #et qu'elle d�pend des parcelles
		    if {[string index  $ligne 3] == "p"} {
			#alors on cr�� une image pour chaque parcelle
			for {set t 0} {$t < [llength [LireParcelles]]} {incr t}  {
			    Imprimer_variable \
				.wm_FaireRapportVariables${visu_no_fen_variable}\_p$t \
				$var $liste_an_selct  variable${no_var} \
				$nb_parcelles_fichier \
				NomParcelles${no_var} $nb_jours_fichier 1
			    #Destruction de la fen�tre correspondant
			    #� la variable
			    destroy .wm_FaireRapportVariables${visu_no_fen_variable}\_p$t
			}
		    } else {
			#Sinon on cr�� qu'une image
			Imprimer_variable \
			    .wm_FaireRapportVariables${visu_no_fen_variable} \
			    $var $liste_an_selct  variable${no_var} \
			    $nb_parcelles_fichier \
			    NomParcelles${no_var} $nb_jours_fichier 1
			#Destruction de la fen�tre correspondant
			#� la variable
			destroy .wm_FaireRapportVariables${visu_no_fen_variable}
		    }
		}
	    }
	}
	
	#Ex�cution de la proc�dure compilant le rapport 
	if {[CompilerRapportLatex $DIR_RAPPORTCOURANT $Nom_Rapport] == 0} {
	    set NOM_RAPPORT $Nom_Rapport
	    #Mise a jour de l'etat des boutons Modifier et Imprimer
	    ${w}.f_bottom.b_Modifier configure -state normal
	    ${w}.f_bottom.b_Visualiser configure -state normal
	    ${w}.f_bottom.b_Imprimer configure -state normal
	}
    }
}


############################################################################
# Procedure : CompilerRapportLatex
# Description : permet de compiler le rapport et de trouver les �ventuelles 
#               erreurs
# Parametres : repertoire nom_rapport
############################################################################
proc CompilerRapportLatex {repertoire nom_rapport} {

    global LOGICIEL
    global DIR_EXECUTION
    
    cd $repertoire
    
    set erreur_latex 0

    #Excution de la compilation du rapport
    catch {exec latex $nom_rapport.tex}
    catch {exec latex $nom_rapport.tex}
    
    #Ouverture du fichier log en lecture
    set f [open $nom_rapport.log r]
    set contents [read $f]
    close $f
    
    #Recherche d'un motif d'erreur
    if {([string first "Error" $contents] != -1) || \
	    ([string first {! D} $contents] != -1) || \
	    ([string first {! E} $contents] != -1) || \
	    ([string first {! I} $contents] != -1) || \
	    ([string first {! M} $contents] != -1) || \
	    ([string first {! N} $contents] != -1) || \
	    ([string first {! P} $contents] != -1) || \
	    ([string first {! T} $contents] != -1) || \
	    ([string first {! U} $contents] != -1) || \
	    ([string first {! Y} $contents] != -1)} {   
	
	set erreur_latex 1

	#Creation de la fenetre indiquant que l'impression ne sera pas lanc�e
	toplevel .wm_FaireRapportErreur
	wm title .wm_FaireRapportErreur \
	    "$LOGICIEL : Erreur de code LaTeX dans le rapport"
	
	#Creation de la frame du bas contenant le bouton Ok
	frame .wm_FaireRapportErreur.f_bottom
	button .wm_FaireRapportErreur.f_bottom.b_Ok -text Ok \
	    -command "destroy .wm_FaireRapportErreur"
	pack  .wm_FaireRapportErreur.f_bottom.b_Ok \
	    -padx 20 -pady 10 -side bottom -expand 1
	pack .wm_FaireRapportErreur.f_bottom -side bottom -anchor s -fill x \
	    -pady 5
    
	#Creation de la ligne separatrice du bas
	frame .wm_FaireRapportErreur.f_sep -width 100 -height 2 \
	    -borderwidth 1 -relief sunken
	pack .wm_FaireRapportErreur.f_sep -side bottom -fill x
	
	#Creation de la frame du haut
        frame .wm_FaireRapportErreur.f_top 
	pack .wm_FaireRapportErreur.f_top -side top -fill x
	
        #Creation d'un label indiquant qu'il y a eu une erreur
	label .wm_FaireRapportErreur.f_top.l_erreur -relief groove \
	    -text "Le rapport ne peut pas etre cr��, car le code LaTex contient une erreur."
	label .wm_FaireRapportErreur.f_top.l_icon -bitmap error -foreground red
	pack .wm_FaireRapportErreur.f_top.l_erreur -fill y \
	    -expand true -side right	
	pack .wm_FaireRapportErreur.f_top.l_icon -side right \
	    -anchor w -padx 16 -pady 12 

	#Creation d'un tixScrolledText pour afficher le contenu du fichier log
	tixScrolledText .wm_FaireRapportErreur.scrtxt_code -scrollbar auto
	.wm_FaireRapportErreur.scrtxt_code.text insert end $contents 
	pack .wm_FaireRapportErreur.scrtxt_code -expand 1 -fill both \
	    -side bottom

	# Inhibition de l'acc�s aux autres fen�tres
	grab .wm_FaireRapportErreur
	tkwait window .wm_FaireRapportErreur
	grab release .wm_FaireRapportErreur
	
    } else {  
	CreerFenetreDialogue $LOGICIEL info \
	          "Votre rapport a �t� realis� ou modifi� sans erreur"
    }

    #Repositionnement dans le repertoire execution
    cd $DIR_EXECUTION

    return $erreur_latex 
}

############################################################################
# Procedure : VisualiserRapport
# Description : permet de visualiser le  rapport courant
# Parametres : aucun
############################################################################
proc VisualiserRapport {repertoire nom_rapport} {

    global DIR_EXECUTION

    #Positionnnement dans le r�pertoire contenant le rapport courant
    cd $repertoire
    #Ex�cution du programme qui permet de visualiser le rapport
    exec xdvi $nom_rapport.dvi
    #Repositionnnement dans le r�pertoire courant (ex�cution)
    cd $DIR_EXECUTION
}

############################################################################
# Procedure : ModifierRapport
# Description : permet d'editer le rapport rapport courant
# Parametres : aucun
############################################################################
proc ModifierRapport {} {

    source ExecuterFaireRapportModifier.tcl
    FaireRapportModifier 
    
}

############################################################################
# Procedure : ImprimerRapport
# Description : lance l'impression du  rapport
# Parametres : aucun
############################################################################
proc ImprimerRapport {} {
  
    global LOGICIEL

    #Chargement des variables globales
    global DIR_EXECUTION
    global DIR_RAPPORTCOURANT
    global NOM_RAPPORT

    #Positionnement dans le repertoire qui contient le rapport courant
    cd $DIR_RAPPORTCOURANT
    
    #Lancement de l'impression
    catch {exec dvips $NOM_RAPPORT.dvi}
    
    #Affichage d'un message d'impression en cours
    CreerFenetreDialogue $LOGICIEL info\
	"Votre rapport est en cours d'impression."
    
    #Repositionnement dans le repertoire execution
    cd $DIR_EXECUTION
}


############################################################################
# Procedure : FermerRapport
# Description : d�truit la fenetre qui permet de creer un rapport
# Parametres : aucun
############################################################################
proc FermerRapport {} {
    
    destroy .wm_FaireRapport
}


############################################################################
# Procedure : CommentaireRapport
# Description : permet de creer une zone de commentaire dans un rapport
# Parametres : variable contenant le rapport
############################################################################
proc CommentaireRapport {var} {


    #Ecriture d'un rep�re pour le d�but de commentaires
    puts $var ""
    puts $var "%D�but de commentaire : commencez apr�s le begin{verbatim}"
    #Ecriture de la commande LaTeX qui permet d'�rire du texte libre
    puts $var "\\begin{verbatim}"
    puts $var ""
    #Ecriture de la commande LaTeX qui arr�te l'�criture de texte libre
    puts $var "\\end{verbatim}"
    #Ecriture d'un rep�re pour la fin de commentaires
    puts $var "%Fin de commentaire : votre commentaire doit s'arr�ter avant le end{verbatim}"
    puts $var ""
}
