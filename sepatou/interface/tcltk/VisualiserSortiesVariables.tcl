# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : VisualiserSortiesVariables.tcl      
# Contenu : Creation du tableau ou d'un graphe contenant les
#           informations relatives a une variable donnee, et     
#           affichage de ces infos suivant le numero de parcelle 
#           courante.    
##############################################################

# Options pour l'affichage graphique
option add *Legend.activeRelief		sunken
option add *activeLine.Color		yellow4
option add *activeLine.Fill		yellow
option add *activeLine.pixels		1.5m


#------------------------------------------------#
# affichage d'une variable
#------------------------------------------------#
#  fen : nom de la fenetre � creer
#  var : nom de variable (= nom de fichier)
#  listclim : liste des annees climatiques
#  chemin : repertoire ou se trouve les annees (finit par /)
#------------------------------------------------#
# Apres lecture, mise a jour de
#   variable$no_variable(i_climat)(i_parcelle)(i_jour)
#   NomParcelles$no_variable(i_parcelle)
#   nb_parcelles_fichier
# les indices commencent a 0
#------------------------------------------------#
proc Afficher_variables {fen var listclim chemin affichage {parcelle foo}} {

    global LOGICIEL

    global type_contenu
    global no_var
    if [info exists no_var] {
	incr no_var
    } else {
	set no_var 0
    }

    global variable${no_var}
    global NomParcelles${no_var}

    # mis en global pour ExecuterFaireRapport.tcl
    global nb_parcelles_fichier
    global nb_jours_fichier


    # lecture des donn�es
    set lecture 0

    set no_clim -1
    set nb_jours_fichier 0

    foreach clim $listclim  {
	incr no_clim
	if {[catch {set fileid [open $chemin$clim/$var r]}] != 0 } {
	    set lecture 1
	    CreerFenetreDialogue $LOGICIEL error \
		"Erreur d'ouverture du fichier $chemin$clim/$var"
	} else {
	    set no_parcelle 0
	    set 1ere_parcelle 1
	    set j 0

	    gets $fileid line
	    if { [string compare $line %texte%] == 0 } {
		set type_contenu $line
		gets $fileid line
	    } else {
		set type_contenu %numerique%
	    }

	    while {[eof $fileid] == 0} {
		if { [string compare [string range $line 0 0] "*"] == 0 } {
		    if {$1ere_parcelle == 1} {
			set 1ere_parcelle 0
		    } else {
			incr no_parcelle
		    }
		    # nom de parcelle
		    set NomParcelles${no_var}($no_parcelle) [string range $line 3 end]

		    set j 0
		} else {
		    set variable${no_var}($no_clim)($no_parcelle)($j) $line
		    incr j 
		}
		gets $fileid line
	    }

	    close $fileid

	    if {$nb_jours_fichier == 0} {
		set nb_jours_fichier [expr $j - 1]
	    } elseif { [expr $j - 1] < $nb_jours_fichier } {
		set nb_jours_fichier [expr $j - 1]
	    }

	    if {$no_parcelle == 0} {
		# la variable ne depend pas des parcelles
		set nb_parcelles_fichier 1
	    } else {
		set nb_parcelles_fichier  [expr $no_parcelle + 1]
	    }
	}
    }

    if {$lecture  == 0} {
	# affichage des donnees
	AfficherDonnees $fen $var $listclim variable${no_var} \
	    $nb_parcelles_fichier NomParcelles${no_var} $nb_jours_fichier \
	    $affichage $parcelle
    }
}



#-----------------------------------------------#
# Procedure qui se charge de creer autant de    #
#  fenetres que de sorties voulues et de les    #
#  remplir par les informations correspondantes.#
#-----------------------------------------------#
# Parametres :                                  #
#  w : nom de la fenetre � creer
#  var : nom de variable ou de parametre
#  listclim : liste des annees climatiques
#  V : tableau ou les donnees sont stockees
#  nb_parcelles : nb de parcelles
#  P : tableau ou les noms de parcelles sont stockes
#  nb_jours : nb de jours par annee
#-----------------------------------------------#
proc AfficherDonnees {w var listclim V nb_parcelles P nb_jours \
		      affichage {parcelle foo}} {

    global LOGICIEL

    global $V
    global $P

    global type_contenu
    global existe_tableau
    global existe_graphique
    global position

    toplevel $w
    wm title $w "$LOGICIEL : Variable $var ([Unite_variable $var])"

    if {$affichage == 0} {
	    wm iconify $w
	}

    # Chaque fenetre est composee de cinq frames positionnes
    #  verticalement : modeaff (mode d'affichage), zoneaff
    #  (zone d'affichage), info (information) , sep (separateur)
    #   et valid (validation).

    # Premier frame : mode d'affichage
    frame $w.f_modeaff -borderwidth 3
    pack $w.f_modeaff -side top

    # Remplissage du premier frame
    button $w.f_modeaff.b_graf -text {Graphique} \
	-command "Choix_graphique $w $var [list $listclim] $V $nb_parcelles $P $nb_jours"
    button $w.f_modeaff.b_num -text {Numerique} -state disabled \
	-command "Choix_tableau $w $var [list $listclim] $V $nb_parcelles $P $nb_jours"
    pack  $w.f_modeaff.b_graf $w.f_modeaff.b_num -side left

    # Deuxieme frame : zone d'affichage
    frame $w.f_zoneaff -borderwidth 2
    pack $w.f_zoneaff -side top
    # Remplissage du deuxieme frame compose d'une partie affichage
    #  et d'une partie choix (navigation entre parcelles)
    frame $w.f_zoneaff.f_zone -borderwidth 2 -relief groove
    pack $w.f_zoneaff.f_zone -side left -fill both -expand true

    # Quatrieme frame : validation
    frame $w.f_valid -borderwidth 2
    pack $w.f_valid -side bottom
    # Remplissage du quatrieme frame
    button $w.f_valid.b_imprimer -text Imprimer \
	-command "Imprimer_variable $w $var [list $listclim] $V $nb_parcelles $P $nb_jours 0"

    button $w.f_valid.b_fermer -text Fermer -command "destroy $w"
    pack $w.f_valid.b_fermer $w.f_valid.b_imprimer -side right \
	-expand true -padx 60

    # Cinquieme frame : separateur
    frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack $w.f_sep -side bottom -fill x -pady 4

    if { $nb_parcelles > 1 } {
	# Troisieme frame : information
	frame $w.f_info -borderwidth 2
	pack $w.f_info -side bottom
	# Remplissage du troisieme frame
	label $w.f_info.l_intitule -text "Nom de la parcelle : "
	label $w.f_info.l_nomparcel
	pack $w.f_info.l_intitule $w.f_info.l_nomparcel -side left \
            -expand true -pady 6
    }

    # Remplissage de la partie affichage du frame zone d'affichage :
    #  creation du tableau ou du graphique dans lequel seront inserees 
    #  les donnees relatives a la variable courante et initialisation
    #  par les infos concernant la premiere parcelle
    
    #-- Ici, on initialise en mode tableau ou graphique 
    # et eventuellement sur une parcelle autre que la 1ere
    set existe_tableau($w) 0
    set existe_graphique($w) 0
    if {$parcelle != "foo"} {
	set position($w) $parcelle
	set nom_parcelle_a_afficher [set ${P}($parcelle)]
	set titre_graphique "${var} ($nom_parcelle_a_afficher)"
    } else {
	set position($w) 0
	set titre_graphique $var
    }
    if {[string compare $type_contenu "%numerique%"] == 0} {
	Choix_graphique $w $titre_graphique $listclim $V $nb_parcelles $P $nb_jours
    } else {
        Choix_tableau $w $var $listclim $V $nb_parcelles $P $nb_jours
        $w.f_modeaff.b_graf configure -state disabled
    }

    if { $nb_parcelles > 1 } {

	frame $w.f_zoneaff.f_choix -borderwidth 2
	pack  $w.f_zoneaff.f_choix -side right -padx 6 -fill both -expand yes

	# Remplissage de la partie choix du frame zone d'affichage
	label $w.f_zoneaff.f_choix.l_prec -text precedente
	button $w.f_zoneaff.f_choix.b_boutonprec -text - -state disabled \
	    -command "Afficher_donnees $w p [list $listclim] $V $nb_parcelles $P $nb_jours"
	label $w.f_zoneaff.f_choix.l_parcel -text parcelle
	button $w.f_zoneaff.f_choix.b_boutonsuiv -text + \
	    -command "Afficher_donnees $w s [list $listclim] $V $nb_parcelles $P $nb_jours"
	label $w.f_zoneaff.f_choix.l_suiv -text suivante 
	pack $w.f_zoneaff.f_choix.l_prec $w.f_zoneaff.f_choix.b_boutonprec \
	    $w.f_zoneaff.f_choix.l_parcel $w.f_zoneaff.f_choix.b_boutonsuiv \
        $w.f_zoneaff.f_choix.l_suiv -side top
    }
}



#---------------------------------------------------------------#
# Procedure qui se charge d'afficher les informations contenues #
#  dans la variable en memoire correspondante a la parcelle     #
#  voulue pour la sortie et les annees climatiques desirees.    #
#---------------------------------------------------------------#
proc Afficher_donnees {w mode listclim V nb_parcelles P nb_jours} {
    
    global $V
    global $P

    global position ;# Indice de la parcelle courante
    global typeaff ;# Type d'affichage pour la fenetre courante
    
    global existe_graphique 

    # Tableau de couleurs
    set colors(0) blue
    set colors(1) green
    set colors(2) red
    set colors(3) brown
    set colors(4) orange
    set colors(5) pink
    set colors(6) white
    set colors(7) yellow
    
    # affichage de la grille en mode tableau
    if {$typeaff($w) == 0} {
	set grille [$w.f_zoneaff.f_zone.scrg_grille subwidget grid]
    }

    #---------------------------------------------------------#
    # Gestion de l'activation/desactivation des boutons + et -
    #  correspondants a parcelle suivante et parcelle precedente
    # Remarque : pas de modification si mode = i ou mode = m.
    if {$position($w) == [expr $nb_parcelles - 2] && $mode == "s"} {
	$w.f_zoneaff.f_choix.b_boutonsuiv configure -state disabled
    } elseif {$mode == "s"} {
	$w.f_zoneaff.f_choix.b_boutonprec configure -state normal
    }
    if {$position($w) == 1 && $mode == "p"} {
	$w.f_zoneaff.f_choix.b_boutonprec configure -state disabled
    } elseif {$mode == "p"} {
	$w.f_zoneaff.f_choix.b_boutonsuiv configure -state normal
    }

    # mise a jour de la parcelle courante
    if {$mode == "p"} {
	set position($w) [expr $position($w) - 1]
    } elseif {$mode == "s"} {
	incr position($w)
    }

    if { $nb_parcelles > 1} {
	# Affichage du libelle de la parcelle courante
	$w.f_info.l_nomparcel \
	    configure -text "[set ${P}($position($w))]"
    }
    
    set nb_clim [llength $listclim]

    if {$typeaff($w) == 0 && $mode == "i"} { 
	# Initialisation du contenu du tableau d'affichage
	$grille set 0 0 -text "Jour"
    }
    set col 0
    if {$typeaff($w) == 1} {
	set valmaxi 0
	set valmini 0
    }
    
    for {set no_clim 0} {$no_clim < $nb_clim } {incr no_clim} {
	set clim [lindex $listclim $no_clim]
	if {$typeaff($w) == 0} { 
	    incr col 
	    $grille set $col 0 -text "$clim"
	} else {
	    # Initialisation des ordonnees
	    set y1 { }
	}

	# Lecture et affichage des infos de la parcelle courante : position($w)
	set i_parcelle  $position($w)

	# Positionnement dans le tableau ou sur le graphique
	set ligne 1
	
	for {set j 0} {$j < $nb_jours} {incr j} {
	    
	    if {$typeaff($w) == 0} {    
		$grille set 0 $ligne -text $ligne
		$grille set $col $ligne -text [set ${V}($no_clim)($i_parcelle)($j)]
	    } elseif {$typeaff($w) == 1} {
		lappend x $ligne
		lappend y1 [set ${V}($no_clim)($i_parcelle)($j)]
		if {[set ${V}($no_clim)($i_parcelle)($j)] > $valmaxi} {
		    set valmaxi [set ${V}($no_clim)($i_parcelle)($j)]
		}
		if {[set ${V}($no_clim)($i_parcelle)($j)] < $valmini} {
		    set valmini [set ${V}($no_clim)($i_parcelle)($j)]
		}
	    }
	    incr ligne 
	}

	# Si on passe en affichage numerique, on cache la ou les courbes
	#  courantes afin de ne pas les voir lorsque l'on reppassera en
	#  affichage graphique apres avoir change de parcelle.
	if {$existe_graphique($w) == 1} {
	    if {$typeaff($w) == 0} {
		if {[$w.f_zoneaff.f_zone.g_graphique line  exists "line${w}$no_clim$i_parcelle"]} {
		    $w.f_zoneaff.f_zone.g_graphique element \
			delete "line${w}$no_clim$i_parcelle"
		}
	    }
	}

	if {$typeaff($w) == 1} {
	    # Si la parcelle courante est differente de la parcelle precedente
	    #  (avant ou apres), alors on efface la courbe precedente
	    if {$mode == "p" || $mode == "s"} {
		if {$mode == "p"} {
		    set efface [expr $i_parcelle + 1]
		} else {
		    set efface [expr $i_parcelle - 1]
		}
		$w.f_zoneaff.f_zone.g_graphique element \
		    delete "line${w}$no_clim$efface"
	    }

	    $w.f_zoneaff.f_zone.g_graphique axis configure "x" -min 0 \
		-max [expr $ligne + 0.001]
	    $w.f_zoneaff.f_zone.g_graphique axis configure "y" -min $valmini \
		-max [expr $valmaxi + 0.001]

	    if {$no_clim < 8} {
		set couleur $colors($no_clim)
	    } else {
		set couleur black
	    }

	    $w.f_zoneaff.f_zone.g_graphique element \
		create line${w}$no_clim$i_parcelle -label "$clim" -xdata $x \
		-ydata $y1 -pixels 0.2m -color $couleur
	}
    }

    if {$typeaff($w) == 1} {
	Blt_ZoomStack $w.f_zoneaff.f_zone.g_graphique
	Blt_Crosshairs $w.f_zoneaff.f_zone.g_graphique
	Blt_ActiveLegend $w.f_zoneaff.f_zone.g_graphique
    }
}


#-------------------------------------------------#
# Procedure qui cree un graphe et qui l'affiche.  #
#-------------------------------------------------#
#    w : frame parent,                            #
#    titre : titre du graphique
#-------------------------------------------------#
proc Afficher_graphique {w titre} {

    graph $w.g_graphique -title "$titre" -width 700 -height 400 -background grey75
    pack $w.g_graphique
}


#-------------------------------------------------#
# Procedure qui se charge d'afficher le tableau   #
#  dans lequel seront inserees les infos relatives#
#  a la variable courante.                        #
#-------------------------------------------------#
#    w : frame parent,                            #
#    nom : nom du fichier de la variable.         #
#-------------------------------------------------#
proc Afficher_tableau {w nom} {

    global editgrid

    # Cree la grille et initialise ses options
    tixScrolledGrid $w.scrg_grille -bd 0 -width 700 -height 400

    pack $w.scrg_grille -expand yes -padx 20 -pady 10

    set grid [$w.scrg_grille subwidget grid]
    $grid config -formatcmd "EditGrid_format $grid"

    # Definition de la taille des colonnes
    $grid size col default -size auto
    $grid size row default -size 1.1char
}



#----------------------------------------------#
# Procedure qui cree un graphique, le rattache # 
#  a un parent et initialise son contenu.      #
#----------------------------------------------#
proc Choix_graphique {w titre lclim V nb_parcelles P nb_jours} {

    global existe_graphique
    global typeaff
    set typeaff($w) 1

    pack forget $w.f_zoneaff.f_zone.scrg_grille

    if {$existe_graphique($w) == 1} {
	pack $w.f_zoneaff.f_zone.g_graphique
	Afficher_donnees $w m $lclim $V $nb_parcelles $P $nb_jours
    } else {
	Afficher_graphique $w.f_zoneaff.f_zone $titre
	Afficher_donnees $w m $lclim $V $nb_parcelles $P $nb_jours
	set existe_graphique($w) 1
    }


    $w.f_modeaff.b_num configure -state normal
    $w.f_modeaff.b_graf configure -state disabled
}

#--------------------------------------------#
# Procedure qui cree un tableau, le rattache #
#  a un parent et initialise son contenu.    #
#--------------------------------------------#
proc Choix_tableau {w var lclim V nb_parcelles P nb_jours} {

    global position ;# Indice de la parcelle courante
    global existe_tableau
    global typeaff
    set typeaff($w) 0

    pack forget $w.f_zoneaff.f_zone.g_graphique

    if {$existe_tableau($w) == 1} {
       pack $w.f_zoneaff.f_zone.scrg_grille
       Afficher_donnees $w m $lclim $V $nb_parcelles $P $nb_jours
    } else {
       Afficher_tableau $w.f_zoneaff.f_zone $var
       Afficher_donnees $w i $lclim $V $nb_parcelles $P $nb_jours
       set existe_tableau($w) 1
    }

    $w.f_modeaff.b_graf configure -state normal
    $w.f_modeaff.b_num configure -state disabled
}


#-----------------------------------------------#
# Procedure qui se charge de l'impression des   #
#  resultats concernant les variables.          #
#-----------------------------------------------#
proc Imprimer_variable {w variable liste_climats V nb_parcelles P \
			    nb_jours rapport} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global DIR_RAPPORTCOURANT
    global CMD_REMOVE
    global CMD_A2PS
    global CMD_IMPRIMER
    global OPT_IMPRIMER

    global typeaff
 
    if { $typeaff($w) == 0 } {
	# Creation d'un fichier contenant les resultats sous forme tabulaire
	
	# Tableau d'indices de la parcelle courante pour une fenetre donnee
	global position
	set i_position $position($w)
	
	set filetxt [open "${DIR_TEMPORAIRE}$w.imp" w]
	Creer_FichierImpression $filetxt $variable $liste_climats $V $nb_parcelles $P $i_position $nb_jours

	if {$rapport == 0} {
	    # Impression et suppression de ce fichier
	    catch {exec $CMD_A2PS ${DIR_TEMPORAIRE}$w.imp}
	    exec $CMD_REMOVE ${DIR_TEMPORAIRE}$w.imp
	    CreerFenetreDialogue $LOGICIEL info "Impression en cours"
	} else {
	    exec mv ${DIR_TEMPORAIRE}$w.imp $DIR_RAPPORTCOURANT
	}
    } else {
	if {$rapport == 0} {
	    # Creation du fichier postscript correspondant a
	    #  l'affichage graphique de la fenetre courante
	    $w.f_zoneaff.f_zone.g_graphique postscript output ${DIR_TEMPORAIRE}$w.ps
	    # Impression et suppression de ce fichier
	    catch {exec $CMD_IMPRIMER $OPT_IMPRIMER ${DIR_TEMPORAIRE}$w.ps}
	    exec $CMD_REMOVE ${DIR_TEMPORAIRE}$w.ps
	    CreerFenetreDialogue $LOGICIEL info "Impression en cours"
	} else {
	    set nom [string range $w 1 end]
	    $w.f_zoneaff.f_zone.g_graphique postscript output ${DIR_RAPPORTCOURANT}$nom.ps
	}
    }
}


#-----------------------------------------------#
# Procedure qui se charge de creer le fichier   #
#  contenant les resultats sous forme texte(*), #
#  Ce fichier sera ensuite imprime et detruit.  #
#-----------------------------------------------#
# (*) Avec affichage des annees par colonne en  #
#  prenant les valeurs d'une variable stockee   #
#  en memoire.                                  #
#-----------------------------------------------#
proc Creer_FichierImpression {filetxt var liste_climats V nb_parcelles P i_position nb_jours} {

    global $V
    global $P

    set nb_clim [llength $liste_climats]

    puts $filetxt "Variable : $var en [Unite_variable $var]"    
    if { $nb_parcelles != 1} {
	puts $filetxt "Parcelle : [set ${P}($i_position)]"
    }
    puts $filetxt " "
    
    puts -nonewline $filetxt " JOUR "
    for {set k 0} {$k < $nb_clim } {incr k} {
	    puts -nonewline $filetxt "|   [lindex $liste_climats $k]  "
    }

    puts $filetxt " "
    puts -nonewline $filetxt "------"
    for {set k 0} {$k < $nb_clim } {incr k} {
	puts -nonewline $filetxt "----------"
    }

    puts $filetxt " "
    set jour 1

    for {set j 0} {$j < $nb_jours} {incr j} {
	
	if { $jour < 100 && $jour >= 10} {
	    puts -nonewline $filetxt " "
	} elseif { $jour < 10 && $jour > 0} {
	    puts -nonewline $filetxt "  "
	} else {
	    puts -nonewline $filetxt ""
	}          
	puts -nonewline $filetxt " $jour  "
	
	for {set k 0} {$k < $nb_clim } {incr k} {
	    puts -nonewline $filetxt "|"
	    set val [set ${V}($k)($i_position)($j)]
	    # Reglage par rapport au nombre de chiffre avant la virgule 
	    if { $val < 1000 && $val >= 100 } {
		puts -nonewline $filetxt " "
	    } elseif { $val < 100 && $val >= 10} {
		puts -nonewline $filetxt "  "
	    } elseif { $val < 10 && $val >= 0} {
		puts -nonewline $filetxt "   "
	    } elseif { $val < 0 && $val > -10} {
		puts -nonewline $filetxt "  "
	    } elseif {$val <= -10 && $val > -100 } {
		puts -nonewline $filetxt " "
	    } else {
		puts -nonewline $filetxt ""
	    }
	    puts -nonewline $filetxt "$val"
	    
	    # Nombre de chiffres apres la virgule
	    set point [string first . $val]
	    set n [expr [string length $val] - [expr $point + 1]]  
	    if {$point == -1} {
		puts -nonewline $filetxt "     "
	    } elseif {$n == 1} {
		puts -nonewline $filetxt "   "
	    } elseif {$n == 2} {
		puts -nonewline $filetxt "  "
	    } elseif {$n == 3} {
		puts -nonewline $filetxt " "
	    } else {
		puts -nonewline $filetxt ""
	    }
	}
	puts $filetxt " " 
	incr jour
    }
    # Fermeture du fichier impression
    close $filetxt 
}


