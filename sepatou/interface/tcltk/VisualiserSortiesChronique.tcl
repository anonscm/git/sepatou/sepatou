# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : VisualiserSortiesChronique.tcl 
# Contenu : Affichage de chronique d'action avec plusieurs 
#           modes d'affichage(graphique, graph detaille, texte).
##############################################################

#---------------------------------------------#
# Procedure qui se charge de creer la fenetre #
#  de visualisation de la chronique d'actions #
#  et qui affiche l'affiche par defaut sous   #
#  forme de texte.                            #
#---------------------------------------------#
# Parametres :                                #
#    fen_cc : fenetre correspondant a la      #
#              chronique courante demandee,   #
#    annee : annee climatique correspondant a #
#             la chronique visualisee.        # 
#    chemin : annee comprise, finit par /
#---------------------------------------------#
proc Afficher_chronique {fen_cc annee chemin affichage} {

    global LOGICIEL
    global FIC_CHRONIQUE
    global FIC_DATES_CLEFS
    
    # memorisation des affichages realises
    global Chronique_texte
    global Chronique_graphique
    global Chronique_graphique_detaille
    set Chronique_texte($fen_cc) 0
    set Chronique_graphique($fen_cc) 0 
    set Chronique_graphique_detaille($fen_cc) 0
    
    if {[file exist $chemin$FIC_CHRONIQUE] && [file exist $chemin$FIC_DATES_CLEFS]} {
	# Creation d'une fenetre
	catch {destroy $fen_cc}
	toplevel $fen_cc
	wm title $fen_cc "$LOGICIEL : Chronique $annee"
        
	if {$affichage == 0} {
	    wm iconify $fen_cc
	}
	
	# La fenetre est composee de quatre frames 
	#  (barre, top, set et fcommands) alignes verticalement 
	
	# Le frame barre est en haut de l'ecran, il est compose
	#  des boutons commandant l'affichage de la chronique
	frame $fen_cc.f_barre -borderwidth 2
	pack $fen_cc.f_barre -side top
	button $fen_cc.f_barre.b_graph -text {Graphique} \
	    -command "AfficherChroniqueGraphique $fen_cc $chemin"
	button $fen_cc.f_barre.b_graphdet -text {Graphique detaille} \
	    -command "AfficherChroniqueGraphiqueDetaille $fen_cc $chemin" 
	button $fen_cc.f_barre.b_text -text {Texte} \
	    -command "AfficherChroniqueTexte $fen_cc  $chemin"
	pack $fen_cc.f_barre.b_graph $fen_cc.f_barre.b_graphdet \
	    $fen_cc.f_barre.b_text -side left
	
	# Le frame top 
	frame $fen_cc.f_top -borderwidth 10
	pack $fen_cc.f_top -side top -fill both -expand true 
	frame $fen_cc.f_top.f_text -borderwidth 10 
	#  text correspond a la zone d'affichage de la chronique
	text $fen_cc.f_top.f_text.t_text -relief sunken -bd 2 \
	    -yscrollcommand "$fen_cc.f_top.f_text.scrb_scroll set" \
	    -setgrid 1 -height 30 -width 100 
	scrollbar $fen_cc.f_top.f_text.scrb_scroll \
	    -command "$fen_cc.f_top.f_text.t_text yview"
	pack $fen_cc.f_top.f_text.scrb_scroll -side right -fill y
	pack $fen_cc.f_top.f_text.t_text -expand yes -side left -fill both
	frame $fen_cc.f_top.f_graph -borderwidth 10

	graph $fen_cc.f_top.f_graph.g_nom  -height 500 -width 130 \
	    -title ""  -plotrelief flat
	pack $fen_cc.f_top.f_graph.g_nom -side left

	graph $fen_cc.f_top.f_graph.g_graph -height 500 -width 750 \
	    -title "Chronique $annee"
	pack $fen_cc.f_top.f_graph.g_graph -fill both -expand true -side left
	Blt_ZoomStack $fen_cc.f_top.f_graph.g_graph
	Blt_Crosshairs $fen_cc.f_top.f_graph.g_graph

	# Affichage des dates clefs
	set fic_clefs [open $chemin$FIC_DATES_CLEFS r]
	gets $fic_clefs zdateMH
	if {$zdateMH == -1} {set zdateMH "-"}
	gets $fic_clefs zdateFin1erCycle
	if {$zdateFin1erCycle == -1} {set zdateFin1erCycle "-"}
	gets $fic_clefs zdateSortieNuit
	if {$zdateSortieNuit == -1} {set zdateSortieNuit "-"}
	gets $fic_clefs zdateFermetureSilo
	if {$zdateFermetureSilo == -1} {set zdateFermetureSilo "-"}
	gets $fic_clefs zdateOuvertureSilo
	if {$zdateOuvertureSilo == -1} {set zdateOuvertureSilo "-"}
	gets $fic_clefs zdateEnsilage
	if {$zdateEnsilage == -1} {set zdateEnsilage "-"}
	gets $fic_clefs zdate1ereFauche
	if {$zdate1ereFauche == -1} {set zdate1ereFauche "-"}
	close $fic_clefs

	frame $fen_cc.f_DatesClefs 
	pack $fen_cc.f_DatesClefs -side top -expand yes -fill x
	message $fen_cc.f_DatesClefs.m_dates -aspect 2000 -text "Mise � l'herbe : $zdateMH     Fin 1er cycle : $zdateFin1erCycle     Sortie nuit : $zdateSortieNuit     Fermeture silo : $zdateFermetureSilo     Ouverture silo : $zdateOuvertureSilo     Ensilage : $zdateEnsilage     1�re fauche : $zdate1ereFauche
"
	pack $fen_cc.f_DatesClefs.m_dates 

	# Les frames sep : separateur et fcommands : commandes imprimer et fermer
	frame $fen_cc.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $fen_cc.f_sep -side top -fill x -pady 5
	frame $fen_cc.f_fcommands
	pack $fen_cc.f_fcommands -side bottom
	button $fen_cc.f_fcommands.imp -text Imprimer \
            -command "Imprimer_chronique $fen_cc $annee 0"
	button $fen_cc.f_fcommands.quit -text Fermer -command "destroy $fen_cc"
	pack $fen_cc.f_fcommands.quit $fen_cc.f_fcommands.imp \
            -side right -padx 60 -pady 4
	
	# Initialisation de l'affichage en graphique
	AfficherChroniqueGraphique $fen_cc $chemin
	
    } else {
	CreerFenetreDialogue $LOGICIEL error \
	    "Erreur d'ouverture du fichier $chemin$FIC_CHRONIQUE"
    }
}


#--------------------------------------------------------#
# affichage de chronique en texte
#--------------------------------------------------------#
proc AfficherChroniqueTexte {w chemin} {

    global FIC_CHRONIQUE

    global Chronique_texte

    if {$Chronique_texte($w) != 1} {
	# le texte n'est pas deja charge
	set file [open $chemin$FIC_CHRONIQUE r]
	$w.f_top.f_text.t_text insert end [read $file]
	close $file
    }

    # Changement du frame .f_top
    pack $w.f_top.f_text 
    pack forget $w.f_top.f_graph 

    ## Changement des etats des boutons type d'affichage
    $w.f_barre.b_text configure -state disabled
    $w.f_barre.b_graph configure -state normal
    $w.f_barre.b_graphdet configure -state disabled
}

	
#--------------------------------------------------------#
# affichage de chronique en graphique
#--------------------------------------------------------#
proc AfficherChroniqueGraphique {w chemin} {

    global FIC_CHRONIQUE

    global Chronique_graphique

    if {$Chronique_graphique($w) != 1} {
	
	set g_nom $w.f_top.f_graph.g_nom
	set g $w.f_top.f_graph.g_graph
	set liste_parcelles [LireParcelles]
	set nb_parcelles [llength $liste_parcelles]

	# le graphique n'est pas fait
	set file [open $chemin$FIC_CHRONIQUE r]

	set p_paturage ""
	set liste_ensilage ""
	set liste_fauche ""
	set liste_azote ""

	# lecture du 1er numero de jour
	gets $file line
	set j_debut [string range $line 3 end]
	lappend J $j_debut  ;# liste des no de jour
	set j $j_debut

	while { [gets $file line] >= 0 } {
	    if { [string range $line 0 0] == "*"} {
		# memorisation des actions lues
		for {set k 0} {$k < $nb_parcelles} {incr k} {
		    set parcelle [lindex $liste_parcelles $k]
		    if  {$parcelle == $p_paturage} {
			    set P${k}($j)(paturage) $q_paturage
		    } else {
			set P${k}($j)(paturage) 0
		    }
		    if {[lsearch $liste_ensilage $parcelle] == -1} {
			set P${k}($j)(ensilage) 0
		    } else {
			set P${k}($j)(ensilage) 1
		    }
		    if {[lsearch $liste_fauche $parcelle] == -1} {
			set P${k}($j)(fauche) 0
		    } else {
			set P${k}($j)(fauche) 1
		    }
		    if {[lsearch $liste_azote $parcelle] == -1} {
			set P${k}($j)(azote) 0
		    } else {
			set P${k}($j)(azote) $q_azote
		    }

		}
		# initialisation des variables journalieres
		set p_paturage ""
		set liste_ensilage ""
		set liste_fauche ""
		set liste_azote ""
		# lecture d'un numero de jour
		set j [string range $line 3 end]
		lappend J $j
	    } else {
		# lecture d'une action
		set debut [Enieme_mot_de_ligne $line 0]
		if { $debut == "Paturage" } {
		    set q_paturage [Enieme_mot_de_ligne $line 1]
		    set p_paturage [Enieme_mot_de_ligne $line 2]
		} elseif { $debut == "Ensilage" } {
		    lappend liste_ensilage [Enieme_mot_de_ligne $line 2]
		} elseif { $debut == "Fauche" } {
		    lappend liste_fauche [Enieme_mot_de_ligne $line 2]
		} elseif { $debut == "Azote" } {
		    set q_azote [Enieme_mot_de_ligne $line 1]
		    lappend liste_azote [Enieme_mot_de_ligne $line 2]
		}
	    }
	}


	# memorisation des actions lues le dernier jour
	for {set k 0} {$k < $nb_parcelles} {incr k} {
	    set parcelle [lindex $liste_parcelles $k]
	    if  {$parcelle == $p_paturage} {
		   set P${k}($j)(paturage) $q_paturage
	    } else {
		set P${k}($j)(paturage) 0
	    }
	    if {[lsearch $liste_ensilage $parcelle] == -1} {
		set P${k}($j)(ensilage) 0
	    } else {
		set P${k}($j)(ensilage) 1
	    }
	    if {[lsearch $liste_fauche $parcelle] == -1} {
		set P${k}($j)(fauche) 0
	    } else {
		set P${k}($j)(fauche) 1
	    }
	}
	set j_fin $j

	close $file
 
	# cr�ation de la l�gende
	$g element create line1 -label Paturage -fill green 
	$g element create line2 -label Ensilage -fill blue 
	$g element create line3 -label Fauche -fill black 
	$g element create line4 -label Azote -fill red
	
	
	$g axis configure "x" -min $j_debut -max $j_fin 
	
	# affichage 
	$g axis configure "x" -min $j_debut -max $j_fin 
	$g axis configure "y" -min 0 -max  $nb_parcelles -hide 1
	
	$g_nom axis configure "x" -hide 1
	$g_nom axis configure "y" -min 0 -max  $nb_parcelles -hide 1
	
	set y_parcelle 0.8
	set y_separation 0.2
	set y2 [expr 0 - $y_separation]
	for {set k 0} {$k < $nb_parcelles} {incr k} {
	    set debut_paturage 0
	    set y1 [expr $y2 + $y_separation]
	    set y2 [expr $y1 + $y_parcelle]
	    
	    # affichage du nom de parcelle
	    set y [expr $y2 - 0.4]
	    $g_nom marker create text -text [lindex $liste_parcelles $k]\
		-coords {0.3 $y} -justify left
	    
	    # affichage du rectangle de parcelle
	    $g marker create polygon -fill white -linewidth 1 \
		-coords {$j_debut $y1 $j_fin $y1 $j_fin $y2 $j_debut $y2 $j_debut $y1}

	    
	    for {set j $j_debut} {$j < $j_fin} {incr j} {
		if { [set P${k}($j)(paturage)] != 0 } {
		    if {$debut_paturage == 0} {
		    set debut_paturage $j
		    }
		} else {
		    if {$debut_paturage != 0} {
		    set x1 [expr $debut_paturage - 0.2]
			set x2 [expr $j - 0.8]
			$g marker create polygon -fill green\
			-coords {$x1 $y1 $x2 $y1 $x2 $y2 $x1 $y2 $x1 $y1}
			set debut_paturage 0
		    }
		    if { [set P${k}($j)(ensilage)] != 0 } {
			set x1 [expr $j - 0.2]
		    set x2 [expr $j + 0.2]
			$g marker create polygon  -fill blue\
			    -coords {$x1 $y1 $x2 $y1 $x2 $y2 $x1 $y2 $x1 $y1}
		    } else {
			if {  [set P${k}($j)(fauche)] != 0 } {
			    set x1 [expr $j - 0.2]
			    set x2 [expr $j + 0.2]
			    $g marker create polygon -fill black\
				-coords {$x1 $y1 $x2 $y1 $x2 $y2 $x1 $y2 $x1 $y1}	    
			}
		    }
		}
		if { [set P${k}($j)(azote)] != 0 } {
		    set x1 [expr $j - 0.4]
		    set x2 [expr $j + 0.4]
		    $g marker create polygon -fill red \
		    -coords {$x1 $y1 $x2 $y1 $x2 $y2 $x1 $y2 $x1 $y1}	    
		}
	    }
	
	    # fin du dernier polygone paturage
	    if {$debut_paturage != 0} {
		set x1 [expr $debut_paturage - 0.2]
		set x2 [expr $j - 0.8]
		$g marker create polygon -fill green\
		    -coords {$x1 $y1 $x2 $y1 $x2 $y2 $x1 $y2 $x1 $y1}
		set debut_paturage 0
	    }

	    set Chronique_graphique($w) 1
	}
    }

    # Changement du frame .f_top
    pack forget $w.f_top.f_text 
    pack $w.f_top.f_graph 

    ## Changement des etats des boutons type d'affichage
    $w.f_barre.b_text configure -state normal
    $w.f_barre.b_graph configure -state disabled
    $w.f_barre.b_graphdet configure -state disabled
}


#-------------------------------------------------#
# Impression de la chronique d'action suivant le type d'affichage
#-------------------------------------------------#
proc Imprimer_chronique {w annee rapport} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global DIR_RAPPORTCOURANT
    global CMD_REMOVE
    global CMD_A2PS
    global CMD_IMPRIMER
    global OPT_IMPRIMER
    
    if {$rapport == 0} {
	if { [string compare [$w.f_barre.b_text cget -state] disabled] == 0 } {
	    # impression texte
	    set filetxt [open "${DIR_TEMPORAIRE}chronique$annee.imp" w]
	    set filetxt [open "${DIR_TEMPORAIRE}chronique$annee.imp" w]
	    puts $filetxt "CHRONIQUE DE L'ANNEE $annee :"
	    puts $filetxt " "
	    $w.f_top.f_text.t_text mark set insert 0.0
	    puts $filetxt [$w.f_top.f_text.t_text get "insert linestart" end]
	    close $filetxt
	    # Impression du fichier
	    catch {exec $CMD_A2PS ${DIR_TEMPORAIRE}chronique$annee.imp}
	    # Suppression du fichier
	    exec $CMD_REMOVE ${DIR_TEMPORAIRE}chronique$annee.imp
	} elseif { [string compare [$w.f_barre.b_graph cget -state] disabled] == 0 } {
	    # impression graphique
	    $w.f_top.f_graph.g_graph postscript output ${DIR_TEMPORAIRE}chronique$annee.ps
	    # Impression du fichier
	    catch {exec $CMD_IMPRIMER $OPT_IMPRIMER ${DIR_TEMPORAIRE}chronique$annee.ps}
	    # Suppression du fichier
	    exec $CMD_REMOVE ${DIR_TEMPORAIRE}chronique$annee.ps
	} elseif { [string compare [$w.f_barre.b_graphdet cget -state] disabled] == 0 } {
	    # Impression graphique detaille
	}
    
	CreerFenetreDialogue $LOGICIEL info "Impression en cours"
    } else {
	# creation du postscript pour le rapport
	$w.f_top.f_graph.g_graph postscript output \
	    ${DIR_RAPPORTCOURANT}chronique$annee.ps   
	$w.f_top.f_graph.g_nom postscript output \
	    ${DIR_RAPPORTCOURANT}legchron$annee.ps 
    }
}
