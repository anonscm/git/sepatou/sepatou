# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : VisualiserSorties.tcl 
# Contenu : Affichage de la fenetre de choix des resultats
#           (des sorties dont on souhaite visualiser les valeurs).
##############################################################


#-----------------------------------------------#
# Procedure qui se charge d'afficher la fenetre #
#  de choix des sorties a visualiser.           #
#-----------------------------------------------#
proc Fenetre_visualisation_sorties {} {

global LOGICIEL
global DIR_TEMPORAIRE
global FIC_PROBLEME
global NOM_STRATEGIE
global VAR_SORTIES_POSSIBLES
global FIC_TRACE
global FIC_CHRONIQUE
global FIC_DATES_CLEFS

global visu_no_fen_variable
set visu_no_fen_variable 0

global l_annees_simulees
global l_totale_sorties_simulees

source VisualiserSortiesProbleme.tcl 
Afficher_probleme 


# Ne pas afficher deux fois la meme fenetre
catch {destroy .wm_VisuSorties}
# Creation d'une fenetre
toplevel .wm_VisuSorties
wm title .wm_VisuSorties "$LOGICIEL : Visualisation des sorties"

# La fenetre est composee de deux frames cotes
#  a cotes, d'un troisieme et quatrieme cales en bas
frame .wm_VisuSorties.f_down -borderwidth 2
pack .wm_VisuSorties.f_down -side bottom
frame .wm_VisuSorties.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
pack .wm_VisuSorties.f_sep -side bottom -fill x -pady 5
frame .wm_VisuSorties.f_topr -borderwidth 2 -relief raised
frame .wm_VisuSorties.f_topl -borderwidth 2 -relief raised
pack .wm_VisuSorties.f_topl .wm_VisuSorties.f_topr\
    -side left -fill both -pady 5 -expand true

########## Remplissage du frame du bas (.wm_VisuSorties.f_down) ###########
button .wm_VisuSorties.f_down.b_ok -text {Afficher} \
    -command Afficher_VisuSorties
button .wm_VisuSorties.f_down.b_annuler -text {Fermer}\
    -command Fermer_VisuSorties
pack .wm_VisuSorties.f_down.b_ok .wm_VisuSorties.f_down.b_annuler\
    -side left -expand 1 -padx 60 -pady 2

########## Remplissage du frame de gauche (.wm_VisuSorties.f_topl) ###########
Creer_titre_souligne .wm_VisuSorties.f_topl.ts_titre "Liste des SORTIES:"

global etat_bresume
set etat_bresume 0
button .wm_VisuSorties.f_topl.b_resume -text {R�sum� de simulations} -state disabled \
       -command "Modifier_boutonresume .wm_VisuSorties.f_topl.b_resume"
pack .wm_VisuSorties.f_topl.b_resume -side top -expand 1 -pady 2

global etat_btrace
set etat_btrace 0
button .wm_VisuSorties.f_topl.b_trace -text {Trace decision} -state disabled \
       -command "Modifier_boutontrace .wm_VisuSorties.f_topl.b_trace"
pack .wm_VisuSorties.f_topl.b_trace -side top -expand 1 -pady 2

global etat_bchronique
set etat_bchronique 0
button .wm_VisuSorties.f_topl.b_chronik -text {Chronique des actions} -state disabled \
       -command "Modifier_boutonchronique .wm_VisuSorties.f_topl.b_chronik"
pack .wm_VisuSorties.f_topl.b_chronik -side top -expand 1 -pady 2

global etat_balimentation
set etat_balimentation 0
button .wm_VisuSorties.f_topl.b_alim -text {Alimentation du troupeau} -state disabled \
       -command "Modifier_boutonalimentation .wm_VisuSorties.f_topl.b_alim"
pack .wm_VisuSorties.f_topl.b_alim -side top -expand 1 -pady 2

label .wm_VisuSorties.f_topl.l_var -text {VARIABLES}
pack .wm_VisuSorties.f_topl.l_var -side top -padx 8

## Premiere scrolled List : sorties obtenues
ScrolledListbox .wm_VisuSorties.f_topl.scrlst_listVar -width 25 -height 12 -setgrid true

set l_annees_simulees [recherche_rep $DIR_TEMPORAIRE$NOM_STRATEGIE]
set a [lindex $l_annees_simulees 0]
set l_totale_sorties_simulees [recherche_fic $DIR_TEMPORAIRE$NOM_STRATEGIE/$a]

foreach s $l_totale_sorties_simulees {
     if {[string compare $s $FIC_TRACE] != 0 && \
	     [string compare $s $FIC_CHRONIQUE] != 0 && \
	     [string compare $s $FIC_DATES_CLEFS] != 0 && \
	     [string compare $s $FIC_PROBLEME] != 0 } {
         .wm_VisuSorties.f_topl.scrlst_listVar.lst_list insert end $s
     } elseif {[string compare $s $FIC_TRACE] == 0} {
           .wm_VisuSorties.f_topl.b_trace configure -state normal
     } elseif {[string compare $s $FIC_CHRONIQUE] == 0} {
           .wm_VisuSorties.f_topl.b_chronik configure -state normal
           .wm_VisuSorties.f_topl.b_alim configure -state normal
           .wm_VisuSorties.f_topl.b_resume configure -state normal 
     }
}

## Deuxieme scrolled List
ScrolledListbox .wm_VisuSorties.f_topl.scrlst_listSelectVar -width 25 -height 12 \
  -setgrid true

## Boutons situes entre les deux scrolled list precedentes
frame .wm_VisuSorties.f_topl.f_contener
button .wm_VisuSorties.f_topl.f_contener.b_-> -text "->" \
    -command {InsertVar .wm_VisuSorties.f_topl.scrlst_listVar.lst_list \
		  .wm_VisuSorties.f_topl.scrlst_listSelectVar.lst_list\
		  .wm_VisuSorties.f_topl.f_contener.b_-> \
		  $l_totale_sorties_simulees } \
    -state disabled
button .wm_VisuSorties.f_topl.f_contener.b_<- -text "<-" \
    -command {InsertVar .wm_VisuSorties.f_topl.scrlst_listSelectVar.lst_list \
		  .wm_VisuSorties.f_topl.scrlst_listVar.lst_list\
		  .wm_VisuSorties.f_topl.f_contener.b_<- \
		  $l_totale_sorties_simulees } \
    -state disabled
pack .wm_VisuSorties.f_topl.f_contener.b_->\
    .wm_VisuSorties.f_topl.f_contener.b_<- -side top

## Actions associees aux scrolled list et aux boutons precedents
bind .wm_VisuSorties.f_topl.scrlst_listVar.lst_list <ButtonPress-1> \
     {ListSelectStart .wm_VisuSorties.f_topl.scrlst_listVar.lst_list %y}
bind .wm_VisuSorties.f_topl.scrlst_listVar.lst_list <B1-Motion> \
    {ListSelectExtend .wm_VisuSorties.f_topl.scrlst_listVar.lst_list %y}
bind .wm_VisuSorties.f_topl.scrlst_listVar.lst_list <ButtonRelease-1> \
    {.wm_VisuSorties.f_topl.f_contener.b_-> configure -state normal}

bind .wm_VisuSorties.f_topl.scrlst_listSelectVar.lst_list <ButtonPress-1> \
    {ListSelectStart .wm_VisuSorties.f_topl.scrlst_listSelectVar.lst_list %y}
bind .wm_VisuSorties.f_topl.scrlst_listSelectVar.lst_list <B1-Motion> \
    {ListSelectExtend .wm_VisuSorties.f_topl.scrlst_listSelectVar.lst_list %y}
bind .wm_VisuSorties.f_topl.scrlst_listSelectVar.lst_list <ButtonRelease-1> \
    {.wm_VisuSorties.f_topl.f_contener.b_<- configure -state normal}

pack .wm_VisuSorties.f_topl.scrlst_listVar .wm_VisuSorties.f_topl.f_contener \
    .wm_VisuSorties.f_topl.scrlst_listSelectVar\
    -side left -expand true -padx 6 -pady 4

######### Remplissage du frame de droite (.wm_VisuSorties.f_topr) ###########
Creer_titre_souligne .wm_VisuSorties.f_topr.ts_titre "Liste des CLIMATS:"

## Troisieme scrolled List : annnes simulees
ScrolledListbox .wm_VisuSorties.f_topr.scrlst_listCli\
    -width 10 -height 20 -setgrid true
foreach annees $l_annees_simulees {
 .wm_VisuSorties.f_topr.scrlst_listCli.lst_list insert end $annees
}

## Quatrieme scrolled List : annees selectionnees
ScrolledListbox .wm_VisuSorties.f_topr.scrlst_listSelectCli\
    -width 10 -height 20 -setgrid true

## Boutons situes entre les deux scrolled list precedentes
frame .wm_VisuSorties.f_topr.f_contener
button .wm_VisuSorties.f_topr.f_contener.b_-> -text "->" \
    -command {InsertVar .wm_VisuSorties.f_topr.scrlst_listCli.lst_list \
		  .wm_VisuSorties.f_topr.scrlst_listSelectCli.lst_list \
		  .wm_VisuSorties.f_topr.f_contener.b_-> \
		  $l_annees_simulees} \
    -state disabled
button .wm_VisuSorties.f_topr.f_contener.b_<- -text "<-" \
    -command {InsertVar .wm_VisuSorties.f_topr.scrlst_listSelectCli.lst_list \
		  .wm_VisuSorties.f_topr.scrlst_listCli.lst_list\
		  .wm_VisuSorties.f_topr.f_contener.b_<- \
		  $l_annees_simulees} \
    -state disabled
pack .wm_VisuSorties.f_topr.f_contener.b_-> \
    .wm_VisuSorties.f_topr.f_contener.b_<- -side top

## Actions associees aux scrolledlist et aux boutons precedents
bind .wm_VisuSorties.f_topr.scrlst_listCli.lst_list <ButtonPress-1> \
     {ListSelectStart .wm_VisuSorties.f_topr.scrlst_listCli.lst_list %y}
bind .wm_VisuSorties.f_topr.scrlst_listCli.lst_list <B1-Motion> \
    {ListSelectExtend .wm_VisuSorties.f_topr.scrlst_listCli.lst_list %y}
bind .wm_VisuSorties.f_topr.scrlst_listCli.lst_list <ButtonRelease-1> \
    {.wm_VisuSorties.f_topr.f_contener.b_-> configure -state normal}

bind .wm_VisuSorties.f_topr.scrlst_listSelectCli.lst_list <ButtonPress-1> \
    {ListSelectStart .wm_VisuSorties.f_topr.scrlst_listSelectCli.lst_list %y}
bind .wm_VisuSorties.f_topr.scrlst_listSelectCli.lst_list <B1-Motion> \
    {ListSelectExtend .wm_VisuSorties.f_topr.scrlst_listSelectCli.lst_list %y}
bind .wm_VisuSorties.f_topr.scrlst_listSelectCli.lst_list <ButtonRelease-1> \
    {.wm_VisuSorties.f_topr.f_contener.b_<- configure -state normal}

pack .wm_VisuSorties.f_topr.scrlst_listCli .wm_VisuSorties.f_topr.f_contener \
     .wm_VisuSorties.f_topr.scrlst_listSelectCli\
    -side left -expand true -padx 6 -pady 4
}


#----------------------------------------------#
# fermeture des sous fenetres
#----------------------------------------------#
proc Fermer_VisuSorties {} {

    set list_fenetres [winfo children .]
    DetruireFenetresPrefixees .wm_VisuSorties $list_fenetres
}


#----------------------------------------------#
# Procedure qui se charge de verifier si       #
#  l'utilisateur a selectionne au moins une    #
#  variable et un climat. Si ce n'est pas le   #
#  cas, affichage d'un message d'erreur, sinon #
#  affichage des fenetres de visualisation.    #
#----------------------------------------------#
proc Afficher_VisuSorties {} {

  global LOGICIEL
  global DIR_TEMPORAIRE
  global NOM_STRATEGIE

  global visu_no_fen_variable

  # etat du bouton resume : appuye ou relache
  global etat_bresume
  # etat du bouton trace : appuye ou relache
  global etat_btrace
  # etat du bouton chronique : appuye ou relache
  global etat_bchronique
  # etat du bouton alimentation : appuye ou relache
  global etat_balimentation
  # Nombres de variables et nombres de climats selectionnes
  set nbvar [ eval [list .wm_VisuSorties.f_topl.scrlst_listSelectVar.lst_list size]]
  set nbclim [ eval [list .wm_VisuSorties.f_topr.scrlst_listSelectCli.lst_list size]]

  # On sauvegarde le contenu de la liste des variables selectionnees
  set liste_var_selct [ eval \
	    [list .wm_VisuSorties.f_topl.scrlst_listSelectVar.lst_list get 0 $nbvar]] 
  # On sauvegarde le contenu de la liste des climats selectionnes
  set liste_clim_selct [ eval \
            [list .wm_VisuSorties.f_topr.scrlst_listSelectCli.lst_list get 0 $nbclim]]
 
    if { $nbvar == 0 && $etat_bresume == 0 && $etat_btrace == 0 &&\
	     $etat_bchronique == 0 && $etat_balimentation == 0} { 
      # Affichage d'une fenetre contenant un premier message d'erreur
       CreerFenetreDialogue $LOGICIEL error \
           "Vous devez selectionner au moins une sortie avant d'afficher ! "
    } elseif { $nbclim == 0} { 
      # Affichage d'une fenetre contenant un deuxieme message d'erreur
       CreerFenetreDialogue $LOGICIEL error \
           "Vous devez selectionner au moins un climat avant d'afficher !"
    } else {

       # Si le bouton resume est appuye, on affiche le resume correspondant
       #  a chaque annee climatique selectionnee
       if {$etat_bresume == 1} {
          # Appel du fichier contenant la procedure Afficher_resume
          source VisualiserSortiesResume.tcl
	  Afficher_resume .wm_VisuSortiesResume \
	      $liste_clim_selct $DIR_TEMPORAIRE$NOM_STRATEGIE/
       }

       # Si le bouton trace est appuye, on affiche la trace correspondant
       #  a chaque annee climatique selectionnee
       if {$etat_btrace == 1} {
          # Appel du fichier contenant la procedure Afficher_trace
          source VisualiserSortiesTrace.tcl
          for {set j 0} {$j < $nbclim } {incr j} {
               set climat [lindex $liste_clim_selct $j]
               Afficher_trace .wm_VisuSortiesTrace$climat $climat \
		   $DIR_TEMPORAIRE$NOM_STRATEGIE/$climat/
          }
       }

       # Si le bouton chronique est appuye, on affiche la chronique
       #  correspondant a chaque annee climatique selectionnee
       if {$etat_bchronique == 1} {
          # Appel du fichier contenant la procedure Afficher_chronique
          source VisualiserSortiesChronique.tcl
          for {set j 0} {$j < $nbclim } {incr j} {
               set climat [lindex $liste_clim_selct $j]
               Afficher_chronique .wm_VisuSortiesChronique$climat $climat \
		   $DIR_TEMPORAIRE$NOM_STRATEGIE/$climat/ 1
          }
       }

       # Si le bouton alimentation est appuye, on affiche l'alimentation
       #  correspondant a chaque annee climatique selectionnee
       if {$etat_balimentation == 1} {
          # Appel du fichier contenant la procedure Afficher_alimentation
          source VisualiserSortiesAlimentation.tcl
          for {set j 0} {$j < $nbclim } {incr j} {
               set climat [lindex $liste_clim_selct $j]
               Afficher_alimentation .wm_VisuSortiesAlimentation$climat $climat 1
          }
       }

       # Affichage des fenetres de visualisation pour chaque variable
       # avec toutes les annees selectionnees
       source VisualiserSortiesVariables.tcl 

       for {set i 0} {$i < $nbvar } {incr i} {
	   incr visu_no_fen_variable
	   set var [lindex $liste_var_selct $i]
	   
	   Afficher_variables .wm_VisuSortiesVariable${visu_no_fen_variable} $var \
	       $liste_clim_selct $DIR_TEMPORAIRE$NOM_STRATEGIE/ 1
       }
    }
}


#--------------------------------------------------------#
# Procedure qui modifie l'etat du bouton resume lors d'un #
#  appui sur ce dernier.(appuye ou relache)              #
#--------------------------------------------------------#
# Parametre :                                            #
#     bouton : bouton indiquant le choix d'affichage du resume
#--------------------------------------------------------#   
proc Modifier_boutonresume {bouton} {
global etat_bresume
    if {$etat_bresume == 0} { 
      $bouton configure -relief sunken
      set etat_bresume 1
    } elseif {$etat_bresume == 1} {
      $bouton configure -relief raised
      set etat_bresume 0
    } 
}


#--------------------------------------------------------#
# Procedure qui modifie l'etat du bouton trace lors d'un #
#  appui sur ce dernier.(appuye ou relache)              #
#--------------------------------------------------------#
# Parametre :                                            #
#     bouton : bouton indiquant le choix d'affichage de  #
#               la trace.                                #
#--------------------------------------------------------#   
proc Modifier_boutontrace {bouton} {
global etat_btrace
    if {$etat_btrace == 0} { 
      $bouton configure -relief sunken
      set etat_btrace 1
    } elseif {$etat_btrace == 1} {
      $bouton configure -relief raised
      set etat_btrace 0
    } 
}

#--------------------------------------------------------#
# Procedure qui modifie l'etat du bouton chronique lors  #
#  d'un appui sur ce dernier.(appuye ou relache)         #
#--------------------------------------------------------#
# Parametre :                                            #
#     bouton : bouton indiquant le choix d'affichage de  #
#               la chronique.                            #
#--------------------------------------------------------#   
proc Modifier_boutonchronique {bouton} {
global etat_bchronique
    if {$etat_bchronique == 0} { 
      $bouton configure -relief sunken
      set etat_bchronique 1
    } elseif {$etat_bchronique == 1} {
      $bouton configure -relief raised
      set etat_bchronique 0
    } 
}

#--------------------------------------------------------#
# Procedure qui modifie l'etat du bouton alimentation    #
#  lors d'un appui sur ce dernier.(appuye ou relache)    #
#--------------------------------------------------------#
# Parametre :                                            #
#     bouton : bouton indiquant le choix d'affichage de  #
#               l'alimentation du troupeau.              #
#--------------------------------------------------------#   
proc Modifier_boutonalimentation {bouton} {
global etat_balimentation
    if {$etat_balimentation == 0} { 
      $bouton configure -relief sunken
      set etat_balimentation 1
    } elseif {$etat_balimentation == 1} {
      $bouton configure -relief raised
      set etat_balimentation 0
    } 
}


