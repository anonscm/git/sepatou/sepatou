# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : SauvegarderStrategie.tcl    
# Contenu : Sauvegarde de la description de l'exploitation
##############################################################

#-----------------------------------------------------------
# Ouverture d'une fenetre demandant de valider la sauvegarde
#-----------------------------------------------------------
proc SauvegarderStrategie {} {

  global LOGICIEL
  global CMD_LS
  global DIR_SIMULATIONS
  global FIC_STRATEGIE_TXT
  global NOM_EXPLOITATION
  global NOM_STRATEGIE

  if { [file exist $DIR_SIMULATIONS${NOM_EXPLOITATION}] == 0} {
	CreerFenetreDialogue $LOGICIEL error \
	    "Il faut sauvegarder au pr�alable l'exploitation."
  } else {
    catch {destroy .wm_SauvegarderStrategie}
    toplevel .wm_SauvegarderStrategie
    wm title .wm_SauvegarderStrategie "$LOGICIEL : Sauvegarde de la strategie courante"

    frame .wm_SauvegarderStrategie.f_aff
    label .wm_SauvegarderStrategie.f_aff.l_icon \
	-bitmap question -foreground red

    # le fichier a-t-il deja ete sauvegarde,
    # les repertoires du chemin existent-ils ?
    append dir_exploitation $DIR_SIMULATIONS $NOM_EXPLOITATION
    append dir_strategie $dir_exploitation /$NOM_STRATEGIE

    set existe_rep_strategie \
        [file exist ${DIR_SIMULATIONS}${NOM_EXPLOITATION}$NOM_STRATEGIE]
    set existe_fichier [file exist \
        ${DIR_SIMULATIONS}${NOM_EXPLOITATION}${NOM_STRATEGIE}$FIC_STRATEGIE_TXT]

    if {$existe_fichier == 0} {
	message .wm_SauvegarderStrategie.f_aff.msg_question \
	    -aspect 1000 -justify center \
	    -text "Voulez-vous vraiment sauvegarder \n la strat�gie : $NOM_STRATEGIE ?"
    } else {
	message .wm_SauvegarderStrategie.f_aff.msg_question \
	    -aspect 1000 -justify center \
	    -text "Voulez-vous vraiment sauvegarder l'exploitation : $NOM_STRATEGIE en �crasant la sauvegarde existante?"
    }

    pack .wm_SauvegarderStrategie.f_aff.l_icon -side left -padx 16 -pady 12
    pack .wm_SauvegarderStrategie.f_aff.msg_question -padx 8 -pady 8
    pack .wm_SauvegarderStrategie.f_aff

    
    ###Zone de commandes des boutons ok, annuler #############################

    frame .wm_SauvegarderStrategie.f_bottom -borderwidth 3
    pack .wm_SauvegarderStrategie.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button .wm_SauvegarderStrategie.f_bottom.b_ok -text Ok \
	    -command "Ok_SauvegarderStrategie $dir_exploitation \
                      $existe_rep_strategie $dir_strategie"
    button .wm_SauvegarderStrategie.f_bottom.b_cancel \
	-text Annuler -command "destroy .wm_SauvegarderStrategie"
    pack .wm_SauvegarderStrategie.f_bottom.b_ok \
         .wm_SauvegarderStrategie.f_bottom.b_cancel -side left -expand 1 

    ### Separateur
    frame .wm_SauvegarderStrategie.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack .wm_SauvegarderStrategie.f_sep -side bottom -fill x -pady 5
  }
}

#-----------------------------------------------------------
# sauvegarde du fichier FIC_STRATEGIE_TXT
#-----------------------------------------------------------
proc Ok_SauvegarderStrategie { dir_exploitation \
				   existe_rep_strategie dir_strategie} {

    global CMD_COPY
    global CMD_MKDIR
    global FIC_STRATEGIE_TXT
    global FIC_FOINTERPRETATION_LNU
    global FIC_INDICATEURS_LNU
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU
    global DIR_TEMPORAIRE

    if {$existe_rep_strategie == 0} {
	exec $CMD_MKDIR $dir_strategie
    }

    exec $CMD_COPY $DIR_TEMPORAIRE$FIC_STRATEGIE_TXT $dir_strategie
    exec $CMD_COPY $DIR_TEMPORAIRE$FIC_FOINTERPRETATION_LNU $dir_strategie
    exec $CMD_COPY $DIR_TEMPORAIRE$FIC_INDICATEURS_LNU $dir_strategie
    exec $CMD_COPY $DIR_TEMPORAIRE$FIC_REGLESPLANIFICATION_LNU $dir_strategie
    exec $CMD_COPY $DIR_TEMPORAIRE$FIC_REGLESOPERATOIRES_LNU $dir_strategie

    destroy .wm_SauvegarderStrategie
}

