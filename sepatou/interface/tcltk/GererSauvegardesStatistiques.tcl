# Copyright (C) 2005 Marie-Josée Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : GererSauvegardesStatistiques.tcl
# contenu : procedures permettant de gerer les statistiques sauvegardees
##################################################################

#----------------------------------------------------------------------------
# Creation de la fenetre de gestion des statistiques
#----------------------------------------------------------------------------
proc GererSauvegardesStatistiques {} {

    global LOGICIEL
    global DIR_SAUVEGARDES_STATISTIQUES
    
    if {[winfo exists .wm_GererStatistiques] == 0} {
	toplevel .wm_GererStatistiques
	wm title .wm_GererStatistiques "LOGICIEL : Gestion des statistiques sauvegardees"
	wm geometry .wm_GererStatistiques 400x500	
	
	label .wm_GererStatistiques.l_statistique -text STATISTIQUES
	pack .wm_GererStatistiques.l_statistique -padx 10 -pady 10

	### Zone des boutons ################################################
	frame .wm_GererStatistiques.f_boutons
	button .wm_GererStatistiques.f_boutons.b_enlever -text Enlever \
	    -command "Enlever_GererStatistiques"
	button .wm_GererStatistiques.f_boutons.b_afficher -text Afficher \
	    -command "Afficher_GererStatistiques"
	pack .wm_GererStatistiques.f_boutons.b_enlever \
	    .wm_GererStatistiques.f_boutons.b_afficher -side left -expand 1 
	pack .wm_GererStatistiques.f_boutons -padx 10 -pady 10 -expand 1


	### Zone de selection ###############################################
	frame .wm_GererStatistiques.f_topw -borderwidth 10
	pack .wm_GererStatistiques.f_topw -padx 20 -pady 20 -side top -expand 1

	#Creation de la Scrolled List
	ScrolledListbox .wm_GererStatistiques.f_topw.scrlst_list \
	    -width 400 -height 15 
	### Chargement des statistiques
	set l_fichiers_stat [recherche_fic $DIR_SAUVEGARDES_STATISTIQUES]
	set nb [llength $l_fichiers_stat]
	for  {set i 0} {$i < $nb} {incr i} {
	    set val [lindex $l_fichiers_stat $i]
	    set longeur_val [string length $val]
	    set val [string range $val 0 [expr $longeur_val - 4]]
	    .wm_GererStatistiques.f_topw.scrlst_list.lst_list insert end $val
	}
	pack .wm_GererStatistiques.f_topw.scrlst_list \
	    -padx 4 -side left -anchor n -expand 1
	
	
	###Zone de commandes du bouton fermer #############################
	
	frame .wm_GererStatistiques.f_bottom -borderwidth 3
	pack .wm_GererStatistiques.f_bottom -side bottom -fill x -pady 5
	button .wm_GererStatistiques.f_bottom.b_fermer -text Fermer \
	    -command "Fermer_GererStatistiques"
	pack .wm_GererStatistiques.f_bottom.b_fermer -side left -expand 1 
	### Separateur
	frame .wm_GererStatistiques.f_sep \
	    -width 100 -height 2 -borderwidth 1 -relief sunken
	pack .wm_GererStatistiques.f_sep -side bottom -fill x -pady 5
    }
}

#----------------------------------------------------------------------------
# fermeture des sous fentres
#----------------------------------------------------------------------------
proc Fermer_GererStatistiques {} {

    set list_fenetres [winfo children .]
    DetruireFenetresPrefixees .wm_GererStatistiques $list_fenetres
}

#----------------------------------------------------------------------------
# Enleve une statistique sauvegardée
#----------------------------------------------------------------------------
proc Enlever_GererStatistiques {} {

    global LOGICIEL
    global DIR_SAUVEGARDES_STATISTIQUES
    global CMD_REMOVE


    set list .wm_GererStatistiques.f_topw.scrlst_list.lst_list    
    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue $LOGICIEL error "Sélectionnez une image statistique"
    } else {
	set rep [$list get $i]
	set msg "Voulez-vous vraiment enlever \n la statistique : $rep"
	if { [Confirmer $LOGICIEL $msg] == 1} {
	    
	    # destruction du fichier
	    exec $CMD_REMOVE  ${DIR_SAUVEGARDES_STATISTIQUES}${rep}.ps

	    # mise a jour de la liste des statistiques
	    $list delete $i
	}
    }
}



#----------------------------------------------------------------------------
# Affiche une statistique sauvegardée
#----------------------------------------------------------------------------
proc Afficher_GererStatistiques {} {

    global LOGICIEL
    global DIR_SAUVEGARDES_STATISTIQUES

    set list .wm_GererStatistiques.f_topw.scrlst_list.lst_list    
    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue $LOGICIEL error "Sélectionnez une statistique"
    } else {
	set rep [$list get $i]
	exec gv ${DIR_SAUVEGARDES_STATISTIQUES}${rep}.ps
    }
}


