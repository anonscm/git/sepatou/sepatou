# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : GeneralImpression.tcl
# contenu : procedures permettant de definir les commandes d'impression
##################################################################

#----------------------------------------------------------------------------
# Creation de la fentre de demande des commandes
#----------------------------------------------------------------------------
proc GeneralImpression {} {

    global LOGICIEL
    global CMD_IMPRIMER
    global OPT_IMPRIMER

    global cmde_imprimer
    global cmde_options

    if {[winfo exists .wm_Sortie] == 0} {
	toplevel .wm_Impression
	wm title .wm_Impression "$LOGICIEL : Description des commandes d'impression"

	# Zone de demande des commandes ###############################
	frame .wm_Impression.f_cmd
	label .wm_Impression.f_cmd.l_imprimer -text "Commande d'impression"
	entry .wm_Impression.f_cmd.e_imprimer -textvariable cmde_imprimer \
	    -width 30 -relief sunken
	set cmde_imprimer $CMD_IMPRIMER 

	frame .wm_Impression.f_opt
	label .wm_Impression.f_opt.l_options -text "    Options d'impression "
	entry .wm_Impression.f_opt.e_options -textvariable cmde_options \
	    -width 30 -relief sunken
	set cmde_options $OPT_IMPRIMER

	pack .wm_Impression.f_cmd.l_imprimer -side left 
	pack .wm_Impression.f_cmd.e_imprimer -side right
	pack .wm_Impression.f_cmd -pady 15
	pack .wm_Impression.f_opt.l_options -side left
	pack .wm_Impression.f_opt.e_options -side right
	pack .wm_Impression.f_opt -pady 15


	# Zone de commandes des boutons ok et annuler ######################
	frame .wm_Impression.f_bottom -borderwidth 3
	button .wm_Impression.f_bottom.b_ok -text Ok \
	    -command "Enregistrer_Impression"
	button .wm_Impression.f_bottom.b_cancel -text Annuler \
	    -command "destroy .wm_Impression" 

	pack .wm_Impression.f_bottom.b_ok .wm_Impression.f_bottom.b_cancel \
	    -padx 20 -pady 10 -side left -expand 1
	pack .wm_Impression.f_bottom -side bottom -anchor s -fill x -pady 5

	### Separateur
	frame .wm_Impression.f_sep \
	    -width 100 -height 2 -borderwidth 1 -relief sunken
	pack .wm_Impression.f_sep -side bottom -fill x -pady 5
    }
}


#----------------------------------------------------------------------------
# verification des donnees, si OK : enregistrement et fermeture de la fenetre
#----------------------------------------------------------------------------
proc Enregistrer_Impression {} {

    global LOGICIEL
    global CMD_IMPRIMER
    global OPT_IMPRIMER

    global cmde_imprimer
    global cmde_options

    if {[PbControleChaine $cmde_imprimer nom]} {
	CreerFenetreDialogue $LOGICIEL error "Commande d'impression invalide"
    } else {
	set CMD_IMPRIMER $cmde_imprimer
	set OPT_IMPRIMER $cmde_options
	destroy .wm_Impression
    }
}
