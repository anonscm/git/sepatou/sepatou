# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

################################################################
# Fichier : SepatouFoUtilitaires.tcl                   
# Contenu : Ensemble de fonctions servant a creer des objets,
#           a executer des commandes et a gerer l'affichage 
#           de divers elements(titres, fenetres de dialogue).
################################################################


######################################################################
# enleve tous les zeros a gauche
######################################################################
proc EnleverZerosAGauche {n} {
set result $n

    while {([string range $result 0 0] == 0) && [string length $result] > 1} {
	set result [string range $result 1 [string length $result]]
    }

return $result
}


######################################################################
# remplace le 1er ~ par le chemin correspondant
######################################################################
proc Remplacer1erTildeDeVariable {v} {
global DIR_RACINE

    if { [string index $v 0] == "~" } {
	set tilde $DIR_RACINE
	set new_v $tilde
	append new_v [string range $v 1 end]
    } else { 
	set new_v $v
    }
}


######################################################################
# ArrondirA1Decimale : arrondit un nombre a 1 decimale
# parametre         : nb = nombre a arrondir
# evaluation        : nombre arrondi
######################################################################
proc ArrondirA1Decimale { nb } {

    set i_point [string first "."  $nb]

    if {$i_point == -1} {
	return $nb
    } else {
	set n [expr $nb * 10]
	set i_point [string first "."  $n]
	set n_arrondi [string range $n 0 [expr $i_point - 1]]
	if {[expr $n - $n_arrondi] >= 0.5} {
	    set n_arrondi [expr $n_arrondi + 1] 
	}
	return [expr $n_arrondi / 10.]
    }
}
	 

######################################################################
#Description : Creation d'une scrolled List box                      #
#parametre : parent : nom de la fenetre contener                     #
#            args : ensemble des parametres de la scrolled liste     #
######################################################################
proc ScrolledListbox { parent args } {

#Creation du cadre contenant tous les objets crees
frame $parent

#Creation de la listbox attachee a des scrollbars
    eval {listbox $parent.lst_list  \
	      -yscrollcommand [list $parent.scrb_y set] \
	      -xscrollcommand [list $parent.scrb_x set]} $args

#Creation des scrollbars et programmation de leurs reflexes
scrollbar $parent.scrb_x -orient horizontal \
    -command [list $parent.lst_list xview]
scrollbar $parent.scrb_y -orient vertical \
    -command [list $parent.lst_list yview]

#gestion des scrollbars
pack $parent.scrb_x -side bottom -fill x
pack $parent.scrb_y -side right -fill y

#gestion de la liste
pack $parent.lst_list -side left -fill both -expand true

return $parent.lst_list

}

#############################################
# Suite de petites procedures s'occupant de #
#  la gestion des elements appartenant a une#
#  ou a plusieurs scrolled listbox :        #
#############################################

#-------------------------------------#
# Procedure qui supprime les elements #
#  selectionnes dans la zone source.  #
# Parametre :                         #
#      w : zone source.               #
#-------------------------------------#
proc ListDeleteEnd {w} {
    foreach i [lsort -decreasing [$w curselection]] {
	$w delete $i
    }
}

#-----------------------------------------------#
# Procedure qui transfere les elts selectionnes de la zone source 
# vers la zone destination en respectant l'ordre d'affichage donne
# Parametres :                                  #
#         scr : zone source,                    #
#         dst : zone destination,               #
#         but : bouton de deplacement,
#         liste_ordonnee : liste ordonnee des elements 
#-----------------------------------------------#
proc InsertVar {scr dst but liste_ordonnee} {

    set liste_scr [$scr get 0 end]
    set liste_choisie ""
    foreach i [$scr curselection] {
	lappend liste_choisie [lindex $liste_scr $i]
    }
    set liste_dst [concat [$dst get 0 end] $liste_choisie]

    $dst delete 0 end

    foreach e $liste_ordonnee {
	if {[lsearch $liste_dst $e]  != -1} {
	    $dst insert end $e
	}
    }

    ListDeleteEnd $scr 
    $but configure -state disabled
}

#-----------------------------------------------#
# Procedure qui selectionne un ou des elements  #
#  d'une liste.                                 #
# Parametres :                                  #
#         w : liste qui contient les elements,  #
#         y : elements final a selectionner.    #
#-----------------------------------------------#
proc ListSelectStart {w y} {
    $w select anchor [$w nearest $y]
}

#-----------------------------------------------#
# Procedure qui ajoute un ou plusieurs element  #
#  a la selection courante.                     #
# Parametres :                                  #
#         w : liste qui contient les elements,  #
#         y : elements final a selectionner.    #
#-----------------------------------------------#
proc ListSelectExtend {w y} {
    $w select set anchor [$w nearest $y]
}

######################################################################
#Description : Creation d'une scrolled liste associee a 3 boutons    #
#              de commande et un titre                               #
#parametre : parent : nom de la fenetre contener                     #
#            text1 : 1ere partie du titre                            #
#            text2 : 2eme partie du titre                            #
#            Proc_Ajout : Nom du programme a executer pour           #
#                         le bouton Ajouter                          #
#            Proc_Editer : Nom du programme a executer pour          #
#                          le bouton Editer                          #
#            Proc_Elever : Nom du programme a executer pour          #
#                          le bouton Enlver                          #
#            mode_visualisation : 'lecture' ou 'ecriture'            #
######################################################################
proc CreerList {parent text1 text2\
    Proc_Ajout Proc_Editer Proc_Enlever dir mode_visualisation} {

#Creation du cadre contenant tous les objets crees
frame $parent

#Creation du titre associe a la Scrolled Listbox
#Le titre s'etend sur 2 lignes
label $parent.l_titre1 -text "$text1"
label $parent.l_titre2 -text "$text2"

#creation de la Scrolled Listbox
ScrolledListbox $parent.scrlst_bot \
    -width 20 -height 20 -setgrid true

#Creation des boutons de commandes de la list
#Association des actions a effectuees aux boutons correspondants
frame $parent.f_mid
button $parent.f_mid.b_ajout -text Ajouter -command "$Proc_Ajout $dir $mode_visualisation"
if {[string compare "lecture" $mode_visualisation] == 0} {
    $parent.f_mid.b_ajout configure -state disabled
}

button $parent.f_mid.b_enlev -text Enlever \
    -command "$Proc_Enlever $parent.scrlst_bot.lst_list"
if {[string compare "lecture" $mode_visualisation] == 0} {
    $parent.f_mid.b_enlev configure -state disabled
}

button $parent.f_mid.b_edit -text Editer \
    -command "$Proc_Editer $dir $parent.scrlst_bot.lst_list $mode_visualisation"

frame $parent.f_mid_option
tixOptionMenu $parent.f_mid_option.opm_op 

#gestion des boutons
pack $parent.f_mid.b_ajout $parent.f_mid.b_enlev \
    $parent.f_mid.b_edit \
    -side left -padx 3
pack $parent.f_mid_option.opm_op -side left -padx 3

#association du titre et des 2 frames crees
pack $parent.l_titre1 $parent.l_titre2 -side top
pack $parent.l_titre2 $parent.f_mid $parent.f_mid_option $parent.scrlst_bot \
    -side top -pady 6 -expand true

}

#-------------------------------------------#
# Procedure qui se charge de creer des      #
#  elements compris entre les chiffres a et #
#  b passes en parametres                   #
#-------------------------------------------#
proc Num {menu a b} {

    #boucle sur le nb d'elements 
    for {set i $a} {$i <= $b} {incr i} {
	$menu add command $i -label "$i"
	
    }
}

#-------------------------------------------#
# Procedure qui se charge de creer un titre #
#  souligne dont le nom et l'intitule sont  #
#  donnes en parametres.                    #
#-------------------------------------------#
proc Creer_titre_souligne {nom_titre titre} {
  text $nom_titre -width 16 -height 1 -relief flat -background gray83
  $nom_titre tag add "souligne" 1.0 end
  $nom_titre tag configure "souligne" -underline yes
  $nom_titre insert end $titre souligne
  $nom_titre configure -state disabled
  pack $nom_titre -side top -fill x -expand 1 -anchor n
}

#-------------------------------------------#
# Procedure qui se charge de creer une fenetre de dialogue
# Parametres :                              #
#      logiciel : nom du logiciel
#      bmp     : icone a afficher
#      message : texte affiche dans la      #
#                 fenetre.                  #
#-------------------------------------------#
proc CreerFenetreDialogue {logiciel bmp message} {
   set fenetre .wm_FenetreDialogue
   toplevel $fenetre
   wm title $fenetre "$logiciel : dialogue"

   button $fenetre.b_ok -text Ok -command "destroy $fenetre" 
   pack $fenetre.b_ok -side bottom -pady 4
   frame $fenetre.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
   pack $fenetre.f_sep -side bottom -fill x -pady 4
   label $fenetre.l_icon -bitmap $bmp -foreground red
   pack $fenetre.l_icon -side left -padx 16 -pady 12
   message $fenetre.msg -justify center -aspect 300 -justify center -text $message
   pack $fenetre.msg -side right -padx 8 -pady 8 -fill both  -expand true
   # On inhibe l'acces aux autres fenetres 
   #  tant que le bouton Ok n'a pas ete appuye
   grab $fenetre
   tkwait window $fenetre
   grab release $fenetre
}

#-------------------------------------------#
# Procedure qui se charge de donner l'unite #
#  correspondant a une variable.            #
# Parametres :                              #
#    variable : variable dont on cherche    #
#                  l'unite,                 #
# En sortie :                               #
#    unite : unite de la variable en entree.#
#-------------------------------------------#
proc Unite_variable {variable} {
 global UNITES_VARIABLES
 set unite " "
 set nb_correspondances [ llength $UNITES_VARIABLES ]
 for {set i 0} {$i < $nb_correspondances} {incr i 1} {
    set correspondance_courante [lindex $UNITES_VARIABLES $i]
    if { [string compare [lindex $correspondance_courante 0] $variable] == 0} {
       set unite [lindex $correspondance_courante 1]
    }
 }
 return $unite
}

######################################################################
# Procedure   : PbControleChaine                                      #
# Description : verifie que chaine soit non vide et du type indique  #
# Parametres  : chaine : contient la chaine de caractere a controler #
#               type : 'nom' 'entier' 'reel'   
# ATTENTION : entiers et r�els POSITIFS UNIQUEMENT
# Evaluation  : 1 = invalide, 0 = valide, -1 = erreur
######################################################################
proc PbControleChaine {chaine type} {
    set l [string length $chaine]

    if {$l == 0} {
	set invalide 1   ;# false
    } elseif {[string compare $type "nom"]==0} {
	set comp "a b c d e f g h i j k l m n o p q r s t u v w x y z \
	              A B C D E F G H I J K L M N O P Q R S T U V W X Y Z \
	              0 1 2 3 4 5 6 7 8 9 _ !"
        set invalide 0
    } elseif {[string compare $type "fichier"]==0} {
	set comp "a b c d e f g h i j k l m n o p q r s t u v w x y z \
	              A B C D E F G H I J K L M N O P Q R S T U V W X Y Z \
	              0 1 2 3 4 5 6 7 8 9"
        set invalide 0
    } elseif {[string compare $type "entier"]==0} {
	set comp "0 1 2 3 4 5 6 7 8 9"
        set invalide 0
    } elseif {[string compare $type "reel"]==0} {
	set comp "0 1 2 3 4 5 6 7 8 9 ."
	# verification qu'il n'y ait pas plusieurs .
	set i_point [string first . $chaine]
	if {$i_point == -1} {
	    set invalide 0 ;# pas de point	
	} else {
	    if {[string first . [string range $chaine [expr $i_point + 1] end] ] == -1} {
		set invalide 0 ;# un point	
	    } else {
		set invalide 1 ;# plus de 1 point	
	    }
	}
    } else {
        puts "La proc�dure ControlerChaine est appel�e avec un mauvais type"
        set invalide -1
    }

    set i 0
    while {$i < $l && $invalide == 0} {
	set invalide 1
	set car [string index $chaine $i]
	if {[string first $car $comp] != -1} {
	    set invalide 0  ;# true
	}
	incr i
    }

    return $invalide
}


######################################################################
#Procedure : Recherche_fic                                           #
#Description : Recherche tous les fichiers qui se trouve             #
#              directement sous le chemin donne                      #
#parametre : chemin : contient le chemin ou doit s'effectuer la      #
#            recherche                                               #
######################################################################
proc recherche_fic {chemin} {

    global CMD_LS

    if {[string index $chemin [expr [string length $chemin] - 1]] != "/"} {
	append chemin_complet  $chemin "/"
    } else {
	set chemin_complet $chemin
    }

    set l [list]
    foreach f [exec $CMD_LS $chemin_complet] {
	if [file isfile $chemin_complet$f] {
	    lappend l $f
	}
    }
    return $l
}


######################################################################
#Procedure : Recherche_rep                                           #
#Description : Recherche tous les repertoires qui se trouve          #
#              directement sous le chemin donne                      #
#parametre : chemin : contient le chemin ou doit s'effectuer la      #
#            recherche                                               #
######################################################################
proc recherche_rep {chemin} {

    global CMD_LS

    if {[string index $chemin [string length $chemin]] != "/"} {
	append chemin_complet  $chemin "/"
    } else {
	set chemin_complet $chemin
    }

    set l [list]
    foreach f [exec $CMD_LS $chemin_complet] {
	if [file isdirectory $chemin_complet$f] {
	    lappend l $f
	}
    }
    return $l
}



#----------------------------------------------------#
# Procedure qui renvoie la chaine apres "= "         #
# de la chaine donnee en parametre                   #
#----------------------------------------------------#
proc SortirValeur {ligne} {

set place_egal [string first "=" $ligne]

    if { $place_egal == -1 } {
       return $ligne
    } else {
       return  [string range $ligne  [expr $place_egal + 2]  end ]
    }
}

#----------------------------------------------------#
# Destruction des fenetres de la liste donnee
# dont le nom commence par le prefixe donne
#----------------------------------------------------#
proc DetruireFenetresPrefixees {prefixe liste_fenetres} {

    append l $liste_fenetres " "
    set chercher 1
    while {$chercher} {
	set place_prefixe [string first $prefixe $l]
	if {$place_prefixe == -1} {
	    set chercher 0
	} else {
	    set l [string range $l $place_prefixe end]
	    set place_blanc [string first " " $l]
	    destroy [string range $l 0  [expr $place_blanc - 1]]
	    set  l [string range $l  $place_blanc end]
	}
    }
}

#----------------------------------------------------#
# renvoie la chaine donnee en entree moins le dernier
# caractere (lorsque l'on lit le contenu d'un texte
# il est ajoute)
#----------------------------------------------------#
proc EpurerText {txt} {
    set l  [string length $txt]
    return [string range $txt  0 [expr $l - 2]]
}

#----------------------------------------------------#
# renvoie la chaine donnee en argument moins les 2 premiers
# caracteres et les 2 derniers (si la chaine a au moins
# 4 caracteres
#----------------------------------------------------#
proc Enlever2caracteresDebutFin {chaine} {
    set l [string length $chaine]
    if {$l > 3} {
	set txt $chaine
        set txt [string range $txt 2 end]
	set txt [string range $txt 0 [expr [string length $txt] - 3] ]
    } else {
	set txt ""
    }
    return $txt
}			


#--------------------------------------------------------------------
# Procedure qui renvoie le enieme mot d'une ligne de caracteres 
# (chaque mot etant separe par des espaces et une tabulation).
# Parametre :                                           
#   ligne : string sur laquelle on recherche le mot,     
#   no : indice du mot a trouver (0 -> 1er, 1 -> 2e,..) 
# En sortie : mot trouve.                               
#--------------------------------------------------------------------
proc Enieme_mot_de_ligne { ligne no } {

    set ind 0
    set l  [string length $ligne]
    for { set m 0 } { $m < $no } { incr m } {
       while {[string index $ligne $ind] != "\t" && $ind < $l} {
          incr ind
	}
    incr ind
    }
    if {$ind == $l} {
	# aucune tabulation n'a ete trouve dans ligne
	puts "ERREUR : appel de Enieme_mot_de_ligne avec une chaine sans tabulation"
	return "ERREUR"
    } else {
	# Initialisation du mot par sa premiere lettre
	set mot [string index $ligne $ind]
	incr ind
	# Tantque l'on n'est pas arrive a la fin du mot, enregistrer le mot
	while {[string index $ligne $ind] != " " && \
		   [string index $ligne $ind] != "\t"} {
	    set mot [append mot [string index $ligne $ind]]
	    incr ind
	}
	return $mot
    }
}


#------------------------------------------------#
# Procedure qui d�termine la visualisation des cellules
# de la grid donn�e en parametre.
#------------------------------------------------#
proc EditGrid_format {w area x1 y1 x2 y2} {
    global editgrid

    set bg(s-margin) gray65
    set bg(x-margin) gray65
    set bg(y-margin) gray65
    set bg(main)     gray20

    case $area {
	main {
            # Le format de la grille consiste en une succession de
            #  cellules n'ayant pqs de bords apparents 
	    $w format grid $x1 $y1 $x2 $y2 \
		-relief raised -bd 1 -bordercolor $bg($area) -filled 0 \
                -bg red -xon 1 -yon 1 -xoff 0 -yoff 0 -anchor se
	}
	{x-margin y-margin s-margin} {
            # Le bord specifie les bords consecutifs apparents
	    $w format border $x1 $y1 $x2 $y2 \
		-fill 1 -relief raised -bd 1 -bg $bg($area) \
		-selectbackground gray80
	}
    }
}



#------------------------------------------------#
# Procedure qui indique que seules les cellules de la grid
# dont la ligne et la colonne ne sont pas 0 sont editable
# Parametres:                                    #
#           x:numero de la ligne                 #
#           y:numero de la colonne               #
#------------------------------------------------#
proc Editable {x y} {
    if {$y!=0 && $x!=0} {
	return 1
    }  else {
	return 0
    }
}


#----------------------------------------------------------------------------
# Demande de confirmation 
#----------------------------------------------------------------------------
proc Confirmer {logiciel message} {

    global reponse  ;# pour procedure Ok ou Annuler
    set reponse 0

    # Creation de la fenetre
    toplevel .wm_FenetreDialogue
    wm title .wm_FenetreDialogue {$logiciel : dialogue}

    # zone d'affichage ###################################################
    frame .wm_FenetreDialogue.f_aff
    label .wm_FenetreDialogue.f_aff.l_icon \
	-bitmap question -foreground red
    message .wm_FenetreDialogue.f_aff.msg_question \
	    -aspect 1000 -justify center \
	    -text $message
    pack .wm_FenetreDialogue.f_aff.l_icon -side left -padx 16 -pady 12
    pack .wm_FenetreDialogue.f_aff.msg_question -padx 8 -pady 8
    pack .wm_FenetreDialogue.f_aff

    
    ###Zone de commandes des boutons ok, annuler #############################

    frame .wm_FenetreDialogue.f_bottom -borderwidth 3
    pack .wm_FenetreDialogue.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button .wm_FenetreDialogue.f_bottom.b_ok -text Ok \
	-command "global reponse; set reponse 1; destroy .wm_FenetreDialogue"
    button .wm_FenetreDialogue.f_bottom.b_cancel -text Annuler \
	-command "global reponse; set reponse 0; destroy .wm_FenetreDialogue"
    pack .wm_FenetreDialogue.f_bottom.b_ok \
         .wm_FenetreDialogue.f_bottom.b_cancel -side left -expand 1 

    ### Separateur
    frame .wm_FenetreDialogue.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack .wm_FenetreDialogue.f_sep -side bottom -fill x -pady 5

    # On inhibe l'acces aux autres fenetres 
    grab .wm_FenetreDialogue
    tkwait window .wm_FenetreDialogue
    grab release .wm_FenetreDialogue

    return $reponse
}



#--------------------------------------------------------------------
# renvoie le nom de la station meteo contenu dans FIC_SIMULATEUR_TXT
#--------------------------------------------------------------------
proc LireNomStationMeteo {} {
    
    global FIC_SIMULATEUR_TXT
    global DIR_TEMPORAIRE

    set nom ""

    if {[catch {set file [open "${DIR_TEMPORAIRE}$FIC_SIMULATEUR_TXT" r]}] \
	== 0} {
	seek $file 0 start
	gets $file line ;# jourd
	gets $file line ;# moid
	gets $file line ;# jourf
	gets $file line ;# moisf
	gets $file line 
	set nom [SortirValeur $line]
	close $file
    }

    return $nom
}

#--------------------------------------------------------------------
# renvoie la graine donnee au generateur climatique
#--------------------------------------------------------------------
proc LireGraine {} {
    
    global FIC_SIMULATEUR_TXT
    global DIR_TEMPORAIRE

    set seed ""

    if {[catch {set file [open "${DIR_TEMPORAIRE}$FIC_SIMULATEUR_TXT" r]}] \
	== 0} {
	seek $file 0 start
	gets $file line ;# jourd
	gets $file line ;# moid
	gets $file line ;# jourf
	gets $file line ;# moisf
	gets $file line ;# station_meteo
	gets $file line ;# nb_annees_generees
	gets $file line 
	set seed [SortirValeur $line]
	close $file
    }

    return $seed
}


#----------------------------------------------------------------#
# renvoie le nombre d'annees a generer
#----------------------------------------------------------------#
proc LireNbAnneesAGenerer {} {

    global DIR_TEMPORAIRE
    global FIC_SIMULATEUR_TXT

    set nb_annees_generees ""

    # lecture des informations
    if {[catch {set file [open "$DIR_TEMPORAIRE$FIC_SIMULATEUR_TXT" r]}] == 0} {
	gets $file line ;# jourd
	gets $file line ;# moid
	gets $file line ;# jourf
	gets $file line ;# moisf
	gets $file line ;# station_meteo
	gets $file line ;# nb annees generees
	set nb_annees_generees [SortirValeur $line]
	close $file
    }

    return $nb_annees_generees
}
   

#----------------------------------------------------------------#
# lister les annees a simuler reelles et generees
#----------------------------------------------------------------#
proc ListerAnneesASimuler {} {
    global DIR_TEMPORAIRE
    global FIC_SIMULATEUR_TXT
    global ANNEE_GENEREE

    set liste_annees_asimuler [list]

    # lecture des informations
    if {[catch {set file [open "$DIR_TEMPORAIRE$FIC_SIMULATEUR_TXT" r]}] == 0} {
	seek $file 0 start
	gets $file line ;# jourd
	gets $file line ;# moid
	gets $file line ;# jourf
	gets $file line ;# moisf
	gets $file line ;# station_meteo
	gets $file line ;# nb annees generees
	set nb_annees_generees [SortirValeur $line]
	gets $file line ;# graine
	while { [gets $file line] > 0} {
	    lappend liste_annees_asimuler $line
	}
	close $file

	for {set i 0} {$i < $nb_annees_generees} {incr i} {
	    lappend liste_annees_asimuler [expr $ANNEE_GENEREE + $i]
	}
    } 
    return $liste_annees_asimuler
}
    

#--------------------------------------------------------------------
# renvoie la liste des noms de parcelles
#--------------------------------------------------------------------
proc LireParcelles {} {
    
    global FIC_EXPLOITATION_TXT
    global DIR_TEMPORAIRE

    set liste_parcelles ""

    if {[catch {set file [open "$DIR_TEMPORAIRE$FIC_EXPLOITATION_TXT" r]}] == 0} {

	gets $file line ;# nom exploitation
	gets $file line ;# localisation
	gets $file line ;# quota
	gets $file line ;#Entete "***PARCELLES"
	gets $file line
	#Tant que l'entete troupeau n'est pas atteint,
	#C'est qu'il reste des parcelles a charger
	while {[string compare "***TROUPEAU" $line] != 0} {
	    lappend liste_parcelles [SortirValeur $line]
	    #Chargement des caracteristiques de la parcelle
	    gets $file line ;# surface
	    gets $file line ;# ru
	    gets $file line ;# caracteristique
	    gets $file line ;# type
	    gets $file line  ;# eloignement
	    gets $file line ;# espece

	    gets $file line
	}
    }
    return $liste_parcelles
}

