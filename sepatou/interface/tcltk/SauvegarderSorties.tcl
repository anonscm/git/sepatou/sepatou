# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : SauvegarderSorties.tcl
# Contenu : Affichage de la fenetre de choix des sorties a sauvegarder
##############################################################

#-------------------------------------#
# Procedure qui se charge de creer et #
#  d'afficher la fenetre de choix des #
#  sorties a sauvegarder.             #
#-------------------------------------#
proc SauvegarderSorties {} {

  global LOGICIEL
  global DIR_SIMULATIONS
  global DIR_TEMPORAIRE
  global FIC_PROBLEME
  global FIC_TRACE
  global FIC_CHRONIQUE
  global NOM_EXPLOITATION
  global NOM_STRATEGIE
  global VAR_SORTIES_POSSIBLES

  global liste_sorties_simulations
  global liste_sorties_selectionnees
  global l_totale_sorties_simulees
  global liste_climats_simules
  global liste_climats_selectionnes
  global l_totale_annees_simulees

  if { [file exist $DIR_SIMULATIONS${NOM_EXPLOITATION}/${NOM_STRATEGIE}] == 0} {
	CreerFenetreDialogue $LOGICIEL error \
	    "Il faut sauvegarder au pr�alable l'exploitation puis la strat�gie."
  } else {
    # Ne pas afficher deux fois la meme fenetre
    catch {destroy .wm_SauvegarderSorties}
    # Creation d'une fenetre
    toplevel .wm_SauvegarderSorties
    wm title .wm_SauvegarderSorties "$LOGICIEL : Sauvegarde des sorties"

    # La fenetre est composee de quatre frames alignees verticalement
    frame .wm_SauvegarderSorties.f_down -borderwidth 2
    pack .wm_SauvegarderSorties.f_down -side bottom
    frame .wm_SauvegarderSorties.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack .wm_SauvegarderSorties.f_sep -side bottom -fill x -pady 5
    frame .wm_SauvegarderSorties.f_top -borderwidth 2
    pack .wm_SauvegarderSorties.f_top -side bottom -fill both -expand true
    frame .wm_SauvegarderSorties.f_toptop -borderwidth 2
    pack .wm_SauvegarderSorties.f_toptop -side bottom
    
    # Le frame du centre est lui-meme compose de deux frames
    #  alignes horizontalement.
    frame .wm_SauvegarderSorties.f_top.f_gauche -borderwidth 2 -relief raised
    frame .wm_SauvegarderSorties.f_top.f_droite -borderwidth 2 -relief raised
    pack .wm_SauvegarderSorties.f_top.f_gauche .wm_SauvegarderSorties.f_top.f_droite \
	-side left -pady 4 -fill both -expand true
    
    # Le premier frame contient un message utilisateur
    label .wm_SauvegarderSorties.f_toptop.txt -text "Veuillez selectionner les sorties que vous \
    souhaitez enregistrer :"
    pack .wm_SauvegarderSorties.f_toptop.txt -padx 8 -pady 5
    
    # Le troisieme frame contient les commandes Ok et Fermer
    button .wm_SauvegarderSorties.f_down.b_ok -text {Sauvegarder} -command Sauvesort
    button .wm_SauvegarderSorties.f_down.b_fermer -text {Fermer} -command "destroy .wm_SauvegarderSorties"
    pack .wm_SauvegarderSorties.f_down.b_ok .wm_SauvegarderSorties.f_down.b_fermer \
	-side left -expand 1 -padx 60 -pady 4
    
    ############### Remplissage du frame du centre de gauche  #################
    Creer_titre_souligne .wm_SauvegarderSorties.f_top.f_gauche.ts_titre "Liste des SORTIES:"
    
    ## Premiere scrolled List : sorties obtenues apres simulations
    set liste_sorties_simulations [ScrolledListbox .wm_SauvegarderSorties.f_top.f_gauche.scrlst_listVar \
				      -width 10 -height 15 -setgrid true]

    # lecture des sorties obtenues dans un repertoire
    set l_totale_annees_simulees [recherche_rep $DIR_TEMPORAIRE$NOM_STRATEGIE]
    set a [lindex $l_totale_annees_simulees 0]
    set l_totale_sorties_simulees [recherche_fic $DIR_TEMPORAIRE$NOM_STRATEGIE/$a]

    foreach s $l_totale_sorties_simulees {
	$liste_sorties_simulations insert end $s
    }

    ## Seconde scrolled List : sorties selectionnees
    set liste_sorties_selectionnees\
	[ScrolledListbox .wm_SauvegarderSorties.f_top.f_gauche.scrlst_listSelectVar \
					-width 10 -height 15 -setgrid true]
    
    ## Boutons situes entre les deux scrolled list precedentes
    frame .wm_SauvegarderSorties.f_top.f_gauche.f_contener
    button .wm_SauvegarderSorties.f_top.f_gauche.f_contener.b_-> -text "->" \
	-command {InsertVar $liste_sorties_simulations \
		      $liste_sorties_selectionnees \
		      .wm_SauvegarderSorties.f_top.f_gauche.f_contener.b_-> \
		      $l_totale_sorties_simulees} \
	-state disabled
    button .wm_SauvegarderSorties.f_top.f_gauche.f_contener.b_<- -text "<-" \
	-command {InsertVar $liste_sorties_selectionnees \
		      $liste_sorties_simulations \
		      .wm_SauvegarderSorties.f_top.f_gauche.f_contener.b_<-\
		      $l_totale_sorties_simulees } \
	-state disabled
    pack .wm_SauvegarderSorties.f_top.f_gauche.f_contener.b_-> \
	.wm_SauvegarderSorties.f_top.f_gauche.f_contener.b_<- -side top
    
    ## Actions associees aux 2 scrolled list et aux 2 boutons precedents
    bind $liste_sorties_simulations <ButtonPress-1> \
	{ListSelectStart $liste_sorties_simulations %y}
    bind $liste_sorties_simulations <B1-Motion> \
	{ListSelectExtend $liste_sorties_simulations %y}
    bind $liste_sorties_simulations <ButtonRelease-1> \
	{.wm_SauvegarderSorties.f_top.f_gauche.f_contener.b_-> configure -state normal}
    
    bind $liste_sorties_selectionnees <ButtonPress-1> \
	{ListSelectStart $liste_sorties_selectionnees %y}
    bind $liste_sorties_selectionnees <B1-Motion> \
	{ListSelectExtend $liste_sorties_selectionnees %y}
    bind $liste_sorties_selectionnees <ButtonRelease-1> \
	{.wm_SauvegarderSorties.f_top.f_gauche.f_contener.b_<- configure -state normal}
    
    pack .wm_SauvegarderSorties.f_top.f_gauche.scrlst_listVar \
	.wm_SauvegarderSorties.f_top.f_gauche.f_contener \
	.wm_SauvegarderSorties.f_top.f_gauche.scrlst_listSelectVar \
	-side left -expand true  -padx 6 -pady 4
    
    ######### Remplissage du frame du centre de droite  ###########
    Creer_titre_souligne .wm_SauvegarderSorties.f_top.f_droite.ts_titre "Liste des CLIMATS:"
    
    ## Troisieme scrolled List :  annees climatiques simulees
    set liste_climats_simules \
	[ScrolledListbox .wm_SauvegarderSorties.f_top.f_droite.scrlst_listCli \
				   -width 10 -height 15 -setgrid true]
    foreach annees $l_totale_annees_simulees {
	$liste_climats_simules insert end $annees
    }
    
    ## Quatrieme scrolled List : annees climatiques simulees selectionnees
    set liste_climats_selectionnes [ScrolledListbox .wm_SauvegarderSorties.f_top.f_droite.scrlst_listSelectCli \
					-width 10 -height 15 -setgrid true]
    
    ## Boutons situes entre les deux scrolled list precedentes
    frame .wm_SauvegarderSorties.f_top.f_droite.f_contener
    button .wm_SauvegarderSorties.f_top.f_droite.f_contener.b_-> -text "->" \
	-command {InsertVar $liste_climats_simules \
		      $liste_climats_selectionnes \
		      .wm_SauvegarderSorties.f_top.f_droite.f_contener.b_-> \
		      $l_totale_annees_simulees } \
	-state disabled
    button .wm_SauvegarderSorties.f_top.f_droite.f_contener.b_<- -text "<-" \
	-command {InsertVar $liste_climats_selectionnes \
		      $liste_climats_simules \
		      .wm_SauvegarderSorties.f_top.f_droite.f_contener.b_<- \
		      $l_totale_annees_simulees } \
	-state disabled
    pack .wm_SauvegarderSorties.f_top.f_droite.f_contener.b_-> \
	.wm_SauvegarderSorties.f_top.f_droite.f_contener.b_<- -side top
    
    ## Actions associees aux scrolledlist et aux boutons precedents
    bind $liste_climats_simules <ButtonPress-1> \
	{ListSelectStart $liste_climats_simules %y}
    bind $liste_climats_simules <B1-Motion> \
	{ListSelectExtend $liste_climats_simules %y}
    bind $liste_climats_simules <ButtonRelease-1> \
	{.wm_SauvegarderSorties.f_top.f_droite.f_contener.b_-> configure -state normal}
    
    bind $liste_climats_selectionnes <ButtonPress-1> \
	{ListSelectStart $liste_climats_selectionnes %y}
    bind $liste_climats_selectionnes <B1-Motion> \
	{ListSelectExtend $liste_climats_selectionnes %y}
    bind $liste_climats_selectionnes <ButtonRelease-1> \
	{.wm_SauvegarderSorties.f_top.f_droite.f_contener.b_<- configure -state normal}
    
    pack .wm_SauvegarderSorties.f_top.f_droite.scrlst_listCli \
	.wm_SauvegarderSorties.f_top.f_droite.f_contener \
	.wm_SauvegarderSorties.f_top.f_droite.scrlst_listSelectCli \
	-side left -expand true -padx 6 -pady 4
  }
}


#-------------------------------------#
# Procedure qui se charge de verifier #
#  les choix de l'utilisateur et de   #
#  sauvegarder les resultats choisis. #
#-------------------------------------#
proc Sauvesort {} {

    global LOGICIEL
    global CMD_COPY
    global CMD_LS
    global CMD_MKDIR
    global DIR_TEMPORAIRE
    global DIR_SIMULATIONS
    global NOM_STRATEGIE
    global NOM_EXPLOITATION
    global FIC_PROBLEME

    global liste_sorties_selectionnees
    global liste_climats_selectionnes

    # Nombres de sorties et nombres de climats selectionnes
    set nb_sorties [ eval \
	 [list $liste_sorties_selectionnees size]]
    set nb_climats [ eval \
	 [list $liste_climats_selectionnes size]]
    # On sauvegarde le contenu de la liste des sortieses selectionnees
    set liste_sorties_selct [ eval \
	  [list $liste_sorties_selectionnees get 0 $nb_sorties]]
    # On sauvegarde le contenu de la liste des climats selectionnes
    set liste_climats_selct [ eval \
          [list $liste_climats_selectionnes get 0 $nb_climats]]
    
    if { $nb_sorties == 0 } { 
	# Affichage d'un message d'erreur
	CreerFenetreDialogue $LOGICIEL error "Aucune sortie selectionnee"
    } elseif { $nb_climats == 0 } { 
	# Affichage d'une fenetre contenant un deuxieme message d'erreur
	CreerFenetreDialogue $LOGICIEL error "Aucun climat selectionne"
    } else {
	# Sauvegarde des resultats selectionnes puis affichage fin sauvegarde
	set nom_station [LireNomStationMeteo]

	if { [file exist $DIR_SIMULATIONS${NOM_EXPLOITATION}/${NOM_STRATEGIE}/${nom_station}] == 0} {
	    exec $CMD_MKDIR $DIR_SIMULATIONS$NOM_EXPLOITATION/$NOM_STRATEGIE/${nom_station}
	}


	for {set j 0} {$j < $nb_climats} {incr j 1} {
	    set clim [lindex $liste_climats_selct $j]

	    if {[file exist $DIR_SIMULATIONS${NOM_EXPLOITATION}/${NOM_STRATEGIE}/${nom_station}/${clim}]  == 0 } {
		exec $CMD_MKDIR $DIR_SIMULATIONS$NOM_EXPLOITATION/$NOM_STRATEGIE/${nom_station}/${clim}
	    }

	    for {set i 0} {$i < $nb_sorties} {incr i 1} {
		set nomfic [lindex $liste_sorties_selct $i]

		if [file exist $DIR_TEMPORAIRE$NOM_STRATEGIE/${clim}/$nomfic] {
		    set existe [file exist $DIR_SIMULATIONS${NOM_EXPLOITATION}/${NOM_STRATEGIE}/${nom_station}/${clim}/${nomfic}]
		    set confirmation 0
		    if { $existe == 1 } {
			set msg "Le fichier $DIR_SIMULATIONS$NOM_EXPLOITATION/$NOM_STRATEGIE/$nom_station/$clim/$nomfic existe deja. 
Voulez-vous l'ecraser ?"
			set confirmation [Confirmer $LOGICIEL $msg]
		    }

		    if {$existe == 0 || $confirmation == 1} {
			exec $CMD_COPY $DIR_TEMPORAIRE$NOM_STRATEGIE/${clim}/$nomfic \
			    $DIR_SIMULATIONS$NOM_EXPLOITATION/$NOM_STRATEGIE/${nom_station}/${clim}/${nomfic}
		    }
		} else {
		    if {[string compare $nomfic $FIC_PROBLEME] != 0} {
			CreerFenetreDialogue $LOGICIEL error "Le fichier $nomfic de l'annee $clim n'existe pas"
		    }
		}
	    }
	}
	CreerFenetreDialogue $LOGICIEL info "Fin sauvegarde sorties"
    }
}

 
