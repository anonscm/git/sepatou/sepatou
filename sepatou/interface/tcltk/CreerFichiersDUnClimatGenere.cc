// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : CreerFichiersDUnClimatGenere.cc
contenu : Genere 4 fichiers climatiques : Pluie.tmp, T.tmp, Rg.tmp, Etp.tmp
          du climat genere avec la localisation, la graine et l'annee donnee.
          Les annees generees commencent a 3000. (3002 est donc la 3eme annee generee)
******************************************************************************/

#include <fstream>
#include <iostream>
#include <string>
#include <math.h>
#include <stdlib.h>

using namespace std;

#define NB_JOURS 366

// pour communication climat genere
typedef struct meteo
        {
        float pluie;   /* en mm */
        float tmoy;    /* en C */
        float rayon;   /* en MJ/m2 */
	float etp;     /* en mm */
        } ST_METEO;
extern void InitGen(const char *ParamFile,const char *DatDir, unsigned int seed); 
extern void WeatherGen(double lat, ST_METEO *met);

#include "cRandom.h"
cRandom Random;

// ============================================================================
// Arrondir : renvoie le nb a 2 decimales
// ============================================================================
float Arrondir (float f)
{
  return ceil(f * pow(10.,2))/pow(10.,2);
}




// ============================================================================
int main (int argc, char* argv[])
{
  ST_METEO Meteo[NB_JOURS]; // zone pour recuperer le climat genere
  double Latitude;
  unsigned int Seed;
  int Annee;
  string Localisation, CheminParaStatClimat, NomParaStatClimat; 
  ofstream fPluie, fT, fRg, fEtp;

  if (argc != 4)
    cout << "ERREUR dans CreerFichiersDUnClimatGenere.cc : il faut 3 arguments : localisation, graine et annee generee (>3000)" << endl;
  else
    {
      Localisation = argv[1];
      Seed = atoi( argv[2] );
      Annee = atoi( argv[3] );

      if ((Localisation != "Segala")  && (Localisation != "Rennes"))
	cout << "ERREUR : Cette localisation n'est pas pr�vue" << endl;
      else
	{
	  if (Localisation == "Segala")
	    Latitude = 44.2;
	  else
	    if (Localisation == "Rennes")
	      Latitude = 48.1;
	    else
	      cout << "ERREUR dans CreerFichiersDUnClimatGenere.cc : la localisation n'est pas correcte" << endl; 
	  
	  // initialisation du generateur climatique
	  CheminParaStatClimat =  "../climats/" + Localisation ;
	  NomParaStatClimat = Localisation + ".sta";
	  
	  InitGen(NomParaStatClimat.c_str(), CheminParaStatClimat.c_str(), Seed);
	  
	  fPluie.open("Pluie.tmp",ios::out);
	  fT.open("T.tmp",ios::out);
	  fRg.open("Rg.tmp",ios::out);
	  fEtp.open("Etp.tmp",ios::out);
	  
	  for (int a=3000; a<=Annee; a++)
	    {
	      WeatherGen(Latitude, &Meteo[0]);
	      if (a == Annee)
		for (int j=1;j<NB_JOURS; j++)
		  {
		    fPluie << Arrondir(Meteo[j-1].pluie) << endl;
		    fT << Arrondir(Meteo[j-1].tmoy) << endl;
		    fRg << Arrondir(Meteo[j-1].rayon) << endl;
		    fEtp << Arrondir(Meteo[j-1].etp) << endl;
		  };
	    };
	  fPluie.close();
	  fT.close();
	  fRg.close();
	  fEtp.close();
	};
    };
}

