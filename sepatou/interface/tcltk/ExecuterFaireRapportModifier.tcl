# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

############################################################################
# Fichier : ExecuterFaireRapportModifier.tcl
# Contenu : Modification du rapport courant
############################################################################

############################################################################
# Procedure : FaireRapportModifier
# Description : cree la fenetre qui permet de modifier un rapport
# Parametres : aucun
############################################################################
proc FaireRapportModifier {} {

    global LOGICIEL
    global DIR_RAPPORTCOURANT
    global NOM_RAPPORT    
    
    #Creation de la fenetre 
    toplevel .wm_FaireRapportModifier
    wm title .wm_FaireRapportModifier "$LOGICIEL : Modification du rapport"
       
    ### Creation de la zone du bas contenant les boutons  ##################
    frame .wm_FaireRapportModifier.f_bottom
    
    #Creation des boutons de la frame du bas button 
    button .wm_FaireRapportModifier.f_bottom.b_Ok -text Ok \
	-command Ok_FaireRapportModifier   
    button .wm_FaireRapportModifier.f_bottom.b_Annuler -text Annuler \
	-command "destroy .wm_FaireRapportModifier" 
    
    #Placement des boutons et de la frame du bas  
    pack  .wm_FaireRapportModifier.f_bottom.b_Ok \
	.wm_FaireRapportModifier.f_bottom.b_Annuler \
	-padx 20 -pady 10 -side left -expand 1
    pack .wm_FaireRapportModifier.f_bottom -side bottom -anchor s -fill x \
	-pady 5
        
    #Creation de la ligne separatrice du bas
    frame .wm_FaireRapportModifier.f_sep -width 100 -height 2 -borderwidth 1 \
	-relief sunken
    #Placement de la ligne separatrice du bas
    pack .wm_FaireRapportModifier.f_sep -side bottom -fill x
    
    
    ### Creation de la zone d'affichage du rapport en latex  #################
    frame .wm_FaireRapportModifier.f_zone
    scrollbar .wm_FaireRapportModifier.f_zone.scrb_rapport \
	-command {.wm_FaireRapportModifier.f_zone.t_rapport yview}
    
    text .wm_FaireRapportModifier.f_zone.t_rapport \
	-yscrollcommand {.wm_FaireRapportModifier.f_zone.scrb_rapport set}
    
    #Placement de la zone d'affichage du rapport en latex
    pack .wm_FaireRapportModifier.f_zone.scrb_rapport -side right -fill y
    pack .wm_FaireRapportModifier.f_zone.t_rapport -side left -expand yes \
	-fill both
    pack .wm_FaireRapportModifier.f_zone -side top -expand yes -fill both \
	-padx 4 -pady 4
    
    
    ### Creation du menu accessible par le bouton droit #################
    menu .wm_FaireRapportModifier.f_zone.t_rapport.edit -tearoff 0
    
    .wm_FaireRapportModifier.f_zone.t_rapport.edit add command \
	-label "Couper" \
	-command {tk_textCut .wm_FaireRapportModifier.f_zone.t_rapport}
    
    .wm_FaireRapportModifier.f_zone.t_rapport.edit add command \
	-label "Copier" \
	-command {tk_textCopy .wm_FaireRapportModifier.f_zone.t_rapport}
    
    .wm_FaireRapportModifier.f_zone.t_rapport.edit add command \
	-label "Coller" \
	-command {tk_textPaste .wm_FaireRapportModifier.f_zone.t_rapport}
    
    bind .wm_FaireRapportModifier.f_zone.t_rapport <ButtonPress-3> {
	tk_popup .wm_FaireRapportModifier.f_zone.t_rapport.edit %X %Y
    }
    
    
    ### Edition du rapport dans la fenetre ###########################
    
	#Ouverture du fichier contenant le rapport en lecture 
	set f [open $DIR_RAPPORTCOURANT$NOM_RAPPORT.tex r]
	#Affectation du contenu du fichier a une variable
	set contents [read $f]
    
    #Fermeture de la lecture du fichier contenant le rapport
    close $f
    
    #Insertion du contenu de la variable dans la zone d'affichage
    .wm_FaireRapportModifier.f_zone.t_rapport configure -state normal 
    .wm_FaireRapportModifier.f_zone.t_rapport delete 1.0 end 
    .wm_FaireRapportModifier.f_zone.t_rapport insert end $contents "typewriter"
   
    #Cr�ation de 2 tag qui permettent de mettre en couleur les s�quences de 
    #d�but et de fin d'espace de commentaires
    #Initialaisation de la variable pos � la position de la premi�re 
    #occurence de %D�but
    set pos [.wm_FaireRapportModifier.f_zone.t_rapport search "%D�but" 0.0 end]
    #boucle pour aller jusqu'� la fin du fichier
    while {$pos != ""} {
	
	set pos_texte "$pos lineend"
	.wm_FaireRapportModifier.f_zone.t_rapport tag add tagdebut \
	    $pos $pos_texte
	.wm_FaireRapportModifier.f_zone.t_rapport tag configure tagdebut \
	    -foreground green
	set pos [.wm_FaireRapportModifier.f_zone.t_rapport search "%Fin" \
		     $pos_texte end]
	if {$pos == ""} {
	    puts "ERREUR : pas de %Fin"
	} else {
	    set pos_texte "$pos lineend"
	   .wm_FaireRapportModifier.f_zone.t_rapport tag add  tagfin \
		$pos $pos_texte 
	    .wm_FaireRapportModifier.f_zone.t_rapport tag configure tagfin \
		-foreground red
	}
	set pos [.wm_FaireRapportModifier.f_zone.t_rapport search "%D�but" \
		     $pos_texte end]
    }
    
    # Inhibition de l'acc�s aux autres fen�tres
    grab .wm_FaireRapportModifier
    tkwait window .wm_FaireRapportModifier
    grab release .wm_FaireRapportModifier
}


############################################################################
# Procedure : Ok_FaireRapportModifier
# Description : sauvegarde les modifications faites au rapport
# Parametres : aucun
############################################################################
proc Ok_FaireRapportModifier {} {
    
    global DIR_RAPPORTCOURANT
    global NOM_RAPPORT
    global CMD_MOVE
    global CMD_REMOVE

    set nouveau [.wm_FaireRapportModifier.f_zone.t_rapport get 1.0 end]

    # ecriture du rapport modifie 
    set f [open ${DIR_RAPPORTCOURANT}____tmp____.tex w]
    puts $f [.wm_FaireRapportModifier.f_zone.t_rapport get 1.0 end]
    close $f
    
    if {[CompilerRapportLatex $DIR_RAPPORTCOURANT ____tmp____]==0} {
	# on met a jour la nouvelle version
	exec $CMD_MOVE  ${DIR_RAPPORTCOURANT}____tmp____.tex ${DIR_RAPPORTCOURANT}${NOM_RAPPORT}.tex
	exec $CMD_MOVE  ${DIR_RAPPORTCOURANT}____tmp____.dvi ${DIR_RAPPORTCOURANT}${NOM_RAPPORT}.dvi
	exec $CMD_MOVE  ${DIR_RAPPORTCOURANT}____tmp____.log ${DIR_RAPPORTCOURANT}${NOM_RAPPORT}.log
	exec $CMD_MOVE  ${DIR_RAPPORTCOURANT}____tmp____.aux ${DIR_RAPPORTCOURANT}${NOM_RAPPORT}.aux
	exec $CMD_MOVE  ${DIR_RAPPORTCOURANT}____tmp____.toc ${DIR_RAPPORTCOURANT}${NOM_RAPPORT}.toc
    } else {
	exec $CMD_REMOVE  ${DIR_RAPPORTCOURANT}____tmp____.tex 
	exec $CMD_REMOVE  ${DIR_RAPPORTCOURANT}____tmp____.dvi 
	exec $CMD_REMOVE  ${DIR_RAPPORTCOURANT}____tmp____.log
	exec $CMD_REMOVE  ${DIR_RAPPORTCOURANT}____tmp____.aux 
	exec $CMD_REMOVE  ${DIR_RAPPORTCOURANT}____tmp____.toc
    }
    
    # fermeture des fenetres
    destroy .wm_FaireRapportModifier
    
}