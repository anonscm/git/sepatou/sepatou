# Copyright (C) 2005 Marie-Josée Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : GererSauvegardesConfigurations.tcl
# contenu : procedures permettant de gerer les configurations 
##################################################################

#----------------------------------------------------------------------------
# Creation de la fenetre de gestion des configurations
#----------------------------------------------------------------------------
proc GererSauvegardesConfigurations {} {

    global LOGICIEL
    global DIR_CONFIGURATIONS
    
    if {[winfo exists .wm_GererConfigurations] == 0} {
	toplevel .wm_GererConfigurations
	wm title .wm_GererConfigurations "$LOGICIEL : Gestion des configurations sauvegardees"
	wm geometry .wm_GererConfigurations 400x500	
	
	label .wm_GererConfigurations.l_configuration -text CONFIGURATIONS
	pack .wm_GererConfigurations.l_configuration -padx 10 -pady 10

	### Zone des boutons ################################################
	frame .wm_GererConfigurations.f_boutons
	button .wm_GererConfigurations.f_boutons.b_enlever -text Enlever \
	    -command "Enlever_GererConfigurations"
	button .wm_GererConfigurations.f_boutons.b_afficher -text Afficher \
	    -command "Afficher_GererConfigurations"
	pack .wm_GererConfigurations.f_boutons.b_enlever \
	    .wm_GererConfigurations.f_boutons.b_afficher -side left -expand 1 
	pack .wm_GererConfigurations.f_boutons -padx 10 -pady 10 -expand 1


	### Zone de selection ###############################################
	frame .wm_GererConfigurations.f_topw -borderwidth 10
	pack .wm_GererConfigurations.f_topw -padx 20 -pady 20 -side top -expand 1

	#Creation de la Scrolled List
	ScrolledListbox .wm_GererConfigurations.f_topw.scrlst_list \
	    -width 50 -height 15 
	### Chargement des configurations
	set list [recherche_rep $DIR_CONFIGURATIONS]
	set nb [llength $list]
	for  {set i 0} {$i < $nb} {incr i} {
	    set val [lindex $list $i]
	    .wm_GererConfigurations.f_topw.scrlst_list.lst_list insert end $val
	}
	pack .wm_GererConfigurations.f_topw.scrlst_list \
	    -padx 4 -side left -anchor n -expand 1
	
	
	###Zone de commandes du bouton fermer #############################
	
	frame .wm_GererConfigurations.f_bottom -borderwidth 3
	pack .wm_GererConfigurations.f_bottom -side bottom -fill x -pady 5
	button .wm_GererConfigurations.f_bottom.b_fermer -text Fermer \
	    -command "Fermer_GererConfigurations"
	pack .wm_GererConfigurations.f_bottom.b_fermer -side left -expand 1 
	### Separateur
	frame .wm_GererConfigurations.f_sep \
	    -width 100 -height 2 -borderwidth 1 -relief sunken
	pack .wm_GererConfigurations.f_sep -side bottom -fill x -pady 5
    }
}

#----------------------------------------------------------------------------
# fermeture des sous fentres
#----------------------------------------------------------------------------
proc Fermer_GererConfigurations {} {

    set list_fenetres [winfo children .]
    DetruireFenetresPrefixees .wm_GererConfigurations $list_fenetres
}

#----------------------------------------------------------------------------
# Enleve une configuration sauvegardée
#----------------------------------------------------------------------------
proc Enlever_GererConfigurations {} {

    global LOGICIEL
    global DIR_CONFIGURATIONS

    global CMD_REMOVE
    global OPT_RM_DIR


    set list .wm_GererConfigurations.f_topw.scrlst_list.lst_list    
    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue $LOGICIEL error "Sélectionnez une configuration"
    } else {
	set rep [$list get $i]
	set message "Voulez-vous vraiment enlever \n la configuration : $rep ?"
	if { [Confirmer $LOGICIEL $message] == 1} {
	    
	    # destruction du repertoire
	    exec $CMD_REMOVE $OPT_RM_DIR ${DIR_CONFIGURATIONS}$rep

	    # mise a jour de la liste des configurations
	    $list delete $i
	}
    }
}



#----------------------------------------------------------------------------
# Affiche une configuration sauvegardée
#----------------------------------------------------------------------------
proc Afficher_GererConfigurations {} {

    global LOGICIEL
    global DIR_CONFIGURATIONS

    set list .wm_GererConfigurations.f_topw.scrlst_list.lst_list    
    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue $LOGICIEL error "Sélectionnez une configuration"
    } else {
	set rep [$list get $i]

	source VisualiserConfiguration.tcl ;# pour VisualiserConfiguration
	VisualiserConfiguration  .wm_GererConfigurations$rep  ${DIR_CONFIGURATIONS}$rep/ $rep
    }
}


