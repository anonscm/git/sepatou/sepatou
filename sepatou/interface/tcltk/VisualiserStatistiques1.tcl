# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##############################################################
# Fichier : VisualiserStatistiques1.tcl 
# Contenu : Affichage de la fenetre de choix des statistiques
##############################################################

# chargement de la bibliotheque de fonctions C
global DIR_EXECUTION
load ${DIR_EXECUTION}libTcl_c.so

#-----------------------------------------------#
# Procedure qui se charge d'afficher la fenetre #
#  de choix des statistiques a visualiser.      #
#-----------------------------------------------#
proc VisualiserStatistiques1 {} {
    global LOGICIEL
    global DIR_STATISTIQUES

    global l_var_statistiques
    global x
    global y
    global z
    global tab_valeurs_max_min
    global num
    global grid

    # initialisation du numero de fenetre
    set num 0

    ###################### lecture des variables statistiques ################
    set l_var_statistiques {}	
    # Elimination des fichiers.ps de la liste
     foreach v [recherche_fic $DIR_STATISTIQUES] {	
	 if {[regexp .ps $v]==0} {
	     lappend l_var_statistiques $v
	 }
     }
    
   ########################## Creation de la fenetre ##########################
    # Ne pas afficher deux fois la meme fenetre
     catch {destroy .wm_VisuStatistiques1}
    # Creation d'une fenetre
     toplevel .wm_VisuStatistiques1
     wm title .wm_VisuStatistiques1 "$LOGICIEL: Visualisation de statistiques"

    # La fenetre est composee de 6 frames 
    #          _____________________
    #          |-------  |         |
    #          ||  2  | 1|         |
    #          |-------  |         |
    #          |-------  |         |
    #          ||  3  |  |     5   |
    #          |-------  |         | 
    #          |-------  |         |
    #          ||  4  |  |         |
    #          |-------  |         |
    #          ---------------------
    #          |         6         |
    #          ---------------------
    

    ### Remplissage du frame du bas (6) 
    frame .wm_VisuStatistiques1.f_down -borderwidth 2
    pack .wm_VisuStatistiques1.f_down -side bottom
    button .wm_VisuStatistiques1.f_down.b_ok -text {Afficher} \
	-command AfficherVisuStatistiques1 
    button .wm_VisuStatistiques1.f_down.b_annuler -text {Fermer} \
	-command FermerVisuStatistiques1
    pack  .wm_VisuStatistiques1.f_down.b_ok \
	.wm_VisuStatistiques1.f_down.b_annuler \
	-side left -expand 1 -padx 60 -pady 2
    # separation
    frame .wm_VisuStatistiques1.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken     
    pack .wm_VisuStatistiques1.f_sep -side bottom -fill x -pady 5
    
    ### Remplissage du frame de gauche (1)  
    frame .wm_VisuStatistiques1.f_topl -borderwidth 2 -relief raised  
    pack .wm_VisuStatistiques1.f_topl -side left -fill both 
    ## Premier tix option menu : variable x (frame 2)
    frame .wm_VisuStatistiques1.f_topl.f_varx -width 200
    label .wm_VisuStatistiques1.f_topl.f_varx.l_titrex \
	-text "Variable X :"
    tixOptionMenu .wm_VisuStatistiques1.f_topl.f_varx.t -variable x \
	-dynamicgeometry true
    #Insertion des variables
    foreach v $l_var_statistiques  { 
	.wm_VisuStatistiques1.f_topl.f_varx.t add command $v 
    } 
    ## Deuxieme tix option menu :variable y (frame 3)
    frame .wm_VisuStatistiques1.f_topl.f_vary
    label .wm_VisuStatistiques1.f_topl.f_vary.l_titrey \
	-text "Variable Y :"
    tixOptionMenu .wm_VisuStatistiques1.f_topl.f_vary.t -variable y \
	-dynamicgeometry true
    #Insertion des variables
    .wm_VisuStatistiques1.f_topl.f_vary.t add command aucune
     foreach v $l_var_statistiques { 
	 .wm_VisuStatistiques1.f_topl.f_vary.t add command $v
     }
    ##Troisieme tix option menu :variable z (frame 4)
    frame .wm_VisuStatistiques1.f_topl.f_varz
    label .wm_VisuStatistiques1.f_topl.f_varz.l_titrez \
	-text "Variable Z :"
    tixOptionMenu .wm_VisuStatistiques1.f_topl.f_varz.t -variable z \
	-dynamicgeometry true
    #Insertion des variables
    .wm_VisuStatistiques1.f_topl.f_varz.t add command aucune
     foreach v $l_var_statistiques  { 
	 .wm_VisuStatistiques1.f_topl.f_varz.t add command $v
     }
    #gestion des frames 
     pack .wm_VisuStatistiques1.f_topl.f_varx \
	.wm_VisuStatistiques1.f_topl.f_vary \
	.wm_VisuStatistiques1.f_topl.f_varz  -expand yes     
    #gestion des labels
     pack .wm_VisuStatistiques1.f_topl.f_varx.l_titrex \
	.wm_VisuStatistiques1.f_topl.f_vary.l_titrey \
	.wm_VisuStatistiques1.f_topl.f_varz.l_titrez -side left -padx 8
    #gestion des tix option menus:
     pack .wm_VisuStatistiques1.f_topl.f_varx.t \
	.wm_VisuStatistiques1.f_topl.f_vary.t \
	.wm_VisuStatistiques1.f_topl.f_varz.t \
	-side left -expand true -padx 8 -pady 4 -fill x
    ### Remplissage du frame de droite (5) 
    frame .wm_VisuStatistiques1.f_topr -borderwidth 2 -relief raised    
    pack .wm_VisuStatistiques1.f_topr -side left -fill both 
    Creer_titre_souligne .wm_VisuStatistiques1.f_topr.ts_titre \
	"Intervalle de  valeur:"
    #Creation de la grille 
     tixScrolledGrid .wm_VisuStatistiques1.f_topr.scrg_grille \
	-bd 0 -width 300 -height 420 
     pack .wm_VisuStatistiques1.f_topr.scrg_grille \
	-expand yes -padx 20 -pady 10
     set grid [.wm_VisuStatistiques1.f_topr.scrg_grille subwidget grid] 
     $grid config -formatcmd "EditGrid_format $grid"    
    #Definition de la taille des colonnes
    $grid size column 0 -size 15char
    $grid size column 1 -size 10char
    $grid size column 2 -size 10char   
    $grid size row default -size 1.1char 
    
    $grid set 0 0 -text "Variables"
    $grid set 2 0 -text "Maximum"
    $grid set 1 0 -text "Minimum"
    $grid config -editnotifycmd "Editable"
    # Remplissage de la grille
    set ligne 1
    foreach v $l_var_statistiques {
	set don [LireFichierStatistique $v]
	set l [list]
	
	for {set i 0} {$i<[llength $don]} {incr i} {
	    lappend l [lindex $don $i]
	}
	set vmax [Max $l]
	set vmin [Min $l]
	# Enregistrement des valeurs sous forme d'un tableau
	set tab_valeurs_max_min($ligne,1) $vmin
	set tab_valeurs_max_min($ligne,2) $vmax
	# remplissage des cellules 
	$grid set 0 $ligne -text $v
	$grid set 1 $ligne -text $vmin
	$grid set 2 $ligne -text $vmax   
	incr ligne
    } 
    $grid config -editdonecmd "EnregistrementIntervalleDeValeurs"
}


#------------------------------------------------#
# Procedure qui donne les valeurs contenues dans #
# un fichier sous forme de liste.                #
# Parametre:                                     #
#           f: fichier                           #
#------------------------------------------------#
proc LireFichierStatistique {f} {

    global DIR_STATISTIQUES

    set contents ""
    if [file exist $DIR_STATISTIQUES$f] {
	set fileid [open $DIR_STATISTIQUES$f]
	set contents [read $fileid]
	close $fileid
    }
    return $contents     
}



#--------------------------------------------------------------------------#
# Enregistrement des elements de la grille
#          parametres
#                    x,y: les cordonnees de la cellule a enregistrer
#--------------------------------------------------------------------------#
proc EnregistrementIntervalleDeValeurs {x y} {

    global LOGICIEL
    global grid
    global tab_valeurs_max_min

    set entree [$grid entrycget $x $y -text]
    set entr1 [$grid entrycget 1 $y -text]
    set entr2 [$grid entrycget 2 $y -text]
    if {$x==1&&$entree<=$entr2||$x==2&&$entr1<=$entree} { 
    set tab_valeurs_max_min($y,$x) [$grid entrycget $x $y -text]
    } else {
	CreerFenetreDialogue $LOGICIEL error \
	    "Attention : Min > Max ou Max < Min"
    }
}

#----------------------------------------------#
# fermeture des sous fenetres
#----------------------------------------------#
proc FermerVisuStatistiques1 {} {

     set list_fenetres [winfo children .]
     DetruireFenetresPrefixees .wm_VisuStatistiques1 $list_fenetres
}

#----------------------------------------------#
# Procedure qui se charge de l' affichage      #
# des fenetres de visualisation.               #
#----------------------------------------------#
proc AfficherVisuStatistiques1 {} {

    global LOGICIEL
    global x
    global y
    global z

    #Affichage d'erreur 
    if {$y=="aucune"} {
	if {$z=="aucune"} {
	    source VisualiserStatistiques1Variable1.tcl	 
	    VisualiserStatistiques1Variable1 $x
	} else {
	    CreerFenetreDialogue $LOGICIEL error \
		"Une variable Z ne peut pas etre pr�cis�e sans avoir une variable Y egalement precis�e."
	} 
    } else {
	if {$z=="aucune"} {
	    source VisualiserStatistiques1Variable2.tcl	
	    VisualiserStatistiques1Variable2  $x $y
	} else {
	    CreerFenetreDialogue $LOGICIEL info \
		"La visualisation pour trois variables n'est pas encore disponible"
	    #	 source VisualiserStatistiques1Variable3.tcl
	    #	 VisualiserStatistiques1Variable3  $x $y $z
	}
    }
}



#######################################################################
# procedures utilisees pour la visualisation de une ou plusieurs variables
#######################################################################

#-------------------------------------------------------------------#
# affichage de la boite a moustache
# parametres:
#            g: le graphe
#            place ; la facon dont la boite est placee
#          x0 x1 x2 x3 x4: les abscisses de la boites
# correspondant � min, 1er quartile, mediane, 3eme quartile et max
#          args: les valeurs exceptionnelles
#---------------------------------------------------------------------#
proc AfficherBoiteAMoustache {g place x0 x1 x2 x3 x4 val_except} {
    # calcul des coordonnees de la bande mediane
    set i [expr [expr $x4-$x0]/100]
    set med1 [expr $x2-$i]
    set med2 [expr $x2+$i]
    set iqr [expr $x3-$x1]
    if {$place=="h"} {
	#creation de la boite horizontale
	$g marker create polygon -name  mediane_poly1 -linewidth 1 \
	    -fill forestgreen -coords {$x1 10 $x3 10 $x3 20  $x1 20}
	$g marker create polygon -name mediane_poly2 -linewidth 0.5 \
	    -fill darkgreen -coords {$med1 10 $med2 10 $med2 20 $med1 20}	
	#mise en place des valeurs exceptionnelles
	if {$val_except!=""} {
	    set j 0
	    if {$x0<[expr $x1-1.5*$iqr]} {
		set x0 [expr $x1-1.5*$iqr]
		foreach v $val_except {
		    incr j
		    if {$v<$x0} {
			$g marker create line \
			    -name "point$j"  -coords {$v 10 $v 20}
			
		    } 
		}
	    } 
	     if {$x1>[expr $x3+1.5*$iqr]} {
		set x1 [expr $x3+1.5*$iqr]
		foreach v $val_except {
		    incr j
		    if {$val_except>$x1} {
			$g marker create line \
			    -name "point$j" \
			    -coords {$v 15 $v 15} 
		    }
		}
	    } 
	}
	#mise en place des moustaches
	$g marker create line -name mediane_ligne1 -linewidth 1 \
	    -coords {$x0 15 $x1 15}
	$g marker create line -name  mediane_ligne2 -linewidth 1 \
	    -coords  {$x3 15 $x4 15}
    } elseif {$place=="v"} {
	#creation de la boite verticale
	$g marker create polygon -name  mediane_poly1 -linewidth 1 \
	    -fill forestgreen -coords {10 $x1 10 $x3 20 $x3 20 $x1}
	$g marker create polygon -name mediane_poly2 -linewidth 0.5 \
	    -fill darkgreen -coords {10 $med1 10 $med2 20 $med2 20 $med1}
	#mise en place des valeurs exceptionnelles
	if {$val_except!=""} {
	    set j 0
	    if {$x0<[expr $x1-1.5*$iqr]} {
		set x0 [expr $x1-1.5*$iqr]
		foreach v $val_except {
		    incr j
		    if {$v<$x0} {
			$g marker create line \
			    -name "point$j" \
			    -coords { 10 $v 20 $v}
		    }
		}
	    } 
	     if {$x1>[expr $x3+1.5*$iqr]} {
		 incr j
		 set x1 [expr $x3+1.5*$iqr]
		 foreach v $val_except {
		     if {$v>$x1} {
			 $g marker create line \
			     -name "point$j" \
			     -coords { 10 $v 20 $v}
		    }
		}
	    } 
	}
	# mise en place des moustaches
	$g marker create line -name  mediane_ligne2 -linewidth 1 \
	    -coords  {15 $x3 15 $x4}
	$g marker create line -name mediane_ligne1 -linewidth 1 \
	    -coords {15 $x0 15 $x1}
    } else {
	error}
}



#------------------------------------------------#
# destruction de la boite a moustache
# parametres:
#            g: le graphique 
#            
#------------------------------------------------#
proc DetruireBoiteAMoustache {g} {

    foreach n [$g marker names] {  
    $g marker delete $n
    }
  
}

#------------------------------------------------------------#
# Procedure qui donne le pas de discretisation  
# courant.   
#            parametre:
#                     var: la variable dont on donne le pas
#                    max min: les maximum et minimum de la variable           
#------------------------------------------------------------#
proc PasCourant {var max min} {
 
    set p [expr ( $max - $min ) / 5.]
    return $p  
}
#-----------------------------------------------#
# Procedure qui donne le pas de discretisation  
# aggrandit.  
#            parametre:
#                    p: l'ancien pas                                    
#-----------------------------------------------#
proc PasAgrandit {p} {    
   
    set p  [expr $p * 2.] 
    return $p
}

#-----------------------------------------------#
# Procedure qui donne le pas de discretisation  #
# reduit. 
#           parametre:
#                    p: l'ancien pas                                        
#-----------------------------------------------#
proc PasReduit {p} {  

    set p [expr $p / 2.] 
    return $p
}


