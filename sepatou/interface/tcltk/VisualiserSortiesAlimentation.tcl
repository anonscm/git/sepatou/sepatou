# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#########################################################
# Fichier : VisualiserSortiesAlimentation.tcl  
# Contenu : Affichage de la fenetre d'alimentation du 
#           troupeau selon l'annee climatique courante.
#########################################################

global color
set color(0) green
set color(1) blue
set color(2) red
set color(3) orange
set color(4) black
set color(5) brown


#------------------------------------------------#
# Procedure qui se charge de creer la fenetre de #
#  visualisation de l'alimentation du troupeau,  #
#  pour l'anne climatique souhaitee.             #
#------------------------------------------------#
# Parametres :                                   #
#    fen_alim : fenetre correspondant a          #
#      l'alimentation voulue,                    #
#    annee : annee climatique desiree.           #
#------------------------------------------------#
proc Afficher_alimentation {fen_alim annee affichage} {

 global LOGICIEL
 global DIR_TEMPORAIRE
 global NOM_STRATEGIE
 global FIC_CHRONIQUE
 global FIC_DATES_CLEFS


 if {[file exist $DIR_TEMPORAIRE$NOM_STRATEGIE/$annee/$FIC_CHRONIQUE ] \
	 && [file exist $DIR_TEMPORAIRE$NOM_STRATEGIE/$annee/$FIC_DATES_CLEFS]} {
     # Creation d'une fenetre
      catch {destroy $fen_alim}
     toplevel $fen_alim
     wm title $fen_alim "$LOGICIEL : Visualisation de l'alimentation du troupeau $annee"

      if {$affichage == 0} {
	  wm iconify $fen_alim
      }


      # La fenetre est composee de trois frames
      #  validation en bas, graphe a gauche et camembert a droite.

      # Frame du bas : Validation : Imprimer et Fermer
      frame $fen_alim.f_valid -borderwidth 2
      pack $fen_alim.f_valid -side bottom
      button $fen_alim.f_valid.b_imprimer -text Imprimer \
         -command "Imprimer_alimentation $fen_alim $annee 0"
      button $fen_alim.f_valid.b_fermer -text Fermer \
         -command "destroy $fen_alim"
      pack $fen_alim.f_valid.b_fermer $fen_alim.f_valid.b_imprimer \
         -side right -expand true -padx 60

      # Affichage des dates clefs
      set fic_clefs [open $DIR_TEMPORAIRE$NOM_STRATEGIE/$annee/$FIC_DATES_CLEFS r]
      gets $fic_clefs zdateMH
      if {$zdateMH == -1} {set zdateMH "-"}
      gets $fic_clefs zdateFin1erCycle
      if {$zdateFin1erCycle == -1} {set zdateFin1erCycle "-"}
      gets $fic_clefs zdateSortieNuit
      if {$zdateSortieNuit == -1} {set zdateSortieNuit "-"}
      gets $fic_clefs zdateFermetureSilo
      if {$zdateFermetureSilo == -1} {set zdateFermetureSilo "-"}
      gets $fic_clefs zdateOuvertureSilo
      if {$zdateOuvertureSilo == -1} {set zdateOuvertureSilo "-"}
      gets $fic_clefs zdateEnsilage
      if {$zdateEnsilage == -1} {set zdateEnsilage "-"}
      gets $fic_clefs zdate1ereFauche
      if {$zdate1ereFauche == -1} {set zdate1ereFauche "-"}
      gets $fic_clefs zQcoupee
      gets $fic_clefs zScoupee
      close $fic_clefs
      
      frame $fen_alim.f_DatesClefs 
      pack $fen_alim.f_DatesClefs -side bottom -expand yes -fill x
      message $fen_alim.f_DatesClefs.m_dates -aspect 2000 -text "Mise � l'herbe : $zdateMH     Fin 1er cycle : $zdateFin1erCycle     Sortie nuit : $zdateSortieNuit     Fermeture silo : $zdateFermetureSilo     Ouverture silo : $zdateOuvertureSilo     Ensilage : $zdateEnsilage     1�re fauche : $zdate1ereFauche
"
      pack $fen_alim.f_DatesClefs.m_dates 

      # Frame de gauche : graphe
      frame $fen_alim.f_graphe -width 650 -height 550 \
         -borderwidth 2 -relief raised -background gray90

      # Frame de droite : camembert
      frame $fen_alim.f_cam  -borderwidth 2 -relief raised \
	 -width 250 -height 550 -background gray95
      pack $fen_alim.f_graphe $fen_alim.f_cam -side left -fill both

      # Remplissage des frames graphe et camembert
      Afficher_graphique_alimentation $fen_alim.f_graphe $annee
      Afficher_camembert_alimentation $fen_alim.f_cam $zQcoupee $zScoupee

  } else { 
      CreerFenetreDialogue $LOGICIEL error "Erreur d'ouverture du
       fichier $DIR_TEMPORAIRE$NOM_STRATEGIE/$annee/$FIC_CHRONIQUE"
  }
}



#--------------------------------------------------------#
# Procedure qui s'occupe d'afficher les informations     #
#  concernant l'alimentation des vaches sur un graphique.#
#--------------------------------------------------------#
# Parametre :                                            #
#   w : parent, ici c'est le cadre contenant le graphe,  #
#   climat : annee climatique prise en compte.           #
#--------------------------------------------------------#
proc Afficher_graphique_alimentation {w climat} {

global ACTIONS_ALIMENTATION
global DIR_TEMPORAIRE
global NOM_STRATEGIE
global FIC_CHRONIQUE

global color

# lecture des donnees #######################################################
  # Ouverture du fichier chronique en lecture :
  set fileid [open [glob $DIR_TEMPORAIRE$NOM_STRATEGIE/$climat/$FIC_CHRONIQUE] r]

  # Positionnement en debut de fichier
  seek $fileid 0 start
  set i 1
  set valmaxi 0
  set valmini 0

  # initialisation des variables locales
  set nbalim [llength $ACTIONS_ALIMENTATION]
  for { set z 0 } {$z < $nbalim} { incr z 1 } {
     set action [lindex $ACTIONS_ALIMENTATION $z]
     set existe_$action 0
     set val$z 0
     global total$z
     set total$z 0
  }

  # lecture du 1er numero de jour
  gets $fileid line
  set i [string range $line 3 end]
  lappend x $i

while { [gets $fileid line] >= 0 } {
    if { [string range $line 0 0] == "*"} {
       # lecture d'un numero de jour
       set i [string range $line 3 end]
       lappend x $i

	for { set z 0 } {$z < $nbalim} { incr z 1 } {
	    set action [lindex $ACTIONS_ALIMENTATION $z]
            if { [set existe_$action] == 0 } {
                 set val$z 0
	    }
	}

        for { set z 0 } {$z < $nbalim} { incr z 1 } {
            set somme 0
            for { set y 0 } {$y <= $z} { incr y 1 } {
		set somme [expr $somme + [set val$y]]
	    }
            lappend y$z $somme
	    if {$somme > $valmaxi} {
              set valmaxi $somme
            }
            if {$somme < $valmini} {
              set valmini $somme
            }
	}

	for { set z 0 } {$z < $nbalim} { incr z 1 } {
	   set action [lindex $ACTIONS_ALIMENTATION $z]
           set existe_$action 0
        }

    } else {
	# lecture d'une action
        set debut [Enieme_mot_de_ligne $line 0]
	if { [Est_nom_actions_possibles $debut] == 1 } {
          set existe_$debut 1
	  set no_action [lsearch $ACTIONS_ALIMENTATION $debut]
          set val$no_action [Enieme_mot_de_ligne $line 1]
          set total$no_action [expr [set total$no_action] + [set val$no_action]]
	}
    }
}
 close $fileid

# affichage #################################################################
graph $w.g_repartition  -width 600 -height 500 \
    -background gray90 -plotbackground gray95 \
   -title "Repartition journaliere de l'alimentation par vache (Kg/jour)"
 pack $w.g_repartition

 $w.g_repartition legend configure -background gray95 

 $w.g_repartition axis configure "x" -min [lindex $x 0] -max [expr $i + 0.01]
 $w.g_repartition axis configure "y" -min $valmini -max [expr $valmaxi + 0.01]

 set one [lindex $x 0]
 set first [list $one $valmini]
 set last [list $i $valmini]

 for { set j [expr $nbalim - 1] } { $j >= 0 } { set j [expr $j - 1] } {
   $w.g_repartition element create "line$j" -xdata $x -ydata [set y$j] -linewidth 0 \
     -label [lindex $ACTIONS_ALIMENTATION $j] -pixels 0.2m -color $color($j)
   set data [$w.g_repartition element cget line$j -data]

   set coords [concat $first $data $last $first]
   $w.g_repartition marker create polygon -name "plein$j" -under true \
     -coords $coords -fill gray83
   $w.g_repartition marker create polygon -name "plein$j" -under true \
     -coords $coords -fill $color($j)
#$w.g_repartition marker create line -name "line$j" -coords $data -linewidth 2
 }

 Blt_ZoomStack $w.g_repartition
 Blt_Crosshairs $w.g_repartition
}



#--------------------------------------------------------#
# Procedure qui s'occupe d'afficher les informations     #
#  concernant l'alimentation des vaches sur un camembert.#
#--------------------------------------------------------#
# Parametre :                                            #
#  w : parent, ici c'est le cadre contenant le camembert.#
#--------------------------------------------------------#
proc Afficher_camembert_alimentation {w zQcoupee zScoupee} {

 global ACTIONS_ALIMENTATION

 global color

 # Initialisation du nombre d'actions Alimentation
 set nbalim [llength $ACTIONS_ALIMENTATION]
 set total 0
 for { set z 0 } {$z < $nbalim} { incr z 1 } {
    global total$z
     set total [expr $total + [set total$z]]
 }
 # Calcul des pourcentages respectifs de chaque action
 for { set z 0 } {$z <  [expr $nbalim - 1]} { incr z 1 } {
     set part$z [expr  ( [set total$z] / $total ) * 100]
     set part$z [ArrondirA1Decimale [set part$z] ]
 }
 set totalpart 0
 for { set z 0 } {$z <  [expr $nbalim - 1]} { incr z 1 } {
     set totalpart [expr $totalpart + [set part$z]]
 }
 set part$z [expr 100 - $totalpart]

 # Calcul des angles respectifs
 for { set z 0 } {$z <  [expr $nbalim - 1]} { incr z 1 } {
     set angle$z [expr [expr 360 * [set part$z]] / 100]
 }

 set totalangle 0
 for { set z 0 } {$z <  [expr $nbalim - 1]} { incr z 1 } {
     set totalangle [expr $totalangle + [set angle$z]]
 }
 set angle$z [expr 360 - $totalangle]

 # Creation du cadre contenant le camembert
 canvas $w.cv_repartition -height 300 -width 300 -background gray95 
 pack $w.cv_repartition -side top -fill both
 # Titre du camembert
 message $w.msg_titre -aspect 1000 -justify center -pady 15 \
      -background gray95 -text \
  "Repartition globale de l'alimentation
   sur la duree de simulation

Quantit� fauch�e : $zQcoupee Kg
Surface fauch�e : $zScoupee ha"
 pack $w.msg_titre -side bottom
 # Creation du camembert par ajouts successifs d'arcs
 set debut 0
 for { set z 0 } {$z < $nbalim } { incr z 1 } {
     $w.cv_repartition create arc 60 60 240 240 -start $debut \
         -extent [set angle$z] -width 2 -fill $color($z)
     set debut [expr $debut + [set angle$z]]
 }
 # Creation de la legende
 for { set z 0 } {$z < $nbalim } { incr z 1 } {
    message $w.msg_part$z -aspect 1000 -justify center  -background gray95\
           -pady 5 -foreground $color($z) \
           -text "[lindex $ACTIONS_ALIMENTATION $z] : [set part$z] %"
    pack $w.msg_part$z -side bottom -pady 2
 }
}


#-------------------------------------------------------#
# Procedure qui verifie si un elements appartients a la #
#  liste des noms des actions possibles.                #
#-------------------------------------------------------#
# Parametre :                                           #
#   elt : element en entree,                            #
# En sortie : reponse (O = non, 1 =oui).                #
#-------------------------------------------------------#
proc Est_nom_actions_possibles { elt } {
global ACTIONS_ALIMENTATION
 set nb_alim [llength $ACTIONS_ALIMENTATION] 
 set reponse 0
    for {set i 0} {$i < $nb_alim} {incr i 1} {
	if { [string compare $elt [lindex $ACTIONS_ALIMENTATION $i]] == 0} {
         set reponse 1
      }
    }
 return $reponse
}


#-----------------------------------------------#
# impression de l'alimentation.  
#-----------------------------------------------#
proc Imprimer_alimentation { w annee rapport} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global DIR_RAPPORTCOURANT
    global CMD_REMOVE
    global CMD_IMPRIMER
    global OPT_IMPRIMER
    

    if {$rapport == 0} {
	# repartition
	$w.f_graphe.g_repartition postscript output ${DIR_TEMPORAIRE}graphique$w.ps
	catch {exec $CMD_IMPRIMER $OPT_IMPRIMER ${DIR_TEMPORAIRE}graphique$w.ps}
	exec $CMD_REMOVE ${DIR_TEMPORAIRE}graphique$w.ps
	
	# camenbert
	catch {exec $CMD_IMPRIMER $OPT_IMPRIMER << [$w.f_cam.cv_repartition postscript]}
	
	CreerFenetreDialogue $LOGICIEL info "Impression en cours"
    } else {
	#Cr�ation du postscript correspondant au graphique
	$w.f_graphe.g_repartition postscript output \
	    ${DIR_RAPPORTCOURANT}alimgraphique$annee.ps
	#Cr�ation du postscript correspondant au camenbert avec sa l�gende
	# non insere car demande a ne pas afficher la fenetre en icone
#	$w.f_cam.cv_repartition postscript -file \
#	    ${DIR_RAPPORTCOURANT}alimcamembert$annee.ps
    }
}


