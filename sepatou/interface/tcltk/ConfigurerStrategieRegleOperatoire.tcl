# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerStrategieRegleOperatoire.tcl 
# Contenu : Description d'une regle operatoire
######################################################################

# Une regle operatoire est une variable de nom : Regle_Op<i>N<n>
# avec <i> numero de l'activite et <n> numero de la regle pour l'activite <i>
# avec les attributs suivants :
#   (nom_reg_op) : nom
#   (si_reg_op) : SI
#   (alors_reg_op) : ALORS
#   (sinon_reg_op) : SINON
#   (commentaires_reg_op) : commentaires
# Pour chaque activite, la liste des regles operatoires est dans 
# la variable list_Regle_Op<i>


 
######################################################################
#Procedure   : Creer_Description_Regle_Operatoire                  
#Description : Creation de la fenetre chargee de decrire une regle 
#              operatoire                                          
#Parametre   : Struct_Regle_Op : Structure contenant les informations
#                                d'une regle operatoire                      
# (nom_reg_op,si_reg_op,alors_reg_op,sinon_reg_op, commentaires_reg_op) 
#              Act : Activite correspondant a la regle operatoire   
#              mode_visu : 'ecriture' 'lecture'
#              type_insertion : type d'insertion dans la liste des fo 
#                               'ajouter' 'editer'
#              mode_visu : 'ecriture' 'lecture'
#              type_insertion : type d'insertion dans la liste des fo 
#                               'ajouter' 'editer'
#              i : position du curseur si edition
######################################################################
proc Creer_Description_Regle_Operatoire {dir Struct_Regle_Op Act mode_visu type_insertion i} {

global LOGICIEL

#Variables communes avec celles du fichier Entree_Exploitation
#Declaration du nom de la structure pour pouvoir l'editer
global $Struct_Regle_Op

### Creation de la fenetre
set ws .wm_RO_$Struct_Regle_Op

if {[winfo exists $ws] == 0} {
    toplevel $ws

    #Titre de la fenetre
    wm title $ws "$LOGICIEL : Description d'une regle operatoire"

###############################################################################
    ### Zone de saisie

    frame $ws.f_top -borderwidth 3

    #Creation des libelles
    label $ws.f_top.l_nom -text Nom 
    label $ws.f_top.l_si -text SI
    label $ws.f_top.l_alors -text "\n ALORS"
    label $ws.f_top.l_sinon -text "\n SINON"
    label $ws.f_top.l_com -text "\n Commentaires"


    # nom
    entry $ws.f_top.e_nom -width 60 -relief sunken 
    $ws.f_top.e_nom delete 0 end

    # si
    frame $ws.f_top.f_si
    text $ws.f_top.f_si.txt_si \
	-width 60 -height 5 -background white \
	-yscrollcommand "$ws.f_top.f_si.scrb_si set"
    $ws.f_top.f_si.txt_si delete 1.0 end
    scrollbar $ws.f_top.f_si.scrb_si -orient vertical \
	-command "$ws.f_top.f_si.txt_si yview"

    # alors
    frame $ws.f_top.f_alors
    text $ws.f_top.f_alors.txt_alors \
	-width 60 -height 7 -background white \
  	-xscrollcommand "$ws.f_top.f_alors.scrb_alors set"
    $ws.f_top.f_alors.txt_alors delete 1.0 end
    scrollbar $ws.f_top.f_alors.scrb_alors -orient vertical \
	-command "$ws.f_top.f_alors.txt_alors yview"

    # sinon
    frame $ws.f_top.f_sinon
    text $ws.f_top.f_sinon.txt_sinon \
	-width 60 -height 7 -background white \
  	-xscrollcommand "$ws.f_top.f_sinon.scrb_sinon set"
    $ws.f_top.f_sinon.txt_sinon delete 1.0 end
    scrollbar $ws.f_top.f_sinon.scrb_sinon -orient vertical \
	-command "$ws.f_top.f_sinon.txt_sinon yview"

    #commentaires
    frame $ws.f_top.f_com
    text $ws.f_top.f_com.txt_com \
	-width 60 -height 5 -background white \
  	-xscrollcommand "$ws.f_top.f_com.scrb_com set"
    $ws.f_top.f_com.txt_com delete 1.0 end
    scrollbar $ws.f_top.f_com.scrb_com -orient vertical \
	-command "$ws.f_top.f_com.txt_com yview"

 
   # lecture des textes, si on edite une regle
    if {$type_insertion == "editer"} {
	set n [set ${Struct_Regle_Op}(nom_reg_op)]
	$ws.f_top.e_nom insert end $n
	$ws.f_top.f_si.txt_si insert end \
	    [set ${Struct_Regle_Op}(si_reg_op)]
	$ws.f_top.f_alors.txt_alors insert end \
	    [set ${Struct_Regle_Op}(alors_reg_op)]
	$ws.f_top.f_sinon.txt_sinon insert end \
	    [set ${Struct_Regle_Op}(sinon_reg_op)]
	$ws.f_top.f_com.txt_com insert end \
	    [set ${Struct_Regle_Op}(commentaires_reg_op)]
    }

    # affichage
    pack $ws.f_top -side top -fill both -expand 1

    pack $ws.f_top.l_nom -side top
    pack $ws.f_top.e_nom -pady 10 -side top -fill x


    pack $ws.f_top.l_si -side top
    pack $ws.f_top.f_si.scrb_si -side right -fill y
    pack $ws.f_top.f_si.txt_si -side top -fill both -expand 1
    pack $ws.f_top.f_si -side top  -fill both -expand 1

    pack $ws.f_top.l_alors -side top
    pack $ws.f_top.f_alors.scrb_alors -side right -fill y
    pack $ws.f_top.f_alors.txt_alors -side top -fill both -expand 1
    pack $ws.f_top.f_alors -side top  -fill both -expand 1

    pack $ws.f_top.l_sinon -side top
    pack $ws.f_top.f_sinon.scrb_sinon -side right -fill y
    pack $ws.f_top.f_sinon.txt_sinon -side top -fill both -expand 1
    pack $ws.f_top.f_sinon -side top  -fill both -expand 1

    pack $ws.f_top.l_com -side top
    pack $ws.f_top.f_com.scrb_com -side right -fill y
    pack $ws.f_top.f_com.txt_com -side top -fill both -expand 1
    pack $ws.f_top.f_com -side top  -fill both -expand 1


###############################################################################
    ### Zone de commandes des boutons ok, annuler et imprimer

    frame $ws.f_bottom -borderwidth 3
    pack $ws.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button $ws.f_bottom.b_ok -text Ok \
	-command "Ok_Regle_Operatoire $Struct_Regle_Op $i $ws $Act $type_insertion" 
    if {[string compare "lecture" $mode_visu] == 0} {
	$ws.f_bottom.b_ok configure -state disabled
    }
    button $ws.f_bottom.b_cancel \
	-text Annuler -command "destroy $ws"
    button $ws.f_bottom.b_imp -text Imprimer\
	-command "Imprimer_Regle_Operatoire $dir $ws"

    pack $ws.f_bottom.b_ok $ws.f_bottom.b_cancel $ws.f_bottom.b_imp \
	-side left -expand 1 

    ### Separateur
    frame $ws.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack $ws.f_sep -side bottom -fill x -pady 5

}
}

######################################################################
#Procedure : Ok_Regle_Operatoire                                     #
#Description : Selon le type choisi, met a jour les informations sur #
#              une regle operatoire ou ajoute une nouvelle           #
#              regle operatoire                                      #
#Parametre : struct : Structure de la regle operatoire               #
#            ind : Indice indiquant la position de l'element a       #
#                  mettre a jour (cas edition uniquement)            #
#            win : nom de la fenetre a detruire apres traitement     #
#            activite : activite de la regle                         # 
#            type : type d'insertion 'ajouter' 'editer'
######################################################################
proc Ok_Regle_Operatoire {struct ind win activite type} {

    global LOGICIEL

    #Variables communes avec celles du fichier Entree_Exploitation
    global ind_Act
    global list_Regle_Op$ind_Act
    #Declaration du contenu de la variable
    global $struct
    global $activite

    set nom_reg_op [$win.f_top.e_nom get]
    # Verification des donnees
    if {[PbControleChaine $nom_reg_op nom]} {
	CreerFenetreDialogue $LOGICIEL error "Nom de r�gle op�ratoire invalide"
    } elseif {[string length [$win.f_top.f_si.txt_si get 1.0 end]] == 1} {
	CreerFenetreDialogue $LOGICIEL error\
	    "Partie SI de la r�gle op�ratoire vide"
    } elseif {[string length [$win.f_top.f_alors.txt_alors get 1.0 end]] == 1} {
	CreerFenetreDialogue $LOGICIEL error\
	    "Partie ALORS de la r�gle op�ratoire vide"
    } else {

	#Memorisation de la liste cree dans le fichier Entree_Strategie.tcl
	set parent .wm_Strategie.f_midleft2.scrlst_bot.lst_list

	#Enregistrement du text
	set ${struct}(nom_reg_op) $nom_reg_op
	set ${struct}(si_reg_op) \
		[EpurerText [$win.f_top.f_si.txt_si get 1.0 end]]
	set ${struct}(alors_reg_op) \
		[EpurerText [$win.f_top.f_alors.txt_alors get 1.0 end]]
	set ${struct}(sinon_reg_op) \
		[EpurerText [$win.f_top.f_sinon.txt_sinon get 1.0 end]]
	set ${struct}(commentaires_reg_op) \
		[EpurerText [$win.f_top.f_com.txt_com get 1.0 end]]

	if {$type == "editer"} {
	    if {[string compare $activite list_Regle_Op$ind_Act] == 0} {
		$parent delete $ind
		$parent insert $ind $nom_reg_op
	    }
	} else {
	    ### Cas d'Ajout

	    if {[string compare $activite list_Regle_Op$ind_Act] == 0} {
		$parent insert end $nom_reg_op
	    } else {
		set ind [string range $activite 13 end]
	    }

	    #Insertion dans la liste en memoire
	    if [info exist $activite] {
		#Cas ou la liste existe deja
		set $activite [linsert [set $activite]\
					       end $struct]
	    } else {
		#Cas ou le premier element est insere
		set $activite [list $struct]
	    }
	}
	destroy $win
    }
}

######################################################################
#Procedure : Imprimer_Regle_Operatoire
#Description : Imprime les donnees saisies par l'utilisateur         #
#Parametre : dir : dir tmp
#            win : nom de la fenetre appelante                       #
######################################################################
proc Imprimer_Regle_Operatoire  {dir win} {
    global LOGICIEL
    global SEPARATEUR_LNU
    global CMD_REMOVE
    global CMD_A2PS

    set file [open ${dir}RegleOperatoire.imp w]

    puts $file $SEPARATEUR_LNU
    puts $file "# [EpurerText [$win.f_top.f_com.txt_com get 1.0 end]] #"
    puts $file $SEPARATEUR_LNU
    puts $file "REGLE OPERATOIRE : [$win.f_top.e_nom get]"
    puts $file $SEPARATEUR_LNU
    puts $file "SI "
    puts $file [EpurerText [$win.f_top.f_si.txt_si get 1.0 end]]
    puts $file $SEPARATEUR_LNU
    puts $file "ALORS "
    puts $file [EpurerText [$win.f_top.f_alors.txt_alors get 1.0 end]]
    puts $file $SEPARATEUR_LNU
    set sinon [EpurerText [$win.f_top.f_sinon.txt_sinon get 1.0 end]]
    if {[string length $sinon] != 0} {
	puts $file "SINON "
	puts $file $sinon
    }
    puts $file $SEPARATEUR_LNU

    close $file

    catch {exec $CMD_A2PS ${dir}RegleOperatoire.imp}
    exec $CMD_REMOVE ${dir}RegleOperatoire.imp

    CreerFenetreDialogue $LOGICIEL info "Impression en cours"
}

