# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#########################################################
# Fichier : VisualiserSortiesResume.tcl 
# Contenu : Affichage d'une fenetre presentant un resume 
#           des simulations realisees
#########################################################



#------------------------------------------------#
# Affichage de la fenetre
#------------------------------------------------#
proc Afficher_resume {w liste_annees dir_strategie} {

    global LOGICIEL

    if {[llength $liste_annees] == 0} {
      CreerFenetreDialogue $LOGICIEL error \
	  "Il n'y a pas de simulations r�alis�es"
    } else {
	# definition des variables contenant les donnees
	global Concentre
	global Mais
	global Foin
	global Herbe
	global LaitReel
	global LaitPotentiel
	global Estockee
	global ZdateMH
	global ZdateFin1erCycle
	global ZdateSortieNuit
	global ZdateFermetureSilo
	global ZdateOuvertureSilo
	global ZdateEnsilage
	global Zdate1ereFauche
	global Zqcoupee
	global Zscoupee
	
	# lecture des donnees
	ResumeLireDonnees $liste_annees $dir_strategie

	# affichage des donnees
	ResumeAfficherDonnees $w $liste_annees 
    }
}



#------------------------------------------------#
# Lecture des donnees des fichiers
#------------------------------------------------#
proc ResumeLireDonnees {liste_annees dir_strategie} {
    global FIC_CHRONIQUE
    global FIC_DATES_CLEFS

    global Concentre
    global Mais
    global Foin
    global Herbe
    global LaitReel
    global LaitPotentiel
    global Estockee
    global ZdateMH
    global ZdateFin1erCycle
    global ZdateSortieNuit
    global ZdateFermetureSilo
    global ZdateOuvertureSilo
    global ZdateEnsilage
    global Zdate1ereFauche
    global Zqcoupee
    global Zscoupee
	

    foreach annee $liste_annees {

	# lecture somme concentre, mais, foin, herbe
	if [file exist $dir_strategie$annee/$FIC_CHRONIQUE] {
	    set fileid [open [glob $dir_strategie$annee/$FIC_CHRONIQUE] r]
	    set Concentre($annee) 0
	    set Mais($annee) 0
	    set Foin($annee) 0
	    set Herbe($annee) 0

	    while { [gets $fileid line] >= 0 } {
		if { [string range $line 0 0] != "*"} {
		    set debut [Enieme_mot_de_ligne $line 0]
		    if {[string compare $debut  Concentre] == 0} {
			set Concentre($annee) \
			    [expr $Concentre($annee) + [Enieme_mot_de_ligne $line 1]]
		    } elseif {[string compare $debut  Mais] == 0} {
			set Mais($annee) \
			    [expr $Mais($annee) + [Enieme_mot_de_ligne $line 1]]
		    } elseif {[string compare $debut  Foin] == 0} {
			set Foin($annee) \
			    [expr $Foin($annee) + [Enieme_mot_de_ligne $line 1]]
		    } elseif {[string compare $debut  Paturage] == 0} {
			set Herbe($annee) \
			    [expr $Herbe($annee) + [Enieme_mot_de_ligne $line 1]]
		    }
		}
	    }
	    close $fileid
	} else {
	    set Concentre($annee) inconnu
	    set Mais($annee) inconnu
	    set Foin($annee) inconnu
	    set Herbe($annee) inconnu
	    }

	# lecture somme de lait reel, jour debut, jour fin (non compris)
	if [file exist $dir_strategie$annee/q_l_r] {
	    set fileid [open [glob $dir_strategie$annee/q_l_r]]
	    gets $fileid line
	    set LaitReel($annee) 0
	    set j 0
	    set j_debut 0
	    set j_fin 0
	    
	    while { [gets $fileid line] >= 0 } {
		incr j
		if {$line == -1} {
		    if {$j_debut != 0 && $j_fin == 0} {
			set j_fin $j
			} 
		} else {
		    if {$j_debut == 0} {
			set j_debut $j
		    }    
		    set LaitReel($annee) [expr $LaitReel($annee) + $line]
		}
	    }
	    if {$j_debut != 0 && $j_fin == 0} {
		# on va jusqu'a la fin du fichier sans trouver -1
		set j_fin $j
	    }
	    close $fileid
	} else {
	    set LaitReel($annee) inconnu
	    set j_debut 0
	    set j_fin 0
	}

	# lecture somme de lait potentiel
	if [file exist $dir_strategie$annee/q_l_p] {
	    set fileid [open [glob $dir_strategie$annee/q_l_p]]
	    gets $fileid line
	    set LaitPotentiel($annee) 0
	    set j 0
	    
	    while { [gets $fileid line] >= 0 } {
		    incr j
		if {$j >= $j_debut && $j < $j_fin} {
		    set LaitPotentiel($annee) [expr $LaitPotentiel($annee) + $line]
		    }
	    }
	    close $fileid
	} else {
	    set LaitPotentiel($annee) inconnu
	}

	# lecture energie stockee
	if [file exist $dir_strategie$annee/Estockee] {
	    set fileid [open [glob $dir_strategie$annee/Estockee]]
	    gets $fileid line 
	    set Estockee($annee) 0
	    set j 0
	    
	    while { [gets $fileid line] >= 0 } {
		    incr j
		if {$j >= $j_debut && $j < $j_fin} {
		    set Estockee($annee) [expr $Estockee($annee) + $line]
		    }
	    }
	    close $fileid
	} else {
	    set Estockee($annee) inconnu
	}

	# lecture des dates clefs
	if [file exist $dir_strategie$annee/$FIC_DATES_CLEFS] {
	    set fileid [open [glob $dir_strategie$annee/$FIC_DATES_CLEFS] r]
	    gets $fileid line
	    set ZdateMH($annee) $line
	    gets $fileid line 
	    set ZdateFin1erCycle($annee) $line
	    gets $fileid line
	    set ZdateSortieNuit($annee)  $line
	    gets $fileid line
	    set ZdateFermetureSilo($annee)  $line
	    gets $fileid line
	    set ZdateOuvertureSilo($annee)  $line
	    gets $fileid line
	    set ZdateEnsilage($annee)  $line
	    gets $fileid line
	    set Zdate1ereFauche($annee)  $line
	    gets $fileid line
	    set Zqcoupee($annee)  $line
	    gets $fileid line
	    set Zscoupee($annee) $line
	}
    }
}


#------------------------------------------------#
# Affichage de la fenetre et des donnees
#------------------------------------------------#
proc ResumeAfficherDonnees { w liste_annees} {

    global LOGICIEL

    catch {destroy $w}
    toplevel $w
    wm title $w "$LOGICIEL : Visualisation de la synthese des resultats de simulations"
    
    global Concentre
    global Mais
    global Foin
    global Herbe
    global LaitReel
    global LaitPotentiel
    global Estockee
    global ZdateMH
    global ZdateFin1erCycle
    global ZdateSortieNuit
    global ZdateFermetureSilo
    global ZdateOuvertureSilo
    global ZdateEnsilage
    global Zdate1ereFauche
    global Zqcoupee
    global Zscoupee

    # Frame du bas : Validation : Imprimer et Fermer
    frame $w.f_valid -borderwidth 2
    pack $w.f_valid -side bottom
    button $w.f_valid.b_imprimer -text Imprimer \
	-command "Imprimer_resume [list $liste_annees]"
    button $w.f_valid.b_fermer -text Fermer \
	-command "destroy $w"
    pack $w.f_valid.b_fermer $w.f_valid.b_imprimer \
	-side right -expand true -padx 60
    
    tixScrolledGrid $w.scrg_grille -bd 0 -width 700 -height 400
    pack $w.scrg_grille -expand yes -padx 20 -pady 10
    set grid [$w.scrg_grille subwidget grid]
    $grid config -formatcmd "EditGrid_format $grid"
    
    # Definition de la taille des colonnes
    $grid size column 0 -size 10char
    $grid size column 1 -size 10char
    $grid size column 2 -size 10char
    $grid size column 3 -size 10char
    $grid size column 4 -size 10char
    $grid size column 5 -size 10char
    $grid size column 6 -size 10char
    $grid size column 7 -size 10char
    $grid size row default -size 1.1char
    
    $grid set 0 0 -text "Annee"
    $grid set 1 0 -text "Mise herbe"
    $grid set 2 0 -text "Fin 1er cycle"
    $grid set 3 0 -text "Sortie nuit"
    $grid set 4 0 -text "Fermeture silo"
    $grid set 5 0 -text "Ouverture silo"
    $grid set 6 0 -text "Ensilage"
    $grid set 7 0 -text "1�re fauche"
    $grid set 8 0 -text "Q coup�e (kg)"
    $grid set 9 0 -text "S coup�e (ha)"
    $grid set 10 0 -text "Concentre"
    $grid set 11 0 -text "Mais"
    $grid set 12 0 -text "Foin"
    $grid set 13 0 -text "Herbe"
    $grid set 14 0 -text "LaitReel"
    $grid set 15 0 -text "LaitPotentiel"
    $grid set 16 0 -text "Estockee"

    set ligne 1
    foreach annee $liste_annees {
	$grid set 0 $ligne -text $annee
	$grid set 1 $ligne -text $ZdateMH($annee)
	$grid set 2 $ligne -text $ZdateFin1erCycle($annee)
	$grid set 3 $ligne -text $ZdateSortieNuit($annee)
	$grid set 4 $ligne -text $ZdateFermetureSilo($annee)
	$grid set 5 $ligne -text $ZdateOuvertureSilo($annee)
	$grid set 6 $ligne -text $ZdateEnsilage($annee)
	$grid set 7 $ligne -text $Zdate1ereFauche($annee)
	$grid set 8 $ligne -text $Zqcoupee($annee)
	$grid set 9 $ligne -text $Zscoupee($annee)
	$grid set 10 $ligne -text $Concentre($annee)
	$grid set 11 $ligne -text $Mais($annee)
	$grid set 12 $ligne -text $Foin($annee)
	$grid set 13 $ligne -text $Herbe($annee)
	$grid set 14 $ligne -text $LaitReel($annee)
	$grid set 15 $ligne -text $LaitPotentiel($annee)
	$grid set 16 $ligne -text $Estockee($annee)

	incr ligne
    }
}

#-----------------------------------------------#
# impression des donnees
#-----------------------------------------------#
proc Imprimer_resume {liste_annees} {

    global LOGICIEL
    global CMD_REMOVE
    global CMD_A2PS
    global CMD_IMPRIMER
    global OPT_IMPRIMER
    global DIR_TEMPORAIRE

    global Concentre
    global Mais
    global Foin
    global Herbe
    global LaitReel
    global LaitPotentiel
    global Estockee
    global ZdateMH
    global ZdateFin1erCycle
    global ZdateSortieNuit
    global ZdateFermetureSilo
    global ZdateOuvertureSilo
    global ZdateEnsilage
    global Zdate1ereFauche
    global Zqcoupee
    global Zscoupee

    set filetxt [open "${DIR_TEMPORAIRE}resume.imp" w]
    puts $filetxt "RESUME DE SIMULATIONS\n\n"

    puts $filetxt "-------------------------------------------------------------------------------"
    puts $filetxt " Annee  |MiseHerbe|Fin1erCyc|SortiNuit|FermeSilo|Ensilage |1erFauche"
    puts $filetxt "-------------------------------------------------------------------------------"

    foreach annee $liste_annees {
	puts -nonewline $filetxt "  $annee  |"
	puts -nonewline $filetxt [FormatterNb_resume $ZdateMH($annee)]
	puts -nonewline $filetxt "|" 
	puts -nonewline $filetxt [FormatterNb_resume $ZdateFin1erCycle($annee)]
	puts -nonewline $filetxt "|" 
	puts -nonewline $filetxt [FormatterNb_resume $ZdateSortieNuit($annee)]
	puts -nonewline $filetxt "|" 
	puts -nonewline $filetxt [FormatterNb_resume $ZdateFermetureSilo($annee)]
	puts -nonewline $filetxt "|"
	puts -nonewline $filetxt [FormatterNb_resume $ZdateOuvertureSilo($annee)]
	puts -nonewline $filetxt "|"
	puts -nonewline $filetxt [FormatterNb_resume $ZdateEnsilage($annee)]
	puts -nonewline $filetxt "|" 
	puts $filetxt [FormatterNb_resume $Zdate1ereFauche($annee)]
    }

    puts $filetxt "-------------------------------------------------------------------------------"


    puts $filetxt "-------------------------------------------------------------------------------"
    puts $filetxt " Annee  |Concentre|  Mais   |  Foin   |  Herbe  |Qcoupee  |Scoupee"
    puts $filetxt "-------------------------------------------------------------------------------"

    foreach annee $liste_annees {
	puts -nonewline $filetxt "  $annee  |"
	puts -nonewline $filetxt [FormatterNb_resume $Concentre($annee)]
	puts -nonewline $filetxt "|" 
	puts -nonewline $filetxt [FormatterNb_resume $Mais($annee)]
	puts -nonewline $filetxt "|" 
	puts -nonewline $filetxt [FormatterNb_resume $Foin($annee)]
	puts -nonewline $filetxt "|" 
	puts -nonewline $filetxt [FormatterNb_resume $Herbe($annee)]
	puts -nonewline $filetxt "|" 
	puts -nonewline $filetxt [FormatterNb_resume $Zqcoupee($annee)]
	puts -nonewline $filetxt "|"
	puts $filetxt [FormatterNb_resume $Zscoupee($annee)]
    }

    puts $filetxt "-------------------------------------------------------------------------------"



    puts $filetxt "-------------------------------------------------------------------------------"
    puts $filetxt " Annee   |LaitReel | LaitPot | Estockee "
    puts $filetxt "-------------------------------------------------------------------------------"

    foreach annee $liste_annees {
	puts -nonewline $filetxt "  $annee  |"
	puts -nonewline $filetxt [FormatterNb_resume $LaitReel($annee)]
	puts -nonewline $filetxt "|" 
	puts -nonewline $filetxt [FormatterNb_resume $LaitPotentiel($annee)]
	puts -nonewline $filetxt "|"
	puts $filetxt [FormatterNb_resume $Estockee($annee)]
    }

    puts $filetxt "-------------------------------------------------------------------------------"

    close $filetxt

    # Impression et suppression de ce fichier
    catch {exec $CMD_A2PS ${DIR_TEMPORAIRE}resume.imp}
    exec $CMD_REMOVE ${DIR_TEMPORAIRE}resume.imp

    CreerFenetreDialogue $LOGICIEL info "Impression en cours"
}

#------------------------------------------------------------------
# renvoie nb sous la forme xxxxxx.xx (9 caracteres)
# en mettant des blancs ou necessaire
#------------------------------------------------------------------
proc FormatterNb_resume {nb} {

    set l [string length $nb]
    if {$l > 9} {
	puts "ERREUR dans la procedure FormatterNb_resume, nb a plus de 9 caracteres"
    } else {
	set i_point [string first . $nb ]
	if {$i_point > 6} {
	    puts "ERREUR dans la procedure FormatterNb_resume, nb a plus de 6 caracteres avant la virgule"
	} else {
	    if {$i_point != -1} {
		set avant [string range $nb 0 [expr $i_point - 1]]
		set apres [string range $nb [expr $i_point + 1] end]
	    } else {
		set avant $nb
		set apres ""
	    }
	    set l_avant [string length $avant]
	    set l_apres [string length $apres]

	    if {$l_avant == 0} {
		set resultat "      "
	    } elseif {$l_avant == 1} {
		set resultat "     "
	    } elseif {$l_avant == 2} {
		set resultat "    "
	    } elseif {$l_avant == 3} {
		set resultat "   "
	    } elseif {$l_avant == 4} {
		set resultat "  "
	    } elseif {$l_avant == 5} {
		set resultat " "
	    } elseif {$l_avant == 6} {
		set resultat ""
	    } 
	    append resultat $avant .

	    if {$l_apres == 0} {
		append resultat "  "
	    } elseif {$l_apres == 1} {
		append resultat $apres " "
	    } else {
		append resultat [string range $apres 0 1]
	    }
		      
	    return $resultat
	}
    }
}
