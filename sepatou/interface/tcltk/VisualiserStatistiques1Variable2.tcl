# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##########################################################################
# Fichier : VisualiserStatistiques1Variable2.tcl
# Contenu : Affichage de la fenetre graphique des variables V1 et V2
##########################################################################


#-----------------------------------------------#
# Procedure qui se charge d'afficher la fenetre #
# graphique des  variables contenues dans la    #
# donnee en parametre                           #
#-----------------------------------------------#
proc VisualiserStatistiques1Variable2 {V1 V2} {

    global LOGICIEL
    global DIR_STATISTIQUES

    global l_var_statistiques
    global tab_valeurs_max_min 
    global pas
    global num
    global color
    global min
    global max

    global palette1; global palette2;
  

    ############################### initialisation ############################
    # initialisation du numero de fenetre 
    incr num
    set w .wm_VisuStatistiques1Variable2$num
    
    global  palette3$num
    global palette_choisie$num

    # initialisation des palettes de couleurs
    set palette1 [list black blue cyan green yellow orange brown red] 
    set palette2 [list white yellow orange red red3 brown black]
    set palette3$num {}

    # initialisation avec la palette1
    set palette_choisie$num palette1
    for {set j 0} {$j<[llength $palette1]} {incr j} {
	set color($j) [lindex $palette1 $j]
    }

    #initialisation des minimums et des maximums
    set i [lsearch -exact $l_var_statistiques $V1]
    incr i
    set j [lsearch -exact $l_var_statistiques $V2]
    incr j

    set min(${num}1) $tab_valeurs_max_min($i,1)
    set max(${num}1) $tab_valeurs_max_min($i,2)
    set min(${num}2) $tab_valeurs_max_min($j,1)
    set max(${num}2) $tab_valeurs_max_min($j,2)

    #initialisation du pas  
    set pas(${num}1) [PasCourant $V1 $max(${num}1) $min(${num}1)]  
    set pas(${num}2) [PasCourant $V2  $max(${num}2) $min(${num}2)]

    # lecture du nombre de valeurs dans les intervalles [min, max]
    set v1 [LireFichierStatistique $V1]
    set v2 [LireFichierStatistique $V2]
    for {set i 0} {$i<=[llength $v1]} {incr i} {
	set val [lindex $v1 $i]
	if {$val>=$min(${num}1)&&$val<=$max(${num}1)} {
	    lappend val_entre_intervalle(${num}1) $val
	}
    } 
    for {set i 0} {$i<=[llength $v2]} {incr i} {
	set val [lindex $v2 $i]
	if {$val>=$min(${num}2)&&$val<=$max(${num}2)} {
	    lappend val_entre_intervalle(${num}2) $val
	}
    }
    set l1 [llength $val_entre_intervalle(${num}1)]
    set l2 [llength $val_entre_intervalle(${num}2)]


    ############################ creation de la fenetre #######################
    # Creation de la fenetre
    toplevel ${w}
    wm title ${w} "$LOGICIEL : Visualisation statistique de $V1 et $V2"

    # La fenetre est compose de 7 frames:   
    #          _____________________
    #          |         7         |  
    #          --------------------|
    #          |  | |         | |  | 
    #          |1 |2|    3    |4| 5|
    #          |  | |         | |  | 
    #          |  | |         | |  |
    #          ---------------------
    #          |         6         |
    #          ---------------------

    ## remplissage du frame du bas (6)
    frame ${w}.f_sep -height 2 \
	-borderwidth 1 -relief sunken
    frame ${w}.f_down -borderwidth 2 
    pack ${w}.f_down \
	${w}.f_sep -side bottom -fill x
    button ${w}.f_down.b_imp \
	-text {Imprimer} -command "ImprimerVisuVariable2 $w"
    button ${w}.f_down.b_mem \
	-text {M�moriser} -command "MemoriserVisuVariable2 $w"
    button ${w}.f_down.b_annuler \
	-text {Fermer} -command "FermerVisuVariable2 $w"
    pack ${w}.f_down.b_mem \
	${w}.f_down.b_imp \
	${w}.f_down.b_annuler \
	-side left  -expand 1 -padx 60 -pady 2
   
    ## remplissage du frame du haut (7)
    frame ${w}.f_top -borderwidth 2 
    pack ${w}.f_top -side top 
    
    button ${w}.f_top.b_nom1 \
	-text "Carres" -width 12 -relief sunken \
	-command "AfficherCarres2 ${w} $num $min(${num}1) $min(${num}2) $max(${num}1) $max(${num}2) $V1 $V2 [list $val_entre_intervalle(${num}1)]  [list $val_entre_intervalle(${num}2)] $pas(${num}1)  $pas(${num}2)"
    button ${w}.f_top.b_nom2 \
	-text "Nuage de points" \
	-width 12 -command "AfficherNuage2 ${w} $min(${num}1) $min(${num}2) $max(${num}1) $max(${num}2) $V1 $V2 [list $val_entre_intervalle(${num}1)] [list $val_entre_intervalle(${num}2)] $pas(${num}1) $pas(${num}2)"   
    pack ${w}.f_top.b_nom1 \
	${w}.f_top.b_nom2 -padx 15 \
	-side left 
   
    # separation du frame 7 avec les frames au-dessous
    frame ${w}.f_septop -height 2 \
	-borderwidth 1 -relief sunken
    pack ${w}.f_septop \
	-side top -fill x -pady 2

    ## remplissage du frame 1
    frame ${w}.f_topl \
	-bg gray85 -borderwidth 2 
    pack  ${w}.f_topl \
	-side left -padx 2 -pady 2

    message ${w}.f_topl.msg_mess1 \
	-width 250 -bg gray85 -borderwidth 2  -justify left\
	-font {Helvetica -12 } \
	-text "Nombre de valeurs de $V1 : $l1 
Nombre  de valeurs de $V2 : $l2"
    pack ${w}.f_topl.msg_mess1 -anchor w

    #separation des frames 1 et 2
    frame ${w}.f_sepl -width 2  \
	-borderwidth 1 -relief sunken  
    pack ${w}.f_sepl \
	-side left -fill y -padx 2
   
    ## remplissage du frame 2
    frame ${w}.f_gauche
    pack ${w}.f_gauche \
	-side left -padx 2 -pady 2 -fill y
    
    # creation des zones de texte pour les quantiles
    text ${w}.f_gauche.t1 \
	-width 5 -height 1 -relief flat -background lightgrey -state disabled
    text ${w}.f_gauche.t2 \
	-width 5 -height 1 -relief flat -background lightgrey -state disabled
    pack ${w}.f_gauche.t1 \
	-side top -padx 8    
    pack ${w}.f_gauche.t2 \
	-side bottom -padx 8
  
    ## remplissage du frame 3
    frame ${w}.f_topmid 
    pack ${w}.f_topmid \
	-side left 
 
    #creation du graphe de la bte a moustache horizontale
    graph ${w}.f_topmid.f_btes_hor \
	-height 160 -width 400 -title "" -plotrelief flat  -rightmargin 20 \
	-leftmargin 102
    if ($max(${num}1)!=$min(${num}1)) {
	${w}.f_topmid.f_btes_hor xaxis \
	    configure -min [expr $min(${num}1)-$pas(${num}1)/2] \
				-max [expr $max(${num}1)+$pas(${num}1)/2] \
			        -stepsize $pas(${num}1) -title "$V1" \
				     -color lightgrey -titlecolor lightgrey
					       
    } else {
	${w}.f_topmid.f_btes_hor xaxis \
	    configure -min [expr $min(${num}1)-0.5] \
	    -max [expr $max(${num}1)+0.5] \
	    -title "$V1" -color lightgrey -titlecolor lightgrey
    }
    ${w}.f_topmid.f_btes_hor \
	yaxis configure -min 0 -max 40 -title "$V1" -color lightgrey \
	-titlecolor lightgrey
    pack ${w}.f_topmid.f_btes_hor \
	-side bottom -anchor e

    # creation du graphe de la bte a moustache verticale
    graph ${w}.f_topmid.f_btes_vert \
	-width 100 -height 400 -topmargin 20 -bottommargin 65 \
	-title "" -plotrelief flat
   if ($max(${num}2)!=$min(${num}2)) {
	${w}.f_topmid.f_btes_vert yaxis \
	    configure -min [expr $min(${num}2)-$pas(${num}2)/2] \
	    -max [expr $max(${num}2)+$pas(${num}2)/2] -stepsize $pas(${num}2) \
	    -title "$V2" -hide 1
    } else {
	${w}.f_topmid.f_btes_vert \
	    yaxis configure -min [expr $min(${num}2)-0.5] \
	    -max [expr $max(${num}2)+0.5] \
	    -title "$V2" -hide 1
    }
   
    ${w}.f_topmid.f_btes_vert xaxis \
	configure -min 0 -max 40 -title "$V2" -color lightgrey \
	-titlecolor lightgrey
    pack ${w}.f_topmid.f_btes_vert \
	-side left -anchor w
    
    # creation du graphique (carres et nuage)
    graph ${w}.f_topmid.g1 -title "" \
	-width 400 -height 400 -rightmargin 20 -leftmargin 100 \
	-topmargin 20 -bottommargin 65 -borderwidth 1
    pack ${w}.f_topmid.g1 	
    
    ## remplissage du frame 4
    frame ${w}.f_droite   
    pack ${w}.f_droite \
	-side left -padx 2 -pady 2 -fill y

    # creation des zones de texte pour les quantiles
    text ${w}.f_droite.t1\
	-width 5 -height 1 -relief flat -background lightgrey -state disabled
    text ${w}.f_droite.t2 \
	-width 5 -height 1 -relief flat -background lightgrey -state disabled
    #package des zones de texte  
    pack ${w}.f_droite.t1 \
	-side top -padx 8
    pack ${w}.f_droite.t2 \
	-side bottom  -padx 8 
    #separation
    frame ${w}.f_sepr -width 2 \
	-borderwidth 1 -relief sunken
    pack ${w}.f_sepr \
	-side left -fill y -padx 2
   
    ## remplissage du frame 5
    frame ${w}.f_topr -borderwidth 2 
    pack  ${w}.f_topr \
	-side left -padx 2 -pady 2

    # boutons generaux, les autres sont geres par les procedures specifiques
   
    button ${w}.f_topr.b_moyenne \
	-text {Moyenne} -width 8 -command \
	"AfficherMoyenne2 ${w} $V1 $V2 [list $val_entre_intervalle(${num}1)] [list $val_entre_intervalle(${num}2)]"
    
    button ${w}.f_topr.b_mediane \
	-text {M�diane} -width 8 -command \
	"AfficherMediane2 ${w}  $V1 $V2 [list $val_entre_intervalle(${num}1)] [list $val_entre_intervalle(${num}2)] $min(${num}1) $min(${num}2) $max(${num}1)  $max(${num}2)"

    
    button ${w}.f_topr.b_quantiles \
	-text {Quantiles} -width 8 -command \
	"AfficherQuantiles2 ${w} [list $val_entre_intervalle(${num}1)] [list $val_entre_intervalle(${num}2)] $min(${num}1) $min(${num}2) $max(${num}1)  $max(${num}2)" 
 
    pack ${w}.f_topr.b_moyenne \
	${w}.f_topr.b_mediane \
	${w}.f_topr.b_quantiles -pady 15

  

    ###################### initialisation du graphique par les carres ########
    AfficherCarres2  ${w}  $num \
	$min(${num}1) $min(${num}2) $max(${num}1) $max(${num}2) $V1 $V2 \
	$val_entre_intervalle(${num}1) $val_entre_intervalle(${num}2) \
	$pas(${num}1)  $pas(${num}2)  

}


#----------------------------------------------#
# fermeture des sous fenetres (fenetres s'appellant $w...)
#----------------------------------------------#
proc FermerVisuVariable2 {w} {

    set list_fenetres [winfo children .]
    DetruireFenetresPrefixees $w $list_fenetres
}

#----------------------------------------------#
#  affiche la fenetre permettant la memorisation du graphique 
#----------------------------------------------#
proc MemoriserVisuVariable2 {w} {

    global LOGICIEL
    global DIR_STATISTIQUES

    catch {destroy ${w}Memoriser}
    toplevel ${w}Memoriser
    wm title ${w}Memoriser \
	"$LOGICIEL : Memorisation de la sortie graphique courante"

    # recherche des fichiers .ps
    set fichiers_ps [list]   
    foreach f [recherche_fic $DIR_STATISTIQUES] {
	if {[regexp .ps $f]==1} {
	    lappend fichiers_ps [string range $f 0 [expr [string length $f]-4]]
	}
    }
    # affichage de la liste de fichiers
    frame ${w}Memoriser.f_aff
    label ${w}Memoriser.f_aff.l_titre \
	-text "Liste des fichiers existants :"
    ScrolledListbox ${w}Memoriser.f_aff.scrlst_liste -setgrid true
    foreach f $fichiers_ps {
	${w}Memoriser.f_aff.scrlst_liste.lst_list insert end $f
    }
    # zone d'entree du nom donne par l'utilisateur
    label ${w}Memoriser.f_aff.l_nom -text "Nom "
    entry ${w}Memoriser.f_aff.e_nom 
    pack ${w}Memoriser.f_aff.l_titre -side top -pady 8 -anchor e
    pack ${w}Memoriser.f_aff.scrlst_liste \
	-side top -pady 15 -anchor e
    pack ${w}Memoriser.f_aff.l_nom -side left -anchor w
    pack ${w}Memoriser.f_aff.e_nom -side left
    pack ${w}Memoriser.f_aff -padx 10 -pady 10

    ###Zone de commandes des boutons ok, annuler
    frame ${w}Memoriser.f_bottom -borderwidth 3
    pack ${w}Memoriser.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button ${w}Memoriser.f_bottom.b_ok -text Ok \
	-command "OkMemoriser2 $w"
    button ${w}Memoriser.f_bottom.b_cancel \
	-text Annuler -command "destroy ${w}Memoriser"
    pack ${w}Memoriser.f_bottom.b_ok \
         ${w}Memoriser.f_bottom.b_cancel -side left -expand 1 

    ### Separateur
    frame ${w}Memoriser.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack ${w}Memoriser.f_sep -side bottom -fill x -pady 5
}

#----------------------------------------------#
# memorisation du graphique 
#                w: la fenetre courante
#----------------------------------------------#
proc OkMemoriser2 {w} {

    global LOGICIEL
    global DIR_STATISTIQUES

    # recuperation des infos
    set nom [${w}Memoriser.f_aff.e_nom get]
    set lfichierPS [${w}Memoriser.f_aff.scrlst_liste.lst_list get 0 end]

    # verification du nom
    if {[PbControleChaine $nom fichier] != 0} {
	CreerFenetreDialogue $LOGICIEL error \
	    "Le nom donn� n'est pas correct"
    } elseif  {[lsearch -exact $lfichierPS $nom] != -1} {
	# creation d'une fenetre de confirmation
	toplevel ${w}MemoriserConfirm
	wm title ${w}MemoriserConfirm "$LOGICIEL : Confirmation de la saisie"
	frame ${w}MemoriserConfirm.f_question
	frame ${w}MemoriserConfirm.f_sep -relief sunken \
	    -borderwidth 1 -height 2
	label ${w}MemoriserConfirm.f_question.l_icon \
	    -bitmap question -foreground red
	message ${w}MemoriserConfirm.f_question.msg_mess \
	    -text "Voulez-vous vraiment m�moriser le graphique en �crasant le graphique existant ? "
	pack ${w}MemoriserConfirm.f_question.l_icon \
	    ${w}MemoriserConfirm.f_question.msg_mess \
	    -side left -pady 12 -pady 16 -expand true
	pack ${w}MemoriserConfirm.f_question \
	    ${w}MemoriserConfirm.f_sep -side top -fill x
	# bouton OK et Annuler
	button ${w}MemoriserConfirm.b_ok -text "Ok" \
	    -command "$w.f_topmid.g1 postscript output ${DIR_STATISTIQUES}$nom.ps;destroy ${w}Memoriser;destroy ${w}MemoriserConfirm" 
	pack ${w}MemoriserConfirm.b_ok \
	    -side left -pady 4 -padx 20 -fill x
	button ${w}MemoriserConfirm.b_annuler -text "Annuler" \
	    -command "destroy ${w}MemoriserConfirm"
	pack ${w}MemoriserConfirm.b_annuler \
	    -side left -padx 20 -pady 4 -fill x 
    } else {
	# enregistrement du fichier
	$w.f_topmid.g1 postscript output ${DIR_STATISTIQUES}$nom.ps
	destroy ${w}MemoriserConfirm
	destroy ${w}Memoriser
    }
}

#----------------------------------------------#
# impression du graphique 
#                        w: la fenetre courante
#----------------------------------------------#
proc ImprimerVisuVariable2 {w} {

    global LOGICIEL
    global DIR_TEMPORAIRE
    global CMD_IMPRIMER
    global OPT_IMPRIMER
    global CMD_REMOVE

    # creation du fichier postcript 
    $w.f_topmid.g1 postscript output ${DIR_TEMPORAIRE}bidim$w.ps
    # impression et supression du fichier
    catch {exec $CMD_IMPRIMER $OPT_IMPRIMER  ${DIR_TEMPORAIRE}bidim$w.ps}
    exec $CMD_REMOVE ${DIR_TEMPORAIRE}bidim$w.ps
    CreerFenetreDialogue $LOGICIEL info "Impression en cours"    
}


#------------------------------------------------------------------------#
# Affichage de l'ecart type (ellipse) et de la moyenne
#-----------------------------------------------------------------------#
proc AfficherMoyenne2 {w V1 V2 val_entre_intervalle1 val_entre_intervalle2} {  

    set etat_bmoyenne2 [$w.f_topr.b_moyenne cget -relief]
    if {$etat_bmoyenne2=="raised"} {
	$w.f_topr.b_moyenne configure -relief sunken
	AfficherMoyenneEcartType $w $V1 $V2 $val_entre_intervalle1 \
	    $val_entre_intervalle2
    } else {
	$w.f_topr.b_moyenne configure -relief raised
	DetruireMoyenneEcartType $w
    }
}

#---------------------------------------------------------------------------#
# creation de la visulisation de la  moyenne et de l' ecart-type 
#---------------------------------------------------------------------------#
proc AfficherMoyenneEcartType {w V1 V2 val_entre_intervalle1  val_entre_intervalle2} {
    
    # destruction de la moyenne si elle existe deja
    catch {DetruireMoyenneEcartType $w}
  
    # lecture des stat elementaires 
    set moy1 [lindex [CalculerStatistiquesElementaires \
			  $val_entre_intervalle1] 3]
    set moy2 [lindex [CalculerStatistiquesElementaires \
			  $val_entre_intervalle2] 3]
    set e_t1 [lindex [CalculerStatistiquesElementaires \
			  $val_entre_intervalle1] 4]
    set e_t2 [lindex [CalculerStatistiquesElementaires \
			  $val_entre_intervalle2] 4]
    # calcul des coords des axes de l'ellipse
    set dif1 [expr $moy1-$e_t1/2]
    set dif2 [expr $moy2-$e_t2/2]
    set som1 [expr $moy1+$e_t1/2]
    set som2 [expr $moy2+$e_t2/2]
    # affichage du point moyenne et des axes  
    $w.f_topmid.g1 element create et1 -symbol none -label "" \
	-color white -xdata {$dif1 $som1} -ydata {$moy2 $moy2} -linewidth 6
    $w.f_topmid.g1 element create et2 -symbol none -label "" \
	-color white -xdata {$moy1 $moy1} -ydata {$dif2 $som2} -linewidth 6 
    $w.f_topmid.g1 element create et12 -symbol none -label "" \
	-color orange -xdata {$dif1 $som1} -ydata {$moy2 $moy2} -linewidth 2 
    $w.f_topmid.g1 element create et22 -symbol none -label "" \
	-color orange -xdata {$moy1 $moy1} -ydata {$dif2 $som2} -linewidth 2 
    $w.f_topmid.g1 element create moyenne_nuage \
	    -xdata $moy1 -ydata $moy2 -label "" \
	-fill orange4 -outline white -outlinewidth 2 -symbol circle -pixels 15 
    #creation de l'ellipse
    CreerEllipse $w.f_topmid.g1 $moy1 $moy2 [expr $e_t1/2]  [expr $e_t2/2]
    # creation des messages donnant les moyennes et les ecarts type
    message $w.f_topl.msg_mess_vide -text "" -bg gray85
    message $w.f_topl.msg_mess_moyenne1 \
	-text "Moyenne de $V1 : [ArrondirA1Decimale $moy1]
Moyenne de $V2 : [ArrondirA1Decimale $moy2 ]" \
	-foreground orange4 -width 250 -justify left -font {Helvetica -12} \
        -bg gray85 -width 250
  
    message $w.f_topl.msg_mess_ecart1 -text "Ecart-type de $V1 : [ArrondirA1Decimale  $e_t1]
Ecart-type de $V2 : [ArrondirA1Decimale $e_t2]" \
	-foreground orange -font {Helvetica -12 } -justify left -width 250 \
        -bg gray85
    message $w.f_topl.msg_mess_reserve -text "(Repr�sentation graphique sous r�serve de normalit�)" \
	-foreground orange -font {Helvetica -10 } -justify left -width 250 \
        -bg gray85

    pack $w.f_topl.msg_mess_vide $w.f_topl.msg_mess_moyenne1 \
	 $w.f_topl.msg_mess_ecart1 $w.f_topl.msg_mess_reserve \
         -anchor w 
}

#-------------------------------------------------#
# destruction de la visualisation de la moyenne et de l'ecart type 
#               w :la fenetre
#------------------------------------------------#
proc DetruireMoyenneEcartType {w} {  
    $w.f_topmid.g1 element delete moyenne_nuage 
    $w.f_topmid.g1 element delete et1
    $w.f_topmid.g1 element delete et2
    $w.f_topmid.g1 element delete et12
    $w.f_topmid.g1 element delete et22
    DetruireEllipse $w.f_topmid.g1 
    destroy $w.f_topl.msg_mess_vide
    destroy $w.f_topl.msg_mess_moyenne1
    destroy $w.f_topl.msg_mess_moyenne2
    destroy $w.f_topl.msg_mess_ecart1
    destroy $w.f_topl.msg_mess_ecart2
    destroy $w.f_topl.msg_mess_reserve
}
#---------------------------------------------------------#
# Procedure qui permet d'afficher l'ellipse de l'ecart type
#         g:le graphique
#         moy1 moy 2 : les moyennes des variables (resp V1 et V2)
#         ecart_type1 ecart_type2: les evart _type des variables
#----------------------------------------------------------#
proc CreerEllipse {g moy1 moy2 ecart_type1 ecart_type2} {

    set xcoords_ellipse {}
    set ycoords_ellipse {}
    set ycoords_ellipse2 {}
   
    # calcul des abscisses
    set xcoords_ellipse \
	[CalculerAbscisseEllipse $moy1 $ecart_type1 ]

    # calcul des ordonnees 
    set ycoords_ellipse \
	[CalculerCoordonneesEllipseHaut $moy2 $ecart_type2]
   
    set ycoords_ellipse2 \
	[CalculerCoordonneesEllipseBas $moy2 $ecart_type2]
   
    # trac� de l'ellipse
    $g element create ellips2 -xdata $xcoords_ellipse -ydata $ycoords_ellipse2 \
	-symbol none -color white -label "" -linewidth 6 
    $g element create ellips3 -xdata $xcoords_ellipse -ydata $ycoords_ellipse2 \
	-symbol none -color orange -label "" -linewidth 2
    $g element create ellips1 -xdata $xcoords_ellipse -ydata $ycoords_ellipse \
	-symbol none -color white -label "" -linewidth 6
    $g element create ellips4 -xdata $xcoords_ellipse -ydata $ycoords_ellipse \
	-symbol none -color orange -label "" -linewidth 2
}

#---------------------------------------------------------#
# Procedure qui permet de detruire l'ellipse de l'ecart type
#         g: le graphique
#----------------------------------------------------------#
proc DetruireEllipse {g} {
    $g element delete ellips1
    $g element delete ellips2 
    $g element delete ellips3
    $g element delete ellips4

}

#--------------------------------------------------------------------#
# procedure qui affiche la boite a moustache de la moyenne
#----------------------------------------------------------------------#
proc AfficherMediane2 {w V1 V2 val_entre_intervalle1  val_entre_intervalle2 \
			   min1 min2 max1 max2 } {

    # lecture des donnees comprise entre l'intervalle choisi 
    # dans la grille des intervalles de valeurs
    set etat_bmediane2 [$w.f_topr.b_mediane cget -relief]
    if {$etat_bmediane2=="raised"} {
	# calcul des statistiques elementaires
	set stat_elem1 [CalculerStatistiquesElementaires \
			    $val_entre_intervalle1]
	set val_except1 [CalculerValeursExceptionnelles \
			 [lindex $stat_elem1 0] \
			     [lindex $stat_elem1 2] $val_entre_intervalle1]
	set stat_elem2 [CalculerStatistiquesElementaires \
			    $val_entre_intervalle2]
	set val_except2 [CalculerValeursExceptionnelles \
			     [lindex $stat_elem2 0] \
			     [lindex $stat_elem2 2] $val_entre_intervalle2]

	#configuration du bouton
	$w.f_topr.b_mediane configure -relief sunken 
	# affichage des boites a moustaches
	AfficherBoiteAMoustache $w.f_topmid.f_btes_vert "v" \
	    $min2 [lindex $stat_elem2 0] [lindex $stat_elem2 1] \
	    [lindex $stat_elem2 2] $max2 $val_except2
	AfficherBoiteAMoustache $w.f_topmid.f_btes_hor "h" \
	    $min1 [lindex $stat_elem1 0] [lindex $stat_elem1 1] \
	    [lindex $stat_elem1 2] $max1 $val_except1
	# affichage des message donnant les medianes et les ecarts type
	message $w.f_topl.msg_mess_vide1 -text "" -bg gray85
	message $w.f_topl.msg_mess_mediane  -justify left -font {Helvetica -12} \
	    -text "M�diane de $V1 :  [ArrondirA1Decimale [lindex $stat_elem1 1] ] 
M�diane de $V2 :  [ArrondirA1Decimale [lindex $stat_elem2 1] ]" \
	    -foreground darkgreen -width 250 -bg gray85 
        message $w.f_topl.msg_mess_quartiles -justify left  \
            -foreground forestgreen -width 250 \
            -bg gray85  -anchor e  -font {Helvetica -12} \
            -text "1er quartile de $V1 : [ArrondirA1Decimale [lindex $stat_elem1 0] ] 
3�me quartile de $V1 : [ArrondirA1Decimale [lindex $stat_elem1 2] ] 
1er quartile de $V2 : [ArrondirA1Decimale [lindex $stat_elem2 0] ] 
3�me quartile de $V2 : [ArrondirA1Decimale [lindex $stat_elem2 2] ]" 

	   pack $w.f_topl.msg_mess_vide1 $w.f_topl.msg_mess_mediane \
	   $w.f_topl.msg_mess_quartiles -pady 2 -anchor w 

    } else {
	$w.f_topr.b_mediane configure -relief raised
	DetruireBoiteAMoustache  $w.f_topmid.f_btes_vert 
	DetruireBoiteAMoustache  $w.f_topmid.f_btes_hor 
	destroy $w.f_topl.msg_mess_mediane 	
        destroy $w.f_topl.msg_mess_quartiles 
        destroy $w.f_topl.msg_mess_vide1

    }
}
#-----------------------------------------------------------#
# Procedure qui active le bind                              #
#-----------------------------------------------------------#   
proc ActiverBind2 { w val_entre_intervalle1 val_entre_intervalle2 min1 min2 \
			max1 max2} {
   
    # mise en place de la zone et des boites    
    $w.f_topmid.g1 configure -bg lightpink  -relief solid
    $w.f_droite.t1 configure -relief sunken -background white
    $w.f_droite.t2 configure -relief sunken -background white
    $w.f_gauche.t2  configure -relief sunken -background white
    $w.f_gauche.t1 configure -relief sunken -background white
    # initialisation des variables globales pour le bind
    set surf [$w.f_topmid.g1 extents plotarea]
    set ecart_x [lindex [$w.f_topmid.g1 cget -plotpadx] 0]
    set ecart_y [lindex [$w.f_topmid.g1 cget -plotpady] 0]
    set xmin [expr [lindex $surf 0]+$ecart_x]
    set xmax [expr [lindex $surf 2]+$xmin-2*$ecart_x]
    set ymin [expr [lindex $surf 1]+$ecart_y]
    set ymax [expr [lindex $surf 3]+$ymin-2*$ecart_y]
    # enregistrement de la valeur d'un pixel
    set n [expr [expr [$w.f_topmid.g1 xaxis cget -max]- \
		     [$w.f_topmid.g1 xaxis cget -min]]/[expr $xmax-$xmin]]
    set p [expr [expr [$w.f_topmid.g1 yaxis cget -max]- \
		     [$w.f_topmid.g1 yaxis cget -min]]/[expr $ymax-$ymin]]

    set surf0 [lindex $surf 0]
    set surf1 [lindex $surf 1]
    # mise en place du crosshairs et des quantiles
    bind  $w.f_topmid.g1 <Motion> "$w.f_topmid.g1 crosshairs configure -position @%x,%y -color red; InsertQuantiles2 $w [list $val_entre_intervalle1] [list $val_entre_intervalle2] $min1 $min2 $max1 $max2 $ecart_x $ecart_y $surf0 $surf1 $n $p "
}

##############################################################################
# proc�dure qui permet de calculer les quantiles et de les inserer dans la 
# zone de texte
#               parametres:w:la fenetre
#               val_entre_intervalle1 val_entre_intervalle2:les variables
#               min1 min2:les minimums
#               max1 max2:les maximums
#               ecart_x ecart_y:les padx et pady du graphique
#               surf0 surf1:les marges a droite et au dessus du graphique
#               n p:les valeurs d'un pixel par rapport a x et par rapport a y
##############################################################################
proc InsertQuantiles2 {w val_entre_intervalle1 val_entre_intervalle2  min1 \
			   min2 max1 max2 ecart_x ecart_y surf0 surf1 n p} {
    
    
    # enregistrement de la position du curseur
    set pos [$w.f_topmid.g1 crosshairs cget -position]
    set pos2 [string trim $pos @]
    set pos3 [split $pos2 ,]

    # calcul des coordonnees reelles 
    set a [expr [$w.f_topmid.g1 xaxis cget -min]+ \
	       $n*[expr [lindex $pos3 0]-$surf0-$ecart_x]]
    set b [expr [$w.f_topmid.g1 yaxis cget -max]- \
	       $p*[expr [lindex $pos3 1]-$surf1-$ecart_y]]

    #  calcul des quantiles
    set qq [CalculerQuantiles2 $val_entre_intervalle1 $val_entre_intervalle2 \
		$a $b]
    set q1 [ArrondirA1Decimale [lindex $qq 0] ]
    set q2 [ArrondirA1Decimale [lindex $qq 1] ]
    set q3 [ArrondirA1Decimale [lindex $qq 2] ]
    set q4 [ArrondirA1Decimale [lindex $qq 3] ]
	 
    # insertion des quantiles
    $w.f_droite.t1 configure -state normal  -foreground red	
    $w.f_droite.t1 insert 1.0 "$q1%    "
    $w.f_droite.t1 delete 1.5 end
    $w.f_droite.t1 configure -state disabled

    $w.f_gauche.t1 configure -state normal -foreground red
    $w.f_gauche.t1 insert 1.0 "$q3%    "
    $w.f_gauche.t1 delete 2.0 end
    $w.f_gauche.t1 configure -state disabled 

    $w.f_droite.t2 configure -state normal -foreground red
    $w.f_droite.t2 insert 1.0 "$q2%    " 
    $w.f_droite.t2 delete 2.0 end
    $w.f_droite.t2 configure -state disabled

    $w.f_gauche.t2  configure -state normal -foreground red
    $w.f_gauche.t2 insert 1.0 "$q4%    "
    $w.f_gauche.t2 delete 2.0 end
    $w.f_gauche.t2  configure -state disabled
    $w.f_topmid.g1 crosshairs on
}

#-----------------------------------------------------------#
# Procedure qui desactive le bind                           #
#        w: la fenetre                                      #
#     graph :le graphique sur lequel on applique le bind    #
#-----------------------------------------------------------#   
proc DesactiverBind2 {w graph} {
    
    # desactivation des affichages associes aux quantiles
    $graph crosshairs off
    $w.f_droite.t2 configure -state normal -relief flat -background lightgrey
    $w.f_droite.t2 delete 0.0 end
    $w.f_droite.t2 configure -state disabled

    $w.f_droite.t1 configure -state normal -relief flat -background lightgrey
    $w.f_droite.t1 delete 0.0 end
    $w.f_droite.t1 configure -state disabled

    $w.f_gauche.t2  configure -state normal -relief flat \
	-background lightgrey
    $w.f_gauche.t2 delete 0.0 end
    $w.f_gauche.t2  configure -state disabled

    $w.f_gauche.t1 configure -state normal -relief flat -background lightgrey
    $w.f_gauche.t1 delete 0.0 end
    $w.f_gauche.t1 configure -state disabled
    bind $graph <Motion> {}
  
}

#-----------------------------------------------------------------------#
# procedure qui affiche les quantiles sur un graphique
#---------------------------------------------------------------------#
proc AfficherQuantiles2 {w val_entre_intervalle1 val_entre_intervalle2 \
			   min1 min2 max1 max2} {  
 
    set etat_bquantile2 [$w.f_topr.b_quantiles cget -relief]
    if {$etat_bquantile2 == "raised"} {
	$w.f_topr.b_quantiles configure -relief sunken
	ActiverBind2  $w $val_entre_intervalle1 $val_entre_intervalle2 \
	    $min1 $min2 $max1 $max2
    } else {
	$w.f_topmid.g1 configure -bg lightgray -relief flat
	$w.f_topr.b_quantiles configure -relief raised       
	DesactiverBind2 $w $w.f_topmid.g1
    } 
}


#------------------------------------------------------------------#
# Proc�dure qui permet l'affichage du graphique en carr�s          #
#------------------------------------------------------------------#
proc AfficherCarres2 {w num min1 min2 max1 max2 V1 V2 \
		        val_entre_intervalle1 val_entre_intervalle2 pas1 pas2} {

    global nb_valeurs
    global color
    
    #effacer le nuage de points s'il existe   
    catch { $w.f_topmid.g1 element delete  nuage_de_points} 
    #effacer les carres s'ils existent
    foreach nom [$w.f_topmid.g1 marker names] {
	catch {$w.f_topmid.g1 marker delete $nom}
    }
    catch {destroy $w.f_topl.cv_legende}

    # configurer les boutons d'affichage de graphique
    $w.f_top.b_nom2 configure -relief raised
    $w.f_top.b_nom1 configure -relief sunken

    # destruction du bouton de suivi chronologique
    catch {destroy $w.f_topr.b_suivi}

    #detruire le bouton couleur s'il existe deja
    catch {destroy $w.f_topr.b_options}
    # bouton d'options des couleurs
    button $w.f_topr.b_options 	-text "Couleurs" \
	-command "ChangerCouleur $w $num $V1 $V2 $min1 $min2 $max1 $max2 $pas1 $pas2 [list $val_entre_intervalle1] [list $val_entre_intervalle2]"
    pack $w.f_topr.b_options -pady 15

    # destruction des boutons de pas s'ils existent deja
    catch {destroy $w.f_topr.msg_pas}
    catch {destroy $w.f_topr.b_pas_plus}
    catch {destroy $w.f_topr.b_pas_moins}
    # creation des boutons de pas de discretisation
    message $w.f_topr.msg_pas -width 135 -text "Pas de discretisation"
    button $w.f_topr.b_pas_plus -text {+} \
	-command "AfficherPasAgranditVariable2 $w $num $V1 $V2 [list $val_entre_intervalle1] [list $val_entre_intervalle2]" 
    button $w.f_topr.b_pas_moins -text {-} \
	-command "AfficherPasReduitVariable2 $w $num $V1 $V2 [list $val_entre_intervalle1] [list $val_entre_intervalle2]"
    pack $w.f_topr.b_pas_plus $w.f_topr.msg_pas $w.f_topr.b_pas_moins \
	-padx 15 -pady 5

    ########################### creation du graphique ##########################
    if {($min1 == $max1) && ($min2 == $max2)} {
	set mini1 [expr $min1 - 0.5]
	set maxi1 [expr $max1 + 0.5]
	set mini2 [expr $min2 - 0.5]
	set maxi2 [expr $max2 + 0.5]
	set pas1 0.1
	set pas2 0.1
    } elseif {$min1 == $max1} {
	set mini1 [expr $min1 - 0.5]
	set maxi1 [expr $max1 + 0.5]
	set mini2 $min2
	set maxi2 $max2
	set pas1 0.1
    } elseif {$min2 == $max2} {
	set mini1 $min1
	set maxi1 $max1
	set mini2 [expr $min2 - 0.5]
	set maxi2 [expr $max2 + 0.5]
	set pas2 0.1
    } else { 
	set mini1 $min1
	set maxi1 $max1
	set mini2 $min2
	set maxi2 $max2
    }
    $w.f_topmid.g1 xaxis configure -title $V1 -min $mini1 -max $maxi1 -stepsize $pas1 
    $w.f_topmid.g1 yaxis configure -title $V2 -min $mini2 -max $maxi2 -stepsize $pas2
    
    # comptage nb par carre
    set compt {}
    for {set x $mini1} {$x<=$maxi1} {set x [expr $x+$pas1]} {
	set xp [expr $x+$pas1]	
	for {set y  $mini2} {$y<=$maxi2} {set y [expr $y+$pas2]} {
	    set yp [expr $y+$pas2]
	    lappend compt [ComptageNbPoints $val_entre_intervalle1 \
			       $val_entre_intervalle2 $x $xp $y $yp]
	}
    }

    
    # initialisation de la liste des intervalles pour chaque couleur
    set list_intervalle_couleur {}
    set nb_points_max [Max $compt]
    set pi [expr $nb_points_max/[array size color]]
    set i 0
    # ATTENTION dans le test ci-dessous ne pas comparer $i a $nb_points_max
    # dans certains cas  $pi * [array size color] est different de $nb_points_max
    # meme si a l'affichage (puts) ce sont les memes nombres
    while {$i  < [expr $pi * [array size color] - 0.001]} {
	lappend list_intervalle_couleur $i
	set i [expr $i+$pi ]
    }
    lappend list_intervalle_couleur $nb_points_max

    #affichage des carres
    set count 0 ;# nb de carre total examines
    for {set x $mini1} {$x<=$maxi1} {set x [expr $x+$pas1]} {
	set xp [expr $x+$pas1]	    
	for {set y $mini2} {$y<=$maxi2} {set y [expr $y+$pas2]} {
	    set yp [expr $y+$pas2]
	    set li [lindex $compt $count] ;# nb de point dans le carre

	    if {$li != 0} {
		for {set i 0} {$i<=[expr [llength $list_intervalle_couleur]-1]} {incr i} {
		    set l [lindex $list_intervalle_couleur $i]
		    set k [lindex $list_intervalle_couleur [expr $i+1]]
		    if {$li > $l && $li <= $k} {
			$w.f_topmid.g1 marker create polygon -name "poly$x$y" \
			    -coords {$x $y $xp $y $xp $yp $x $yp} -under 1 -fill $color($i)
			set i [llength $list_intervalle_couleur]
		    }
		}
	    }
	    incr count  	      
	} 
    }
    
    # affichage de la legende
    AfficherLegendeCarres $w  $list_intervalle_couleur

    # re affichage de la moyenne
    set etat_bmoyenne2 [$w.f_topr.b_moyenne cget -relief]
    if {$etat_bmoyenne2=="sunken"} {
	catch {DetruireMoyenneEcartType $w}
	AfficherMoyenneEcartType $w \
	    $V1 $V2 $val_entre_intervalle1  $val_entre_intervalle2
    }

    # gestion des boutons
    if {($max1 == $min1) && ($max2 == $min2)} {
	$w.f_topr.b_pas_moins configure -state disabled
	$w.f_topr.b_pas_plus configure -state disabled
	$w.f_topr.b_moyenne configure -state disabled
	$w.f_topr.b_mediane configure -state disabled
	$w.f_topr.b_quantiles configure -state disabled
   } else {
	if { $count == 1 } {
	    $w.f_topr.b_pas_moins configure -state normal
	    $w.f_topr.b_pas_plus configure -state disabled
	} else {
	    if { $count > 6400 } {
		$w.f_topr.b_pas_moins configure -state disabled
		$w.f_topr.b_pas_plus configure -state normal
	    } else {
		$w.f_topr.b_pas_moins configure -state normal
		$w.f_topr.b_pas_plus configure -state normal
	    }
	}
    }
}

#--------------------------------------------------------------------#
# Proc�dure qui permet l'affichage de la l�gende du graphe en carres #
# parametres:                                                        #
#            w :la fenetre dans laquelle on l'affiche                #
#            list_intervalle_couleur:la liste des intervalles        #
#                            pris pour les differentes couleurs      #
#--------------------------------------------------------------------#
proc AfficherLegendeCarres {w list_intervalle_couleur} {
    global color

    #detruire la legende si elle existe deja
    catch {DetruireLegendeCarres $w}
   
    # creation du canvas pour la legende
    canvas $w.f_topl.cv_legende -width 250 -height 200 -bg gray85
    pack  $w.f_topl.cv_legende -side bottom -pady 20
    $w.f_topl.cv_legende create text 90 15 -text " Nombre de valeurs par carr�s :\n Gris : 0"
    # creation de la legende
    set place 10
    for {set i 0} {$i<[expr [llength $list_intervalle_couleur]-1]} {incr i} {
	set place [expr $place+20]
	$w.f_topl.cv_legende create rect 10 $place 30 [expr $place+20] \
	    -fill $color($i)
	$w.f_topl.cv_legende create text 40 [expr $place+10] \
	    -text "] [lindex $list_intervalle_couleur $i] ; [lindex $list_intervalle_couleur [expr $i+1]] ]" \
	    -anchor w
    }
}




#-----------------------------------------------------#
# procedure qui permet de changer les couleur du graphique
#-----------------------------------------------------#
proc ChangerCouleur {w num V1 V2 min1 min2 max1 max2 pas1 pas2 \
			 val_entre_intervalle1 val_entre_intervalle2} {

    global LOGICIEL
    global color
    global palette1; global palette2; 
    # definition de la palette 3 specifique a chaque fenetre
    global palette3$num
    # nom de la palette choisie specifique a chaque fenetre
    # permet de reselectionner la palette courante a l'affichage
    global palette_choisie$num
    # valeur choisie qui ne sera effectivement memorisee dans palette_choisie$num
    # si OK et verification des entrees correcte
    global pal$num

    ############################ creation de la fenetre #######################
    catch {destroy ${w}Couleur}
    toplevel ${w}Couleur
    wm title ${w}Couleur "$LOGICIEL : Changement de palette de couleur"
    # la fenetre est composee de 3 frames :
    #          _____________________        
    #          |         |         |
    #          |    1    |    2    |
    #          |         |         |
    #          ---------------------
    #          |         3         |
    #          ---------------------
    
   
    ###Zone de commandes des boutons ok, annuler (frame 3)
    frame ${w}Couleur.f_bottom -borderwidth 3
    pack ${w}Couleur.f_bottom -side bottom -fill both -pady 5
    #Creation des boutons
    button ${w}Couleur.f_bottom.b_ok -text Ok \
	-command "OkCouleur $w $num $V1 $V2 $min1 $min2 $max1 $max2 $pas1 $pas2 [list $val_entre_intervalle1] [list $val_entre_intervalle2] "
    button ${w}Couleur.f_bottom.b_cancel \
	-text Annuler -command "destroy ${w}Couleur"
    pack ${w}Couleur.f_bottom.b_ok \
         ${w}Couleur.f_bottom.b_cancel -side left -expand 1 
    # Separateur
    frame ${w}Couleur.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack ${w}Couleur.f_sep -side bottom -fill both -pady 5
    
    ###Zone d'affichage des boutons de selection de palettes
    frame ${w}Couleur.f_aff1  
    pack ${w}Couleur.f_aff1 -padx 10 -pady 10 \
	-side left -fill both
    # bouton de selection de la palette 1  
    radiobutton ${w}Couleur.f_aff1.cb_palette1 \
	-text "Palette 1" -variable pal$num  -value palette1
    pack ${w}Couleur.f_aff1.cb_palette1 -side top
    ${w}Couleur.f_aff1.cb_palette1 select
    # bouton de selection de la palette 2 
    radiobutton ${w}Couleur.f_aff1.cb_palette2  \
	-text "Palette 2" -variable pal$num  -value palette2
    pack ${w}Couleur.f_aff1.cb_palette2 -side top -pady 40
    # bouton de selection de la palette 3
    radiobutton ${w}Couleur.f_aff1.cb_palette3 \
	-text "Palette 3" -variable pal$num -value palette3$num
    pack ${w}Couleur.f_aff1.cb_palette3 -side top 

    ###Zone d'affichage des noms et des couleurs de chaque palette
    frame ${w}Couleur.f_aff2

    pack ${w}Couleur.f_aff2 -padx 10 -pady 10 -side left
      # affichage des noms des couleurs de la palette 1
    entry ${w}Couleur.f_aff2.e_palette1 -width 50 
    ${w}Couleur.f_aff2.e_palette1 insert 0 $palette1 
    ${w}Couleur.f_aff2.e_palette1 configure -state disabled   
    pack ${w}Couleur.f_aff2.e_palette1 
    # affichage des couleurs  de la palette1
    canvas ${w}Couleur.f_aff2.c_couleurs1 -height 30
    pack ${w}Couleur.f_aff2.c_couleurs1 -side top -anchor e \
	-pady 5
    set place 0
    for {set i 0} {$i<[llength $palette1]} {incr i} {
	set place [expr $place+20]
	${w}Couleur.f_aff2.c_couleurs1 create rect \
	     $place 10 [expr $place+20] 30 \
	    -fill [lindex $palette1 $i]
    }
    # affichage des noms des couleurs de la palette 2
    entry ${w}Couleur.f_aff2.e_palette2 -width 50
    ${w}Couleur.f_aff2.e_palette2 insert 0 $palette2 
    ${w}Couleur.f_aff2.e_palette2 configure -state disabled
    pack ${w}Couleur.f_aff2.e_palette2 
    # affichage des couleurs  de la palette2
    canvas ${w}Couleur.f_aff2.c_couleurs2 -height 30
    pack ${w}Couleur.f_aff2.c_couleurs2 -side top -anchor e \
	-pady 5
    set place 0
    for {set i 0} {$i<[llength $palette2]} {incr i} {
	set place [expr $place+20]
	${w}Couleur.f_aff2.c_couleurs2 create rect \
	     $place 10 [expr $place+20] 30 \
	    -fill [lindex $palette2 $i]
    }   
    # affichage entree de la palette 3
    entry ${w}Couleur.f_aff2.e_palette3 -width 50 
    ${w}Couleur.f_aff2.e_palette3 insert 0 [set palette3$num]
    pack ${w}Couleur.f_aff2.e_palette3 


    set pal$num [set palette_choisie$num]
}

#---------------------------------------------------------------------#
# Changement de la couleur 
#----------------------------------------------------------------------#
proc OkCouleur {w num V1 V2 min1 min2 max1 max2 pas1 pas2 \
		    val_entre_intervalle1 val_entre_intervalle2 } {
    global LOGICIEL
    global color
    global palette1; global palette2; global palette3$num
    global palette_choisie$num
    global pal$num

    if {[string compare [set pal$num] palette3$num]==0 \
	    && [${w}Couleur.f_aff2.e_palette3 get]==""} {
	CreerFenetreDialogue $LOGICIEL error \
	    "La palette 3 a �t� choisie sans sp�cifier de couleurs"
    } else {
	set palette_choisie$num [set pal$num]

	# initialisation eventuelle de palette3
	if {[string compare [set palette_choisie$num] palette3$num]==0} {
	    set palette3$num [${w}Couleur.f_aff2.e_palette3 get]
	}

	# initialisation de color
	set k [array size color]
	for {set j 0} {$j<$k} {incr j} {
	    unset color($j) 
	} 
	for {set j 0} {$j<[llength [set [set palette_choisie$num]]]} {incr j} {
	    set color($j) [lindex [set [set palette_choisie$num]] $j]
	}

	# reaffichage des carres
	destroy ${w}Couleur
	AfficherCarres2 $w $num $min1 $min2 $max1 $max2 $V1 $V2 \
	    $val_entre_intervalle1 $val_entre_intervalle2 $pas1 $pas2
    } 
}




#------------------------------------------------------#
# Agrandir le pas de discretisation
#-------------------------------------------------------#
proc AfficherPasAgranditVariable2 {w num V1 V2 val_entre_intervalle1 val_entre_intervalle2} {
    global pas  
    global min
    global max

    set pas(${num}1) [PasAgrandit $pas(${num}1)]
    set pas(${num}2) [PasAgrandit $pas(${num}2)]

    AfficherCarres2 ${w} $num\
	$min(${num}1) $min(${num}2) $max(${num}1) $max(${num}2) $V1 $V2 \
	$val_entre_intervalle1 $val_entre_intervalle2 \
	$pas(${num}1)  $pas(${num}2)  
}

#-------------------------------------------------------#
# Reduire le pas de discretisation
#-------------------------------------------------------#
proc AfficherPasReduitVariable2 {w num V1 V2 val_entre_intervalle1 val_entre_intervalle2} {

    global pas   
    global min
    global max

    set pas(${num}1) [PasReduit $pas(${num}1)]
    set pas(${num}2) [PasReduit $pas(${num}2)]
    
    AfficherCarres2 ${w}  $num\
	$min(${num}1) $min(${num}2) $max(${num}1) $max(${num}2) $V1 $V2 \
	$val_entre_intervalle1 $val_entre_intervalle2 \
	$pas(${num}1)  $pas(${num}2)  
}



#------------------------------------------------------------------#
# Proc�dure qui permet l'affichage du nuage de points              #
#------------------------------------------------------------------#
proc AfficherNuage2 {w min1 min2 max1 max2 V1 V2 val_entre_intervalle1 \
			 val_entre_intervalle2 pas1 pas2} {
     
    ######################## destruction du graphique en carres s'il exite ####
    # effacer le graphique en carr�s s'il existe 
    foreach nom [$w.f_topmid.g1 marker names] {
	catch {$w.f_topmid.g1 marker delete $nom}
    }
    catch {destroy $w.f_topl.cv_legende }
    #effacer le nuage de points s'il existe   
    catch { $w.f_topmid.g1 element delete  nuage_de_points} 

    # gestion des boutons
    $w.f_top.b_nom2 configure -relief sunken
    $w.f_top.b_nom1 configure -relief raised
    # destruction des boutons de pas
    catch {destroy $w.f_topr.msg_pas}
    catch {destroy $w.f_topr.b_pas_plus}
    catch {destroy $w.f_topr.b_pas_moins}
    # destruction du bouton couleurs
    catch {destroy $w.f_topr.b_options}
    # destruction du bouton de suivi s'il existe deja
    catch {destroy $w.f_topr.b_suivi}
    #creation du bouton de suivi
    button $w.f_topr.b_suivi -text "Chronologie" \
	-command "AfficherChronologie $w [list $val_entre_intervalle1] [list $val_entre_intervalle2]"
    pack  $w.f_topr.b_suivi -pady 15 

    #creer le nuage de points   
    if {$min1 == $max1 && $min2 == $max2} {
	set mini1 [expr $min1 - 0.5]
	set maxi1 [expr $max1 + 0.5]
	set mini2 [expr $min2 - 0.5]
	set maxi2 [expr $max2 + 0.5]
	set pas1 0.1
	set pas2 0.1
    } elseif {$min1 == $max1} {
	set mini1 [expr $min1 - 0.5]
	set maxi1 [expr $max1 + 0.5]
	set mini2 $min2
	set maxi2 $max2
	set pas1 0.1
    } elseif {$min2 == $max2} {
	set mini1 $min1
	set maxi1 $max1
	set mini2 [expr $min2 - 0.5]
	set maxi2 [expr $max2 + 0.5]
	set pas2 0.1
    } else { 
	set mini1 $min1
	set maxi1 $max1
	set mini2 $min2
	set maxi2 $max2
    }
    $w.f_topmid.g1 xaxis configure -title $V1 -min $mini1 -max $maxi1 -stepsize $pas1  
    $w.f_topmid.g1 yaxis configure -title $V2 -min $mini2 -max $maxi2 -stepsize $pas2 	      	    

    $w.f_topmid.g1 element create nuage_de_points -label "" -linewidth 0 \
	-xdata $val_entre_intervalle1 -ydata $val_entre_intervalle2 

    # re-affichage de la moyenne
    set etat_bmoyenne2 [$w.f_topr.b_moyenne \cget -relief]
    if {$etat_bmoyenne2=="sunken"} {
	AfficherMoyenneEcartType $w $V1 $V2 $val_entre_intervalle1 \
	    $val_entre_intervalle2 
    }   

    # gestion des boutons
    if {($max1 == $min1) && ($max2 == $min2)} {
	$w.f_topr.b_suivi configure -state disabled
    }
}



#------------------------------------------------------------------#
# Proc�dure qui permet l'affichage du suivi sur le nuage de points #
#------------------------------------------------------------------#
proc AfficherChronologie {w val_entre_intervalle1 val_entre_intervalle2} { 
   
    if {[$w.f_topr.b_suivi cget -relief]=="raised"} {
	$w.f_topr.b_suivi configure -relief sunken
	#etablir le suivi
	$w.f_topmid.g1 element delete nuage_de_points 
	$w.f_topmid.g1 element create nuage_de_points -label "" \
	    -linewidth 1 -xdata $val_entre_intervalle1 \
	    -ydata $val_entre_intervalle2 
    } else {
	$w.f_topr.b_suivi configure -relief raised
	#annuler le suivi
	$w.f_topmid.g1 element configure nuage_de_points \
	    -linewidth 0
    }	
}


