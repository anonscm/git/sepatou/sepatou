/*
** Copyright (C) 2005  Marie-Jos�e Cros <cros@toulouse.inra.fr>
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
** 
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software 
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/******************************************************************************
fichier : Tcl_c.c
contenu : declaration de fonctions C appelables comme procedures Tcl
          et de la procedure appelee lors du chargement du package
******************************************************************************
Realisation de la bibliotheque libTcl_c.so :
          gcc -fpic -shared Tcl_c.c -o libTcl_c.so
Utilisation de la bibliotheque libTcl_c.so :
          tixwish
                % load /homes/.../libTcl_c.so 
                % Max 1 2
                2.0
******************************************************************************/

#include <tcl8.4/tcl.h>
#include <math.h>
#include <stdlib.h>

// ---------------------------------------------------------------------
// fonction qui calcule le maximum d'une liste
// argument = 1 liste (les valeurs de la variable)
// evaluation = un nombre (le maximum des valeurs)
// ---------------------------------------------------------------------
int Max (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error, n, i;
  Tcl_Obj **T;
  double f;
  double *F;
  double max=0;
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  ///////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 2 ) {
    Tcl_WrongNumArgs(interp, 1, objv, "?liste?");
    return TCL_ERROR;
  };
  // lecture de la liste
  error = Tcl_ListObjGetElements( interp, objv[1], &n, &T );
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((F=(double *)malloc(n*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation","");
  for (i=0; i<n; i++) {
    error = Tcl_GetDoubleFromObj(interp, T[i], &f);
    if (error != TCL_OK)
      return error;
    F[i] = f;
  };

  //////////////// calcul 
  max=F[0];
  for (i=0; i<n; i++) { 
    if (F[i]>max) 
      max=F[i];
  };

  //////////////// ecriture du resultat
  Tcl_SetDoubleObj(resultPtr, max); 

  free(F);
  return TCL_OK;    
    
}

// ---------------------------------------------------------------------
// fonction qui calcule le minimum d'une liste
// argument = 1 liste (les valeurs de la variable)
// evaluation = un nombre (le minimum des valeurs)
// ---------------------------------------------------------------------
int Min (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error, n, i;
  Tcl_Obj **T;
  double f;
  double *F;
  double min;
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

 ///////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 2 ) {
    Tcl_WrongNumArgs(interp, 1, objv, "?liste?");
    return TCL_ERROR;
  };
  // lecture de la liste
  error = Tcl_ListObjGetElements( interp, objv[1], &n, &T );
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((F=(double *)malloc(n*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation","");
  for (i=0; i<n; i++) {
    error = Tcl_GetDoubleFromObj(interp, T[i], &f);
    if (error != TCL_OK)
      return error;
    F[i] = f;
  };

  //////////////////// calcul 
  min=F[0];
  for (i=0; i<n; i++) { 
       if (F[i]<min) 
  	 min=F[i];
  };
  
  //////////////////// ecriture du resultat
  Tcl_SetDoubleObj(resultPtr, min); 

  free(F);
  return TCL_OK;    
}


// ---------------------------------------------------------------------
// fonction qui calcule le nombre de valeurs d'une variable dans un intervalle 
// arguments = 1 liste (les valeurs de la variable), 
//             3 nombres (1 pas, le min, le max)
// evaluation = une liste de nombre de valeurs 
// ---------------------------------------------------------------------
int ComptageNbValeurs (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error, n, i;
  Tcl_Obj **T;
  double f;
  double *F;
  double x=0;
  double max=0;
  double min=0;
  double pas=0;
  int count;
  int nb;
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);
  
  ///////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 5) {
    Tcl_WrongNumArgs(interp, 1, objv, "?i? ?j? ?k? ?liste?");
    return TCL_ERROR;
  };
  // lecture de la liste
  error = Tcl_ListObjGetElements( interp, objv[4], &n, &T );
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((F=(double *)malloc(n*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation","");
  for (i=0; i<n; i++) {
    error = Tcl_GetDoubleFromObj(interp, T[i], &f);
    if (error != TCL_OK)
      return error;
    F[i] = f;
  };
  // lecture du min et du max
  error = Tcl_GetDoubleFromObj(interp, objv[1], &min);
  if (error != TCL_OK) 
    return error;
  error = Tcl_GetDoubleFromObj(interp, objv[2], &max);
  if (error != TCL_OK) 
    return error;
  // lecture du pas
  error = Tcl_GetDoubleFromObj(interp, objv[3], &pas);
  if (error != TCL_OK) 
    return error;
  
  //////////////////// calcul et ecriture du resultat
  nb = 0;
  for (x=min; x<=max; x = min + nb*pas ) {
    count=0;
    for (i=0; i<n; i++) 
      if ( (F[i] >=  min + nb*pas) && 
	   ( F[i]< min + (nb+1)*pas) ) 
	count++;
    
    error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewIntObj(count));
    if (error!=TCL_OK)
      return error;
    nb++;
  };
  
  free(F);
  return TCL_OK;
}

// ---------------------------------------------------------------------
// fonction qui calcule le milieu des  intervalles 
// arguments = 1 liste (les valeurs de la varible),
//             3 nombres (1 pas, le min, le max)
// evaluation = une liste donnant les milieu des intervalles 
// ---------------------------------------------------------------------
int Intervalles (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error,n,i;
  Tcl_Obj **T;
  double f;
  double *F;
  double x=0;
  double max=0;
  double min=0;
  double pas=0;
  double milieu_intervalle =0 ;
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  ///////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 5) {
    Tcl_WrongNumArgs(interp, 1, objv, "?i? ?j? ?k? ?liste?");
    return TCL_ERROR;
  };
  // lecture de la liste
  error = Tcl_ListObjGetElements( interp, objv[4], &n, &T );
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((F=(double *)malloc(n*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation","");
  for (i=0; i<n; i++) {
    error = Tcl_GetDoubleFromObj(interp, T[i], &f);
    if (error != TCL_OK)
      return error;
    F[i] = f;
  };
  // lecture du min et du max
  error = Tcl_GetDoubleFromObj(interp, objv[1], &min);
  if (error != TCL_OK) 
    return error;
  error = Tcl_GetDoubleFromObj(interp, objv[2], &max);
  if (error != TCL_OK) 
    return error;
  // lecture du pas
  error = Tcl_GetDoubleFromObj(interp, objv[3], &pas);
  if (error != TCL_OK) 
    return error;
  
  //////////////////// calcul et ecriture du resultat 
   for (x=min; x<=max; x=pas+x ) {
     milieu_intervalle = x+pas/2;
     error=Tcl_ListObjAppendElement(interp, resultPtr, 
				    Tcl_NewDoubleObj(milieu_intervalle));
     if (error!=TCL_OK)
       return error;
   };
   
   free(F);
   return TCL_OK;
}

//----------------------------------------------------------------------------
// fonction qui calcule les quantiles de la visualisation histogramme
// arguments : 1 liste (les valeurs de la variable), 
//             1 nombre (abscisse de l'intersection du crosshairs)   
// evaluation : 1 liste de 2 quantiles
//---------------------------------------------------------------------------- 
int CalculerQuantiles1 (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error,n,i;
  Tcl_Obj **T;
  double f;
  double *F;
  double x=0;
  double q1,q2;
  double count1=0.0;
  double count2=0.0;
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  ///////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 3) {
    Tcl_WrongNumArgs(interp, 1, objv, " ?liste? ?i?");
    return TCL_ERROR;
  };
  // lecture de la liste
  error = Tcl_ListObjGetElements( interp, objv[1], &n, &T );
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((F=(double *)malloc(n*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation","");
  for (i=0; i<n; i++) {
    error = Tcl_GetDoubleFromObj(interp, T[i], &f);
    if (error != TCL_OK)
      return error;
    F[i] = f;
  };
  // lecture de l'abscisse
  error = Tcl_GetDoubleFromObj(interp, objv[2], &x);
  if (error != TCL_OK) 
    return error;

  //////////////////// calcul (x pass� cot� sup)
   for (i=0; i<n; i++) {
      if (F[i]>=x) 
	count1++;
      else count2++;
    };
   q1=count1*100/n;
   q2=count2*100/n;

   // ecriture du resultat
   error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(q1));
   if (error!=TCL_OK)
     return error;
   error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(q2));
   if (error!=TCL_OK)
     return error;

   free(F);
   return TCL_OK;
}

//----------------------------------------------------------------------------
// fonction qui calcule les quantiles de la visualisation bidimensionnelle
// arguments : 2 listes (les valeurs de v1 et les valeurs de V2)
//             2 nombres (coordonnees de l'intersection du crosshairs)   
// evaluation : 1 liste de 4 quantiles
//---------------------------------------------------------------------------- 
int CalculerQuantiles2 (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error,n,m,i,j;
  Tcl_Obj **T, **K;
  double f,g;  
  double *F,*G; 
  double x=0;
  double y=0;
  double q1,q2,q3,q4; 
  double countx=0;
  double countx1=0;
  double countx2=0;
  double county1=0;
  double county2=0;
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  //////////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 5) {
    Tcl_WrongNumArgs(interp, 1, objv, " ?liste? ?liste? ?i? ?j?");
    return TCL_ERROR;
  };

  // lecture de la liste X
  error = Tcl_ListObjGetElements( interp, objv[1], &n, &T );
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((F=(double *)malloc(n*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation","");
  for (i=0; i<n; i++) {
    error = Tcl_GetDoubleFromObj(interp, T[i], &f);
    if (error != TCL_OK)
      return error;
    F[i] = f;
  };

  // lecture de la liste Y
  error = Tcl_ListObjGetElements( interp, objv[2], &m, &K );
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((G=(double *)malloc(m*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation","");
  for (i=0; i<m; i++) {
    error = Tcl_GetDoubleFromObj(interp, K[i], &g);
    if (error != TCL_OK)
      return error;
    G[i] = g;
  };

  // lecture de l'abscisse
  error = Tcl_GetDoubleFromObj(interp, objv[3], &x);
  if (error != TCL_OK) 
    return error;
  // lecture de l'ordonnee
  error = Tcl_GetDoubleFromObj(interp, objv[4], &y);
  if (error != TCL_OK) 
    return error;

  //////////////////// calcul (x et y pass� cote sup)
  // on fait ici le test d'inf�riorit� de n ou m car on peut 
  // avoir un nombre diff�rent lorsqu'on diminue l'intervalle
  if (n<=m) {
    for (i=0; i<n; i++) { 
      countx++;
      if ((F[i]>=x)&& (G[i]>=y))	 
	countx1++;
      else
	if ((F[i]<x)&& (G[i]<y))
	  county2++;
	else
	   if ((F[i]<x)&&(G[i]>=y))
	     county1++;       
	   else 
	     countx2++;      
    };
  } else {
      for (i=0; i<m; i++) { 
	countx++;
	if ((F[i]>=x)&& (G[i]>=y))	 
	  countx1++;
	else
	  if ((F[i]<x)&& (G[i]<y))
	    county2++;
	  else
	    if ((F[i]<x)&&(G[i]>=y))
	      county1++;       
	    else 
	      countx2++;      
      };
    };

   q1=countx1*100/countx;
   q2=countx2*100/countx;
   q3=county1*100/countx;
   q4=county2*100/countx;
  
   ///////// ecriture du resultat
   error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(q1));
   if (error!=TCL_OK)
     return error;
   error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(q2));
   if (error!=TCL_OK)
     return error;
   error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(q3));
   if (error!=TCL_OK)
     return error;
   error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(q4));
   if (error!=TCL_OK)
     return error;

   free(F);
   free(G);
   return TCL_OK;
}

// -------------------------------------------------------------------------
// procedure qui calcule tous les quartiles , la mediane , la moyenne
// et l'ecart type d'une variable
// argument : 1 liste (les valeurs de la variable)
// evaluation 1 liste (1er quartile mediane 2�me quartile moyenne ecart type)
// --------------------------------------------------------------------------
int CalculerStatistiquesElementaires (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error,n,i,j,l; 
  Tcl_Obj **T;
  double f;
  double *F;  
  double q1,q3,k,med,var,moy,p,decimale; 
  double probs[5];
  double quantiles[5];
  int low[5]; 
  int high[5];
  double order[5];
  double count=0;
  double sum=0;  
  double stdr=0;
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  ///////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 2) {
    Tcl_WrongNumArgs(interp, 1, objv, "?liste?");
    return TCL_ERROR;
  };
  // lecture de la liste
  error = Tcl_ListObjGetElements( interp, objv[1], &n, &T);
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((F=(double *)malloc(n*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation","");
  for (i=0; i<n; i++) {
    error = Tcl_GetDoubleFromObj(interp, T[i], &f);
    if (error != TCL_OK)
      return error;
    F[i] = f;
  };

  //////////////////// calcul 
  // calcul de la moyenne et de l'ecart type
  for (i=0; i<n; i++) { 
    sum=sum+F[i];
  };
  moy=sum/n;
  for (i=0; i<n; i++) {
    stdr=stdr+(F[i]-moy)*(F[i]-moy);
  };
  var=sqrt(stdr/n);

  // calcul des quartiles
  // mise en ordre croissant des valeurs
    for (i=0; i<n; i++) {  
      for (j=i; j<n; j++) {
	if (F[i]>F[j]) {
	  p=F[i];
	  F[i]=F[j];
	  F[j]=p;
	};
      };   
    };
    // calcul des quantiles
    probs[0]=0;
    probs[1]=0.25;
    probs[2]=0.50;
    probs[3]=0.75; 
    probs[4]=1;
    for (i=0;i<=4;i++) {
      order[i]=1+(n-1)*probs[i];
      decimale=order[i]-floor(order[i]);
      if (decimale!=0.0) 
	{
	  low[i]=floor(order[i]);
	  if (low[i]<1) {
	    low[i]=1;
	  };
	  high[i]=low[i]+1;
	  if (high[i]>n) {
	    high[i]=n;
	  };
	  quantiles[i]=(1-decimale)*F[low[i]-1]+decimale*F[high[i]-1];
      }
      else {
	quantiles[i]=F[(int)order[i]-1];
      };
    };
    q1=quantiles[1];
    med=quantiles[2];
    q3=quantiles[3];  
 
    //////////// ecriture des resultats
    error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(q1));
    if (error!=TCL_OK)
      return error;
    error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(med));
    if (error!=TCL_OK)
      return error;
    error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(q3));
    if (error!=TCL_OK)
      return error;
    error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(moy));
    if (error!=TCL_OK)
      return error;
    error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(var));
    if (error!=TCL_OK)
      return error;
 
   free(F);
   return TCL_OK;
}

// -------------------------------------------------------------------------
// procedure qui donne les valeurs exceptionnelles pour une boite a moustache
// argument : une liste 2 nombres (le 1er et le 3eme quartile)
// evaluation 1 liste de valeurs exceptionnelles (peut etre vide)
// --------------------------------------------------------------------------
int CalculerValeursExceptionnelles  (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error,n,i; 
  Tcl_Obj **T;
  double f;
  double *F;  
  double q1,q3,iqr,a,b;  
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  ///////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 4) {
    Tcl_WrongNumArgs(interp, 1, objv, "?i? ?j? ?liste? ?i? ?j?");
    return TCL_ERROR;
  }
  // lecture de la liste
  error = Tcl_ListObjGetElements( interp, objv[3], &n, &T);
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((F=(double *)malloc(n*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation","");
  for (i=0; i<n; i++) {
    error = Tcl_GetDoubleFromObj(interp, T[i], &f);
    if (error != TCL_OK)
      return error;
    F[i] = f;
  };
  //lecture du 1er quartile
  error = Tcl_GetDoubleFromObj(interp, objv[1], &q1);
  if (error != TCL_OK) 
    return error;
  //lecture du 3em quartile
  error = Tcl_GetDoubleFromObj(interp, objv[2], &q3);
  if (error != TCL_OK) 
    return error;

  /////////////////// calcul 
  iqr=q3-q1;
  a=q1-(1.5*iqr);
  b=q3+(1.5*iqr);

  for (i=0; i<n; i++) {
    if ((F[i]<a)||(F[i]>b)) 
      {
      error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(F[i]));
      if (error!=TCL_OK)
	return error;
      };
  };    
 
  free(F);
  return TCL_OK;
}


// -------------------------------------------------------------------------
// procedure qui calcule les ordonnees de l'ellipse partie haute
// argument : 2 nombres (1 moyenne et 1 ecart type)
// evaluation 1 liste des ordonn�es de la partie haute de l'ellipse
// --------------------------------------------------------------------------
int CalculerCoordonneesEllipseHaut (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error;
  double moy1,moy2,et1,et2,x,y;
 // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  //////////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 3) {
    Tcl_WrongNumArgs(interp, 1, objv, "?i? ?j? ");
    return TCL_ERROR;
  };
  // lecture de la moyenne de la 2�me variable
  error = Tcl_GetDoubleFromObj(interp, objv[1], &moy2);
  if (error != TCL_OK) 
    return error;
  // lecture de l'ecart type de la 2�me variable 
  error = Tcl_GetDoubleFromObj(interp, objv[2], &et2);
  if (error != TCL_OK) 
    return error;
 
  //////////////////// calcul et ecriture du resultat
  for (x=0;x<=M_PI ;x=x+(M_PI/20)) 
    {
      y=moy2+et2*sqrt(1-cos(x)*cos(x));
      error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(y));
      if (error!=TCL_OK)
	return error;
    };
  
   return TCL_OK;
}


// -------------------------------------------------------------------------
// procedure qui calcule les ordonnees de l'ellipse partie basse
// argument : 2 nombres (1 moyenne et 1 ecart type)
// evaluation 1 liste des ordonn�es de la partie basse de l'ellipse 
// --------------------------------------------------------------------------
int CalculerCoordonneesEllipseBas (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error;
  double moy1,moy2,et1,et2,x,y;
 // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  //////////////////// lecture des arguments 
  // test du nombre d'argument
  if ( objc != 3) {
    Tcl_WrongNumArgs(interp, 1, objv, "?i? ?j?");
    return TCL_ERROR;
  }
  // lecture de la moyenne de la 2�me variable
  error = Tcl_GetDoubleFromObj(interp, objv[1], &moy2);
  if (error != TCL_OK) 
    return error;
  // lecture de l'ecart type de la 2�me variable 
  error = Tcl_GetDoubleFromObj(interp, objv[2], &et2);
  if (error != TCL_OK) 
    return error;
 
  //////////////////// calcul et ecriture du resultat ////////////////////////
  // calcul et lecture des coordonnees 
  for (x=M_PI;x<=2*M_PI ;x=x+(M_PI/20)) {
      y=moy2-et2*sqrt(1-cos(x)*cos(x));
      error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(y));
      if (error!=TCL_OK)
	return error;
  };
 
  return TCL_OK;
}


// -------------------------------------------------------------------------
// procedure qui calcule les abscisses de l'ellipse
// argument : 2 nombres (1 moyenne et 1 ecart type)
// evaluation 1 liste des abscisses de l'ellipse
// --------------------------------------------------------------------------
int CalculerAbscisseEllipse (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error;
  double moy1,moy2,et1,et2,x,t;
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  //////////////////// lecture des arguments /////////////////////////////
  // test du nombre d'argument
  if ( objc != 3) {
    Tcl_WrongNumArgs(interp, 1, objv, "?i? ?j? ");
    return TCL_ERROR;
  };
  // lecture de la moyenne de la 1�re variable
  error = Tcl_GetDoubleFromObj(interp, objv[1], &moy1);
  if (error != TCL_OK) 
    return error;
  // lecture de l'ecart type de la 1�re variable 
  error = Tcl_GetDoubleFromObj(interp, objv[2], &et1);
  if (error != TCL_OK) 
    return error;
 
  //////////////////// calcul et ecriture du resultat 
  for (t=0 ;t<=M_PI_2 ;t=t+(M_PI_2/10)) {
      x=moy1-et1*sqrt(1-sin(t)*sin(t));
      error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(x));
      if (error!=TCL_OK)
	return error;
  };
  for (t=M_PI_2+(M_PI_2/10) ;t<=M_PI ;t=t+(M_PI_2/10)) {
    x=moy1+et1*sqrt(1-sin(t)*sin(t));
    error=Tcl_ListObjAppendElement(interp, resultPtr, Tcl_NewDoubleObj(x));
    if (error!=TCL_OK)
      return error;
  };
 
  return TCL_OK;
}


// ---------------------------------------------------------------------
// fonction qui calcule le nombre de points dans un carre
// arguments = 2 listes (les valeurs des deux variables),
//          4 nombres (les bornes des intervalles)
// evaluation = un nombre de points 
// ---------------------------------------------------------------------
int ComptageNbPoints (ClientData clientData, Tcl_Interp *interp,
	 int objc, Tcl_Obj *CONST objv[])
{
  Tcl_Obj *resultPtr;
  int error, n, m, i;
  Tcl_Obj **T, **K;
  double f,g,a1,a2,b1,b2;
  double count=0;
  double *F;
  double *G;
  // declaration du pointeur de resultat
  resultPtr = Tcl_GetObjResult(interp);

  ///////////////// lecture des arguments //////////////////////
  // test du nombre d'argument
  if ( objc != 7) {
    Tcl_WrongNumArgs(interp, 1, objv, "?liste? ?liste? ?i? ?j? ?k? ?l?");
    return TCL_ERROR;
  };
  // lecture de la liste de la 1ere variable
  error = Tcl_ListObjGetElements( interp, objv[1], &n, &T );
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((F=(double *)malloc(n*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation",n);
  for (i=0; i<n; i++) {
    error = Tcl_GetDoubleFromObj(interp, T[i], &f);
    if (error != TCL_OK)
      return error;
    F[i] = f;
  };
 // lecture de la liste de la 2eme variable
  error = Tcl_ListObjGetElements( interp, objv[2], &m, &K );
  if (error != TCL_OK)
    return error;
  // allocation dynamique
  if ((G=(double *)malloc(m*sizeof(double)))==(double *)NULL)
    sprintf (interp->result, "erreur d'allocation",m);
  for (i=0; i<m; i++) {
    error = Tcl_GetDoubleFromObj(interp, K[i], &g);
    if (error != TCL_OK)
      return error;
    G[i] = g;
  };
  // lecture des bornes du 1er intervalle
  error = Tcl_GetDoubleFromObj(interp, objv[3], &a1);
  if (error != TCL_OK) 
    return error;
  error = Tcl_GetDoubleFromObj(interp, objv[4], &b1);
  if (error != TCL_OK) 
    return error;
  // lecture des bornes du 2eme intervalle
  error = Tcl_GetDoubleFromObj(interp, objv[5], &a2);
  if (error != TCL_OK) 
    return error;
  error = Tcl_GetDoubleFromObj(interp, objv[6], &b2);
  if (error != TCL_OK) 
    return error; 

  //////////////////// calcul 
  if (n<=m) {
   for (i=0; i<n; i++ ) {
     if (F[i]>=a1 && F[i]<b1 && G[i]>=a2 && G[i]<b2)
	    count++;  
   };
  } else {
    for (i=0; i<m; i++ ) { 
      if (F[i]>=a1 && F[i]<b1 && G[i]>=a2 && G[i]<b2)
	    count++;  
   };
  };

  /////////////////// ecriture du resultat
  Tcl_SetDoubleObj(resultPtr, count);

  free(F);
  free(G);
  return TCL_OK;
}



 
// ---------------------------------------------------------------------
// procedure appelee lors du chargement du package : <package>_Init
// ---------------------------------------------------------------------
int Tcl_c_Init (Tcl_Interp *interp)
{
  Tcl_CreateObjCommand(interp, "Max", 
		       Max,
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "Min", 
		       Min,
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "ComptageNbValeurs", 
		       ComptageNbValeurs, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "Intervalles", 
		       Intervalles, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "CalculerQuantiles1", 
		       CalculerQuantiles1, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "CalculerQuantiles2", 
		       CalculerQuantiles2, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "CalculerStatistiquesElementaires", 
		       CalculerStatistiquesElementaires, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL); 
  Tcl_CreateObjCommand(interp, "CalculerValeursExceptionnelles", 
		       CalculerValeursExceptionnelles, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "CalculerCoordonneesEllipseHaut", 
		       CalculerCoordonneesEllipseHaut, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "CalculerCoordonneesEllipseBas",
		       CalculerCoordonneesEllipseBas, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "CalculerAbscisseEllipse", 
		       CalculerAbscisseEllipse, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  Tcl_CreateObjCommand(interp, "ComptageNbPoints", 
		       ComptageNbPoints, 
		       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
  return TCL_OK;
};


