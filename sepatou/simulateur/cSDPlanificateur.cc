// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSDPlanificateur.cc
contenu : definition des methodes de la classe cSDPlanificateur
******************************************************************************/

#include <fstream>
#include <string>
#include <vector>

#include "pat.h"

#include "cSDPlanificateur.h"

#include "cSDOperatoire.h"
#include "cReglePlanification.h"
#include "cPlan.h"
#include "cSimulateur.h"
#include "cTemps.h"
#include "cResultat.h"
#include "cEvenement.h"
#include "cAnomalie.h"

class cEnsembleParcelles;

extern cSDOperatoire SDOperatoire;

extern cSimulateur Simulateur;
extern cTemps Temps;
extern cResultat Resultat;
extern cEnsembleParcelles EnsembleParcellesVide;
extern cEvenement Evt_PB_SDOperatoire;

extern void CreerReglesPlanification(void);


// ============================================================================
// InitElementsInvariants :initialisation des elts invariants 
//                         lors des simulations
// ============================================================================
void cSDPlanificateur::InitElementsInvariants(void)
{
ifstream fStrategie;
string CheminStrategie = CHEMIN_STRATEGIE_TXT;
string commentaires;

// lecture de Strategie.txt 
fStrategie.open (CheminStrategie.c_str(), ios::in);
if (!fStrategie)
 throw (new cAnomalie ("Dans la methode cSDPlanificateur.InitElementsInvariants, probleme pour ouvrir le fichier strategie.txt : " + CheminStrategie ) );

fStrategie >> commentaires >> commentaires >> NomStrategie; 
fStrategie >> commentaires >> commentaires >> J2emeApportAzote;
fStrategie >> commentaires >> commentaires >> M2emeApportAzote;
fStrategie >> commentaires >> commentaires >> J3emeApportAzote;
fStrategie >> commentaires >> commentaires >> M3emeApportAzote;
fStrategie.close();

CreerReglesPlanification();
}


// ============================================================================
// Init :initialisation des elts au jour de debut de simulation
// ============================================================================
void cSDPlanificateur::InitElementsVariants(void)
{
Jour1erApportAzote = Temps.NoJour(Simulateur.JourDebut, Simulateur.MoisDebut);
Jour2emeApportAzote = Temps.NoJour(J2emeApportAzote,M2emeApportAzote);
Jour3emeApportAzote = Temps.NoJour(J3emeApportAzote,M3emeApportAzote);

for (int i=0; i<NB_VAR_SDP; i++)
  {
   iVariable[i] = 0;
   fVariable[i] = 0.0;
   pVariable[i] = NULL;
   eVariable[i] = EnsembleParcellesVide;
   sVariable[i] = "";
  };

// declaration de toutes les regles actives
for (int k=lRegles.size()-1; k>=0; k--)
  lRegles[k]->Active = ACTIVE;

InitlEvtADetecter();

ModifPlans = DATE_INCONNUE;
}


// ============================================================================
// LibererMemoire : liberer memoire dynamique
// ============================================================================
void cSDPlanificateur::LibererMemoire(void)
{
int k,n;

n = lRegles.size();
for (k=n-1; k>=0; k--)
  {
  delete lRegles[k];
  lRegles.pop_back();
  };

 lEvtADetecter.clear();
}


// ============================================================================
// AjouterRegle : ajouter une regle
// ============================================================================
void cSDPlanificateur::AjouterRegle(cReglePlanification* r)
{
lRegles.push_back(r);
}


// ============================================================================
// InitlEvtADetecter : initialisation de la liste des evts a detecter 
//                     en fonction de lRegles
// ============================================================================
void cSDPlanificateur::InitlEvtADetecter(void)
{
lEvtADetecter.clear();

for (int i=lRegles.size()-1; i>=0; i--)
  if ( lRegles[i]->Active == ACTIVE )
      lEvtADetecter.push_back( lRegles[i]->Evenement );
lEvtADetecter.push_back(&Evt_PB_SDOperatoire);
} 


// ============================================================================
//  RetirerEvtADetecter : enleve un evt de lEvtADetecte
// ============================================================================
void cSDPlanificateur::RetirerEvtADetecter(cEvenement* e)
{
for (unsigned int i=0; i<lEvtADetecter.size(); i++)
   if (lEvtADetecter[i] == e)
     {
     lEvtADetecter.erase(lEvtADetecter.begin()+i);
     i=lEvtADetecter.size()+1;
     };
}


// ============================================================================
// TraiterEvenements : declenchement des regles correspondant aux evts
//                     appel du SDOperatoire pour realiser les nouveaux plans
// ============================================================================
RESULTAT cSDPlanificateur::TraiterEvenements(vector <cEvenement*> l_evts)
{
if (Resultat.Sauver_trace)
  Resultat.fTrace << "EXAMEN DES REGLES DE PLANIFICATION" << endl;

if (Evt_PB_SDOperatoire.DETECTER())
  {
  if (Resultat.Sauver_trace)
    Resultat.fTrace << "probleme : le systeme decisionnel operatoire ne peut pas decider d'action" << endl;
  return PB;   // aucune regle pour le gerer
  }
else
  {
  // Declenchement des regles concernees par les evts,
  // regles mises NON_ACTIVE si NON_REUTILISABLE avec MAJ de lEvtADetecter
  for (unsigned int  i=0; i<lRegles.size(); i++)  
    {
    if ((Resultat.Sauver_trace) && (lRegles[i]->Active==ACTIVE))
         Resultat.fTrace << "si " << lRegles[i]->Nom << endl;
    if ((lRegles[i]->DECLENCHEUR()) && (lRegles[i]->Active==ACTIVE))
      {
       if (Resultat.Sauver_trace)
         Resultat.fTrace << "alors " << lRegles[i]->Nom << endl;
       lRegles[i]->ALORS();
       if (lRegles[i]->Reutilisable == NON_REUTILISABLE)
       	 {
          lRegles[i]->Active = NON_ACTIVE; 
          RetirerEvtADetecter(lRegles[i]->Evenement);
         };
      };
    };
  ModifPlans = Temps.J;
  return OK;
  };
}


