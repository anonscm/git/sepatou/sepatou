// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSystemeProduction.cc
contenu : definition des methodes de la classe cSystemeProduction
******************************************************************************/


#include "cSystemeProduction.h"

#include "cSDPlanificateur.h"
#include "cSDOperatoire.h"
#include "cSIObservation.h"
#include "cSISurveillance.h"
#include "cSBExecutant.h"
#include "cSBBiophysique.h"

extern cSDPlanificateur SDPlanificateur;
extern cSDOperatoire SDOperatoire;
extern cSIObservation SIObservation;
extern cSISurveillance SISurveillance;
extern cSBExecutant SBExecutant;
extern cSBBiophysique SBBiophysique;


// ============================================================================
// InitElementsInvariants : Initialisation des elements invariants 
//                          lors des simulations
// ============================================================================
void cSystemeProduction::InitElementsInvariants(void)
{
SDPlanificateur.InitElementsInvariants();
SDOperatoire.InitElementsInvariants();
SBBiophysique.InitElementsInvariants();
// APRES Biophysique
SIObservation.InitElementsInvariants();
}


// ============================================================================
// InitElementsVariants : Initialisation des elements variants 
//                        au jour de debut d une simulation 
// ============================================================================
void cSystemeProduction::InitElementsVariants(void)
{
SDPlanificateur.InitElementsVariants();
SDOperatoire.InitElementsVariants();
SBExecutant.InitElementsVariants();
SBBiophysique.InitElementsVariants();
SIObservation.InitElementsVariants();
}


// ============================================================================
// LibererMemoire : Liberer memoire dynamique
// ============================================================================
void cSystemeProduction::LibererMemoire(void)
{
SDPlanificateur.LibererMemoire();
SDOperatoire.LibererMemoire();
SBExecutant.LibererMemoire();
SBBiophysique.LibererMemoire();
SIObservation.LibererMemoire();
}
