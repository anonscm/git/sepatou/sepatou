// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSystemeProduction.h
contenu : definition de la classe cSystemeProduction
******************************************************************************/
#ifndef _CSYSTEMEPRODUCTION_H
#define _CSYSTEMEPRODUCTION_H

class cSDPlanificateur;
class cSDOperatoire;
class cSIObservation;
class cSISurveillance;
class cSBExecutant;
class cSBBiophysique;


// ============================================================================
// cSystemeProduction : representation du systeme de production
// ============================================================================
class cSystemeProduction
{
public:  //--------------------------------------------------------------------
void InitElementsInvariants(void);  // Initialisation des elements invariants
                                    // lors des simulations
void InitElementsVariants(void);    // Initialisation des elements variants
                                    // au jour de debut d une simulation
void LibererMemoire(void);          // Liberer memoire dynamique
private:  //-------------------------------------------------------------------

};


#endif
