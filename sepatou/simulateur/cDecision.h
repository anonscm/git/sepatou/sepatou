// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cDecision.h
contenu : definition de la classe cDecision
******************************************************************************/
#ifndef _CDECISION_H
#define _CDECISION_H


#include "cAction.h"
#include "cDirective.h"
#include "cPlan.h"   

extern void cAction_InitElementsVariants(void);
extern void cDirective_InitElementsVariants(void);
extern void cPlan_InitElementsVariants(void);

// ============================================================================
// cDecision : representation d une decision
// ============================================================================
class cDecision
{
public:  //--------------------------------------------------------------------

static void InitElementsVariants(void){
  cAction_InitElementsVariants();
  cDirective_InitElementsVariants();
  cPlan_InitElementsVariants();
}; // Initialisation au jour de debut des
   // elts variants lors des simulations

static void LibererMemoire(void){
  cAction::LibererMemoire();
  cDirective::LibererMemoire();
}; // liberer memoire dynamique

};


#endif
