// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cTroupeau.h
contenu : definition des classes cVache et cTroupeau
******************************************************************************/
#ifndef _CTROUPEAU_H
#define _CTROUPEAU_H

#include "pat.h"

class cParcelle;

// ============================================================================
// cVache : representation d une vache
// ============================================================================
class cVache
{
public:  //--------------------------------------------------------------------
int JourVelage;                // jour du velage
int MoisVelage;                // mois de velage
int AnneeVelage;               // annee de velage
ANNEE_VELAGE NomAnneeVelage;   // annee precedente ou courante
float q_l_M;                   // quantite journaliere maximum de lait
                               // potentiellement produit en Kg
float q_l_p[NB_JOURS];         // quantite journaliere de lait
                               // potentiellement produit en Kg 
float q_l_r[NB_JOURS];         // quantite journaliere de lait
                               // produit en Kg 
float V_M[NB_JOURS];           // capacite d ingestion maximum un jour en UE
float Estockee[NB_JOURS];      // energie stockee un jour en UFL
float q_c_e[NB_JOURS];         // quantit� de concentr� ing�r�
float q_m_e[NB_JOURS];         // quantit� de mais ing�r�
float q_f_e[NB_JOURS];         // quantit� de foin ing�r�
float q_h_e[NB_JOURS];         // quantit� d'herbe ing�r�e

void InitElementsInvariants(float q,int j,int m,ANNEE_VELAGE a);
void InitElementsVariants(void);  // init en debut de simulation
void Evoluer(void);               // evoluer d un pas de temps

private:  //-------------------------------------------------------------------
float CE_c;                    // valeur energetique du concentre en UFL/Kg
float CE_m;                    // valeur energetique du mais en UFL/Kg
float CE_f;                    // valeur energetique du foin en UFL/Kg
float CV_c;                    // capacite d encombrement du concentre en UE/Kg
float CV_m;                    // capacite d encombrement du mais en UE/Kg
float CV_f;                    // capacite d encombrement du foin en UE/Kg

// pour mise au point du modele
float Dmoy_ev[NB_JOURS];   // digestibilite in vitro de l herbe ingeree


void Init_q_l_p(void);
void Init_V_M(void);

float CalculerComplementMais(float qc, float qf);
float Calculerq_h_e(float qc, float qf, float qm, bool paturage_max);
float CalculerCE_h(float qhe);
float CalculerProductionLaitReelleMAX(float q_c_e, float q_m_e, 
                                    float q_f_e, float q_h_e);
void VerifierValeurs (void);
};


// ============================================================================
// cTroupeau : representation d un troupeau de vaches
// ============================================================================
class cTroupeau
{
public:  //--------------------------------------------------------------------
cVache Vache;                 // vache type du troupeau
int n;                        // nombre de vache
cParcelle* ParcelleCourante;  // parcelle ou se trouve le troupeau
 
void InitElementsInvariants(int  nb, float q,int j,int m,ANNEE_VELAGE a);
                                  // init elts invariants
void InitElementsVariants(void);  // init en debut de simulation
void Evoluer(void);               // evoluer d un pas de temps
};


#endif
