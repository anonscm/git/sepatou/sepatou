// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSISurveillance.h
contenu : definition de la classe cSISurveillance
******************************************************************************/
#ifndef _CSISURVEILLANCE_H
#define _CSISURVEILLANCE_H


#include <vector>

using namespace std;

#include "pat.h"

#include "cEvenement.h"

// ============================================================================
// cSISurveillance : representation du systeme d information de surveillance SIS
// ============================================================================
class cSISurveillance
{
public:  //--------------------------------------------------------------------

RESULTAT Reveiller(void);             // au nouveau pas de temps signale, 
                                      // signalement d evts aux SD Planificateur
				      // ou Operatoire ou demande d evolution 
                                      // du systeme sans action realisees

private:  //-------------------------------------------------------------------
vector <cEvenement*> lEvts;           // liste d evts detectes

void DetecterEvtSDPlanificateur(void);// met a jour lEvts : avec evts 
                                      // detectes pour le SD Planificateur
void DetecterEvtSDOperatoire(void);   // met a jour lEvts : avec evts
};                                    // detectes pour le SD Operatoire


#endif
