// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cEvenement.cc
contenu : definition des methodes de la classe cEvenement.c
******************************************************************************/

#include "cEvenement.h"

#include "cTemps.h"
#include "cSDPlanificateur.h"
#include "cSDOperatoire.h"
#include "cSBExecutant.h"


extern cTemps Temps;
extern cSDPlanificateur SDPlanificateur;
extern cSDOperatoire SDOperatoire;
extern cSBExecutant SBExecutant;

// ============================================================================
// cEvenement : constructeur
// ============================================================================
cEvenement::cEvenement( bool (*detecter)() )         // constructeur
{
DETECTER = detecter;
}


bool DETECTER_PB_SDOperatoire (void)
{
if (SDOperatoire.Probleme)
   return true;
else
   return false;
}

bool DETECTER_PB_SBExecutant (void)
{
if (SBExecutant.Probleme)
   return true;
else
   return false;
}

bool DETECTER_NOUVEAU_JOUR (void)
{
return true;
}

bool DETECTER_MODIF_PLANS (void)
{
if (SDPlanificateur.ModifPlans == Temps.J)
   return true;
else
   return false;
}
