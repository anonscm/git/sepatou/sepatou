// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cRegleOperatoire.h
contenu : definition de la classe cRegleOperatoire
******************************************************************************/
#ifndef _CREGLEOPERATOIRE_H
#define _CREGLEOPERATOIRE_H


#include <string>

using namespace std;

#include "pat.h"



// ============================================================================
// cRegleOperatoire : representation d une regle Operatoire
// ============================================================================
class cRegleOperatoire
{
public:  //--------------------------------------------------------------------
cRegleOperatoire( ACTIVITE activite, 
                  bool (*si)(), 
                  void (*alors)(), 
                  void (*sinon)(), 
                  string nom );

string          Nom;          // nom de la regle
ACTIVITE        Activite;      // Activite liee a la regle

bool (*SI)();                 // SI de la regle
void (*ALORS)();              // ALORS de la regle
void (*SINON)();              // SINON de la regle
};


#endif
