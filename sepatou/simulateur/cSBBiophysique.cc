// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSBBiophysique.cc
contenu : definition des methodes de la classe cSBBiophysique
******************************************************************************/

#include <fstream>
#include <vector>
#include <string>

#include "cSBBiophysique.h"

#include "pat.h"
#include "cTemps.h"
#include "cClimat.h"
#include "cStock.h"
#include "cTroupeau.h"
#include "cParcelle.h"
#include "cSBExecutant.h"
#include "cAction.h"
#include "cEnsembleParcelles.h"
#include "cAnomalie.h"

extern cTemps Temps;
extern cClimat Climat;
extern cTroupeau Troupeau;
extern cStockConcentre StockConcentre;
extern cStockMais StockMais;
extern cStockFoin StockFoin;
extern cStockAzote StockAzote;
extern cSBExecutant SBExecutant;


// ============================================================================
// InitElementsInvariants : Initialisation des elements invariants 
//         - definition du troupeau et des vaches,
//         - definition des parcelles
//         - definition stocks
// ============================================================================
void cSBBiophysique::InitElementsInvariants(void)
{
bool boucle;
ifstream fExploitation;
string CheminExploitation;
string commentaire;
string nom, caracteristique, type_sol, eloignement, qualite_foin, espece;
string NomAnneeVelage;
int ru, n, JourVelage, MoisVelage;
float q_init_T;
float surface, q_l_M, quantite_foin;
CARACTERISTIQUE_PARCELLE enum_caracteristique;
TYPE_SOL enum_type_sol;
ELOIGNEMENT enum_eloignement;
ESPECE_HERBE enum_espece;


// lecture dans fichier de la description de l'exploitation
CheminExploitation = CHEMIN_EXPLOITATION_TXT;
fExploitation.open(CheminExploitation.c_str(), ios::in);
if (!fExploitation)
  throw (new cAnomalie ("Dans la methode cSBiophysique.InitElementsInvariants, probleme pour ouvrir le fichier Exploitation.txt : " + CheminExploitation ) );

fExploitation >> commentaire >> commentaire >> NomExploitation;
fExploitation >> commentaire >> commentaire >> Localisation;
fExploitation >> commentaire >> commentaire >> Quota;

fExploitation >> commentaire; // ***PARCELLES
boucle = true;
while (boucle)
  {
  fExploitation >> commentaire;
  if  (commentaire[0] == '*') // on a lu le prochain commentaire
     boucle = false;
  else
    {
    fExploitation >> commentaire >> nom;
    fExploitation >> commentaire >> commentaire >> surface;
    fExploitation >> commentaire >> commentaire >> ru;
    fExploitation >> commentaire >> commentaire >> caracteristique;
    enum_caracteristique = TransformerCaracteristique(caracteristique);
    fExploitation >> commentaire >> commentaire >> type_sol;
    enum_type_sol = TransformerTypeSol(type_sol);
    fExploitation >> commentaire >> commentaire >> eloignement;
    enum_eloignement = TransformerEloignement(eloignement);
    fExploitation >> commentaire >> commentaire >> espece;
    enum_espece = TransformerEspece(espece);
    Parcelles.Ajouter(new cParcelle(nom, surface,ru, enum_caracteristique, 
                     enum_type_sol, enum_eloignement, enum_espece) );
    };
  };

fExploitation >> commentaire >> commentaire >> n; 
fExploitation >> commentaire >> commentaire >> q_l_M;
fExploitation >> commentaire >> commentaire >> JourVelage;
fExploitation >> commentaire >> commentaire >> MoisVelage;
fExploitation  >> commentaire >> commentaire>> NomAnneeVelage;
Troupeau.InitElementsInvariants( n, q_l_M, JourVelage, MoisVelage,
                               TransformerNomAnneeVelage(NomAnneeVelage) );

fExploitation >> commentaire; 
fExploitation >> commentaire >> commentaire >> q_init_T;
StockMais.InitElementsInvariants(q_init_T * 1000.0);

fExploitation >> commentaire; 
fExploitation >> commentaire >> commentaire >> qualite_foin;
fExploitation >> commentaire >> commentaire >> quantite_foin;
StockFoin.InitElementsInvariants(quantite_foin,TransformerQualiteFoin(qualite_foin) );

fExploitation.close();

StockConcentre.InitElementsInvariants(1000000 );
StockAzote.InitElementsInvariants(10000 );
}


// ============================================================================
// InitElementsVariants : Initialisation des elements variants 
//                        au jour de debut d une simulation
// ============================================================================
void cSBBiophysique::InitElementsVariants(void)
{
for (int j=32; j<NB_JOURS; j++)
  {
    qcoupee[j] = 0;
    scoupee[j] = 0;
  };

Troupeau.InitElementsVariants();

for (unsigned int i=0; i<Parcelles.l.size(); i++)
   Parcelles.l[i]->InitElementsVariants();

StockConcentre.InitElementsVariants();

StockMais.InitElementsVariants();

StockFoin.InitElementsVariants();

StockAzote.InitElementsVariants();
}

// ============================================================================
// LibererMemoire : liberer memoire dynamique
// ============================================================================
void cSBBiophysique::LibererMemoire(void)
{
int i,n;
n = Parcelles.l.size();
for (i=n-1;i>=0;i--)
  {
  delete Parcelles.l[i];
   Parcelles.Enlever(Parcelles.l[i]);
  };
}


// ============================================================================
// Evoluer : evoluer d un pas de temps
// ============================================================================
void cSBBiophysique::Evoluer(void)
{
unsigned int i;
cParcelle* p;

if ( SBExecutant.lActionsPaturageASimuler.size() == 1)
  SBExecutant.lActionsPaturageASimuler[0]->Parcelle->MAJJourDebutPaturage();
Troupeau.Evoluer();
for (unsigned int i=0; i<Parcelles.l.size(); i++)
   Parcelles.l[i]->Evoluer();

for (i=0; i<SBExecutant.lActionsEnsilageASimuler.size(); i++)
  {
    p = SBExecutant.lActionsEnsilageASimuler[i]->Parcelle;
    scoupee[Temps.J] += p->S;
    qcoupee[Temps.J] += (p->MS[Temps.J-1] - p->MS[Temps.J]) * 10 * p->S ;
  };
for (i=0; i<SBExecutant.lActionsFaucheASimuler.size(); i++)
  {
    p = SBExecutant.lActionsFaucheASimuler[i]->Parcelle;
    scoupee[Temps.J] += p->S;
    qcoupee[Temps.J] += (p->MS[Temps.J-1] - p->MS[Temps.J]) * 10 * p->S ;
  };

StockConcentre.Evoluer();
StockMais.Evoluer();
StockFoin.Evoluer();
StockAzote.Evoluer();
}



// ============================================================================
// TransformerCaracteristique : transformation string en enum
// ============================================================================
CARACTERISTIQUE_PARCELLE 
             cSBBiophysique::TransformerCaracteristique(string caracteristique)
{
if (caracteristique == "paturable")
   return CARACTERISTIQUE_PATURABLE;
else
   if (caracteristique == "mixte")
      return CARACTERISTIQUE_MIXTE;
   else
      throw (new cAnomalie ("Dans la methode cSBiophysique.TransformerCaracteristique, mauvaise valeur de caracteristique de parcelle") );
}


// ============================================================================
// TransformerTypeSol : transformation string en enum
// ============================================================================
TYPE_SOL cSBBiophysique::TransformerTypeSol(string type_sol)
{
if (type_sol == "sableux")
    return TYPE_SABLEUX;
else
    if (type_sol == "limono-sableux")
       return TYPE_LIMONO_SABLEUX;
    else 
       if (type_sol == "limoneux")
           return TYPE_LIMONEUX;
       else
            if (type_sol == "limono-argileux")
                return TYPE_LIMONO_ARGILEUX;
            else
                if (type_sol == "argileux")
                     return TYPE_ARGILEUX;
                else
                     throw (new cAnomalie ("Dans la methode cSBiophysique.TransformerTypeSol, mauvaise valeur de type de sol de parcelle") );
}


// ============================================================================
// TransformerEloignement : transformation string en enum
// ============================================================================
ELOIGNEMENT cSBBiophysique::TransformerEloignement(string eloignement)
{
if (eloignement == "loin")
    return LOIN;
else
    if (eloignement ==  "pres")
         return PRES;
    else
         throw (new cAnomalie ("Dans la methode cSBiophysique.TransformerEloignement, mauvaise valeur de eloignement de parcelle") );
}


// ============================================================================
// TransformerEspece : transformation string en enum
// ============================================================================
ESPECE_HERBE cSBBiophysique::TransformerEspece(string espece)
{
if (espece == "Dactyle")
    return DACTYLE;
else
    if (espece ==  "RayGrassAnglais")
         return RAY_GRASS_ANGLAIS;
    else
        if (espece ==  "FetuqueElevee")
             return FETUQUE_ELEVEE;
        else
             throw (new cAnomalie ("Dans la methode cSBiophysique.TransformerEspece, mauvaise valeur d'espece d'herbe de parcelle") );
}


// ============================================================================
// TransformerNomAnneeVelage : transformation string en enum
// ============================================================================
ANNEE_VELAGE cSBBiophysique::TransformerNomAnneeVelage(string NomAnneeVelage)
{
if (NomAnneeVelage == "annee-precedente")
    return VELAGE_ANNEE_PRECEDENTE;
else
    if (NomAnneeVelage ==  "annee-courante")
         return VELAGE_ANNEE_COURANTE;
    else
         throw (new cAnomalie ("Dans la methode cSBiophysique.TransformerNomAnneeVelage, mauvaise valeur de l'annee de velage.") );
}


// ============================================================================
// TransformerQualiteFoin : transformation string en enum
// ============================================================================
QUALITE_FOIN cSBBiophysique::TransformerQualiteFoin(string qualite_foin)
{
if (qualite_foin == "bonne")
   return QUALITE_BONNE;
else
   if (qualite_foin == "moyen")
       return QUALITE_MOYEN;
   else
       if (qualite_foin == "passable")
           return QUALITE_PASSABLE;
       else
            throw (new cAnomalie ("Dans la methode cSBiophysique.TransformerQualiteFoin, mauvaise valeur de qualite de foin") );
}


