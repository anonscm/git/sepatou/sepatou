// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSIObservation.cc
contenu : definition des methodes de la classe cSIObservation
******************************************************************************/

#include <string>
#include <vector>

#include "cSIObservation.h"

#include "pat.h"
#include "Strategie.h"

#include "cSimulateur.h"
#include "cResultat.h"
#include "cSDOperatoire.h"
#include "cClimat.h"
#include "cTemps.h"
#include "cFoInterpretation.h"
#include "cIndicateur.h"
#include "cAction.h"
#include "cPlan.h"
#include "cSBBiophysique.h"
#include "cStock.h"
#include "cTroupeau.h"
#include "cParcelle.h"
#include "cAnomalie.h"
#include "cEnsembleParcelles.h"

extern cSimulateur Simulateur;
extern cResultat Resultat;
extern cSBBiophysique SBBiophysique;
extern cTroupeau Troupeau;
extern cStockMais StockMais;
extern cStockFoin StockFoin;
extern cSDOperatoire SDOperatoire;
extern cPlanAlimentationConservee PlanAlimentationConservee;
extern cPlanPaturage PlanPaturage; 
extern cClimat Climat;
extern cTemps Temps;
extern cParcelle* etable;

extern vector <cActionMais*> lACTIONS_MAIS; //liste des actions Mais
extern vector <cActionPaturage*> lACTIONS_PATURAGE; // liste des actions Paturage
extern vector <cActionEnsilage*> lACTIONS_ENSILAGE; // liste des actions Ensilage
extern vector <cActionFauche*> lACTIONS_FAUCHE; // liste des actions Fauche

extern void CreerFoInterpretation(void);
extern void CreerIndicateurs(void);

// ============================================================================
// InitElementsInvariants : Initialisation des elements Invariants 
// ============================================================================
void cSIObservation::InitElementsInvariants(void)
{
int k;

CreerFoInterpretation();
CreerIndicateurs();

if (Resultat.Sauver_Fonctions || Resultat.Sauver_Statistiques)
  {
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (bFo[k] != NULL)
	MemobFonctions[k] = new bool[NB_JOURS];
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (fFo[k] != NULL)
	MemofFonctions[k] = new float[NB_JOURS];
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (pFo[k] != NULL)
	MemopFonctions[k] = new string[NB_JOURS];
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (eFo[k] != NULL)
	MemoeFonctions[k] = new string[NB_JOURS];
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (sFo[k] != NULL)
	MemosFonctions[k] = new string[NB_JOURS];
  };

if (Resultat.Sauver_Indicateurs)
    for (k=NB_INDICATEURS_PREDEFINIS; k<NB_MAX_INDICATEURS; k++)
      if (Indicateurs[k] != NULL)
	MemoIndicateurs[k] = new int[NB_JOURS];
}

// ============================================================================
// InitElementsVariants : Initialisation des elements Invariants 
// ============================================================================
void cSIObservation::InitElementsVariants(void)
{
int k;

for (int i=0; i<NB_MAX_INDICATEURS  ;i++)
  if (Indicateurs[i] != NULL)
    Indicateurs[i]->InitElementsVariants();

if (Resultat.Sauver_Fonctions || Resultat.Sauver_Statistiques)
  {
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemobFonctions[k] != NULL)
	for (int j = 0; j<NB_JOURS; j++)
	  MemobFonctions[k][j] = 0;
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemofFonctions[k] != NULL)
	for (int j = 0; j<NB_JOURS; j++)
	  MemofFonctions[k][j] = -1;
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemopFonctions[k] != NULL)
	for (int j = 0; j<NB_JOURS; j++)
	  MemopFonctions[k][j] = "";
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemoeFonctions[k] != NULL)
	for (int j = 0; j<NB_JOURS; j++)
	  MemoeFonctions[k][j] = "";
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemosFonctions[k] != NULL)
	for (int j = 0; j<NB_JOURS; j++)
	  MemosFonctions[k][j] = "";
  };

if (Resultat.Sauver_Indicateurs)
    for (int k=NB_INDICATEURS_PREDEFINIS; k<NB_MAX_INDICATEURS; k++)
      if (MemoIndicateurs[k] != NULL)
	for (int j = 0; j<NB_JOURS; j++)
	  MemoIndicateurs[k][j] = -1;
}

// ============================================================================
// LibererMemoire : liberer memoire dynamique
// ============================================================================
void cSIObservation::LibererMemoire(void)
{
int k;

for ( k=0; k<NB_MAX_FO_TYPE; k++)
  {
    if (bFo[k] != NULL)
      delete bFo[k];
    if (fFo[k] != NULL)
      delete fFo[k];
    if (pFo[k] != NULL)
      delete pFo[k];
    if (eFo[k] != NULL)
      delete eFo[k];
    if (sFo[k] != NULL)
      delete sFo[k];
  };
for ( k=0; k<NB_MAX_INDICATEURS; k++)
  {
    if (Indicateurs[k] != NULL)
      delete Indicateurs[k];
  };

if (Resultat.Sauver_Fonctions || Resultat.Sauver_Statistiques)
  {
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemobFonctions[k] != NULL)
	delete[] MemobFonctions[k];
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemofFonctions[k] != NULL)
	delete[] MemofFonctions[k];
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemopFonctions[k] != NULL)
	delete[] MemopFonctions[k];
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemoeFonctions[k] != NULL)
	delete[] MemoeFonctions[k];
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemosFonctions[k] != NULL)
	delete[] MemosFonctions[k];
  };

if (Resultat.Sauver_Indicateurs)
  if (Resultat.Sauver_Indicateurs)
    for (k=NB_INDICATEURS_PREDEFINIS; k<NB_MAX_INDICATEURS; k++)
      if (MemoIndicateurs[k] != NULL)
	delete[] MemoIndicateurs[k];
}


// ============================================================================
// MemoriserFonctionsIndicateurs
// ============================================================================
void cSIObservation::MemoriserFonctionsIndicateurs(void)
{
int k;
string s;
cEnsembleParcelles E;

if (Resultat.Sauver_Fonctions || Resultat.Sauver_Statistiques)
  {
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemobFonctions[k] != NULL)
	MemobFonctions[k][Temps.J] = bValeurFo( (bFO_INTERPRETATION) k);
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemofFonctions[k] != NULL)
	MemofFonctions[k][Temps.J] = fValeurFo( (fFO_INTERPRETATION) k);
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemopFonctions[k] != NULL)
	if (pValeurFo( (pFO_INTERPRETATION) k) != NULL)
	  MemopFonctions[k][Temps.J] = pValeurFo( (pFO_INTERPRETATION) k)->Nom;
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemoeFonctions[k] != NULL)
	{
	  E = eValeurFo( (eFO_INTERPRETATION) k);
	  s = "";
	  for (unsigned int p=0; p < E.l.size(); p++)
	      s = s + E.l[p]->Nom + " ";
	  MemoeFonctions[k][Temps.J] = s;
	};
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (MemosFonctions[k] != NULL)
	MemosFonctions[k][Temps.J] = sValeurFo( (sFO_INTERPRETATION) k);
  };

if (Resultat.Sauver_Indicateurs)
  for (int k=NB_INDICATEURS_PREDEFINIS; k<NB_MAX_INDICATEURS; k++)
      if (MemoIndicateurs[k] != NULL)
	MemoIndicateurs[k][Temps.J] = ValeurIndicateur((INDICATEUR) k);
}


// ============================================================================
// NbVaches : Nb de vaches
// ============================================================================
int cSIObservation::NbVaches(void)
{
  return Troupeau.n;
}

// ============================================================================
// QuantiteLaitMax : quantite de lait journaliere maximum potentielle
//                   pour une vache
// ============================================================================
float cSIObservation::QuantiteLaitMax(void)
{
  return Troupeau.Vache.q_l_M;
}

// ============================================================================
// QuantiteLaitPotentielle : quantite de lait journaliere potentielle
//                           un jour donne,  pour une vache
// ============================================================================
float cSIObservation::QuantiteLaitPotentielle(int j)
{
  return Troupeau.Vache.q_l_p[j];
}

// ============================================================================
// QuantiteLaitReelle : quantite de lait journaliere reelle
//                      un jour donne,  pour une vache
// ============================================================================
float cSIObservation::QuantiteLaitReelle(int j)
{
  return Troupeau.Vache.q_l_r[j];
}

// ============================================================================
// QuantiteConcentreMax : quantite de concentre maximum un jour donne,
// pour une vache : 1Kg par 3Kg de lait potentiel superieur a 20Kg
// ============================================================================
float cSIObservation::QuantiteConcentreMax(int j)
{
float q = Troupeau.Vache.q_l_p[j];
if (q>=20)
  {
  q = q - 20;
  return (q / 3);
  }
else
  return 0.;
}

// ============================================================================
// ReserveMais : reserve mais en Tonnes un jour donne
// ============================================================================
float cSIObservation::ReserveMais(int j)
{
return (StockMais.Etat(j) * 0.001);
}

// ============================================================================
// ReserveFoin : reserve foin en Kg
// ============================================================================
float cSIObservation::ReserveFoin(int j)
{
return StockFoin.Etat(j);
}

// ============================================================================
// QualiteFoin : reserve foin en Kg
// ============================================================================
QUALITE_FOIN cSIObservation::QualiteFoin(void)
{
return StockFoin.Qualite;
}

// ============================================================================
// Parcelles : ensemble des parcelles
// ============================================================================
cEnsembleParcelles cSIObservation::Parcelles(void)
{
return SBBiophysique.Parcelles;
}

// ============================================================================
// ParcelleCourante : parcelle ou se trouve le troupeau
// ============================================================================
cParcelle* cSIObservation::ParcelleCourante(void)
{
return Troupeau.ParcelleCourante;
}

// ============================================================================
// Nom : renvoie le pointeur de la parcelle de nom 'nom'
// ============================================================================
cParcelle* cSIObservation::Nom(string nom)
{
cParcelle* p = NULL;

for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == nom)
    p = SBBiophysique.Parcelles.l[i];

if (p == NULL)
  if (nom == "etable")
    p = etable;
  else
    throw (new cAnomalie ("Dans la methode cSIObservation::Nom, le nom de la parcelle n'est pas connue") );

 return p;
}

// ============================================================================
// CroissanceHerbe : croissance de l'herbe en Kg MS/ha/jour 
//                   (moyenne sur les 7 derniers jours)
// ep : ensemble de parcelles, seules les parcelles non paturee, fauchee
//      depuis 7 + n jours seront prises en compte
// ============================================================================
float cSIObservation::CroissanceHerbe (cEnsembleParcelles ep, int n)
{
float c = 0;

int nb_parcelles = 0;

if (Temps.J > Temps.NoJour(Simulateur.JourDebut, Simulateur.MoisDebut) + 7)
  {
    for (unsigned int i=0; i< ep.l.size(); i++)
      if ( ep.l[i]->Joter < Temps.J - 7 - n )
	{
	  c = c + (ep.l[i]->MS[Temps.J] - ep.l[i]->MS[Temps.J-7]) * 10 / 7;
	  nb_parcelles++;
	};
    if (nb_parcelles != 0)
      return c / nb_parcelles;
    else
      return -1;
  } 
else 
  return -1;
}



// ============================================================================
// DernierePatureeRecoltee : retourne la parcelle paturee ou recoltee
//                           en dernier de ep
// ============================================================================
cParcelle* cSIObservation::DernierePatureeRecoltee(cEnsembleParcelles ep)
{
cParcelle* p = NULL;
int j = 999;

for (unsigned int i=0; i< ep.l.size(); i++)
  if ( ep.l[i]->Joter < j )
    {
      j = ep.l[i]->Joter;
      p = ep.l[i];
    };

return p;
}


// ============================================================================
// MS : retourne MS[j] de la parcelle de nom "nom"
// ============================================================================
float cSIObservation::MS(string nom)
{
 return MS(nom, Temps.J);
}

float cSIObservation::MS(string nom, int j)
{
cParcelle* p = NULL;

for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == nom)
    p = SBBiophysique.Parcelles.l[i];

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::MS, le nom de la parcelle n'est pas connu") );

 return p->MS[j];
}

float cSIObservation::MS(cParcelle* p)
{
  return MS(p, Temps.J);
}

float cSIObservation::MS(cParcelle* p, int j)
{
bool bparcelle = false;

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::MS, la parcelle n'est pas specifiee") );


for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == p->Nom)
    bparcelle = true;

if (bparcelle == false)
    throw (new cAnomalie ("Dans la methode cSIObservation::MS, le nom de la parcelle n'est pas connu") );
 
return p->MS[j];
}



// ============================================================================
// Caracteristique 
// ============================================================================
CARACTERISTIQUE_PARCELLE  cSIObservation::Caracteristique(cParcelle* p)
{
bool bparcelle = false;

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::Caracteristique, la parcelle n'est pas specifiee") );


for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == p->Nom)
    bparcelle = true;

if (bparcelle == false)
    throw (new cAnomalie ("Dans la methode cSIObservation::Caracteristique, le nom de la parcelle n'est pas connu") );
 
return p->Caracteristique;
}



// ============================================================================
// TypeSol 
// ============================================================================
TYPE_SOL cSIObservation::TypeSol(cParcelle* p)
{
bool bparcelle = false;

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::TypeSol, la parcelle n'est pas specifiee") );


for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == p->Nom)
    bparcelle = true;

if (bparcelle == false)
    throw (new cAnomalie ("Dans la methode cSIObservation::TypeSol, le nom de la parcelle n'est pas connu") );
 
return p->TypeSol;
}



// ============================================================================
// Eloignement 
// ============================================================================
ELOIGNEMENT cSIObservation::Eloignement(cParcelle* p)
{
bool bparcelle = false;

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::Eloignement, la parcelle n'est pas specifiee") );


for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == p->Nom)
    bparcelle = true;

if (bparcelle == false)
    throw (new cAnomalie ("Dans la methode cSIObservation::Eloignement, le nom de la parcelle n'est pas connu") );
 
return p->Eloignement;
}



// ============================================================================
// Espece 
// ============================================================================
ESPECE_HERBE cSIObservation::Espece(cParcelle* p)
{
bool bparcelle = false;

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::Espece, la parcelle n'est pas specifiee") );


for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == p->Nom)
    bparcelle = true;

if (bparcelle == false)
    throw (new cAnomalie ("Dans la methode cSIObservation::Espece, le nom de la parcelle n'est pas connu") );
 
return p->Espece;
}


// ============================================================================
// HerbeIng�r�e : retourne q_h_e[j]
// ============================================================================
float cSIObservation::HerbeIngeree(int j)
{
if (j > Temps.J)
    throw (new cAnomalie ("Dans la methode cSIObservation::HerbeIng�r�e, demande de HerbeIng�r�e pour une date posterieure a la date courante.") );

  return Troupeau.Vache.q_h_e[j];
}



// ============================================================================
//  HauteurHerbe : 
// ============================================================================
float cSIObservation::HauteurHerbe(string nom, float d)
{
cParcelle* p = NULL;

for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == nom)
    p = SBBiophysique.Parcelles.l[i];

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::HauteurHerbe, le nom de la parcelle n'est pas connu") );

 return HauteurHerbe(p, d);
}

float cSIObservation::HauteurHerbe(cParcelle* p, float d)
{
bool bparcelle = false;

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::HauteurHerbe, la parcelle n'est pas specifiee") );


for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == p->Nom)
    bparcelle = true;

if (bparcelle == false)
    throw (new cAnomalie ("Dans la methode cSIObservation::HauteurHerbe, le nom de la parcelle n'est pas connu") );
 
return 10.0 * p->MS[Temps.J]/d;
}


// ============================================================================
// DisponibiliteHerbe : disponibilite en herbe le jour J 
// ============================================================================
float cSIObservation::DisponibiliteHerbe (cEnsembleParcelles ep)
{
return DisponibiliteHerbe(ep, Temps.J);;
}

float cSIObservation::DisponibiliteHerbe(cEnsembleParcelles ep, 
                                         int j, float q, float ms_reste)
{
float q_h_d = 0;

for (unsigned int i=0; i< ep.l.size(); i++)
  {
  if (ep.l[i]->MS[j] > ms_reste)
     q_h_d += 10 * (ep.l[i]->MS[j] - ms_reste) * ep.l[i]->SU[j];
  };
return q_h_d / (q * Troupeau.n);
}

// ============================================================================
// Paturables : ensembles des parcelles de ep proposant au moins q Kg d'herbe
// par vache en laissant au moins ms_reste et non deja choisies pour etre
// ensilees ou fauchees, et ayant deja ete paturee, ensilee ou fauchee 
// si ST > 600 �C.jour
// ============================================================================
cEnsembleParcelles cSIObservation::Paturables(cEnsembleParcelles ep, 
        float q, float ms_reste, int nj_min, int nj_max, float ms_max)
{
unsigned int i;
int nj;

cEnsembleParcelles Epaturables,E,Epef;
Epaturables = ep;
 
for (i=0; i <SDOperatoire.Ensiler.l.size(); i++)
  Epaturables.Enlever( SDOperatoire.Ensiler.l[i] );
for (i=0; i <SDOperatoire.Faucher.l.size(); i++)
  Epaturables.Enlever( SDOperatoire.Faucher.l[i] );

if (SommeTbase0(32, Temps.J) > 600)
  {
    Epef = DejaPatureesEnsileesFauchees (Parcelles());
    E = Epaturables;
    for (i=0; i<E.l.size(); i++)
      if (!(Epef.Parmi(E.l[i])))
	Epaturables.Enlever(E.l[i]);
  };

E = Epaturables;
for (i=0; i<E.l.size(); i++)
   if (!(AuMoinsUnJour(E.l[i],q,ms_reste)) )
      Epaturables.Enlever(E.l[i]);

E = Epaturables;
for (i=0; i<E.l.size(); i++)
  {
    nj = Temps.J - E.l[i]->JfinDerniereUtilisation;
    if ( nj < nj_min || nj > nj_max )
      Epaturables.Enlever(E.l[i]);
  };

E = Epaturables;
for (i=0; i<E.l.size(); i++)
    if ( E.l[i]->MS[Temps.J] > ms_max )
      Epaturables.Enlever(E.l[i]);

return Epaturables;
}

// ============================================================================
// AuMoinsUnJour : indique si la parcelle propose q Kg d'herbe par vache
//                 en laissant ms_reste
// ============================================================================
bool cSIObservation::AuMoinsUnJour(cParcelle* p, float q, float ms_reste)
{
float total_ms_Kg;
    
if (p->MS[Temps.J] <=  ms_reste)
    return false;
else
  {
   total_ms_Kg = (p->MS[Temps.J] - ms_reste) * p->SU[Temps.J] * 10;
   if ((total_ms_Kg / Troupeau.n) > q )
          return true;
   else
          return false;
  };
}

// ============================================================================
// Ensilables : ensembles des parcelles de ep ensilables (au moins ms_min)
//  et non deja choisies pour etre paturees ou fauchees
// ============================================================================
cEnsembleParcelles cSIObservation::Ensilables(cEnsembleParcelles ep, 
                                               float ms_min)
{
unsigned int i;

cEnsembleParcelles Eensilables,E;
Eensilables = ep;

Eensilables.Enlever( SDOperatoire.DeplacerTroupeau );
for (i=0; i <SDOperatoire.Faucher.l.size(); i++)
  Eensilables.Enlever( SDOperatoire.Faucher.l[i] );

E=Eensilables;
for (i=0; i<E.l.size(); i++)
   if (E.l[i]->MS[ Temps.J ] < 300 )
        Eensilables.Enlever(E.l[i]);

return Eensilables;
}

// ============================================================================
// Fauchables : ensembles des parcelles de ep fauchables (au moins ms_min)
//  et non deja choisies pour etre paturees ou ensilees
// ============================================================================
cEnsembleParcelles cSIObservation::Fauchables(cEnsembleParcelles ep, 
                                               float ms_min)
{
unsigned int i;

cEnsembleParcelles Efauchables,E;
Efauchables = ep;

Efauchables.Enlever( SDOperatoire.DeplacerTroupeau );
for (i=0; i <SDOperatoire.Ensiler.l.size(); i++)
  Efauchables.Enlever( SDOperatoire.Ensiler.l[i] );

E=Efauchables;
for (i=0; i<E.l.size(); i++)
   if (E.l[i]->MS[ Temps.J ] < ms_min )
        Efauchables.Enlever(E.l[i]);

return Efauchables;
}

// ============================================================================
// DejaPaturees : renvoie les parcelles deja paturees n fois exactement
// ============================================================================
cEnsembleParcelles cSIObservation::DejaPaturees (int n, cEnsembleParcelles ep)
{
cEnsembleParcelles E;

for (unsigned int i=0; i <ep.l.size(); i++)
   for (unsigned int k=0; k<lACTIONS_PATURAGE.size(); k++)
     if ( lACTIONS_PATURAGE[k]->Parcelle == ep.l[i] )
       if ( lACTIONS_PATURAGE[k]->Parcelle->NbPaturage == n )
	 E.Ajouter (ep.l[i]);

return E;
}

// ============================================================================
// DejaPatureesEnsileesFauchees : renvoie les parcelles deja paturees, ensilees
// ou fauchees
// ============================================================================
cEnsembleParcelles cSIObservation::DejaPatureesEnsileesFauchees 
                                                      (cEnsembleParcelles ep)
{
unsigned int k;
bool parcelle_a_garder;
cEnsembleParcelles E;

for (unsigned int i=0; i <ep.l.size(); i++)
  {
   parcelle_a_garder = false;
   for (k=0; k<lACTIONS_PATURAGE.size(); k++)
     if ( lACTIONS_PATURAGE[k]->Parcelle == ep.l[i] )
       if ( lACTIONS_PATURAGE[k]->Jour < Temps.J )
         parcelle_a_garder = true;
   if (!parcelle_a_garder)
     for (k=0; k<lACTIONS_ENSILAGE.size(); k++)
        if ( lACTIONS_ENSILAGE[k]->Parcelle == ep.l[i] )
	  if ( lACTIONS_ENSILAGE[k]->Jour < Temps.J )
	    parcelle_a_garder = true;
   if (!parcelle_a_garder)
     for (k=0; k<lACTIONS_FAUCHE.size(); k++)
        if ( lACTIONS_FAUCHE[k]->Parcelle == ep.l[i] )
	  if ( lACTIONS_FAUCHE[k]->Jour < Temps.J )
	    parcelle_a_garder = true;

  if (parcelle_a_garder)
     E.Ajouter (ep.l[i]);
  };
return E;
}

// ============================================================================
// DejaEnsilees : renvoie les parcelles deja ensilees
// ============================================================================
cEnsembleParcelles cSIObservation::DejaEnsilees (cEnsembleParcelles ep)
{
cEnsembleParcelles E;

for (unsigned int i=0; i <ep.l.size(); i++)
  for (unsigned int k=0; k<lACTIONS_ENSILAGE.size(); k++)
        if ( lACTIONS_ENSILAGE[k]->Parcelle == ep.l[i] )
	  if ( lACTIONS_ENSILAGE[k]->Jour < Temps.J )
            E.Ajouter (ep.l[i]);
return E;
}


// ============================================================================
// PremieresFauches : ensemble des parcelles ou ont eu lieu les 1eres fauches
// ============================================================================
cEnsembleParcelles cSIObservation::PremieresFauches (cEnsembleParcelles ep)
{
unsigned int i, k;
cEnsembleParcelles E;
int date_1ere_fauche = DATE_INCONNUE;

// recherche date de la 1ere fauche
for (i=0; i <ep.l.size(); i++)
    for (k=0; k<lACTIONS_FAUCHE.size(); k++)
        if ( lACTIONS_FAUCHE[k]->Parcelle == ep.l[i] )
	  if ((date_1ere_fauche == DATE_INCONNUE) ||
	      (lACTIONS_FAUCHE[k]->Jour < date_1ere_fauche))
	    date_1ere_fauche = lACTIONS_FAUCHE[k]->Jour;

// mise a jour de la liste des parcelles fauch�es en 1er
for (i=0; i <ep.l.size(); i++)
    for (k=0; k<lACTIONS_FAUCHE.size(); k++)
        if ( ( lACTIONS_FAUCHE[k]->Parcelle == ep.l[i] ) &&
	     (lACTIONS_FAUCHE[k]->Jour == date_1ere_fauche) )
	  E.Ajouter (ep.l[i]);

return E;
}

// ============================================================================
// Quota : quota de production laitiere de l'exploitation
// ============================================================================
int cSIObservation::Quota(void)
{
return SBBiophysique.Quota;
}

// ============================================================================
// Pluie : en mm
// ============================================================================
float cSIObservation::Pluie(int j)
{
return Climat.Pluie[j];
}

// ============================================================================
// T : en �C
// ============================================================================
float cSIObservation::T(int j)
{
return Climat.T[j];
}

// ============================================================================
// ETP : retourne ETP[j] 
// ============================================================================
float cSIObservation::ETP(int j)
{
if (j > Temps.J)
    throw (new cAnomalie ("Dans la methode cSIObservation::ETP, demande de l'ETP pour une date posterieure a la date courante.") );

return Climat.ETP[j];
}

// ============================================================================
// SommeTbase0 : somme de temperature base 0 en �C de j1 a j2
// ============================================================================
float cSIObservation::SommeTbase0(int j1, int j2)
{
float sT = 0;
for (int j=j1; j<=j2; j++)
   sT += Climat.Tbase0[j];
return sT;
}

// ============================================================================
// xVisualiser : visualisation de la valeur de l'argument
//               (permet notamment de visualiser des variables utilisateur LnU)
// ============================================================================
bool cSIObservation::bVisualiser(bool b)
{
  return b;
}

float cSIObservation::fVisualiser(float f)
{
  return f;
}

string cSIObservation::pVisualiser(cParcelle* p)
{
  return p->Nom;
}

string cSIObservation::eVisualiser(cEnsembleParcelles e)
{
  string s;
  for (unsigned int i=0; i<e.l.size(); i++)
    s = s + e.l[i]->Nom + " ";
  return s;
}

string cSIObservation::sVisualiser(string s)
{
  return s;
}

// ============================================================================
// bValeurFo : valeur d'une fonction booleenne
// ============================================================================
bool cSIObservation::bValeurFo(bFO_INTERPRETATION k)
{
return bFo[k]->CODE();
}

// ============================================================================
// fValeurFo : valeur d'une fonction de valeur float
// ============================================================================
float cSIObservation::fValeurFo(fFO_INTERPRETATION k)
{
return fFo[k]->CODE();
}

// ============================================================================
// fValeurFo : valeur d'une fonction de valeur cParcelle*
// ============================================================================
cParcelle* cSIObservation::pValeurFo(pFO_INTERPRETATION k)
{
return pFo[k]->CODE();
}

// ============================================================================
// eValeurFo : valeur d'une fonction de valeur cEnsembleParcelles
// ============================================================================
cEnsembleParcelles cSIObservation::eValeurFo(eFO_INTERPRETATION k)
{
return eFo[k]->CODE();
}

// ============================================================================
// sValeurFo : valeur d'une fonction de valeur string
// ============================================================================
string cSIObservation::sValeurFo(sFO_INTERPRETATION k)
{
return sFo[k]->CODE();
}

// ============================================================================
// bAjouterFo : ajout d'une fonction de valeur booleen
// ============================================================================
void cSIObservation::bAjouterFo(cbFoInterpretation* f)
{
for (int i=0; i<NB_MAX_FO_TYPE; i++)
  if (bFo[i] == NULL)
    {
    bFo[i]=f;
    i = NB_MAX_FO_TYPE;
    };
}

// ============================================================================
// fAjouterFo : ajout d'une fonction de valeur float
// ============================================================================
void cSIObservation::fAjouterFo(cfFoInterpretation* f)
{
for (int i=0; i<NB_MAX_FO_TYPE; i++)
  if (fFo[i] == NULL)
    {
    fFo[i]=f;
    i = NB_MAX_FO_TYPE;
    };
}

// ============================================================================
// pAjouterFo : ajout d'une fonction de valeur cParcelle*
// ============================================================================
void cSIObservation::pAjouterFo(cpFoInterpretation* f)
{
for (int i=0; i<NB_MAX_FO_TYPE; i++)
  if (pFo[i] == NULL)
    {
    pFo[i]=f;
    i = NB_MAX_FO_TYPE;
    };
}

// ============================================================================
// eAjouterFo : ajout d'une fonction de valeur cEnsembleParcelles
// ============================================================================
void cSIObservation::eAjouterFo(ceFoInterpretation* f)
{
for (int i=0; i<NB_MAX_FO_TYPE; i++)
  if (eFo[i] == NULL)
    {
    eFo[i]=f;
    i = NB_MAX_FO_TYPE;
    };
}

// ============================================================================
// sAjouterFo : ajout d'une fonction de valeur string
// ============================================================================
void cSIObservation::sAjouterFo(csFoInterpretation* f)
{
for (int i=0; i<NB_MAX_FO_TYPE; i++)
  if (sFo[i] == NULL)
    {
    sFo[i]=f;
    i = NB_MAX_FO_TYPE;
    };
}

// ============================================================================
// ValeurIndicateur : valeur d'un indicateur
// ============================================================================
int cSIObservation::ValeurIndicateur(INDICATEUR k)
{
  return Indicateurs[k]->Valeur();
}

// ============================================================================
// AjouterIndicateur : ajout d'un indicateur
// ============================================================================
void cSIObservation::AjouterIndicateur(cIndicateur* indicateur)
{
for (int i=0; i<NB_MAX_INDICATEURS; i++)
  if (Indicateurs[i] == NULL)
    {
    Indicateurs[i]= indicateur;
    i = NB_MAX_INDICATEURS;
    };
}

// ============================================================================
// Avant : indique si la date courante est avant la valeur de l indicateur
// ============================================================================
bool cSIObservation::Avant(INDICATEUR k)
{
return Avant(Indicateurs[k]->Valeur());
}

bool cSIObservation::Avant(int valeur)
{
if (valeur == DATE_INCONNUE)
   return true;
else
   return false;
}
// ============================================================================
// A : indique si la date courante est a la valeur de l indicateur
// ============================================================================
bool cSIObservation::A(INDICATEUR k)
{
return A(Indicateurs[k]->Valeur());
}

bool cSIObservation::A(int valeur)
{
if (valeur == Temps.J)
   return true;
else
   return false;
}

// ============================================================================
// Apres : indique si la date courante est apres la valeur de l indicateur
// ============================================================================
bool cSIObservation::Apres(INDICATEUR k)
{
return Apres(Indicateurs[k]->Valeur());
}

bool cSIObservation::Apres(int valeur)
{
if ( (valeur != DATE_INCONNUE) &&
     (valeur != Temps.J) )
   return true;
else
   return false;
}


// ============================================================================
// pour l'optimisateur
// ============================================================================
float cSIObservation::Somme(float* f )
{
  float s=0; 
  for (int j=32; j<=Temps.J; j++) 
    s+= f[j]; 
  return s;
}
float cSIObservation::Maximum(float* f )
{
  float max = f[32]; 
  for (int j=33; j<=Temps.J; j++) 
    if (f[j] > max)
      max = f[j];
  return max;
}
float cSIObservation::Minimum(float* f )
{
  float min = f[32]; 
  for (int j=33; j<=Temps.J; j++) 
    if (f[j] < min)
      min = f[j];
  return min;
}

float* cSIObservation::Vq_l_r(void) 
{
  return Troupeau.Vache.q_l_r;
}
float* cSIObservation::Vq_l_p(void) 
{
  return Troupeau.Vache.q_l_p;
}
float* cSIObservation::VEstockee(void) 
{
  return Troupeau.Vache.Estockee;
}
float* cSIObservation::Vqconcentre(void) 
{
  return Troupeau.Vache.q_c_e;
}
float* cSIObservation::Vqmais(void) 
{
  return Troupeau.Vache.q_m_e;
}
float* cSIObservation::Vqfoin(void) 
{
  return Troupeau.Vache.q_f_e;
}
float* cSIObservation::Vqherbe(void) 
{
  return Troupeau.Vache.q_h_e;
}
float* cSIObservation::Vqcoupee(void) 
{
  return SBBiophysique.qcoupee;
}
float* cSIObservation::Vscoupee(void) 
{
  return SBBiophysique.scoupee;
}


int cSIObservation::DateMiseHerbe(void)
{
  int d = DATE_INCONNUE;
  if (lACTIONS_PATURAGE.size() > 0)
    d = lACTIONS_PATURAGE[0]->Jour;
  return d;
}

int cSIObservation::DateFin1erCycle(void)
{
  cParcelle* p = NULL;
  int jour = DATE_INCONNUE;
  int d = DATE_INCONNUE;
  if (lACTIONS_PATURAGE.size() > 0)
    {
      cEnsembleParcelles E_paturee;      
      for (unsigned int i = 0; i < lACTIONS_PATURAGE.size(); i++)
	{
	  // la parcelle a d�ja �t� patur�e mais pas la veille
	  if (E_paturee.Parmi(lACTIONS_PATURAGE[i]->Parcelle) 
	      && !(jour == lACTIONS_PATURAGE[i]->Jour - 1
		   && p == lACTIONS_PATURAGE[i]->Parcelle) )
	    {
	      d = lACTIONS_PATURAGE[i]->Jour - 1;
	      i = lACTIONS_PATURAGE.size();
	    }
	  else
	    {
	      E_paturee = E_paturee + lACTIONS_PATURAGE[i]->Parcelle;
	      p = lACTIONS_PATURAGE[i]->Parcelle;
	      jour = lACTIONS_PATURAGE[i]->Jour;
	    };
	};
    }
  return d;
}

int cSIObservation::DateSortieNuit(void)
{
  bool b_trouve;
  int d = DATE_INCONNUE;
  if (lACTIONS_PATURAGE.size() > 0)
    {
      b_trouve = false;
      for (unsigned int i = 0; i < lACTIONS_PATURAGE.size(); i++)
	if (lACTIONS_PATURAGE[i]->DureePaturage == DUREE_PATURAGE_JOUR_NUIT)
	  {
	    d = lACTIONS_PATURAGE[i]->Jour;
	    i = lACTIONS_PATURAGE.size();
	    b_trouve = true;
	  };
      if  (!b_trouve)
	d = DATE_INCONNUE;
    }
  return d;
}


#define DUREE_FERMETURE_SILO 5 // 5 jours sans mais

int cSIObservation::DateFermetureSilo(void)
{
  int d_ferme = DATE_INCONNUE;

  // on v�rifie qu'il n'y a pas eu de fermeture puis ouverture
  for (unsigned int n=0; n<lACTIONS_MAIS.size()-1; n++)
    if ( lACTIONS_MAIS[n+1]->Jour - lACTIONS_MAIS[n]->Jour -1 >= DUREE_FERMETURE_SILO )
      {d_ferme = lACTIONS_MAIS[n]->Jour + 1; n = lACTIONS_MAIS.size();}

  // si ce n'est pas le cas, on v�rifie que la derni�re action Mais
  // a eu lieu DUREE_FERMETURE_SILO avant la fin de la simulation
    if (d_ferme == DATE_INCONNUE && lACTIONS_MAIS.size() > 1 &&
       Temps.J - lACTIONS_MAIS[lACTIONS_MAIS.size()-1]->Jour -1 >= DUREE_FERMETURE_SILO )
      d_ferme = lACTIONS_MAIS[lACTIONS_MAIS.size()-1]->Jour + 1;

  return d_ferme;
}

int cSIObservation::DateOuvertureSilo(void)
{
  int d_ouvert = DATE_INCONNUE;

  for (unsigned int n=0; n<lACTIONS_MAIS.size()-2; n++)
    if ( lACTIONS_MAIS[n+1]->Jour - lACTIONS_MAIS[n]->Jour -1 >= DUREE_FERMETURE_SILO )
      d_ouvert = lACTIONS_MAIS[n+1]->Jour;

  return d_ouvert;
}


int cSIObservation::DateEnsilage(void)
{
  int d = DATE_INCONNUE;
  if (lACTIONS_ENSILAGE.size() > 0)
    d = lACTIONS_ENSILAGE[0]->Jour;
  return d;
}

int cSIObservation::Date1ereFauche(void)
{
  int d = DATE_INCONNUE;
  if (lACTIONS_FAUCHE.size() > 0)
    d = lACTIONS_FAUCHE[0]->Jour;
   return d;
}

// ============================================================================
// S : Donne la surface d'une parcelle
// ============================================================================
float cSIObservation::S (string nom)
{
cParcelle* p = NULL;

for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == nom)
    p = SBBiophysique.Parcelles.l[i];

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::S, le nom de la parcelle n'est pas connu") );

 return p->S;
}


float cSIObservation::S (cParcelle* p)
{
bool bparcelle = false;

if (p == NULL)
    throw (new cAnomalie ("Dans la methode cSIObservation::S, la parcelle n'est pas specifiee") );


for (unsigned int i=0; i< SBBiophysique.Parcelles.l.size(); i++)
  if (SBBiophysique.Parcelles.l[i]->Nom == p->Nom)
    bparcelle = true;

if (bparcelle == false)
    throw (new cAnomalie ("Dans la methode cSIObservation::S, le nom de la parcelle n'est pas connu") );
 
return p->S;
}



// ============================================================================
// S_Parcelles : Donne la surface d'un ensemble de parcelles
// ============================================================================
float cSIObservation::S_Parcelles (cEnsembleParcelles ep)
{
float s = 0;

for (unsigned int i=0; i< ep.l.size(); i++)
     s +=  ep.l[i]->S;

return s;
}
