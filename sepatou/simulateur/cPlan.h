// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cPlan.h
contenu : definition des classes cPlan, cPlanAlimentationConservee,
                                 cPlanPaturage, cPlanFauche, cPlanFertilisation
                                 cPlanCoordination
******************************************************************************/

/******************************************************************************
PROGRAMMATION A AMELIORER
- LDirectives au niveau de cPlan, avec RAZ dans cPlan.Init
- MAJ Activite et lPLANS dans constructeur de cPlan
******************************************************************************/
#ifndef _CPLAN_H
#define _CPLAN_H

#include <vector>

#include "pat.h"  

#include "cEnsembleParcelles.h"

using namespace std;

class cDirective;

// ============================================================================
// cPlan : representation d un plan
// ============================================================================
class cPlan
{
public:  //--------------------------------------------------------------------
ACTIVITE Activite ;                     // type d activite  

cDirective* DirectiveActive;            // directive qui s applique
vector <cDirective*> lDirectives;       // liste des directives du plan

//static void InitElementsVariants(void)
//static cPlan* PlanDeActivite(ACTIVITE activite); // retourne le plan associe
//static void MAJPlans(void);                      // mise a jour des plans

void MAJ(void);                         // mise a jour du plan
void AjouterDirective (cDirective* d);  // ajout d une directive au plan

// pour les m�thodes virtuelles suivantes d�claration d'un descructeur virtuel
virtual ~cPlan() {};
virtual void Init(void) = 0;                // initialise les attributs
virtual void InitVariablesDecision(void) = 0;  // init var decision du plan
};

// ============================================================================
// cPlanAlimentationConservee : representation d un plan alimentation conservee
// ============================================================================
class cPlanAlimentationConservee: public cPlan
{
public:  //--------------------------------------------------------------------
float AlimentationConcentre;
float AlimentationMais;
float AlimentationFoin;

void Init(void);                        // initialise les attributs
void InitVariablesDecision(void);       // init var decision du plan
};

// ============================================================================
// cPlanPaturage : representation d un plan paturage
// ============================================================================
class cPlanPaturage: public cPlan
{
public:  //--------------------------------------------------------------------
ALIMENTATION_HERBE AlimentationHerbe;      // mode alimentation herbe
cEnsembleParcelles ParcellesAPaturer;      // parcelles a paturer

void Init(void);                           // initialise les attributs
void InitVariablesDecision(void);          // init var decision du plan
};

// ============================================================================
// cPlanFauche : representation d un plan fauche
// ============================================================================
class cPlanFauche: public cPlan
{
public:  //--------------------------------------------------------------------
cEnsembleParcelles ParcellesAEnsiler;      // parcelles a ensiler
cEnsembleParcelles ParcellesAFaucher;      // parcelles a faucher

void Init(void);                           // initialise les attributs
void InitVariablesDecision(void);          // init var decision du plan
};

// ============================================================================
// cPlanFertilisation : representation d un plan fertilisation
// ============================================================================
class cPlanFertilisation: public cPlan
{
public:  //--------------------------------------------------------------------
cEnsembleParcelles ParcellesAFertiliser;   // parcelles a fertiliser
REGIME_AZOTE RegimeAzote;                  // regime azote

void Init(void);                           // initialise les attributs
void InitVariablesDecision(void);          // init var decision du plan
};

// ============================================================================
// cPlanCoordination : representation d un plan coordination
// ============================================================================
class cPlanCoordination: public cPlan
{
public:  //--------------------------------------------------------------------
ACTIVITE OrdreActivites[NB_ACTIVITES];

void Init(void);                        // initialise les attributs
void InitVariablesDecision(void);       // init var decision du plan
};



#endif
