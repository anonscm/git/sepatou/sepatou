// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cDirective.h
contenu : definition de la classe cDirective
******************************************************************************/

#ifndef _CDIRECTIVE_H
#define _CDIRECTIVE_H

extern void cDirective_InitElementsVariants(void);

// ============================================================================
// cDirective : representation d une directive
// ============================================================================
class cDirective
{
public:  //--------------------------------------------------------------------

cDirective( void (*debut)(),
            int (* du)(),
            int (* au)(),
            void (* faire)(),
            void (* fin)() );             // constructeur

bool Active;                              // directive consideree
void (* DEBUT)();
int (* DU)();
int (* AU)();
void (* FAIRE)();
void (* FIN)();

// static void InitElementsVariants(void)
static void LibererMemoire(void){
  cDirective_InitElementsVariants();
};  // liberer memoire dynamique

};


#endif
