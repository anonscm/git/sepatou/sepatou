// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSDPlanificateur.h
contenu : definition de la classe cSDPlanificateur
******************************************************************************/
#ifndef _CSDPLANIFICATEUR_H
#define _CSDPLANIFICATEUR_H

#include <string>
#include <vector>

using namespace std;

#include "pat.h"
#include "cEnsembleParcelles.h"

class cParcelle;
class cReglePlanification;
class cPlan;
class cEvenement;


// ============================================================================
// cSDPlanificateur : representation du systeme decisionnel planificateur
// ============================================================================
class cSDPlanificateur
{
public:  //--------------------------------------------------------------------
vector <cReglePlanification*> lRegles;  // liste des regles
vector <cEvenement*>  lEvtADetecter;    // liste des evenements a detecter
int ModifPlans;                         // no jour derniere modification
                                        // des plans

string NomStrategie;                    // nom de la straegie
int Jour1erApportAzote;                 // no jour 1er apport azote
int Jour2emeApportAzote;                // no jour 2eme apport azote
int Jour3emeApportAzote;                // no jour 3eme apport azote

// variables utilisateur LnU
bool                          bVariable[NB_VAR_SDP];
int                           iVariable[NB_VAR_SDP]; 
float                         fVariable[NB_VAR_SDP];
cParcelle*                    pVariable[NB_VAR_SDP];
cEnsembleParcelles            eVariable[NB_VAR_SDP];
string                        sVariable[NB_VAR_SDP];
cEnsembleParcelles            ParcellesGardees;
cEnsembleParcelles            ParcellesSuspendues;
cEnsembleParcelles            ParcellesUtilisateur1;
cEnsembleParcelles            ParcellesUtilisateur2;
cEnsembleParcelles            ParcellesUtilisateur3;


void InitElementsInvariants(void);            // init des elts invariants
void InitElementsVariants(void);              // initialisation des elts
                                              // au jour de debut de simulation
void LibererMemoire(void);                    // liberer memoire dynamique
void AjouterRegle(cReglePlanification* r);    // ajouter une regle
void InitlEvtADetecter(void);                 // mise a jour liste des evts
                                              // a detecter
void RetirerEvtADetecter(cEvenement* e);      // enleve un evt de lEvtADetecte
RESULTAT TraiterEvenements(vector <cEvenement*> l_evts);            
                                              // traiter l occurence d une
                                              // liste d evts

// attributs ajout�s pour l'optimisateur
double Teta1;
double Teta2;
double Teta3;
double Teta4;
double Teta5;
double Teta6;
double Teta7;
double Teta8;
double Teta9;
double Teta10;

private:  //-------------------------------------------------------------------
int J2emeApportAzote;                         // jour 2eme apport azote
int M2emeApportAzote;                         // mois 2eme apport azote
int J3emeApportAzote;                         // jour 3eme apport azote
int M3emeApportAzote;                         // mois 3eme apport azote

};

#endif
