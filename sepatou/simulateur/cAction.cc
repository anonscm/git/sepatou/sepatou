// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cAction.cc
contenu : definition des methodes des classes cAction, cActionConcentre, 
          cActionMais, cActionFoin, cActionPaturage, cActionEnsilage,
          cActionFauche, cActionAzote 
******************************************************************************/

#include <vector>

                      
#include "cAction.h"

#include "pat.h"  
#include "cTemps.h"
#include "cStock.h"
#include "cTroupeau.h" 
#include "cParcelle.h"
#include "cAnomalie.h"

extern cTemps Temps;
extern cStockMais StockMais;
extern cStockFoin StockFoin;
extern cTroupeau Troupeau;

extern vector <cActionConcentre*> lACTIONS_CONCENTRE; // liste des actions Concentre
extern vector <cActionMais*> lACTIONS_MAIS; // liste des actions Mais
extern vector <cActionFoin*> lACTIONS_FOIN; // liste des actions Foin
extern vector <cActionPaturage*> lACTIONS_PATURAGE; // liste des actions Paturage
extern vector <cActionEnsilage*> lACTIONS_ENSILAGE; // liste des actions Ensilage
extern vector <cActionFauche*> lACTIONS_FAUCHE; // liste des actions Fauche
extern vector <cActionAzote*> lACTIONS_AZOTE; // liste des actions Azote



// ============================================================================
// InitElementsVariants : Initialisation des elements variants 
//                        au jour de debut d une simulation : 
//                        destruction de toutes les instances d'action 
//                        memorisees dans la variable globale lACTIONS
// ============================================================================
// ATTENTION d�finit comme fonction car pas possible de d�finir en statique
// avec upgrade compilateur
// ============================================================================
void cAction_InitElementsVariants(void)
{
int i;

for (i = lACTIONS_CONCENTRE.size()-1; i>=0; i--)
  {
  delete lACTIONS_CONCENTRE[i];
  lACTIONS_CONCENTRE.pop_back();
  };

for (i = lACTIONS_MAIS.size()-1; i>=0; i--)
  {
  delete lACTIONS_MAIS[i];
  lACTIONS_MAIS.pop_back();
  };

for (i = lACTIONS_FOIN.size()-1; i>=0; i--)
  {
  delete lACTIONS_FOIN[i];
  lACTIONS_FOIN.pop_back();
  };

for (i = lACTIONS_PATURAGE.size()-1; i>=0; i--)
  {
  delete lACTIONS_PATURAGE[i];
  lACTIONS_PATURAGE.pop_back();
  };

for (i = lACTIONS_ENSILAGE.size()-1; i>=0; i--)
  {
  delete lACTIONS_ENSILAGE[i];
  lACTIONS_ENSILAGE.pop_back();
  };

for (i = lACTIONS_FAUCHE.size()-1; i>=0; i--)
  {
  delete lACTIONS_FAUCHE[i];
  lACTIONS_FAUCHE.pop_back();
  };

for (i = lACTIONS_AZOTE.size()-1; i>=0; i--)
  {
  delete lACTIONS_AZOTE[i];
  lACTIONS_AZOTE.pop_back();
  };
};


// ============================================================================
// cActionConcentre : constructeur
// ============================================================================
cActionConcentre::cActionConcentre (float q)
{
q_c = q;
Jour = Temps.J;

lACTIONS_CONCENTRE.push_back(this);
}

// ============================================================================
// cActionConcentre.Realisable : test action realisable
// ============================================================================
bool cActionConcentre::Realisable(void)
{
return true;
}


// ============================================================================
// cActionMais : constructeur
// ============================================================================
cActionMais::cActionMais (float q)
{
q_m = q;
Jour = Temps.J;

lACTIONS_MAIS.push_back(this);
}

// ============================================================================
// cActionMais.Realisable : test action realisable 
// ============================================================================
bool cActionMais::Realisable(void)
{
if (q_m == MAIS_COMPLEMENT)
    throw (new cAnomalie (" Dans la methode cActionMais.Realisable, appel sans argument avec une quantite de mais valant 'complement'.") );
if (q_m * Troupeau.n > StockMais.Etat(Temps.J))
   return false;
else
   return true;
}

// ============================================================================
// cActionMais.Realisable : MAj q_m en Kg et test si action realisable
// le complement en mais n est accepte que s il n y a pas de paturage
// ============================================================================
bool cActionMais::Realisable(float qc, float qf)
{
if (q_m == MAIS_COMPLEMENT)
    q_m = Troupeau.Vache.V_M[Temps.J] - qc - qf;
else
   throw (new cAnomalie (" Dans la methode cActionMais.Realisable, appel avec argument avec une quantite de mais ne valant pas 'complement'.") );
return Realisable();
}


// ============================================================================
// cActionFoin : constructeur
// ============================================================================
cActionFoin::cActionFoin (float q)
{
q_f = q;
Jour = Temps.J;

lACTIONS_FOIN.push_back(this);
}

// ============================================================================
// cActionFoin.Realisable : test action realisable
// ============================================================================
bool cActionFoin::Realisable(void)
{
if (q_f * Troupeau.n > StockFoin.Etat(Temps.J))
   return false;
else
   return true;
}


// ============================================================================
// cActionPaturage : constructeur
// ============================================================================
cActionPaturage::cActionPaturage (cParcelle* parcelle, DUREE_PATURAGE duree_paturage)
{
q_h_e = QUANTITE_INCONNUE;
Parcelle = parcelle;
DureePaturage = duree_paturage;
Jour = Temps.J;

lACTIONS_PATURAGE.push_back(this);
}

// ============================================================================
// cActionPaturage.Realisable : test action realisable
// ============================================================================
bool cActionPaturage::Realisable(void)
{
return true;
}


// ============================================================================
// cActionEnsilage : constructeur
// ============================================================================
cActionEnsilage::cActionEnsilage (cParcelle* parcelle)
{
Parcelle = parcelle;
Jour = Temps.J;

lACTIONS_ENSILAGE.push_back(this);
}

// ============================================================================
// cActionEnsilage.Realisable : test action realisable
// ============================================================================
bool cActionEnsilage::Realisable(void)
{
return true;
}


// ============================================================================
// cActionFauche : constructeur
// ============================================================================
cActionFauche::cActionFauche (cParcelle* parcelle)
{
Parcelle = parcelle;
Jour = Temps.J;

lACTIONS_FAUCHE.push_back(this);
}

// ============================================================================
// cActionFauche.Realisable : test action realisable
// ============================================================================
bool cActionFauche::Realisable(void)
{
return true;
}


// ============================================================================
// cActionAzote : constructeur
// ============================================================================
cActionAzote::cActionAzote (float q,cParcelle* parcelle)
{
q_N = q;
Parcelle = parcelle;
Jour = Temps.J;

lACTIONS_AZOTE.push_back(this);
}

// ============================================================================
// cActionAzote.Realisable : test action realisable
// ============================================================================
bool cActionAzote::Realisable(void)
{
return true;
}

