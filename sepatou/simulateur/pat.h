// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : pat.h
contenu : definition des define et enum
******************************************************************************/

#ifndef _PAT_H
#define _PAT_H



// DEFINITION DE CHEMINS
#define CHEMIN_SIMULATEUR_TXT "tmp/Simulateur.cfg"
#define  CHEMIN_STRATEGIE_TXT "tmp/Strategie.cfg"
#define  CHEMIN_EXPLOITATION_TXT "tmp/Exploitation.cfg"
#define  CHEMIN_SORTIES_TXT "tmp/Sorties.cfg"

#define CHEMIN_CLIMATS "../climats/"


// DEFINITIONS DIVERSES GLOBALES
// nb maximum d annees generees
#define ANNEE_GENEREE 3000
 
// nb maximum de jours simules
#define NB_JOURS 366

#define NB_MAX_FO_TYPE 100
#define NB_MAX_INDICATEURS 100
#define NB_INDICATEURS_PREDEFINIS 5


// nb max de variables utilisateur LnU par type (int, float, ...)
#define NB_VAR_SDP 150
#define NB_VAR_SDO 150

#define DATE_INCONNUE -1



// enum pour resultat de l appel de certaines methodes
enum RESULTAT {OK, PB, RESULTAT_INCONNU};  

// DEFINE ET ENUM POUR VALEUR STRING DE VARIABLES DE DECISION PLANIFICATION
enum ACTIVITE {ACTIVITE_ALIMENTATION_CONSERVEE, ACTIVITE_PATURAGE, 
                   ACTIVITE_FAUCHE, ACTIVITE_FERTILISATION, ACTIVITE_COORDINATION,
                    NB_ACTIVITES, ACTIVITE_INCONNU};
#define CONCENTRE_OUI -1
#define CONCENTRE_1_3 -2
#define CONCENTRE_2_3 -3
#define CONCENTRE_NON -4
#define CONCENTRE_INCONNU -5
#define MAIS_COMPLEMENT -1
#define MAIS_NON -2
#define MAIS_INCONNU -3
enum ALIMENTATION_FOIN {FOIN_NON, FOIN_LEST, FOIN_LEST_REGULATEUR, FOIN_INCONNU};
enum ALIMENTATION_HERBE {HERBE_JOUR, HERBE_JOUR_NUIT, HERBE_NON, HERBE_INCONNU};
enum REGIME_AZOTE {REGIME_AZOTE_FAIBLE, REGIME_AZOTE_MOYEN, REGIME_AZOTE_FORT, 
                        REGIME_AZOTE_INCONNU};

// ENUM POUR VALEUR STRING DE VARIABLES DE DECISION OPERATOIRE
enum ACTION {ACTION_CONCENTRE, ACTION_MAIS, ACTION_FOIN, ACTION_PATURAGE,
                  ACTION_ENSILAGE, ACTION_FAUCHE, ACTION_AZOTE, NB_ACTIONS, 
                  ACTION_INCONNU};
enum DUREE_PATURAGE {DUREE_PATURAGE_JOUR, DUREE_PATURAGE_JOUR_NUIT,
                     DUREE_PATURAGE_INCONNU};

// ENUM POUR REGLE DE PLANIFICATION
// regle de planification reutilisable ou pas
enum REUTILISATION {REUTILISABLE, NON_REUTILISABLE, REUTILISATION_INCONNU};
// regle de planification active (a considerer) ou non
enum RP_ACTIVE {ACTIVE, NON_ACTIVE, RP_ACTIVE_INCONNU};

// DEFINE ET ENUM SYSTEME DE PRODUCTION
enum CARACTERISTIQUE_PARCELLE {CARACTERISTIQUE_PATURABLE, 
			       CARACTERISTIQUE_MIXTE, CARACTERISTIQUE_INCONNU};
enum TYPE_SOL {TYPE_SABLEUX, TYPE_LIMONO_SABLEUX, TYPE_LIMONEUX, 
               TYPE_LIMONO_ARGILEUX, TYPE_ARGILEUX, TYPE_INCONNU };
enum ELOIGNEMENT {LOIN, PRES, INCONNU};
enum ESPECE_HERBE {DACTYLE, RAY_GRASS_ANGLAIS, FETUQUE_ELEVEE};
enum ANNEE_VELAGE {VELAGE_ANNEE_PRECEDENTE, VELAGE_ANNEE_COURANTE};
enum QUALITE_FOIN {QUALITE_BONNE, QUALITE_MOYEN, QUALITE_PASSABLE};
enum TYPE_OTER_PARCELLE {TYPE_OTER_COUPE, TYPE_OTER_PATURAGE, TYPE_OTER_INCONNU};

#define MS_INCONNUE -1
#define IF_INCONNU -1
#define Dmoy_INCONNUE -1
#define IN_INCONNU -1
#define EAUd_INCONNUE -1
#define Q_H_O_INCONNUE -1
#define IH_INCONNU -1
#define SU_INCONNUE -1
#define ETR_INCONNUE -1
#define ETP_INCONNUE -1

#define QUANTITE_INCONNUE -1
#define ENERGIE_INCONNUE -1

#define CALCULER_EI -9999

#endif
