// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cResultat.h
contenu : definition de la classe cResultat
******************************************************************************/
#ifndef _CRESULTAT_H
#define _CRESULTAT_H


#include <string>
#include <fstream>

#include "pat.h"

using namespace std;

// ============================================================================
// cResultat : classe g�rant les resultats : ecriture sur disque
// ============================================================================
class cResultat
{
public:  //--------------------------------------------------------------------
ofstream fTrace;
bool Sauver_trace;
bool Sauver_Fonctions;
bool Sauver_Indicateurs;
bool Sauver_Statistiques;

void InitElementsInvariants(void);// initialisation des variables a sauvegarder
void InitElementsVariants(void); // ouverture eventuelle du fichier trace
void MemoriserResultat(RESULTAT r);// ecriture sur disque


private:  //-------------------------------------------------------------------
bool Sauver_agenda;
bool Sauver_MS;
bool Sauver_MSc;
bool Sauver_MSs;
bool Sauver_IF;
bool Sauver_Dmoy;
bool Sauver_IN;
bool Sauver_EAUd;
bool Sauver_IH;
bool Sauver_SU;
bool Sauver_V_M;
bool Sauver_q_l_p;
bool Sauver_q_l_r;
bool Sauver_Estockee;

int ZDateMH;
int ZDateFin1erCycle;
int ZDateSortieNuit;
int ZDateFermetureSilo;
int ZDateOuvertureSilo;
int ZDateEnsilage;
int ZDate1ereFauche;
float ZQfauchee;
float ZSfauchee;

void EcrireArretProbleme(string chemin_annee, string chemin_tmp);
                                        // ecriture causes problemes
void CalculerSorties (void);
void EcrireParcelles(string chemin);    // ecrire parcelles
void EcrireTroupeau(string chemin);     // ecrire troupeau
void EcrireActions(string chemin);      // ecriture des actions
void EcrireFonctions(string chemin);    
void EcrireIndicateurs(string chemin);    
void EcrireStatistiques(string chemin);    
};



#endif
