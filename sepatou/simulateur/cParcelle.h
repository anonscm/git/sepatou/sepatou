// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cParcelle.h
contenu : definition de la classe cParcelle
******************************************************************************/
#ifndef _CPARCELLE_H
#define _CPARCELLE_H

#include <string>

using namespace std;

#include "pat.h"

class cClimat;

// ============================================================================
// cParcelle : representation d une parcelle
// ============================================================================
class cParcelle
{
public:  //--------------------------------------------------------------------
cParcelle(string nom,
          float surface,
          int ru, 
          CARACTERISTIQUE_PARCELLE caracteristique, 
          TYPE_SOL type_sol, 
          ELOIGNEMENT eloignement,
          ESPECE_HERBE espece);           // constructeur

string Nom;                               // nom
float S;                                  // surface en ha
int RU;                                   // reserve utile en mm
CARACTERISTIQUE_PARCELLE Caracteristique; // caracteristique
TYPE_SOL TypeSol;                         // type de sol
ELOIGNEMENT Eloignement;                  // eloignement
ESPECE_HERBE Espece;                      // espece de l'herbe

float SPs_max;                            // duree perte des feuilles 
                                          // par senescence pour MS
float SPs_min;                            // duree perte des feuilles 
                                          // par senescence pour IF
float SPv;                                // caract de la structure du couvert
float SPd;                                // digestibilite maximale des feuilles

float MS[NB_JOURS];                   // matiere seche  de l herbe en g/m2
float MSc[NB_JOURS];                  // matiere seche creee en g/m2
float MSs[NB_JOURS];                  // matiere seche perdue par senescence en g/m2
float MS80[NB_JOURS];                 // matiere seche > 80 de l herbe en g/m2
float IF[NB_JOURS];                   // indice foliaire de l herbe
float Dmoy[NB_JOURS];                 // digestibilite moyenne de l herbe
float IN[NB_JOURS];                   // indice de nutrition azotee herbe
float IH[NB_JOURS];                   // indice hydrique
float SU[NB_JOURS];                   // surface utile
float EAUd[NB_JOURS];                 // eau disponible
float q_h_o[NB_JOURS];                // herbe offerte par vache
int   Joter;                          // jour derniere coupe ou paturage
int   JfinDerniereUtilisation;        // jour fin derniere utilisation
int   NbPaturage;                     // nb de groupe d'actions Paturage

void InitElementsVariants(void);          // init en debut de simulation
void MAJJourDebutPaturage(void);          // MAJ de Jdp en debut de paturage
void Evoluer(void);                       // evoluer d un pas de temps

float CalculerIntegraleDstrate (float q1, float q2);
bool ResoudreEquationAvecIntegraleDstrate(float q2, float V, float V1, float V2, float & x);


private:  //-------------------------------------------------------------------
float ETR[NB_JOURS];                   // evapo-transpiration reelle

int nbBouses;                          // nombre de bouses sur la parcelle
int Jepiaison;                         // jour de l'epiaison
int Jdp;                               // jour du debut de paturage
TYPE_OTER_PARCELLE ActionOter;         // derniere action otant de l'herbe

// mise au point du modele
float MSe[NB_JOURS];                   // matiere seche ingeree en g/m2

float Astrate;
float Bstrate;
float Cstrate;
float qh;
int   Jstrate;                         // jour ou Dstrate a ete calcule

float MS_JSPs_max;                         
int   JSPs_max;
float IF_JSPs_min;                         
int   JSPs_min;

void MS_Evoluer_Naturel(cClimat & climat);
void MS_Evoluer_Paturage(cClimat & climat);
void MS_Evoluer_Coupe(void);

void IF_Evoluer_Naturel(cClimat & climat);
void IF_Evoluer_Paturage_Coupe(void);

void Dmoy_Evoluer_Naturel(void);
void Dmoy_Evoluer_Paturage(void);
void Dmoy_Evoluer_Coupe(void);

void IN_Evoluer_Naturel(cClimat & climat);
void IN_Evoluer_Azote(cClimat & climat, float q);

void SU_Evoluer_Naturel(cClimat & climat);
void SU_Evoluer_Paturage(DUREE_PATURAGE d);

void IH_Evoluer(cClimat & climat);
void EAUd_Evoluer(cClimat & climat);
void q_h_o_Evoluer(void);

float CalculerMSc(cClimat & climat, float valeur);
void MAJJepiaison(cClimat & climat);
int CalculerJSPs_max(cClimat & climat, int j_courant);
int CalculerJSPs_min(cClimat & climat, int j_courant);
float CalculerSommeMSc(int jdebut);     // de jdebut compris a Temps.J compris
float CalculerSommeMSs(int jdebut);     // de jdebut compris a Temps.J compris

void VerifierValeurs(void);
};



#endif
