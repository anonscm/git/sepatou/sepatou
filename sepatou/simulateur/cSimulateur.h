// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSimulateur.h
contenu : definition de la classe cSimulateur
******************************************************************************/
#ifndef _CSIMULATEUR_H
#define _CSIMULATEUR_H 1


#include <vector>
#include <string>

using namespace std;

#include "cClimat.h"

// ============================================================================
// cSimulateur : gestion du deroulement des simulations et des sauvegardes
// ============================================================================
class cSimulateur
{
public:  //--------------------------------------------------------------------
int          JourDebut;             // jour de debut de simulation
int          MoisDebut;             // mois de debut de simulation
int          JourFin;               // jour de fin de simulation 
int          MoisFin;               // mois de fin de simulation
int          Annee;                 // annee simulee

void FaireSimulations(void);        // Realisation des simulations

// cet attribut et ces m�thodes ont �t� mis en public
// pour pouvoir etre accessibles par l'optimisateur
int          NbClimatsGeneres;      // nombre de climats generes

void InitElementsInvariants(void);  // Initialisation des elements invariants
                                    // lors des simulations
void Simuler(cClimat* climat = NULL); // Realisation d une simulation
void LibererMemoire(void);          // Liberation de memoire dynamique

private:  //-------------------------------------------------------------------
vector <int> lAnneesClimats;        // liste des annees climatiques reelles
string       Localisation;          // localisation du systeme de production
unsigned int Seed;                  // graine pour la g�n�ration de climat

void InitElementsVariants(cClimat* climat);    
// Init debut de simulation des elts variants lors des simulations
};


#endif
