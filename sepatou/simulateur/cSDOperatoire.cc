// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSOperatoire.cc
contenu : definition des methodes de la classe cSDOperatoire
******************************************************************************/


#include <vector>

#include "pat.h"

#include "cSDOperatoire.h"

#include "cResultat.h"
#include "cAction.h"
#include "cPlan.h"
#include "cRegleOperatoire.h"
#include "cSDPlanificateur.h"
#include "cParcelle.h"
#include "cTemps.h"
#include "cEvenement.h"
class cEnsembleParcelles;

extern cResultat Resultat;
extern cParcelle* etable;
extern cEnsembleParcelles EnsembleParcellesVide;
extern cSDPlanificateur SDPlanificateur;
extern cTemps Temps;

extern cPlanAlimentationConservee PlanAlimentationConservee;
extern cPlanPaturage PlanPaturage;
extern cPlanFauche PlanFauche;
extern cPlanFertilisation PlanFertilisation;
extern cPlanCoordination PlanCoordination;

extern cEvenement Evt_PB_SBExecutant;
extern cEvenement Evt_NouveauJour;
extern cEvenement Evt_ModifPlans;

extern void CreerReglesOperatoires(void);
extern float Arrondir (float f);

extern cPlan* cPlan_PlanDeActivite(ACTIVITE activite); 
extern void cPlan_MAJPlans(void);                    

// ============================================================================
// InitElementsInvariants :initialisation des elts invariants 
//                         lors des simulations
// ============================================================================
void cSDOperatoire::InitElementsInvariants(void)
{
CreerReglesOperatoires();

InitlEvtADetecter();    // mise a jour de lEvtADetecter
}


// ============================================================================
// Init :initialisation des elts au jour de debut de simulation
// ============================================================================
void cSDOperatoire::InitElementsVariants(void)
{
int i;

InitVariablesDecision();

RAZlActionsARealiser();
NoJourlACTIONS = DATE_INCONNUE;

for (i=0; i<NB_VAR_SDO; i++)
  {
   bVariable[i] = false;
   iVariable[i] = 0;
   fVariable[i] = 0.0;
   pVariable[i] = NULL;
   eVariable[i] = EnsembleParcellesVide;
   sVariable[i] = "";
  };

Probleme = false;
}

// ============================================================================
// LibererMemoire : liberer memoire dynamique
// ============================================================================
void cSDOperatoire::LibererMemoire(void)
{
for (int i=0; i<NB_ACTIVITES; i++)
  for ( int k=lRegles[i].size()-1; k>=0; k--)
    {
      delete lRegles[i][k];
      lRegles[i].pop_back();
    };

lEvtADetecter.clear();

RAZlActionsARealiser();
}

// ============================================================================
// InitVariablesDecision : init variables de decision
// ============================================================================
void cSDOperatoire::InitVariablesDecision(void)
{
ApportConcentre = 0.0;
ApportMais = 0.0;
ApportFoin = 0.0;
DeplacerTroupeau = NULL;
DureePaturage = DUREE_PATURAGE_INCONNU;
Ensiler = EnsembleParcellesVide;
Faucher = EnsembleParcellesVide;
ApportAzote = -1;
Fertiliser = EnsembleParcellesVide;

for (int i=0; i<NB_ACTIONS; i++)
  OrdreActions[i]=ACTION_INCONNU;
}


// ============================================================================
// RAZlActionsARealiser : vide les listes des actions a realiser
// ============================================================================
void cSDOperatoire::RAZlActionsARealiser(void)
{
lActionsConcentreARealiser.clear();
lActionsMaisARealiser.clear();
lActionsFoinARealiser.clear();
lActionsPaturageARealiser.clear();
lActionsEnsilageARealiser.clear();
lActionsFaucheARealiser.clear();
lActionsAzoteARealiser.clear();
}

// ============================================================================
// AjouterRegle : ajouter une regle
// ============================================================================
void cSDOperatoire::AjouterRegle(cRegleOperatoire* r)
{
lRegles[r->Activite].push_back(r);
}


// ============================================================================
// InitlEvtADetecter : mise a jour de la liste des evts a detecter 
// ============================================================================
void cSDOperatoire::InitlEvtADetecter(void)
{
// on vide la liste des evts a detecter
lEvtADetecter.clear();

lEvtADetecter.push_back(&Evt_NouveauJour);
lEvtADetecter.push_back(&Evt_ModifPlans);
lEvtADetecter.push_back(&Evt_PB_SBExecutant);
}


// ============================================================================
// TraiterEvenements : traiter les evts donnes en arguments qui ont ete detecte
// ============================================================================
RESULTAT cSDOperatoire::TraiterEvenements(vector <cEvenement*> l_evt)
{
if (Resultat.Sauver_trace)
  Resultat.fTrace << "EXAMEN DES REGLES OPERATOIRES" << endl;

if (Evt_PB_SBExecutant.DETECTER())
  {
  if (Resultat.Sauver_trace)
    Resultat.fTrace << "probleme : le systeme biotechnique executant  ne peut pas executer une action" << endl;

  Probleme = true;
  return PB;   // aucune regle pour le gerer
  }
else
  {// evenement modif plans ou nouveau jour
  // mise a jour des plans a realiser
  cPlan_MAJPlans();

  // mise a jour des variables de decision et creation des actions decidees
  DeciderActions();

  Probleme = false;
  return OK;
  };
}


// ============================================================================
// DeciderActions : decider des actions a realiser
// ============================================================================
void cSDOperatoire::DeciderActions(void)
{
ACTIVITE activite;
int i;

RAZlActionsARealiser();
InitVariablesDecision();

// Essai de declenchement des regles a considerer 
// avec MAJ de variables de decision
for (i=0; i<NB_ACTIVITES; i++)  
  {
  activite = PlanCoordination.OrdreActivites[i]; // on considere les activites
                                                   // dans l ordre specifie
  if ( cPlan_PlanDeActivite(activite)->DirectiveActive != NULL )
    // une directive s applique
    for (unsigned int k=0; k<lRegles[activite].size(); k++)  // pour chaque regle
                                                    // de l activite
      {
       if (Resultat.Sauver_trace)
         Resultat.fTrace << "si " << lRegles[activite][k]->Nom<< endl;
       if (lRegles[activite][k]->SI == NULL) // partie SI non explicitee car 
	   {                                 // toujours vraie
	   if (Resultat.Sauver_trace)
              Resultat.fTrace << "alors " << lRegles[activite][k]->Nom<< endl;
           lRegles[activite][k]->ALORS();
           }
       else
            if (lRegles[activite][k]->SI())         // declenchement de la regle
	        {
		if (Resultat.Sauver_trace)
                   Resultat.fTrace << "alors " << lRegles[activite][k]->Nom<< endl;
                lRegles[activite][k]->ALORS();
                }
            else
                if (lRegles[activite][k]->SINON != NULL)
	                {
                        if (Resultat.Sauver_trace)
                            Resultat.fTrace << "sinon " << lRegles[activite][k]->Nom<< endl;
                        lRegles[activite][k]->SINON();
                        };
       };
  };

if (Resultat.Sauver_trace)
  Resultat.fTrace << "ACTIONS DECIDEES" << endl;
// Creation des actions decidees et MAJ de lActionsARealiser
if (ApportConcentre != 0.0)
  {
  cActionConcentre* Ac = new cActionConcentre(ApportConcentre);
  lActionsConcentreARealiser.push_back(Ac);
  if (Resultat.Sauver_trace)
     Resultat.fTrace << "Action Concentre : " << Arrondir(Ac->q_c) << " Kg/vache" << endl;
  };
if (ApportMais != 0.0)
  {
  cActionMais* Am = new cActionMais(ApportMais);
  lActionsMaisARealiser.push_back(Am);
  if (Resultat.Sauver_trace)
    if (Am->q_m == MAIS_COMPLEMENT)
       Resultat.fTrace << "Action Mais : complement" << endl;
    else
       Resultat.fTrace << "Action Mais : " << Arrondir(Am->q_m) << " Kg/vache" << endl;
  };
if (ApportFoin != 0.0)
  {
  cActionFoin* Afo = new cActionFoin(ApportFoin);
  lActionsFoinARealiser.push_back(Afo);
  if (Resultat.Sauver_trace)
    Resultat.fTrace << "Action Foin : " << Arrondir(Afo->q_f) << " Kg/vache" << endl;
  };
if ( (DeplacerTroupeau != NULL) && (DeplacerTroupeau != etable) &&
     ( (DureePaturage == DUREE_PATURAGE_JOUR) ||
       (DureePaturage == DUREE_PATURAGE_JOUR_NUIT) ) &&
     (DeplacerTroupeau->q_h_o[Temps.J] > 0)  )
  {
  cActionPaturage* Ap = new cActionPaturage(DeplacerTroupeau, DureePaturage);
  lActionsPaturageARealiser.push_back(Ap);
  if (Resultat.Sauver_trace)
    Resultat.fTrace << "Action Paturage : " << Ap->Parcelle->Nom << endl;
  };
if (Ensiler.l.size() != 0)
   for (i=Ensiler.l.size()-1; i>=0; i--)
       {
        cActionEnsilage* Ae = new cActionEnsilage( Ensiler.l[i]);
        lActionsEnsilageARealiser.push_back(Ae);
        if (Resultat.Sauver_trace)
           Resultat.fTrace << "Action Ensilage : " << Ae->Parcelle->Nom << endl;
       };
if (Faucher.l.size() != 0)
   for (i=Faucher.l.size()-1; i>=0; i--)
       {
        cActionFauche* Af = new cActionFauche( Faucher.l[i]);
        lActionsFaucheARealiser.push_back(Af);
        if (Resultat.Sauver_trace)
           Resultat.fTrace << "Action Fauche : " << Af->Parcelle->Nom << endl;
       };
if ((Fertiliser.l.size() != 0) && (ApportAzote != -1))
   for (i=Fertiliser.l.size()-1; i>=0; i--)
       {
       cActionAzote* Aa = new cActionAzote(ApportAzote,Fertiliser.l[i]);
       lActionsAzoteARealiser.push_back(Aa);
        if (Resultat.Sauver_trace)
           Resultat.fTrace << "Action Azote : " << Aa->Parcelle->Nom << endl;
       };
NoJourlACTIONS = Temps.J;
}



