// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cTemps.h
contenu : definition de la classe cTemps
******************************************************************************/
#ifndef _CTEMPS_H
#define _CTEMPS_H 1

#include "pat.h"

// ============================================================================
// cTemps : representation du temps
// ============================================================================
class cTemps
{
public:  //--------------------------------------------------------------------

int  J;                          // numero du jour courant
int  Annee;                      // annee simulee

int  NoJour(int j, int m);       // Calcul du numero de jour d une date
                                 // pour l annee courante
int  NoJour(int j, int m, int a);// Calcul du numero de jour d une date
                                 // pour l annee specifiee
void InitElementsVariants(int annee, int jour_debut, int mois_debut, 
                      int jour_fin, int mois_fin);  
                                 // Init au jour de debut de simulation
RESULTAT Ecouler(void);          // ecoulement du temps jusqu au NoJourFin
                                 // avec appel de SISurveillance 
                                 // a chaque pas de temps

private:  //-------------------------------------------------------------------
int  NoJourFin;                   // numero du jour de fin de simulation

};


#endif
