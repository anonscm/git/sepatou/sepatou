// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSimulateur.cc
contenu : definition des methodes de la classe cSimulateur
******************************************************************************/

#include <vector>
#include <string>
#include <fstream>

#include "pat.h"

#include "cSimulateur.h"

#include "cTemps.h"
#include "cResultat.h"
#include "cEnvironnementExterieur.h"
#include "cResultat.h"
#include "cDecision.h"
#include "cSystemeProduction.h"
#include "cAnomalie.h"

extern cTemps Temps;
extern cResultat Resultat;
extern cEnvironnementExterieur EnvironnementExterieur;
extern cSystemeProduction SystemeProduction;


// ============================================================================
// Simuler : Realisation d'une simulation
// ============================================================================
void cSimulateur::Simuler(cClimat* climat)
{
RESULTAT resultat;  

try
   {
     InitElementsVariants(climat);
     resultat = Temps.Ecouler();
     Resultat.MemoriserResultat( resultat );
   }
catch (cAnomalie* anomalie) 
  {
    if (Resultat.Sauver_trace)       // fermeture eventuelle 
      Resultat.fTrace.close();       // du fichier trace de l annee
    anomalie->Afficher();
  }
}


// ============================================================================
// FaireSimulations : Realisation des simulations et des sauvegardes sur disque
// ============================================================================
void cSimulateur::FaireSimulations(void)
{
try
   {
     InitElementsInvariants();
     
     for (int i=0; i<NbClimatsGeneres; i++)
       {
	 Annee = ANNEE_GENEREE+i;     
	 Simuler();
       }

     for (unsigned int i=0; i<lAnneesClimats.size(); i++)
       {
	 Annee = lAnneesClimats[i];
	 Simuler();
       }
   }
catch (cAnomalie* anomalie) {
    anomalie->Afficher();}

LibererMemoire();
}


// ============================================================================
// InitElementsInvariants : initialisation de tous les elements invariants 
//                          de l'application lors des simulations
// ============================================================================
void cSimulateur::InitElementsInvariants(void)
{
ifstream fSimulateur;
int annee;
string CheminSimulateur = CHEMIN_SIMULATEUR_TXT;
string commentaire;

// pour le noyau de simulation
fSimulateur.open(CheminSimulateur.c_str(), ios::in);
if (!fSimulateur) 
    throw (new cAnomalie ("Dans la methode cSimulateur.InitElementsInvariants, probleme pour ouvrir le fichier  : " + CheminSimulateur ) );

fSimulateur >> commentaire >> commentaire >> JourDebut;
fSimulateur >> commentaire >> commentaire >> MoisDebut;
fSimulateur >> commentaire >> commentaire >> JourFin;
fSimulateur >> commentaire >> commentaire >> MoisFin;

fSimulateur >> commentaire >> commentaire >> Localisation;

fSimulateur >> commentaire >> commentaire >> NbClimatsGeneres;
fSimulateur >> commentaire >> commentaire >> Seed;

while (fSimulateur >> annee)
   lAnneesClimats.push_back(annee);

fSimulateur.close();

Resultat.InitElementsInvariants();

// pour l'environnement ext�rieur
EnvironnementExterieur.InitElementsInvariants(Localisation, Seed);

// pour le systeme de production
SystemeProduction.InitElementsInvariants();

// pour les decisions pas d'elements invariants

}


// ============================================================================
// InitElementsVariants : initialisation de tous les elements variants
//                        de l'application lors des simulations
// ============================================================================
void cSimulateur::InitElementsVariants(cClimat* climat)
{
// pour le noyau de simulation
Temps.InitElementsVariants(Annee,JourDebut,MoisDebut, JourFin, MoisFin);
Resultat.InitElementsVariants();

// pour l'environnement ext�rieur
if (climat == NULL)
  EnvironnementExterieur.InitElementsVariants(Annee);
else
  EnvironnementExterieur.InitElementsVariants(climat);

// pour le systeme de production
SystemeProduction.InitElementsVariants();

// pour les decisions
cDecision::InitElementsVariants();
}


// ============================================================================
// LibererMemoire : Liberation de memoire dynamique
// ============================================================================
void cSimulateur::LibererMemoire(void)
{
// pour le noyau de simulation

// pour l'environnement ext�rieur

// pour le systeme de production
SystemeProduction.LibererMemoire();

// pour les decisions
cDecision::LibererMemoire();
}


