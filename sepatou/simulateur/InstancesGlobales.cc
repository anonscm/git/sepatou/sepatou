// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : InstancesGlobales.cc
contenu : definition des instances globales
          le contenu de ce fichier a �t� s�par� de main.cc
          pour pouvoir etre reutilis� par l'optimisateur
******************************************************************************/


#include <vector>

#include "pat.h"

#include "cRandom.h"
#include "cSimulateur.h"
#include "cTemps.h"
#include "cResultat.h"
#include "cEnvironnementExterieur.h"
#include "cClimat.h"
#include "cAction.h"
#include "cDirective.h"
#include "cPlan.h"
#include "cSystemeProduction.h"
#include "cSDPlanificateur.h"
#include "cSDOperatoire.h"
#include "cSBExecutant.h"
#include "cSBBiophysique.h"
#include "cTroupeau.h"
#include "cParcelle.h"
#include "cSIObservation.h"
#include "cSISurveillance.h"
#include "cStock.h"
#include "cEnsembleParcelles.h"
#include "cEvenement.h"

// creation des instances "globales"
cRandom Random;
cSimulateur Simulateur;
cTemps Temps;
cResultat Resultat;

cClimat Climat;
cEnvironnementExterieur EnvironnementExterieur; // attention, apres les climats

cSDPlanificateur SDPlanificateur;
cSDOperatoire SDOperatoire;
cSIObservation SIObservation;
cSISurveillance SISurveillance;
cSBExecutant SBExecutant;
cSBBiophysique SBBiophysique;
cSystemeProduction SystemeProduction;
cStockConcentre StockConcentre;
cStockMais StockMais;
cStockFoin StockFoin;
cStockAzote StockAzote;
cTroupeau Troupeau;
cParcelle* etable = new cParcelle("etable",1,100,CARACTERISTIQUE_INCONNU, TYPE_INCONNU, INCONNU, DACTYLE);

cPlanAlimentationConservee PlanAlimentationConservee;
cPlanPaturage PlanPaturage;
cPlanFauche PlanFauche;
cPlanFertilisation PlanFertilisation;
cPlanCoordination PlanCoordination;

cEnsembleParcelles EnsembleParcellesVide;

extern bool DETECTER_PB_SDOperatoire (void);
extern bool DETECTER_PB_SBExecutant (void);
extern bool DETECTER_NOUVEAU_JOUR (void);
extern bool DETECTER_MODIF_PLANS (void);
cEvenement Evt_PB_SDOperatoire(DETECTER_PB_SDOperatoire);
cEvenement Evt_PB_SBExecutant(DETECTER_PB_SBExecutant);
cEvenement Evt_NouveauJour(DETECTER_NOUVEAU_JOUR);
cEvenement Evt_ModifPlans(DETECTER_MODIF_PLANS);

// declaration de variables globales car impossibilit�
// de creer des attributs de classe objet sans instancier la classe C++
vector <cDirective*> lDIRECTIVES;

vector <cActionConcentre*> lACTIONS_CONCENTRE; // liste des actions Concentre
vector <cActionMais*> lACTIONS_MAIS; // liste des actions Mais
vector <cActionFoin*> lACTIONS_FOIN; // liste des actions Foin
vector <cActionPaturage*> lACTIONS_PATURAGE; // liste des actions Paturage
vector <cActionEnsilage*> lACTIONS_ENSILAGE; // liste des actions Ensilage
vector <cActionFauche*> lACTIONS_FAUCHE; // liste des actions Fauche
vector <cActionAzote*> lACTIONS_AZOTE; // liste des actions Azote

