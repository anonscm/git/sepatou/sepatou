// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cTroupeau.cc
contenu : definition des methodes des classescVache et cTroupeau
******************************************************************************/

#include <vector>
#include <math.h>

#include "cTroupeau.h"

#include "pat.h"
#include "cTemps.h"
#include "cAction.h"
#include "cSBExecutant.h"
#include "cParcelle.h"
#include "cStock.h"
#include "cAnomalie.h"

extern cTemps Temps;
extern cSBExecutant SBExecutant;
extern cStockFoin StockFoin;
extern float min(float x1, float x2);
extern float max(float x1, float x2);



// ============================================================================
// cVache.InitElementsInvariants
// ============================================================================
void cVache::InitElementsInvariants(float q,int j,int m,ANNEE_VELAGE a)
{
q_l_M = q;
JourVelage = j;
MoisVelage = m;
NomAnneeVelage = a;

CE_c = 1.10;
CE_m = 0.90;
if (StockFoin.Qualite == QUALITE_BONNE)
  CE_f = 0.80;
else
  if (StockFoin.Qualite == QUALITE_MOYEN)
    CE_f = 0.70;
  else
    if (StockFoin.Qualite == QUALITE_PASSABLE)
      CE_f = 0.60;
    else
      throw (new cAnomalie ("Dans la methode cVache.InitElementsInvariants, la valeur de la qualite de foin n'est pas permise.") );

CV_m = 1.0;
CV_f = CV_m;
CV_c = 0.6 * CV_m;

if (NomAnneeVelage == VELAGE_ANNEE_PRECEDENTE)
  AnneeVelage = Temps.Annee - 1;
else 
  if (NomAnneeVelage == VELAGE_ANNEE_COURANTE)
     AnneeVelage = Temps.Annee;
  else 
     throw (new cAnomalie ("Dans la methode cVache.InitElementsVariants, la valeur de l'ann�e de velage n'est pas permise.") );
Init_q_l_p();
Init_V_M();  // attention, apres q_l_p 
}

// ============================================================================
// cVache.InitElementsVariants : init en debut de simulation
// ATTENTION : apres initialisation de Temps.Annee
// ============================================================================
void cVache::InitElementsVariants(void)
{
for (int i=0; i<NB_JOURS; i++)
  {
  q_l_r[i] = 0;
  q_c_e[i] = 0;
  q_m_e[i] = 0;
  q_f_e[i] = 0;
  q_h_e[i] = 0;
  Estockee[i] = 0;

  // mise au point du modele
  Dmoy_ev[i] = -1;    
  };
}


// ============================================================================
// cVache.Evoluer : evoluer d un pas de temps
// ============================================================================
void cVache::Evoluer(void)
{
float q_l_rMAX;
bool paturage_max;

// action Concentre ----------------------------------------------------------
if ( SBExecutant.lActionsConcentreASimuler.size() == 0)
  q_c_e[Temps.J] = 0;
else 
 if ( SBExecutant.lActionsConcentreASimuler.size() == 1) 
   q_c_e[Temps.J] = SBExecutant.lActionsConcentreASimuler[0]->q_c;
 else
   throw (new cAnomalie ("Dans la methode cVache.Evoluer, il y a plus d'une action Concentre a simuler.") );

// action Foin ----------------------------------------------------------
if ( SBExecutant.lActionsFoinASimuler.size() == 0)
  q_f_e[Temps.J] = 0;
else 
 if ( SBExecutant.lActionsFoinASimuler.size() == 1) 
   q_f_e[Temps.J] = SBExecutant.lActionsFoinASimuler[0]->q_f;
 else
   throw (new cAnomalie ("Dans la methode cVache.Evoluer, il y a plus d'une action Foin a simuler.") );

// action Mais ----------------------------------------------------------
if ( SBExecutant.lActionsMaisASimuler.size() == 0)
  q_m_e[Temps.J] = 0;
else 
 if ( SBExecutant.lActionsMaisASimuler.size() == 1) 
   if (SBExecutant.lActionsMaisASimuler[0]->q_m == MAIS_COMPLEMENT)
     {
      if ( SBExecutant.lActionsPaturageASimuler.size() != 0)
        throw (new cAnomalie ("Dans la methode cVache.Evoluer, il est demande de completer en mais alors qu'il y a une action Paturage") );
      else
        {
        q_m_e[Temps.J] = CalculerComplementMais(q_c_e[Temps.J], q_f_e[Temps.J]);
        SBExecutant.lActionsMaisASimuler[0]->q_m = q_m_e[Temps.J];
        };
     }
   else
      q_m_e[Temps.J] = SBExecutant.lActionsMaisASimuler[0]->q_m;
 else
   throw (new cAnomalie ("Dans la methode cVache.Evoluer, il y a plus d'une action Mais a simuler.") );

// action Paturage -------------------------------------------------------
if ( SBExecutant.lActionsPaturageASimuler.size() == 0)
  q_h_e[Temps.J] = 0;
else 
 if ( SBExecutant.lActionsPaturageASimuler.size() == 1) 
   {
   if ( SBExecutant.lActionsPaturageASimuler[0]->DureePaturage 
                                                   == DUREE_PATURAGE_JOUR )
      paturage_max = false;
   else
      paturage_max = true;

   q_h_e[Temps.J] = Calculerq_h_e(q_c_e[Temps.J], 
				  q_f_e[Temps.J], 
				  q_m_e[Temps.J], 
				  paturage_max);
   SBExecutant.lActionsPaturageASimuler[0]->q_h_e = q_h_e[Temps.J];
   }
 else
   throw (new cAnomalie ("Dans la methode cVache.Evoluer, il y a plus d'une action Paturage a simuler.") );

// calcul production de lait ---------------------------------------------
q_l_rMAX = CalculerProductionLaitReelleMAX(q_c_e[Temps.J], q_m_e[Temps.J], q_f_e[Temps.J], q_h_e[Temps.J]);

if (q_l_rMAX < 0)
  {
    Estockee[Temps.J] = q_l_rMAX * 0.44;
    q_l_r[Temps.J] = 0;
  }
else
  if (q_l_rMAX <= q_l_p[Temps.J])
    {
      Estockee[Temps.J] = 0;
      q_l_r[Temps.J] = q_l_rMAX;
    }
  else
    {
      Estockee[Temps.J] = (q_l_rMAX - q_l_p[Temps.J]) * 0.44;
      q_l_r[Temps.J] = q_l_p[Temps.J];
    }

VerifierValeurs();
}

// ============================================================================
// cVache.CalculerProductionLaitReelleMAX
// ============================================================================
float cVache::CalculerProductionLaitReelleMAX(float q_c_e, float q_m_e, 
                                 float q_f_e, float q_h_e)
{
float E_c_e, E_m_e, E_f_e, E_h_e, E_entretien;

if ( (q_c_e == 0) && (q_m_e == 0) && (q_f_e == 0) && (q_h_e == 0) )
  return 0.0;
else
  {
    if (q_h_e == 0)
      E_entretien = 5.0;
    else
      E_entretien = 6.0;

    E_c_e = CE_c * q_c_e;
    E_m_e = CE_m * q_m_e;
    E_f_e  = CE_f * q_f_e;
    E_h_e = CalculerCE_h(q_h_e) * q_h_e;

    return ( (E_c_e + E_m_e + E_f_e + E_h_e - E_entretien) / 0.44 );
  };
}



// ============================================================================
// cVache.Init_q_l_p
// ============================================================================
void cVache::Init_q_l_p(void)
{
float Njvj;
int NoJourVelage;

NoJourVelage = Temps.NoJour(JourVelage,MoisVelage,AnneeVelage);

for (int j=1; j<NB_JOURS; j++)
    {
      if (Temps.Annee == AnneeVelage)
	{
	  if (j < NoJourVelage)
	    Njvj = -1;
	  else
	    Njvj = j - NoJourVelage;
	}
      else
	Njvj = Temps.NoJour(31,12,AnneeVelage) - NoJourVelage + j;

      if ((Njvj == -1)||(Njvj > 300))
	q_l_p[j] = 0;
      else
	if ((Njvj >= 0) && (Njvj <= 40))
	  q_l_p[j] = q_l_M;
	else
	  q_l_p[j] = q_l_M * pow((1 - 0.0035),(Njvj - 40));
    };
}


// ============================================================================
// cVache.Init_V_M
// ============================================================================
void cVache::Init_V_M(void)
{
for (int j=1; j<NB_JOURS; j++)
    V_M[j] = 22.0 - (8.25 * exp(-0.02 * q_l_p[j]));
}

// ============================================================================
// cVache.CalculerComplementMais
// ============================================================================
float cVache::CalculerComplementMais(float qc, float qf)
{
float V_m_e;

V_m_e = V_M[Temps.J] - CV_c * qc - CV_f * qf;

return ( V_m_e / CV_m );
}


// ============================================================================
// cVache.Calculerq_h_e
// ============================================================================
float cVache::Calculerq_h_e(float qc, float qf, float qm, bool paturage_max)
{
float x, kp;
float V_h_M, qM_h_i, qM_h_p;
cParcelle* p = SBExecutant.lActionsPaturageASimuler[0]->Parcelle; 
float qho = p->q_h_o[Temps.J];

   // 1. calcul herbe maximum ingerable
   // calcul de la capacite volumique disponible pour l'herbe
   V_h_M = V_M[Temps.J] - CV_c * qc - CV_f * qf - CV_m * qm;
   
   if (V_h_M <= 0)
     return 0;
   else
     {
       if (p->ResoudreEquationAvecIntegraleDstrate( qho,
						    V_h_M, 
						    1.2117, 
						    -0.0033, 
						    x) )
	 qM_h_i = qho - x;
       else
	 qM_h_i = qho;
      }

   // 2. Calcul de la quantit� d'herbe maximum paturable par une vache
   if (paturage_max)
     kp = 1;
   else
     kp = 0.80;

   if (qho >= 20)
     qM_h_p = kp * qho;
   else // r�gression sur les points (0,0), (10,7) et (20,20)
     qM_h_p = kp * qho * (0.03 * qho + 0.4 );


   // 3. La quantit� patur�e est le min des 2 valeurs pr�c�dentes
   return min(qM_h_i, qM_h_p);
}



// ============================================================================
// cVache.CalculerCE_h
// ============================================================================
float cVache::CalculerCE_h(float qhe)
{
if (qhe != 0)
  {
    float Dmoy_e;
    cParcelle* p = SBExecutant.lActionsPaturageASimuler[0]->Parcelle;
    float qho = p->q_h_o[Temps.J];
    
    Dmoy_e =  ( p->CalculerIntegraleDstrate(qho - qhe, qho) ) / qhe;
    Dmoy_ev[Temps.J] = 0.72 * Dmoy_e + 19.2;
    
    return ( -0.08 + 0.015 * Dmoy_ev[Temps.J]);
  }
else
  return 0.0;
}

// ============================================================================
// cVache.VerifierValeurs
// ============================================================================
void cVache::VerifierValeurs(void)
{
if ( (q_l_r[Temps.J] < 0) || (q_l_r[Temps.J] > 50))
   throw (new cAnomalie ("Dans la methode cVache.VerifierValeurs, q_l_r est hors intervalle des valeurs permises") );
}


// ============================================================================
// cTroupeau.InitElementsInvariants
// ============================================================================
void cTroupeau::InitElementsInvariants(int  nb, float q,int j,int m,ANNEE_VELAGE a)
{
n = nb;

Vache.InitElementsInvariants( q, j, m, a);
}


// ============================================================================
// cTroupeau.InitElementsVariants : init en debut de simulation
// ============================================================================
void cTroupeau::InitElementsVariants(void)
{
ParcelleCourante = NULL;
Vache.InitElementsVariants();
}

// ============================================================================
// cTroupeau.Evoluer : evoluer d un pas de temps
// ============================================================================
void cTroupeau::Evoluer(void)
{
if (SBExecutant.lActionsPaturageASimuler.size() != 0)
   ParcelleCourante = SBExecutant.lActionsPaturageASimuler[0]->Parcelle;
Vache.Evoluer();
}


