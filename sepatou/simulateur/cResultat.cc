// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cResultat.cc
contenu : definition des methodes de la classe cResultat
******************************************************************************/

#include <vector>
#include <string>
#include <math.h>

#include "cResultat.h"

#include "pat.h"
#include "cSimulateur.h"
#include "cTemps.h"
#include "cAction.h"
#include "cSIObservation.h"
#include "cIndicateur.h"
#include "cFoInterpretation.h"
#include "cSDPlanificateur.h"
#include "cSBBiophysique.h"
#include "cSBExecutant.h"
#include "cParcelle.h"
#include "cTroupeau.h"
#include "cAnomalie.h"

extern cSimulateur Simulateur;
extern cTemps Temps;
extern cSIObservation SIObservation;
extern cSDPlanificateur SDPlanificateur;
extern cSBBiophysique SBBiophysique;
extern cSBExecutant SBExecutant;
extern cTroupeau Troupeau;

extern vector <cActionConcentre*> lACTIONS_CONCENTRE;
extern vector <cActionMais*> lACTIONS_MAIS;
extern vector <cActionFoin*> lACTIONS_FOIN;
extern vector <cActionPaturage*> lACTIONS_PATURAGE;
extern vector <cActionEnsilage*> lACTIONS_ENSILAGE;
extern vector <cActionFauche*> lACTIONS_FAUCHE;
extern vector <cActionAzote*> lACTIONS_AZOTE;

extern string itoa (int A);
extern float Arrondir (float f);


// ============================================================================
// InitElementsInvariants : Initialisation des elements invariants 
// lors des simulations : drapeau des variables a sauvegarder
// ============================================================================
void cResultat::InitElementsInvariants(void)
{
string var;
string CheminSorties;
ifstream fSorties;

// lecture des drapeaux
CheminSorties = CHEMIN_SORTIES_TXT;
fSorties.open(CheminSorties.c_str(), ios::in);
if (!fSorties)
    throw (new cAnomalie ("Dans la methode cResultat::InitElementsInvariants, probleme pour ouvrir le fichier indiquant quelles variables sauvegarder : " + CheminSorties) );

Sauver_trace = false;
Sauver_MS = false;
Sauver_MSc = false;
Sauver_MSs = false;
Sauver_IF = false;
Sauver_Dmoy = false;
Sauver_IN = false;
Sauver_EAUd = false;
Sauver_IH = false;
Sauver_SU = false;
Sauver_V_M = false;
Sauver_q_l_p = false;
Sauver_Estockee = false;
Sauver_agenda = false;
Sauver_Fonctions = false;
Sauver_Indicateurs = false;
Sauver_Statistiques = false;

while (fSorties >> var)
  {
  if (var == "trace") Sauver_trace = true;
  else 
  if (var == "MS")  Sauver_MS = true;
  else
  if (var == "MSc")  Sauver_MSc = true;
  else
  if (var == "MSs")  Sauver_MSs = true;
  else
  if (var == "IF")  Sauver_IF = true;
  else
  if (var == "Dmoy")  Sauver_Dmoy = true;
  else
  if (var == "IN")  Sauver_IN = true;
  else
  if (var == "EAUd")  Sauver_EAUd = true;
  else
  if (var == "IH")  Sauver_IH = true;
  else
  if (var == "SU")  Sauver_SU = true;
  else
  if (var == "V_M")  Sauver_V_M = true;
  else
  if (var == "q_l_p")  Sauver_q_l_p = true;
  else
  if (var == "q_l_r")  Sauver_q_l_r = true;
  else
  if (var == "Estockee")  Sauver_Estockee = true;
  else
  if (var == "chronique")  Sauver_agenda = true;
  else
  if (var == "FONCTIONS")  Sauver_Fonctions = true;
  else
  if (var == "INDICATEURS")  Sauver_Indicateurs = true;
  else
  if (var == "STATISTIQUES")  Sauver_Statistiques = true;
  else
    throw (new cAnomalie ("Dans la methode cResultat::InitElementsInvariants, une variable inconnue est demandee a saubegarder : " + var) );
 };
fSorties.close();
} 


// ============================================================================
// InitElementsVariants : ouverture eventuelle du fichier trace
// ============================================================================
void cResultat::InitElementsVariants(void)
{
string CheminTrace;

if (Sauver_trace)
  {
   CheminTrace = "tmp/" + SDPlanificateur.NomStrategie + "/" 
                                            + itoa(Temps.Annee) + "/trace";
   fTrace.open (CheminTrace.c_str(), ios::out);
   if (!fTrace)
    throw (new cAnomalie ("Dans la methode cResultat::InitElementsVariants, probleme pour ouvrir le fichier : " + CheminTrace) );
  };
}


// ============================================================================
// MemoriserResultat : ecriture sur disque des resultats
// ============================================================================
void cResultat::MemoriserResultat(RESULTAT r)
{
if (Sauver_trace)       // fermeture eventuelle du fichier trace de l annee
  fTrace.close();

string CheminSortie;

CheminSortie = "tmp/" + SDPlanificateur.NomStrategie + "/" + itoa(Temps.Annee) + "/";

if (r != OK)
    EcrireArretProbleme(CheminSortie, "tmp/");
if (Sauver_agenda || Sauver_Statistiques)
  CalculerSorties();

EcrireParcelles(CheminSortie);
EcrireTroupeau(CheminSortie);
EcrireActions(CheminSortie);
EcrireFonctions(CheminSortie);
EcrireIndicateurs(CheminSortie);
EcrireStatistiques("tmp/statistiques/");
}


// ============================================================================
//  EcrireArretProbleme : ecriture causes problemes sur disque
// Si le r�pertoire tmp/strategie/annee existe, un fichier Probleme y est cree
// sinon un fichier tmp/Probleme contient tous les problemes rencontres
// ============================================================================
void cResultat::EcrireArretProbleme(string chemin_annee, string chemin_tmp)
{
ofstream f;
unsigned int i;
string chemin;

chemin = chemin_annee + "Probleme";
f.open (chemin.c_str(), ios::out);
if (!f)
    {
    f.close();
    chemin = chemin_tmp + "Probleme";
    f.open (chemin.c_str(), ios::app);
    if (!f)
	throw (new cAnomalie ("Dans la methode cResultat::EcrireArretProbleme, probleme pour ouvrir le fichier : Probleme") );
    };


f << "----------------------------------------------------------------------"<<endl;
f << "PROBLEME RENCONTRE LORS DE LA SIMULATION DE L'ANNEE " << Simulateur.Annee << endl;
f << "La simulation a �t� arret�e le jour : "<< Temps.J << endl<< endl;



if ((SBExecutant.lActionsConcentreNonRealisables.size()==0) &&
     (SBExecutant.lActionsMaisNonRealisables.size()==0) &&
     (SBExecutant.lActionsFoinNonRealisables.size()==0) &&
     (SBExecutant.lActionsPaturageNonRealisables.size()==0) &&
     (SBExecutant.lActionsEnsilageNonRealisables.size()==0) &&
     (SBExecutant.lActionsFaucheNonRealisables.size()==0) &&
     (SBExecutant.lActionsAzoteNonRealisables.size()==0))
  f <<  "Le syst�me d�cisionnel n'a pas pu d�cider d'action en accord avec les plans courants." << endl << endl;
else
  {
  f <<  "Le syst�me biophysique ex�cutant n'a pas pu r�aliser une action d�cid�e par le syst�me d�cisionnel."<<endl;
  f << "Les actions suivantes n'ont pu etre r�alis�es :"<<endl;

   for (i=0; i<SBExecutant.lActionsConcentreNonRealisables.size(); i++)
     {
       f << "Concentre\t";
       f << Arrondir(SBExecutant.lActionsConcentreNonRealisables[i]->q_c) << "\t";
       f << "-\t";
       f << "-" << endl;
     };
  for (i=0; i<SBExecutant.lActionsMaisNonRealisables.size(); i++)
     {
       f << "Mais\t";
       f << Arrondir(SBExecutant.lActionsMaisNonRealisables[i]->q_m) << "\t";
       f << "-\t";
       f << "-" << endl;
     };
  for (i=0; i<SBExecutant.lActionsFoinNonRealisables.size(); i++)
     {
       f << "Foin\t";
       f << Arrondir(SBExecutant.lActionsFoinNonRealisables[i]->q_f) << "\t";
       f << "-\t";
       f << "-" << endl;
     };
  for (i=0; i<SBExecutant.lActionsPaturageNonRealisables.size(); i++)
     {
       f << "Paturage\t";
       f << Arrondir(SBExecutant.lActionsPaturageNonRealisables[i]->q_h_e) << "\t";
       f << SBExecutant.lActionsPaturageNonRealisables[i]->Parcelle << "\t";
       f << (int) SBExecutant.lActionsPaturageNonRealisables[i]->DureePaturage << endl;
     };
  for (i=0; i<SBExecutant.lActionsEnsilageNonRealisables.size(); i++)
     {
       f << "Ensilage\t";
       f << "-\t";
       f << SBExecutant.lActionsEnsilageNonRealisables[i]->Parcelle <<"\t";
       f << "-" << endl;
     };
  for (i=0; i<SBExecutant.lActionsFaucheNonRealisables.size(); i++)
     {
       f << "Fauche\t";
       f << "-\t";
       f << SBExecutant.lActionsFaucheNonRealisables[i]->Parcelle <<"\t";
       f << "-" << endl;
     };
  for (i=0; i<SBExecutant.lActionsAzoteNonRealisables.size(); i++)
     {
       f << "Azote\t";
       f << Arrondir(SBExecutant.lActionsAzoteNonRealisables[i]->q_N) << "\t";
       f << SBExecutant.lActionsAzoteNonRealisables[i]->Parcelle <<"\t";
       f << "-" << endl;
     };
  };

  f.close();
}


// ============================================================================
// CalculerSorties
// ============================================================================
void cResultat::CalculerSorties (void)
{
  // date de mise � l'herbe : jour 1ere action Paturage
    ZDateMH = SIObservation.DateMiseHerbe();

  // date de fin de 1er cycle : jour ou 2 eme groupe Paturage d'une parcelle
    ZDateFin1erCycle = SIObservation.DateFin1erCycle();

  // date de sortie nuit
    ZDateSortieNuit = SIObservation.DateSortieNuit();

  // date de fermeture et ouverture silo
    ZDateFermetureSilo = SIObservation.DateFermetureSilo();
    ZDateOuvertureSilo = SIObservation.DateOuvertureSilo();

  // date de l'ensilage
    ZDateEnsilage = SIObservation.DateEnsilage();

  // date de la 1�re fauche
    ZDate1ereFauche = SIObservation.Date1ereFauche();

  // quantit� coup�e en Kg
  ZQfauchee = SIObservation.Somme(SIObservation.Vqcoupee());

  // surface coup�e
  ZSfauchee = SIObservation.Somme(SIObservation.Vscoupee());
}


// ============================================================================
//  EcrireParcelles : ecriture parcelles sur disque
// ============================================================================
void cResultat::EcrireParcelles(string chemin)
{
unsigned int i;
int j;
ofstream f;
string fic;

if (Sauver_MS)
  {
  fic = chemin + "MS";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireParcelles, probleme pour ouvrir le fichier : " + chemin + "MS") );
  for (i=0; i<SBBiophysique.Parcelles.l.size(); i++)
    {
      f << "***" << SBBiophysique.Parcelles.l[i]->Nom << endl;
     for (j=1; j<NB_JOURS; j++)
       f << Arrondir(SBBiophysique.Parcelles.l[i]->MS[j]) << endl;
     };
   f.close();
  };

if (Sauver_MSc)
  {
  fic = chemin + "MSc";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireParcelles, probleme pour ouvrir le fichier : " + chemin + "MSc") );
  for (i=0; i<SBBiophysique.Parcelles.l.size(); i++)
    {
      f << "***" << SBBiophysique.Parcelles.l[i]->Nom << endl;
     for (j=1; j<NB_JOURS; j++)
       f << Arrondir(SBBiophysique.Parcelles.l[i]->MSc[j]) << endl;
     };
   f.close();
  };

if (Sauver_MSs)
  {
  fic = chemin + "MSs";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireParcelles, probleme pour ouvrir le fichier : " + chemin + "MSs") );
  for (i=0; i<SBBiophysique.Parcelles.l.size(); i++)
    {
      f << "***" << SBBiophysique.Parcelles.l[i]->Nom << endl;
     for (j=1; j<NB_JOURS; j++)
       f << Arrondir(SBBiophysique.Parcelles.l[i]->MSs[j])<< endl;
     };
   f.close();
  };

if (Sauver_IF)
  {
  fic = chemin + "IF";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireParcelles, probleme pour ouvrir le fichier : " + chemin + "IF") );
  for (i=0; i<SBBiophysique.Parcelles.l.size(); i++)
    {
      f << "***" << SBBiophysique.Parcelles.l[i]->Nom << endl;
     for (j=1; j<NB_JOURS; j++)
       f << Arrondir(SBBiophysique.Parcelles.l[i]->IF[j]) << endl;
     };
   f.close();
  };

if (Sauver_Dmoy)
  {
  fic = chemin + "Dmoy";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireParcelles, probleme pour ouvrir le fichier : " + chemin + "Dmoy") );
  for (i=0; i<SBBiophysique.Parcelles.l.size(); i++)
    {
      f << "***" << SBBiophysique.Parcelles.l[i]->Nom << endl;
     for (j=1; j<NB_JOURS; j++)
       f << Arrondir(SBBiophysique.Parcelles.l[i]->Dmoy[j]) << endl;
     };
   f.close();
  };

if (Sauver_IN)
  {
  fic = chemin + "IN";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireParcelles, probleme pour ouvrir le fichier : " + chemin + "IN") );
  for (i=0; i<SBBiophysique.Parcelles.l.size(); i++)
    {
      f << "***" << SBBiophysique.Parcelles.l[i]->Nom << endl;
     for (j=1; j<NB_JOURS; j++)
       f << Arrondir(SBBiophysique.Parcelles.l[i]->IN[j]) << endl;
     };
   f.close();
  };

if (Sauver_EAUd)
  {
  fic = chemin + "EAUd";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireParcelles, probleme pour ouvrir le fichier : " + chemin + "EAUd") );
  for (i=0; i<SBBiophysique.Parcelles.l.size(); i++)
    {
      f << "***" << SBBiophysique.Parcelles.l[i]->Nom << endl;
     for (j=1; j<NB_JOURS; j++)
       f << Arrondir(SBBiophysique.Parcelles.l[i]->EAUd[j]) << endl;
     };
   f.close();
  };

if (Sauver_IH)
  {
  fic = chemin + "IH";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireParcelles, probleme pour ouvrir le fichier : " + chemin + "IH") );
  for (i=0; i<SBBiophysique.Parcelles.l.size(); i++)
    {
      f << "***" << SBBiophysique.Parcelles.l[i]->Nom << endl;
     for (j=1; j<NB_JOURS; j++)
       f << Arrondir(SBBiophysique.Parcelles.l[i]->IH[j]) << endl;
     };
   f.close();
  };

if (Sauver_SU)
  {
  fic = chemin + "SU";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireParcelles, probleme pour ouvrir le fichier : " + chemin + "SU") );
  for (i=0; i<SBBiophysique.Parcelles.l.size(); i++)
    {
      f << "***" << SBBiophysique.Parcelles.l[i]->Nom << endl;
     for (j=1; j<NB_JOURS; j++)
       f << Arrondir(SBBiophysique.Parcelles.l[i]->SU[j]) << endl;
     };
   f.close();
  };
}



// ============================================================================
//  EcrireTroupeau : ecriture troupeau sur disque
// ============================================================================
void cResultat::EcrireTroupeau(string chemin)
{
int j;
ofstream f;
string fic;

if (Sauver_V_M)
  {
  fic = chemin + "V_M";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireTroupeau, probleme pour ouvrir le fichier : " + chemin + "V_M") );
  for (j=1; j<NB_JOURS; j++)
    f << Arrondir(Troupeau.Vache.V_M[j]) << endl;
  f.close();
  };
if (Sauver_q_l_p)
  {
  fic = chemin + "q_l_p";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireTroupeau, probleme pour ouvrir le fichier : " + chemin + "q_l_p") );
  for (j=1; j<NB_JOURS; j++)
    f << Arrondir(Troupeau.Vache.q_l_p[j]) << endl;
  f.close();
  };
if (Sauver_q_l_r)
  {
  fic = chemin + "q_l_r";
  f.open (fic.c_str(), ios::out);
  if (!f)
  throw (new cAnomalie ("Dans la methode cResultat::EcrireTroupeau, probleme pour ouvrir le fichier : " + chemin + "q_l_r") );
  for (j=1; j<NB_JOURS; j++)
    f << Arrondir(Troupeau.Vache.q_l_r[j]) << endl;
  f.close();
  };
if (Sauver_Estockee)
  {
  fic = chemin + "Estockee";
  f.open (fic.c_str(), ios::out);
  if (!f)
  throw (new cAnomalie ("Dans la methode cResultat::EcrireTroupeau, probleme pour ouvrir le fichier : " + chemin + "Estockee") );
  for (j=1; j<NB_JOURS; j++)
    f << Arrondir(Troupeau.Vache.Estockee[j]) << endl;
  f.close();
  };
}



// ============================================================================
// EcrireActions : sauvegarde des actions
// ============================================================================
void cResultat::EcrireActions(string chemin)
{
unsigned int i;
int j;
ofstream f;
string fic;

if (Sauver_agenda)
  {
  fic = chemin +  "chronique";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireActions, probleme pour ouvrir le fichier : " + chemin + fic) );
  for (j=Temps.NoJour(Simulateur.JourDebut, Simulateur.MoisDebut) ;
       j<Temps.NoJour(Simulateur.JourFin, Simulateur.MoisFin); j++)
    {
    f << "***" << j << endl;

    for (i=0; i<lACTIONS_CONCENTRE.size(); i++)
      if (lACTIONS_CONCENTRE[i]->Jour == j)
	{
	  f << "Concentre" << "\t";
	  f << Arrondir(lACTIONS_CONCENTRE[i]->q_c) << "\t";
	  f << "-      " << "\t";
	  f << "-" <<endl;
	};

    for (i=0; i<lACTIONS_MAIS.size(); i++)
      if (lACTIONS_MAIS[i]->Jour == j)
	{
	  f << "Mais     " << "\t";
	  f << Arrondir(lACTIONS_MAIS[i]->q_m) << "\t";
	  f << "-      " << "\t";
	  f << "-" <<endl;
	};

    for (i=0; i<lACTIONS_FOIN.size(); i++)
      if (lACTIONS_FOIN[i]->Jour == j)
	{
	  f << "Foin     " << "\t";
	  f << Arrondir(lACTIONS_FOIN[i]->q_f) << "\t";
	  f << "-      " << "\t";
	  f << "-" <<endl;
	};

    for (i=0; i<lACTIONS_PATURAGE.size(); i++)
      if (lACTIONS_PATURAGE[i]->Jour == j)
	{
	  f << "Paturage " << "\t";
	  f <<  Arrondir(lACTIONS_PATURAGE[i]->q_h_e) << "\t";
	  f <<  lACTIONS_PATURAGE[i]->Parcelle->Nom << "\t";
	  f << (int) lACTIONS_PATURAGE[i]->DureePaturage << endl;
	};

    for (i=0; i<lACTIONS_ENSILAGE.size(); i++)
      if (lACTIONS_ENSILAGE[i]->Jour == j)
	{
	  f << "Ensilage " << "\t";
	  f << "- " << "\t";
	  f << lACTIONS_ENSILAGE[i]->Parcelle->Nom << "\t";
	  f << "-" <<endl;
	};


    for (i=0; i<lACTIONS_FAUCHE.size(); i++)
      if (lACTIONS_FAUCHE[i]->Jour == j)
	{
	  f << "Fauche   " << "\t";
	  f << "- "  << "\t";
	  f << lACTIONS_FAUCHE[i]->Parcelle->Nom << "\t";
	  f << "-" <<endl;
	};

    for (i=0; i<lACTIONS_AZOTE.size(); i++)
      if (lACTIONS_AZOTE[i]->Jour == j)
	{
	  f << "Azote    " << "\t";
	  f << Arrondir(lACTIONS_AZOTE[i]->q_N) << "\t";
	  f << lACTIONS_AZOTE[i]->Parcelle->Nom << "\t";
	  f << "-" <<endl;
	};
    };

  f.close();

  fic = chemin + "clefs";
  f.open (fic.c_str(), ios::out);
  if (!f)
    throw (new cAnomalie ("Dans la methode cResultat::EcrireActions, probleme pour ouvrir le fichier : " + chemin + fic) );

  // date de mise � l'herbe
  f << ZDateMH << endl;
  
  // date de fin de 1er cycle
  f << ZDateFin1erCycle << endl;
  
  // date de sortie nuit
  f << ZDateSortieNuit << endl;
  
  // date de fermuture silo
  f << ZDateFermetureSilo << endl;
  
  // date d'ouverture silo
  f << ZDateOuvertureSilo << endl;

  // date ensilage
  f << ZDateEnsilage << endl;

  // date 1ereFauche
  f << ZDate1ereFauche << endl;
  
  // quantit� coup�e    
  f << Arrondir(ZQfauchee) << endl;

  // surface coup�e
  f << Arrondir(ZSfauchee) << endl;  

  f.close();
  };
}

// ============================================================================
// EcrireFonctions
// ============================================================================
void cResultat::EcrireFonctions(string chemin)
{
int j, k;
ofstream f;
string fic;

if (Sauver_Fonctions)
  {
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (SIObservation.bFo[k] != NULL)
	{
          fic = chemin + SIObservation.bFo[k]->Nom;
	  f.open (fic.c_str(), ios::out);
	  if (!f)
	    throw (new cAnomalie ("Dans la methode cResultat::EcrireFonctions, probleme pour ouvrir le fichier : " + chemin + SIObservation.bFo[k]->Nom) );
	  for (j=1; j<NB_JOURS; j++)
	    f <<SIObservation.MemobFonctions[k][j] << endl;
	  f.close();
	};
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (SIObservation.fFo[k] != NULL)
	{
	  fic = chemin + SIObservation.fFo[k]->Nom;
	  f.open (fic.c_str(), ios::out);
	  if (!f)
	    throw (new cAnomalie ("Dans la methode cResultat::EcrireFonctions, probleme pour ouvrir le fichier : " + chemin + SIObservation.fFo[k]->Nom) );
	  for (j=1; j<NB_JOURS; j++)
	    f << Arrondir(SIObservation.MemofFonctions[k][j]) << endl;
	  f.close();
	};
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (SIObservation.pFo[k] != NULL)
	{
	  fic = chemin + SIObservation.pFo[k]->Nom;
	  f.open (fic.c_str(), ios::out);
	  if (!f)
	    throw (new cAnomalie ("Dans la methode cResultat::EcrireFonctions, probleme pour ouvrir le fichier : " + chemin + SIObservation.pFo[k]->Nom) );
	  f << "%texte%" << endl;
	  for (j=1; j<NB_JOURS; j++)
	    f << SIObservation.MemopFonctions[k][j] << endl;
	  f.close();
	};
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (SIObservation.eFo[k] != NULL)
	{
	  fic = chemin + SIObservation.eFo[k]->Nom;
	  f.open (fic.c_str(), ios::out);
	  if (!f)
	    throw (new cAnomalie ("Dans la methode cResultat::EcrireFonctions, probleme pour ouvrir le fichier : " + chemin + SIObservation.eFo[k]->Nom) );
	  f << "%texte%" << endl;
	  for (j=1; j<NB_JOURS; j++)
	    f << SIObservation.MemoeFonctions[k][j] << endl;
	  f.close();
	};
    for (k=0; k<NB_MAX_FO_TYPE; k++)
      if (SIObservation.sFo[k] != NULL)
	{
	  fic = chemin + SIObservation.sFo[k]->Nom;
	  f.open (fic.c_str(), ios::out);
	  if (!f)
	    throw (new cAnomalie ("Dans la methode cResultat::EcrireFonctions, probleme pour ouvrir le fichier : " + chemin + SIObservation.sFo[k]->Nom) );
	  f << "%texte%" << endl;
	  for (j=1; j<NB_JOURS; j++)
	    f << SIObservation.MemosFonctions[k][j] << endl;
	  f.close();
	};
  };
}

// ============================================================================
// EcrireIndicateurs
// ============================================================================
void cResultat::EcrireIndicateurs(string chemin)
{
int j;
ofstream f;
string fic;

if (Sauver_Indicateurs)
    for (int k=NB_INDICATEURS_PREDEFINIS; k<NB_MAX_INDICATEURS; k++)
      if (SIObservation.Indicateurs[k] != NULL)
	{
	  fic = chemin + SIObservation.Indicateurs[k]->Nom;
	  f.open (fic.c_str(), ios::out);
	  if (!f)
	    throw (new cAnomalie ("Dans la methode cResultat::EcrireIndicateurs, probleme pour ouvrir le fichier : " + chemin + SIObservation.Indicateurs[k]->Nom) );
	  for (j=1; j<NB_JOURS; j++)
	    f << Arrondir(SIObservation.MemoIndicateurs[k][j]) << endl;
	  f.close();
	};
}

// ============================================================================
// EcrireStatistiques
// ============================================================================
void cResultat::EcrireStatistiques(string chemin)
{
unsigned int i;
int j, jdebut, jfin;
float x;
ofstream f;
string fic;

if (Sauver_Statistiques)
  {
    jdebut = Temps.NoJour(Simulateur.JourDebut, Simulateur.MoisDebut);
    jfin = Temps.NoJour(Simulateur.JourFin, Simulateur.MoisFin);

    // Sq_l_r
    x = 0;
    for (j = jdebut; j<jfin; j++)
       x += Troupeau.Vache.q_l_r[j];
    fic = chemin + "Sq_l_r";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "Sq_l_r\n") );
    f << Arrondir(x) << endl;
    f.close();

    // Sq_l_p
    x = 0;
    for (j = jdebut; j<jfin; j++)
       x += Troupeau.Vache.q_l_p[j];
    fic = chemin + "Sq_l_p";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "Sq_l_p\n") );
    f << Arrondir(x) << endl;
    f.close();

    // SEstockee
    x = 0;
    for (j = jdebut; j<jfin; j++)
       x += Troupeau.Vache.Estockee[j];
    fic = chemin + "SEstockee";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "SEstockee\n") );
    f << Arrondir(x) << endl;
    f.close();

    // Sq_concentre
    x = 0;
    for (i=0; i<lACTIONS_CONCENTRE.size(); i++)
	x += lACTIONS_CONCENTRE[i]->q_c;
    fic = chemin + "Sq_concentre";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "Sq_concentre\n") );
    f << Arrondir(x) << endl;
    f.close();

    // Sq_foin
    x = 0;
    for (i=0; i<lACTIONS_FOIN.size(); i++)
	x += lACTIONS_FOIN[i]->q_f;
    fic = chemin + "Sq_foin";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "Sq_foin\n") );
    f << Arrondir(x) << endl;
    f.close();

    // Sq_mais
    x = 0;
    for (i=0; i<lACTIONS_MAIS.size(); i++)
	x += lACTIONS_MAIS[i]->q_m;
    fic = chemin + "Sq_mais";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "Sq_mais\n") );
    f << Arrondir(x) << endl;
    f.close();

    // Sq_herbe
    x = 0;
    for (i=0; i<lACTIONS_PATURAGE.size(); i++)
	x += lACTIONS_PATURAGE[i]->q_h_e;
    fic = chemin + "Sq_herbe";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "Sq_herbe\n") );
    f << Arrondir(x) << endl;
    f.close();

    // DateMH
    fic = chemin + "DateMH";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "DateMH\n") );    
    f << ZDateMH << endl;
    f.close();

    // DateFin1erCycle
    fic = chemin + "DateFin1erCycle";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "DateFin1erCycle\n") );    
    f << ZDateFin1erCycle << endl;
    f.close();

    // DateSortieNuit
    fic = chemin + "DateSortieNuit";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "DateSortieNuit\n") );    
    f << ZDateSortieNuit << endl;
    f.close();

    // DateFermetureSilo
    fic = chemin + "DateFermetureSilo";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "DateFermetureSilo\n") );    
    f << ZDateFermetureSilo << endl;
    f.close();

    // DateOuvertureSilo
    fic = chemin + "DateOuvertureSilo";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "DateOuvertureSilo\n") );    
    f << ZDateOuvertureSilo << endl;
    f.close();

    // DateEnsilage
    fic = chemin + "DateEnsilage";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "DateEnsilage\n") );    
    f << ZDateEnsilage << endl;
    f.close();

    // Date1ereFauche
    fic = chemin + "Date1ereFauche";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "Date1ereFauche\n") );    
    f << ZDate1ereFauche << endl;
    f.close();

    // Sqfauchee
    fic = chemin + "Sqfauchee";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "Sqfauchee\n") );    
    f << Arrondir(ZQfauchee) << endl;
    f.close();
    
    // Ssfauchee
    fic = chemin + "Ssfauchee";
    f.open (fic.c_str(), ios::app);
    if (!f)
      throw (new cAnomalie ("Dans la methode cResultat::EcrireStatistiques, probleme pour ouvrir le fichier : " + chemin + "Ssfauchee\n") );    
    f << Arrondir(ZSfauchee) << endl;
    f.close();

	
    // Fonctions d'interpretation numeriques (moyenne annuelle)
    for (i=0; i<NB_MAX_FO_TYPE; i++)
      if (SIObservation.fFo[i] != NULL)
	{
	  x=0;
	  for (j=jdebut; j<jfin; j++)
	    x += SIObservation.MemofFonctions[i][j];
	  x = x / (jfin - jdebut);
	  fic = chemin + SIObservation.fFo[i]->Nom;
	  f.open (fic.c_str(), ios::app);
	  if (!f)
	    throw (new cAnomalie ("Dans la methode cResultat::EcrireFonctions, probleme pour ouvrir le fichier : " + chemin + SIObservation.fFo[i]->Nom) );
	  f << Arrondir(x) << endl;
	  f.close();
	};

  };
}
