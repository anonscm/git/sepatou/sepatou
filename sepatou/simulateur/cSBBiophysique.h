// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSBBiophysique.h
contenu : definition de la classe cSBBiophysique
******************************************************************************/
#ifndef _CSBBIOPHYSIQUE_H
#define _CSBBIOPHYSIQUE_H

#include <string>

using namespace std;

#include "pat.h"
#include "cEnsembleParcelles.h"

class cParcelle;


// ============================================================================
// cSBiophysique : representation du systeme biophysique
// ============================================================================
class cSBBiophysique
{
public:  //--------------------------------------------------------------------
int Quota;
string NomExploitation;
string Localisation;
cEnsembleParcelles Parcelles;

float qcoupee[NB_JOURS];
float scoupee[NB_JOURS];

void InitElementsInvariants(void); // init elts invariants
void InitElementsVariants(void);   // init en debut de simulation
void LibererMemoire(void);         // liberer memoire dynamique
void Evoluer(void);                // evoluer d un pas de temps


private:  //-------------------------------------------------------------------
CARACTERISTIQUE_PARCELLE TransformerCaracteristique(string caracteristique);
TYPE_SOL TransformerTypeSol(string type_sol);
ELOIGNEMENT TransformerEloignement(string eloignement);
ESPECE_HERBE TransformerEspece(string espece);
ANNEE_VELAGE TransformerNomAnneeVelage(string NomAnneeVelage);
QUALITE_FOIN TransformerQualiteFoin(string qualite_foin);

};

#endif
