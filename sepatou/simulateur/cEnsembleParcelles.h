// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cEnsembleParcelles.h
contenu : definition de la classe ccEnsembleParcelles
******************************************************************************/
#ifndef _CENSEMBLEPARCELLES_H
#define _CENSEMBLEPARCELLES_H

#include <string>
#include <vector>

using namespace std;

class cParcelle;


// ============================================================================
// cEnsembleParcelles : representation d un ens de parcelles (sans duplicata)
// ============================================================================
class cEnsembleParcelles
{
public:  //--------------------------------------------------------------------
vector <cParcelle*> l;

bool Parmi(cParcelle* p);  
bool NonVide(void);

cEnsembleParcelles Mixte(void);
cEnsembleParcelles NonMixte(void);
cParcelle* MinMS(void);
cParcelle* MaxMS(void);
cParcelle* PlusAncienneNonUtilisee(void);
cEnsembleParcelles DerniereUtilisationAvantXJours(int n);
cEnsembleParcelles MSSuperieureA(float ms);
cEnsembleParcelles AjouterNom(string nom);
cEnsembleParcelles EnleverNom(string nom);
void Ajouter(cParcelle* p);
void Enlever(cParcelle* p);

cEnsembleParcelles  operator- (cParcelle* p); // notation differente de Enlever
cEnsembleParcelles  operator+ (cParcelle* p); // notation differente de Ajouter
cEnsembleParcelles  operator- (cEnsembleParcelles E);
cEnsembleParcelles  operator+ (cEnsembleParcelles E);
bool operator== (cEnsembleParcelles E);
bool operator!= (cEnsembleParcelles E);
};


#endif
