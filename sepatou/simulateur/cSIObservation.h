// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSIObservation.h
contenu : definition de la classe cSIObservation
******************************************************************************/
#ifndef _CSIOBSERVATION_H
#define _CSIOBSERVATION_H


#include <string>

using namespace std;

#include "pat.h"
#include "Strategie.h"

class cParcelle;
class cEnsembleParcelles;

class cbFoInterpretation;
class cfFoInterpretation;
class cpFoInterpretation;
class ceFoInterpretation;
class csFoInterpretation;
class cIndicateur;

// ============================================================================
// cSIObservation : representation du systeme d information d observation
// ============================================================================
class cSIObservation
{
public:  //--------------------------------------------------------------------
void              InitElementsInvariants(void); 
void              InitElementsVariants(void); 
void              LibererMemoire(void);         // liberer memoire dynamique
void              MemoriserFonctionsIndicateurs(void);

int               NbVaches (void);
float             QuantiteLaitMax (void);
float             QuantiteLaitPotentielle (int j);
float             QuantiteLaitReelle (int j);

float             QuantiteConcentreMax(int j);

float             CroissanceHerbe (cEnsembleParcelles ep, int n);

float             ReserveMais (int j);
float             ReserveFoin( int j);
QUALITE_FOIN      QualiteFoin(void);

cEnsembleParcelles Parcelles(void);
cParcelle*        ParcelleCourante(void);
cParcelle*        Nom(string nom);
cParcelle*        DernierePatureeRecoltee(cEnsembleParcelles ep);

float             MS(string nom);
float             MS(string nom, int j);
float             MS(cParcelle* p);
float             MS(cParcelle* p, int j);
CARACTERISTIQUE_PARCELLE  Caracteristique(cParcelle* p);
TYPE_SOL          TypeSol(cParcelle* p);
ELOIGNEMENT       Eloignement(cParcelle* p);
ESPECE_HERBE      Espece(cParcelle* p);
float             HerbeIngeree(int j);
float             HauteurHerbe(string nom, float d);
float             HauteurHerbe(cParcelle* p, float d);

float             DisponibiliteHerbe (cEnsembleParcelles ep);
float             DisponibiliteHerbe (cEnsembleParcelles ep, int j, float q=15, float ms_reste = 150);

cEnsembleParcelles Paturables (cEnsembleParcelles ep, float q = 15, float ms_reste = 150,int nj_min = 15, int nj_max = 60, float ms_max = 9999);
cEnsembleParcelles Ensilables (cEnsembleParcelles ep, float ms_min = 300);
cEnsembleParcelles Fauchables (cEnsembleParcelles ep, float ms_min = 300);
cEnsembleParcelles DejaPaturees (int n, cEnsembleParcelles ep);
cEnsembleParcelles DejaPatureesEnsileesFauchees (cEnsembleParcelles ep);
cEnsembleParcelles DejaEnsilees (cEnsembleParcelles ep);
cEnsembleParcelles PremieresFauches (cEnsembleParcelles ep);

int               Quota(void);

float             Pluie (int j);
float             T (int j);
float             ETP(int j);
float             SommeTbase0 (int j1, int j2);

bool              bVisualiser(bool b);
float             fVisualiser(float f);
string            pVisualiser(cParcelle* p);
string            eVisualiser(cEnsembleParcelles e);
string            sVisualiser(string s);

bool              bValeurFo(bFO_INTERPRETATION k);
float             fValeurFo(fFO_INTERPRETATION k);
cParcelle*        pValeurFo(pFO_INTERPRETATION k);
cEnsembleParcelles eValeurFo(eFO_INTERPRETATION k);
string            sValeurFo(sFO_INTERPRETATION k);

void              bAjouterFo(cbFoInterpretation* f);
void              fAjouterFo(cfFoInterpretation* f);
void              pAjouterFo(cpFoInterpretation* f);
void              eAjouterFo(ceFoInterpretation* f);
void              sAjouterFo(csFoInterpretation* f);

int               ValeurIndicateur(INDICATEUR k);
bool              Avant(INDICATEUR k);
bool              Avant(int valeur);
bool              Apres(INDICATEUR k);
bool              Apres(int valeur);
bool              A(INDICATEUR k);
bool              A(int valeur);
void              AjouterIndicateur(cIndicateur* indicateur);

// mis en public pour lecture par cResultat
cbFoInterpretation* bFo[NB_MAX_FO_TYPE];
cfFoInterpretation* fFo[NB_MAX_FO_TYPE];
cpFoInterpretation* pFo[NB_MAX_FO_TYPE];
ceFoInterpretation* eFo[NB_MAX_FO_TYPE];
csFoInterpretation* sFo[NB_MAX_FO_TYPE];

cIndicateur* Indicateurs[NB_MAX_INDICATEURS];

// memorisation des fonctions d'interpretation et des indicateurs
bool*             MemobFonctions[NB_MAX_FO_TYPE];
float*            MemofFonctions[NB_MAX_FO_TYPE];
string*           MemopFonctions[NB_MAX_FO_TYPE];
string*           MemoeFonctions[NB_MAX_FO_TYPE];
string*           MemosFonctions[NB_MAX_FO_TYPE];
int*              MemoIndicateurs[NB_MAX_INDICATEURS];

// m�thodes introduites pour l'optimisateur
float Somme(float* f);
float Maximum(float* f);
float Minimum(float* f);

float* Vq_l_r(void);
float* Vq_l_p(void);
float* VEstockee(void);
float* Vqconcentre(void);
float* Vqmais(void);
float* Vqfoin(void);
float* Vqherbe(void);
float* Vqcoupee(void);
float* Vscoupee(void);

int DateMiseHerbe(void);
int DateFin1erCycle(void);
int DateSortieNuit(void);
int DateFermetureSilo(void);
int DateOuvertureSilo(void);
int DateEnsilage(void);
int Date1ereFauche(void);

 float S (string nom);    // surface d'une parcelle
float S (cParcelle* p);   // surface d'une parcelle
float S_Parcelles (cEnsembleParcelles ep);  // surface d'un ens de parcelles


private:  //-------------------------------------------------------------------
bool              AuMoinsUnJour(cParcelle* p, float q, float ms_reste);
};


#endif
