// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSDOperatoire.h
contenu : definition de la classe cSDOperatoire
******************************************************************************/
#ifndef _CSDOPERATOIRE_H
#define _CSDOPERATOIRE_H


#include <string>
#include <vector>

using namespace std;

#include "pat.h"
#include "cPlan.h"
#include "cEnsembleParcelles.h"

class cParcelle;
class cActionConcentre;
class cActionMais;
class cActionFoin;
class cActionPaturage;
class cActionEnsilage;
class cActionFauche;
class cActionAzote;
class cRegleOperatoire;
class cEvenement;


// ============================================================================
// cSDOperatoire : representation du systeme decisionnel operatoire
// ============================================================================
class cSDOperatoire
{
public:  //--------------------------------------------------------------------

// description du systeme operatoire
vector <cRegleOperatoire*> lRegles[NB_ACTIVITES];// liste des regles
vector <cEvenement*>  lEvtADetecter;             // liste des evts a detecter


// Variables de decision du systeme decisionnel operatoire
ACTION                     OrdreActions[NB_ACTIONS];
                                              // ordre des actions
                                              // pour le SB Executant

float                      ApportConcentre;   // quantite de concentre a donner
                                              // par vache en Kg
float                      ApportMais;        // quantite de mais a donner
                                              // par vache en Kg
float                      ApportFoin;        // quantite de foin a donner
                                              // par vache en Kg
cParcelle*                 DeplacerTroupeau;  // parcelle ou va le troupeau
DUREE_PATURAGE             DureePaturage;     // duree du paturage
cEnsembleParcelles         Ensiler;           // parcelles a ensiler
cEnsembleParcelles         Faucher;           // parcelles a faucher
float                      ApportAzote;       // quantite d azote a apporter
                                              // en kg/ha
cEnsembleParcelles         Fertiliser;        // parcelles a fertiliser

// liste par type d action des actions a realiser
vector <cActionConcentre*> lActionsConcentreARealiser; 
vector <cActionMais*> lActionsMaisARealiser; 
vector <cActionFoin*> lActionsFoinARealiser; 
vector <cActionPaturage*> lActionsPaturageARealiser; 
vector <cActionEnsilage*> lActionsEnsilageARealiser; 
vector <cActionFauche*> lActionsFaucheARealiser; 
vector <cActionAzote*> lActionsAzoteARealiser; 
int NoJourlACTIONS;                          // jour ou les lACTIONS sont MAJ

// variables utilisateur LnU
bool                       bVariable[NB_VAR_SDO];
int                        iVariable[NB_VAR_SDO]; 
float                      fVariable[NB_VAR_SDO];
cParcelle*                 pVariable[NB_VAR_SDO];
cEnsembleParcelles         eVariable[NB_VAR_SDO];
string                     sVariable[NB_VAR_SDO];

bool Probleme;                         // true si impossible decider actions

void InitElementsInvariants(void);     // initialisation des elts invariants
void InitElementsVariants(void);       // initialisation des elts
                                       // au jour de debut de simulation
void LibererMemoire(void);             // liberer memoire dynamique
void InitVariablesDecision(void);      // init variables de decision
void AjouterRegle(cRegleOperatoire* r);// ajouter une regle
void InitlEvtADetecter(void);          // mise a jour de la liste des evts 
                                       // a detecter
RESULTAT TraiterEvenements(vector <cEvenement*> l_evt);
                                       // traiter les evts donnes en arguments
                                       // qui ont ete detecte


private:  //-------------------------------------------------------------------
void DeciderActions(void);             // mise a jour des variables de decision
                                       // et creation des actions decidees
void RAZlActionsARealiser(void);        // vide les listes des actions a realiser
cPlan* PlanDeActivite(ACTIVITE activite); // retourne le plan associe
};


#endif
