// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cTemps.cc
contenu : definition des methodes de la classe cTemps
******************************************************************************/


#include "cTemps.h" 
  
#include "pat.h"
#include "cResultat.h"
#include "cSIObservation.h"
#include "cSISurveillance.h"
#include "cSBExecutant.h"
#include "cAnomalie.h"

extern cResultat Resultat;
extern cSIObservation SIObservation;
extern cSISurveillance SISurveillance;
extern cSBExecutant SBExecutant;


// ============================================================================
// NoJour : Calcul du numero de jour d'une date (jour, mois) 
//          pour l'annee courante
// ============================================================================
int cTemps::NoJour(int jour, int mois)
{
int no = jour;
switch(mois)
  {
  case 12 : no += 30;
  case 11 : no += 31;
  case 10 : no += 30;
  case 9 : no += 31;
  case 8 : no += 31;
  case 7 : no += 30;
  case 6 : no += 31;
  case 5 : no += 30;
  case 4 : no += 31;
  case 3 : no += 28;
  case 2 : no += 31;
  case 1 : break;
  default : throw (new cAnomalie (" Dans la methode cTemps.NoJour pour l'annee courante, la valeur de mois est incorrecte ") );
  };
  return no;
}


// ============================================================================
// NoJour : Calcul du numero de jour d'une date (jour, mois) 
//          pour l'annee specifiee
// ============================================================================
int cTemps::NoJour(int jour, int mois, int a)
{
int no = jour;
switch(mois)
  {
  case 12 : no += 30;
  case 11 : no += 31;
  case 10 : no += 30;
  case 9 : no += 31;
  case 8 : no += 31;
  case 7 : no += 30;
  case 6 : no += 31;
  case 5 : no += 30;
  case 4 : no += 31;
  case 3 : no += 28;
  case 2 : no += 31;
  case 1 : break;
  default : throw (new cAnomalie (" Dans la methode cTemps.NoJour avec annee specifiee, la valeur de mois est incorrecte.") );
  };
  return no;
}

// ============================================================================
// Init : Initialisation au jour de debut de simulation
// ============================================================================
void cTemps::InitElementsVariants(int annee, int jour_debut, int mois_debut,
                                  int jour_fin, int mois_fin)
{
Annee = annee;
NoJourFin = NoJour(jour_fin, mois_fin);
J = NoJour(jour_debut, mois_debut);
}


// ============================================================================
// PasserPasTempsSuivant : Passage au pas de temps (jour) suivant
// ============================================================================
RESULTAT cTemps::Ecouler(void)
{
bool boucler = true;

while (boucler)
  {
  if (J >= NoJourFin)
      boucler = false;
  else
    {
    if (Resultat.Sauver_trace)
       Resultat.fTrace << "J = " << J <<" ----------------------------------------------------"<<endl;
    if  (SISurveillance.Reveiller() != OK)
      boucler = false;
    else
       if (SBExecutant.Evoluer() == OK)
	 {
	 SIObservation.MemoriserFonctionsIndicateurs();
	 J += 1;
	 };
    };
  };

if ( J == NoJourFin )
  return OK;
else
  return PB;
}


