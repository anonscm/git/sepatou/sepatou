// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cStock.cc
contenu : definition des methodes des classes cStockConcentre,
          cStockMais, cStockFoin, cStockAzote
******************************************************************************/


#include "cStock.h"

#include "pat.h"
#include "cSimulateur.h"
#include "cTemps.h"
#include "cTroupeau.h"
#include "cParcelle.h"
#include "cAction.h"
#include "cSBExecutant.h"
#include "cAnomalie.h"

extern cSimulateur Simulateur;
extern cTemps Temps;
extern cTroupeau Troupeau;
extern cSBExecutant SBExecutant;

// ============================================================================
// cStockConcentre.InitElementsInvariants 
// ============================================================================
void cStockConcentre::InitElementsInvariants(float q_Kg)
{
q_init_Kg = q_Kg;
}

// ============================================================================
// cStockConcentre.InitElementsVariants : initialisation en debut de simulation
// ============================================================================
void cStockConcentre::InitElementsVariants(void)
{
int Jdebut = Temps.NoJour(Simulateur.JourDebut, Simulateur.MoisDebut);

for (int i=0; i<NB_JOURS; i++)
   Quantite[i] = QUANTITE_INCONNUE;
Quantite[Jdebut] = q_init_Kg;
}

// ============================================================================
// cStockConcentre.Etat : etat du stock en Kg un jour donne
// ============================================================================
float cStockConcentre::Etat(int j)
{
if (j > Temps.J)
    throw (new cAnomalie ("Dans la methode cStockConcentre::Etat, la valeur de j est posterieure a Temps.J.") );
return Quantite[j];
}

// ============================================================================
// cStockConcentre.Evoluer : evoluer d un jour
// ============================================================================
void cStockConcentre::Evoluer(void)
{
float q_a_enlever;
cActionConcentre* Ac;

if (SBExecutant.lActionsConcentreASimuler.size() == 0)
  Quantite[Temps.J+1] = Quantite[Temps.J];
else
  for (unsigned int i=0; i<SBExecutant.lActionsConcentreASimuler.size(); i++)
    {
    Ac = SBExecutant.lActionsConcentreASimuler[i];
    q_a_enlever = Ac->q_c * Troupeau.n;
    if (q_a_enlever > Quantite[Temps.J])
       throw (new cAnomalie ("Dans la methode cStockConcentre::Evoluer, la quantite a enlever est superieure a la quantite presente.") );
    Quantite[Temps.J+1] = Quantite[Temps.J] - q_a_enlever;
    };
}





// ============================================================================
// cStockMais.InitElementsInvariants
// ============================================================================
void cStockMais::InitElementsInvariants(float q_Kg)
{
q_init = q_Kg;
}

// ============================================================================
// cStockMais.InitElementsVariants : initialisation en debut de simulation
// en considerant qu'il faut 15 Kg par vache par jour
// ============================================================================
void cStockMais::InitElementsVariants(void)
{
int JDebut = Temps.NoJour(Simulateur.JourDebut, Simulateur.MoisDebut);

for (int i=0; i<NB_JOURS; i++)
   Quantite[i] = QUANTITE_INCONNUE;

// en considerant 15 Kg de mais par vache par jour
Quantite[JDebut] = q_init;
}

// ============================================================================
// cStockMais.Etat : etat du stock en Kg un jour donne
// ============================================================================
float cStockMais::Etat(int j)
{
if (j > Temps.J)
    throw (new cAnomalie ("Dans la methode cStockMais::Etat, la valeur de j est posterieure a Temps.J.") );
return Quantite[j];
}


// ============================================================================
// cStockMais.Evoluer : evoluer d un jour
// ============================================================================
void cStockMais::Evoluer(void)
{
float q_a_enlever;
cActionMais* Am;

if (SBExecutant.lActionsMaisASimuler.size() == 0)
  {
  Quantite[Temps.J+1] = Quantite[Temps.J];
  }
else
  for (unsigned int i=0; i<SBExecutant.lActionsMaisASimuler.size(); i++)
    {
    Am = SBExecutant.lActionsMaisASimuler[i];
    q_a_enlever = Am->q_m * Troupeau.n ;
    if (q_a_enlever > Quantite[Temps.J])
        throw (new cAnomalie ("Dans la methode cStockMais::Evoluer, la quantite a enlever est superieure a la quantite presente.") );
    Quantite[Temps.J+1] = Quantite[Temps.J] - q_a_enlever;
    };
}






// ============================================================================
// cStockFoin.InitElementsInvariants : initialisation en debut de simulation
// ============================================================================
void cStockFoin::InitElementsInvariants(float q_Kg, QUALITE_FOIN qualite)
{
Qualite = qualite;
q_init_Kg = q_Kg;
}

// ============================================================================
// cStockFoin.InitElementsVariants : initialisation en debut de simulation
// ============================================================================
void cStockFoin::InitElementsVariants(void)
{
int JDebut = Temps.NoJour(Simulateur.JourDebut, Simulateur.MoisDebut);

for (int i=0; i<NB_JOURS; i++)
   Quantite[i] = QUANTITE_INCONNUE;

Quantite[JDebut] = q_init_Kg;
}

// ============================================================================
// cStockFoin.Etat : etat du stock en Kg un jour donne
// ============================================================================
float cStockFoin::Etat(int j)
{
if (j > Temps.J)
    throw (new cAnomalie ("Dans la methode cStockFoin::Etat, la valeur de j est posterieure a Temps.J.") );
return Quantite[j];
}

// ============================================================================
// cStockFoin.Evoluer : evoluer d un jour
// ============================================================================
void cStockFoin::Evoluer(void)
{
float q_a_enlever;
cActionFoin* Af;

if (SBExecutant.lActionsFoinASimuler.size() == 0)
  Quantite[Temps.J+1] = Quantite[Temps.J];
else
  for (unsigned int i=0; i<SBExecutant.lActionsFoinASimuler.size(); i++)
    {
    Af = SBExecutant.lActionsFoinASimuler[i];
    q_a_enlever = Af->q_f * Troupeau.n ;
    if (q_a_enlever > Quantite[Temps.J])
         throw (new cAnomalie ("Dans la methode cStockFoin::Evoluer, la quantite a enlever est superieure a la quantite presente.") );
    Quantite[Temps.J+1] = Quantite[Temps.J] - q_a_enlever;
    };
}





// ============================================================================
// cStockAzote.InitElementsInvariants
// ============================================================================
void cStockAzote::InitElementsInvariants(float q_Kg)
{
q_init_Kg = q_Kg;
}

// ============================================================================
// cStockAzote.InitElementsVariants : initialisation en debut de simulation
// ============================================================================
void cStockAzote::InitElementsVariants(void)
{
int JDebut = Temps.NoJour(Simulateur.JourDebut, Simulateur.MoisDebut);

for (int i=0; i<NB_JOURS; i++)
   Quantite[i] = QUANTITE_INCONNUE;

Quantite[JDebut] = q_init_Kg;
}

// ============================================================================
// cStockAzote.Etat : etat du stock en Kg un jour donne
// ============================================================================
float cStockAzote::Etat(int j)
{
if (j > Temps.J)
    throw (new cAnomalie ("Dans la methode cStockAzote::Etat, la valeur de j est posterieure a Temps.J.") );
return Quantite[j];
}


// ============================================================================
// cStockAzote.Evoluer : evoluer d un jour
// ============================================================================
void cStockAzote::Evoluer(void)
{
float q_a_enlever;
cActionAzote* Az;

if (SBExecutant.lActionsAzoteASimuler.size() == 0)
  Quantite[Temps.J+1] = Quantite[Temps.J];
else
  for (unsigned int i=0; i<SBExecutant.lActionsAzoteASimuler.size(); i++)
    {
     Az = SBExecutant.lActionsAzoteASimuler[i];
     q_a_enlever = Az->q_N * Az->Parcelle->S;
     if (q_a_enlever > Quantite[Temps.J])
         throw (new cAnomalie ("Dans la methode cStockAzote::Evoluer, la quantite a enlever est superieure a la quantite presente.") );
    Quantite[Temps.J+1] = Quantite[Temps.J] - q_a_enlever;
    };
}



