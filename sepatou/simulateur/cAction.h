// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cAction.h
contenu : definition des classes cAction, cActionConcentre, cActionMais,
           cActionFoin, cActionPaturage, cActionEnsilage, cActionFauche,
           cActionAzote
******************************************************************************/

/******************************************************************************
PROGRAMMATION A AMELIORER :
- faire cAction.cAction qui fait :
     {Jour = Temps.J;}
  apres le constructeur de chaque action complete ceci.
- declarer cAction.Realisable() {return true}
  la methode n'est redefinie que pour les actions ou la methode est differente
- la methode cAction.InitElementsVariants ne reste pas au niveau de la classe
  car elle n'est pas d�finie en ligne ce qui n'est pas possible par son contenu
  qui fait appel � des sous classes de cAction
******************************************************************************/


#ifndef _CACTION_H
#define _CACTION_H

#include "pat.h"  


class cParcelle;

extern void cAction_InitElementsVariants(void);

// ============================================================================
// cAction : representation d une action
// ============================================================================
class cAction
{
public:  //--------------------------------------------------------------------

int Jour;                   // jour ou l action est realisee

// static void InitElementsVariants(void);
static void LibererMemoire(void){
  cAction_InitElementsVariants();
};      // liberer memoire dynamique

};

// ============================================================================
// cActionConcentre : representation d une action Concentre
// ============================================================================
class cActionConcentre: public cAction
{
public:  //--------------------------------------------------------------------
cActionConcentre (float q); // constructeur

float q_c;                  // quantite de concentre donnee a une vache en Kg

bool Realisable(void);      // test action realisable
};

// ============================================================================
// cActionMais : representation d une action Mais
// ============================================================================
class cActionMais: public cAction

{
public:  //--------------------------------------------------------------------
cActionMais (float q);      // constructeur

float q_m;                  // quantite de mais donnee a une vache en Kg

bool Realisable(void);      // test action realisable
bool Realisable(float qc, float qf); // test action realisable pour mais =
                            // complement et MAJ de q_m en Kg
};

// ============================================================================
// cActionFoin : representation d une action Foin
// ============================================================================
class cActionFoin: public cAction
{
public:  //--------------------------------------------------------------------
cActionFoin (float q);      // constructeur

float q_f;                  // quantite de foin donnee a une vache en Kg

bool Realisable(void);     // test action realisable
};


// ============================================================================
// cActionPaturage : representation d une action Paturage
// ============================================================================
class cActionPaturage: public cAction
{
public:  //--------------------------------------------------------------------
cActionPaturage (cParcelle* parcelle, DUREE_PATURAGE duree_paturage); 
                                          // constructeur
float q_h_e;                              // quantite d'herbe ingeree
cParcelle* Parcelle;                      // parcelle paturee
DUREE_PATURAGE DureePaturage;             // duree du paturage

bool Realisable(void);                    // test action realisable
};

// ============================================================================
// cActionEnsilage : representation d une action Ensilage
// ============================================================================
class cActionEnsilage: public cAction
{
public:  //--------------------------------------------------------------------
cActionEnsilage (cParcelle* parcelle);    // constructeur

cParcelle* Parcelle;                      // parcelle a ensiler

bool Realisable(void);                    // test action realisable
};

// ============================================================================
// cActionFauche : representation d une action Fauche
// ============================================================================
class cActionFauche: public cAction
{
public:  //--------------------------------------------------------------------
cActionFauche (cParcelle* parcelle);   // constructeur

cParcelle* Parcelle;                   // parcelle a faucher

bool Realisable(void);                 // test action realisable
};

// ============================================================================
// cActionAzote : representation d une action Azote
// ============================================================================
class cActionAzote: public cAction
{
public:  //--------------------------------------------------------------------
cActionAzote (float q, cParcelle* parcelle);// constructeur

float q_N;                                  // en Kg/ha
cParcelle* Parcelle;                        // parcelle a fertiliser

bool Realisable(void);                      // test action realisable
};

#endif
