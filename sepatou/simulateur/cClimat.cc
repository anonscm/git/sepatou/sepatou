// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cClimat.cc
contenu : definition des methodes de la classe cClimat
******************************************************************************/

#include <fstream>
#include <string>

#include "pat.h"

#include "cTemps.h"
#include "cClimat.h"
#include "cEnvironnementExterieur.h"
#include "cAnomalie.h"

extern cTemps Temps;
extern cEnvironnementExterieur EnvironnementExterieur;

// pour communication climat genere
typedef struct meteo
        {
        float pluie;   /* en mm */
        float tmoy;    /* en C */
        float rayon;   /* en MJ/m2 */
	float etp;     /* en mm */
        } ST_METEO;


extern string itoa (int A);

extern void WeatherGen(double lat, ST_METEO *met);


// ============================================================================
// Init : initialisation des attributs
// ============================================================================
void cClimat::Init(string localisation, int annee)
{
ifstream fT, fRg, fPluie, fEtp;
string CheminT, CheminRg, CheminPluie, CheminEtp;
string NomAnnee;
ST_METEO Meteo[NB_JOURS]; // zone pour recuperer le climat genere
int j;

if (annee >= ANNEE_GENEREE)
  {
   // creation d'un climat
   WeatherGen(EnvironnementExterieur.Latitude, &Meteo[0]);
   // mise a jour des attributs climatiques de l'objet
   for (j=1;j<NB_JOURS; j++)
      {
      Pluie[j] = Meteo[j-1].pluie;
      T[j] = Meteo[j-1].tmoy;
      if (T[j] < 0)
        Tbase0[j] = 0.0;
      else
        Tbase0[j] = T[j];
      Rg[j] = Meteo[j-1].rayon;
      ETP[j] = Meteo[j-1].etp;
      };
  }
else
  {
   NomAnnee = itoa(annee);
   CheminT = CHEMIN_CLIMATS + localisation + "/" + NomAnnee + ".T";
   CheminRg = CHEMIN_CLIMATS + localisation + "/" + NomAnnee + ".Rg";
   CheminPluie = CHEMIN_CLIMATS + localisation + "/" + NomAnnee  + ".Pluie";
   CheminEtp = CHEMIN_CLIMATS + localisation + "/" + NomAnnee  + ".Etp";
      
   fT.open (CheminT.c_str(), ios::in);
   fRg.open (CheminRg.c_str(), ios::in);
   fPluie.open (CheminPluie.c_str(), ios::in);
   fEtp.open (CheminEtp.c_str(), ios::in);

   if (!fT)
     throw (new cAnomalie ("Dans la methode cClimat.Init, probleme pour ouvrir le fichier temperature : " + CheminT) );
   if (!fRg)
     throw (new cAnomalie ("Dans la methode cClimat.Init, probleme pour ouvrir le fichier rayonnment : " + CheminRg) );
   if (!fPluie)
     throw (new cAnomalie ("Dans la methode cClimat.Init, probleme pour ouvrir le fichier pluie : " + CheminPluie ) );
  if (!fEtp)
    throw (new cAnomalie ("Dans la methode cClimat.Init, probleme pour ouvrir le fichier ETP : " + CheminEtp ) );
   
   for (j=1; j<NB_JOURS; j++)
     {
       fT >> T[j];
       if (T[j] < 0.) Tbase0[j]=0.0;
       else  Tbase0[j]=T[j];
       
       fRg >> Rg[j];
       
       fPluie >> Pluie[j];

       fEtp >> ETP[j];
     };
  };
}


void cClimat::Init(cClimat* climat)
{
  if (climat == NULL)
    throw (new cAnomalie ("Dans la methode cClimat.Init, un climat vide est sp�cifi�") );
  else
    {
      for (int j=0; j<NB_JOURS; j++) {
	// mise � jour des attributs avec le climat donn�
	T[j] = climat->T[j];
	Tbase0[j] = climat->Tbase0[j];
	Rg[j] = climat->Rg[j];
	Pluie[j] = climat->Pluie[j];
	ETP[j] = climat->ETP[j];
      }
    }
}





