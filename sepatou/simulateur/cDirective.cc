// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cDirective.cc
contenu : definition des methodes de la classe cDirective
******************************************************************************/

#include <vector>

using namespace std;

#include "cDirective.h"

extern vector <cDirective*> lDIRECTIVES;


// ============================================================================
// cDirective : constructeur
// ============================================================================
cDirective::cDirective( void (*debut)(),
                        int (* du)(),
                        int (* au)(),
                        void (* faire)(),
                        void (* fin)() )
{
DEBUT = debut;
DU = du;
AU = au;
FAIRE = faire;
FIN = fin;

Active = false;

lDIRECTIVES.push_back(this);  
}


// ============================================================================
// InitElementsVariants : Initialisation des elements variants 
//                        au jour de debut d une simulation
// ============================================================================
// ATTENTION d�finit comme fonction car pas possible de d�finir en statique
// avec upgrade compilateur
// ============================================================================
void cDirective_InitElementsVariants(void)
{
for (int i = lDIRECTIVES.size()-1; i>=0;i--)
    {
     delete lDIRECTIVES[i];
     lDIRECTIVES.pop_back();
    };
}


