// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/*************************************************************
fichier : Strategie.h
contenu : definition d'enumerations :
	      des variables utilisateur du systeme de planification
	      des variables utilisateur du systeme operatoire
	      des fonctions d'interpretations
	      des indicateurs
	  le contenu des enum est le minimum possible
          Ce fichier minimum permet de faire une pr�compilation 
          des fichiers faisant un include de Strategie.h
          sachant que le traducteur va g�n�rer un fichier complet
          incorporant les donn�es d�finies dans les autres fichiers traduits
*************************************************************/

#ifndef _STRATEGIE_H
#define _STRATEGIE_H

enum VARIABLE_SDPLANIFICATEUR {  };

enum VARIABLE_SDOPERATOIRE {  };


enum bFO_INTERPRETATION {  };

enum fFO_INTERPRETATION {  
                          NB_fFO_INTERPRETATION };

enum sFO_INTERPRETATION { 
                          NB_sFO_INTERPRETATION };

enum pFO_INTERPRETATION {  
                          NB_pFO_INTERPRETATION };

enum eFO_INTERPRETATION {  
                          NB_eFO_INTERPRETATION };

enum INDICATEUR { ENUM_I_Jdebut ,
                  ENUM_I_Jfin ,
                  ENUM_I_JN1 ,
                  ENUM_I_JN2 ,
                  ENUM_I_JN3 ,
                  NB_INDICATEUR };


#endif
