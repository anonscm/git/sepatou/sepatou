// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSBExecutant.cc
contenu : definition des methodes de la classe cSBExecutant
******************************************************************************/

#include <vector>

#include "pat.h"

#include "cSBExecutant.h"

#include "cTemps.h"
#include "cAction.h"
#include "cSDOperatoire.h"
#include "cSBBiophysique.h"
#include "cAnomalie.h"

extern cTemps Temps;
extern cSDOperatoire SDOperatoire;
extern cSBBiophysique SBBiophysique;

// ============================================================================
// InitElementsVariants : initialisation en debut de simulation
// ============================================================================
void cSBExecutant::InitElementsVariants(void)
{
Probleme = false;
RAZlActionsASimuler();
RAZlActionsNonRealisables();
}

// ============================================================================
// LibererMemoire : liberer memoire dynamique
// ============================================================================
void cSBExecutant::LibererMemoire(void)
{
RAZlActionsASimuler();
}

// ============================================================================
// RAZlActionsASimuler : vide les listes des actions a simuler
// ============================================================================
void cSBExecutant::RAZlActionsASimuler(void)
{
lActionsConcentreASimuler.clear();
lActionsMaisASimuler.clear();
lActionsFoinASimuler.clear();
lActionsPaturageASimuler.clear();
lActionsEnsilageASimuler.clear();
lActionsFaucheASimuler.clear();
lActionsAzoteASimuler.clear();
}

// ============================================================================
// RAZlActionsNonRealisables : vide les listes des actions non realisables
// ============================================================================
void cSBExecutant::RAZlActionsNonRealisables(void)
{
lActionsConcentreNonRealisables.clear();
lActionsMaisNonRealisables.clear();
lActionsFoinNonRealisables.clear();
lActionsPaturageNonRealisables.clear();
lActionsEnsilageNonRealisables.clear();
lActionsFaucheNonRealisables.clear();
lActionsAzoteNonRealisables.clear();
}

// ============================================================================
// Evoluer :
// si sortie = PB, actions non  realisables dans lActionsXNonRealisables
// ============================================================================
RESULTAT cSBExecutant::Evoluer(void)
{
RAZlActionsASimuler();
RAZlActionsNonRealisables();

if (SDOperatoire.NoJourlACTIONS == Temps.J)
  if (Realisable())
       { 
        lActionsConcentreASimuler = SDOperatoire.lActionsConcentreARealiser;
	lActionsMaisASimuler = SDOperatoire.lActionsMaisARealiser;
	lActionsFoinASimuler = SDOperatoire.lActionsFoinARealiser;
	lActionsPaturageASimuler = SDOperatoire.lActionsPaturageARealiser;
	lActionsEnsilageASimuler = SDOperatoire.lActionsEnsilageARealiser;
        lActionsFaucheASimuler = SDOperatoire.lActionsFaucheARealiser;
	lActionsAzoteASimuler = SDOperatoire.lActionsAzoteARealiser;
        for (int i=0; i<NB_ACTIONS; i++) 
	       OrdreActions[i] = SDOperatoire.OrdreActions[i];
        SBBiophysique.Evoluer();
        Probleme = false;
        return OK;
        }
   else
       {
	Probleme = true;
        return PB;
       }
else
  {
  SBBiophysique.Evoluer();
  Probleme = false;
  return OK;
  };  
}

// ============================================================================
// Realisable : examine si les actions demandees de realiser le sont vraiment
// dans le cas d actions ou une quantite n'est pas numerique, calcul de cette
// valeur.
// ============================================================================
bool cSBExecutant::Realisable(void)
{
int i;
float qc, qf;
bool realisable = true;

for (i=SDOperatoire.lActionsConcentreARealiser.size()-1; i>=0; i--)
  if ( ! SDOperatoire.lActionsConcentreARealiser[i]->Realisable() )
   {
   lActionsConcentreNonRealisables.push_back(SDOperatoire.lActionsConcentreARealiser[i]);
   realisable = false;
   };
for (i=SDOperatoire.lActionsMaisARealiser.size()-1; i>=0; i--)
  if ( SDOperatoire.lActionsMaisARealiser[i]->q_m == MAIS_COMPLEMENT)
      {
      if (SDOperatoire.lActionsConcentreARealiser.size()==0)
         qc = 0;
      else 
         if (SDOperatoire.lActionsConcentreARealiser.size()==1)
           qc = SDOperatoire.lActionsConcentreARealiser[0]->q_c;
         else
           throw (new cAnomalie (" Dans la methode cSBExecutant::Realisable, plus d une action concentre a prendre en compte avec un complement de mais.") );
      if (SDOperatoire.lActionsFoinARealiser.size()==0)
         qf = 0;
      else 
         if (SDOperatoire.lActionsFoinARealiser.size()==1)
           qf = SDOperatoire.lActionsFoinARealiser[0]->q_f;
         else
           throw (new cAnomalie (" Dans la methode cSBExecutant::Realisable, plus d une action foin a prendre en compte avec un complement de mais.") );
      if (SDOperatoire.lActionsPaturageARealiser.size()!=0)
           throw (new cAnomalie (" Dans la methode cSBExecutant::Realisable, une action paturage a prendre en compte avec un complement de mais.") ); 
      if ( ! SDOperatoire.lActionsMaisARealiser[i]->Realisable(qc, qf) )
         {
          lActionsMaisNonRealisables.push_back(SDOperatoire.lActionsMaisARealiser[i]);
          realisable = false;
         };
      }
  else 
      if ( ! SDOperatoire.lActionsMaisARealiser[i]->Realisable() )
          {
          lActionsMaisNonRealisables.push_back(SDOperatoire.lActionsMaisARealiser[i]);
          realisable = false;
           };
for (i=SDOperatoire.lActionsFoinARealiser.size()-1; i>=0; i--)
  if ( ! SDOperatoire.lActionsFoinARealiser[i]->Realisable() )
         {
          lActionsFoinNonRealisables.push_back(SDOperatoire.lActionsFoinARealiser[i]);
          realisable = false;
         };
for (i=SDOperatoire.lActionsPaturageARealiser.size()-1; i>=0; i--)
  if ( ! SDOperatoire.lActionsPaturageARealiser[i]->Realisable() )
     {
     lActionsPaturageNonRealisables.push_back(SDOperatoire.lActionsPaturageARealiser[i]);
     realisable = false;
     };
for (i=SDOperatoire.lActionsEnsilageARealiser.size()-1; i>=0; i--)
  if ( ! SDOperatoire.lActionsEnsilageARealiser[i]->Realisable() )
    {
    lActionsEnsilageNonRealisables.push_back(SDOperatoire.lActionsEnsilageARealiser[i]);
    realisable = false;
    };
for (i=SDOperatoire.lActionsFaucheARealiser.size()-1; i>=0; i--)
  if ( ! SDOperatoire.lActionsFaucheARealiser[i]->Realisable() )
     {
     lActionsFaucheNonRealisables.push_back(SDOperatoire.lActionsFaucheARealiser[i]);
     realisable = false;
     };
for (i=SDOperatoire.lActionsAzoteARealiser.size()-1; i>=0; i--)
  if ( ! SDOperatoire.lActionsAzoteARealiser[i]->Realisable() )
   {
   lActionsAzoteNonRealisables.push_back(SDOperatoire.lActionsAzoteARealiser[i]);
   realisable = false;
   };
return realisable;
}


