// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cEnsembleParcelles.cc
contenu : definition des methodes de la classe cEnsembleParcelles
******************************************************************************/

#include <string>
#include <vector>

#include "cEnsembleParcelles.h"

#include "pat.h"
#include "cTemps.h"
#include "cSIObservation.h"
#include "cParcelle.h"
#include "cAnomalie.h"

extern cTemps Temps;
extern cSIObservation SIObservation;


// ============================================================================
// Parmi : une parcelle est-elle parmi l'ensemble
// ============================================================================
bool cEnsembleParcelles::Parmi(cParcelle* p)
{
bool appartient = false;

for (unsigned int i=0; i<l.size(); i++)
  if (l[i] == p)
    {
     appartient = true;
     i = l.size();
    };
return appartient;
}

// ============================================================================
// NonVide : vrai si l'ensemble est non vide
// ============================================================================
bool cEnsembleParcelles::NonVide(void)
{
if (l.size() != 0)
  return true;
else
  return false;
}

// ============================================================================
// Mixte : renvoie l'ensemble des parcelles mixte
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::Mixte(void)
{
cEnsembleParcelles E;
for (unsigned int i=0; i<l.size(); i++)
   if (l[i]->Caracteristique == CARACTERISTIQUE_MIXTE)
      E.l.push_back(l[i]);
return E;
}

// ============================================================================
// NonMixte : renvoie l'ensemble des parcelles mixte
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::NonMixte(void)
{
cEnsembleParcelles E;
for (unsigned int i=0; i<l.size(); i++)
   if (l[i]->Caracteristique == CARACTERISTIQUE_PATURABLE)
      E.l.push_back(l[i]);
return E;
}

// ============================================================================
// MinMS : renvoie une parcelle avec MS min
// ============================================================================
cParcelle* cEnsembleParcelles::MinMS(void)
{
cParcelle* p = NULL;

if (l.size()!=0)
  {
   p = l[0];
   for (unsigned int i=1; i<l.size(); i++)
      if (l[i]->MS[Temps.J] < p->MS[Temps.J])
         p = l[i];
  };

return p;
}


// ============================================================================
// MaxMS : renvoie une parcelle avec un MAS max
// ============================================================================
cParcelle* cEnsembleParcelles::MaxMS(void)
{
cParcelle* p = NULL;

if (l.size()!=0)
  {
   p = l[0];
   for (unsigned int i=1; i<l.size(); i++)
      if (l[i]->MS[Temps.J] > p->MS[Temps.J])
         p = l[i];
  };

return p;
}

// ============================================================================
// PlusAncienneNonUtilisee : parcelle ou a eu lieu le plus ancien paturage,
// ensilage, fauche sans rien (excepte Azote) depuis
// ============================================================================
cParcelle* cEnsembleParcelles::PlusAncienneNonUtilisee(void)
{
cParcelle* p = NULL;

if (l.size()!=0)
  {
   p = l[0];
   for (unsigned int i=1; i<l.size(); i++)
      if (l[i]->Joter < p->Joter)
         p = l[i];
  };
return p;
}

// ============================================================================
// DerniereUtilisationAvantXJours : ensemble des parcelles dont le dernier
// paturage, ensilage, fauche a ete fait avant X jours
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::DerniereUtilisationAvantXJours(int n)
{
cEnsembleParcelles E;

for (unsigned int i=0; i<l.size(); i++)
    if (l[i]->Joter < Temps.J - n)
      E.l.push_back(l[i]);

return E;
}


// ============================================================================
// MSSuperieurA : renvoie l'ensemble des parcelles dont MS est superieure 
// a la valeur de l'argument 
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::MSSuperieureA(float ms)
{
cEnsembleParcelles E;

for (unsigned int i=0; i<l.size(); i++)
    if (l[i]->MS[Temps.J] >= ms)
      E.l.push_back(l[i]);

return E;
}


// ============================================================================
// AjouterNom : ajouter une parcelle decrite par son nom
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::AjouterNom(string nom)
{
cEnsembleParcelles E;
E.l = l;
cParcelle* p = NULL;

// on cherche la parcelle de ce nom
for (unsigned int i=0; i<SIObservation.Parcelles().l.size(); i++)
  if (SIObservation.Parcelles().l[i]->Nom == nom )
    p = SIObservation.Parcelles().l[i];
if (p == NULL)
  throw (new cAnomalie (" Dans la methode cEnsembleParcelles.AjouterNom, il n'y a pas de parcelle du nom donne") );

// si elle n'y est pas deja, on l'ajoute
if (!E.Parmi(p))
  E.l.push_back(p);
return E;
}

// ============================================================================
// EnleverNom : enlever une parcelle decrite par son nom
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::EnleverNom(string nom)
{
cEnsembleParcelles E;
E.l = l;
cParcelle* p = NULL;

// on cherche la parcelle de ce nom
for (unsigned int i=0; i<SIObservation.Parcelles().l.size(); i++)
  if (SIObservation.Parcelles().l[i]->Nom == nom )
    p = SIObservation.Parcelles().l[i];
if (p == NULL)
  throw (new cAnomalie (" Dans la methode cEnsembleParcelles.EnleverNom, il n'y a pas de parcelle du nom donne") );

// on l'enleve
for (unsigned int i=0; i<E.l.size(); i++)
  if (E.l[i] == p)
    {
      E.l.erase(E.l.begin()+i);
      i = E.l.size()+1;
    };
return E;
}

// ============================================================================
// Ajouter : ajouter une parcelle decrite par un pointeur
// ============================================================================
void cEnsembleParcelles::Ajouter(cParcelle* p)
{
if (! Parmi(p))
  l.push_back(p);
}

// ============================================================================
// operator + : ajouter une parcelle decrite par son pointeur
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::operator + (cParcelle* p)
{
cEnsembleParcelles E;
E.l = l;
if (! E.Parmi(p))
  E.l.push_back(p);
return E;
}

// ============================================================================
// Enlever : enlever une parcelle decrite par un pointeur
// ============================================================================
void cEnsembleParcelles::Enlever(cParcelle* p)
{
for (unsigned int i=0; i<l.size(); i++)
  if (l[i] == p)
    {
      l.erase(l.begin()+i);
      i = l.size()+1;
    };
}


// ============================================================================
// operator - : enlever une parcelle decrite par son pointeur
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::operator - (cParcelle* p)
{
cEnsembleParcelles E;

for (unsigned int i=0; i<l.size(); i++)
  if (l[i] != p)
    E.l.push_back(l[i]);

return E;
}


// ============================================================================
// operator - : enlever un ensemble de parcelles
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::operator - (cEnsembleParcelles E)
{
cEnsembleParcelles Eresultat;

for (unsigned int i=0; i<l.size(); i++)
  if (!E.Parmi(l[i]))
    Eresultat.l.push_back(l[i]);

return Eresultat;
}


// ============================================================================
// operator + : ajouter un ensemble de parcelles
// ============================================================================
cEnsembleParcelles cEnsembleParcelles::operator + (cEnsembleParcelles E)
{
cEnsembleParcelles Eresultat;

Eresultat.l = l;

for (unsigned int i=0; i<E.l.size(); i++)
  if (!Eresultat.Parmi(E.l[i]))
    Eresultat.l.push_back(E.l[i]);

return Eresultat;
}


// ============================================================================
bool cEnsembleParcelles::operator == (cEnsembleParcelles E)
{
return E.l == l;
}


// ============================================================================
bool cEnsembleParcelles::operator != (cEnsembleParcelles E)
{
return E.l != l;
}


