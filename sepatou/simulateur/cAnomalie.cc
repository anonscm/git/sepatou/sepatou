// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cAnomalie.cc
contenu : definition des methodes de la classe cAnomalie
******************************************************************************/

#include <iostream>
#include <string>

#include "cAnomalie.h"
#include "cSimulateur.h"

extern cSimulateur Simulateur;


// ============================================================================
//  cAnomalie : constructeur
// ============================================================================
cAnomalie::cAnomalie(string cause)
{
Cause = cause;
}

// ============================================================================
//  cAnomalie : constructeur
// ============================================================================
void cAnomalie::Afficher(void)
{
cout << "----------- ANOMALIE ----------" << endl;
cout << "Annee : " << Simulateur.Annee << endl;
cout << Cause << endl;
cout << "-------------------------------" << endl;
}

