// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cClimat.h
contenu : definition de la classe cClimat
******************************************************************************/
#ifndef _CCLIMAT_H
#define _CCLIMAT_H

#include <string>

using namespace std;

#include "pat.h"


// ============================================================================
// cClimat : representation d un climat
// ============================================================================
class cClimat
{
public:  //--------------------------------------------------------------------

float   T[NB_JOURS];          // temperature moyenne journaliere en C
float   Tbase0[NB_JOURS];     // T en remplacant les temperatures < 0 par 0
float   Rg[NB_JOURS];         // rayonnement moyen journalier en MJ/m2
float   Pluie[NB_JOURS];      // pluie moyenne journaliere en mm
float   ETP[NB_JOURS];        // ETP en mm

void Init(string localisation, int annee);  // initialisation des attributs
void Init(cClimat* climat);                  
};


#endif
