// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cEnvironnementExterieur.cc
contenu : definition des methodes de la classe cEnvironnementExterieur
******************************************************************************/

#include <string>

#include "pat.h"

#include "cEnvironnementExterieur.h"
#include "cClimat.h"
#include "cAnomalie.h"

extern cClimat Climat;

extern void InitGen(const char *ParamFile, const char *DatDir, unsigned int seed); 

// ============================================================================
// InitElementsInvariants : Initialisation des elements invariants 
//                          lors des simulations
// ============================================================================
void cEnvironnementExterieur::InitElementsInvariants(string localisation, 
                                                     unsigned int seed)
{
string CheminParaStatClimat, NomParaStatClimat; 

Seed = seed;
LocalisationClimat = localisation;

if (localisation == "Segala")
  Latitude = 44.2;
else
  if (localisation == "Rennes")
    Latitude = 48.1;
  else throw (new cAnomalie ("Dans la methode cEnvironnementExterieur::InitElementsInvariants, cette localisation de station m�t�o n'est pas accept�e : " + localisation ) );

// initialisation du generateur climatique
CheminParaStatClimat =  CHEMIN_CLIMATS + LocalisationClimat ;
NomParaStatClimat = LocalisationClimat + ".sta";

InitGen(NomParaStatClimat.c_str(), CheminParaStatClimat.c_str(), Seed);
}


// ============================================================================
// InitElementsVariants : Initialisation des elements variants 
//                        au jour de debut d une simulation
// ============================================================================
void cEnvironnementExterieur::InitElementsVariants(int annee)
{
Climat.Init(LocalisationClimat,annee);
}

void cEnvironnementExterieur::InitElementsVariants(cClimat* climat)
{
Climat.Init(climat);
}
