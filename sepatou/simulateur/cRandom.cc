// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cRandom.cc
contenu : definition des m�thodes de la classe cRandom
******************************************************************************/

using namespace std;

#include <iostream>
#include <stdlib.h>

#include "cRandom.h"


// ============================================================================
// Init : initialisation du g�n�rateur climatique
// ============================================================================
void cRandom::Init(unsigned int seed)
{
  int j;
  double dum;
  
  srand (seed);
  // sp�cialement important quand le multiplicateur : a utlis� par rand est petit
  // rand() �tant une relation de r�currence I(j+1) = a I(j) + c   (mod m)
  // qui g�n�re une s�quence d'entiers  entre 0 et m-1 (e.g. RAND_MAX)
  // "linear congruential generators"
  for (j=1; j<=97;j++) dum = rand();
  for (j=1; j<=97;j++) v[j] = rand();
  y = rand();

  Initialise = true;
  MaxRand = RAND_MAX+1.0;
}



// ============================================================================
// Generate01 : g�n�re un nombre entre 0 et 1
// ============================================================================
double cRandom::Generate01(void)
{
  int j;
  
  if (Initialise)
    {
      j = 1 +  (int) (97.0 *  y/MaxRand);
      y = v[j];
      v[j] = rand();
      return y/MaxRand;
    }
  else
    {
      cout << "Dans la methode cRandom::Generate01, demande de g�n�rer un nombre alors que le g�n�rateur n'est pas initialis�" <<endl;
      return 0;
    }
}
