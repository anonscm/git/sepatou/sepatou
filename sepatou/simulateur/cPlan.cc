// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cPlan.cc
contenu : definition des methodes des classes cPlan, cPlanAlimentationConservee,
          cPlanPaturage, cPlanFauche, cPlanFertilisation, cPlanCoordination
******************************************************************************/

#include <vector>

#include "cPlan.h"

#include "pat.h"

#include "cDirective.h"
#include "cTemps.h"
#include "cAnomalie.h"
#include "cEnsembleParcelles.h"


extern cEnsembleParcelles EnsembleParcellesVide;
extern cPlanAlimentationConservee PlanAlimentationConservee;
extern cPlanPaturage PlanPaturage;
extern cPlanFauche PlanFauche;
extern cPlanFertilisation PlanFertilisation;
extern cPlanCoordination PlanCoordination;
extern cTemps Temps;


// ============================================================================
// cPlan::InitElementsVariants : Initialisation des elements variants 
//                               au jour de debut d une simulation 
// ============================================================================
// ATTENTION d�finit comme fonction car pas possible de d�finir en statique
// avec upgrade compilateur
// ============================================================================
void cPlan_InitElementsVariants(void)
{
PlanAlimentationConservee.Init();
PlanPaturage.Init();
PlanFauche.Init();
PlanFertilisation.Init();
PlanCoordination.Init();
}




// ============================================================================
// cPlan::PlanDeActivite : retourne le plan associe 
// ============================================================================
// ATTENTION d�finit comme fonction car pas possible de d�finir en statique
// avec upgrade compilateur
// ============================================================================
cPlan* cPlan_PlanDeActivite(ACTIVITE activite)
{
switch (activite)
  {
  case ACTIVITE_ALIMENTATION_CONSERVEE : return &PlanAlimentationConservee; break;
  case ACTIVITE_PATURAGE : return &PlanPaturage; break;
  case ACTIVITE_FAUCHE : return &PlanFauche; break;
  case ACTIVITE_FERTILISATION : return &PlanFertilisation; break;
  case ACTIVITE_COORDINATION : return &PlanCoordination; break;
  default : throw (new cAnomalie ("Dans la methode cPlan::PlanDeActivite, un numero d'activite non reconnu est donne.") );
  };
}
 

// ============================================================================
// cPlan::MAJPlans : mise a jour des plans
// ============================================================================
// ATTENTION d�finit comme fonction car pas possible de d�finir en statique
// avec upgrade compilateur
// ============================================================================
void cPlan_MAJPlans(void)
{
PlanAlimentationConservee.MAJ();
PlanPaturage.MAJ();
PlanFauche.MAJ();
PlanFertilisation.MAJ();
PlanCoordination.MAJ();
}


// ============================================================================
// cPlan.AjouterDirective :Init ajout d'une directive au plan
// ============================================================================
void cPlan::AjouterDirective (cDirective* d)
{
lDirectives.push_back(d);
}


// ============================================================================
// cPlan.MAJ : mise a jour de la directive active
//             mise a jour des variables de decision du plan si
//                 - nouvelle directive (FAIRE)
//                 - pas de directive active (InitVariableDecision)
// ============================================================================
void cPlan::MAJ(void)
{
if (DirectiveActive != NULL)
    // une directive est en cours
    if (DirectiveActive->AU() == Temps.J) 
	  {// elle cesse d'etre active
           if (DirectiveActive->FIN != NULL)
              DirectiveActive->FIN();
           DirectiveActive->Active = false;
           DirectiveActive = NULL;
          }
    else
          if ( (DirectiveActive->AU() >= 1) &&
               (DirectiveActive->AU() < Temps.J) )
               throw (new cAnomalie ("Dans la methode cPlan.MAJ, une directive est active alors qu'elle aurait du etre desactive avant ce jour.") );

if (DirectiveActive==NULL)
    for (unsigned int i=0; i<lDirectives.size();i++)
       if ( lDirectives[i]->DU() == Temps.J )
	 {
         if (lDirectives[i]->DEBUT!= NULL)
              lDirectives[i]->DEBUT();        
         lDirectives[i]->Active = true;
         DirectiveActive = lDirectives[i];
         DirectiveActive->FAIRE();
         i = lDirectives.size();
         } ;

if (DirectiveActive == NULL)
    InitVariablesDecision();

}






// ============================================================================
// cPlanAlimentationConservee.Init : initialise les attributs
// ============================================================================
void cPlanAlimentationConservee::Init(void)
{
Activite = ACTIVITE_ALIMENTATION_CONSERVEE; 

InitVariablesDecision();

DirectiveActive = NULL;

lDirectives.clear();
}

// ============================================================================
// cPlanAlimentationConserve.InitVariablesDecision
// ============================================================================
void cPlanAlimentationConservee::InitVariablesDecision(void)
{
AlimentationConcentre = 0.0;
AlimentationMais = 0.0;
AlimentationFoin = 0.0;
}


// ============================================================================
// cPlanPaturage.Init : initialise les attributs
// ============================================================================
void cPlanPaturage::Init(void)
{
Activite = ACTIVITE_PATURAGE;

InitVariablesDecision();

DirectiveActive = NULL;

lDirectives.clear();
}

// ============================================================================
// cPlanPaturage.InitVariablesDecision
// ============================================================================
void cPlanPaturage::InitVariablesDecision(void)
{
AlimentationHerbe = HERBE_INCONNU;
ParcellesAPaturer = EnsembleParcellesVide;
}


// ============================================================================
// cPlanFauche.Init : initialise les attributs
// ============================================================================
void cPlanFauche::Init(void)
{
Activite = ACTIVITE_FAUCHE;

InitVariablesDecision();

DirectiveActive = NULL;

lDirectives.clear();
}

// ============================================================================
// cPlanFauche.InitVariablesDecision
// ============================================================================
void cPlanFauche::InitVariablesDecision(void)
{
ParcellesAEnsiler = EnsembleParcellesVide;
ParcellesAFaucher = EnsembleParcellesVide;
}


// ============================================================================
// cPlanFertilisation.Init : initialise les attributs
// ============================================================================
void cPlanFertilisation::Init(void)
{
Activite = ACTIVITE_FERTILISATION;

InitVariablesDecision();

DirectiveActive = NULL;

lDirectives.clear();
}

// ============================================================================
// cPlanFertilisation.InitVariablesDecision
// ============================================================================
void cPlanFertilisation::InitVariablesDecision(void)
{
ParcellesAFertiliser = EnsembleParcellesVide;
RegimeAzote = REGIME_AZOTE_INCONNU;
}


// ============================================================================
// cPlanCoordination.Init : initialise les attributs
// ============================================================================
void cPlanCoordination::Init(void)
{
Activite = ACTIVITE_COORDINATION;

DirectiveActive = NULL;

InitVariablesDecision();

lDirectives.clear();
}

// ============================================================================
// cPlanCoordination.InitVariablesDecision
// ============================================================================
void cPlanCoordination::InitVariablesDecision(void)
{
for (int i= NB_ACTIVITES - 1; i>=0; i--)
      OrdreActivites[i]= ACTIVITE_INCONNU;
}


