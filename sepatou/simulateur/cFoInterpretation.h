// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cFoInterpretation.h
contenu : definition des classes cFoInterpretation, cbFoInterpretation,
          cfFoInterpretation, cpFoInterpretation, ceFoInterpretation,
          csFoInterpretation,
******************************************************************************/
#ifndef _CFOINTERPRETATION_H
#define _CFOINTERPRETATION_H

#include <string>

using namespace std;

class cParcelle;
class cEnsembleParcelles;

// ============================================================================
// cFoInterpretation : representation d une fonction d interpretation
// ============================================================================
class cFoInterpretation
{
public:  //--------------------------------------------------------------------
string      Nom;          // nom de la fonction

};

// ============================================================================
// ciFoInterpretation : representation d une fonction d interpretation
//                      renvoyant un booleen
// ============================================================================
class cbFoInterpretation : public cFoInterpretation
{
public:  //--------------------------------------------------------------------
cbFoInterpretation(bool (*code)(), string nom);

bool (* CODE)();

};

// ============================================================================
// cfFoInterpretation : representation d une fonction d interpretation
//                      renvoyant un flottant
// ============================================================================
class cfFoInterpretation : public cFoInterpretation
{
public:  //--------------------------------------------------------------------
cfFoInterpretation(float (*code)(), string nom);

float (* CODE)();

};

// ============================================================================
// cpFoInterpretation : representation d une fonction d interpretation
//                      renvoyant un pointeur sur une parcelle
// ============================================================================
class cpFoInterpretation : public cFoInterpretation
{
public:  //--------------------------------------------------------------------
cpFoInterpretation(cParcelle* (*code)(), string nom);

cParcelle* (* CODE)();

};

// ============================================================================
// ceFoInterpretation : representation d une fonction d interpretation
//                      renvoyant un pointeur sur un ensemble de parcelles
// ============================================================================
class ceFoInterpretation : public cFoInterpretation
{
public:  //--------------------------------------------------------------------
ceFoInterpretation(cEnsembleParcelles (*code)(), string nom);
cEnsembleParcelles (* CODE)();

};

// ============================================================================
// csFoInterpretation : representation d une fonction d interpretation
//                      renvoyant une string
// ============================================================================
class csFoInterpretation : public cFoInterpretation
{
public:  //--------------------------------------------------------------------
csFoInterpretation(string (*code)(), string nom);
string (* CODE)();

};

#endif
