/* 
****************************************************************** 
fichier : generateur/GenerationClimat/gen_seed.c
contenu : Stochastic Weather Simulation Model 
autheur : M. Semenov modifie par F. Garcia, M.-J Cros  
******************************************************************* 
*/

using namespace std;

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "cRandom.h"
extern cRandom Random;

#define DRY 0
#define WET 1
#define MaxF 4
#define PERIOD  365
#define Pi 3.14159265



/******************* definition de structures **************************/
typedef struct meteo
        {
        float pluie;   /* en mm */
        float tmoy;    /* en C */
        float rayon;   /* en MJ/m2 */
	float etp;     /* en mm */
} ST_METEO;

typedef struct fourier
{
	float a[10];	/* a[], b[] Fourier coefficients */
	float b[10];
	int n;		/* number of Fourier series */
	float T;	/* period for Fourier integration */
} Fourier;



/****************** Global data discription ********************************/

/* Wet & Dry series: */ 
/*  probability of occurence (PR) and average lenght (MN) */
	Fourier WetMN, WetLPR, WetLMN, DryMN, DryLPR, DryLMN;

/* Precipitation probability of occuarence (PR)of M and L groups */
/* and mean (MN) amount, mm */
	Fourier RainMPR, RainMMN, RainLPR, RainLMN;

/* Moy temperature */
/* mean (MN) and sd (SD) for wet/dry  (W/D) series, C */
	Fourier MoyDMN, MoyDSD, MoyWMN, MoyWSD; 

/* Radiation mean (MN) and sd (SD) for wet/dry (W/D) series, hours */
	Fourier RadDMN, RadDSD, RadWMN, RadWSD;

/* Coefficients de correlation */
        Fourier RadCor, MoyCor;

/* Linear Regression of ETP */
        float  Alpha, Beta, Gamma;



/* declaration de certaines fonctions du fichier ******************/
void WeatherOutput(ST_METEO *res, int day, int seria, double lat);
void FourierInit(char *file);
int WetSeries(int day);
int DrySeries(int day); 
float Precipitation(int day);
float  MoyTem(int day, int SerType);
float  Rad(int day, int SerType);
void MaxMinRadiation(int day, double latitude, double *MinRad, double *MaxRad);
float ExpDistr(double lambda);
float NormDistr(void);
int Choice(int n, float p[]);


/* *****************************************************/
/*    definition des fonctions relative a Fourier      */
/* *****************************************************/
float GetValue(Fourier *f, int t)
{ 
  int i;
  float value,omega;
  
  omega = 2*Pi/f->T;
  value = f->a[0]/2;
  for(i=1;i<f->n;++i)
    value += f->b[i]*sin(omega*i*t)+f->a[i]*cos(omega*i*t);
  
  return(value);
}

void Init(Fourier *f, float Period, int N, float *A, float *B)
{
  int i;
  
  f->T = Period;
  f->n = N;
  for(i=0;i<N; ++ i){
    f->a[i] = *(A+i);
    f->b[i] = *(B+i);
  }
}


/*-----------------------------------*/
/*   Model parameter input           */
/*-----------------------------------*/
#define mov_com do fgets(ln,MAX_LINE,dat); while (ln[0] == '/') 
#define MAX_LINE 200 
#define FOR4 "%f %f %f %f" 
#define FOR3 "%f %f %f"

void FourierInit(char *file) 
{  
  FILE *dat; 
  char ln[MAX_LINE]; 
  float a[10], b[10]; 
  
  if((dat=fopen(file, "r"))==NULL)
    { printf("\nParameter File not found ... %s \n", file);
    exit(0);
    }
  /* Series parameters: */
  /* probability of occuarence (PR) and avearege lenght (MN) */
  /* for series (<=8) and series (>8) */
  
  /* Wet series */
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&WetMN, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&WetLPR, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&WetLMN, PERIOD,MaxF,a,b);
  
  /* Dry series */
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&DryMN, PERIOD,MaxF,a,b);

  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&DryLPR, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&DryLMN, PERIOD,MaxF,a,b);
  
  /* Precipitation parameters, probability and amout of L and M groups, mm */
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&RainMPR, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&RainLPR, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&RainMMN, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&RainLMN, PERIOD,MaxF,a,b);
  
  /* Moy temperature parameters (MN and SD) */
  /* for wet and dry series (W, D) */
  /* Wet series */
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&MoyWMN, PERIOD,MaxF,a,b);
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&MoyWSD, PERIOD,MaxF,a,b);
  /* Dry series */
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&MoyDMN, PERIOD,MaxF,a,b);
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&MoyDSD, PERIOD,MaxF,a,b);
  /* correlations */
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&MoyCor, PERIOD,MaxF,a,b);
  
  
  /* Radiation parameters (MN and SD) for wet and dry series (W, D) */
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&RadWMN, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&RadWSD, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&RadDMN, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&RadDSD, PERIOD,MaxF,a,b);
  
  mov_com; sscanf(ln,FOR4,a,a+1,a+2,a+3);
  mov_com; sscanf(ln,FOR4,b,b+1,b+2,b+3);
  Init(&RadCor, PERIOD,MaxF,a,b);
  
  /* parametres regression ETP */
  mov_com; sscanf(ln,FOR3, a,a+1,a+2);
  Alpha = a[0];
  Beta = a[1];
  Gamma = a[2];

  fclose(dat); 
}


/*-------------------------------------------------- */
/*    InitGen is a programm which initializes weather generator  */
/*    ParFile	- parameter file */
/*    DatDir	- data directory for parameter file */
/*-------------------------------------------------- */
void InitGen(const char *ParamFile,const char *DatDir, unsigned int seed)
{ 
  char ParamPath[100];
  
  /* Initialize Fourier classes */
  strcpy(ParamPath, DatDir);
  strcat(ParamPath,"/");
  strcat(ParamPath, ParamFile);
  
  FourierInit(ParamPath);
  
  /* initialisation du generateur aleatoire */
  Random.Init(seed);
}


/*-----------------------------------*/
/*     Random choice                 */
/*  n    - number of choices         */
/*  p[]  - probability vector        */
/*-----------------------------------*/
int Choice(int n, float p[])
{ 
  int i;
  float x;
  x = Random.Generate01();
  for(i=0; i<n; ++i) if( x <= p[i]) return(i+1);
  return(n);
}


/*--------------------------------------*/
/*    Exponential Distribution          */
/*  1/lambda equals mean value          */
/*--------------------------------------*/
float ExpDistr(double lambda)
{ 
  float x;
  x=Random.Generate01(); if(x==1) x = 0.999;
  return(-log(1-x)/lambda);
}

/*--------------------------------------*/
/*          Normal Distribution         */
/*  mean  - avearage                    */
/*  sigma - sd                          */
/*--------------------------------------*/
float NormDistr(void)
{ 
  float x;
  double mean=0,sigma=1;
  int i;
  x = 0;
  for(i=0; i<12; ++i)  x += Random.Generate01();
  x -= 6;
  return(x*sigma+mean);
}


/*------------------------------------------------- */
/*	Create  and store daily weather */
/*	res     - meteo structure to store daily weather     */
/*	day	- day of the year */
/*	seria	- WET/DRY seria flag */
/*------------------------------------------------- */
void WeatherOutput(ST_METEO *res, int day, int seria, double lat)
{ 
  static float ETP = 0;
  float r;
  double MaxRad, MinRad;
  
  /* Temperature, C */
  res->tmoy  = MoyTem(day,seria);
  
  /* Precipitation, mm */
  if (seria == WET ) res->pluie  = Precipitation(day);
  else res->pluie = 0.;
  
  /* Radiation, MJ/m2/j */
  r = Rad(day,seria);
  MaxMinRadiation(day, lat, &MinRad, &MaxRad);  
  if (r >= MaxRad)
    res->rayon = MaxRad;
  else
    if (r <= MinRad)
      res->rayon = MinRad;
    else
      res->rayon = r;
  
  /* Etp based on linear regression */
  ETP = Alpha * res->tmoy * res->tmoy + Beta * res->rayon * res->rayon + Gamma * ETP; 
  res->etp = ETP;
}

/*------------------------------------*/
/*   Modeling of Wet Series          */
/*------------------------------------*/
int WetSeries(int day) 
{ 
  float p[15]; 
  int s; 
  int EndSer=8; 
  double lamda; 
  /* Probablity interval for a mixed distribution */ 
  p[0] = GetValue(&WetLPR, day); 
  p[1] = 1; 
  
  /* Lenght of series */
  switch(Choice(2,p)) 
    { case 1: /* Series > 8 */  
      lamda = 1/GetValue(&WetLMN, day); 
    s=(int)(ExpDistr(lamda)+0.5+EndSer); 
    break; 
    
    case 2: /* Series < 8 */
      lamda = GetValue(&WetMN, day); 
      s=(int)(ExpDistr(lamda)+0.6); 
      break; 
    }
  if( s == 0 ) s=1; 
  
  return(s); 
} 

/*------------------------------------*/
/*   Modeling of Dry Series          */
/*------------------------------------*/
int DrySeries(int day) 
{
  float p[15]; 
  int s; 
  int EndSer=8; 
  double lamda; 
  /* Probablity interval for a mixed distribution */
  p[0] = GetValue(&DryLPR, day); 
  p[1] = 1; 
  /* Lenght of series */ 
  switch(Choice(2,p)) 
    { case 1: /* Series > 8 */ 
      lamda = 1/GetValue(&DryLMN, day); 
    s=(int)(ExpDistr(lamda)+0.5+EndSer); 
    break; 
    case 2: /* Series < 8 */ 
      lamda = GetValue(&DryMN, day); 
      s=(int)(ExpDistr(lamda)+0.6); 
      break; 
    }
  if( s == 0 ) s=1; 
  
  return(s); 
} 



/*-----------------------------------*/
/*    Modelling of precipitation     */
/*-----------------------------------*/
float Precipitation(int day)
{ 
  float w,p[10],x,lambda;
  
  /*for  CLAIRE ONLY! Precipitaion should be adjested by this vector */
  /*to compensate decrease in in mean monthly precipittaion */
  /* The following vecor for RES */
  /* float R[]= {1.26,1.99,1.90,1.44,1.40,1.26,1.99,1.58,1.54,1.27,1.33,1.77}; */
  
  /* Probability interval for L, M and S groups */
  p[0] = GetValue(&RainLPR, day);		/* (0,p[0])    -L */
  p[1] = p[0] + GetValue(&RainMPR, day); 	/* (p[0],p[1]) -M */
  p[2] = 1;                                   /* (p[1],p[2]) -S */
  
  /* Amount of precipitation, mm */
  switch(Choice(3,p))
    {
    case 1: /* L - precipitation, average value */
      w = GetValue(&RainLMN, day);
      break;
    case 2: /* M - precipitation, exponentially distributed */
      lambda  = 1/GetValue(&RainMMN, day);
      w = ExpDistr(lambda);
      break;
    case 3: /* S - precipitation, uniformly distributed */
      w = 0.2;
      x = 3*Random.Generate01();
      if(x < 1) w = 0.1;
      if(2 < x) w = 0.3;
      
      break;
    };
  
  return(w);
}


/* Function Month(day) return the month number for a day */
int Month(int day)
{
  int DayInMonth[] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
  int k;
  k = 0;
  while( (day = day - DayInMonth[k]) > 0 ) ++k;
  return(k+1);
}

/*------------------------------------------*/
/*     Modelling of temperature             */
/*------------------------------------------*/
float  MoyTem(int day, int SerType)
{ 
  float mean, sigma, ro;
  static float Resid, NewResid, PrevResid=0;
  
  /* Calculate mean and sd for the current day considering the series type */
  switch(SerType){
  case WET:
    mean  = GetValue(&MoyWMN, day);
    sigma = GetValue(&MoyWSD, day);
    break;
  case DRY:
    mean  = GetValue(&MoyDMN, day);
    sigma = GetValue(&MoyDSD, day);
    break;
  }
  ro = GetValue(&MoyCor, day);
  
  /* Calculate current moy temperature */
  switch(day){
  case 1:	/* If day ==1, */
    Resid = NormDistr();
    NewResid = Resid;
    break;
  default: /* for other days temperature is autocorrelated */
    Resid = NormDistr();
    NewResid =  (Resid + ro*PrevResid) / sqrt(1 + ro*ro);
    break;
  }
  PrevResid = Resid;
  
  return(NewResid*sigma + mean); 
}



/*--------------------------------------------------*/
/* Modelisation stat des radiations / fichier param */
/*--------------------------------------------------*/
float  Rad(int day, int SerType)
{ 
  float mean, sigma, ro;
  static float Resid, NewResid, PrevResid=0;

  /* Calculate mean and sd for the current day considering the series type */
  switch(SerType){
  case WET:
    mean  = GetValue(&RadWMN, day);
    sigma = GetValue(&RadWSD, day);
    break;
  case DRY:
    mean  = GetValue(&RadDMN, day);
    sigma = GetValue(&RadDSD, day);
    break;
  }
  
  ro = GetValue(&RadCor, day);
  
  /* Calculate current radiation  */
  switch(day){
    
  case 1:    /* If day == 1 than */
    Resid = NormDistr();
    NewResid = Resid;
    break;
    
  default: /* for other days radiation autocorrelated */
    Resid = NormDistr();
    NewResid =  (Resid + ro*PrevResid) / sqrt(1 + ro*ro);
    break;
  }
  
  PrevResid = Resid;
  
  return(NewResid*sigma + mean);
}




/*------------------------------------------------- */
/* MaxRadiation calculates max radiation */
/* for the location */
/*     day - day in a year */
/*     latitude - latitude of a site */
void MaxMinRadiation(int day, double latitude, double *MinRad, double *MaxRad)
{
  double  sd, h, ra, xlat, scon =1370; /* W/m2 */

  /* Change from degree to radian */
  xlat = latitude*2*Pi/360;
    
  /*solar declination */
  sd = 0.41*cos(2.0*Pi*(day-172.)/365.);
  h = acos(-tan(xlat)*tan(sd));
  
  /*radiation at the top of the atmos */
  ra = (86400./Pi)*scon*1.035*(h*sin(xlat)*sin(sd)+cos(xlat)*cos(sd)*sin(h));
  
  /*If sunshine is zero, then a = 0.10 */
  *MinRad = (ra*0.1)/1000000.;
  
  /* radiation (MJ/m2) = (ra*a)/1000000 */
  
  /* If sunshine == daylength the a= 0.34, b=0.46 */
  *MaxRad = ra*0.8/1000000.;
}



/*--------------------------------------*/
/*--------------------------------------*/
void WeatherGen(double lat, ST_METEO *met)
{ 
  int day,seria,s,i;
  
  day = 0;
  seria = DRY;
  while(day < 365 )
    switch(seria)
      {
      case WET:
	s = WetSeries(day);
	for(i=1;(i<=s)&&(day<365);++i){
	  WeatherOutput(&met[day], day+1, seria, lat);
	  day++;
	}
	seria = DRY;
	break;
      case DRY:
	s = DrySeries(day);
	for(i=1;(i<=s)&&(day<365);++i){
	  WeatherOutput(&met[day], day+1, seria, lat);
	  day++;
	}
	seria = WET;
	break;
      };
}

