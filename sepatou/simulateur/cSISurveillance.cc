// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cSISurveillance.cc
contenu : definition des methodes de la classe cSISurveillance
******************************************************************************/

#include <vector>

#include "cSISurveillance.h"

#include "pat.h"
#include "cEvenement.h"
#include "cSIObservation.h"
#include "cSDPlanificateur.h"
#include "cSDOperatoire.h"

extern cSIObservation SIObservation;
extern cSDPlanificateur SDPlanificateur;
extern cSDOperatoire SDOperatoire;

// ============================================================================
// Reveiller : au nouveau pas de temps signale, detection d evts 
//             aux SD Planificateur ou Operatoire 
//             sortie PB si SDP ne peut traiter les evts detectes
//             sortie OK si pas d'evts ou traitement reussi de tt evt detecte
// ============================================================================
RESULTAT cSISurveillance::Reveiller(void)
{
RESULTAT resultat = RESULTAT_INCONNU;
RESULTAT resultat_sdp = RESULTAT_INCONNU;
RESULTAT resultat_sdo = RESULTAT_INCONNU;

while ( (resultat != OK) && (resultat != PB) )
  {
  DetecterEvtSDPlanificateur();
  resultat_sdp = OK;
  if (lEvts.size() != 0)
     resultat_sdp = SDPlanificateur.TraiterEvenements(lEvts);
  if (resultat_sdp != OK)
     resultat = PB;
  else 
    {
     DetecterEvtSDOperatoire();
     resultat_sdo = OK;
     if (lEvts.size() != 0)
       resultat_sdo = SDOperatoire.TraiterEvenements(lEvts);
     if (resultat_sdo == OK)
         resultat = OK;
    };
  };
return resultat;
}


// ============================================================================
//  DetecterEvtSDPlanificateur : met a jour lEvts avec les evts detectes
//                               pour le SD Planificateur
// ============================================================================
void  cSISurveillance::DetecterEvtSDPlanificateur(void)
{
lEvts.clear();

for (int i=SDPlanificateur.lEvtADetecter.size()-1; i>=0; i--)
    if (SDPlanificateur.lEvtADetecter[i]->DETECTER()) 
        lEvts.push_back(SDPlanificateur.lEvtADetecter[i]);
}


// ============================================================================
//  DetecterEvtSDOperatoire : met a jour lEvts avec les evts detectes
//                            pour le SD Operatoire
// ============================================================================
void  cSISurveillance::DetecterEvtSDOperatoire(void)
{
lEvts.clear();

for (int i=SDOperatoire.lEvtADetecter.size()-1; i>=0; i--)
    if (SDOperatoire.lEvtADetecter[i]->DETECTER()) 
       lEvts.push_back(SDOperatoire.lEvtADetecter[i]);
}

