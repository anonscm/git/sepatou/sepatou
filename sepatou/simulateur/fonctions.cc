// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : Fonctions.cc
contenu : definition de fonctions generales
******************************************************************************/

#include <math.h>
#include <string>
#include "cAnomalie.h"


/* itoach **********************************************************
renvoie sous forme de caractere le chiffre donne en entree
********************************************************************/
char itoach (int n)
{
if ((n<0) || (n>9))
    {return 'x';}
 else
    return '0' + n;
}

/* itoa ************************************************************
renvoie sous forme de chaine de caractere "xxxxxxx",
la date numerique donnee en argument : de 0 � 9999999
*********************************************************************/
string itoa (int A)
{
string s;
int M1, m100, m10, m1, u100, u10, u1;
float f;

if ((A<0) || (A>9999999))
  {
   throw (new cAnomalie("Dans la fonction itoa, A n'est pas dans [0 , 9 999 999]"));
   s = "";
  }
else
   {
   f = A;
   M1 = (int) f/1000000;
   m100 = (int) (f - M1*1000000)/100000;
   m10 = (int) (f - M1*1000000 - m100*100000)/10000;
   m1 = (int) (f - M1*1000000 - m100*100000 - m10*10000)/1000;
   u100 = (int) (f - M1*1000000 - m100*100000 - m10*10000 - m1*1000)/100;
   u10 = (int) (f - M1*1000000 - m100*100000 - m10*10000 - m1*1000 - u100*100)/10;
   u1 = (int) (f - M1*1000000 - m100*100000 - m10*10000 - m1*1000 - u100*100 - u10*10);

   if (A > 999999)
     s += itoach(M1);
   if (A > 99999)
     s += itoach(m100);
   if (A > 9999)
     s += itoach(m10);
   if (A > 999)
     s += itoach(m1);
   if (A > 99)
     s += itoach(u100);
   if (A > 9)
     s += itoach(u10);
   s += itoach(u1);
   };
return s;  
}


// ============================================================================
// Arrondir : renvoie le nb a 2 decimales
// ============================================================================
float Arrondir (float f)
{
  float a;
  float c = ceil(f * 100.);
  if (c - f * 100 > 0.5)
    a = (c - 1) / 100.;
  else 
    a = c / 100.;
  return a;
}


// ============================================================================
// min : renvoie le min de 2 float
// ============================================================================
float min(float x1, float x2)
{
if (x1 <= x2)
  return x1;
else
  return x2;
}


// ============================================================================
// max : renvoie le max de 2 float
// ============================================================================
float max(float x1, float x2)
{
if (x1 >= x2)
  return x1;
else
  return x2;
}


// ============================================================================
// ResoudreEquation2ndDegre : resolution d une equation : ax2 + bx + c = 0
// retourne le nombre de solution : 0, 1 (x1) ou 2 (x1 et x2)
// ============================================================================
int ResoudreEquation2ndDegre (float a, float b, float c, float & x1, float & x2)
{
float delta = pow(b,2) - 4.0 * a * c;

if (delta < 0)
  {
  x1 = -9999;
  x2 = -9999;
  return 0;
  }
else
  if (delta == 0)
    {
    x1 = - b / (2.0 * a);
    x2 = -9999;
    return 1;
    }
  else
    {
    x1 = (- b + sqrt(delta)) / (2.0 * a);
    x2 = (- b - sqrt(delta)) / (2.0 * a);
    return 2;
    }
}



// ============================================================================
// CalculerIntegralePolynome1erDegre : retourne la valeur de l integrale
//    q2
//   /  (aq+b) dq
//  q1
// ============================================================================
float CalculerIntegralePolynome1erDegre (float a, float b, float q1, float q2)
{
return ( (a/2.0) * (pow(q2,2)-pow(q1,2)) + b * (q2-q1) );
}
