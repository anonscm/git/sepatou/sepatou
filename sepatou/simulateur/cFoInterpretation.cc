// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cFoInterpretation.cc
contenu : definition des methodes des classes cbFoInterpretation, 
          cfFoInterpretation, cpFoInterpretation, ceFoInterpretation,
          csFoInterpretation
******************************************************************************/

#include <string>

#include "cFoInterpretation.h"

class cParcelle;
class cEnsembleParcelles;

// ============================================================================
// cbFoInterpretation :: cbFoInterpretation
// ============================================================================
cbFoInterpretation::cbFoInterpretation(bool (*code)(), string nom)
{
CODE = code;
Nom = nom;
}

// ============================================================================
// cfFoInterpretation :: cfFoInterpretation
// ============================================================================
cfFoInterpretation::cfFoInterpretation(float (*code)(), string nom)
{
CODE = code;
Nom = nom;
}

// ============================================================================
// cpFoInterpretation :: cpFoInterpretation
// ============================================================================
cpFoInterpretation::cpFoInterpretation(cParcelle* (*code)(), string nom)
{
CODE = code;
Nom = nom;
}

// ============================================================================
// ceFoInterpretation :: ceFoInterpretation
// ============================================================================
ceFoInterpretation::ceFoInterpretation(cEnsembleParcelles (*code)(), string nom)
{
CODE = code;
Nom = nom;
}

// ============================================================================
// csFoInterpretation :: csFoInterpretation
// ============================================================================
csFoInterpretation::csFoInterpretation(string (*code)(), string nom)
{
CODE = code;
Nom = nom;
}
