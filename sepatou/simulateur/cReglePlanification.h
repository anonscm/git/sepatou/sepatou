// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cReglePlanification.h
contenu : definition de la classe cReglePlanification
******************************************************************************/
#ifndef _CREGLEPLANIFICATION_H
#define _CREGLEPLANIFICATION_H

#include <string>

using namespace std;

#include "pat.h"

class cEvenement;

// ============================================================================
// cReglePlanification : representation d une regle de planification
// ============================================================================
class cReglePlanification
{
public:  //--------------------------------------------------------------------
cReglePlanification( bool (*declencheur)(), void (*alors)(), 
                     REUTILISATION reutilisable, string nom );
~cReglePlanification(void);

string               Nom;     // nom de la regle
REUTILISATION Reutilisable;   // reutilisation de la regle
RP_ACTIVE Active;             // regle a considerer
cEvenement* Evenement;        // evt corespondant au declenchement de la regle


bool (*DECLENCHEUR)();        // declencheur de la regle
void (*ALORS)();              // ALORS de la regle
};


#endif
