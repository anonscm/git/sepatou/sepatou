// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cParcelle.cc
contenu : definition des methodes de la classe cParcelle
******************************************************************************/

#include <vector>
#include <math.h>

#include "cParcelle.h"

#include "pat.h"
#include "cSimulateur.h"
#include "cTemps.h"
#include "cAction.h"
#include "cSDPlanificateur.h"
#include "cSBExecutant.h" 
#include "cTroupeau.h"
#include "cClimat.h"
#include "cAnomalie.h"

extern cTemps Temps;
extern cSimulateur Simulateur;
extern cSDPlanificateur SDPlanificateur;
extern cSBExecutant SBExecutant;
extern cTroupeau Troupeau;
extern cClimat Climat;

extern float min(float x1, float x2);
extern float max(float x1, float x2);
extern float CalculerIntegralePolynome1erDegre(float a, float b, float q1,float q2);
extern int ResoudreEquation2ndDegre(float a, float b, float c, float & x1, float & x2);

#define MS_INIT 100; //g/m2
#define MS_COUPE 125; //g/m2

// ============================================================================
// cParcelle : constructeur avec mise a jour des caracteristiques invariantes
// ============================================================================
cParcelle::cParcelle (string nom, float surface, int ru, 
          CARACTERISTIQUE_PARCELLE caracteristique, 
          TYPE_SOL type_sol, 
          ELOIGNEMENT eloignement,
          ESPECE_HERBE espece)
{
Nom = nom;
S = surface;
RU = ru;
Caracteristique = caracteristique;
TypeSol = type_sol;
Eloignement = eloignement;
Espece = espece;

if (Espece == DACTYLE)
  {
  SPs_max = 800.0;
  SPs_min = 600.0;
  SPd = 90;
  SPv = 1.0;
  }
else
  if (Espece == RAY_GRASS_ANGLAIS)
    {
      SPs_max = 700.0;
      SPs_min = 500.0;
      SPd = 95;
      SPv = 1.2;
    }
  else
    if (Espece == FETUQUE_ELEVEE)
      {
	SPs_max = 900.0;
	SPs_min = 700.0;
	SPd = 85;
	SPv = 1.1;
      }
    else
      throw (new cAnomalie ("Dans la methode cParcelle::cParcelle, espece d'herbe inconnue : " + Espece) );
}

// ============================================================================
// InitElementsVariants : init en debut de simulation
// ============================================================================
void cParcelle::InitElementsVariants(void)
{
int Jdebut = Temps.NoJour(Simulateur.JourDebut, Simulateur.MoisDebut);

for (int i=0; i<NB_JOURS; i++)
  {
   MS[i] = MS_INCONNUE;
   MS80[i] = MS_INCONNUE;
   IF[i] = IF_INCONNU;
   Dmoy[i] = Dmoy_INCONNUE;
   IN[i] = IN_INCONNU;
   IH[i] = IH_INCONNU;
   SU[i] = SU_INCONNUE;
   EAUd[i] = EAUd_INCONNUE;
   q_h_o[i] = Q_H_O_INCONNUE;

   ETR[i] = ETR_INCONNUE;

   nbBouses = 0;

   // mise au point du modele
   MSc[i] = MS_INCONNUE;
   MSs[i] = MS_INCONNUE;
   MSe[i] = MS_INCONNUE;
  };

Astrate = -1;
Bstrate = -1;
Cstrate = -1;
qh = -1;
Jstrate = DATE_INCONNUE;

// initialisation du jour precedent le debut de simulation
if (Jdebut == 32)
  {//  le 1/2
   MS[Jdebut] = MS_INIT;
   if (MS[Jdebut] > 80)
     MS80[Jdebut] = MS[Jdebut] - 80.0;
   else
     MS80[Jdebut] = 0.0;
   MS_JSPs_max = MS[Jdebut];
   JSPs_max = 31;
   IF[Jdebut] = SPv  * 1.9 * pow( 0.01 * MS[Jdebut], 0.73);  // attention apres MS
   IF_JSPs_min =  IF[Jdebut];
   JSPs_min = 31;
   Dmoy[Jdebut] = Dmoy_INCONNUE;
   IN[Jdebut] = 100.0; // utile pour les parcelles evoluant sans action,
                     // pour les autres, reinitialise apres action Azote
   IH[Jdebut] = 0.0; 
   SU [Jdebut] = S;
   EAUd[Jdebut] = RU;
   q_h_o[Jdebut] = 0.0;

   ETR[Jdebut] = 0.0;

   Joter = 31;
   JfinDerniereUtilisation = 31;
   NbPaturage = 0;
   ActionOter = TYPE_OTER_COUPE;
   Jepiaison = DATE_INCONNUE; // depend du climat moyen ou courant
   Jdp = DATE_INCONNUE;
  }
else
  throw (new cAnomalie ("Dans la methode cParcelle.InitElementsVariants, pas d'initialisation prevu pour la parcelle a cette date.") );
}

// ============================================================================
// MAJJourDebutPaturage : mise a jour de Jdp avant de faire evoluer le troupeau
// car Jdp utilise dans CalculerIntegraleDstrate et 
// ResoudreEquationAvecIntegraleDstrate : methodes appelees
// pour faire evoluer le troupeau
// ============================================================================
void cParcelle::MAJJourDebutPaturage(void)
{
if (Jdp == DATE_INCONNUE)
  {
    Jdp = Temps.J;
    NbPaturage = NbPaturage + 1;
    if ( (Jstrate == DATE_INCONNUE)  || 
	 (Jdp - Jstrate > 15) ) 
      {
	Jstrate = Temps.J;
	Astrate = 29.25 / q_h_o[Jdp];
	Bstrate = Dmoy[Jdp] - 13;
	Cstrate = Dmoy[Jdp] + 6.5;
	qh = (2.0/3.0) * q_h_o[Jdp];
      };
  };
}


// ============================================================================
// Evoluer : evoluer d un pas de temps
// ============================================================================
void cParcelle::Evoluer(void)
{
bool paturage, ensilage, fauche, azote;
unsigned int i;
float q_n;
DUREE_PATURAGE duree_paturage;

// recherche des actions s appliquant a la parcelle
paturage = false;
if ( SBExecutant.lActionsPaturageASimuler.size() == 1)
  if ( SBExecutant.lActionsPaturageASimuler[0]->Parcelle == this )
    {
      paturage = true;
      duree_paturage =  SBExecutant.lActionsPaturageASimuler[0]->DureePaturage;
    };

ensilage = false;
for (i=0; i<SBExecutant.lActionsEnsilageASimuler.size(); i++)
    if ( SBExecutant.lActionsEnsilageASimuler[i]->Parcelle == this )
      ensilage = true;

fauche = false;
for (i=0; i<SBExecutant.lActionsFaucheASimuler.size(); i++)
    if ( SBExecutant.lActionsFaucheASimuler[i]->Parcelle == this )
      fauche = true;

azote = false;
for (i=0; i<SBExecutant.lActionsAzoteASimuler.size(); i++)
    if ( SBExecutant.lActionsAzoteASimuler[i]->Parcelle == this )
    {
      azote = true;
      q_n = SBExecutant.lActionsAzoteASimuler[i]->q_N;
    };

// verification qu il n y ait pas de combinaisons impossibles (excepte pour complement mais)
if ( (paturage && ensilage) ||
     (paturage && fauche) ||
     (ensilage && fauche) )
      throw (new cAnomalie ("Dans la methode cParcelle.Evoluer, combinaison impossible d actions a simuler un jour.") );

// demande d evolution des variables d etat en tenant compte 
// de l ordre execution voulu
if (paturage)
  {
  if (azote)
    IN_Evoluer_Azote(Climat, q_n);
  else 
    IN_Evoluer_Naturel (Climat);
  IH_Evoluer(Climat);
  EAUd_Evoluer(Climat);
  SU_Evoluer_Paturage(duree_paturage);
  MS_Evoluer_Paturage(Climat);
  q_h_o_Evoluer();
  IF_Evoluer_Paturage_Coupe();
  Dmoy_Evoluer_Paturage();
  }
else
  if ( (ensilage) || (fauche) )
    {
     if (azote)
       IN_Evoluer_Azote(Climat, q_n);
     else 
       IN_Evoluer_Naturel (Climat);
     IH_Evoluer(Climat);
     EAUd_Evoluer(Climat);
     SU_Evoluer_Naturel(Climat);
     MS_Evoluer_Coupe();
     q_h_o_Evoluer();
     IF_Evoluer_Paturage_Coupe();
     Dmoy_Evoluer_Coupe();
    }
  else
    {
     if (azote)
       IN_Evoluer_Azote(Climat, q_n);
     else 
       IN_Evoluer_Naturel (Climat);
     IH_Evoluer(Climat);
     EAUd_Evoluer(Climat);
     SU_Evoluer_Naturel(Climat);
     MS_Evoluer_Naturel (Climat);
     q_h_o_Evoluer();
     IF_Evoluer_Naturel (Climat);
     Dmoy_Evoluer_Naturel ();
    }

// mise a jour des dates reperes
if ( (paturage) || (ensilage) || (fauche) )
   Joter = Temps.J;
if  (paturage)
  ActionOter = TYPE_OTER_PATURAGE;
if ( (ensilage) || (fauche) )
  {
  ActionOter = TYPE_OTER_COUPE;
  JfinDerniereUtilisation = Temps.J;
  };
if  (!paturage) 
  {
   Jdp = DATE_INCONNUE;
   MSe[Temps.J] = 0;
   if ( (Joter == Temps.J-1) && (ActionOter == TYPE_OTER_PATURAGE) )
     JfinDerniereUtilisation = Temps.J - 1;
  };

if ( JSPs_max < Joter)
  {
    JSPs_max = Joter;
    MS_JSPs_max = MS[Joter+1];
  }
if ( JSPs_min < Joter)
  {
    JSPs_min = Joter;
    IF_JSPs_min = IF[Joter+1];
  }

VerifierValeurs();
}


// ============================================================================
// MS_Evoluer_Naturel
// ============================================================================
void cParcelle::MS_Evoluer_Naturel(cClimat & climat)
{

// calcul de MSc --------------------------------------------------------------
MSc[Temps.J] = CalculerMSc(climat, CALCULER_EI);

// calcul de MSs --------------------------------------------------------------
float alpha = 0.85;

int jsps = CalculerJSPs_max(climat, Temps.J);
if (jsps != DATE_INCONNUE)
  {
    MS_JSPs_max = MS[Temps.J - 1];
    JSPs_max = Temps.J - 1;
  };
MSs[Temps.J] = alpha * MS_JSPs_max * min ( climat.Tbase0[Temps.J], 18.0 ) / SPs_max;

// calcul de MS --------------------------------------------------------------
MS[Temps.J+1] = MS[Temps.J] + MSc[Temps.J] - MSs[Temps.J];
if (MS[Temps.J+1] > 80)
  MS80[Temps.J+1] = MS[Temps.J+1] - 80.0;
else 
  MS80[Temps.J+1] = 0;
}

// ============================================================================
// CalculerJSPs_max
// ============================================================================
int cParcelle::CalculerJSPs_max(cClimat & climat, int j_courant)
{
int jsps = DATE_INCONNUE;

float somT = 0.0;
for (int j=j_courant; j>JSPs_max; j--)
  {
  somT += min ( climat.Tbase0[j], 18.0);
  if (somT >= SPs_max)
    {
    jsps = j;
    j = 0;
    };
  };
return jsps;
}


// ============================================================================
// CalculerMSc
// ============================================================================
float cParcelle::CalculerMSc(cClimat & climat, float valeur)
{
float mu;
if ( Temps.J < Temps.NoJour(1,5) )
   mu = 0.57;
else
   mu = 0.52;

float ei;
if (valeur == CALCULER_EI)
   ei = 0.95 * (1.0 - exp(- mu * IF[Temps.J]) );
else 
   ei = valeur;
float PAR = 0.48 * ei * climat.Rg[Temps.J];

float ap;
ap = - (0.6/180)*Temps.J + 2.5 + 32 * 0.6 /180;
float ad;
MAJJepiaison(climat);
if ( (Temps.J < Jepiaison) && (Joter == 31) )
  ad = 1.1;
else 
  ad = 1;

float aT;
float t  = climat.T[Temps.J];
if (t < 0)
  aT = 0.02;
else
  aT = 0.037 + 0.09 * min(18,t) - 0.0022 * pow (min(18,t), 2);

float f = aT * 0.01 * IN[Temps.J] * IH[Temps.J] * ad * ap * PAR ;

return ( f);
}


// ============================================================================
// MAJJepiaison
// il s'agit en fait du jour de debut de montaison
// ============================================================================
void cParcelle::MAJJepiaison(cClimat & climat)
{
if (Jepiaison == DATE_INCONNUE)
  {
    float SomT = 0.0;
    for (int j=32; j<365; j++)
      {
	SomT += climat.Tbase0[j];
	if (SomT >= 600)
	  {
	    Jepiaison = j;
	    j = 365;
	  };
      };
  };
}


// ============================================================================
// MS_Evoluer_Paturage
// ============================================================================
void cParcelle::MS_Evoluer_Paturage(cClimat & climat)
{
float qhe = SBExecutant.lActionsPaturageASimuler[0]->q_h_e;

// verification que les valeurs necessaires sont connues
if ( qhe == QUANTITE_INCONNUE )
  throw (new cAnomalie ("Dans la methode cParcelle.MS_Evoluer_Paturage, la quantite d'herbe ingeree le jour j doit etre connue pour calculer MS(j+1)") );

// calcul de MSc --------------------------------------------------------------
MSc[Temps.J] = CalculerMSc(climat, 0.95);

// calcul de MSs --------------------------------------------------------------
MSs[Temps.J] = MSs[Jdp - 1];

// calcul de MSe --------------------------------------------------------------
MSe[Temps.J] = qhe * Troupeau.n * 0.1 / SU[Temps.J];

// calcul de MS ---------------------------------------------------------------
MS[Temps.J+1] = MS[Temps.J] + MSc[Temps.J] - MSs[Temps.J] - MSe[Temps.J];
if (MS[Temps.J+1] > 80)
  MS80[Temps.J+1] = MS[Temps.J+1] - 80;
else 
  MS80[Temps.J+1] = 0;
}

// ============================================================================
// MS_Evoluer_Coupe
// ============================================================================
void cParcelle::MS_Evoluer_Coupe(void)
{
MS[Temps.J+1] = MS_COUPE;
MSc[Temps.J] = 0;
MSs[Temps.J] = 0;
MS80[Temps.J+1] = 0;
}


// ============================================================================
// IF_Evoluer_Naturel
// ============================================================================
void cParcelle::IF_Evoluer_Naturel(cClimat & climat)
{
float IFc = 11 * 0.000001 * (IN[Temps.J] - 20) * pow(min(climat.Tbase0[Temps.J],18),2) * IH[Temps.J];

int jsps = CalculerJSPs_min(climat, Temps.J);
if (jsps != DATE_INCONNUE)
  {
    IF_JSPs_min = IF[Temps.J - 1];
    JSPs_min = Temps.J - 1;
  };

float IFs = IF_JSPs_min * min (18, climat.Tbase0[Temps.J]) / SPs_min;

IF[Temps.J+1] = max(0, min(12, IF[Temps.J] + IFc - IFs));
}

// ============================================================================
// CalculerJSPs_min
// ============================================================================
int cParcelle::CalculerJSPs_min(cClimat & climat, int j_courant)
{
int jsps = DATE_INCONNUE;

float somT = 0.0;
for (int j=j_courant; j>JSPs_min; j--)
  {
  somT += min ( climat.Tbase0[j], 18.0);
  if (somT >= SPs_min)
    {
    jsps = j;
    j = 0;
    };
  };
return jsps;
}


// ============================================================================
// IF_Evoluer_Paturage_Coupe
// ============================================================================
void cParcelle::IF_Evoluer_Paturage_Coupe(void)
{
// verification que les valeurs necessaires sont connues
if ( (MS[Temps.J+1] == MS_INCONNUE)  )
  throw (new cAnomalie ("Dans la methode cParcelle.IF_Evoluer_Paturage_Coupe, la valeur de MS(j+1) doit etre connue pour calculer IF(j+1)") );

IF[Temps.J+1] = max (0, SPv * 1.9 * pow(0.01 * MS[Temps.J] , 0.73) * MS[Temps.J+1]/MS[Temps.J]);
}


// ============================================================================
// Dmoy_Evoluer_Naturel
// ============================================================================
void cParcelle::Dmoy_Evoluer_Naturel(void)
{
float SomMSc = CalculerSommeMSc(Joter+1);
float g = 0.0588 * Temps.J -0.5280;

if ( (Temps.J < 30) || (Temps.J > 220))
    throw (new cAnomalie ("Dans la methode cParcelle.Dmoy_Evoluer_Naturel, le calcul de g(j) n'est valide que du jour 30 ou jour 220") );

float Dmoy_f = max(40,(SPd - g) * ( 0.9 + 0.001 * IN[Temps.J] ) - 0.04 * SomMSc);

if (ActionOter == TYPE_OTER_COUPE)
  Dmoy[Temps.J+1] = Dmoy_f;
else
  if (ActionOter == TYPE_OTER_PATURAGE)
    {
    float SomMSs = CalculerSommeMSs(Joter+1);
    Dmoy[Temps.J+1] = max(40,( Dmoy_f*SomMSc + Dmoy[Joter+1]*max(0,MS[Joter+1] - SomMSs) ) /
	( SomMSc + max(0,MS[Joter+1] - SomMSs) ));
    };
}

// ============================================================================
// CalculerSommeMSc
// ============================================================================
float cParcelle::CalculerSommeMSc(int jdebut)
{
float somMSc = 0;
for (int j = jdebut; j<=Temps.J; j++)
  {
  if (MSc[j] == MS_INCONNUE)
    throw (new cAnomalie ("Dans la methode cParcelle.CalculerSommeMSc, une valeur de MSc demandee est inconnue") );
  somMSc += MSc[j];
  };
return somMSc;
}


// ============================================================================
// CalculerSommeMSs
// ============================================================================
float cParcelle::CalculerSommeMSs(int jdebut)
{
float somMSs = 0.0;
for (int j = jdebut; j<=Temps.J; j++)
  {
  if (MSs[j] == MS_INCONNUE)
    throw (new cAnomalie ("Dans la methode cParcelle.CalculerSommeMSs, une valeur de MSs demandee est inconnue") );
  somMSs += MSs[j];
  };
return somMSs;
}


// ============================================================================
// Dmoy_Evoluer_Paturage
// ============================================================================
void cParcelle::Dmoy_Evoluer_Paturage(void)
{
// verification que les valeurs necessaires sont connues
if ( q_h_o[Temps.J+1] == Q_H_O_INCONNUE  )
  throw (new cAnomalie ("Dans la methode cParcelle.Dmoy_Evoluer_Paturage, la quantite d'herbe offerte le jour j+1 doit etre connue pour calculer Dmoy(j+1)") );

if (q_h_o[Temps.J+1] > 1)
  Dmoy[Temps.J+1] = max(40, CalculerIntegraleDstrate(0,q_h_o[Temps.J+1]) / q_h_o[Temps.J+1]);
else
  Dmoy[Temps.J+1] = Dmoy[Temps.J];
}


// ============================================================================
// Dmoy_Evoluer_Coupe
// ============================================================================
void cParcelle::Dmoy_Evoluer_Coupe(void)
{
Dmoy[Temps.J+1] = Dmoy_INCONNUE;
}


// ============================================================================
// IN_Evoluer_Naturel
// ============================================================================
void cParcelle::IN_Evoluer_Naturel(cClimat & climat)
{
int JN1 = SDPlanificateur.Jour1erApportAzote;
int JN2 = SDPlanificateur.Jour2emeApportAzote;
int JN3 = SDPlanificateur.Jour3emeApportAzote;
int Jfin = Temps.NoJour(Simulateur.JourFin, Simulateur.MoisFin);

if (Temps.J < JN2)
  IN[Temps.J+1] = ( (IN[JN2] - IN[JN1]) / (JN2 - JN1) ) * (Temps.J+1 -JN1) + IN[JN1];
else
  if (Temps.J < JN3)
    IN[Temps.J+1] = ( (IN[JN3] - IN[JN2]) / (JN3 - JN2) ) * (Temps.J+1 -JN2) + IN[JN2];
  else
    IN[Temps.J+1] = ( (IN[Jfin] - IN[JN3]) / (Jfin - JN3) ) * (Temps.J+1 -JN3) + IN[JN3];
}


// ============================================================================
// IN_Evoluer_Azote
// ============================================================================
void cParcelle::IN_Evoluer_Azote(cClimat & climat, float q)
{
int JN1 = SDPlanificateur.Jour1erApportAzote;
int JN2 = SDPlanificateur.Jour2emeApportAzote;
int JN3 = SDPlanificateur.Jour3emeApportAzote;
int Jfin = Temps.NoJour(Simulateur.JourFin, Simulateur.MoisFin);

if (Temps.J == JN1)
  {
  if (q == 3)
    {IN[JN1] = 95; IN[JN2] = 90;}
  else
    if (q == 2)
      {IN[JN1] = 90; IN[JN2] = 85;}
    else
      if (q == 1)
	{IN[JN1] = 80; IN[JN2] = 75;}
      else
	throw (new cAnomalie ("Dans la methode cParcelle.IN_Evoluer_Azote, pour le 1er apport d'azote la quantite apportee n'est pas permise") );
    }
else
  if (Temps.J == JN2)
    {
      if (q == 3)
	{IN[JN3] = 85;}
      else
	if (q == 2)
	  {IN[JN3] = 75;}
	else
	  if (q == 1)
	    {IN[JN3] = 65;}
	  else   
	    throw (new cAnomalie ("Dans la methode cParcelle.IN_Evoluer_Azote, pour le 2eme apport d'azote la quantite apportee n'est pas permise") );
    }
  else
      if (Temps.J == JN3)
	{
	  if (q == 3)
	    {IN[Jfin] = 85;}
	  else
	    if (q == 2)
	      {IN[Jfin] = 70;}
	    else
	      if (q == 1)
		{IN[Jfin] = 60;}
	      else   
		throw (new cAnomalie ("Dans la methode cParcelle.IN_Evoluer_Azote, pour le 3eme apport d'azote la quantite apportee n'est pas permise") );
	}
      else 
	throw (new cAnomalie ("Dans la methode cParcelle.IN_Evoluer_Azote, le jour courant n'est pas un jour prevu pour apporter de l'azote") );

IN_Evoluer_Naturel(climat);
}



// ============================================================================
// SU_Evoluer_Naturel
// ============================================================================
void cParcelle::SU_Evoluer_Naturel(cClimat & climat)
{
SU[Temps.J+1] = SU[Temps.J];
}


// ============================================================================
// SU_Evoluer_Paturage
// ============================================================================
void cParcelle::SU_Evoluer_Paturage(DUREE_PATURAGE d)
{
// calcul nb de bouses
if (d == DUREE_PATURAGE_JOUR)
  nbBouses += 10;
else
  nbBouses += 20;

SU[Temps.J+1] = S - Troupeau.n * 0.07 * 0.0001 * nbBouses;
}


// ============================================================================
// IH_Evoluer
// ============================================================================
void cParcelle::IH_Evoluer(cClimat & climat)
{
// calcul ETR -----------------------------------------------------------------
ETR[Temps.J] = min (climat.ETP[Temps.J], EAUd[Temps.J]);

// calcul IH ------------------------------------------------------------------
if (climat.ETP[Temps.J] != 0)
  IH[Temps.J+1]= ETR[Temps.J] / climat.ETP[Temps.J];
else
  IH[Temps.J+1]= 1;
}


// ============================================================================
// EAUd_Evoluer
// ============================================================================
void cParcelle::EAUd_Evoluer(cClimat & climat)
{
EAUd[Temps.J+1] = min ( climat.Pluie[Temps.J] + EAUd[Temps.J] - ETR[Temps.J], RU );
}


// ============================================================================
// q_h_o_Evoluer 
// ============================================================================
void cParcelle::q_h_o_Evoluer (void)
{
// verification que les valeurs necessaires sont connues
if ( (MS80[Temps.J+1] == MS_INCONNUE) ||
     (SU[Temps.J+1] == SU_INCONNUE)  )
  throw (new cAnomalie ("Dans la methode cParcelle.q_h_o_Evoluer, MS80(j+1) et SU(j+1) doivent etre connues pour calculer q_h_o(j+1)") );

q_h_o[Temps.J+1] = ( 10.0 * MS80[Temps.J+1] * SU[Temps.J+1]) / Troupeau.n ;
}


// ============================================================================
// CalculerIntegraleDstrate : retourne la valeur de l'int�grale :
//    q2
//    / Dstrate(q) dq
//   q1
// ============================================================================
float cParcelle::CalculerIntegraleDstrate (float q1, float q2)
{
if (q2 <= qh)
  return CalculerIntegralePolynome1erDegre(Astrate, Bstrate, q1, q2);
else
  return ( CalculerIntegralePolynome1erDegre(Astrate, Bstrate, q1, qh)
            + CalculerIntegralePolynome1erDegre(0.0, Cstrate, qh, q2) );
}


// ============================================================================
// ResoudreEquationAvecIntegraleDstrate : retourne true si x contient la solution,
// retourne false si pas de solution dans [0, q2]
//    q2
//    / (V1 + V2 * Dstrate(q)) dq  = V
//   x
// ============================================================================
bool cParcelle::ResoudreEquationAvecIntegraleDstrate (float q2, float V, float V1, float V2, float & x)
{
float x1, x2;

float A = V2 * Astrate;
float B = V2 * Bstrate + V1;
float C = V2 * Cstrate + V1;

if (q2 <= qh)
  switch( ResoudreEquation2ndDegre( (- A/2.0), (- B),
            ( (A/2.0) * pow(q2,2) + B * q2 - V), x1, x2) )
    {
    case 2: if ( (x1>=0) && (x1<=q2) )
               if ( (x2>=0) && (x2<=q2) ) 
                   throw (new cAnomalie ("Dans la m�thode cParcelle.ResoudreEquationAvecIntegraleDstrate, cas q2 <= qh : 2 solutions trouv�es dans l'intervalle") );
               else
                   {x = x1; return true;}
            else
               if ( (x2>=0) && (x2<=q2) )
                   {x = x2; return true;}
               else
                   {x = -1; return false;};

    case 1: if ( (x1>=0) && (x1<=q2) )
               {x = x1; return true;}
            else
               {x = -1; return false;};

    case 0: {x = -1; return false;};

    default : throw (new cAnomalie ("Dans la m�thode cParcelle.ResoudreEquationAvecIntegraleDstrate, cas q2 <= qh : erreur") );
    }

 else  // q2 > qh
   {
   // 1ere hypothese : x > qh
   x1 = q2 - ( V/C );
   if ( (x1>qh) && (x1<= q2) )
     {x = x1; return true;}
   else // 2eme hypothese : x <= qh
     switch( ResoudreEquation2ndDegre( (- A/2.0), (- B),
            ( (A/2.0) * pow(qh,2) + B * qh + C * (q2-qh) - V), x1, x2) )
       {
       case 2 : if ( (x1>=0) && (x1<=qh) )
                   if ( (x2>=0) && (x2<=qh) ) 
                      throw (new cAnomalie ("Dans la m�thode cParcelle.ResoudreEquationAvecIntegraleDstrate, cas q2 > qh : 2 solutions trouv�es dans l'intervalle") );
                   else
                      {x = x1; return true;}
                else
                   if ( (x2>=0) && (x2<=qh) )
                      {x = x2; return true;}
                   else
                      {x = -1; return false;};

       case 1 : if ( (x1>=0) && (x1<=qh) )
                   {x = x1; return true;}
                else
                   {x = -1; return false;};

       case 0 : {x = -1; return false;};

       default : throw (new cAnomalie ("Dans la m�thode cParcelle.ResoudreEquationAvecIntegraleDstrate, cas q2 > qh : erreur") );
      };
   };
}

// ============================================================================
// VerifierValeurs : verifie les valeurs du jour
// ============================================================================
void cParcelle::VerifierValeurs (void)
{
if ( (MS[Temps.J] < 0) || (MS[Temps.J] > 1500))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, MS est hors intervalle des valeurs permises") );

if ( (MSc[Temps.J] < 0) || (MSc[Temps.J] > 50))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, MSc est hors intervalle des valeurs permises") );

if ( (MSs[Temps.J] < 0) || (MSs[Temps.J] > 50))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, MSs est hors intervalle des valeurs permises") );

if ( (MS80[Temps.J] < 0) || (MS80[Temps.J] > 1420))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, MS80 est hors intervalle des valeurs permises") );

if ( (IF[Temps.J] < 0) || (IF[Temps.J] > 15))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, IF est hors intervalle des valeurs permises") );

if ( ((Dmoy[Temps.J] < 40) || (Dmoy[Temps.J] > 100)) 
     && (Dmoy[Temps.J] != Dmoy_INCONNUE))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, Dmoy est hors intervalle des valeurs permises") );

if ( (IN[Temps.J] < 0) || (IN[Temps.J] > 100))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, IN est hors intervalle des valeurs permises") );

if ( (IH[Temps.J] < 0) || (IH[Temps.J] > 1))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, IH est hors intervalle des valeurs permises") );

if ( (EAUd[Temps.J] < 0) || (EAUd[Temps.J] > 250))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, EAUd est hors intervalle des valeurs permises") );

if ( (q_h_o[Temps.J] < 0) || (q_h_o[Temps.J] > 2000))
  throw (new cAnomalie ("Dans la m�thode cParcelle.VerifierValeurs, q_h_o est hors intervalle des valeurs permises") );
}

