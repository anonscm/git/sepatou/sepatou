// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cRandom.h
contenu : d�finition de la classe cRandom : g�n�rateur de nombre al�atoire
          Le g�n�rateur est tir� de 'NUMERICAL RECIPES in C: The Art of 
          Scientific Computing' de W.H. Press,
          B.P. Flannery, S. A. TeuKolsky, W. T. Vetterling, 
          Cambridge University Press, 1988, page 207
******************************************************************************/
#ifndef _CRANDOM_H
#define _CRANDOM_H


class cRandom
{
 public:
  void Init(unsigned int seed);
  double Generate01(void);
  bool Initialise;

 private:
  double y;
  double v[98];  // le nombre exact 98 n'est pas important
  double MaxRand;
};


#endif
