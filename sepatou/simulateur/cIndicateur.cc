// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cIndicateur.cc
contenu : definition des methodes de la classe cIndicateur
******************************************************************************/

#include <string>

#include "cIndicateur.h"

#include "pat.h"
#include "cTemps.h"

extern cTemps Temps;

// ============================================================================
// cIndicateur : initialisation des attributs
// ============================================================================
cIndicateur::cIndicateur (bool (*caracteristique)(), string nom)
{
CARACTERISTIQUE = caracteristique;
Nom = nom;
jour_valeur = DATE_INCONNUE;
_valeur = DATE_INCONNUE;
}

// ============================================================================
// InitElementsVariants : initialisation des elts variants
// ============================================================================
void cIndicateur::InitElementsVariants(void)
{
jour_valeur = DATE_INCONNUE;
_valeur = DATE_INCONNUE;
}


// ============================================================================
// MAJValeur : mise a jour de Valeur
// ============================================================================
int cIndicateur::Valeur (void)
{
if (jour_valeur != Temps.J)
  {
    jour_valeur = Temps.J;
    
    if (CARACTERISTIQUE())
      {
	if (_valeur == DATE_INCONNUE)
	  _valeur = Temps.J;
      }
    else
      {
	if (_valeur != DATE_INCONNUE)
	  _valeur = DATE_INCONNUE;
      }
  }

return _valeur;
}
