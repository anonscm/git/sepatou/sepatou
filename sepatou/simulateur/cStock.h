// Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

/******************************************************************************
fichier : cStock.h
contenu : definition des classes cStockConcentre, cStockMais, cStockFoin
          cStockAzote
******************************************************************************/
#ifndef _CSTOCK_H
#define _CSTOCK_H


#include "pat.h"



// ============================================================================
// cStockConcentre : representation d un stock de concentre
// ============================================================================
class cStockConcentre
{
public:  //--------------------------------------------------------------------
void InitElementsInvariants(float q_Kg);  
void InitElementsVariants(void);    // initialisation en debut de simulation
float Etat(int j);                  // etat du stock en Kg le jour j

void Evoluer(void);                 // evoluer d un jour

private: //-------------------------------------------------------------------
float Quantite[NB_JOURS];       // quantite du stock en Kg un jour donne
float q_init_Kg;                    // etat du stock en debut de simulation
};



// ============================================================================
// cStockMais : representation d un stock de mais
// ============================================================================
class cStockMais
{
public:  //--------------------------------------------------------------------
void InitElementsInvariants(float q_Kg);
void InitElementsVariants(void);
float Etat(int j);                  // etat du stock en Kg le jour j

void Evoluer(void);                 // evoluer d un jour

private: //-------------------------------------------------------------------
float Quantite[NB_JOURS];           // quantite du stock en Kg un jour donne
float q_init;                       // quantite en debut de simulation
};




// ============================================================================
// cStockFoin : representation d un stock de foin
// ============================================================================
class cStockFoin
{
public:  //--------------------------------------------------------------------
QUALITE_FOIN Qualite;                     // qualite du foin

void InitElementsInvariants(float q_Kg, QUALITE_FOIN qualite);
void InitElementsVariants(void);

float Etat(int j);                  // etat du stock en Kg le jour j

void Evoluer(void);                 // evoluer d un jour

private: //-------------------------------------------------------------------
float Quantite[NB_JOURS];        // quantite du stock en Kg un jour donne
float q_init_Kg;                     // quantite en debut de simulation
};



// ============================================================================
// cStockAzote : representation d un stock d azote
// ============================================================================
class cStockAzote
{
public:  //--------------------------------------------------------------------
void InitElementsInvariants(float q_Kg);  
void InitElementsVariants();    // initialisation en debut de simulation

float Etat(int j);                  // etat du stock en Kg le jour j

void Evoluer(void);                 // evoluer d un jour

private: //-------------------------------------------------------------------
float Quantite[NB_JOURS];        // quantite du stock en Kg un jour donne
float q_init_Kg;                     // quantite en debut de simulation
};

#endif
