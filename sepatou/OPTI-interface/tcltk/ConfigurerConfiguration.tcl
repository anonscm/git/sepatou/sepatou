# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerConfiguration.tcl  
# Contenu : Chargement d'un configuration OPTI-SEPATOU ou SEPATOU
#           comme configuration courante
######################################################################

######################################################################
# Procedure   : Desc_Configuration
# Description : cr�e la fenetre de choix de la configuration � charger
######################################################################
proc Desc_Configuration {dir_configurations  type} {

    set w .wm_ConfigurerConfiguration

    if {[winfo exists $w] == 0} {
	toplevel $w
	wm title $w "OPTI-SEPATOU : Chargement d'une configuration courante"
	
	label $w.l_configuration -text "Configurations $type"
	pack $w.l_configuration -pady 20


	### Zone de selection ##############################################
	frame $w.f_topw -borderwidth 10
	pack $w.f_topw -padx 20 -pady 20 -side top -expand 1

	#Creation de la Scrolled List
	ScrolledListbox $w.f_topw.scrlst_list  -width 50 -height 15 
	set list [recherche_rep $dir_configurations]
	set nb [llength $list]
	for  {set i 0} {$i < $nb} {incr i} {
	    set val [lindex $list $i]
	    $w.f_topw.scrlst_list.lst_list insert end $val
	}
	pack $w.f_topw.scrlst_list -padx 4 -side left -anchor n -expand 1
	

	###Zone de commandes des boutons ok, annuler #########################
	frame $w.f_bottom -borderwidth 3
	pack $w.f_bottom -side bottom -fill x -pady 5

	button $w.f_bottom.b_ok -text Ok \
	    -command "Ok_ChargerConfiguration $w $dir_configurations $type"
	button $w.f_bottom.b_cancel  -text Annuler -command "destroy $w"
	pack $w.f_bottom.b_ok $w.f_bottom.b_cancel -side left -expand 1 
	### Separateur
	frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $w.f_sep -side bottom -fill x -pady 5	
    }
}


#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
proc Ok_ChargerConfiguration {w dir_configurations type} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_EXPLOITATION_TXT
    global FIC_STRATEGIE_TXT
    global FIC_FOINTERPRETATION_LNU
    global FIC_INDICATEURS_LNU
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU
    global FIC_SIMULATEUR_TXT
    global FIC_CRITERE_LNU
    global FIC_ALGORITHME_TXT
    global FIC_SORTIES_OPTIMISATEUR_TXT
    global SEPARATEUR_LNU
    global CMD_COPY
    global MODIF_STRATEGIE
    
    set list $w.f_topw.scrlst_list.lst_list    
    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue OPTI-SEPATOU error "S�lectionnez une configuration"
    } else {
	set rep [$list get $i]
	exec $CMD_COPY $dir_configurations$rep/$FIC_EXPLOITATION_TXT ${DIR_OPTI-TEMPORAIRE}
	exec $CMD_COPY $dir_configurations$rep/$FIC_STRATEGIE_TXT ${DIR_OPTI-TEMPORAIRE}
	exec $CMD_COPY $dir_configurations$rep/$FIC_FOINTERPRETATION_LNU ${DIR_OPTI-TEMPORAIRE}
	exec $CMD_COPY $dir_configurations$rep/$FIC_INDICATEURS_LNU ${DIR_OPTI-TEMPORAIRE}
	exec $CMD_COPY $dir_configurations$rep/$FIC_REGLESPLANIFICATION_LNU ${DIR_OPTI-TEMPORAIRE}
	exec $CMD_COPY $dir_configurations$rep/$FIC_REGLESOPERATOIRES_LNU ${DIR_OPTI-TEMPORAIRE}
	exec $CMD_COPY $dir_configurations$rep/$FIC_SIMULATEUR_TXT ${DIR_OPTI-TEMPORAIRE}

	if {[string compare $type "OPTI-SEPATOU"] == 0} {
	    exec $CMD_COPY $dir_configurations$rep/$FIC_CRITERE_LNU ${DIR_OPTI-TEMPORAIRE}
	    exec $CMD_COPY $dir_configurations$rep/$FIC_ALGORITHME_TXT ${DIR_OPTI-TEMPORAIRE}
	    exec $CMD_COPY $dir_configurations$rep/$FIC_SORTIES_OPTIMISATEUR_TXT ${DIR_OPTI-TEMPORAIRE}
	} else {
	    # cr�ation de nouveaux fichiers
	    set file [open ${DIR_OPTI-TEMPORAIRE}$FIC_ALGORITHME_TXT w]
	    puts $file "Algorithme = Systematique
Nom	 Min	 Max	 Type	 Pas	 Init	 c   	 
Teta1	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999	
Teta2	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999	
Teta3	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999	
Teta4	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999	
Teta5	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999
Teta6	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999
Teta7	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999
Teta8	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999
Teta9	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999
Teta10	 -9999	 -9999	 -9999	 -9999	 -9999	 -9999
A = -9999 
B = -9999
NbIterations = -9999
Ecart = -9999
"
	    close $file
	    set file [open ${DIR_OPTI-TEMPORAIRE}$FIC_CRITERE_LNU w]
	    puts $file "$SEPARATEUR_LNU
# #
$SEPARATEUR_LNU
FONCTION : Critere
CODE : 
"
	    close $file

	    set file [open ${DIR_OPTI-TEMPORAIRE}$FIC_SORTIES_OPTIMISATEUR_TXT w]
	    puts $file "Critere"
	    close $file
	}
	set MODIF_STRATEGIE 1
	destroy $w
   }
}

