# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : VisualiserOptimum.tcl  
# Contenu : Visualisation de l'optimum
######################################################################

######################################################################
# Procedure   : VisualiserOptimum
#             : Pour l'algorithme Partition2p, l'optimum d'un parametre
#               n'est pas une valeur mais un intervalle
######################################################################
proc VisualiserOptimum {nom_algorithme} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_OPTIMUM
    global PARAMETRES_A_OPTIMISER
    global VIDE

    set w .wm_VisualiserOptimum

    if {[winfo exists $w] == 0} {

	# lecture des valeurs
	set file [open ${DIR_OPTI-TEMPORAIRE}$FIC_OPTIMUM r]
	gets $file line 
	if {[string compare $nom_algorithme "Partition2p"] != 0} {
	    scan $line "%f %f %f %f %f %f %f %f %f %f %f" \
		teta1 teta2 teta3 teta4 teta5 teta6 teta7 teta8 teta9 teta10 critere
	    set i 1
	    foreach p $PARAMETRES_A_OPTIMISER {
		if {[set teta$i] == $VIDE} { set teta$i "" }
		incr i
	    }
	} else {
	    scan $line "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f" \
		tetam1 tetaM1 tetam2 tetaM2 tetam3 tetaM3 tetam4 tetaM4 tetam5 tetaM5 \
		tetam6 tetaM6 tetam7 tetaM7 tetam8 tetaM8 tetam9 tetaM9 tetam10 tetaM10
 
	    set i 1
	    foreach p $PARAMETRES_A_OPTIMISER {
		if {[set tetam$i] == $VIDE} { set tetam$i "" }
		if {[set tetaM$i] == $VIDE} { set tetaM$i "" }
		set teta$i "[set tetam$i] - [set tetaM$i]"
		incr i
	    }	    
	    # le scan ne marche plus apr�s 20 valeurs
	    set critere [lindex $line 20]
	}
	close $file

	# cr�ation de la fenetre
	toplevel $w
	wm title $w "OPTI-SEPATOU : Visualisation de l'optimum"

 	label $w.l_titre -text "OPTIMUM"
	pack $w.l_titre -pady 20

	tixGrid $w.g_para \
	    -width 2 -height [expr 1 + [llength $PARAMETRES_A_OPTIMISER]]
	$w.g_para config -formatcmd "EditGrid_format $w.g_para"
        $w.g_para size column 0 -size 10char
        $w.g_para size column 1 -size 20char

	$w.g_para set 0 0 -text " Parametre"
	$w.g_para set 1 0 -text "  Valeur"

	set i 1
	foreach p $PARAMETRES_A_OPTIMISER {
	    $w.g_para set 0 $i \
		-text [lindex $PARAMETRES_A_OPTIMISER [expr $i - 1]]
	    $w.g_para set 1 $i -text [set teta$i]
	    incr i
	}
	pack  $w.g_para -padx 20 -pady 20

	label $w.l_critere -text "Valeur critere : $critere"
	pack $w.l_critere -pady 10 -padx 50

	### Zone de commandes des boutons fermer et imprimer
	frame $w.f_bottom -borderwidth 3
	pack $w.f_bottom -side bottom -fill x -pady 5

	#Creation des boutons
	button $w.f_bottom.b_fermer \
	    -text Fermer -command "destroy $w" 
	button $w.f_bottom.b_imp \
	    -text Imprimer -command "ImprimerOptimum $teta1 $teta2 $teta3 $teta4 $teta5 $teta6 $teta7 $teta8 $teta9 $teta10 $critere"

	pack $w.f_bottom.b_fermer $w.f_bottom.b_imp \
	    -side left -expand 1 

	### Separateur
	frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $w.f_sep -side bottom -fill x -pady 5
    }
}

######################################################################
#  Procedure   : ImprimerOptimum
######################################################################
proc ImprimerOptimum {teta1 teta2 teta3 teta4 teta5 teta6 teta7 teta8 teta9 teta10 critere} {

    global DIR_OPTI-TEMPORAIRE
    global CMD_REMOVE
    global CMD_A2PS


    # cr�ation d'un fichier
    set file [open ${DIR_OPTI-TEMPORAIRE}Optimum.imp w]

    puts $file "OPTIMUM TROUVE

Teta1 = $teta1
Teta2 = $teta2
Teta3 = $teta3
Teta4 = $teta4
Teta5 = $teta5
Teta6 = $teta6
Teta7 = $teta7
Teta8 = $teta8
Teta9 = $teta9
Teta10 = $teta10

Valeur du critere : $critere
"
    close $file

    # demande d'impression
    catch {exec $CMD_A2PS ${DIR_OPTI-TEMPORAIRE}Optimum.imp}
    exec $CMD_REMOVE ${DIR_OPTI-TEMPORAIRE}Optimum.imp

    CreerFenetreDialogue OPTI-SEPATOU info "Impression en cours"

}