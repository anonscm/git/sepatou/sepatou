# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerSortiesOptimisateur.tcl  
# Contenu : Description des sorties d�sir�es de l'optimisateur
######################################################################

######################################################################
# Procedure   : Desc_SortiesOptimisateur
# Description : cr�e la fenetre d�crivant les sorties d�sir�es
######################################################################
proc Desc_SortiesOptimisateur {} {

    set w .wm_ConfigurerSortiesOptimisateur

    if {[winfo exists $w] == 0} {
	toplevel $w
	wm title $w {OPTI-SEPATOU : Description des sorties desirees}

	# sorties possibles
	label $w.l_titre -text "Selectionnez les sorties desirees"
	pack $w.l_titre -pady 20 -padx 10

	listbox $w.lst_sorties -selectmode multiple
	$w.lst_sorties insert end Critere
	$w.lst_sorties insert end Parametres
	$w.lst_sorties insert end GradientStochastique
	$w.lst_sorties insert end Niveau
	pack $w.lst_sorties -pady 10


	### Zone de commandes des boutons ok, annuler et imprimer
	frame $w.f_bottom -borderwidth 3
	pack $w.f_bottom -side bottom -fill x -pady 5
	button $w.f_bottom.b_ok \
	    -text Ok -command "Ok_ConfigurerSortiesOptimisateur $w" 
	button $w.f_bottom.b_cancel \
	    -text Annuler -command "destroy $w"
	pack $w.f_bottom.b_ok $w.f_bottom.b_cancel \
	    -side left -expand 1 
	### Separateur
	frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $w.f_sep -side bottom -fill x -pady 5

	# Chargement des sorties d�sir�es si elles existent
	ChargerSortiesOptimisateur $w
    }
}

######################################################################
# Procedure   : Ok_ConfigurerSortiesOptimisateur
# Description : Enregistrement des sorties choisies
######################################################################
proc Ok_ConfigurerSortiesOptimisateur {w} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_SORTIES_OPTIMISATEUR_TXT

    set fichier [open ${DIR_OPTI-TEMPORAIRE}$FIC_SORTIES_OPTIMISATEUR_TXT w]
    foreach i [$w.lst_sorties curselection] {
	puts $fichier [$w.lst_sorties get $i]
    }
    close $fichier

    destroy $w
}



######################################################################
# Procedure   : ChargerSortiesOptimisateur
# Description : Lecture des sorties d�j� choisies
######################################################################
proc ChargerSortiesOptimisateur {w} {
    
    global DIR_OPTI-TEMPORAIRE
    global FIC_SORTIES_OPTIMISATEUR_TXT

    if {[catch {set file [open ${DIR_OPTI-TEMPORAIRE}$FIC_SORTIES_OPTIMISATEUR_TXT r]}] == 0} {

	while {[eof $file] != 1} {
	    gets $file line
	    if {[string compare $line "Critere"] == 0} {
		$w.lst_sorties selection set 0
	    } elseif {[string compare $line "Parametres"] == 0} {
		$w.lst_sorties selection set 1
	    } elseif {[string compare $line "GradientStochastique"] == 0} {
		$w.lst_sorties selection set 2
	    } elseif {[string compare $line "Niveau"] == 0} {
		$w.lst_sorties selection set 3
	    }
	}
    }
}
