# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : SauvegarderConfiguration.tcl  
# Contenu : Sauvegarde d'une configuration OPTI-SEPATOU ou SEPATOU
######################################################################

######################################################################
# Procedure   : SauvegarderConfiguration
######################################################################
proc SauvegarderConfiguration {type} {

    set w .wm_SauvegarderConfiguration$type

    catch {destroy $w}
    toplevel $w
    wm title $w {OPTI-SEPATOU : Sauvegarde de la configuration courante}

    label $w.l_titre -text "Sauvegarde de la configuration courante en configuration $type"
    pack $w.l_titre -pady 20 -padx 10

    frame $w.f_aff
    label $w.f_aff.l_nom -text "Nom de configuration"
    entry $w.f_aff.e_nom -width 50


    pack $w.f_aff.l_nom -side left
    pack $w.f_aff.e_nom -side right -expand 1
    pack $w.f_aff -padx 10 -pady 10 -expand 1

    
    ###Zone de commandes des boutons ok, annuler #############################
    frame $w.f_bottom -borderwidth 3
    pack $w.f_bottom -side bottom -fill x -pady 5

    #Creation des boutons
    button $w.f_bottom.b_ok -text Ok \
	-command "Ok_SauvegarderConfiguration $w $type"
    button $w.f_bottom.b_cancel \
	-text Annuler -command "destroy $w"
    pack $w.f_bottom.b_ok \
         $w.f_bottom.b_cancel -side left -expand 1 

    ### Separateur
    frame $w.f_sep \
	-width 100 -height 2 -borderwidth 1 -relief sunken
    pack $w.f_sep -side bottom -fill x -pady 5
}


#-----------------------------------------------------------
# sauvegarde de la configuration
#-----------------------------------------------------------
proc Ok_SauvegarderConfiguration {w type} {

    global CMD_COPY
    global CMD_MKDIR
    global CMD_REMOVE

    global DIR_OPTI-TEMPORAIRE
    global DIR_OPTI-CONFIGURATIONS
    global FIC_EXPLOITATION_TXT
    global FIC_STRATEGIE_TXT
    global FIC_FOINTERPRETATION_LNU
    global FIC_INDICATEURS_LNU
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU
    global FIC_SIMULATEUR_TXT
    global FIC_CRITERE_LNU
    global FIC_ALGORITHME_TXT
    global FIC_SORTIES_OPTIMISATEUR_TXT
    global PARAMETRES_A_OPTIMISER
    global FIC_OPTIMUM

    global DIR_CONFIGURATIONS
    global FIC_SORTIES_TXT

    set nom [$w.f_aff.e_nom get]

    if {[string compare $type "OPTI-SEPATOU"] == 0} {
	set dir_configurations ${DIR_OPTI-CONFIGURATIONS}
    } elseif  {[string compare $type "SEPATOU"] == 0} {
	set dir_configurations ${DIR_CONFIGURATIONS}
    }
    set existe_rep [file exist ${dir_configurations}$nom]

    set confirmation 0
    if {$existe_rep == 1} {
	set confirmation [Confirmer OPTI-SEPATOU "La configuration $nom existe dej�. Souhaitez-vous enregistrer sur cette configuration existante ?"]
    } else {
	exec $CMD_MKDIR ${dir_configurations}$nom
    }

    if {$existe_rep == 0 || $confirmation == 1} {
	# copie des fichiers communs � OPTI-SEPATOU et SEPATOU
	exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_EXPLOITATION_TXT ${dir_configurations}$nom
	exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_STRATEGIE_TXT ${dir_configurations}$nom
	exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_FOINTERPRETATION_LNU ${dir_configurations}$nom
	exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_INDICATEURS_LNU ${dir_configurations}$nom
	exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_REGLESPLANIFICATION_LNU ${dir_configurations}$nom
	exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_REGLESOPERATOIRES_LNU ${dir_configurations}$nom
	exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_SIMULATEUR_TXT ${dir_configurations}$nom
	if {[string compare $type "OPTI-SEPATOU"] == 0} {
	    exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_CRITERE_LNU ${dir_configurations}$nom
	    exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_ALGORITHME_TXT ${dir_configurations}$nom
	    exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_SORTIES_OPTIMISATEUR_TXT ${dir_configurations}$nom
	} elseif  {[string compare $type "SEPATOU"] == 0} {
	    set file [open ${DIR_OPTI-TEMPORAIRE}$FIC_SORTIES_TXT w]
	    puts $file "chronique
STATISTIQUES
FONCTIONS
INDICATEURS" 	
	    close $file
	    exec $CMD_COPY ${DIR_OPTI-TEMPORAIRE}$FIC_SORTIES_TXT ${dir_configurations}$nom
	    exec $CMD_REMOVE ${DIR_OPTI-TEMPORAIRE}$FIC_SORTIES_TXT

	    # Remplacement des Tetas par les valeurs optimum
	    if {[catch {set file [open ${DIR_OPTI-TEMPORAIRE}$FIC_OPTIMUM r]}] == 0} {
		set nom_algorithme [LireNomAlgorithme]
		gets $file line 
		if {[string compare $nom_algorithme "Partition2p"] != 0} {
		    scan $line "%f %f %f %f %f %f %f %f %f %f %f" \
			teta1 teta2 teta3 teta4 teta5 teta6 teta7 teta8 teta9 teta10 critere
		} else {
		    scan $line "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f" \
			tetam1 tetaM1 tetam2 tetaM2 tetam3 tetaM3 tetam4 tetaM4 tetam5 tetaM5 \
			tetam6 tetaM6 tetam7 tetaM7 tetam8 tetaM8 tetam9 tetaM9 tetam10 tetaM10		    
		    set i 1
		    foreach p $PARAMETRES_A_OPTIMISER {
			set teta$i [expr [set tetam$i] + ([set tetaM$i] - [set tetam$i])/2]
			incr i
		    }	    
		}
		close $file
		
		foreach fic [list $FIC_FOINTERPRETATION_LNU $FIC_INDICATEURS_LNU $FIC_REGLESPLANIFICATION_LNU $FIC_REGLESOPERATOIRES_LNU] {
		    set file [open ${dir_configurations}$nom/$fic r]
		    set contenu [read $file]
		    close $file
		    
		    set i 1
		    foreach p $PARAMETRES_A_OPTIMISER {
			regsub -all -- $p $contenu [set teta$i] nouveau_contenu
			set contenu $nouveau_contenu
			incr i
		    }
		    
		    set file [open ${dir_configurations}$nom/$fic w]
		    puts $file $nouveau_contenu
		    close $file
		}
	    } else {
		CreerFenetreDialogue OPTI-SEPATOU error \
		    "Impossible d'ouvrir le fichier des optimum. Le remplacement des Tetas par leur valeur optimum n'a pas ete fait"
	    }
	}
    }

    destroy $w
}
