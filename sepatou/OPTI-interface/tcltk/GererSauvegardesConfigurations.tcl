# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : GererSauvegardesConfigurations.tcl  
# Contenu : Gestion des sauvegardes de configurations OPTI-SEPATOU
#           et SEPATOU
######################################################################

######################################################################
# Procedure   : GererSauvegardesConfigurations
######################################################################
proc GererSauvegardesConfigurations {dir_configurations type} {

    set w .wm_GererConfigurations$type

    if {[winfo exists $w] == 0} {
	toplevel $w
	wm title $w "OPTI-SEPATOU : Gestion des configurations $type sauvegardees"	

	label $w.l_configuration -text "CONFIGURATIONS $type"
	pack $w.l_configuration -padx 10 -pady 10


	### Zone de selection ###############################################
	frame $w.f_topw -borderwidth 10
	pack $w.f_topw -padx 20 -pady 20 -side top -expand 1

	#Creation de la Scrolled List
	ScrolledListbox $w.f_topw.scrlst_list  -width 50 -height 15 
	set list [recherche_rep ${dir_configurations}]
	set nb [llength $list]
	for  {set i 0} {$i < $nb} {incr i} {
	    set val [lindex $list $i]
	    $w.f_topw.scrlst_list.lst_list insert end $val
	}
	pack $w.f_topw.scrlst_list -padx 4 -side left -anchor n -expand 1
	
	
	###Zone de commandes des boutons  ###################################
	frame $w.f_bottom -borderwidth 3
	pack $w.f_bottom -side bottom -fill x -pady 5
	button $w.f_bottom.b_enlever -text Enlever \
	    -command "Enlever_GererConfigurations ${dir_configurations} $w.f_topw.scrlst_list.lst_list"
	button $w.f_bottom.b_fermer -text Fermer \
	    -command "destroy $w"
	pack $w.f_bottom.b_enlever $w.f_bottom.b_fermer -side left -expand 1 
	### Separateur
	frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $w.f_sep -side bottom -fill x -pady 5
    }
}


#----------------------------------------------------------------------------
# Enleve une configuration sauvegard�e
#----------------------------------------------------------------------------
proc Enlever_GererConfigurations {dir_configurations list} {

    global CMD_REMOVE
    global OPT_RM_DIR

    set i [$list curselection]
    if {$i == ""} {
	CreerFenetreDialogue OPTI-SEPATOU error "Selectionnez une configuration."
    } else {
	set rep [$list get $i]
	set message "Voulez-vous vraiment enlever \n la configuration : $rep ?"
	if { [Confirmer OPTI-SEPATOU $message] == 1} {
	    
	    # destruction du repertoire
	    exec $CMD_REMOVE $OPT_RM_DIR ${dir_configurations}$rep

	    # mise a jour de la liste des configurations
	    $list delete $i
	}
    }
}

