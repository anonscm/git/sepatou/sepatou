# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

####################################################################
# fichier : OPTI-SepatouVariablesGlobales.tcl
# contenu : Definition des variables globales a toute l'interface
#####################################################################

# nom du logiciel
global LOGICIEL
set LOGICIEL "OPTI-SEPATOU"

# DIR_RACINE est defini dans SepatouVariablesGlobales.tcl

#####################################################################

# chemin du repertoire des configurations sauvegardees
global DIR_OPTI-CONFIGURATIONS
set DIR_OPTI-CONFIGURATIONS ${DIR_RACINE}/OPTI-configurations/

# chemin du repertoire pour l'execution 
global DIR_OPTI-EXECUTION
set DIR_OPTI-EXECUTION ${DIR_RACINE}/OPTI-execution/

# chemin du repertoire temporaire de travail 
global DIR_OPTI-TEMPORAIRE
set DIR_OPTI-TEMPORAIRE ${DIR_RACINE}/OPTI-execution/tmp/


#####################################################################
#                Definition des noms de fichiers      
#####################################################################

# nom de l'executable de l'optimisateur
global NOM_OPTIMISATEUR
set NOM_OPTIMISATEUR OPTI

# fichier du critere en lnu
global FIC_CRITERE_LNU
set FIC_CRITERE_LNU "Critere.lnu"

# fichier de configuration de l'algorithme d'optimisation 
global FIC_ALGORITHME_TXT
set FIC_ALGORITHME_TXT "Algorithme.cfg"

# fichier des sorties voulues
global FIC_SORTIES_OPTIMISATEUR_TXT
set FIC_SORTIES_OPTIMISATEUR_TXT "SortiesOptimisateur.cfg"


#####################################################################
#           Definition des noms de sorties possibles     
#####################################################################

# nom du fichier de l'optimum
global FIC_OPTIMUM
set FIC_OPTIMUM Optimum

# nom du fichier de sortie des param�tres (algorithmes Systematique et Kiefer) 
global FIC_PARAMETRES
set FIC_PARAMETRES Parametres

# nom du fichier de sortie du crit�re
global FIC_CRITERE
set FIC_CRITERE Critere

# nom du fichier de sortie du gradient stochastique (algorithme Kiefer)
global FIC_GRADIENT_STOCHASTIQUE
set FIC_GRADIENT_STOCHASTIQUE GradientStochastique

# nom du fichier de sortie des niveaux (algorithme Partition2p)
global FIC_NIVEAU
set FIC_NIVEAU Niveau

# nom du fichier contenant les donn�es permettant de reprendre
# une optimisation avec l'algorithme de Kiefer
global FIC_REPRISE
set FIC_REPRISE Reprise

# nom du fichier contenant l'�valuation de l'optimum
global FIC_EVALUATION_OPTIMUM
set FIC_EVALUATION_OPTIMUM EvaluationOptimum


#####################################################################
#           Definition de variables
#####################################################################

# nom de l'ex�cutable �valuant un optimum
global NOM_EVALUER_OPTIMUM
set NOM_EVALUER_OPTIMUM EVALUER_OPTIMUM

# liste des parametres � optimiser
global PARAMETRES_A_OPTIMISER
set PARAMETRES_A_OPTIMISER [list Teta1 Teta2 Teta3 Teta4 Teta5 Teta6 Teta7 Teta8 Teta9 Teta10]

# valeur pour vide
global VIDE 
set VIDE -9999

# valeur pour al�a
global ALEA 
set ALEA -9998

