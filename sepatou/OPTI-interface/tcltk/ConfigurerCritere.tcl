# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerCritere.tcl  
# Contenu : Description du crit�re � optimiser
######################################################################

######################################################################
# Procedure   : Desc_Critere
# Description : cr�e la fenetre d�crivant le crit�re � optimiser
######################################################################
proc Desc_Critere {} {

    set w .wm_ConfigurerCritere

    if {[winfo exists $w] == 0} {
	toplevel $w
	wm title $w {OPTI-SEPATOU : Description du critere a optimiser}

	frame $w.f_critere
	label $w.f_critere.l_critere -text "Critere � optimiser :"
	pack $w.f_critere.l_critere -pady 10

	text $w.f_critere.txt_critere -width 60 -height 10 -background white \
	    -yscrollcommand "$w.f_critere.scrb_critere set"
	scrollbar $w.f_critere.scrb_critere -orient vertical \
	    -command "$w.f_critere.txt_critere yview"

	pack $w.f_critere
	pack $w.f_critere.txt_critere  -side left
	pack $w.f_critere.scrb_critere -side right -fill y

	### Zone de commandes des boutons ok, annuler et imprimer
	frame $w.f_bottom -borderwidth 3
	pack $w.f_bottom -side bottom -fill x -pady 5

	#Creation des boutons
	button $w.f_bottom.b_ok \
	    -text Ok -command "Ok_ConfigurerCritere $w" 
	button $w.f_bottom.b_cancel \
	    -text Annuler -command "destroy $w"
	button $w.f_bottom.b_imp \
	    -text Imprimer -command "Imprimer_ConfigurerCritere $w"

	pack $w.f_bottom.b_ok $w.f_bottom.b_cancel $w.f_bottom.b_imp \
	    -side left -expand 1 

	### Separateur
	frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $w.f_sep -side bottom -fill x -pady 5

	# Chargement du critere temporaire s'il existe
	ChargerCritereTemporaire $w
    }
}

proc Ok_ConfigurerCritere {w} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_CRITERE_LNU
    global SEPARATEUR_LNU

    if  {[string length [$w.f_critere.txt_critere get 1.0 end]] == 1} {
	CreerFenetreDialogue OPTI-SEPATOU error "Le code du critere est vide"
    } else {
	# enregistrement dans un fichier
	set file [open ${DIR_OPTI-TEMPORAIRE}$FIC_CRITERE_LNU w]
	
	puts $file $SEPARATEUR_LNU
	puts $file "# #"
	puts $file $SEPARATEUR_LNU
	puts $file "FONCTION : CRITERE"
	puts $file "CODE : "
	puts $file [EpurerText [$w.f_critere.txt_critere get 1.0 end]]
	
	close $file

	# fermeture de la fenetre
	destroy $w
    }
}


proc Imprimer_ConfigurerCritere {w} {

    global DIR_OPTI-TEMPORAIRE
    global SEPARATEUR_LNU
    global CMD_REMOVE
    global CMD_A2PS

    set file [open ${DIR_OPTI-TEMPORAIRE}Critere.imp w]

    puts $file $SEPARATEUR_LNU
    puts $file "# #"
    puts $file $SEPARATEUR_LNU
    puts $file "FONCTION : CRITERE"
    puts $file "CODE : "
    puts $file [EpurerText [$w.f_critere.txt_critere get 1.0 end]]

    close $file

    catch {exec $CMD_A2PS ${DIR_OPTI-TEMPORAIRE}Critere.imp}
    exec $CMD_REMOVE ${DIR_OPTI-TEMPORAIRE}Critere.imp

    CreerFenetreDialogue OPTI-SEPATOU info "Impression en cours"
}



proc ChargerCritereTemporaire {w} {
    
    global DIR_OPTI-TEMPORAIRE
    global FIC_CRITERE_LNU

    if {[catch {set file [open ${DIR_OPTI-TEMPORAIRE}$FIC_CRITERE_LNU r]}] == 0} {

	gets $file line ;# $SEPARATEUR_LNU
	gets $file ;# "# #"
	gets $file ;# $SEPARATEUR_LNU
	gets $file ;# "FONCTION : CRITERE"
	gets $file ;# "CODE : "
	while {[eof $file] != 1} {
	    gets $file line
	    $w.f_critere.txt_critere insert end $line
	}
    }
}
