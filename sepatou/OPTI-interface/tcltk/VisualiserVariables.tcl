# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : VisualiserVariables.tcl  
# Contenu : Visualisation de l'�volution d'une ou plusieurs variables
######################################################################


######################################################################
# Procedure   : VisualiserVariables
#               Visualise sous forme de courbes et de tableau
#               une variable ou 10 variables : les t�tas
# arguments   : nb_var = 1 : une variable)
#                     != 1 : 10 variables = les t�tas
#               var = nom du fichier sous DIR_OPTI-TEMPORAIRE
######################################################################
proc VisualiserVariables {nb_var var} {

    global DIR_OPTI-TEMPORAIRE
    global VIDE

     # Tableau de couleurs pour les courbes
    set couleur(1) blue
    set couleur(2) red
    set couleur(3) white
    set couleur(4) orange
    set couleur(5) pink
    set couleur(6) brown
    set couleur(7) yellow
    set couleur(8) green
    set couleur(9) violet
    set couleur(10) black

   # lecture des donn�es #############################################
    set x [list]
    set y [list]
    set nb_iterations 0
    set file [open ${DIR_OPTI-TEMPORAIRE}$var r]
    while {[eof $file] != 1} {
	gets $file line
	if {$line != ""} {
	    incr nb_iterations
	    lappend x $nb_iterations
	    if {$nb_var == 1} {
		lappend y1 $line
	    } else {
		scan $line "%f %f %f %f %f %f %f %f %f %f" \
		    v1 v2 v3 v4 v5 v6 v7 v8 v9 v10
		lappend y1 $v1; lappend y2 $v2; lappend y3 $v3
		lappend y4 $v4; lappend y5 $v5; lappend y6 $v6
		lappend y7 $v7; lappend y8 $v8; lappend y9 $v9
		lappend y10 $v10
	    }
	}
    }
    close $file


    # affichage #######################################################
    set w .wm_Visualiser$var
    catch {destroy $w}
    toplevel $w
    wm title $w "OPTI-SEPATOU : Visualisation $var"
    
    ### Zone de commandes des boutons ok, annuler et imprimer ##########
    frame $w.f_bottom -borderwidth 3
    pack $w.f_bottom -side bottom -fill x -pady 5
    
    #Creation des boutons
    button $w.f_bottom.b_Fermer \
	-text Fermer -command "destroy $w" 
    button $w.f_bottom.b_imp \
	-text Imprimer -command "ImprimerCourbes $w $nb_var $var $nb_iterations"
    pack $w.f_bottom.b_Fermer $w.f_bottom.b_imp \
	-side left -expand 1 
    
    ### Separateur
    frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack $w.f_sep -side bottom -fill x -pady 5
    
    
    # NoteBook #########################################################
    tixNoteBook $w.nb_courbes
    $w.nb_courbes add graphique -label "Graphique"
    set Graphique [$w.nb_courbes subwidget graphique]
    $w.nb_courbes add tableau -label "Tableau"
    set Tableau [$w.nb_courbes subwidget tableau]
    pack $w.nb_courbes -side left -padx 10 -pady 10
    
    # page graphique
    graph $Graphique.g_courbes -title $var -width 700 -height 400 -background grey75
    pack $Graphique.g_courbes
    Blt_ZoomStack $Graphique.g_courbes
    Blt_Crosshairs $Graphique.g_courbes
    Blt_ActiveLegend $Graphique.g_courbes
    $Graphique.g_courbes legend configure -activeforeground yellow

    if {$nb_var == 1} {
	$Graphique.g_courbes element create ligne -label $var \
	    -xdata $x -ydata $y1 -pixels 0.2m 
    } else {
	for {set j 1} {$j <= $nb_var} {incr j} {
	    if {[expr [lindex [set y$j] 0] -  $VIDE] != 0} {
		$Graphique.g_courbes element create ligne$j -label Teta$j \
		    -xdata $x -ydata [set y$j] -pixels 0.2m -color $couleur($j)
	    }
	}
    }
    
    # page tableau
    tixScrolledGrid $Tableau.scrg_courbes -bd 0 -width 700 -height 400
    pack $Tableau.scrg_courbes -expand yes -padx 20 -pady 10
    
    set grid [$Tableau.scrg_courbes subwidget grid]
    $grid config -formatcmd "EditGrid_format $grid"

    $grid set 0 0 -text "Iteration"
    for {set i 1} {$i <= $nb_iterations} {incr i} {
	$grid set 0 $i -text $i
    }
    for {set j 1} {$j <= $nb_var} {incr j} {
	if {$nb_var == 1} {
	    $grid set $j 0 -text Valeur
	} else {
	    $grid set $j 0 -text Teta$j
	}
	for {set i 1} {$i <= $nb_iterations} {incr i} {
	    if {[expr [lindex [set y$j] 0] -  $VIDE] != 0} {
		$grid set $j $i -text [lindex [set y$j] [expr $i - 1]]
	    } else {
		$grid set $j $i -text " "
	    }
	}
    }
}


proc ImprimerCourbes {w nb_var var nb_iterations} {

    global DIR_OPTI-TEMPORAIRE
    global CMD_REMOVE
    global CMD_A2PS
    global CMD_IMPRIMER
    global OPT_IMPRIMER

    if {[string compare [$w.nb_courbes raised] graphique] == 0} {
	set fichier "Courbes.ps"
	set Graphique [$w.nb_courbes subwidget graphique]
	$Graphique.g_courbes postscript output ${DIR_OPTI-TEMPORAIRE}$fichier
    } else {
	set fichier "Courbes.imp"
	set Tableau [$w.nb_courbes subwidget tableau]
	set grid [$Tableau.scrg_courbes subwidget grid]
 	set filetxt [open "${DIR_OPTI-TEMPORAIRE}$fichier" w]
	puts $filetxt $var
	puts $filetxt ""
	if {$nb_var == 1} {
	    puts $filetxt "n\tValeur"
	    for {set i 1} {$i <= $nb_iterations} {incr i} {
		puts $filetxt "$i\t[$grid  entrycget 1 $i -text]" 
	    }
	} else {
	    puts $filetxt "n\tTeta1\tTeta2\tTeta3\tTeta4\tTeta5\tTeta6\tTeta7\tTeta8\tTeta9\tTeta10"
	    for {set i 1} {$i <= $nb_iterations} {incr i} {
		puts $filetxt "$i\t[$grid  entrycget 1 $i -text]\t[$grid  entrycget 2 $i -text]\t[$grid  entrycget 3 $i -text]\t[$grid  entrycget 4 $i -text]\t[$grid  entrycget 5 $i -text]\t[$grid  entrycget 6 $i -text]\t[$grid  entrycget 7 $i -text]\t[$grid  entrycget 8 $i -text]\t[$grid  entrycget 9 $i -text]\t[$grid  entrycget 10 $i -text]"
	    }
	}
	close $filetxt
    }	

    catch {exec $CMD_IMPRIMER $OPT_IMPRIMER ${DIR_OPTI-TEMPORAIRE}$fichier}
    exec $CMD_REMOVE ${DIR_OPTI-TEMPORAIRE}$fichier
    CreerFenetreDialogue OPTI-SEPATOU info "Impression en cours"
}