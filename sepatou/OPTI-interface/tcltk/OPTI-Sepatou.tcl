# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

##################################################################
# fichier : OPTI-Sepatou.tcl
# contenu : interface graphique du logiciel OPTI-SEPATOU
##################################################################

##### chargement des fichiers generaux ###########################
source SepatouFoUtilitaires.tcl
source SepatouVariablesGlobales.tcl
source OPTI-SepatouVariablesGlobales.tcl


##### chargement des extensions ##################################
load /usr/lib/libTix8.4.so.1
lappend auto_path $tix_library
namespace import -force tix::*
load /usr/lib/libBLT.so
lappend auto_path $blt_library
namespace import blt::*
namespace import -force blt::tile::*


##### variables globales #########################################
global CONFIG_EXPLOITATION
global CONFIG_STRATEGIE
global CONFIG_CLIMATS
global CONFIG_ALGORITHME
global CONFIG_CRITERE
global CONFIG_SORTIES_OPTIMISATEUR
global MODIF_STRATEGIE
set CONFIG_EXPLOITATION 0
set CONFIG_STRATEGIE 0
set CONFIG_CLIMATS 0
set CONFIG_ALGORITHME 0
set CONFIG_CRITERE 0
set CONFIG_SORTIES_OPTIMISATEUR 0
set MODIF_STRATEGIE 0


#----------------------------------------------------------------#
# procedure qui affiche la fenetre principale
#----------------------------------------------------------------#
proc Afficher_Fenetre_Principale {} {

###### mise a jour du titre de la fenetre principale ##############
wm title . {OPTI-SEPATOU : aide a l'optimisation de strategies \
            pour le paturage tournant}

##### affichage d une zone affichage texte sous le menu principal #
frame .f_affichage
text .f_affichage.t_texte -width 80 -height 30 -state disabled \
    -yscrollcommand {.f_affichage.scrb_scroll set} \
    -background #fdd ;# fond rose pale
scrollbar .f_affichage.scrb_scroll -command {.f_affichage.t_texte yview}
pack .f_affichage.scrb_scroll -side right -fill y
pack .f_affichage.t_texte -side left -fill both -expand true
pack .f_affichage -side bottom -fill both -expand true


######## declaration du menu General ###############################
menubutton .mb_General -text "General"  -menu .mb_General.m_menu
pack .mb_General -side left
menu .mb_General.m_menu -tearoff 0
.mb_General.m_menu add separator
.mb_General.m_menu add command -label "Presentation" -command General_Presentation
.mb_General.m_menu add command -label Impression -command General_Impression
.mb_General.m_menu add command -label Quitter -command General_Quitter

######## declaration du menu Configurer ###########################  
menubutton .mb_Configurer  -text Configurer  -menu .mb_Configurer.m_menu
pack .mb_Configurer -side left
menu .mb_Configurer.m_menu -tearoff 0
.mb_Configurer.m_menu add separator
.mb_Configurer.m_menu add command -label "Exploitation" -command Configurer_Exploitation
.mb_Configurer.m_menu add command -label "Strategie" -command Configurer_Strategie
.mb_Configurer.m_menu add command -label "Climat" -command Configurer_Climat
.mb_Configurer.m_menu add command -label "Algorithme"  -command Configurer_Algorithme
.mb_Configurer.m_menu add command -label "Critere"  -command Configurer_Critere
.mb_Configurer.m_menu add command -label "SortiesDesirees"  -command Configurer_SortiesOptimisateur
.mb_Configurer.m_menu add separator
.mb_Configurer.m_menu add command -label "Configuration OPTI-SEPATOU" \
    -command Configurer_ConfigurationOPTI-SEPATOU
.mb_Configurer.m_menu add command -label "Configuration SEPATOU" \
    -command Configurer_ConfigurationSEPATOU

######## declaration du menu Executer ############################
menubutton .mb_Executer -text "Executer"  -menu .mb_Executer.m_menu
pack .mb_Executer -side left
menu .mb_Executer.m_menu -tearoff 0
.mb_Executer.m_menu add separator 
.mb_Executer.m_menu add command -label "TraduireStrategie" \
    -command Executer_TraduireStrategie
.mb_Executer.m_menu add command -label "Optimiser" -command Executer_Optimiser
.mb_Executer.m_menu add command -label "Poursuivre" -command Executer_Poursuivre
.mb_Executer.m_menu add command -label "EvaluerOptimum" -command Executer_EvaluerOptimum

######## declaration du menu Visualiser  ########################
menubutton .mb_Visualiser -text Visualiser  -menu .mb_Visualiser.m_menu
pack .mb_Visualiser -side left
menu .mb_Visualiser.m_menu -tearoff 0
.mb_Visualiser.m_menu add separator
.mb_Visualiser.m_menu add command -label "Optimum" -command Visualiser_Optimum
.mb_Visualiser.m_menu add command -label "Critere" -command Visualiser_Critere
.mb_Visualiser.m_menu add command -label "Parametres" -command Visualiser_Parametres
.mb_Visualiser.m_menu add command -label "GradientStochastique" \
    -command Visualiser_GradientStochastique
.mb_Visualiser.m_menu add command -label "Niveau" -command Visualiser_Niveau

######## declaration du menu Sauvegarder  ########################
menubutton .mb_Sauvegarder -text Sauvegarder  -menu .mb_Sauvegarder.m_menu
pack .mb_Sauvegarder -side left
menu .mb_Sauvegarder.m_menu -tearoff 0
.mb_Sauvegarder.m_menu add separator
.mb_Sauvegarder.m_menu add command -label "Configuration OPTI-SEPATOU" -command Sauvegarder_ConfigurationOPTI-SEPATOU
.mb_Sauvegarder.m_menu add command -label "Configuration SEPATOU" -command Sauvegarder_ConfigurationSEPATOU

######## declaration du menu GererSauvegardes ################################
menubutton .mb_GererSauvegardes -text "GererSauvegardes"  -menu .mb_GererSauvegardes.m_menu
pack .mb_GererSauvegardes -side right
menu .mb_GererSauvegardes.m_menu -tearoff 0
.mb_GererSauvegardes.m_menu add command -label "Configurations OPTI-SEPATOU" \
    -command GererSauvegardes_ConfigurationsOPTI-SEPATOU
.mb_GererSauvegardes.m_menu add command -label "Configurations SEPATOU" \
    -command GererSauvegardes_ConfigurationsSEPATOU

###### initialisation des appels possibles #########################
.mb_Executer.m_menu entryconfigure 1 -state disabled
.mb_Executer.m_menu entryconfigure 2 -state disabled
.mb_Executer.m_menu entryconfigure 3 -state disabled
.mb_Executer.m_menu entryconfigure 4 -state disabled
.mb_Visualiser.m_menu entryconfigure 1 -state disabled
.mb_Visualiser.m_menu entryconfigure 2 -state disabled
.mb_Visualiser.m_menu entryconfigure 3 -state disabled
.mb_Visualiser.m_menu entryconfigure 4 -state disabled
.mb_Visualiser.m_menu entryconfigure 5 -state disabled
.mb_Sauvegarder.m_menu entryconfigure 1 -state disabled
.mb_Sauvegarder.m_menu entryconfigure 2 -state disabled
}

#----------------------------------------------------------------#
# procedure qui decrit le logiciel
#----------------------------------------------------------------#
proc General_Presentation {} {
 CreerFenetreDialogue OPTI-SEPATOU info \
       "OPTI-SEPATOU est un logiciel d'aide � l'optimisation de strategies pour conduire l'alimentation d'un troupeau de vache pratiquant le paturage tournant.

Version : v1.21 (Mars 2006)"
}

#----------------------------------------------------------------#
# procedure qui enregistre la commande d'impression
#----------------------------------------------------------------#
proc General_Impression {} {
    # demande des commandes d'impression
    source GeneralImpression.tcl
    GeneralImpression
}

#----------------------------------------------------------------#
# procedure qui arrete le logiciel
#----------------------------------------------------------------#
proc General_Quitter {} {
    toplevel .wm_Quitter
    wm title .wm_Quitter {OPTI-SEPATOU : Quitter}

    frame .wm_Quitter.f_aff
    label .wm_Quitter.f_aff.l_icon -bitmap question -foreground red
    message .wm_Quitter.f_aff.msg_Quitter -aspect 1000 -justify center \
    -text "Voulez-vous vraiment quitter ?"
    pack .wm_Quitter.f_aff.l_icon -side left -padx 16 -pady 12
    pack .wm_Quitter.f_aff.msg_Quitter -padx 8 -pady 8
    pack .wm_Quitter.f_aff

    frame .wm_Quitter.f_controle 
    button .wm_Quitter.f_controle.b_ok -text "Ok" -command exit
    button .wm_Quitter.f_controle.b_annuler -text "Annuler" \
	-command {destroy .wm_Quitter}
    pack .wm_Quitter.f_controle.b_ok -side left -padx 10
    pack .wm_Quitter.f_controle.b_annuler  -padx 10
    pack .wm_Quitter.f_controle -expand 1 -side bottom

    frame .wm_Quitter.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
    pack .wm_Quitter.f_sep -fill x -pady 4  -expand 1 -side bottom

    grab .wm_Quitter
    tkwait window .wm_Quitter
    grab release .wm_Quitter
}

#----------------------------------------------------------------#
# procedure qui configure l'exploitation
#----------------------------------------------------------------#
proc Configurer_Exploitation {} {
    global DIR_OPTI-TEMPORAIRE
    global CONFIG_EXPLOITATION

    # description de l'exploitation et
    # ecriture du fichier temporaire tmp/Exploitation.cfg
    source ConfigurerExploitation.tcl
    Desc_Exploitation ecriture ${DIR_OPTI-TEMPORAIRE}

    # mise a jour des appels possibles
    set CONFIG_EXPLOITATION 1
    TesterFinConfiguration

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration exploitation\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui configure la strategie
#----------------------------------------------------------------#
proc Configurer_Strategie {} {
    global DIR_OPTI-TEMPORAIRE
    global CONFIG_STRATEGIE

    source ConfigurerStrategie.tcl
    Desc_Strategie ecriture ${DIR_OPTI-TEMPORAIRE}

    # mise a jour des appels possibles
    set CONFIG_STRATEGIE 1
    TesterFinConfiguration  
 
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration strategie\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui caracterise le climat voulu
#----------------------------------------------------------------#
proc Configurer_Climat {} {
    global DIR_OPTI-TEMPORAIRE
    global CONFIG_CLIMATS

    # description du climat et
    # ecriture du fichier temporaire tmp/Simulateur.cfg
    source ConfigurerSimulations.tcl
    Desc_Simulation ${DIR_OPTI-TEMPORAIRE} optimisation

    # mise a jour des appels possibles
    set CONFIG_CLIMATS 1
    TesterFinConfiguration

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration du climat\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui configure l'algorithme d'optimisation
#----------------------------------------------------------------#
proc Configurer_Algorithme {} {
    global CONFIG_ALGORITHME

    # description de l'algorithme desire et
    # ecriture du fichier temporaire tmp/Algorithme.cfg
    source ConfigurerAlgorithme.tcl
    Desc_Algorithme

    # mise a jour des appels possibles
    set CONFIG_ALGORITHME 1
    TesterFinConfiguration

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration de l'algorithme d'optimisation\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui configure le critere � optimiser
#----------------------------------------------------------------#
proc Configurer_Critere {} {
    global CONFIG_CRITERE

    # description du critere et
    # ecriture du fichier temporaire tmp/Critere.lnu
    source ConfigurerCritere.tcl
    Desc_Critere

    # mise a jour des appels possibles
    set CONFIG_CRITERE 1
    TesterFinConfiguration

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration du critere � optimiser\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui configure les sorties d�sir�es
#----------------------------------------------------------------#
proc Configurer_SortiesOptimisateur {} {
    global CONFIG_SORTIES_OPTIMISATEUR

    # ecriture du fichier temporaire tmp/SortiesOptimisateur.cfg
    source ConfigurerSortiesOptimisateur.tcl
    Desc_SortiesOptimisateur

    # mise a jour des appels possibles
    set CONFIG_SORTIES_OPTIMISATEUR 1
    TesterFinConfiguration

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Configuration des sorties desirees\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui charge une configuration OPTI-SEPATOU
#----------------------------------------------------------------#
proc Configurer_ConfigurationOPTI-SEPATOU {} {
    global CONFIG_EXPLOITATION
    global CONFIG_STRATEGIE
    global CONFIG_CLIMATS
    global CONFIG_ALGORITHME
    global CONFIG_CRITERE
    global CONFIG_SORTIES_OPTIMISATEUR
    global DIR_OPTI-CONFIGURATIONS

    # chargement configuration
    source ConfigurerConfiguration.tcl
    Desc_Configuration ${DIR_OPTI-CONFIGURATIONS} "OPTI-SEPATOU"

    # mise a jour des appels possibles
    set CONFIG_EXPLOITATION 1
    set CONFIG_STRATEGIE 1
    set CONFIG_CLIMATS 1
    set CONFIG_ALGORITHME 1
    set CONFIG_CRITERE 1
    set CONFIG_SORTIES_OPTIMISATEUR 1
    TesterFinConfiguration
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Chargement d'une configuration OPTI-SEPATOU\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui charge une configuration SEPATOU
#----------------------------------------------------------------#
proc Configurer_ConfigurationSEPATOU {} {
    global DIR_CONFIGURATIONS
    global CONFIG_EXPLOITATION
    global CONFIG_STRATEGIE
    global CONFIG_CLIMATS

    # chargement configuration
    source ConfigurerConfiguration.tcl
    Desc_Configuration $DIR_CONFIGURATIONS "SEPATOU"
        
    # mise a jour des appels possibles
    set CONFIG_EXPLOITATION 1
    set CONFIG_STRATEGIE 1
    set CONFIG_CLIMATS 1
    TesterFinConfiguration
	
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Chargement d'une configuration SEPATOU\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}



#----------------------------------------------------------------#
# procedure qui traduit la strategie courante
#----------------------------------------------------------------#
proc Executer_TraduireStrategie {} {
    global DIR_OPTI-TEMPORAIRE
    global FIC_FOINTERPRETATION_LNU
    global FIC_INDICATEURS_LNU
    global FIC_REGLESPLANIFICATION_LNU
    global FIC_REGLESOPERATOIRES_LNU
    global FIC_STRATEGIE_LNU
    global FIC_FOINTERPRETATION_CC
    global FIC_INDICATEURS_CC
    global FIC_REGLESPLANIFICATION_CC
    global FIC_REGLESOPERATOIRES_CC
    global FIC_STRATEGIE_H
    global FIC_CRITERE_LNU
    global NOM_OPTIMISATEUR
    global CMD_CAT
    global CMD_REMOVE
    global OPT_RM_DIR
    global MODIF_STRATEGIE
    global COMPILER_EVALUER_OPTIMUM

    if {$MODIF_STRATEGIE == 1} {
	busy hold .
	update
	# destruction du fichier existant, en cas de probleme en cours
	# de procedure evite une execution avec le fichier pre-existant
	if [file exist $NOM_OPTIMISATEUR] {
	    exec $CMD_REMOVE $OPT_RM_DIR $NOM_OPTIMISATEUR
	}

	# rassemblement des fichiers lnu dans un seul fichier
	exec $CMD_CAT ${DIR_OPTI-TEMPORAIRE}$FIC_FOINTERPRETATION_LNU > $FIC_STRATEGIE_LNU
	exec $CMD_CAT ${DIR_OPTI-TEMPORAIRE}$FIC_CRITERE_LNU >> $FIC_STRATEGIE_LNU
	exec $CMD_CAT ${DIR_OPTI-TEMPORAIRE}$FIC_INDICATEURS_LNU >> $FIC_STRATEGIE_LNU
	exec $CMD_CAT ${DIR_OPTI-TEMPORAIRE}$FIC_REGLESPLANIFICATION_LNU >> $FIC_STRATEGIE_LNU
	exec $CMD_CAT ${DIR_OPTI-TEMPORAIRE}$FIC_REGLESOPERATOIRES_LNU >> $FIC_STRATEGIE_LNU

	# traduction strategie LnU --> C++ 
	catch {exec VERIFIER_STRATEGIE $FIC_STRATEGIE_LNU > SortieVerificateur.tmp}
	# affichage resultat
       .f_affichage.t_texte configure -state normal
       if {[catch {set file [open "SortieVerificateur.tmp" r]}] == 0} {
	   .f_affichage.t_texte insert end "\n\n"
	   .f_affichage.t_texte insert end [read $file]
	   .f_affichage.t_texte yview end
	   close $file
	   exec $CMD_REMOVE $OPT_RM_DIR SortieVerificateur.tmp
       }
       .f_affichage.t_texte configure -state disabled
 
	if [file exist OKverification] {
	    exec $CMD_REMOVE $OPT_RM_DIR OKverification 
	    catch {exec TRADUIRE_STRATEGIE $FIC_STRATEGIE_LNU > SortieTraducteur.tmp}
	    # affichage resultat
	    .f_affichage.t_texte configure -state normal
	    if {[catch {set file [open "SortieTraducteur.tmp" r]}] == 0} {
		.f_affichage.t_texte insert end [read $file]
		.f_affichage.t_texte yview end
		close $file
		exec $CMD_REMOVE $OPT_RM_DIR SortieTraducteur.tmp
	    }
	    .f_affichage.t_texte configure -state disabled

	    if [file exist FichTmp.tmp] {
		exec $CMD_REMOVE $OPT_RM_DIR FichTmp.tmp
	    }

	    if [file exist OKtraduction] {
		exec $CMD_REMOVE $OPT_RM_DIR OKtraduction 

		if [file exist FichTmp.tmp] {
		    exec $CMD_REMOVE $OPT_RM_DIR FichTmp.tmp
		}
		
		# compilation du simulateur
		exec COMPILER_OPTIMISATEUR > SortieCompilation.tmp
		.f_affichage.t_texte configure -state normal
		if {[catch {set file [open "SortieCompilation.tmp" r]}] == 0} {
		    .f_affichage.t_texte configure -state normal
		    .f_affichage.t_texte insert end "\n\n"
		    .f_affichage.t_texte insert end [read $file]
		    close $file
		    exec $CMD_REMOVE $OPT_RM_DIR SortieCompilation.tmp
		    .f_affichage.t_texte yview end
		}

		# compilation pour �valuer l'optimum trouv�
		exec COMPILER_EVALUER_OPTIMUM > SortieCompilation.tmp
		.f_affichage.t_texte configure -state normal
		if {[catch {set file [open "SortieCompilation.tmp" r]}] == 0} {
		    .f_affichage.t_texte configure -state normal
		    .f_affichage.t_texte insert end "\n\n"
		    .f_affichage.t_texte insert end [read $file]
		    close $file
		    exec $CMD_REMOVE $OPT_RM_DIR SortieCompilation.tmp
		    .f_affichage.t_texte yview end
		}

		# destruction des fichiers temporaires crees
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_STRATEGIE_LNU
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_STRATEGIE_H
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_INDICATEURS_CC
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_FOINTERPRETATION_CC
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_REGLESPLANIFICATION_CC
		exec $CMD_REMOVE $OPT_RM_DIR $FIC_REGLESOPERATOIRES_CC

		# affichage dans zone texte
		.f_affichage.t_texte insert end "Fin traduction strategie\n"
		.f_affichage.t_texte yview end
		.f_affichage.t_texte configure -state disabled

		# mise a jour des appels possibles
		.mb_Executer.m_menu entryconfigure 2 -state normal 

		set MODIF_STRATEGIE 0
	    }
	}
	busy release .
    }
    beep 100
}

#----------------------------------------------------------------#
# procedure qui realise les simulations voulues
#----------------------------------------------------------------#
proc Executer_Optimiser {} {
   global DIR_OPTI-TEMPORAIRE
   global FIC_PROBLEME
   global FIC_OPTIMUM
   global FIC_PARAMETRES
   global FIC_CRITERE
   global FIC_GRADIENT_STOCHASTIQUE
   global FIC_NIVEAU
   global NOM_OPTIMISATEUR
   global MODIF_STRATEGIE
   global CMD_REMOVE
   global OPT_RM_DIR
   global FIC_FOINTERPRETATION_LNU
   global FIC_INDICATEURS_LNU
   global FIC_REGLESPLANIFICATION_LNU
   global FIC_REGLESOPERATOIRES_LNU
   global PARAMETRES_A_OPTIMISER
   global FIC_REPRISE

   if {$MODIF_STRATEGIE == 1} { 
	# la strategie est modifiee ##############################
       CreerFenetreDialogue OPTI-SEPATOU error \
           "La strategie a ete modifiee, il faut la traduire avant d'optimiser."

    } else {
	# la strategie n'a pas ete modifiee ################################
	# v�rification de la coh�rence des param�tres entre la strat�gie 
	# et les donn�es de l'algorithme
	foreach fic [list $FIC_FOINTERPRETATION_LNU $FIC_INDICATEURS_LNU $FIC_REGLESPLANIFICATION_LNU $FIC_REGLESOPERATOIRES_LNU] {
	    set file [open ${DIR_OPTI-TEMPORAIRE}$fic r]
	    set contenu [read $file]
	    close $file
	    append strategie $contenu
	}
	set parametres_algorithme [LireParametresAlgorithme]

	set parametres_probleme [list]
	foreach p $PARAMETRES_A_OPTIMISER {
	    if { ([lsearch $parametres_algorithme $p] != -1 &&
		  [regsub $p $strategie "" v] == 0) || 
		 ([lsearch $parametres_algorithme $p] == -1 &&
		  [regsub $p $strategie "" v] != 0) } {
		lappend parametres_probleme $p
	    }
	}
	if {[llength $parametres_probleme] != 0} {
	    CreerFenetreDialogue OPTI-SEPATOU error \
		"Ce(s) parametre(s) : $parametres_probleme est (sont) utilise(s) dans la strategie mais pas defini(s) dans la description de l'algorithme ou inversement."
	} else {

	    # destruction d'anciens fichiers
	    catch {exec $CMD_REMOVE $OPT_RM_DIR ${DIR_OPTI-TEMPORAIRE}$FIC_PROBLEME}
	    catch {exec $CMD_REMOVE $OPT_RM_DIR ${DIR_OPTI-TEMPORAIRE}$FIC_OPTIMUM}
	    catch {exec $CMD_REMOVE $OPT_RM_DIR ${DIR_OPTI-TEMPORAIRE}$FIC_PARAMETRES}
	    catch {exec $CMD_REMOVE $OPT_RM_DIR ${DIR_OPTI-TEMPORAIRE}$FIC_CRITERE}
	    catch {exec $CMD_REMOVE $OPT_RM_DIR ${DIR_OPTI-TEMPORAIRE}$FIC_GRADIENT_STOCHASTIQUE}
 	    catch {exec $CMD_REMOVE $OPT_RM_DIR ${DIR_OPTI-TEMPORAIRE}$FIC_NIVEAU}
	    catch {exec $CMD_REMOVE $OPT_RM_DIR $FIC_REPRISE}
	    
	    # lancement de l'optimisation
	    exec $NOM_OPTIMISATEUR > SortieOptimisateur.tmp  
	    if {[catch {set file [open "SortieOptimisateur.tmp" r]}] == 0} {
		.f_affichage.t_texte configure -state normal
		.f_affichage.t_texte insert end "\n\n"
		.f_affichage.t_texte insert end [read $file]
		close $file
		exec $CMD_REMOVE $OPT_RM_DIR SortieOptimisateur.tmp
		.f_affichage.t_texte yview end
		.f_affichage.t_texte configure -state disabled
	    }
	    
	    # mise a jour des appels possibles
	    .mb_Executer.m_menu entryconfigure 3 -state normal 
	    .mb_Executer.m_menu entryconfigure 4 -state normal 
	    .mb_Visualiser.m_menu entryconfigure 1 -state normal 
	    .mb_Visualiser.m_menu entryconfigure 2 -state normal 
	    .mb_Visualiser.m_menu entryconfigure 3 -state normal 
	    .mb_Visualiser.m_menu entryconfigure 4 -state normal 
	    .mb_Visualiser.m_menu entryconfigure 5 -state normal 
	    .mb_Sauvegarder.m_menu entryconfigure 2 -state normal
	    
	    # affichage dans zone texte
	    .f_affichage.t_texte configure -state normal
	    .f_affichage.t_texte insert end "Realisation de l'optimisation demandee\n"
	    .f_affichage.t_texte yview end
	    .f_affichage.t_texte configure -state disabled
	}
	beep 100
    }
}


#----------------------------------------------------------------#
# Poursuite d'une optimisation avec l'algorithme de Kiefer
#----------------------------------------------------------------#
proc Executer_Poursuivre {} {
    global MODIF_STRATEGIE
    global DIR_OPTI-TEMPORAIRE
    global NOM_OPTIMISATEUR
    global CMD_REMOVE
    global OPT_RM_DIR
    global FIC_REPRISE

    if {$MODIF_STRATEGIE == 1} { 
	# la strategie est modifiee ##############################
	CreerFenetreDialogue OPTI-SEPATOU error "La strategie a ete modifiee, il faut la traduire avant d'optimiser."
    } else {
	if [file exists $FIC_REPRISE] {
	    # lancements de l'optimisation
	    exec $NOM_OPTIMISATEUR > SortieOptimisateur.tmp  
	    if {[catch {set file [open "SortieOptimisateur.tmp" r]}] == 0} {
		.f_affichage.t_texte configure -state normal
		.f_affichage.t_texte insert end "\n\n"
		.f_affichage.t_texte insert end [read $file]
		close $file
		exec $CMD_REMOVE $OPT_RM_DIR SortieOptimisateur.tmp
		.f_affichage.t_texte yview end
		.f_affichage.t_texte configure -state disabled
	    }
	    # affichage dans zone texte
	    .f_affichage.t_texte configure -state normal
	    .f_affichage.t_texte insert end "Poursuite de l'optimisation\n"
	    .f_affichage.t_texte yview end
	    .f_affichage.t_texte configure -state disabled
	} else {
	    CreerFenetreDialogue OPTI-SEPATOU error "Il n'est possible de poursuivre qu'apres une optimisation avec l'algorithme de Kiefer."
	}
    }
    beep 100
}


#----------------------------------------------------------------#
# procedure qui �value l'optimum trouv�
#----------------------------------------------------------------#
proc Executer_EvaluerOptimum {} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_EVALUATION_OPTIMUM
    global FIC_OPTIMUM
    global CMD_REMOVE
    global OPT_RM_DIR
    global NOM_EVALUER_OPTIMUM

    if [file exists ${DIR_OPTI-TEMPORAIRE}$FIC_OPTIMUM] {
	if [file exists ${DIR_OPTI-TEMPORAIRE}$FIC_EVALUATION_OPTIMUM] {
	    catch {exec $CMD_REMOVE $OPT_RM_DIR $FIC_EVALUATION_OPTIMUM}
	}

	exec $NOM_EVALUER_OPTIMUM  >   $FIC_EVALUATION_OPTIMUM
	if {[catch {set file [open $FIC_EVALUATION_OPTIMUM r]}] == 0} {
	    .f_affichage.t_texte configure -state normal
	    .f_affichage.t_texte insert end "\n\n"
	    .f_affichage.t_texte insert end [read $file]
	    close $file
	    .f_affichage.t_texte yview end
	    .f_affichage.t_texte configure -state disabled
	}
    } else {
	CreerFenetreDialogue OPTI-SEPATOU error \
	    "Il n'existe pas de fichier $FIC_EVALUATION_OPTIMUM"
    }
    beep 100
}


#----------------------------------------------------------------#
# procedure qui visualise l'optimum
#----------------------------------------------------------------#
proc Visualiser_Optimum {} {
    
    set algo_choisi [LireNomAlgorithme]
    source VisualiserOptimum.tcl
    VisualiserOptimum $algo_choisi
  
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Visualisation de l'optimum\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui visualise le crit�re
#----------------------------------------------------------------#
proc Visualiser_Critere {} {
    global FIC_CRITERE

    if {[string compare  [LireNomAlgorithme] "Partition2p"] != 0} {
	source VisualiserVariables.tcl
	VisualiserVariables 1 $FIC_CRITERE
     } else {
	 source VisualiserCriterePartition2p.tcl
	 VisualiserCritere
    }

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Visualisation de l'evolution du critere\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui visualise les parametres
#----------------------------------------------------------------#
proc Visualiser_Parametres {} {
    global DIR_OPTI-TEMPORAIRE
    global FIC_PARAMETRES
    global PARAMETRES_A_OPTIMISER

    if [file exists ${DIR_OPTI-TEMPORAIRE}$FIC_PARAMETRES ] {
	source VisualiserVariables.tcl
	VisualiserVariables [llength $PARAMETRES_A_OPTIMISER] $FIC_PARAMETRES
    } else {
	CreerFenetreDialogue OPTI-SEPATOU error \
	    "Il n'existe pas de sortie : parametres"
    }

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Visualisation de l'evolution des parametres\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui visualise le gradient stochastique
#----------------------------------------------------------------#
proc Visualiser_GradientStochastique {} {
    global DIR_OPTI-TEMPORAIRE
    global FIC_GRADIENT_STOCHASTIQUE
    global PARAMETRES_A_OPTIMISER

    if [file exists ${DIR_OPTI-TEMPORAIRE}$FIC_GRADIENT_STOCHASTIQUE] {
	source VisualiserVariables.tcl
	VisualiserVariables [llength $PARAMETRES_A_OPTIMISER] $FIC_GRADIENT_STOCHASTIQUE
    } else {
	CreerFenetreDialogue OPTI-SEPATOU error \
	    "Il n'existe pas de sortie : gradient stochastique"
    }

    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Visualisation du gradient stochastique\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui visualise le niveau
#----------------------------------------------------------------#
proc Visualiser_Niveau {} {
    global DIR_OPTI-TEMPORAIRE
    global FIC_NIVEAU

    if [file exists ${DIR_OPTI-TEMPORAIRE}$FIC_NIVEAU] {
	if {[file size ${DIR_OPTI-TEMPORAIRE}$FIC_NIVEAU] != 0} {
	    source VisualiserVariables.tcl
	    VisualiserVariables 1 $FIC_NIVEAU
	} else {
	    CreerFenetreDialogue OPTI-SEPATOU error \
		"Aucune partition n'a ete realisee."
	}
    } else {
	CreerFenetreDialogue OPTI-SEPATOU error \
	    "Il n'existe pas de sortie : Niveau"
    }
}

#----------------------------------------------------------------#
# procedure qui sauvegarde une configuration OPTI-SEPATOU
#----------------------------------------------------------------#
proc Sauvegarder_ConfigurationOPTI-SEPATOU {} {

    source SauvegarderConfiguration.tcl
    SauvegarderConfiguration OPTI-SEPATOU
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Sauvegarde de configuration OPTI-SEPATOU\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui sauvegarde une configuration SEPATOU
#----------------------------------------------------------------#
proc Sauvegarder_ConfigurationSEPATOU {} {

    source SauvegarderConfiguration.tcl
    SauvegarderConfiguration SEPATOU
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Sauvegarde de configuration SEPATOU\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui gere les configurations OPTI-SEPATOU sauvegardees
#----------------------------------------------------------------#
proc GererSauvegardes_ConfigurationsOPTI-SEPATOU {} {
    global DIR_OPTI-CONFIGURATIONS

    source GererSauvegardesConfigurations.tcl
    GererSauvegardesConfigurations ${DIR_OPTI-CONFIGURATIONS} OPTI-SEPATOU
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Gestion des configurations OPTI-SEPATOU\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}

#----------------------------------------------------------------#
# procedure qui gere les configurations SEPATOU sauvegardees
#----------------------------------------------------------------#
proc GererSauvegardes_ConfigurationsSEPATOU {} {
    global DIR_CONFIGURATIONS

    source GererSauvegardesConfigurations.tcl
    GererSauvegardesConfigurations  ${DIR_CONFIGURATIONS} SEPATOU
    
    # affichage dans zone texte
    .f_affichage.t_texte configure -state normal
    .f_affichage.t_texte insert end "Gestion des configurations SEPATOU\n"
    .f_affichage.t_texte yview end
    .f_affichage.t_texte configure -state disabled
}


#----------------------------------------------------------------#
# procedure qui teste si une configuration complete a ete decrite
# et qui met a jour le menu principal
#----------------------------------------------------------------#
proc TesterFinConfiguration {} {
    global CONFIG_EXPLOITATION
    global CONFIG_STRATEGIE
    global CONFIG_CLIMATS
    global CONFIG_ALGORITHME
    global CONFIG_CRITERE
    global CONFIG_SORTIES_OPTIMISATEUR

    if {$CONFIG_EXPLOITATION == 1} {
	if {$CONFIG_STRATEGIE == 1} {
	    if {$CONFIG_CLIMATS == 1} {
		if {$CONFIG_ALGORITHME == 1} {
		    if {$CONFIG_CRITERE == 1} {
			if {$CONFIG_SORTIES_OPTIMISATEUR == 1} {
			    .mb_Executer.m_menu entryconfigure 1 -state normal 
			    .mb_Sauvegarder.m_menu entryconfigure 1 -state normal
			}
		    }
		}
	    }
	}
    }
}



#----------------------------------------------------------------#
# appel de la fenetre principale
#----------------------------------------------------------------#
Afficher_Fenetre_Principale


#----------------------------------------------------------------#
# Lecture du nom de l'algorithme
#----------------------------------------------------------------#
proc LireNomAlgorithme {} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_ALGORITHME_TXT

    set file [open "${DIR_OPTI-TEMPORAIRE}$FIC_ALGORITHME_TXT" r]
    gets $file line
    set nom_algo [SortirValeur $line]
    close $file

    return $nom_algo
}

#----------------------------------------------------------------#
# Lecture des parametres d�finis avec l'algorithme
#----------------------------------------------------------------#
proc LireParametresAlgorithme {} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_ALGORITHME_TXT
    global PARAMETRES_A_OPTIMISER
    global VIDE
 
    set parametres_algorithme [list]
 
    set file [open "${DIR_OPTI-TEMPORAIRE}$FIC_ALGORITHME_TXT" r]
    gets $file line ;# algorithme
    gets $file line ;# commentaires
    for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
	gets $file line 
	scan $line "%s %f %f %f %f %f %f" para min max typ pas initial c
	if {$typ != $VIDE} {
	    lappend parametres_algorithme $para
	}
    }
    close $file
    
    return $parametres_algorithme
}

