# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : ConfigurerAlgorithme.tcl  
# Contenu : Description de l'algorithme d'optimisation
######################################################################

######################################################################
# Procedure   : Desc_Algorithme
# Description : cr�e la fenetre d�crivant l'algorithme d'optimisation d�sir�
######################################################################
proc Desc_Algorithme {} {

    global PARAMETRES_A_OPTIMISER
    global gridPara
    global gridSystematique
    global pageKiefer
    global gridKiefer
    global pagePartition2p
    global gridPartition2p

    set w .wm_ConfigurerAlgorithme

    if {[winfo exists $w] == 0} {
	toplevel $w
	wm title $w {OPTI-SEPATOU : Description de l'algorithme d'optimisation et des parametres a optimiser}


	### Zone de commandes des boutons ok, annuler et imprimer ##########
	frame $w.f_bottom -borderwidth 3
	pack $w.f_bottom -side bottom -fill x -pady 5

	#Creation des boutons
	button $w.f_bottom.b_ok \
	    -text Ok -command "Ok_ConfigurerAlgorithme $w" 
	button $w.f_bottom.b_cancel \
	    -text Annuler -command "destroy $w"
	button $w.f_bottom.b_imp \
	    -text Imprimer -command "Imprimer_ConfigurerAlgorithme $w"

	pack $w.f_bottom.b_ok $w.f_bottom.b_cancel $w.f_bottom.b_imp \
	    -side left -expand 1 

	### Separateur
	frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $w.f_sep -side bottom -fill x -pady 5


	# frame de gauche d�crivant l'algorithme d'optimisation ############
	frame $w.f_algo
	tixNoteBook $w.f_algo.nb_algo
	$w.f_algo.nb_algo add systematique -label "Systematique"
	set pageSystematique [$w.f_algo.nb_algo subwidget systematique]
	$w.f_algo.nb_algo add kiefer -label "Kiefer"
	set pageKiefer [$w.f_algo.nb_algo subwidget kiefer]
	$w.f_algo.nb_algo add partition2p -label "Partition2p"
	set pagePartition2p [$w.f_algo.nb_algo subwidget partition2p]

	pack $w.f_algo -side left -padx 10 -pady 10
	pack $w.f_algo.nb_algo


	# page Systematique
	tixGrid $pageSystematique.g_sys  \
	    -width 2 -height [expr 1 + [llength $PARAMETRES_A_OPTIMISER]]
	set gridSystematique $pageSystematique.g_sys
	$gridSystematique config -formatcmd "EditGrid_format $gridSystematique"
	$gridSystematique config -editnotifycmd "Editable"
	# �criture des commentaires
	$gridSystematique set 0 0 -text "Parametres"
	set i 0
	foreach p $PARAMETRES_A_OPTIMISER {
	    $gridSystematique set 0 [expr $i + 1 ] \
		-text [lindex $PARAMETRES_A_OPTIMISER $i]
	    $gridSystematique set 1 [expr $i + 1 ] -text ""
	    incr i
	}
	$gridSystematique set 1 0 -text "Pas"
	pack $pageSystematique.g_sys -pady 80


	#page Kiefer
	label $pageKiefer.l_A  -text "A (>0)        "
	entry $pageKiefer.e_A 
	label  $pageKiefer.l_B -text "B (>0 et <0.5)"
	entry $pageKiefer.e_B
	pack $pageKiefer.l_A $pageKiefer.e_A
	pack $pageKiefer.l_B $pageKiefer.e_B

	tixGrid $pageKiefer.g_kiefer \
	    -width 3 -height [expr 1 + [llength $PARAMETRES_A_OPTIMISER]]
	set gridKiefer $pageKiefer.g_kiefer
	$gridKiefer config -formatcmd "EditGrid_format $gridKiefer"
	$gridKiefer config -editnotifycmd "Editable"
	# �criture des commentaires
	$gridKiefer set 0 0 -text "Parametres"
	set i 0
	foreach p $PARAMETRES_A_OPTIMISER {
	    $gridKiefer set 0 [expr $i + 1 ] \
		-text [lindex $PARAMETRES_A_OPTIMISER $i]
	    $gridKiefer set 1 [expr $i + 1 ] -text ""
	    $gridKiefer set 2 [expr $i + 1 ] -text ""
	    incr i
	}
	$gridKiefer set 1 0 -text "Initial"
	$gridKiefer set 2 0 -text "c"
	pack $pageKiefer.g_kiefer -pady 10

	label $pageKiefer.l_arret -text "CONDITIONS D'ARRET"
	label $pageKiefer.l_nb_iterations -text "Nb iterations"
	entry $pageKiefer.e_nb_iterations
	label $pageKiefer.l_ecart -text "Ecart        "
	label $pageKiefer.l_fin -text ""
	entry $pageKiefer.e_ecart
	pack $pageKiefer.l_arret -pady 10
	pack $pageKiefer.l_nb_iterations $pageKiefer.e_nb_iterations
	pack $pageKiefer.l_ecart $pageKiefer.e_ecart
	pack $pageKiefer.l_fin


	# page Partition2p
	label $pagePartition2p.l_nb_point -text "Nb point par cellule"
	entry $pagePartition2p.e_nb_point
	pack $pagePartition2p.l_nb_point $pagePartition2p.e_nb_point
	label $pagePartition2p.l_nb_meilleurs -text "Nb meilleures cellules gardees"
	entry $pagePartition2p.e_nb_meilleurs
	pack $pagePartition2p.l_nb_meilleurs $pagePartition2p.e_nb_meilleurs
	tixGrid $pagePartition2p.g_part  \
	    -width 2 -height [expr 1 + [llength $PARAMETRES_A_OPTIMISER]]
	set gridPartition2p $pagePartition2p.g_part
	$gridPartition2p config -formatcmd "EditGrid_format $gridPartition2p"
	$gridPartition2p config -editnotifycmd "Editable"
	# �criture des commentaires
	$gridPartition2p set 0 0 -text "Parametres"
	set i 0
	foreach p $PARAMETRES_A_OPTIMISER {
	    $gridPartition2p set 0 [expr $i + 1 ] \
		-text [lindex $PARAMETRES_A_OPTIMISER $i]
	    $gridPartition2p set 1 [expr $i + 1 ] -text ""
	    incr i
	}
	$gridPartition2p set 1 0 -text "Pas"
	pack $pagePartition2p.g_part -pady 20
	
	label $pagePartition2p.l_arret -text "CONDITIONS D'ARRET"
	label $pagePartition2p.l_nb_simul -text "Nb simulations max"
	entry $pagePartition2p.e_nb_simul
	pack $pagePartition2p.l_arret  -pady 10
	pack $pagePartition2p.l_nb_simul $pagePartition2p.e_nb_simul


	# frame de droite d�crivant les param�tres � optimiser ##############
	frame $w.f_para
	label $w.f_para.l_para -text "Description des param�tres � optimiser"
	tixGrid $w.f_para.g_para \
	    -width 4 -height [expr 1 + [llength $PARAMETRES_A_OPTIMISER]]
	set gridPara $w.f_para.g_para
	$gridPara config -formatcmd "EditGrid_format $gridPara"
	$gridPara config -editnotifycmd "Editable"
	# pas de proc�dure de v�rification de valeur (-editdonecmd)
	# car on peut commencer d'un tableau vide

	# �criture des commentaires
	$gridPara set 0 0 -text "Parametres"
	set i 0
	foreach p $PARAMETRES_A_OPTIMISER {
	    set no_ligne [expr $i + 1]
	    $gridPara set 0 $no_ligne \
		-text [lindex $PARAMETRES_A_OPTIMISER $i]
	    $gridPara set 1 $no_ligne -text ""
	    $gridPara set 2 $no_ligne -text ""
	    $gridPara set 3 $no_ligne -text ""
	    incr i
	}
	$gridPara set 1 0 -text "Minimum"
	$gridPara set 2 0 -text "Maximum"
	$gridPara set 3 0 -text "Type"

	pack $w.f_para -side left -padx 10 -pady 10
	pack $w.f_para.l_para 
	pack $w.f_para.g_para -pady 20

	label $w.f_para.l_optimisation -text "OPTIMISATION"
	tixOptionMenu $w.f_para.op_optimisation
	$w.f_para.op_optimisation add command maximiser -label "maximiser"
	$w.f_para.op_optimisation add command minimiser -label "minimiser"
	pack $w.f_para.l_optimisation  -pady 10
	pack $w.f_para.op_optimisation 

	label $w.f_para.l_climats -text "CLIMATS"
	tixOptionMenu $w.f_para.op_climats
	$w.f_para.op_climats add command aleatoires -label "aleatoires"
	$w.f_para.op_climats add command semi_aleatoires -label "aleatoires communs "
	$w.f_para.op_climats add command reels -label "reels"
	pack $w.f_para.l_climats  -pady 10
	pack $w.f_para.op_climats 

	# Chargement du critere temporaire s'il existe
	ChargerAlgorithmeTemporaire $w
    }
}



######################################################################
# Procedure   : Ok_ConfigurerAlgorithme
# Description : V�rification des valeurs donn�es et enregistrement
#               dans le fichier temporaire ad-hoc
######################################################################
proc Ok_ConfigurerAlgorithme {w} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_ALGORITHME_TXT
    global PARAMETRES_A_OPTIMISER
    global VIDE
    global ALEA

    global gridPara
    global gridSystematique
    global pageKiefer
    global gridKiefer
    global pagePartition2p
    global gridPartition2p

    # lecture des donn�es
    set algo_choisi [$w.f_algo.nb_algo raised]
    set a [$pageKiefer.e_A get]
    set b [$pageKiefer.e_B get]
    set nb [$pageKiefer.e_nb_iterations get]
    set ecart [$pageKiefer.e_ecart get]
    for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
	if  {[string compare $algo_choisi "systematique"] == 0} {
	    set pas($i) [$gridSystematique entrycget 1 $i -text]
	} else {
	    set pas($i) [$gridPartition2p entrycget 1 $i -text]
	}
	set ini($i) [$gridKiefer entrycget 1 $i -text]
	set c($i) [$gridKiefer entrycget 2 $i -text]	
	set min($i) [$gridPara  entrycget 1 $i -text] 
	set max($i) [$gridPara  entrycget 2 $i -text] 
	set typ($i) [$gridPara  entrycget 3 $i -text] 
    }		
    set optimiser [$w.f_para.op_optimisation cget -value] 
    set climats [$w.f_para.op_climats cget -value] 
    set sim [$pagePartition2p.e_nb_simul get]
    set pt [$pagePartition2p.e_nb_point get]
    set meilleures [$pagePartition2p.e_nb_meilleurs get]
    
    set erreur 0
    for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
	# on ne fait la v�rification que si le type est d�fini
	if {[string length [set typ($i)]] != 0} {
	    # V�rification de l'algorithme
	    if {[string compare $algo_choisi "systematique"] == 0 ||
		[string compare $algo_choisi "partition2p"] == 0 } {
		if [PbControleChaine $pas($i) reel] {
		    set erreur 1
		     set msg "Pour chaque param�tre definit (pour lequel le type est specifie : 'entier', 'reel'), le pas doit etre donne : c'est un reel positif."
		}
	    } elseif  {[string compare $algo_choisi "kiefer"] == 0} {
		if { ([PbControleChaine $ini($i) reel] \
			  && ([string compare $ini($i) "alea"] != 0)) \
			  || [PbControleChaine $c($i) reel]  } {
		    set erreur 1
		    set msg "Pour chaque param�tre definit (pour lequel le type est specifie : 'entier', 'reel'), la valeur initiale doit etre donnee : un reel ou le mot 'alea', ainsi que la valeur de c : un reel entre 0 non compris et 0.5 non compris."
		}
	    }
	    if {$erreur == 0} {
		# V�rification des param�tres
		if {[PbControleChaine $min($i) reel] \
			|| [PbControleChaine $max($i) reel] \
			|| ( $max($i) <  $min($i) ) \
			|| ($typ($i) != "entier" && $typ($i) != "reel") } {
		     set erreur 1
		    set msg "Pour chaque param�tre definit (pour lequel le type est specifie : 'entier', 'reel'), la valeur minimum et maximum doit etre donnee : un reel avec le maximum superieur ou egal au minimum."
		}
	    }
	}
	
	if {$erreur != 0} {
	    set i [expr 1 + [llength $PARAMETRES_A_OPTIMISER]]
	}
     }
    
    if {$erreur == 0} {
	if  {[string compare $algo_choisi "kiefer"] == 0} {
	    if {[PbControleChaine $a reel] \
		    || [PbControleChaine $b reel] \
		     || $b <= 0 || $b >= 0.5  } {
		set erreur 1
		set msg "Les coefficients A (reel positif) et B (reel entre 0 non compris et 0.5 non compris) doivent etre donnes."
	    }
	    if {[PbControleChaine $nb entier] && ($nb!="")} {
		 set erreur 1
		set msg "Le nombre d'iteration doit etre un entier positif"
	    }
	     if {[PbControleChaine $ecart reel] && ($ecart!="")} {
		 set erreur 1
		 set msg "L'ecart doit etre un reel positif."
	     }
	    if { ($ecart == "" && $nb == "") } {
		set erreur 1
		 set msg "Une au moins des conditions d'arret doit etre specifiee : nombre d'iteration (entier positif) ou ecart (reel positif)."
	    }
	} 
	 if {[string compare $algo_choisi "partition2p"] == 0} {
	     if {[PbControleChaine $meilleures entier] && ($meilleures != "")} {
		 set erreur 1
		 set msg "Le nombre de meilleures cellules gardees doit etre un entier positif"
	     }
	     if {[PbControleChaine $pt entier] && ($pt != "")} {
		 set erreur 1
		 set msg "Le nombre de points par cellule doit etre un entier positif"
	     }
	     if {[PbControleChaine $sim entier] && ($sim != "")} {
		 set erreur 1
		 set msg "Le nombre maximum de simulations realisees doit etre un entier positif"
	     }
	 }
     }
     
     if {$erreur != 0} {
	 CreerFenetreDialogue OPTI-SEPATOU error $msg
     } else {
	 # enregistrement de l'algorithme et des param�tres
	 set file [open "${DIR_OPTI-TEMPORAIRE}$FIC_ALGORITHME_TXT" w]
	 if {[string compare $algo_choisi "systematique"] == 0} {
             puts $file "Algorithme = Systematique"
	 } elseif  {[string compare $algo_choisi "kiefer"] == 0} {
             puts $file "Algorithme = Kiefer" 
	 } elseif  {[string compare $algo_choisi "partition2p"] == 0} {
             puts $file "Algorithme = Partition2p"
	 }
	 puts $file "Nom\tMin\tMax\tTyp\tPas\tInit\tc"
	 for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
	     if {[string length [set typ($i)]] == 0} {
		 set MIN $VIDE; set MAX $VIDE; set TYP $VIDE; 
		 set PAS $VIDE; set INI $VIDE; set C $VIDE
	     } else {
		 if {[string compare [set typ($i)] "entier"] == 0} {
		     set TYP 0
		 } else {
		     set TYP 1
		}
		 set MIN [set min($i)]; set MAX [set max($i)]
		 if {[string length [set pas($i)]] == 0} {set PAS $VIDE} else {set PAS [set pas($i)]}
		 if {[string length [set ini($i)]] == 0} {set INI $VIDE} else {set INI [set ini($i)]}
		 if {[string compare [set ini($i)] "alea"] == 0} {set INI $ALEA}
		 if {[string length [set c($i)]] == 0} {set C $VIDE} else {set C [set c($i)]}
	     }
	     set line [format "%s\t%f\t%f\t%f\t%f\t%f\t%f" \
			   Teta$i  $MIN $MAX $TYP $PAS $INI $C]
	     puts $file $line 
	 }
	 
	 if {$a == ""} {set a $VIDE}
	 puts $file "A = $a"
	 if {$b == ""} {set b $VIDE}
	 puts $file "B = $b"
	 if {$nb == ""} {set nb $VIDE}
	 puts $file "NbIterations = $nb"
	 if {$ecart == ""} {set ecart $VIDE}
	 puts $file "Ecart = $ecart"
	 puts $file "Optimiser = $optimiser"
	 if {$pt== ""} {set pt $VIDE}
         puts $file "NbPtCellule = $pt"
	 puts $file "Climats = $climats"
	 if {$sim== ""} {set sim $VIDE}
	 puts $file "NbSimulations = $sim"
	 if {$meilleures== ""} {set meilleures $VIDE}
	 puts $file "NbMeilleurs = $meilleures"

	 close $file
	 destroy $w
     }
}


######################################################################
# Procedure   : Imprimer_ConfigurerAlgorithme
# Description : Imprime le contenu de la fenetre de configuration d'algorithme
######################################################################
proc Imprimer_ConfigurerAlgorithme {w} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_ALGORITHME_TXT
    global PARAMETRES_A_OPTIMISER
    global CMD_REMOVE
    global CMD_A2PS
    
    global gridPara
    global gridSystematique
    global pageKiefer
    global gridKiefer
    global pagePartition2p
    global gridPartition2p


    # lecture des donn�es
    set algo_choisi [$w.f_algo.nb_algo raised]
    set a [$pageKiefer.e_A get]
    set b [$pageKiefer.e_B get]
    set nb [$pageKiefer.e_nb_iterations get]
    set ecart [$pageKiefer.e_ecart get]
    for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
	if  {[string compare $algo_choisi "systematique"] == 0} {
	    set pas($i) [$gridSystematique entrycget 1 $i -text]
	} else {
	    set pas($i) [$gridPartition2p entrycget 1 $i -text]
	}
	set ini($i) [$gridKiefer entrycget 1 $i -text]
	set c($i) [$gridKiefer entrycget 2 $i -text]	
	set min($i) [$gridPara  entrycget 1 $i -text] 
	set max($i) [$gridPara  entrycget 2 $i -text] 
	set typ($i) [$gridPara  entrycget 3 $i -text] 
    }		
    set optimiser [$w.f_para.op_optimisation cget -value] 
    set climats [$w.f_para.op_climats cget -value] 
    set sim [$pagePartition2p.e_nb_simul get]
    set pt [$pagePartition2p.e_nb_point get]
    set meilleures [$pagePartition2p.e_nb_meilleurs get]
    
    # �criture dans le fichier � imprimer
    set file [open ${DIR_OPTI-TEMPORAIRE}Algorithme.imp w]
    if {[string compare $algo_choisi "systematique"] == 0} {
	puts $file "Algorithme = Systematique"
    } elseif  {[string compare $algo_choisi "kiefer"] == 0} {
	puts $file "Algorithme = Kiefer" 
    } elseif  {[string compare $algo_choisi "partition2p"] == 0} {
	puts $file "Algorithme = Partition2p" 
    }
    puts $file "Nom\tMin\tMax\tType\tPas\tInit\tc"
    for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
	set MIN [set min($i)]; set MAX [set max($i)]
	set TYP [set typ($i)]
	set PAS [set pas($i)]; set INI [set ini($i)]; set C [set c($i)]
	set line [format "%s\t%s\t%s\t%s\t%s\t%s\t%s" \
		      Teta$i  $MIN $MAX $TYP $PAS $INI $C]
	puts $file $line 
    }
    
    puts $file "A = $a"
    puts $file "B = $b"
    puts $file "NbIterations = $nb"
    puts $file "Ecart = $ecart"
    puts $file "Optimiser = $optimiser"
    puts $file "NbPtCellule = $pt"
    puts $file "Climats = $climats"
    puts $file "NbSimulations = $sim"
    puts $file "NbMeilleurs = $Meilleures"

    close $file

    catch {exec $CMD_A2PS ${DIR_OPTI-TEMPORAIRE}Algorithme.imp}
    exec $CMD_REMOVE ${DIR_OPTI-TEMPORAIRE}Algorithme.imp

    CreerFenetreDialogue OPTI-SEPATOU info "Impression en cours"
}


######################################################################
# Procedure   : ChargerAlgorithmeTemporaire
# Description : lecture de la description de l'algorithme et des
#               param�tres dans le fichier temporaire ad-hoc s'il existe
######################################################################
proc ChargerAlgorithmeTemporaire {w} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_ALGORITHME_TXT
    global PARAMETRES_A_OPTIMISER
    global VIDE
    global ALEA
 
    global gridPara
    global gridSystematique
    global pageKiefer
    global gridKiefer
    global pagePartition2p
    global gridPartition2p
  
    if {[catch {set file [open "${DIR_OPTI-TEMPORAIRE}$FIC_ALGORITHME_TXT" r]}]  == 0} {
	
	# lecture des valeurs dans le fichier
	gets $file line
	set algorithme [SortirValeur $line]
	gets $file line ;# commentaires
	
	for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
	    gets $file line 
	    scan $line "%s %f %f %f %f %f %f" \
		para min($i) max($i) typ($i) pas($i) initial($i) c($i)
	    if { [set typ($i)] == 0 } {
		set typ($i) "entier"
	    } elseif { [set typ($i)] == 1 } {
		set typ($i) "reel"
	    }
	}
	gets $file line
	set A [SortirValeur $line]
	gets $file line
	set B [SortirValeur $line]
	gets $file line
	set nb_iterations [SortirValeur $line]
	gets $file line
	set ecart [SortirValeur $line]
	gets $file line
	set optimiser [SortirValeur $line]
        gets $file line
	set nb_pt_cellule [SortirValeur $line]
        gets $file line
	set climats [SortirValeur $line]
        gets $file line
	set nb_simulations [SortirValeur $line]
        gets $file line
	set nb_meilleurs [SortirValeur $line]

	# ecriture dans la fenetre
	if {[string compare $algorithme "Systematique"] == 0} {
	    $w.f_algo.nb_algo raise systematique
	    set gridSystematique [$w.f_algo.nb_algo subwidget systematique].g_sys
	    for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
		if {[set pas($i)] != $VIDE} {
		    $gridSystematique set 1 $i -text [set pas($i)]
		}
	    }
	} elseif {[string compare $algorithme "Kiefer"] == 0} {
	    $w.f_algo.nb_algo raise kiefer
	    $pageKiefer.e_A insert end $A
	    $pageKiefer.e_B insert end $B
	    for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
		if {[set initial($i)] != $VIDE} {
		    if  {[set initial($i)] == $ALEA} {
			$gridKiefer set 1 $i -text "alea"
		    } else {
			$gridKiefer set 1 $i -text [set initial($i)] 
		    }
		}
		if {[set c($i)] != $VIDE} {
		    $gridKiefer set 2 $i -text [set c($i)] 
		}
	    }
	    if {$nb_iterations != $VIDE} {
		$pageKiefer.e_nb_iterations insert end $nb_iterations
	    }
	    if {$ecart != $VIDE} {
		$pageKiefer.e_ecart insert end $ecart
	    }
	} elseif {[string compare $algorithme "Partition2p"] == 0} {
	    $w.f_algo.nb_algo raise partition2p
	    $pagePartition2p.e_nb_point insert end $nb_pt_cellule
	    $pagePartition2p.e_nb_meilleurs insert end $nb_meilleurs
	    $pagePartition2p.e_nb_simul insert end $nb_simulations
	    for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
		if {[set pas($i)] != $VIDE} {
		    $gridPartition2p set 1 $i -text [set pas($i)]
		}
	    }
	} 
	
	
	for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
	    if {[set min($i)] != $VIDE} {
		$gridPara set 1 $i -text [set min($i)] 
	    }
	    if {[set max($i)] != $VIDE} {
		$gridPara set 2 $i -text [set max($i)] 
	    }
	    if {[set typ($i)] != $VIDE} {
		$gridPara set 3 $i -text [set typ($i)] 
	    }
	}
	
	$w.f_para.op_optimisation configure -value $optimiser
	$w.f_para.op_climats configure -value $climats
    }
}


