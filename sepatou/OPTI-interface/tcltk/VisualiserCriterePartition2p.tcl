# Copyright (C) 2005 Marie-Jos�e Cros <cros@toulouse.inra.fr>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

######################################################################
# Fichier : VisualiserCriterePartition2p.tcl  
# Contenu : Visualisation du critere pour l'algorithme Partition2p
######################################################################

######################################################################
# Procedure   : VisualiserCritere
######################################################################
proc VisualiserCritere {} {

    global DIR_OPTI-TEMPORAIRE
    global FIC_CRITERE
    global FIC_ALGORITHME_TXT
    global PARAMETRES_A_OPTIMISER
    global no_fen
    global p1
    global p2
    global n
    global lcellule
    global paramin ;#array, indice de 1 � 10
    global paramax ;#array, indice de 1 � 10 

    set no_fen 0
    set w .wm_VisualiserCritere

    if {[winfo exists $w] == 0} {
	# lecture des infos dans les fichiers ################################
	# recherche des parametres utilis�s (lpara liste des indices (de 0 � 9)
	# de la liste PARAMETRES_A_OPTIMISER
	# et des domaines de recherche associ�s (paramin et paramax)
	list lpara
	if {[catch {set file [open "${DIR_OPTI-TEMPORAIRE}$FIC_ALGORITHME_TXT" r]}]  == 0} {
	    gets $file line ;# nom algorithme
	    gets $file line ;# commentaires
	    for {set i 1} {$i <= [llength $PARAMETRES_A_OPTIMISER]} {incr i} {
		gets $file line 
		scan $line "%s %f %f %f %f %f %f"  para paramin($i) paramax($i) typ pas initial c
		if { $typ == 0 || $typ ==1 } {lappend lpara [expr $i - 1]}
	    }
	}

	# lecture des cellules et des niveaux pr�sents
	set lcellule {}
	set lniveau {}
	set fileid [open ${DIR_OPTI-TEMPORAIRE}$FIC_CRITERE r]
	while {[eof $fileid] != 1} {
	    gets $fileid line
	    if {$line != {}} { ;# on enl�ve la derni�re ligne vide
		lappend lcellule $line
		set niveau [lindex $line 22]
		if {[lsearch $lniveau $niveau]  == -1} {
		    #le niveau n'est pas dans la liste, on l'ins�re
		    if {[llength $lniveau] == 0} {
			lappend lniveau $niveau
		    } else {
			set nb_niveau [llength $lniveau]
			for {set i 0} {$i < $nb_niveau} {incr i} {
			    if { $niveau < [lindex $lniveau $i] } {
				set lniveau [linsert $lniveau $i $niveau]
				set i $nb_niveau
			    } else {
				if { $i == [expr $nb_niveau - 1] } {
				    lappend lniveau $niveau
				}
			    }
			}
		    }
		}
	    }
	}
	close $fileid
	set nb_cellule [llength $lcellule]

	# cr�ation de la fenetre #############################################
	# la fenetre a 5 frames : f1 et f2 sont dans le frame f
	#  f1 (tableau des cellules)  | f2 (zone configuration affichage)
	#   -----------------------------------------------------------
	#         fbottom - fsep (zone boutons Afficher, Fermer)

	toplevel $w
	wm title $w "OPTI-SEPATOU : Visualisation des cellules optimum"

 	label $w.l_titre1 -text "VISUALISATION DES CELLULES OPTIMUM"
	pack $w.l_titre1 -pady 20

	frame $w.f ; pack $w.f
	# frame f1 : tableau des cellules optimum
	frame $w.f.f1 ; pack $w.f.f1 -side left
	tixScrolledGrid $w.f.f1.scrg_grille -bd 0 -width 600 -height 400
	pack $w.f.f1.scrg_grille -expand yes -padx 20 
	set grid [$w.f.f1.scrg_grille subwidget grid]
	$grid config -formatcmd "EditGrid_format $grid"
	# Definition des colonnes
	set nb_collones [expr [llength $lpara] * 2 + 3]
	for {set i 0} {$i < $nb_collones} {incr i} {
	    $grid size column $i -size 12char
	}
	Afficher_cellules_tableau $grid $lcellule $lpara 


	# frame f2 : configuration de l'affichage
	frame $w.f.f2 -borderwidth 1 -relief sunken
	pack $w.f.f2 -side top -pady 40 -padx 20
 	label $w.f.f2.l_titre -text "AFFICHAGE"
	pack $w.f.f2.l_titre -pady 15
	label $w.f.f2.l_para -text "S�lectionner parametres"
	pack $w.f.f2.l_para -pady 10
	tixOptionMenu $w.f.f2.t_p1 -variable p1 -dynamicgeometry true
	tixOptionMenu $w.f.f2.t_p2 -variable p2 -dynamicgeometry true
	foreach i $lpara  { 
	    $w.f.f2.t_p1 add command [lindex $PARAMETRES_A_OPTIMISER $i] 
	    $w.f.f2.t_p2 add command [lindex $PARAMETRES_A_OPTIMISER $i] 
	} 
	pack $w.f.f2.t_p1 $w.f.f2.t_p2 -pady 5
 	label $w.f.f2.l_niveau -text "S�lectionner 1 niveau"
	pack $w.f.f2.l_niveau -pady 20
	tixOptionMenu $w.f.f2.t_n -variable n -dynamicgeometry true
	$w.f.f2.t_n add command tous
	foreach niveau $lniveau {
	    $w.f.f2.t_n add command $niveau
	}
	pack $w.f.f2.t_n 
 	label $w.f.f2.l_nb -text "Nb cellules ( <= $nb_cellule)"
	entry $w.f.f2.e_nb  -width 10 -relief sunken
	$w.f.f2.e_nb insert 0 $nb_cellule

 	label $w.f.f2.l_blanc -text ""
	pack $w.f.f2.l_nb -pady 20
	pack $w.f.f2.e_nb 
	pack $w.f.f2.l_blanc -pady 10

	# frames du bas : boutons Afficher, Fermer
	frame $w.f_bottom -borderwidth 3
	pack $w.f_bottom -side bottom -fill x -pady 5
	button $w.f_bottom.b_afficher -text Afficher \
	    -command "Afficher_Critere $w"
	button $w.f_bottom.b_fermer -text Fermer \
	    -command "Fermer_Critere $w"
	pack $w.f_bottom.b_afficher $w.f_bottom.b_fermer -side left -expand 1
	frame $w.f_sep -width 100 -height 2 -borderwidth 1 -relief sunken
	pack $w.f_sep -side bottom -fill x -pady 5
    }
}


######################################################################
# Procedure   : Fermer_Critere
######################################################################
proc Fermer_Critere {w} {
    set lfenetres [winfo children .] 
    DetruireFenetresPrefixees $w  $lfenetres
}


######################################################################
# Procedure   : Afficher_Critere
######################################################################
proc Afficher_Critere {w} {

    global no_fen
    global lcellule
    global p1
    global p2
    global n

    # v�rification du nb de cellules � consid�rer
    set nb_cel [llength $lcellule]
    set n_cel_voulu [$w.f.f2.e_nb get]
    if {[PbControleChaine $n_cel_voulu entier] || $n_cel_voulu > $nb_cel} {
	CreerFenetreDialogue OPTI-SEPATOU error \
	    "Le nombre de cellules doit etre un nombre entier inf�rieur � $nb_cel"
    } else {
	# cr�ation d'une liste des cellules de niveau voulu
	set lcel {}
	for {set i 0} {$i < $n_cel_voulu} {incr i} {
	set c [lindex $lcellule $i]	    
	    if {$n == "tous" || [lindex $c 22] == $n} {
		lappend lcel $c
	    }
	}

	if {[llength $lcel] == 0} {
	CreerFenetreDialogue OPTI-SEPATOU error \
	    "La param�trisation ne s�lectionne aucune cellule � afficher"
	} else {
	    
	    # cr�ation de la fenetre de visualisation
	    #         f1 (boutons choix graphique ou num�rique)
	    #-----------------------------------------------------
	    #         f2 (graphique+f3)/ (num�rique)
	    #------------------------------------------------------
	    #         f5 - f6
	    incr no_fen
	    set w_visu $w$no_fen
	    toplevel $w_visu
	    wm title $w_visu "OPTI-SEPATOU : Visualisation des bornes de $p1 et $p2 des cellules optimum pour le niveau $n "
	    
	    # variable globale 0 : ni graph ni tableau cr��s, 1 : graph cr��, 
	    # 2 : graph et tableau cr��s
	    global affichage_critere
	    set affichage_critere($w_visu) 0
	    
	    # frame f1
	    frame $w_visu.f1 -borderwidth 3
	    pack $w_visu.f1 -side top
	    button $w_visu.f1.b_graf -text Graphique \
		-command "Afficher_Critere_Graphique $w_visu [list $lcel]" 
	    button $w_visu.f1.g_num -text Numerique \
		-command "Afficher_Critere_Numerique $w_visu [list $lcel]"
	    pack $w_visu.f1.b_graf  $w_visu.f1.g_num -side left
	    
	    # frame f2
	    frame $w_visu.f2 -borderwidth 2
	    pack $w_visu.f2 -side top
	    Afficher_Critere_Graphique $w_visu $lcel
	    
	    # frame f5 - f6
	    frame $w_visu.f5 -borderwidth 3
	    pack $w_visu.f5 -side bottom -fill x -pady 5
	    button $w_visu.f5.b_fermer -text Fermer -command "destroy $w_visu" 
	    pack $w_visu.f5.b_fermer -side left -expand 1 
	    frame $w_visu.f6 -width 100 -height 2 -borderwidth 1 -relief sunken
	    pack $w_visu.f6 -side bottom -fill x -pady 5
	}
    }
}


######################################################################
# Procedure   : Afficher_Critere_Graphique
######################################################################
proc Afficher_Critere_Graphique {w lcellule_n} {

    global paramin
    global paramax
    global p1
    global p2
    global affichage_critere

    if {$affichage_critere($w) == 0} {
	set affichage_critere($w) 1

	# initialisation de la palette de couleurs
	set palette [list black blue green yellow orange red] 
	for {set j 0} {$j < [llength $palette]} {incr j} {
	    set color($j) [lindex $palette $j] 
	}
	# on considere une couleur de moins, le derniere �tant pour le max
	set nb_color [expr [llength $palette] - 1]
	# recherche de l'intervalle de variation du critere
	set c [lindex $lcellule_n 0]
	set critere [lindex $c 20]
	set critere_min $critere
	set critere_max $critere
	for {set i 1} {$i < [llength $lcellule_n]} {incr i} {
	    set c [lindex $lcellule_n $i]
	    set critere [lindex $c 20]
	    if {$critere < $critere_min} {
		set critere_min $critere
	    }
	    if {$critere > $critere_max} {
		set critere_max $critere
	    }
	}

	# multiplication par 1.0 pour convertir en re�l
	# dans le cas o� la diff�rence est enti�re
	set pas_critere [expr ($critere_max - $critere_min)*1.0 / $nb_color]

	# cr�ation du graphe 
	graph $w.f2.g_graphique -title "Zones optimum trouv�es pour $p1 et $p2" \
	    -width 600 -height 600 -background grey75 
	pack $w.f2.g_graphique -padx 10 -side left

	# recherche des indices des parametres (de 0 � 9)
	set i_p1 [string range $p1 4 end]
	set i_p2 [string range $p2 4 end]
	$w.f2.g_graphique axis configure "x" \
	    -min $paramin($i_p1) -max $paramax($i_p1) 
	$w.f2.g_graphique axis configure "y" \
	    -min $paramin($i_p2) -max $paramax($i_p2)
	    
	# pour chaque cellule cr�ation du carr� ad hoc
	# on parcours la liste de cellule des plus mauvaises (les derni�res)
	# vers les meilleures (les 1eres), ainsi les cellules les meilleures 
	# seront visibles (cas o� memes valeurs pour les 2 parametres choisis
	# avec des valeurs diff�rentes pour les autres param�tres)
	for {set i [expr [llength $lcellule_n] - 1]} {$i >= 0 } {set i [expr $i-1]} {
	    set c [lindex $lcellule_n $i]
	    set min_p1 [lindex $c [expr ($i_p1 - 1)*2 + 0]]
	    set max_p1 [lindex $c [expr ($i_p1 - 1)*2 + 1]]
	    set min_p2 [lindex $c [expr ($i_p2 - 1)*2 + 0]]
	    set max_p2 [lindex $c [expr ($i_p2 - 1)*2 + 1]]
	    set critere [lindex $c 20]
	    if {$pas_critere != 0} {
		set no_color [expr [expr $critere - $critere_min ] / $pas_critere]
		set i_point [string first "." $no_color]
		if {$i_point != -1} {
		    set no_color [string range $no_color 0 [expr $i_point - 1]]
		}
	    } else {
		set no_color 0
	    }
	    $w.f2.g_graphique marker create polygon \
		-coords {$min_p1 $min_p2 $max_p1 $min_p2  $max_p1 $max_p2 \
			     $min_p1 $max_p2} -under 1 -fill $color($no_color)
	}

	# affichage de la l�gende et de l'avertissement
	frame $w.f2.f3
	pack $w.f2.f3 -side right -expand yes 

	canvas $w.f2.f3.cv_legende -bg gray85 -width 250 -height 200 
	pack  $w.f2.f3.cv_legende -padx 10
	$w.f2.f3.cv_legende create text 90 15 -text "Valeur du critere:"
	set place 10
	if {$pas_critere != 0} {
	    set i_max [llength $palette]
	} else {
	    set i_max 1
	}
	for {set i [expr $i_max-1]} {$i>=0} {set i [expr $i-1]} {
	    set place [expr $place+20]
	    $w.f2.f3.cv_legende create rect 10 $place 30 [expr $place+20] -fill $color($i)
	    $w.f2.f3.cv_legende create text 40 [expr $place+10] \
		-text "\[[expr $critere_min + $i*$pas_critere] ; [expr $critere_min + ($i+1)*$pas_critere] \[" -anchor w
	}

	label $w.f2.f3.l_attention -text "
[llength $lcellule_n] cellule(s) a(ont) �t� s�lectionn�e(s).

ATTENTION
Lorsque plusieurs cellules
sont supperpos�es (m�mes intervalles
pour les 2 parametres choisis), seule
la meilleure cellule est visible,
soit max J(para1,para2,para3) .
    para2.

De toutes petites cellules peuvent
ne pas �tre visualisables." -width 40 -font 10 -justify left
	pack $w.f2.f3.l_attention -pady 20
    } else {
	if {$affichage_critere($w) == 2} {
	    pack forget $w.f2.scrg_grille
	    pack $w.f2.g_graphique -side left -padx 10
	    pack $w.f2.f3 -side right -expand yes 
	}
    }


    # changement d'�tat des boutons
    $w.f1.b_graf configure -state disabled 
    $w.f1.g_num configure -state normal
}


######################################################################
# Procedure   : Afficher_Critere_Numerique
######################################################################
proc Afficher_Critere_Numerique {w lcellule_n} {
    global p1
    global p2
    global affichage_critere

    pack forget $w.f2.g_graphique 
    pack forget $w.f2.f3

    if {$affichage_critere($w) == 1} {
	# creation du tableau
	set affichage_critere($w) 2
	tixScrolledGrid $w.f2.scrg_grille -bd 0 -width 840 -height 580
	pack $w.f2.scrg_grille -expand yes -padx 20 -pady 10
	set grid [$w.f2.scrg_grille subwidget grid]
	$grid config -formatcmd "EditGrid_format $grid"
	
	# Definition de la taille des colonnes
	for {set i 0} {$i < 7} {incr i} {
	    $grid size column $i -size 10char
	}
	    
	# recherche des indices des parametres (de 0 � 9)
	set i_p1 [expr [string range $p1 4 end] - 1]
	set i_p2 [expr [string range $p2 4 end] - 1]
	Afficher_cellules_tableau $grid $lcellule_n [list $i_p1 $i_p2]
    } else {
	pack $w.f2.scrg_grille -expand yes -padx 20 -pady 10
    }

    # changement d'�tat des boutons
    $w.f1.b_graf configure -state normal 
    $w.f1.g_num configure -state disabled
}


######################################################################
# Procedure   : Afficher_cellules_tableau
#               grid : tableau
#               lpara_utilise : liste des indices des para utilis�s
######################################################################
proc Afficher_cellules_tableau {grid lcel lpara_utilise} {

    global PARAMETRES_A_OPTIMISER

    $grid set 0 0 -text "num�ro"
    set col 1
    foreach i $lpara_utilise  { 
	$grid set $col 0 -text "[lindex $PARAMETRES_A_OPTIMISER $i] min"
	$grid set [expr $col + 1] 0 -text "[lindex $PARAMETRES_A_OPTIMISER $i] max"
	set col [expr $col + 2]
    } 
    $grid set $col 0 -text "critere"
    $grid set [expr $col + 1] 0 -text "variance"
    $grid set [expr $col + 2] 0 -text "niveau"
    
    set ligne 1
    # pour chaque cellule affichage des donn�es
    for {set i 0} {$i < [llength $lcel]} {incr i} {
	$grid set 0 $ligne -text [expr $i + 1]
	set c [lindex $lcel $i]
	set col 1
	foreach i_p $lpara_utilise  { 
	    set min_p [lindex $c [expr $i_p*2 + 0]]
	    set max_p [lindex $c [expr $i_p*2 + 1]]
	    $grid set $col $ligne -text $min_p
	    $grid set [expr $col + 1] $ligne -text $max_p
	    set col [expr $col + 2]
	} 
	set critere [lindex $c 20]
	set variance [lindex $c 21]
	set niveau [lindex $c 22]
	$grid set $col $ligne -text $critere
	$grid set [expr $col + 1] $ligne -text $variance
	$grid set [expr $col + 2] $ligne -text $niveau
	
	incr ligne
    }
}
